<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); 
Route::post("/register/regDato", array('uses' => 'App\Http\Controllers\RegisterController@registraDatos'));
// [imprime solicitud]
Route::get('/register/imprimeSolicitud_afp/{id}', ['uses' => 'App\Http\Controllers\RegisterController@reporteDocumento']);
// Route::post("/register/{id}/regDato", array('uses' => 'App\Http\Controllers\RegisterController@registraDatos'));

// $ruta = request()->segments();
// $urlAccion = $ruta == null? '' : $ruta[0];
// $action = $ruta == null? '' : (isset($ruta[1])? $ruta[1] : '');

// Route::group(['middleware' => 'auth'], function () {
    $controller = 'App\Http\Controllers';
    $ruta = request()->segments();
    $urlAccion = $ruta == null? '' : $ruta[0]; 

    $action = $ruta == null? '' : (isset($ruta[1])? $ruta[1] : '');

    // ---------------------------------------------------------------------------------
    // -------------------------- Pestaña [ ADMINISTRACIÓN ] ---------------------------
    // ---------------------------------------------------------------------------------
    // [personal]
    route::resource('personal', $controller.'\Administracion\DatosgenericosController');
    // [roles]
    route::resource('roles', $controller.'\Administracion\RolesController');
    // ---------------------------------------------------------------------------------

    route::resource('almacen', $controller.'\AlmacenController');
	// ---------------------------------------------------------------------------------
    // ----------------------------- Pestaña [ AFILIACIÓN ] ----------------------------
    // ---------------------------------------------------------------------------------
    // [poblacion]
    route::resource('hcl_poblacion', $controller.'\Afiliacion\Hcl_poblacionController'); 
    Route::post($urlAccion."/buscaEmpleadorCreate", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaEmpleador'));
    Route::post($urlAccion."/{id}/buscaEmpleador", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaEmpleador'));
    Route::post("/hcl_poblacion/buscaParentescoCreate", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaParentesco'));
    Route::get("/hcl_poblacion/{id}/createBeneficiario", array('as' => 'createBeneficiario', 'uses' => $controller.'\Afiliacion\Hcl_poblacionController@createBeneficiario'));
    Route::post("/hcl_poblacion/{id}/beneficiario", array('as' => 'storeBeneficiario', 'uses' => $controller.'\Afiliacion\Hcl_poblacionController@storeBeneficiario'));
    Route::resource('seg_ficha_generada', $controller.'\Seguimiento\Seg_ficha_generadaController');
    
    // [empresa]
    route::resource('empresa', $controller.'\Afiliacion\Hcl_empleadorController');
    // [cuaderno]
    route::resource('cuaderno', $controller.'\Afiliacion\Hcl_cuadernoController');
    // [cuaderno_personal]
    route::resource('cuaderno_personal', $controller.'\Afiliacion\Hcl_cuaderno_personalController');
    // Route::post("/cuaderno_personal/{id}/buscaMedico", array('uses' => 'App\Http\Controllers\Administracion\DatosgenericosController@busca_Medico'));
    // [plantilla_personal]
    route::resource('plantilla_personal', $controller.'\Afiliacion\Hcl_plantillaController'); 
     // [reporte]  
    route::resource('hcl_reporte', $controller.'\Salud\PdfController');
    Route::get('hcl_reporte/imprimeDocumento/{id}', array('uses' => $controller.'\Salud\PdfController@imprimeDocumento'));


    // ---------------------------------------------------------------------------------------------
    // -------------------------------------- [ MEDICINA DEL TRABAJO ] --------------------------------------
    // ---------------------------------------------------------------------------------------------
    // [med_clasificacion]
    Route::resource('med_clasificacion', $controller.'\Medicina_trabajo\Med_clasificacionController'); 
    // [med_administracion]
    Route::resource('med_administracion', $controller.'\Medicina_trabajo\Med_administracionController'); 
    // [med_registro]
    Route::resource('med_registro', $controller.'\Medicina_trabajo\Med_registroController'); 
    // // [med_datos]
    // Route::resource('med_datos', 'Medicina_trabajo\Med_datosController'); 
    // [med_lista_cargos]
    Route::resource('med_lista_cargos', $controller.'\Medicina_trabajo\Med_lista_cargosController'); 
    Route::post("/".$urlAccion."/buscarPersona", array('uses' => $controller.'\Medicina_trabajo\Med_lista_cargosController@buscar_Persona')); 
    Route::post("/".$urlAccion."/asignaCargo", array('uses' => $controller.'\Medicina_trabajo\Med_lista_cargosController@asigna_Cargo')); 
    Route::post($urlAccion."/buscaClasificacionCreate", array('uses' => $controller.'\Medicina_trabajo\Med_clasificacionController@buscaMed_clasificacion'));

    // Route::post($urlAccion."/buscaClasificacionCreate", array('uses' => 'Medicina_trabajo\Med_clasificacionController@buscaMed_clasificacion'));
    // [imprime solicitud]
    Route::get($urlAccion.'/imprimeSolicitud_Salud/{id}', ['uses' => $controller.'\Medicina_trabajo\ReporteController@reporteDocumento']);
    // [valida datos] 
    Route::post($urlAccion."/{id}/validarRegistro", array('uses' => $controller.'\Medicina_trabajo\Med_registroController@validarRegistro'));
    // [Elimina datos] 
    Route::post($urlAccion."/{id}/eliminarRegistro", array('uses' => $controller.'\Medicina_trabajo\Med_registroController@eliminarRegistro'));
    // [med_cuaderno_personal]
    Route::resource('med_cuaderno_personal', $controller.'\Medicina_trabajo\Med_cuaderno_personalController'); 

    // [hsi_actividad_economica_division]
    Route::resource('hsi_division', $controller.'\Medicina_trabajo\Hsi_actividad_economica_divisionController'); 
    // [hsi_actividad_economica_agrupacion]
    Route::resource('hsi_agrupacion', $controller.'\Medicina_trabajo\Hsi_actividad_economica_agrupacionController'); 
    // [hsi_tipo_registro]
    Route::resource('hsi_tipo_registro', $controller.'\Medicina_trabajo\Hsi_tipo_registroController'); 
    // [hsi_tipo]
    Route::resource('hsi_tipo', $controller.'\Medicina_trabajo\Hsi_tipoController'); 
    // [hsi_registro_datos]
    Route::resource('hsi_registro_datos', $controller.'\Medicina_trabajo\Hsi_registro_datosController'); 
    Route::post($urlAccion."/buscaPaciente_hsiCreate", array('uses' => $controller.'\Medicina_trabajo\Hsi_registro_datosController@buscaPaciente_hsi'));
    Route::post($urlAccion."/{id}/buscaPaciente_hsi", array('uses' => $controller.'\Medicina_trabajo\Hsi_registro_datosController@buscaPaciente_hsi'));

    Route::post($urlAccion."/buscaHsi_divisionCreate", array('uses' => $controller.'\Medicina_trabajo\Hsi_actividad_economica_divisionController@buscaHsi_division'));
    Route::post($urlAccion."/{id}/buscaHsi_division", array('uses' => $controller.'\Medicina_trabajo\Hsi_actividad_economica_divisionController@buscaHsi_division'));
    Route::post($urlAccion."/muestraDatosHsiCreate", array('uses' => $controller.'\Medicina_trabajo\Hsi_tipoController@muestraDatosHsi'));
    Route::post($urlAccion."/{id}/muestraDatosHsi", array('uses' => $controller.'\Medicina_trabajo\Hsi_tipoController@muestraDatosHsi'));  

    Route::post($urlAccion."/buscaPaciente_solicitudCreate", array('uses' => $controller.'\Medicina_trabajo\Med_registroController@buscaPaciente_solicitud'));
    Route::post($urlAccion."/{id}/buscaPaciente_solicitud", array('uses' => $controller.'\Medicina_trabajo\Med_registroController@buscaPaciente_solicitud'));
      
    // [med_programacion]
    // Route::resource('med_programacion', 'Medicina_trabajo\Med_programacionController'); 
    // // -------------------->> "Programados"
    // Route::post($urlAccion."/buscaPoblacion_med", ['uses' => 'Medicina_trabajo\Med_programacionController@buscaPoblacionMed']);
    // Route::post($urlAccion."/{id}/buscaPoblacion_med", ['uses' => 'Medicina_trabajo\Med_programacionController@buscaPoblacionMed']);
    // ------------------------------------ [ FIN MEDICINA DEL TRABAJO ] ------------------------------------

    // ---------------------------------------------------------------------------------
    // ------------------------------ Pestaña [ FARMACIA ] -----------------------------
    // ---------------------------------------------------------------------------------
    // [TIPOS DE TRANSACCIONES]
    route::resource('far_adm_tipotransaccion', $controller.'\Farmacia\Far_adm_tipotransaccionController');
    // [far_sucursales]
    route::resource('sucursal', $controller.'\Farmacia\Far_sucursalesController'); 
    // [far_adm_articulo]
    route::resource('articulo', $controller.'\Farmacia\Far_adm_articuloController'); 
    // ------------------------------------ [ FIN FARMACIA ] ------------------------------------
    
    // ---------------------------------------------------------------------------------
    // ------------------------------- Pestaña [ SALUD ] -------------------------------
    // ---------------------------------------------------------------------------------
    // [consulta_externa]
    
    route::resource('hcl_cabecera_registro', $controller.'\Salud\Hcl_cabecera_registroController');
    route::resource('hcl_lista_atencion', $controller.'\Salud\Hcl_lista_atencionController');
    Route::post($urlAccion."/buscaProductoCuadernoCreate", array('uses' => 'Farmacia\Far_articulo_sucursalController@buscaProductoCuaderno'));
    Route::post($urlAccion."/{id}/buscaProductoCuaderno", array('uses' => 'Farmacia\Far_articulo_sucursalController@buscaProductoCuaderno'));
    Route::post($urlAccion."/muestraVariables_signos_vitalesCreate", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@muestraVariables_signos_vitales'));
    Route::post($urlAccion."/muestraHcl_cieCreate", array('uses' => $controller.'\Afiliacion\Hcl_cieController@muestraHcl_cie'));
    Route::post($urlAccion."/{id}/muestraHcl_cie", array('uses' => $controller.'\Afiliacion\Hcl_cieController@muestraHcl_cie'));
    // muestra historial clinico
    Route::post($urlAccion."/muestraHistoriaClinicoCreate", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@muestra_HistoriaClinico'));
    Route::post($urlAccion."/{id}/muestraHistoriaClinico", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@muestra_HistoriaClinico'));
    Route::post($urlAccion."/verifica_opciones_reporte", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@verifica_opciones_reporte'));

    Route::get($urlAccion."/imprime_Salud/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_Salud'));
    Route::get($urlAccion."/imprime_Receta/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_Receta'));
    Route::get($urlAccion."/imprime_Laboratorio/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_Laboratorio'));
    Route::get($urlAccion."/imprime_Imagenologia/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_Imagenologia'));
    Route::get($urlAccion."/{id}/imprime_Imagenologia", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_Imagenologia'));

    Route::get($urlAccion."/imprime_CertificadoMedico/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_CertificadoMedico'));
    Route::get($urlAccion."/{id}/imprime_CertificadoMedico", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_CertificadoMedico'));

    Route::get($urlAccion."/imprime_BajaMedica/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_BajaMedica'));
    Route::get($urlAccion."/{id}/imprime_BajaMedica", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_BajaMedica'));

    Route::get($urlAccion."/imprime_ReferenciaMedica/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_ReferenciaMedica'));
    Route::get($urlAccion."/{id}/imprime_ReferenciaMedica", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_ReferenciaMedica'));    

    Route::get($urlAccion."/imprime_InternacionMedica/{id}", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@imprime_InternacionMedica')); 
    // "Botón buscar Plantilla"
    Route::post($urlAccion."/buscaplantillas_saludCreate", array('uses' => $controller.'\Afiliacion\Hcl_plantillaController@buscador_plantillas_salud'));
    Route::post($urlAccion."/{id}/buscaplantillas_salud", array('uses' => $controller.'\Afiliacion\Hcl_plantillaController@buscador_plantillas_salud'));
    // "Botón registrar Plantilla"
    Route::post($urlAccion."/registrarplantilla_saludCreate", array('uses' => $controller.'\Afiliacion\Hcl_plantillaController@registrar_plantilla_salud'));
    Route::post($urlAccion."/{id}/registrarplantilla_salud", array('uses' => $controller.'\Afiliacion\Hcl_plantillaController@registrar_plantilla_salud'));
    // "Botón busca articulos farmacia"
    Route::post($urlAccion."/buscaProductoCreate", array('uses' => $controller.'\Farmacia\Far_articulo_sucursalController@buscaProducto'));
    Route::post($urlAccion."/{id}/buscaProducto", array('uses' => $controller.'\Farmacia\Far_articulo_sucursalController@buscaProducto'));
     // "elimina receta"
    Route::post($urlAccion."/{id}/elminarReceta", array('uses' => $controller.'\Salud\Hcl_cabecera_registroController@elminar_Receta'));
    
    // "Buscador Especialidad"
    Route::post($urlAccion."/muestraEspecialidadCreate", array('uses' => $controller.'\Afiliacion\Hcl_cuadernoController@muestraEspecialidad')); 
    Route::post($urlAccion."/{id}/muestraEspecialidad", array('uses' => $controller.'\Afiliacion\Hcl_cuadernoController@muestraEspecialidad'));
    // "Buscador hcl_fichas_registro"    
    route::resource('hcl_fichas_registradas', $controller.'\Afiliacion\Hcl_fichas_registradasController'); 

    Route::post($urlAccion."/buscaPaciente_postCreate", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@buscaPaciente_post'));
    Route::post($urlAccion."/{id}/buscaPaciente_post", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@buscaPaciente_post'));

    // "Buscador hcl_fichas_registro"    
    route::resource('hcl_fichas_registro', $controller.'\Afiliacion\Hcl_fichas_registroController'); 
    // [Elimina datos] 
    Route::post($urlAccion."/{id}/eliminarFichas", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registroController@eliminarFichas'));
    // [registrar atencion] 
    Route::post($urlAccion."/{id}/registraAtencion", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@registraAtencion'));
    // [Eliminar atencion] 
    Route::post($urlAccion."/{id}/eliminarAtencion", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@eliminarAtencion'));
    // [imprime atencion] 
    Route::get($urlAccion."/imprimeFicha/{id}", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@reporteFicha'));
    // [imprime atencion] 
    Route::get($urlAccion."/imprimeMedicina/{id}", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@reporteMedicina'));
    // [imprime lista] 
    Route::get($urlAccion."/reporteLista/{id}", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@reporteLista'));
    // [busca paciente programado] 
    // Route::post($urlAccion."/buscaPaciente_fichaCreate", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@buscaPaciente_ficha'));
    Route::post($urlAccion."/{id}/buscaPaciente_ficha", array('uses' => $controller.'\Afiliacion\Hcl_fichas_registradasController@buscaPaciente_ficha')); 
    
    // *****************************************************************************************************
    // ---------------------------------------- [ RUTAS GENERALES ] ----------------------------------------
    // ***************************************************************************************************** 
    Route::post($urlAccion."/buscaPacientesCreate", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaPacientes'));
    Route::post($urlAccion."/{id}/buscaPacientes", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaPacientes'));
    Route::post($urlAccion."/buscaEspecialidadCreate", array('uses' => $controller.'\Afiliacion\Hcl_cuadernoController@busca_Especialidad'));
    Route::post($urlAccion."/{id}/buscaEspecialidad", array('uses' => $controller.'\Afiliacion\Hcl_cuadernoController@busca_Especialidad'));
    Route::post($urlAccion."/buscaMedicoCreate", array('uses' => $controller.'\Administracion\DatosgenericosController@busca_Medico'));
    Route::post($urlAccion."/{id}/buscaMedico", array('uses' => $controller.'\Administracion\DatosgenericosController@busca_Medico'));
    
    Route::post($urlAccion."/buscaExamenesCreate", array('uses' => $controller.'\Imagenologia\Ima_registroprogramacionController@buscaExamenes'));
    Route::post($urlAccion."/{id}/buscaExamenes", array('uses' => $controller.'\Imagenologia\Ima_registroprogramacionController@buscaExamenes'));

    Route::post($urlAccion."/buscaEmpleadorCreate", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaEmpleador'));
    Route::post($urlAccion."/{id}/buscaEmpleador", array('uses' => $controller.'\Afiliacion\Hcl_poblacionController@buscaEmpleador'));
    // *****************************************************************************************************

// });