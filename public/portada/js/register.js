$(document).ready( function () { 
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
    $('#datos').DataTable({
        "language":{
            "search": "Buscar",
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "paginate":{
                "previous": "Anterior",
                "next": "Siguiente",
                "first": "Primero",
                "last": "Ultimo"
            }
        }
    });
} );

$('#registro').click(function() {
   $('#cardDatos').css({display: 'none'});
   $('#vImprimeSolicitud').css({display: 'none'});
   $('#cardRegistro').css({display: ''});
});

$('#btnSuccess').click(function() { 
   $('#cardDatos').css({display: ''});
   $('#cardRegistro').css({display: 'none'});
   $('#vImprimeSolicitud').css({display: 'none'}); 
   $.ajax(
   {
        url: '/register/regDato',
        type: 'post',
        data: {  
            documento: $('#txtDocumento').val(),
            complemento: $('#txtComp').val(),
            paterno: $('#txtPaterno').val(),
            materno: $('#txtMaterno').val(),
            casada: $('#txtApellido').val(),
            nombres: $('#txtNombres').val(),
            _token: $('#token').val()
        },     
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

$('#btnCancel').click(function() {
   $('#cardDatos').css({display: ''});
   $('#cardRegistro').css({display: 'none'});
   $('#vImprimeSolicitud').css({display: 'none'});
});
$('#btnCancel_print').click(function() {
   $('#cardDatos').css({display: ''});
   $('#cardRegistro').css({display: 'none'});
   $('#vImprimeSolicitud').css({display: 'none'});
});

// ========================================= [ REPORTE ] =========================================
var imprimirSolicitud = function(id) {  
   	$('#cardDatos').css({display: 'none'});
   	$('#vImprimeSolicitud').css({display: ''});
   	$('#cardRegistro').css({display: 'none'});
    $('.progress').show();
    
    var parametro = {
        opcion: 1,
        id: id,
    };
    $('#imprimeSolicitud').html("").append($("<iframe></iframe>", {
        src: '/register/imprimeSolicitud_afp/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
//  ---------------->> FIN "REPORTE"
// ===============================================================================================
