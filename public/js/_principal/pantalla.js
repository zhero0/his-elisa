var tiempoLimite = 10000;
// var tiempoLimite = 130000;
var moduloURL = '';

$( document ).ready(function() {

	if($('#txtScenario').val() == 'index')
	{
		if($('#txtBuscar').val() != '')
		{
			/*
				miligendo   segundo	  minuto
				1,000	 	1		  0,0166667
				60,000		60 		  1
				120,000		120 	  2
			*/
		    setTimeout(function() {
		        $('#btnNuevaBusqueda').click();

		        // DESPUES DE 1 segundos..el focus se posiciona para la escritura
			    setTimeout(function() {
			        $('#txtBuscar').click();
			    }, 1000);
		    }, tiempoLimite);
	    }
	    else
	    	$('#txtBuscar').click();
    }

    if($('#txtScenario').val() == 'view')
    {
    	setTimeout(function() {
	        window.location = "/inter_pantalla"
	    }, tiempoLimite);
    }
});


$('#btnNuevaBusqueda').click(function() {
	$('#txtBuscar').val('');
	$('#formBuscador').submit();
	$('#txtBuscar').focus();
});