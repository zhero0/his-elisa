var modal_OPCION = 0;
var modal_ROUTE = "";
var altoSeleccionado = 0;
var indiceSelected = -1;

let eventTime, paciente, activeSearch;

$(document).ready(function () {
    if ($("#txtScenario").val() == "index") {
        mouseEnter();
    }

    if ($("#txtScenario").val() == "view") {
        $("#form :input").prop("disabled", true);
        setTimeout(() => {
            $("#btnSalir").prop("disabled", false);
        }, 200);
    }

    setTimeout(function () {
        $("#divMensaje, #divMensajeError").hide();
    }, 5000);

    $(".main-panel").scroll(function () {
        if ($(".main-panel").scrollTop() > 79) {
            $(".floatTop").fadeIn(500);
        } else $(".floatTop").hide();
    });
});

var scroll_Inicio = function () {
    $(".main-panel").animate({ scrollTop: 0 }, "slow");
};

function mouseEnter() {
    $(".card-body .panelIndex, .tarjeta").mouseenter(function () {
        var identificador = $(this).attr("id");
        if (identificador !== undefined) {
            identificador = identificador.split("_")[1];
            $(".divButtonOption_" + identificador).fadeIn(300);
        }
    });
    $(".card-body .panelIndex, .tarjeta").mouseleave(function () {
        var identificador = $(this).attr("id");
        if (identificador !== undefined) {
            identificador = identificador.split("_")[1];
            $(".divButtonOption_" + identificador).hide();
        }
    });
}

async function buscadorPaciente(pagination = 1) {
    let object = {
        pagination,
        seccionLoader: "listaPacientes",
        seccionContenido: "_dataPacientes",
        ruta: "/poblacion",
        asegurado: $("#txtSearch_paciente").val().trim(),
    };
    await searchFields(object);
}
var changePaciente = function () {
    $(".divContentData").hide();
    $("#divDatosPaciente").fadeIn(500);
    buscadorPaciente();
    setTimeout(() => {
        $("#txtSearch_paciente").select();
    }, 500);
};

//  ---------------->> "MODAL BUSCADORES"
/*
    Buscadores en programacion LABORATORIO - IMAGENOLOGIA
    
    1 => "ESPECIALIDADES"
    2 => "MÉDICOS"
*/
var openModalBuscador = function (option) {
    modal_OPCION = option;
    var titulo = "";

    if (option == 1) {
        titulo = "ESPECIALIDADES";
        modal_ROUTE = "buscaEspecialidad";
    } else {
        titulo = "MÉDICOS";
        modal_ROUTE = "buscaMedico";
    }

    $("#vModalBuscador").modal();
    $("#vModalBuscador .modal-title").text(titulo);
    $("#txtHiddenData").val("");
    $("#txtBuscador").val("");
    modalBuscador();
};
var modalBuscador = function (e) {
    var buscar = 0;
    if (e != undefined) {
        if (e.which == 13) {
            if ($("#txtBuscador").val() == $("#txtHiddenData").val()) {
                modalSeleccion(indiceSelected);
            } else {
                $("#txtHiddenData").val($("#txtBuscador").val());
                buscar = 1;
            }
        } else tableEvent(e.keyCode);
    } else buscar = 1;

    if (buscar == 1) {
        $(".__modalContent").html("");
        $.ajax({
            url: modal_ROUTE,
            type: "post",
            data: {
                _token: $("#token").val(),
                dato: $("#txtBuscador").val(),
            },
            success: function (dato) {
                $(".__modalContent").html(dato.view1);
                $(".table-hover").perfectScrollbar();
                $("#txtBuscador").focus();
                indiceSelected = -1;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                // alert('error! ');
            },
        });
    }
};
var modalSeleccion = function (id) {
    $("#vModalBuscador").modal("toggle");
    let dato = $("#data" + id).text();
    dato = JSON.parse(dato);

    if (modal_OPCION == 1) {
        // ESPECIALIDADES
        $("#idhcl_cuaderno").val(dato.id);
        $("#hcl_cuaderno").val(dato.nombre);
    } else if (modal_OPCION == 2) {
        // MÉDICOS
        $("#iddatosgenericos").val(dato.id);
        $("#datosgenericos").val(dato.nombres + " " + dato.apellidos);
    } else if (modal_OPCION == 3) {
        // EMPLEADOR
        $("#idhcl_empleador").val(dato.id);
        $("#hcl_empleador").val(dato.nombre);
    }
};
var tableEvent = function (keycode) {
    /*
        38 => arriba
        40 => abajo
    */
    if (keycode == 38 || keycode == 40)
        $("#row" + indiceSelected).removeClass("selected");

    if (keycode == 38) {
        if (indiceSelected == 0) {
            indiceSelected = 1;
        }

        indiceSelected--; // prev

        var alto = $("#row" + indiceSelected).height();
        altoSeleccionado -= alto;
    } else if (keycode == 40) {
        // Ultima fila
        var lastRow = $("#tbodyContent").find("tr:last").attr("id");
        lastRow = lastRow.split("row");
        if (indiceSelected == lastRow[1]) {
            indiceSelected = -1;
            $("#tableContent").scrollTop(0);
        }

        indiceSelected++; // next

        var alto = $("#row" + indiceSelected).height();
        altoSeleccionado += alto;
    }

    if (keycode == 38 || keycode == 40) {
        teclaEnter = 1;
        $("#row" + indiceSelected).addClass("selected");

        if (altoSeleccionado >= 0) {
            var position = $("#row" + indiceSelected).position();
            if (position.top <= 60)
                // up
                $("#tableContent").scrollTop(altoSeleccionado - 12);
            else if (position.top >= $("#tableContent").height())
                // down
                $("#tableContent").scrollTop(altoSeleccionado - 12);
        } else {
            altoSeleccionado = 0;
            $("#tableContent").scrollTop(0);
        }
    }
};
//  ---------------->> FIN "MODAL BUSCADORES"
