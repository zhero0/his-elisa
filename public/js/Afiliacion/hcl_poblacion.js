var url = '';
var scenario = '';
var data;
let d = document,
$btnSearch = d.getElementById("btnSearch");
$( document ).ready(function() {  
    $('#divContenedorDatosPersona').hide();
    if ($('#txtScenario').val() == 'create') 
    {
        scenario = 1;
        url = 'Create';
    }
    if ($('#txtScenario').val() == 'createBeneficiario') 
    { 
        scenario = 2;
    }
    if ($('#txtScenario').val() == 'view' || $('#txtScenario').val() == 'edit') 
    {
        var datoEmpleador = $('#txtListaPoblacion').val();
        var data = JSON.parse(datoEmpleador);
        $('#divContenedorEmpresa').hide();
        $('#divContenedorDatosPersona').show();
        var nomb = data.nombre;
        var nro = data.nro_empleador;
        $('#nro_emp').text('N° EMPLEADOR: '+nro);
        $('#nomb_emp').text('NOMBRE EMPLEADOR: '+nomb);

        if($('#txtGenero').val() == 1) // MASCULINO
            $('#masc').prop('checked', true);
        else // FEMENINO
            $('#fem').prop('checked', true);

        // Carga los datos de Parentesco al combobox
        var data = JSON.parse( $('#txtJsonHcl_afiliacion_parentesco').val() );
        if (data.length > 0) {
            for(var i = 0; i < data.length; i++)
            {
                var arrayParametro = {
                    id: data[i].id,
                    descripcion: data[i].descripcion 
                };
                generarComboParentesco(arrayParametro);
            }
            $('#parentesco').val( $('#txtIdparentesco').val() );
        }
        else
            $('#parentesco').append('<option>Seleccionar::</option>');
    }

});
var i = 0;

$('#txtBuscadorEmpleador').keypress(function(e) {
    if(e.which == 13) {  
        $('#btnBuscadorEmpleador').click();
    }
});

$('#btnBuscadorEmpleador').click(function() {
    $.ajax({
        url: 'buscaEmpleador' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorEmpleador').val()
        },
        success: function(dato) {            
            $('#tablaEmpleador').empty(); 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if (data.length==0) {
                  $('#tablaEmpleador').append(
                      '<tr id="idListaVacia">' + 
                          '<td>Lista Vacia...</td>' +
                      '</tr>'
                  );                
            }
            for(var i = 0; i < data.length; i++)
            {
                var arrayParametro = {
                    id: data[i].id,
                    nombre: data[i].nombre,
                    nro_empleador: data[i].nro_empleador
                };
                generarTuplaEmpresa(arrayParametro);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            bootbox.alert('Por favor actualize la página! ');
        }
    });
});
var generarTuplaEmpresa = function(arrayParametro)
{
    var botonSeleccionar = '';
    var id = arrayParametro['id'];
    var nro_empleador = arrayParametro['nro_empleador'];
    var nombre = arrayParametro['nombre'];
    
    if($('#txtScenario').val() != 'view')
    { 
        botonSeleccionar = 
            '<td class="col-xs-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="verDetalleEmpresa('+i+', ' + id + ')" style="cursor: pointer;" title="Seleccionar empleador..."></span>'
            +'</td>';
    }

    $('#tablaEmpleador').append(             
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdId' + i + '">' + id + '</td>'
          + '<td id="tdNro' + i + '">' + nro_empleador + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'          
          + botonSeleccionar
       +'</tr>'
    );
    i++;
}


var verDetalleEmpresa = function(id, idtabla)
{   
    $('#txtId_empleador').val(idtabla);
    var nomb = $('#tdNombre' + id).text();
    var nro = $('#tdNro' + id).text();
    var id = $('#tdId' + id).text();

    $('#nro_emp').text('N° EMPLEADOR: '+nro);
    $('#nomb_emp').text('NOMBRE EMPLEADOR: '+nomb);
    $('#idempleador').val(id);
    $('#divContenedorDatosPersona').show();
    $('#divContenedorEmpresa').hide();
}

$('#btnBuscarEmp').click(function() {
    $('#divContenedorEmpresa').show();
    $('#divContenedorDatosPersona').hide();
});

$('#masc').click(function() { 
    if (scenario == 1) 
    {
        ajaxBuscarDatoParentesco(1);
    }
    if (scenario == 2) 
    {
        ajaxBuscarDatoParentesco2(1);
    }    
});

$('#fem').click(function() {  
    if (scenario == 1) 
    {
        ajaxBuscarDatoParentesco(2);
    }
    if (scenario == 2) 
    {
        ajaxBuscarDatoParentesco2(2);
    }
});
var ajaxBuscarDatoParentesco = function(genero) {
    $.ajax({
        url: 'buscaParentesco' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: genero
        },
        success: function(dato) {            
            $('#parentesco').empty(); 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if (data.length==0) {
                  $('#parentesco').append(
                    '<option>Seleccionar::</option>'                      
                  );                
            }
            for(var i = 0; i < data.length; i++)
            {
                var arrayParametro = {
                    id: data[i].id,
                    descripcion: data[i].descripcion 
                };
                generarComboParentesco(arrayParametro);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            bootbox.alert('Por favor actualize la página! ');
        }
    });
}
var ajaxBuscarDatoParentesco2 = function(genero) {
    $.ajax({
        url: 'buscaParentesco' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: genero
        },
        success: function(dato) {            
            $('#parentesco').empty(); 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if (data.length==0) {
                  $('#parentesco').append(
                    '<option>Seleccionar::</option>'                      
                  );                
            }
            for(var i = 0; i < data.length; i++)
            {
                var arrayParametro = {
                    id: data[i].id,
                    descripcion: data[i].descripcion 
                };
                generarComboParentesco(arrayParametro);
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            bootbox.alert('Por favor actualize la página! ');
        }
    });
}

var generarComboParentesco = function(arrayParametro) {
    var id = arrayParametro['id'];
    var descripcion = arrayParametro['descripcion'];  

    $('#parentesco').append(             
        '<option value="'+id+'">'+descripcion+'</option>'                  
    ); 
}

$('#btnGuardarPoblacion').click(function() { 
    // var datosFila = {};
    // var datosTabla = [];
    // datosFila.numero_historia = $("#numero_historia").val();
    // datosFila.primer_apellido = $("#primer_apellido").val();
    // datosFila.segundo_apellido = $("#segundo_apellido").val();
    // datosFila.nombre = $("#nombre").val();
    // datosFila.documento = $("#documento").val(); 
    // if( $("#formulario input[name='optradio']:radio").is(':checked')) {  
    //     datosFila.sexo = '1';
    // }else{  
    //     datosFila.sexo = '2';
    // }   
    // datosFila.fecha_presentacion = $("#fecha_presentacion").val(); 
    // datosFila.parentesco = $("#parentesco").val(); 
    // datosFila.cuaderno = $("#cuaderno").val(); 
    // datosFila.cobertura = $("#cobertura").val(); 
    // datosFila.comentarios = $("#comentarios").val();    
    // datosFila.departamento = $("#departamento").val();     
 
    Swal.fire({
                icon: 'warning',
                title: 'Registro.',
                text: 'Desea guardar la información?', 
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, deseo registrar!'
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Registro completo!', '', 'success') 
                    $("#form").submit();
                }  
            }) 
});

$('#btnGuardarPoblacionBeneficiario').click(function() { 
    // var datosFila = {};
    // var datosTabla = [];
    // datosFila.numero_historia = $("#numero_historia").val();
    // datosFila.primer_apellido = $("#primer_apellido").val();
    // datosFila.segundo_apellido = $("#segundo_apellido").val();
    // datosFila.nombre = $("#nombre").val();
    // datosFila.documento = $("#documento").val(); 
    // if( $("#formulario input[name='optradio']:radio").is(':checked')) {  
    //     datosFila.sexo = '1';
    // }else{  
    //     datosFila.sexo = '2';
    // }   
    // datosFila.fecha_presentacion = $("#fecha_presentacion").val(); 
    // datosFila.parentesco = $("#parentesco").val(); 
    // datosFila.cuaderno = $("#cuaderno").val(); 
    // datosFila.cobertura = $("#cobertura").val(); 
    // datosFila.comentarios = $("#comentarios").val();    
    // datosFila.departamento = $("#departamento").val();     
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {   
            // datosTabla.push(datosFila);
            // var jsonListaPoblacion = JSON.stringify(datosTabla);
            //     $('#txtListaPoblacion').val(jsonListaPoblacion);

            $("#formBeneficiario").submit();
        }
    });
});


function limpiarCampos() {
    let d = document;

    d.getElementsByName("numero_historia")[0].value = "";
    d.getElementsByName("nombre")[0].value = "";
    d.getElementsByName("primer_apellido")[0].value = "";
    d.getElementsByName("segundo_apellido")[0].value = "";
    d.getElementsByName("sexo")[0].value = 0;
    d.getElementsByName("parentesco")[0].value = "";
    d.getElementsByName("domicilio")[0].value = "";
    d.getElementsByName("telefono")[0].value = "";

    // EMPLEADOR
    d.getElementsByName("idempleador")[0].value = "";
    // d.getElementsByName("nomb_emp")[0].value = "";

    // CIUDAD
    d.getElementsByName("id_municipio")[0].value = "";
    // d.getElementsByName("municipio")[0].value = "";

    d.getElementsByName("json_poblacion_erp")[0].value = "";
}
 $btnSearch.addEventListener("click", e => {
    alert('ok');
    let dato,
        param,
        fecha_nacimiento = d.getElementsByName("fecha_nacimiento")[0].value,
        documento = d.getElementsByName("documento")[0].value;

    if(fecha_nacimiento === "" || documento === "") {
        alert("Por favor complete los campos requeridos");
        return;
    }

    param = {
        fecha_nacimiento,
        documento
    }
    d.getElementById("error").style.display = "none";
    d.getElementById("mensaje").style.display = "block";
    
    fetch(`/poblacion/show?param=${ JSON.stringify(param) }`)
        .then(res => res.json())
        .then(dato => {
            d.getElementById("mensaje").style.display = "none";
            data = dato;
            if(data.status === "success") {
                dato = JSON.parse(data.response);
                d.getElementsByName("iderp")[0].value = dato.id;

                d.getElementsByName("numero_registro_sive")[0].value = dato.documentoIdentidad;
                d.getElementsByName("procedencia")[0].value = dato.ubicacion;
                d.getElementsByName("ciudad")[0].value = dato.ubicacion;
                d.getElementsByName("primer_apellido")[0].value = dato.primerApellido;
                d.getElementsByName("segundo_apellido")[0].value = dato.segundoApellido;
                d.getElementsByName("nombre")[0].value = dato.nombres;
                d.getElementsByName("sexo")[0].value = dato.sexo;
                d.getElementsByName("edad")[0].value = Edad(dato.fechaNacimiento);
                d.getElementsByName("nro_empleador")[0].value = dato.nroPatronal;
                
                d.getElementsByName("numero_historia")[0].value = dato.matricula;
                d.getElementsByName("parentesco")[0].value = dato.parentesco;
                d.getElementsByName("tipo_seguro")[0].value = dato.estadoAsegurado;
                d.getElementsByName("razon_empleador")[0].value = dato.razonSocial;
                d.getElementsByName("calle")[0].value = dato.direccion;
            }
            else {
                d.getElementsByName("iderp")[0].value = 0;

                d.getElementsByName("numero_registro_sive")[0].value = '';
                d.getElementsByName("procedencia")[0].value = '';
                d.getElementsByName("ciudad")[0].value = '';
                d.getElementsByName("primer_apellido")[0].value = '';
                d.getElementsByName("segundo_apellido")[0].value = '';
                d.getElementsByName("nombre")[0].value = '';
                d.getElementsByName("sexo")[0].value = 0;
                d.getElementsByName("edad")[0].value = '';
                d.getElementsByName("nro_empleador")[0].value = '';

                d.getElementsByName("numero_historia")[0].value = '';
                d.getElementsByName("parentesco")[0].value = '';
                d.getElementsByName("tipo_seguro")[0].value = '';
                d.getElementsByName("razon_empleador")[0].value = '';
                d.getElementsByName("calle")[0].value = '';
            }
        })
        .catch(err => {
            d.getElementById("mensaje").style.display = "none";
            d.getElementById("error").style.display = "block";
            // console.log(err);
        });
});

function Edad(FechaNacimiento) {
    var fechaNace = new Date(FechaNacimiento);
    var fechaActual = new Date()

    var mes = fechaActual.getMonth();
    var dia = fechaActual.getDate();
    var año = fechaActual.getFullYear();

    fechaActual.setDate(dia);
    fechaActual.setMonth(mes);
    fechaActual.setFullYear(año);

    edad = Math.floor(((fechaActual - fechaNace) / (1000 * 60 * 60 * 24) / 365));  
    return edad;
}