var url = '';
var modal_ROUTE = '';
var moduloURL = 'med_programacion';
var IDHCL_POBLACION = 0;
var opcANTERIOR = -1;

$(document).ready(function() {    
    if($('#txtScenario').val() == 'index')
    {
        // panel del buscador flotante
        $('.main-panel').scroll(function() {
            if ($('.main-panel').scrollTop() > 79) {
                $("#indexSearch").attr('class', 'row panelIndex panelbuscadorFlotante');
            }
            else
                $("#indexSearch").attr('class', 'row panelIndex');
        });
    } 

    if($('#txtScenario').val() == 'edit' || $('#txtScenario').val() == 'view')
    {
        IDHCL_POBLACION = $('#idhcl_poblacion').val();
        verComplementario(0, '_cDatos');
   
        // Deshabilito los input si viene desde CONSULTA EXTERNA
        if($('#idhcl_cabecera_registro').val() > 0)
        {
            var divDeshabilitar = '#divPaciente, #divHospitalizado, #divEspecialidad, #divMedico, #link_examenes';
            $(divDeshabilitar).css({
                'pointer-events': 'none', opacity: '0.8'
            });
        }
    }
    
    if($('#txtScenario').val() == 'view')
    {
        $("#form :input").prop("disabled", true);
    }
    
    $(document).keydown(function(tecla) {
        if(tecla.which == 120) { // F9 => GUARDAR
            $('#btnGuardar').click();
        }
    });
});



//  ---------------->> "PACIENTE"
var seleccionaPaciente = function(id) { 
    IDHCL_POBLACION = id;
    $('#divDatosPaciente').hide();
    $('.divContentData').fadeIn(500);

    var arrayDato = {
        idalmacen: $('#idalmacen').val(),
        idhcl_poblacion: id,
    };
    $.ajax({
        url: 'dataPaciente',
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: arrayDato
        },
        success: function(dato) {
            $('._dataAsegurado').html(dato.view1);
            
            verComplementario(0, '_cDatos');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // bootbox.alert('error! ');
        }
    });
}
//  ---------------->> FIN "PACIENTE"



//  ---------------->> "COMPLEMENTARIOS"
var verComplementario = function(opcion, IDvista) {
    if(opcANTERIOR != opcion)
    {
        __verComplementario(opcion, IDvista);
        if($('#txtScenario').val() == 'create')
        {
            if(opcion == 1) { // EXÁMENES Ima.
                searchExamenIma();
            }
        }

        setTimeout(function() {
            $('.divLoaderContent').hide();
            $('#' + IDvista).show();
            activeOptions(opcion);
        }, 300);
    }
}
//  ---------------->> FIN "COMPLEMENTARIOS"


// ========================================= [ REPORTE ] =========================================
var imprimirSolicitudPro = function(id) { 
    $('#vImprimeSolicitud').modal();
    $('.progress').show();
    
    var parametro = {
        opcion: 2,
        id: id,
    };
    $('#imprimeSolicitud').html("").append($("<iframe></iframe>", {
        src: moduloURL + '/imprimeSolicitud_Salud/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
//  ---------------->> FIN "REPORTE"
// ===============================================================================================



// ==================================== [ GUARDAR ] ====================================
var validate_Datos = function(opcion) {
    var textOption = ($('#textOption_' + opcion).text()).trim();
    var idhcl_especialidad = $('#idhcl_especialidad').val();
    
    if(idhcl_especialidad == null) {
        $('#idhcl_especialidad').addClass('validate');
        $('#mensajeValidacion').text(textOption + ': Complete los datos obligatorios (*) ');
        $('#mensajeValidacion').show();
        return 1;
    }
    return 0;
}
var validate_Examenes = function(opcion) {
    var textOption = ($('#textOption_' + opcion).text()).trim();
    if(datosTablaIma.length > 0) {
        datosTablaIma = Array.from(new Set(datosTablaIma));
        var jsonExamenes = JSON.stringify(datosTablaIma);
        $('#examenes_IMA').val(jsonExamenes);
        return 0;
    }
    else {
        $('#mensajeValidacion').text(textOption + ': Por Favor Seleccione algún Exámen. ');
        $('#mensajeValidacion').show();
        return 1;
    }
} 
// =====================================================================================

var seleccionarClasificacion = function() {
    var idmed_clasificacion = $('#idmed_clasificacion').val();
 
    if (idmed_clasificacion == 1 || idmed_clasificacion == 2) {
        
        $.ajax({
            url: 'buscaClasificacion' + url,
            type: 'post',
            data: {
                _token: $('#token').val(),
                dato: idmed_clasificacion,
            },
            success: function(dato) { 
                var JSONString = dato;
                var data = JSON.parse(JSONString);
                $('#txtCosto_tramite').val(''); 
                
                if(data.length > 0)
                { 
                    for(var k = 0; k < data.length; k++)
                    {
                        $('#txtCosto_formulario').val(3); 
                        $('#txtCosto_tramite').val(data[k].costo_tramite); 
                        $('#divPanelDatos').show();
                    }
                    // $('#txtBuscador_Formulario').focus();
                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('error! ');
            }
        });            
    }else{
        $('#txtCosto_tramite').val('');
        $('#divPanelDatos').hide();
    }
    
   // $('#btnEliminar').hide();
}



// ---------------->> "MODAL BUSCADORES"
/*
    1 => "CONSULTORIO"
    2 => "CIUDAD"
    3 => "EMPLEADOR"
*/
var abrirVentanaMed = function(option, titulo, ruta) { 
    modal_OPCION = option;
    modal_ROUTE = ruta;

    $('.__modalContent').html('');
    $('#vModalBuscadorMed').modal();
    $('#vModalBuscadorMed .modal-title').text(titulo);
    $('#txtHiddenData').val('');
    $('#txtBuscador').val('');
    searchVentanaProg();
}
var searchVentanaProg = function(e) {
    var buscar = 0;
    if(e != undefined) {
        if(e.which == 13) {
            if( $('#txtBuscador').val() == $('#txtHiddenData').val() ) {
                modalSeleccion(indiceSelected);
            }
            else {
                $('#txtHiddenData').val( $('#txtBuscador').val() );
                buscar = 1;
            }            
        }
        else
            tableEvent(e.keyCode);
    }
    else
        buscar = 1;

    if(buscar == 1) {
        $('.__modalContent').addClass('loadingContent');
        $.ajax({
            url: modal_ROUTE,
            type: 'post',
            data: {
                _token: $('#token').val(),
                dato: $('#txtBuscador').val(),
            },
            success: function(dato) {
                $('.__modalContent').removeClass('loadingContent');
                $('.__modalContent').html(dato.view1);
                $('.table-hover').perfectScrollbar();
                $('#txtBuscador').focus();
                indiceSelected = -1;
            },
            error: function(xhr, ajaxOptions, thrownError) {
                // alert('error! ');
            }
        });
    }
}
var modalSeleccion = function(id) {
    $('#vModalBuscadorMed').modal('toggle');
    var dato = $('#data' + id).text();
    dato = JSON.parse(dato);
    if(modal_OPCION == 1) { // ESPECIALIDADES
        $('#idmed_registro').val(dato.id);
        $('#poblacion').val(dato.hcl_poblacion_primer_apellido + ' ' + dato.hcl_poblacion_segundo_apellido + ' ' + dato.hcl_poblacion_nombres);        
        $('#empleador').val(dato.hcl_empleador_nombre);  
    } 
}
// ---------------->> FIN "MODAL BUSCADORES"

var guardar = function() {
    var idmed_registro = $('#idmed_registro').val();
    var fecha_registro = $('#fecha_registro').val(); 
    var hora_registro = $('#hora_registro').val(); 
 
    if(idmed_registro != null  && fecha_registro != '' && hora_registro != '')
    {
        bootbox.confirm('Desea guardar la información? ', function (confirmed) {
            if(confirmed) { 
                $("#form").attr('onsubmit', '');
                $("#form").submit();
            }
        });
    }
} 