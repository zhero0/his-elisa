var url = '';
var i = 0;
var modal_ROUTE = '';
var moduloURL = 'med_registro';
var IDHCL_POBLACION = 0;
var opcANTERIOR = -1;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var idmed_tipo_cancelacion = 0;
var totalDatosCseleccion = 0;
var totalDatosDseleccion = 0;
var datosC = [];
var datosD = [];

$(document).ready(function() {    
    if($('#txtScenario').val() == 'index')
    { 
        
    }
        
    if($('#txtScenario').val() == 'create')
    {
        url = 'Create';
        // setTimeout(function() {            
        //     buscadorPaciente();
        // }, 500);
    }

    if($('#txtScenario').val() == 'edit' || $('#txtScenario').val() == 'view')
    {
         
    }
    
    if($('#txtScenario').val() == 'view')
    {
        $("#form :input").prop("disabled", true);
    }
    
    // $(document).keydown(function(tecla) {
    //     if(tecla.which == 120) { // F9 => GUARDAR
    //         $('#btnGuardar').click();
    //     }
    // });
});
 
// ---------------->> "MODAL BUSCADORES"
/*
    1 => "CONSULTORIO"
    2 => "CIUDAD"
    3 => "EMPLEADOR"
*/
var abrirVentanaMed = function(option, titulo, ruta) { 
    modal_OPCION = option;
    modal_ROUTE = ruta;

    if (modal_OPCION == 1) {
        $('#vModalBuscador_poblacion').modal();
        $('#vModalBuscador_poblacion .modal-title').text(titulo);
    }
    if (modal_OPCION == 2) {
        $('#vModalBuscador_empleador').modal();
        $('#vModalBuscador_empleador .modal-title').text(titulo);
    }
    if (modal_OPCION == 3) {
        $('#vModalBuscador_actividad').modal();
        $('#vModalBuscador_actividad .modal-title').text(titulo);
    }
    // $('#txtHiddenData').val('');
    // $('#txtBuscador').val(''); 
    // searchVentanaMed();
} 
// tabla poblacion
$('#txtBuscadorPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    } 
});
$('#btnBuscarPaciente').click(function() { 
    $.ajax({
        url: 'buscaPaciente_hsi' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorPaciente').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyPaciente').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].res_idhcl_poblacion,
                        numero_historia: data[i].res_numero_historia,
                        nombre: data[i].res_nombre,
                        documento: data[i].res_documento,
                        primer_apellido: data[i].res_primer_apellido,
                        segundo_apellido: data[i].res_segundo_apellido,
                        fecha_nacimiento: data[i].res_fecha_nacimiento,
                        sexo: data[i].res_sexo,
                        edad: data[i].res_edad,
                        idhcl_empleador: data[i].res_idhcl_empleador,
                        nro_empleador: data[i].res_nro_empleador,
                        nombre_empleador: data[i].res_nombre_empleador
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                $('#txtBuscadorPaciente').focus();
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}); 

var generarTuplaPaciente = function(arrayParametro) {
    var id = arrayParametro['id'];
    var numero_historia = arrayParametro['numero_historia'];
    var documento = arrayParametro['documento'];
    var nombre = arrayParametro['nombre'];
    var primer_apellido = arrayParametro['primer_apellido'];
    var segundo_apellido = arrayParametro['segundo_apellido'];

    var fecha_nacimiento = arrayParametro['fecha_nacimiento'];
    var sexo = arrayParametro['sexo'];
    var edad = arrayParametro['edad'];

    var idhcl_empleador = arrayParametro['idhcl_empleador'];
    var nro_empleador = arrayParametro['nro_empleador'];
    var nombre_empleador = arrayParametro['nombre_empleador'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClickPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdDocumento' + i + '">' + documento + '</td>' 
          + '<td id="tdApellidos' + i + '">' + primer_apellido + ' ' + segundo_apellido + '</td>'
            + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>'
            + '<td style="display: none;" id="tdfecha_nacimiento' + i + '">' + fecha_nacimiento + '</td>'
            + '<td style="display: none;" id="tdsexo' + i + '">' + sexo + '</td>'
            + '<td style="display: none;" id="tdedad' + i + '">' + edad + '</td>'
            + '<td style="display: none;" id="tdidhcl_empleador' + i + '">' + idhcl_empleador + '</td>'
            + '<td style="display: none;"id="tdNro_empleador' + i + '">' + nro_empleador + '</td>' 
            + '<td style="display: none;" id="tdnombre_empleador' + i + '">' + nombre_empleador + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClickPaciente = function(id, idtabla) {
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) {
    $('#vModalBuscador_poblacion').modal('toggle');

    $('#idhcl_poblacion').val($('#tdIdPaciente' + id).text());
    $('#poblacion').val($('#tdApellidos' + id).text() + ' ' + $('#tdNombrePaciente' + id).text()); 
    // $('#txtidhcl_poblacion').val( $('#tdIdPaciente' + id).text() );
    $('#res_numero_historia').val( $('#tdNumeroHistoria' + id).text() );
    $('#res_fecha_nacimiento').val( $('#tdfecha_nacimiento' + id).text() );
    $('#res_edad').val( $('#tdedad' + id).text() );
    $('#res_sexo').val( $('#tdsexo' + id).text() == '1'? 'MASCULINO':'FEMENINO' );
    $('#sexo').val( $('#tdsexo' + id).text() );
    $('#txtBuscadorPaciente').val('');
    $('#idhcl_empleador').val($('#tdidhcl_empleador' + id).text());
    $('#empleador').val($('#tdnombre_empleador' + id).text()); 
    $('#res_nro_empleador').val($('#tdNro_empleador' + id).text()); 
}
// hasta aqui
// tabla empleador
$('#txtBuscadorEmpleador').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarEmpleador').click();
    } 
});
$('#btnBuscarEmpleador').click(function() { 
    $.ajax({
        url: 'buscaEmpleador' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorEmpleador').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyEmpleador').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        nro_empleador: data[i].nro_empleador,
                        nombre_empleador: data[i].nombre 
                    };
                    generarTuplaEmpleador(arrayParametro);
                }
                $('#txtBuscadorEmpleador').focus();
            }
            else
                $('#tbodyEmpleador').append(
                  '<tr id="idListaVaciaEmpleador">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}); 

var generarTuplaEmpleador = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nro_empleador = arrayParametro['nro_empleador']; 
    var nombre_empleador = arrayParametro['nombre_empleador']; 
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaEmpleador('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyEmpleador').append(
       '<tr ondblclick="eventoDobleClick('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdidhcl_empleador' + i + '">' + id + '</td>'
          + '<td id="tdNro_empleador' + i + '">' + nro_empleador + '</td>' 
            + '<td id="tdnombre_empleador' + i + '">' + nombre_empleador + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick = function(id, idtabla) {
    seleccionaEmpleador(id, idtabla);
}
var seleccionaEmpleador = function(id, idtabla) {
    $('#vModalBuscador_empleador').modal('toggle'); 
    $('#idhcl_empleador').val($('#tdidhcl_empleador' + id).text());
    $('#empleador').val($('#tdnombre_empleador' + id).text()); 
    $('#res_nro_empleador').val($('#tdNro_empleador' + id).text()); 

    $('#txtBuscadorEmpleador').val('');
} 

var guardar = function() { 
    var idmed_clasificacion = $('#idmed_clasificacion').val();
    var fecha_registro = $('#idfecha_registro').val(); 
 
    if(idmed_tipo_cancelacion != null  && idmed_clasificacion != null  && fecha_registro != '' )
    { 
        $("#form").attr('onsubmit', '');
        $("#form").submit(); 
    }
} 


var seleccionarClasificacion = function() {
    var idhsi_actividad = $('#idhsi_actividad_economica_agrupacion').val(); 
    // if( $('#radioSi').is(':checked') )
    // {
    //     alert( $('#radioSi').val());
    // }
    // if( $('#radioNo').is(':checked') )
    // {
    //     alert( $('#radioNo').val());
    // }

    if (idhsi_actividad > 0) { 
        $.ajax({
            url: 'buscaHsi_division' + url,
            type: 'post',
            data: {
                _token: $('#token').val(),
                dato: idhsi_actividad,
            },
            success: function(dato) { 
                var JSONString = dato;
                var data = JSON.parse(JSONString);  
                $('#tbodyActividad').empty(); 
                if(data.length > 0)
                { 
                    for(var i = 0; i < data.length; i++)
                    { 
                        var arrayParametro = {
                        id: data[i].id,
                        idhsi_agrupacion: data[i].idhsi_agrupacion,
                        idhsi_division: data[i].idhsi_division,
                        datoAgrupacion: data[i].codificacion+' - '+data[i].nombre, 
                        datoActividad: data[i].res_codificacion+' - '+data[i].res_nombre
                        };
                        generarTuplaActividad(arrayParametro);
                    }  
                } 
                else
                    $('#tbodyActividad').append(
                      '<tr id="idListaVaciaActividad">' + 
                          '<td>Lista Vacía...</td>' +
                      '</tr>'
                    );
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('error! ');
            }
        });   
    }     
} 

var generarTuplaActividad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var idhsi_agrupacion = arrayParametro['idhsi_agrupacion'];
    var idhsi_division = arrayParametro['idhsi_division'];
    var datoAgrupacion = arrayParametro['datoAgrupacion'];
    var datoActividad = arrayParametro['datoActividad']; 
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaActividad('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyActividad').append(
       '<tr ondblclick="eventoDobleClickActividad('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdAgrupacion' + i + '">' + idhsi_agrupacion + '</td>'
          + '<td style="display: none;" id="tdActividad' + i + '">' + idhsi_division + '</td>'
          + '<td style="display: none;" id="tddatoActividad' + i + '">' + datoActividad + '</td>' 
          + '<td id="tddatoAgrupacion' + i + '">' + datoAgrupacion + '</td>'             
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClickActividad = function(id, idtabla) {
    seleccionaActividad(id, idtabla);
}
var seleccionaActividad = function(id, idtabla) {
    $('#vModalBuscador_actividad').modal('toggle');

    $('#idhsi_actividad_economica_division').val($('#tdAgrupacion' + id).text());
    $('#idHsi_actividad_economica_agrupacion').val($('#tdActividad' + id).text());
    $('#datoActividad').val($('#tddatoActividad' + id).text()); 
    $('#datoAgrupacion').val( $('#tddatoAgrupacion' + id).text() ); 
}


// ======================================= [ "Opcion C" ] =======================================
$('#txtBuscarC').keypress(function(e) {
    if(e.which == 13)
        $('#btnBuscarC').click();
});
$('#btnBuscarC').click(function() {
    ajaxMuestraDatosC(1, $('#btnBuscarC').val());
    $('#txtBuscarC').focus();
    // alert($('#btnBuscarC').val());
});
var ajaxMuestraDatosC = function(buscarTodo, buscarOpcion) {    
    $.ajax({
        url: 'muestraDatosHsi' + url,
        type: 'post',
        data: {
            _token: $('#token').val(), 
            buscarTodo: buscarTodo,
            buscarOpcion: buscarOpcion,
            datos: JSON.stringify(datosC),
            dato: $('#txtBuscarC').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyC').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {
                    var arrayParametro = {
                        id: data[j].id,
                        codigo: data[j].codigo,
                        descripcion: data[j].nombre,
                    };
                    generarListaC(arrayParametro);
                }
                seleccionarC_Nuevamente();
            }
            else
                $('#tbodyC').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaC = function(arrayParametro) {
    var id = arrayParametro['id'];
    // var codigo = arrayParametro['codigo'];
    var descripcion = arrayParametro['descripcion']+' ('+arrayParametro['codigo']+')';
    
    // if (arrayParametro['band'] == 1) {
    //     var checkbox = '<div class="form-check">'
    //                 +   '<input type="checkbox" class="form-check-input" id="checkSeleccionarCie' + i + '" checked>'
    //                 +   ' <label class="form-check-label" for="checkSeleccionarCie' + i + '" style="cursor: pointer;">'
    //                 +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
    //                 +   ' </label>'
    //              +  '</div>';
    //              diagnosticoSeleccionados();
    // }else
    var checkbox = '<div class="form-check">'
                +   '<input type="checkbox" class="form-check-input" id="checkSeleccionarC' + i + '">'
                +   ' <label class="form-check-label" for="checkSeleccionarC' + i + '" style="cursor: pointer;">'
                +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                +   ' </label>'
             +  '</div>';
    
    $('#tbodyC').append(
       '<tr id="row-' + i + '">' + 
            '<td style="display: none;" id="tdId_C' + i + '">' + id + '</td>' +
            '<td id="tdDescripcion' + i + '">' + descripcion + '</td>' + 
            '<td class="col-xs-1">' + checkbox + '</td>' + 
       '</tr>'
    );
    
    $('#checkSeleccionarC' + i).click(function() {
        datosC_Seleccionados();

        if($(this).is(':checked'))
            totalDatosCseleccion++;
        else
            totalDatosCseleccion--;

        $('#spanTotalC').text(totalDatosCseleccion);
        $('#txtBuscarC').focus();
    });
    
    i++;
}
var seleccionarC_Nuevamente = function() {
    var arrayDatosTabla = datosC;
    
    arrayDatosTabla = Array.from(new Set(arrayDatosTabla));
    if(arrayDatosTabla.length > 0)
    {
        for(i = 0; i < arrayDatosTabla.length; i++)
        {
            $("#tablaContenedorC tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                if($.isNumeric(identificador))
                {
                    if($('#tdId_C' + identificador).text() == arrayDatosTabla[i])
                    {
                        $('#checkSeleccionarC' + identificador).prop('checked', true);
                        return;
                    }
                }
            });
        }
    }
}
var datosC_Seleccionados = function() {
    $("#tablaContenedorC tbody tr").each(function (index)
    {
        var identificador = ($(this).attr('id')).split('-')[1];
        if($.isNumeric(identificador))
        {
            var Idhcl_tipo = $('#tdId_C' + identificador).text();
            if($('#checkSeleccionarC' + identificador).is(':checked'))
                datosC.push(Idhcl_tipo);
            else
            {
                datosC = jQuery.grep(datosC, function(value) {
                  return value != Idhcl_tipo;
                });
            }
        }
    });
    datosC = Array.from(new Set(datosC));
}
$('#btnVisualizaC').click(function() {
    ajaxMuestraDatosC(0, $('#btnBuscarC').val()); 
    $('#txtBuscarC').val('');
    $('#txtBuscarC').focus();
});



// ======================================= [ "Opcion D" ] =======================================
$('#txtBuscarD').keypress(function(e) {
    if(e.which == 13)
        $('#btnBuscarD').click();
});
$('#btnBuscarD').click(function() {
    ajaxMuestraDatosD(1, $('#btnBuscarD').val());
    $('#txtBuscarD').focus(); 
});
var ajaxMuestraDatosD = function(buscarTodo, buscarOpcion) {    
    $.ajax({
        url: 'muestraDatosHsi' + url,
        type: 'post',
        data: {
            _token: $('#token').val(), 
            buscarTodo: buscarTodo,
            buscarOpcion: buscarOpcion,
            datos: JSON.stringify(datosD),
            dato: $('#txtBuscarD').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyD').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {
                    var arrayParametro = {
                        id: data[j].id,
                        codigo: data[j].codigo,
                        descripcion: data[j].nombre,
                    };
                    generarListaD(arrayParametro);
                }
                seleccionarD_Nuevamente();
            }
            else
                $('#tbodyD').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaD = function(arrayParametro) {
    var id = arrayParametro['id'];
    // var codigo = arrayParametro['codigo'];
    var descripcion = arrayParametro['descripcion']+' ('+arrayParametro['codigo']+')';
    
    // if (arrayParametro['band'] == 1) {
    //     var checkbox = '<div class="form-check">'
    //                 +   '<input type="checkbox" class="form-check-input" id="checkSeleccionarCie' + i + '" checked>'
    //                 +   ' <label class="form-check-label" for="checkSeleccionarCie' + i + '" style="cursor: pointer;">'
    //                 +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
    //                 +   ' </label>'
    //              +  '</div>';
    //              diagnosticoSeleccionados();
    // }else
    var checkbox = '<div class="form-check">'
                +   '<input type="checkbox" class="form-check-input" id="checkSeleccionarD' + i + '">'
                +   ' <label class="form-check-label" for="checkSeleccionarD' + i + '" style="cursor: pointer;">'
                +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                +   ' </label>'
             +  '</div>';
    
    $('#tbodyD').append(
       '<tr id="row-' + i + '">' + 
            '<td style="display: none;" id="tdId_D' + i + '">' + id + '</td>' +
            '<td id="tdDescripcion' + i + '">' + descripcion + '</td>' + 
            '<td class="col-xs-1">' + checkbox + '</td>' + 
       '</tr>'
    );
    
    $('#checkSeleccionarD' + i).click(function() {
        datosD_Seleccionados();

        if($(this).is(':checked'))
            totalDatosDseleccion++;
        else
            totalDatosDseleccion--;

        $('#spanTotalD').text(totalDatosDseleccion);
        $('#txtBuscarD').focus();
    });
    
    i++;
}
var seleccionarD_Nuevamente = function() {
    var arrayDatosTabla = datosD;
    
    arrayDatosTabla = Array.from(new Set(arrayDatosTabla));
    if(arrayDatosTabla.length > 0)
    {
        for(i = 0; i < arrayDatosTabla.length; i++)
        {
            $("#tablaContenedorD tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                if($.isNumeric(identificador))
                {
                    if($('#tdId_D' + identificador).text() == arrayDatosTabla[i])
                    {
                        $('#checkSeleccionarD' + identificador).prop('checked', true);
                        return;
                    }
                }
            });
        }
    }
}
var datosD_Seleccionados = function() {
    $("#tablaContenedorD tbody tr").each(function (index)
    {
        var identificador = ($(this).attr('id')).split('-')[1];
        if($.isNumeric(identificador))
        {
            var Idhcl_tipoD = $('#tdId_D' + identificador).text();
            if($('#checkSeleccionarD' + identificador).is(':checked'))
                datosD.push(Idhcl_tipoD);
            else
            {
                datosD = jQuery.grep(datosD, function(value) {
                  return value != Idhcl_tipoD;
                });
            }
        }
    });
    datosD = Array.from(new Set(datosD));
}
$('#btnVisualizaD').click(function() {
    ajaxMuestraDatosD(0, $('#btnBuscarD').val()); 
    $('#txtBuscarD').val('');
    $('#txtBuscarD').focus();
});
