var url = '';
var i = 0;
var modal_ROUTE = '';
var moduloURL = 'med_registro';
var IDHCL_POBLACION = 0;
var opcANTERIOR = -1;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var idmed_tipo_cancelacion = 0;

$(document).ready(function() {    
    if($('#txtScenario').val() == 'index')
    { 
        
    }
        
    if($('#txtScenario').val() == 'create')
    {
        url = 'Create';
        // setTimeout(function() {            
        //     buscadorPaciente();
        // }, 500);
    }

    if($('#txtScenario').val() == 'edit' || $('#txtScenario').val() == 'view')
    {
         
    }
    
    if($('#txtScenario').val() == 'view')
    {
        $("#form :input").prop("disabled", true);
    }
    
    // $(document).keydown(function(tecla) {
    //     if(tecla.which == 120) { // F9 => GUARDAR
    //         $('#btnGuardar').click();
    //     }
    // });
});



//  ---------------->> "PACIENTE"
var seleccionaPaciente = function(id) {
    alert('aqui entra');
    IDHCL_POBLACION = id;
    $('#divDatosPaciente').hide();
    $('.divContentData').fadeIn(500);

    var arrayDato = {
        idalmacen: $('#idalmacen').val(),
        idhcl_poblacion: id,
    };
    $.ajax({
        url: 'dataPaciente',
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: arrayDato
        },
        success: function(dato) {
            $('._dataAsegurado').html(dato.view1);
            
            verComplementario(0, '_cDatos');
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // bootbox.alert('error! ');
        }
    });
}
//  ---------------->> FIN "PACIENTE"



// //  ---------------->> "COMPLEMENTARIOS"
// var verComplementario = function(opcion, IDvista) {
//     if(opcANTERIOR != opcion)
//     {
//         __verComplementario(opcion, IDvista);
//         if($('#txtScenario').val() == 'create')
//         {
//             if(opcion == 1) { // EXÁMENES Ima.
//                 searchExamenIma();
//             }
//         }

//         setTimeout(function() {
//             $('.divLoaderContent').hide();
//             $('#' + IDvista).show();
//             activeOptions(opcion);
//         }, 300);
//     }
// }
// //  ---------------->> FIN "COMPLEMENTARIOS"


// ========================================= [ REPORTE ] =========================================
var imprimirSolicitud = function(id) { 
    $('#vImprimeSolicitud').modal();
    $('.progress').show();
    
    var parametro = {
        opcion: 1,
        id: id,
    };
    $('#imprimeSolicitud').html("").append($("<iframe></iframe>", {
        src: moduloURL + '/imprimeSolicitud_Salud/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
//  ---------------->> FIN "REPORTE"
// ===============================================================================================



// ==================================== [ GUARDAR ] ====================================
var validate_Datos = function(opcion) {
    var textOption = ($('#textOption_' + opcion).text()).trim();
    var idhcl_especialidad = $('#idhcl_especialidad').val();
    
    if(idhcl_especialidad == null) {
        $('#idhcl_especialidad').addClass('validate');
        $('#mensajeValidacion').text(textOption + ': Complete los datos obligatorios (*) ');
        $('#mensajeValidacion').show();
        return 1;
    }
    return 0;
}
var validate_Examenes = function(opcion) {
    var textOption = ($('#textOption_' + opcion).text()).trim();
    if(datosTablaIma.length > 0) {
        datosTablaIma = Array.from(new Set(datosTablaIma));
        var jsonExamenes = JSON.stringify(datosTablaIma);
        $('#examenes_IMA').val(jsonExamenes);
        return 0;
    }
    else {
        $('#mensajeValidacion').text(textOption + ': Por Favor Seleccione algún Exámen. ');
        $('#mensajeValidacion').show();
        return 1;
    }
}
// var guardar = function() {
//     $('#idhcl_poblacion').val(IDHCL_POBLACION);
    
//     // ---------->> (  DATOS )
//     var opcion = $('.complementario .dropdown-menu p:first').attr('id');
//     opcion = opcion.split('_')[1];
//     var result = validate_Datos(opcion);
    
//     if(result == 0) {
//         $('.complementario .dropdown-menu input').each(function() {
//             var identificador = $(this).attr('id');
//             if( $('#' + identificador).is(':checked') )
//             {
//                 var opcion = identificador.split('_')[1];
                
//                 if(opcion == 1) // ---------->> ( EXÁMENES )
//                     result = validate_Examenes(opcion);
                
//                 // Si existe alguna opción por validar FINALIZA aquí.
//                 if(result == 1) {
//                     return false;
//                 }
//             }
//         });
//     }
//     if(result == 0)
//     {
//         bootbox.confirm('Desea guardar la información? ', function (confirmed) {
//             if(confirmed) {
//                 $("#form").attr('onsubmit', '');
//                 $("#form").submit();
//             }
//         });
//     }
// }
// =====================================================================================

var seleccionarClasificacion = function() {
    var idmed_clasificacion = $('#idmed_clasificacion').val();
 
    if (idmed_clasificacion == 1 || idmed_clasificacion == 2) {
        
        $.ajax({
            url: 'buscaClasificacion' + url,
            type: 'post',
            data: {
                _token: $('#token').val(),
                dato: idmed_clasificacion,
            },
            success: function(dato) { 
                var JSONString = dato;
                var data = JSON.parse(JSONString);
                $('#txtCosto_tramite').val(''); 
                
                if(data.length > 0)
                { 
                    for(var k = 0; k < data.length; k++)
                    {
                        $('#txtCosto_formulario').val(3); 
                        $('#txtCosto_tramite').val(data[k].costo_tramite); 
                        $('#divPanelDatos').show();
                    }
                    // $('#txtBuscador_Formulario').focus();
                } 
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('error! ');
            }
        });            
    }else{
        $('#txtCosto_tramite').val('');
        $('#divPanelDatos').hide();
    }
    
   // $('#btnEliminar').hide();
} 

var habilitaRegistro = function() {
    idmed_tipo_cancelacion = $('#idmed_tipo_cancelacion').val();
    var band = false;
    if (idmed_tipo_cancelacion == 2 || idmed_tipo_cancelacion == 3) {
        band = false; 
    }else{
        band = true; 
    }
    $('#txtnro_deposito').attr("readonly", band);
    $('#txtbanco_deposito').attr("readonly", band);
    $('#txtfecha_deposito').attr("readonly", band);
    $('#txtidentificacion').attr("readonly", band);
}

// ---------------->> "MODAL BUSCADORES"
/*
    1 => "CONSULTORIO"
    2 => "CIUDAD"
    3 => "EMPLEADOR"
*/
var abrirVentanaMed = function(option, titulo, ruta) { 
    modal_OPCION = option;
    modal_ROUTE = ruta;

    if (modal_OPCION == 1) {
        $('#vModalBuscador_poblacion').modal();
        $('#vModalBuscador_poblacion .modal-title').text(titulo);
    }
    if (modal_OPCION == 3) {
        $('#vModalBuscador_empleador').modal();
        $('#vModalBuscador_empleador .modal-title').text(titulo);
    }
    
    // $('#txtHiddenData').val('');
    // $('#txtBuscador').val(''); 
    // searchVentanaMed();
} 
// tabla poblacion
$('#txtBuscadorPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    } 
});
$('#btnBuscarPaciente').click(function() { 
    $.ajax({
        url: 'buscaPaciente_solicitud' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorPaciente').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyPaciente').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        numero_historia: data[i].numero_historia,
                        nombre: data[i].nombre,
                        documento: data[i].documento,
                        primer_apellido: data[i].primer_apellido,
                        segundo_apellido: data[i].segundo_apellido,
                        idempleador: data[i].idempleador,
                        nro_empleador: data[i].nro_empleador,
                        empresa: data[i].empresa
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                $('#txtBuscadorPaciente').focus();
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}); 

var generarTuplaPaciente = function(arrayParametro) {
    var id = arrayParametro['id'];
    var numero_historia = arrayParametro['numero_historia'];
    var documento = arrayParametro['documento'];
    var nombre = arrayParametro['nombre'];
    var primer_apellido = arrayParametro['primer_apellido'];
    var segundo_apellido = arrayParametro['segundo_apellido'];
    var idempleador = arrayParametro['idempleador'];
    var nro_empleador = arrayParametro['nro_empleador'];
    var empresa = arrayParametro['empresa'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClickPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdempleador' + i + '">' + idempleador + '</td>'
          + '<td style="display: none;" id="tdNro' + i + '">' + nro_empleador + '</td>'
          + '<td style="display: none;" id="tdEmpresa' + i + '">' + empresa + '</td>'
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdDocumento' + i + '">' + documento + '</td>' 
          + '<td id="tdApellidos' + i + '">' + primer_apellido + ' ' + segundo_apellido + '</td>'
            + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClickPaciente = function(id, idtabla) {
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) {
    $('#vModalBuscador_poblacion').modal('toggle');

    $('#idhcl_poblacion').val($('#tdIdPaciente' + id).text());
    $('#poblacion').val($('#tdApellidos' + id).text() + ' ' + $('#tdNombrePaciente' + id).text());
    
    $('#idhcl_empleador').val($('#tdempleador' + id).text());
    $('#empleador').val($('#tdNro' + id).text() + ' ' + $('#tdEmpresa' + id).text()); 
    // $('#txtidhcl_poblacion').val( $('#tdIdPaciente' + id).text() );
    // $('#datoNumeroPaciente').text( $('#tdNumeroHistoria' + id).text() );
    // $('#datoNombrePaciente').text( $('#tdNombrePaciente' + id).text() );
    // $('#datoApellidosPaciente').text( $('#tdApellidos' + id).text() );

    $('#txtBuscadorPaciente').val('');
}
// hasta aqui
// tabla empleador
$('#txtBuscadorEmpleador').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarEmpleador').click();
    } 
});
$('#btnBuscarEmpleador').click(function() { 
    $.ajax({
        url: 'buscaEmpleador' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorEmpleador').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyEmpleador').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        nro_empleador: data[i].nro_empleador,
                        nombre: data[i].nombre 
                    };
                    generarTuplaEmpleador(arrayParametro);
                }
                $('#txtBuscadorEmpleador').focus();
            }
            else
                $('#tbodyEmpleador').append(
                  '<tr id="idListaVaciaEmpleador">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}); 

var generarTuplaEmpleador = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nro_empleador = arrayParametro['nro_empleador']; 
    var nombre = arrayParametro['nombre']; 
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaEmpleador('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyEmpleador').append(
       '<tr ondblclick="eventoDobleClick('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdEmpleador' + i + '">' + id + '</td>'
          + '<td id="tdNro_empleador' + i + '">' + nro_empleador + '</td>' 
            + '<td id="tdNombreEmpleador' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick = function(id, idtabla) {
    seleccionaEmpleador(id, idtabla);
}
var seleccionaEmpleador = function(id, idtabla) {
    $('#vModalBuscador_empleador').modal('toggle'); 
    $('#idhcl_empleador').val($('#tdIdEmpleador' + id).text());
    $('#empleador').val($('#tdNro_empleador' + id).text() + ' ' + $('#tdNombreEmpleador' + id).text()); 

    $('#txtBuscadorEmpleador').val('');
}
// hasta aqui
// var searchVentanaMed = function(e) { 
//     alert('jaja');
//     // var buscar = 0;
//     // if(e != undefined) {
//     //     if(e.which == 13) {
//     //         if( $('#txtBuscador').val() == $('#txtHiddenData').val() ) {
//     //             modalSeleccion(indiceSelected);
//     //         }
//     //         else {
//     //             $('#txtHiddenData').val( $('#txtBuscador').val() );
//     //             buscar = 1;
//     //         }            
//     //     }
//     //     else
//     //         tableEvent(e.keyCode);
//     // }
//     // else
//     //     buscar = 1; 




//     // if(buscar == 1) {
//     //     $('.__modalContent').addClass('loadingContent');
//     //     $.ajax({
//     //         url: modal_ROUTE,
//     //         type: 'post',
//     //         data: {
//     //             _token: $('#token').val(),
//     //             dato: $('#txtBuscador').val(),
//     //         },
//     //         success: function(dato) {
//     //             $('.__modalContent').removeClass('loadingContent');
//     //             $('.__modalContent').html(dato.view1);
//     //             $('.table-hover').perfectScrollbar();
//     //             $('#txtBuscador').focus();
//     //             indiceSelected = -1;
//     //         },
//     //         error: function(xhr, ajaxOptions, thrownError) {
//     //             // alert('error! ');
//     //         }
//     //     });
//     // }
// }

// var modalSeleccion = function(id) {
//     $('#vModalBuscador').modal('toggle');
//     var dato = $('#data' + id).text();
//     dato = JSON.parse(dato);

//     if(modal_OPCION == 1) { // ESPECIALIDADES
//         $('#idhcl_poblacion').val(dato.id);
//         $('#poblacion').val(dato.primer_apellido + ' ' + dato.segundo_apellido + ' ' + dato.nombre);
//     } 
//     else { // EMPLEADOR
//         $('#idhcl_empleador').val(dato.id);
//         $('#empleador').val(dato.nombre);
//     }
// }
// ---------------->> FIN "MODAL BUSCADORES"

var guardar = function() { 
    var idmed_clasificacion = $('#idmed_clasificacion').val();
    var fecha_registro = $('#idfecha_registro').val(); 
 
    if(idmed_tipo_cancelacion != null  && idmed_clasificacion != null  && fecha_registro != '' )
    { 
        $("#form").attr('onsubmit', '');
        $("#form").submit(); 
    }
} 

var validarRegistro = function() 
{   
    $.ajax({
        url: 'validarRegistro' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            nro_folio: $('#nro_folio').val()
        }, 
        success: function(dato) {  
            $("#btnEliminar").show();        
            $('#btnGuardarValidar').hide();
            $('#btnGuardar').hide();           
            $('#nro_folio').attr("readonly", true);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    }); 
} 

var eliminarRegistro = function() 
{   
    $.ajax({
        url: 'eliminarRegistro' + url,
        type: 'post',
        data: {
            _token: $('#token').val()
        }, 
        success: function(dato) {
            $('#btnGuardarValidar').hide();
            $('#btnEliminar').hide();
            $('#nro_folio').attr("readonly", true);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    }); 
} 