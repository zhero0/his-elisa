var url = '';
var i = 0;
var datosTabla = [];
var datoEnviado = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
$(document).ready(function() { 
    if($('#txtScenario').val() == 'create')
        url = 'Create';
});

var abrirVentana = function(option, titulo, ruta) { 
    modal_OPCION = option;
    modal_ROUTE = ruta;

    if (modal_OPCION == 1) {
        $('#vModalBuscadorMedicos').modal();
        $('#vModalBuscadorMedicos .modal-title').text(titulo);
    }
    if (modal_OPCION == 2) {
        $('#vModalBuscadorEspecialidad').modal();
        $('#vModalBuscadorEspecialidad .modal-title').text(titulo);
    }
    
    // $('#txtHiddenData').val('');
    // $('#txtBuscador').val(''); 
    // searchVentanaMed();
} 




// ========================================== [ DATOS ] ==========================================
// [ Médico Solicitante ] 
$('#txtBuscador_Medico').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Medico').click();
    }
});
$('#btnBuscar_Medico').click(function() {
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Medico').val()
        },
        success: function(dato) { 
            var JSONString = dato; 
            var data = JSON.parse(JSONString);

            $('#tbody_Medico').empty(); 

            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Medico').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr id="rowEspecialidad-'+i+'" ondblclick="eventoDobleClick_Medico('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" class="default">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Medico = function(id, idtabla) {
    seleccionaMedico(id, idtabla);
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscadorMedicos').modal('toggle');
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#poblacion').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Medico').val('');
}
 
// [ Especialidad ] 
$('#txtBuscador_Especialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Especialidad').click();
    }
});
$('#btnBuscar_Especialidad').click(function() {
    $.ajax({
        url: 'buscaEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Especialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscador_Especialidad').focus();
            }
            else
                $('#tbody_Especialidad').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaCuaderno('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr id="rowEspecialidad-'+i+'" ondblclick="eventoDobleClick_Cuaderno('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" class="default">'
          + '<td style="display: none;" id="td_idhcl_cuaderno' + i + '">' + id + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Cuaderno = function(id, idtabla) {
    seleccionaCuaderno(id, idtabla);
}
var seleccionaCuaderno = function(id, idtabla) {
    $('#vModalBuscadorEspecialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_nombre' + id).text() );
    $('#idhcl_cuaderno').val( idtabla );
    $('#txtBuscador_Especialidad').val('');
}
// ===============================================================================================
 
// ========================================= [ GUARDAR ] =========================================
$('#btnGuardar').click( function(e) { 
    $("#form").submit(); 
});
// ===============================================================================================