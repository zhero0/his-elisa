var url = '';
var i = 0;
var datosTabla = [];
var datoEnviado = 0;
$(document).ready(function() { 
    if($('#txtScenario').val() == 'create')
        url = 'Create';
});

//  ---------------->> "asigna cargo"
 
var asignaCargo = function(id) {  
    $("#vModalBuscador").modal({
        backdrop: "static",
        keyboard: true,
    });
    datoEnviado = id;
}


// [ PLANTILLA ]
 
$("#txtBuscador_Persona").keypress(function (e) {
    if (e.which == 13) {
        $("#btnBuscar_Persona").click();
    }
});
$("#btnBuscar_Persona").click(function () {
    // alert($("#txtBuscador_Persona").val());
    $.ajax({
        url: 'med_lista_cargos/buscarPersona',
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Persona').val()
        },
        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medico').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos,
                        matricula: data[k].matricula
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Persona').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscador').modal('toggle');

    // alert(datoEnviado);
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtDatos-'+ datoEnviado).val( nombres );    
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Medico').val('');
    registrarDato(idtabla, datoEnviado);
}

var registrarDato = function(iddatosgenericos, idCargo) {    
    $('#tbodyRegistros').addClass('loadingContent');    
    $.ajax({ 
        url: 'med_lista_cargos/asignaCargo',
        type: 'POST',
        data: {
            _token: $('#token').val(),
            iddatosgenericos: iddatosgenericos,
            idCargo: idCargo
        },
        success: function(dato) {
            // $('#tbodyRegistros').removeClass('loadingContent');
            $('indexContent').html(dato.view1); 
        }, 
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
// [ FIN PLANTILLA ]

//  ---------------->> FIN "asigna"