$( document ).ready(function() {    
});

// ========================================= [ GUARDAR ] =========================================
$('#btnGuardarPassword').click(function() {
    var password = $('#txtNuevoPassword').val();
    var password2 = $('#txtNuevoPassword2').val();
    if(password == password2 && password != '' && password2 != '')
      $("#form").submit();
    else
    {
      if(password != '' || password2 != '')
        $textoMensaje = 'Las Contraseñas son OBLIGATORIAS! ';
      else
        $textoMensaje = 'Las Contraseñas NO Coinciden!';

      $('#textoValidacion').text($textoMensaje);
      $('#textoValidacion').show();
      $('#txtNuevoPassword').select();

      setTimeout(function() {
          $('#textoValidacion').hide();
      }, 5000);
    }
  });
// ======================================= [ FIN GUARDAR ] =======================================