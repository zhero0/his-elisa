var mostrar = 1;
var hora = 0;
var minuto = 0;
$(document).ready(function() {
    $('#divHora_fin').hide();

    var txtHora_Minuto_AmPM = $('#txtHora_Minuto_AmPM_fin').val();
    if(txtHora_Minuto_AmPM != '')
    {
        hora = txtHora_Minuto_AmPM.substr(0, 2);
        minuto = txtHora_Minuto_AmPM.substr(3, 2);
        $('#txtHora_fin').val( hora );
        $('#txtMinuto_fin').val( minuto );
        $('#selectAm_Pm_fin').val( txtHora_Minuto_AmPM.substr(6, 2) );
    }
});

$('#btnHora_fin').click(function() {
    if(mostrar == 1)
    {
        $('#divHora_fin').show();
        mostrar = 0;
    }
    else
    {
        $('#divHora_fin').hide();
        mostrar = 1;
    }
});


// ( HORA )
$('#txtHora_fin').click(function() {
    $('#txtHora_fin').select();
});
$('#txtHora_fin').blur(function() {
    this.value = muestra_valida_tiempo(this.value, 1);
});
$('#btnHora_arriba_fin').click(function() {
    hora++;
    hora = muestra_valida_tiempo(hora, 1);
});
$('#btnHora_abajo_fin').click(function() {
    hora--;
    hora = muestra_valida_tiempo(hora, 1);
});


// ( MINUTO )
$('#txtMinuto_fin').click(function() {
    $('#txtMinuto_fin').select();
});
$('#txtMinuto_fin').blur(function() {
    this.value = muestra_valida_tiempo(this.value, 0);
});
$('#btnMinuto_arriba_fin').click(function() {
    minuto++;
    minuto = muestra_valida_tiempo(minuto, 0);
});
$('#btnMinuto_abajo_fin').click(function() {
    minuto--;
    minuto = muestra_valida_tiempo(minuto, 0);
});
var muestra_valida_tiempo = function(valor, isHora) {
    var retorno;
    var tiempo = isHora == 1? 11 : 59;
    if(!$.isNumeric(valor))
        retorno = '00';
    else
    {
        valor = parseInt(valor);

        if(valor == tiempo + 1)
            retorno = '00';
        else
        {
            if(valor > 0 && valor <= 9)
                retorno = '0' + valor;
            else
            {
                if(valor > tiempo)
                    retorno = '00';
                else
                {
                    if(valor == -1)
                        retorno = tiempo;
                    else
                        retorno = valor == 0? '00' : valor;
                }
            }
        }
    }
    if(isHora == 1)
        hora = retorno;
    else
        minuto = retorno;

    if(isHora == 1)
        $('#txtHora_fin').val(retorno);
    else
        $('#txtMinuto_fin').val(retorno);

    var Hora_Minuto_AmPM = $('#txtHora_fin').val() + ':' + $('#txtMinuto_fin').val() + ' ' + $('#selectAm_Pm_fin').val();
    $('#txtHora_Minuto_AmPM_fin').val(Hora_Minuto_AmPM);
    
    return retorno;
}


$('#selectAm_Pm_fin').change(function() {
    var Hora_Minuto_AmPM = $('#txtHora_fin').val() + ':' + $('#txtMinuto_fin').val() + ' ' + $('#selectAm_Pm_fin').val();
    $('#txtHora_Minuto_AmPM_fin').val(Hora_Minuto_AmPM); 
});