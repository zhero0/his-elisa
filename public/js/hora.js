var mostrar = 1;
var hora = 0;
var minuto = 0;
$(document).ready(function() {
    $('#divHora').hide(); 

    var txtHora_Minuto_AmPM = $('#txtHora_Minuto_AmPM').val();
    if(txtHora_Minuto_AmPM != '')
    {
        hora = txtHora_Minuto_AmPM.substr(0, 2);
        minuto = txtHora_Minuto_AmPM.substr(3, 2);
        $('#txtHora').val( hora );
        $('#txtMinuto').val( minuto );
        $('#selectAm_Pm').val( txtHora_Minuto_AmPM.substr(6, 2) );
    }
});

$('#btnHora').click(function() {
    if(mostrar == 1)
    {
        $('#divHora').show();
        mostrar = 0;
    }
    else
    {
        $('#divHora').hide();
        mostrar = 1;
    }
});


// ( HORA )
$('#txtHora').click(function() {
    $('#txtHora').select();
});
$('#txtHora').blur(function() {
    this.value = muestra_valida_tiempo(this.value, 1);
});
$('#btnHora_arriba').click(function() {
    hora++;
    hora = muestra_valida_tiempo(hora, 1);
});
$('#btnHora_abajo').click(function() {
    hora--;
    hora = muestra_valida_tiempo(hora, 1);
});


// ( MINUTO )
$('#txtMinuto').click(function() {
    $('#txtMinuto').select();
});
$('#txtMinuto').blur(function() {
    this.value = muestra_valida_tiempo(this.value, 0);
});
$('#btnMinuto_arriba').click(function() {
    minuto++;
    minuto = muestra_valida_tiempo(minuto, 0);
});
$('#btnMinuto_abajo').click(function() {
    minuto--;
    minuto = muestra_valida_tiempo(minuto, 0);
});
var muestra_valida_tiempo = function(valor, isHora) {
    var retorno;
    var tiempo = isHora == 1? 11 : 59;
    if(!$.isNumeric(valor))
        retorno = '00';
    else
    {
        valor = parseInt(valor);

        if(valor == tiempo + 1)
            retorno = '00';
        else
        {
            if(valor > 0 && valor <= 9)
                retorno = '0' + valor;
            else
            {
                if(valor > tiempo)
                    retorno = '00';
                else
                {
                    if(valor == -1)
                        retorno = tiempo;
                    else
                        retorno = valor == 0? '00' : valor;
                }
            }
        }
    }
    if(isHora == 1)
        hora = retorno;
    else
        minuto = retorno;

    if(isHora == 1)
        $('#txtHora').val(retorno);
    else
        $('#txtMinuto').val(retorno);

    var Hora_Minuto_AmPM = $('#txtHora').val() + ':' + $('#txtMinuto').val() + ' ' + $('#selectAm_Pm').val();
    $('#txtHora_Minuto_AmPM').val(Hora_Minuto_AmPM);
    
    return retorno;
}


$('#selectAm_Pm').change(function() {
    var Hora_Minuto_AmPM = $('#txtHora').val() + ':' + $('#txtMinuto').val() + ' ' + $('#selectAm_Pm').val();
    $('#txtHora_Minuto_AmPM').val(Hora_Minuto_AmPM); 
});