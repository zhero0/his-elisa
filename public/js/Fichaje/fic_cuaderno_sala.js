var url = '';
var i = 0;
var datosTabla = [];
var totalExamenSeleccion = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';

$(document).ready(function() {
    
    $('#divErrorPaciente').hide();
    $('#divErrorExamen').hide();
    if($('#txtScenario').val() == 'create')
        url = 'Create';

    if($('#txtScenario').val() == 'edit')
    {
        var datosPoblacion = $('#txtJsonHcl_poblacion').val();
        var dato = JSON.parse(datosPoblacion);
        
        $('#txtidhcl_poblacion').val(dato.id);
        $('#datoNumeroPaciente').text(dato.numero_historia);
        $('#datoNombrePaciente').text(dato.nombre);
        $('#datoApellidosPaciente').text(dato.primer_apellido + ' ' + dato.segundo_apellido);

        $('#txtHora_Minuto_AmPM').val( $('#txtHiddenHora').val() );
    }
 
});
 

// ========================================== [ DATOS ] ==========================================
// [ SALA ]
$('#btnModal_Sala').click(function() {
    $('#vModalBuscadorSala').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Sala').click();
});
$('#txtBuscador_Sala').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Sala').click();
    }
});
$('#btnBuscar_Sala').click(function() {
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Sala').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Sala').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Sala').focus();
            }
            else
                $('#tbody_Sala').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Sala').append(
       '<tr id="rowEspecialidad-'+i+'" ondblclick="eventoDobleClick_Medico('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" class="default">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Medico = function(id, idtabla) {
    seleccionaMedico(id, idtabla);
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscadorMedicos').modal('toggle');
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtMedico').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Sala').val('');
}


// [ Especialidad ]
$('#btnModal_Especialidad').click(function() {
    $('#vModalBuscadorEspecialidad').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Especialidad').click();
});
$('#txtBuscador_Especialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Especialidad').click();
    }
});
$('#btnBuscar_Especialidad').click(function() {
    $.ajax({
        url: 'buscaEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Especialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscador_Especialidad').focus();
            }
            else
                $('#tbody_Especialidad').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaCuaderno('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr id="rowEspecialidad-'+i+'" ondblclick="eventoDobleClick_Cuaderno('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" class="default">'
          + '<td style="display: none;" id="td_idhcl_cuaderno' + i + '">' + id + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Cuaderno = function(id, idtabla) {
    seleccionaCuaderno(id, idtabla);
}
var seleccionaCuaderno = function(id, idtabla) {
    $('#vModalBuscadorEspecialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_nombre' + id).text() );
    $('#idhcl_cuaderno').val( idtabla );
    $('#txtBuscador_Especialidad').val('');
}
// ===============================================================================================
 


// ====================================== [ ESPECIALIDADES ] ======================================
$('#btnActualizaEspecialidad').click(function() {
    $.ajax({
        url: 'buscaProgramados' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: 77
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if(data.length > 0)
            {
                $('#divInformacionProgramados').empty();
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        total: data[i].total
                    };
                    InformacionProgramados(arrayParametro);
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var InformacionProgramados = function(arrayParametro) {
    var myArray = ['aqua', 'blue', 'orange'];
    var randomItem = myArray[Math.floor(Math.random()*myArray.length)];

    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var total = arrayParametro['total'];

    $('#divInformacionProgramados').append(
        '<div class="col-xs-4">' +
           '<div class="info-box" style="border: 1px solid #bbb2b2;">' + 
                '<span class="info-box-icon bg-' + randomItem + '">' + 
                    '<h1>' + total + '</h1>' + 
                '</span>' + 
                '<div class="info-box-content" style="font-size: 13px;">' + 
                    '(' + codigo + ') ' + nombre + 
                '</div>' + 
            '</div>' +
        '</div>'
    );
    i++;
}
// ================================================================================================


// ========================================= [ GUARDAR ] =========================================
$('#btnGuardarCuaderno').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {
            $('#txtJsonCuadernos').val('');
            
            $("#form").submit();
        }
    });
});
// ===============================================================================================
