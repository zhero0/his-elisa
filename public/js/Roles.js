$( document ).ready(function() {
    

});
var i = 0;

$("#usuario").keypress(function(){
    $('#usuario').val( ($('#usuario').val()).trim() );
});

$('#btnGuardarRol').click( function(e) {
    var permisos = $('input:checkbox:checked.classPermiso').map(function () {
                    return this.value;
                }).get();
    if(permisos == 0)
    {
        $('#lblMensajeError').text('Por favor seleccione algún permiso! ');
        $('#divMensajeError').show();
        e.preventDefault();
        return;
    }
    if($('#name').val() == '')
    {
        $('#name').css({'border': '1px solid red'});
        $('#name').focus();
        $('#lblMensajeError').text('Complete el dato requerido! ');
        $('#divMensajeError').show();
        e.preventDefault();
        return;
    }

    bootbox.confirm('Desea guardar los cambios? ', function (confirmed) {
        if (confirmed) {
            var myJSON = JSON.stringify(permisos);
            $('#txtHiddenPermisos').val(myJSON);
            $("#form").submit();
        }
    });

});

$('input[type="checkbox"]').change(checkboxChanged);

function checkboxChanged() {
    var $this = $(this),
        checked = $this.prop("checked"),
        container = $this.parent(),
        siblings = container.siblings();

    container.find('input[type="checkbox"]')
    .prop({
        indeterminate: false,
        checked: checked
    })
    .siblings('label')
    .removeClass('custom-checked custom-unchecked custom-indeterminate')
    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

    checkSiblings(container, checked);
}

function checkSiblings($el, checked)
{
    var parent = $el.parent().parent(),
        all = true,
        indeterminate = false;

    $el.siblings().each(function() {
      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
    });
    
    if (all && checked) {
      parent.children('input[type="checkbox"]')
      .prop({
          indeterminate: false,
          checked: checked
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass(checked ? 'custom-checked' : 'custom-unchecked');

      checkSiblings(parent, checked);
    }
    else {
      $el.parents("li").children('input[type="checkbox"]')
      .prop({
          indeterminate: true,
          checked: false
      })
      .siblings('label')
      .removeClass('custom-checked custom-unchecked custom-indeterminate')
      .addClass('custom-indeterminate');
    }
}