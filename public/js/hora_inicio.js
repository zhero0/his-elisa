var mostrar = 1;
var hora = 0;
var minuto = 0;
$(document).ready(function() {
    $('#divHora_inicio').hide();

    var txtHora_Minuto_AmPM = $('#txtHora_Minuto_AmPM_inicio').val();
    if(txtHora_Minuto_AmPM != '')
    {
        hora = txtHora_Minuto_AmPM.substr(0, 2);
        minuto = txtHora_Minuto_AmPM.substr(3, 2);
        $('#txtHora_inicio').val( hora );
        $('#txtMinuto_inicio').val( minuto );
        $('#selectAm_Pm_inicio').val( txtHora_Minuto_AmPM.substr(6, 2) );
    }
});

$('#btnHora_inicio').click(function() {
    if(mostrar == 1)
    {
        $('#divHora_inicio').show();
        mostrar = 0;
    }
    else
    {
        $('#divHora_inicio').hide();
        mostrar = 1;
    }
});


// ( HORA )
$('#txtHora_inicio').click(function() {
    $('#txtHora_inicio').select();
});
$('#txtHora_inicio').blur(function() {
    this.value = muestra_valida_tiempo_inicio(this.value, 1);
});
$('#btnHora_arriba_inicio').click(function() {
    hora++;
    hora = muestra_valida_tiempo_inicio(hora, 1);
});
$('#btnHora_abajo_inicio').click(function() {
    hora--;
    hora = muestra_valida_tiempo_inicio(hora, 1);
});


// ( MINUTO )
$('#txtMinuto_inicio').click(function() {
    $('#txtMinuto_inicio').select();
});
$('#txtMinuto_inicio').blur(function() {
    this.value = muestra_valida_tiempo_inicio(this.value, 0);
});
$('#btnMinuto_arriba_inicio').click(function() {
    minuto++;
    minuto = muestra_valida_tiempo_inicio(minuto, 0);
});
$('#btnMinuto_abajo_inicio').click(function() {
    minuto--;
    minuto = muestra_valida_tiempo_inicio(minuto, 0);
});
var muestra_valida_tiempo_inicio = function(valor, isHora) {
    var retorno;
    var tiempo = isHora == 1? 11 : 59;
    if(!$.isNumeric(valor))
        retorno = '00';
    else
    {
        valor = parseInt(valor);

        if(valor == tiempo + 1)
            retorno = '00';
        else
        {
            if(valor > 0 && valor <= 9)
                retorno = '0' + valor;
            else
            {
                if(valor > tiempo)
                    retorno = '00';
                else
                {
                    if(valor == -1)
                        retorno = tiempo;
                    else
                        retorno = valor == 0? '00' : valor;
                }
            }
        }
    }
    if(isHora == 1)
        hora = retorno;
    else
        minuto = retorno;

    if(isHora == 1)
        $('#txtHora_inicio').val(retorno);
    else
        $('#txtMinuto_inicio').val(retorno);

    var Hora_Minuto_AmPM = $('#txtHora_inicio').val() + ':' + $('#txtMinuto_inicio').val() + ' ' + $('#selectAm_Pm_inicio').val();
    $('#txtHora_Minuto_AmPM_inicio').val(Hora_Minuto_AmPM);
    
    return retorno;
}


$('#selectAm_Pm_inicio').change(function() {
    var Hora_Minuto_AmPM = $('#txtHora_inicio').val() + ':' + $('#txtMinuto_inicio').val() + ' ' + $('#selectAm_Pm_inicio').val();
    $('#txtHora_Minuto_AmPM_inicio').val(Hora_Minuto_AmPM); 
});