var url = '';
var i = 0;
var idPersona = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var moduloURL = 'edit';
var opcionBusqueda = 0;
$(document).ready(function() { 

    $('#fechaText').text($('#dateHoy').val());

    // if($('#txtScenario').val() == 'edit')
    // {
    //     // url = 'Edit';
    // } 
    // $('#body').attr('class','layout-fixed.sidebar-mini.sidebar-collapse');
     // $('#toggle-button').on('expanded.lte.controlsidebar', handleExpandedEvent);
    // console.log($('#toggle-button'));
});

var solicitarAtencion = function(id) {  
    $('#vModalBuscador_poblacion').modal();   
    alert(idPersona);
    idPersona = id; 
}

// tabla poblacion
$('#txtFindPaterno').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    } 
});
$('#txtFindMaterno').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    } 
});
$('#txtFindNombres').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    } 
});
$('#btnBuscarPaciente').click(function() { 
    $.ajax({
        url: 'buscaPaciente_post' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            paterno: $('#txtFindPaterno').val(),
            materno: $('#txtFindMaterno').val(),
            nombres: $('#txtFindNombres').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyPaciente').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].res_idhcl_poblacion,
                        numero_historia: data[i].res_numero_historia,
                        nombre: data[i].res_nombre,
                        documento: data[i].res_documento,
                        primer_apellido: data[i].res_primer_apellido,
                        segundo_apellido: data[i].res_segundo_apellido,
                        fecha_nacimiento: data[i].res_fecha_nacimiento,
                        sexo: data[i].res_sexo,
                        edad: data[i].res_edad,
                        idhcl_empleador: data[i].res_idhcl_empleador,
                        nro_empleador: data[i].res_nro_empleador,
                        nombre_empleador: data[i].res_nombre_empleador
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                $('#txtBuscadorPaciente').focus();
                $('#txtFindPaterno').val('');
                $('#txtFindMaterno').val('');
                $('#txtFindNombres').val(''); 
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}); 

var generarTuplaPaciente = function(arrayParametro) {
    var id = arrayParametro['id'];
    var numero_historia = arrayParametro['numero_historia'];
    var documento = arrayParametro['documento'];
    var nombre = arrayParametro['nombre'];
    var primer_apellido = arrayParametro['primer_apellido'];
    var segundo_apellido = arrayParametro['segundo_apellido'];

    var fecha_nacimiento = arrayParametro['fecha_nacimiento'];
    var sexo = arrayParametro['sexo'];
    var edad = arrayParametro['edad'];

    var idhcl_empleador = arrayParametro['idhcl_empleador'];
    var nro_empleador = arrayParametro['nro_empleador'];
    var nombre_empleador = arrayParametro['nombre_empleador'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="fa fa-check" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClickPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdDocumento' + i + '">' + documento + '</td>' 
          + '<td id="tdApellidos' + i + '">' + primer_apellido + ' ' + segundo_apellido + '</td>'
            + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>'
            + '<td style="display: none;" id="tdfecha_nacimiento' + i + '">' + fecha_nacimiento + '</td>'
            + '<td style="display: none;" id="tdsexo' + i + '">' + sexo + '</td>'
            + '<td style="display: none;" id="tdedad' + i + '">' + edad + '</td>'
            + '<td style="display: none;" id="tdidhcl_empleador' + i + '">' + idhcl_empleador + '</td>'
            + '<td style="display: none;"id="tdNro_empleador' + i + '">' + nro_empleador + '</td>' 
            + '<td style="display: none;" id="tdnombre_empleador' + i + '">' + nombre_empleador + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClickPaciente = function(id, idtabla) {
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) {
    $('#vModalBuscador_poblacion').modal('toggle');

    $('#idhcl_poblacion').val($('#tdIdPaciente' + id).text());
    $('#idhcl_empleador').val($('#tdidhcl_empleador' + id).text());
    $('#txtBuscadorPaciente').val('');

    $('#tdMedico' + idPersona).text('holaaa');
    $('#tdDocumento_fic' + idPersona).text($('#tdDocumento' + id).text());
    $('#tdPersona_fic' + idPersona).text($('#tdApellidos' + id).text() + ' ' + $('#tdNombrePaciente' + id).text());
    $('#tdReserva_fic' + idPersona).text('Reservado');
    $('#tdMedico_fic' + idPersona).text( $('#idMedico_titulo').text()); 
    $('#btnAtencion' + idPersona).css({display: 'none'});
    $('#btnLiberar' + idPersona).css({display: ''}); 
    $('#btnImpresion' + idPersona).css({display: ''});
    $('#btnEstudios' + idPersona).css({display: ''});    

    $('#btnImpresion'+ idPersona).attr('class', 'btn btn-block btn-outline-success btn-sm');
    $('#btnEstudios'+ idPersona).attr('class', 'btn btn-outline-dark btn-sm');
    var idhcl_cuaderno_fic  = $('#idhcl_cuaderno').val();
    var iddatosgenericos_fic  = $('#iddatosgenericos').val();
    var idhcl_poblacion_fic  = $('#tdIdPaciente' + id).text();
    $.ajax({
        url: 'registraAtencion' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idhcl_fichas_programadas: idPersona,
            idhcl_cuaderno_fic: idhcl_cuaderno_fic,
            iddatosgenericos_fic: iddatosgenericos_fic,
            idhcl_poblacion_fic: idhcl_poblacion_fic
        }, 
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });

    idPersona = 0;
 
    // $('#poblacion').val($('#tdApellidos' + id).text() + ' ' + $('#tdNombrePaciente' + id).text()); 
    // // $('#txtidhcl_poblacion').val( $('#tdIdPaciente' + id).text() );
    // $('#res_numero_historia').val( $('#tdNumeroHistoria' + id).text() );
    // $('#res_fecha_nacimiento').val( $('#tdfecha_nacimiento' + id).text() );
    // $('#res_edad').val( $('#tdedad' + id).text() );
    // $('#res_sexo').val( $('#tdsexo' + id).text() == '1'? 'MASCULINO':'FEMENINO' );
    // $('#sexo').val( $('#tdsexo' + id).text() );
    
    
    // $('#empleador').val($('#tdnombre_empleador' + id).text()); 
    // $('#res_nro_empleador').val($('#tdNro_empleador' + id).text()); 
}

var eliminarAtencion = function(id) {   
    $('#tdMedico' + id).text('holaaa');
    $('#tdDocumento_fic' + id).text('');
    $('#tdPersona_fic' + id).text('');
    $('#tdReserva_fic' + id).text('Libre');
    $('#tdMedico_fic' + id).text( ''); 
    $('#btnAtencion' + id).css({display: ''});
    $('#btnLiberar' + id).css({display: 'none'}); 
    $('#btnImpresion' + id).css({display: 'none'});  
    $('#btnImpresion'+ id).attr('class', 'btn btn-block btn-outline-danger btn-sm');
    $('#btnEstudios' + id).css({display: 'none'});  
    $('#btnEstudios'+ id).attr('class', 'btn btn-block btn-outline-danger btn-sm');

    $.ajax({
        url: 'eliminarAtencion' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idhcl_fichas_programadas: id 
        }, 
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });

    idPersona = 0;
}

// var imprimirAtencion = function(id) {   

//     idPersona = 0;
// }


// ========================================= [ REPORTE ] =========================================
var imprimirAtencion = function(id) { 
    $('#vImprimeSolicitud').modal();    
    $('.progress').show();
    
    var parametro = {        
        id: id,
    };
    $('#imprimeAtencion').html("").append($("<iframe></iframe>", {
        src: url+'/edit/imprimeFicha/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
var imprimirEstudios = function(id) { 
    $('#vImprimeSolicitud').modal();    
    $('.progress').show();
    
    var parametro = {        
        id: id,
    };
    $('#imprimeAtencion').html("").append($("<iframe></iframe>", {
        src: url+'/edit/imprimeMedicina/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
//  ---------------->> FIN "REPORTE"
$('#dateFicha').keypress(function(e) {
    if(e.which == 13) {
        if ($('#dateFicha').val()!='') {
            opcionBusqueda = 1;
            $('#fechaText').text($('#dateFicha').val());
            $('#btnBuscar').click();
        }else{
            opcionBusqueda = 3;
        }  
    } 
});
$('#txtDocumento').keypress(function(e) {
    if(e.which == 13) {
        if ($('#txtDocumento').val()!='') {
            opcionBusqueda = 2;
            $('#btnBuscar').click();
        }else{
            opcionBusqueda = 3;
        } 
    } 
});
$('#txtPaterno').keypress(function(e) {
    if(e.which == 13) {
        if ($('#txtPaterno').val()!='') {
            opcionBusqueda = 2;
            $('#btnBuscar').click();
        }else{
            opcionBusqueda = 3;
        }         
    } 
});
$('#txtMaterno').keypress(function(e) {
    if(e.which == 13) {
        if ($('#txtMaterno').val()!='') {
            opcionBusqueda = 2;
            $('#btnBuscar').click();
        }else{
            opcionBusqueda = 3;
        } 
    } 
});
$('#txtNombres').keypress(function(e) {
    if(e.which == 13) {
        if ($('#txtNombres').val()!='') {
            opcionBusqueda = 2;
            $('#btnBuscar').click();
        }else{
            opcionBusqueda = 3;
        }  
    } 
});
$('#btnBuscar').click(function() { 
    var fecha = $('#dateFicha').val();
    var documento = $('#txtDocumento').val();
    var paterno = $('#txtPaterno').val();
    var materno = $('#txtMaterno').val();
    var nombres = $('#txtNombres').val();  
    var iddatosgenericos = $('#iddatosgenericos').val(); 
    var idhcl_cuaderno = $('#idhcl_cuaderno').val(); 


    if (opcionBusqueda == 1 || opcionBusqueda == 2) {  
        $.ajax({
            url: 'buscaPaciente_ficha' + url,
            type: 'post',
            data: {
                _token: $('#token').val(),             
                documento: documento,
                paterno: paterno,
                materno: materno,
                nombres: nombres,
                fecha: fecha,
                opcionBusqueda: opcionBusqueda,
                iddatosgenericos: iddatosgenericos,
                idhcl_cuaderno: idhcl_cuaderno,
            },
            success: function(dato) {
                var JSONString = dato;
                var data = JSON.parse(JSONString);
                $('#tbodyContent').empty();
               
                if(data.length > 0)
                {
                    for(var i = 0; i < data.length; i++)
                    {
                        var arrayParametro = {
                            id: data[i].id,
                            res_idhcl_fichas_registro: data[i].res_idhcl_fichas_registro,
                            res_numero: data[i].res_numero,
                            res_fecha_registro: data[i].res_fecha_registro,
                            res_hora_registro: data[i].res_hora_registro,
                            res_idhcl_ficha_estado: data[i].res_idhcl_ficha_estado,
                            res_idhcl_poblacion: data[i].res_idhcl_poblacion,
                            res_documento: data[i].res_documento,
                            res_primer_apellido: data[i].res_primer_apellido,
                            res_segundo_apellido: data[i].res_segundo_apellido,
                            res_nombre: data[i].res_nombre,
                            res_sigla: data[i].res_sigla,
                            res_especialidad: data[i].res_especialidad,
                            res_matricula: data[i].res_matricula,
                            res_ap_medico: data[i].res_ap_medico,
                            res_nombres_medico: data[i].res_nombres_medico,
                            res_ficha_id: data[i].res_ficha_id,
                            res_ficha_nombre: data[i].res_ficha_nombre
                        };
                        generarTuplaFichaje(arrayParametro); 
                    }
                    $('#dateFicha').focus();
                    $('#dateFicha').val('');
                    $('#txtDocumento').val('');
                    $('#txtPaterno').val('');
                    $('#txtMaterno').val('');
                    $('#txtNombres').val(''); 
                }
                else
                    $('#tbodyContent').append(
                      '<tr id="idListaVaciaPaciente">' + 
                          '<td>Lista Vacía...</td>' +
                      '</tr>'
                    );
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert('error! ');
            } 
        }); 
        opcionBusqueda = 3;
    }; 
    if (opcionBusqueda == 3) { 
        $('#tbodyContent').empty();
        $('#tbodyContent').append(
            '<tr id="idListaVaciaPaciente">' + 
                '<td>Lista Vacía...</td>' +
            '</tr>'
        );
    };

    
    
});
 
var generarTuplaFichaje = function(arrayParametro) {

    var dateHoy = $('#dateHoy').val();
    var fechaFind = $('#dateFicha').val(); 
 
    var id = arrayParametro['id'];
    var res_idhcl_fichas_registro = arrayParametro['res_idhcl_fichas_registro'];
    var res_numero = arrayParametro['res_numero']; 
    var res_fecha_registro = arrayParametro['res_fecha_registro']; 
    var res_hora_registro = arrayParametro['res_hora_registro']; 
    var res_idhcl_ficha_estado = arrayParametro['res_idhcl_ficha_estado']; 
    var res_idhcl_poblacion = arrayParametro['res_idhcl_poblacion']; 
    var res_documento = arrayParametro['res_documento'] != null ? arrayParametro['res_documento'] : '';  
    var res_primer_apellido = arrayParametro['res_primer_apellido']  != null ? arrayParametro['res_primer_apellido'] : '';   
    var res_segundo_apellido = arrayParametro['res_segundo_apellido']  != null ? arrayParametro['res_segundo_apellido'] : '';
    var res_nombre = arrayParametro['res_nombre'] != null ? arrayParametro['res_nombre'] : ''; 
    var paciente = res_primer_apellido + ' ' + res_segundo_apellido  + ' ' + res_nombre;
    var res_sigla = arrayParametro['res_sigla']; 
    var res_especialidad = arrayParametro['res_especialidad']; 
    var res_matricula = arrayParametro['res_matricula']; 
    var res_ap_medico = arrayParametro['res_ap_medico'] != null ? arrayParametro['res_ap_medico'] : ''; 
    var res_nombres_medico = arrayParametro['res_nombres_medico'] != null ? arrayParametro['res_nombres_medico'] : ''; 
    var medico =  res_ap_medico+' '+res_nombres_medico;
    var res_ficha_id = arrayParametro['res_ficha_id']; 
    var res_ficha_nombre = arrayParametro['res_ficha_nombre']; 
    
    var modelAtencion = res_ficha_id == 1? '':'none';
    var modelImprimir = res_ficha_id == 2? 'btn-outline-success':'btn-outline-danger';
    var modelImprimir_estudios = res_ficha_id == 2? 'btn-outline-dark':'btn-outline-danger';
    var modelEliminar = res_ficha_id == 1? 'none':'';
    var modelEliminarImp = res_ficha_id == 1? 'none':'';
    var botonSeleccion = '';
 
    botonSeleccion = 
        '<td class="td-actions text-right">'+
            '<div class="btn-group">'+
                '<a onclick="solicitarAtencion('+id+')" id="btnAtencion'+id+'"  class="btn btn-outline-info btn-sm" style="display:'+modelAtencion+';  rel="tooltip" title="Registrar reserva">'+                    
                '<i class="fa fa-user"></i>'+
                '</a>'+
                '<a onclick="eliminarAtencion('+id+')" id="btnLiberar'+id+'"  class="btn btn-outline-danger btn-sm" style="display:'+modelEliminar+'; rel="tooltip" title="Eliminar reserva">'+
                '<i class="fa fa-trash"></i>'+
                '</a>'+
                '<a onclick="imprimirAtencion('+id+')" id="btnImpresion'+id+'" class="btn btn-block '+modelImprimir+' btn-sm"  style="display:'+modelEliminarImp+';  rel="tooltip" title="Imprimir reserva">'+
                '<i class="fa fa-print"></i>'+
                '</a>'+
                '<a onclick="imprimirEstudios('+id+')" id="btnEstudios'+id+'" class="btn '+modelImprimir_estudios+' btn-sm"  style="display:'+modelEliminarImp+';  rel="tooltip" title="Imprimir estudios">'+
                '<i class="fa fa-print"></i>'+
                '</a>'+
            '</div>'+
        '</td>';
 
    if (fechaFind<dateHoy) { 
        botonSeleccion = 
            '<td class="td-actions text-right">'+
                '<div class="btn-group">'+ 
                '</div>'+
            '</td>';
    }

    $('#tbodyContent').append(
       '<tr role="row" style="font-size: 12px;">'
            + '<td style="display: none;" id="tdIdFicha' + i + '">' + id + '</td>'
            + '<td >' + res_numero + '</td>'
            + '<td>' + res_hora_registro + '</td>' 
            + '<td class="text-left">' + res_fecha_registro + '</td>'
            + '<td class="text-left" id="tdMedico_fic'+id+'">' + medico + '</td>'
            + '<td class="text-left" id="tdDocumento_fic'+id+'">' + res_documento + '</td>'
            + '<td class="text-left" id="tdPersona_fic'+id+'">' + paciente + '</td>'
            + '<td class="text-left" id="tdReserva_fic'+id+'">' + res_ficha_nombre + '</td>'
            + '<td></td>'
            + '<td></td>' 
            + botonSeleccion
       +'</tr>'
    ); 
    i++;  
}


var imprimirLista = function() { 

    var fechaFind = $('#fechaText').text();
    var iddatosgenericos = $('#iddatosgenericos').val(); 
    var idhcl_cuaderno = $('#idhcl_cuaderno').val();  

    $('#vImprimeSolicitud').modal();    
    $('.progress').show();
    
    var parametro = {        
        fechaFind: fechaFind,
        iddatosgenericos: iddatosgenericos,
        idhcl_cuaderno: idhcl_cuaderno,
    };
    $('#imprimeAtencion').html("").append($("<iframe></iframe>", {
        src: url+'/edit/reporteLista/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
 