var url = '';

$(document).ready(function() {
     url = 'Create';

    if($('#txtScenario').val() == 'edit')
    {
    	url = '';
    	var json_Informacion = JSON.parse($('#txtHiddenJSONdatos').val());
    	$('#checkSeguridad').prop('checked', json_Informacion.nivel_seguridad == 1? true : false);
    }
  	
});
  

// ========================================= [ GUARDAR ] =========================================
$('#btnGuardar').click( function(e) { 
    Swal.fire({
                icon: 'warning',
                title: 'Registro de especialidad.',
                text: 'Desea guardar la información?', 
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, deseo registrar!'
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Registro completo!', '', 'success')
                    var datoExtra = {};
                    datoExtra.seguridad = $("#checkSeguridad").is(':checked')? 1 : 0;
                    $('#txtHiddenJSONdatos').val( JSON.stringify(datoExtra) );
                    $("#form").submit();
                }  
            }) 
});
// ===============================================================================================