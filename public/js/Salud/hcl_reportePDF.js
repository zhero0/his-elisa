var opcion = 1;
var arrayTitulos;
var arrayDatos;
$(document).ready(function() 
{  
    var datas = JSON.parse($('#data').val()); 
    var datas2 = JSON.parse($('#data2').val()); 
    var datas3 = JSON.parse($('#data3').val()); 
    var datas4 = JSON.parse($('#data4').val()); 

    if(datas != "") 
    {  
        var total = 0;
        for(var c = 0; c < datas.length; c++)
        {  
            total +=  parseInt(datas[c].data);
        }          
        $('#spanTotal').text(total);
        $('#spanTotal2').text(total);
        $('#spanTotal4').text(total);
        $('#spanTotal5').text(total);
    } 
  
    $('#container').highcharts({
            title: {
                text: 'Lista de atenciones medicas'
            },
            series:datas,
            xAxis: {
              categories: ['Totales']
            }, 
            yAxis: {
              title: ['a']
            },
            chart: {
                type: 'bar'
            }

        });

    $('#container2').highcharts({
        title: {
          text: 'Lista de atenciones medicas'
        },
        // subtitle: {
        //   text: 'Total Sales - 2014'
        // },
        chart: {
          type: 'column'
        },
        xAxis: {
          categories: ['Totales'],
        },
        yAxis: {
          title: {
            text: 'Valores'
          }
        },
        series: datas
    });

    $('#container3').highcharts({
        title: {
          text: 'Acmeaa Widget Company'
        },
        subtitle: {
          text: 'Total Sales - 2014'
        },
        chart: {
          type: 'column'
        },
        xAxis: {
          categories: ['Sue Frost', 'Jim Stone', 'Linda Brady', 'Edward Lee'],
        },
        yAxis: {
          title: {
            text: 'Totales'
          }
        },
        series: datas2
    });
    $('#container4').highcharts({
        title: {
          text: 'CONSULTAS REALIZADA POR EDAD'
        },
        subtitle: {
          text: 'Total Sales - 2014'
        },
        chart: {
          type: 'column'
        },
        xAxis: {
          categories: ['Sue Frost', 'Jim Stone', 'Linda Brady', 'Edward Lee'],
        },
        yAxis: {
          title: {
            text: 'Totales'
          }
        },
        series: datas3
    });
    $('#container5').highcharts({
        title: {
          text: 'ATENCIONES REALIZADAS POR GENERO'
        },
        subtitle: {
          text: 'Total Sales - 2014'
        },
        chart: {
          type: 'column'
        },
        xAxis: {
          categories: ['Genero'],
        },
        yAxis: {
          title: {
            text: 'Totales'
          }
        },
        series: datas4
    });
 
});


for(var i = 1; i <= $("#contenedorReportes a").length; i++)
    $('#div' + i).hide();
$('#div1').show();

$("#contenedorReportes a").click(function() {
    $("#contenedorReportes a span").attr('class', 'glyphicon glyphicon-minus');
    $('span', this).attr('class', 'glyphicon glyphicon-ok');
    
    $("#contenedorReportes a").css({"color": "rgba(42, 64, 63, 0.88)", "font-weight": "normal", "font-size": "15px"});
    $(this).css({"color": "rgb(0, 166, 90)", "font-weight": "bold", "font-size": "15px"});

    var opcionReporte = $(this).attr('value');
    opcion = opcionReporte; 

    for(var i = 1; i <= $("#contenedorReportes a").length; i++)
        $('#div' + i).hide();
    $('#div' + opcionReporte).show();
    $('#txtOpcionReporte').val(opcionReporte);
});

$('#btnImprimir').click(function() { 
    var particularcheck = 4;
    $('#vImprimeProgramacionResultado').modal({ backdrop: 'static', keyboard: false });
    if( $('#particular').is(':checked') ) {
        particularcheck = 1;
    }else {
        particularcheck = 4;
    }
    
    var parametro = {
        opcion: opcion,
        especialidadLista: $('#especialidadLista').val(),    
        especialidadForm: $('#especialidadForm').val(),              
        particular: particularcheck,       
        fechaLista: $('#fechaLista').val(),
        fechaForm: $('#fechaForm').val(),
        fechaDesde: $('#fechaDesde').val(),
        fechaHasta: $('#fechaHasta').val(),
        fechaDesde_mdt: $('#fechaDesde_mdt').val(),
        fechaHasta_mdt: $('#fechaHasta_mdt').val(),        
        deposito_mdt: $('#deposito_mdt').val(),
        banco_mdt: $('#banco_mdt').val()
    };
    $('.progress').show();
    $('#secccionImprimeDocumento').html("").append($("<iframe></iframe>", {
        src: 'hcl_reporte/imprimeDocumento/' + JSON.stringify(parametro),
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 2000);
    }, 1000);

    
    // $("#form").submit();
});