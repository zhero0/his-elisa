var url = '';
var i = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
$(document).ready(function() {  
    if($('#txtScenario').val() == 'create')
        url = 'Create';  
});

var abrirVentanaMed = function(option, titulo, ruta) { 
    modal_OPCION = option;
    modal_ROUTE = ruta;

    if (modal_OPCION == 1) {
        $('#vModalBuscador_especialidad').modal();
        $('#vModalBuscador_especialidad .modal-title').text(titulo);
    }
    if (modal_OPCION == 2) {
        $('#vModalBuscador_medico').modal();
        $('#vModalBuscador_medico .modal-title').text(titulo);
    }  
} 

// tabla especialidad
$('#txtBuscadorEspecialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarEspecialidad').click();
    } 
});
$('#btnBuscarEspecialidad').click(function() {
    $.ajax({
        url: 'buscaEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorEspecialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        sigla: data[k].sigla,
                        nombre: data[k].nombre
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscadorEspecialidad').focus();
            }
            else 
                $('#tbody_Especialidad').append(
                  '<tr id="idListaVaciaEspecialidad">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var sigla = arrayParametro['sigla'];
    var nombre = arrayParametro['nombre'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaEspecialidad('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr ondblclick="eventoDobleClick_Especialidad('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick +'" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idhcl_cuaderno' + i + '">' + id + '</td>'
          + '<td id="td_sigla' + i + '">' + sigla + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Especialidad = function(id, idtabla) {
    seleccionaEspecialidad(id, idtabla);
}
var seleccionaEspecialidad = function(id, idtabla) {
    $('#vModalBuscador_especialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_sigla' + id).text()+' - '+$('#td_nombre' + id).text() );
    $('#idhcl_cuaderno').val( idtabla );
    $('#txtBuscadorEspecialidad').val('');
}

// tabla medico

$('#txtBuscadorMedico').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarMedico').click();
    }
});
$('#btnBuscarMedico').click(function() {
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorMedico').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medico').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        matricula: data[k].matricula,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscadorMedico').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr id="idListaVaciaMedico">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var matricula = arrayParametro['matricula'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr ondblclick="eventoDobleClick_Medico('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_matricula' + i + '">' + matricula + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>' 
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Medico = function(id, idtabla) {
    seleccionaMedico(id, idtabla);
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscador_medico').modal('toggle');
    
    var nombres = $('#td_matricula' + id).text() + ' - ' +$('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtMedico').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscadorMedico').val('');
}


var guardar = function() { 
    var iddatosgenericos = $('#iddatosgenericos').val();
    var idhcl_cuaderno = $('#idhcl_cuaderno').val(); 
 
    if(iddatosgenericos != null  && idhcl_cuaderno != null )
    { 
        $("#form").attr('onsubmit', '');
        $("#form").submit(); 
    }
} 


var eliminarRegistro = function() 
{   
    $.ajax({
        url: 'eliminarFichas' + url,
        type: 'post',
        data: {
            _token: $('#token').val()
            
        }, 
        success: function(dato) {
            $('#btnGuardarValidar').hide();
            $('#btnEliminar').hide();
            $('#nro_folio').attr("readonly", true);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    }); 
} 