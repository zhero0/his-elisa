$(document).ready(function() {  
    url = 'Create';  
        $('#uno').mouseover(function() {
            artyom.say("enlace 1")
        });

        $('#uno').mouseout(function() {
            artyom.shutUp();
        });

        $('#dos').mouseover(function() {
            artyom.say("enlace 2")
        });

        $('#tres').mouseover(function() {
            artyom.say("enlace 3")
        });

        $('#cuatro').mouseover(function() {
            artyom.say("enlace 4")
        });

        //El sistema responde
        artyom.addCommands([
            {
                indexes:["buenos días",'cuál es tu banda favorita','Saluda a mis seguidores'],
                action: function(i){
                    if (i==0) {
                        artyom.say("Hola Raul, buenos dias");
                    }
                    if (i==1) {
                        artyom.say("Raul, me gusta tu banda BLESS");
                    }
                    if (i==2) {
                        artyom.say("Hola gente, espero les este yendo muy bien y que este tutorial les ayude de mucho. Saludos");
                    }
                }
            },
            {
                indexes:['me voy','chau'],
                action: function(){
                    alert('ok, chau');                  
                }
            },
            {
                indexes:['abrir google','abrir facebook', 'abrir youtube'],
                action: function(i){
                    if (i==0) {
                        artyom.say("Abriendo google");
                        window.open("http://www.google.com",'_blank');
                    }
                    if (i==1) {
                        artyom.say("Abriendo facebook");
                        window.open("http://www.facebook.com/intecsolt/",'_blank');
                    }
                    if (i==2) {
                        artyom.say("Abriendo youtube");
                        window.open("https://www.youtube.com/channel/UCF721oswf7EUSsQbuGFoMZQ",'_blank');
                    }
                }
            },
            {
                indexes:['limpiar'],
                action: function(){
                    $('#salida').val('');
                }
            }
        ]); 

        // Escribir lo que escucha.
        artyom.redirectRecognizedTextOutput(function(text,isFinal){
            var texto = $('#salida');
            if (isFinal) {
                texto.val(text);
            }else{
                
            }
        });


        //inicializamos la libreria Artyom
        function startArtyom(){
            artyom.initialize({
                lang: "es-ES",
                continuous:true,// Reconoce 1 solo comando y para de escuchar
                listen:true, // Iniciar !
                debug:true, // Muestra un informe en la consola
                speed:1 // Habla normalmente
            });
        };
        
        // Stop libreria;
        function stopArtyom(){
            artyom.fatality();// Detener cualquier instancia previa
        };

        // leer texto
        $('#btnLeer').click(function (e) {
            e.preventDefault();
            var btn = $('#btnLeer');
            if (artyom.speechSupported()) {
                btn.addClass('disabled');
                btn.attr('disabled', 'disabled');

                var text = $('#leer').val();
                if (text) {
                    var lines = $("#leer").val().split("\n").filter(function (e) {
                        return e
                    });
                    var totalLines = lines.length - 1;

                    for (var c = 0; c < lines.length; c++) {
                        var line = lines[c];
                        if (totalLines == c) {
                            artyom.say(line, {
                                onEnd: function () {
                                    btn.removeAttr('disabled');
                                    btn.removeClass('disabled');
                                }
                            });
                        } else {
                            artyom.say(line);
                        }
                    }
                }
            } else {
                alert("Your browser cannot talk !");
            }
        });

       
});

$('#btnBuscadorLaboratorio').click(function() {
    alert('asd');
    return;
    // $.ajax({        
    //     // url: 'buscaVariables', 
    //     url: 'buscaEmpleador' + url,
    //     type: 'post',
    //     data: {
    //         _token: $('#token').val(),
    //         dato: $('#txtBuscadorEmpleador').val()
    //     },
    //     success: function(dato) {            
    //         $('#tablaEmpleador').empty(); 
    //         var JSONString = dato;
    //         var data = JSON.parse(JSONString);
    //         if (data.length==0) {
    //               $('#tablaEmpleador').append(
    //                   '<tr id="idListaVacia">' + 
    //                       '<td>Lista Vacia...</td>' +
    //                   '</tr>'
    //               );                
    //         }
    //         for(var i = 0; i < data.length; i++)
    //         {
    //             var arrayParametro = {
    //                 id: data[i].id,
    //                 nombre: data[i].nombre,
    //                 nro_empleador: data[i].nro_empleador
    //             };
    //             generarTuplaEmpresa(arrayParametro);
    //         }
    //     },
    //     error: function(xhr, ajaxOptions, thrownError) {
    //         bootbox.alert('Por favor actualize la página! ');
    //     }
    // });
});

$('#btnBuscadorImagenologia').click(function() {
    
    alert('asd');
    return;
    $('#vModalBuscadorImagenologia').modal({
      backdrop: 'static',
      keyboard: true
    });
    
    // $.ajax({        
    //     // url: 'buscaVariables', 
    //     url: 'buscaEmpleador' + url,
    //     type: 'post',
    //     data: {
    //         _token: $('#token').val(),
    //         dato: $('#txtBuscadorEmpleador').val()
    //     },
    //     success: function(dato) {            
    //         $('#tablaEmpleador').empty(); 
    //         var JSONString = dato;
    //         var data = JSON.parse(JSONString);
    //         if (data.length==0) {
    //               $('#tablaEmpleador').append(
    //                   '<tr id="idListaVacia">' + 
    //                       '<td>Lista Vacia...</td>' +
    //                   '</tr>'
    //               );                
    //         }
    //         for(var i = 0; i < data.length; i++)
    //         {
    //             var arrayParametro = {
    //                 id: data[i].id,
    //                 nombre: data[i].nombre,
    //                 nro_empleador: data[i].nro_empleador
    //             };
    //             generarTuplaEmpresa(arrayParametro);
    //         }
    //     },
    //     error: function(xhr, ajaxOptions, thrownError) {
    //         bootbox.alert('Por favor actualize la página! ');
    //     }
    // });
});



    // $(document).ready(function() {

        
    // });

