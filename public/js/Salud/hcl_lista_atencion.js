var url = '';
var i = 0;
var j = 0;
var contador_table = 0; 
var diasBaja = 0;
var desdeBaja = '';
var hastaBaja = '';
var datosDiagnostico = [];
var datosTablaLab = [];
var datosTabla = [];
var totalDiagnosticoCieSeleccion = 0;
var totalExamenSeleccionLab = 0;
var totalExamenSeleccion = 0;
var opcionButton = 0; //    1 => Laboratorio       2 => Imagenología
var observacion_lab = '';
var observacion_ima = '';
var idhcl_cabecera_registro = 0;
var idsexo = 0;
var primeraCajita = -1, ultimaCajita = -1;
var TallaCajita = -1, PesoCajita = -1, IMCCajita = -1;
var hcl_clasificacionsalud = '';
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var textoNoMostrarContenido = 'NO ES POSIBLE VISUALIZAR ESTE CONTENIDO! ';
 
var editor_plantilla; 
var editor_historial; 
var editor_certificado; 
$(document).ready(function() {    
    url = 'Create';

    // $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini');
    // $('#divDatosPaciente').css({display: 'none'});
    // $('#divContenedorSalud').css({display: 'block'});
    // muestraVariablesSignosVitales(20); 

    if($('#txtScenario').val() == 'create' || $('#txtScenario').val() == 'edit')
    {
        hcl_clasificacionsalud = JSON.parse($('#txtHidden_hcl_clasificacionsalud').val());   
        ClassicEditor
            .create(document.querySelector('#ckeditor_plantilla'))
            .then(editor =>{
                editor_plantilla=editor;  
                console.log(editor); 
            })
            .catch(error=>{
                console.error(error);
            }
            ); 
        ClassicEditor
            .create(document.querySelector('#desc_historial'))
            .then(editor =>{
                editor_historial=editor;
                editor.width = 500;
                editor.height = 500; 
                console.log(editor);
            })
            .catch(error=>{
                console.error(error);
            }
            );
        ClassicEditor
            .create(document.querySelector('#taDescripcion_CertificadoMedico'))
            .then(editor =>{
                editor_certificado=editor;
                editor.width = 500;
                editor.height = 500; 
                console.log(editor);
            })
            .catch(error=>{
                console.error(error);
            }
            );
            var json_Informacion = JSON.parse($('#txtHiddenJSONdatos').val());
        var Hcl_poblacion = JSON.parse(json_Informacion.Hcl_poblacion);
        var Hcl_datos_signos_vitales = JSON.parse(json_Informacion.Hcl_datos_signos_vitales);
        // [ VARIABLES SIGNOS VITALES ]
        
        seteaDatos_paciente(Hcl_poblacion); 
        $('#tbody_Variables_signos_vitales').empty();
        for(var c = 0; c < Hcl_datos_signos_vitales.length; c++)
        {
            var arrayParametro = {
                idhcl_variables_signos_vitales: Hcl_datos_signos_vitales[c].idhcl_variables_signos_vitales,
                contenido_variable: Hcl_datos_signos_vitales[c].contenido_variable != undefined? Hcl_datos_signos_vitales[c].contenido_variable : '',
                variable: Hcl_datos_signos_vitales[c].variable,
                unidad: Hcl_datos_signos_vitales[c].unidad,
            };
            // alert(Hcl_datos_signos_vitales[c].idhcl_variables_signos_vitales);
            generarTuplaSignoVital(arrayParametro);
        }
        calculoIMC(TallaCajita, PesoCajita, IMCCajita);
        generarListaDiagnostico(); 
        // ajaxMuestraEstablecimiento();  
    }
    
    if($('#txtScenario').val() == 'edit')
    {
        $('#btnGuardar').css({display: ''});
        url = ''; 
        $('#txtHora_Minuto_AmPM').val( $('#txtHiddenHora').val() );
        $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini');
        $('#divDatosPaciente').css({display: 'none'});          
        $('#divContenedorSalud').css({display: ''});
        $('#divContenedorDatos_paciente').css({display: ''});
        var json_Informacion = JSON.parse($('#txtHiddenJSONdatos').val());
        var tipo_consulta = json_Informacion.tipo_consulta;
        var Hcl_poblacion = JSON.parse(json_Informacion.Hcl_poblacion);
        var Lab_examenes_registroprogramacion = json_Informacion.Lab_examenes_registroprogramacion != ''? JSON.parse(json_Informacion.Lab_examenes_registroprogramacion) : '';
        var Ima_examenes_registroprogramacion = json_Informacion.Ima_examenes_registroprogramacion != ''? JSON.parse(json_Informacion.Ima_examenes_registroprogramacion) : '';
        var Hcl_registro_bajas_medicas = JSON.parse(json_Informacion.Hcl_registro_bajas_medicas);
        var Hcl_registro_referencias_medicas = JSON.parse(json_Informacion.Hcl_registro_referencias_medicas);
        var Hcl_datos_signos_vitales = JSON.parse(json_Informacion.Hcl_datos_signos_vitales);
        
        if(json_Informacion.Far_recetas_dispensadas != "") {
            var Far_recetas_dispensadas =  JSON.parse(json_Informacion.Far_recetas_dispensadas);  
            for(var c = 0; c < Far_recetas_dispensadas.length; c++)
            { 
                var arrayParametro = {
                    id: Far_recetas_dispensadas[c].id,  
                    idfar_articulo_sucursal: Far_recetas_dispensadas[c].idfar_articulo_sucursal,
                    indicacion_articulo: Far_recetas_dispensadas[c].indicacion_articulo,
                    cantidad: Far_recetas_dispensadas[c].cantidad,
                    eliminado: Far_recetas_dispensadas[c].eliminado,
                    codificacion: Far_recetas_dispensadas[c].codificacion,
                    descripcion: Far_recetas_dispensadas[c].descripcion,
                    unidad: Far_recetas_dispensadas[c].unidad,
                    concentracion: Far_recetas_dispensadas[c].concentracion
                };
                generarMedicamento(0,arrayParametro); 
            }  
        }

        seteaDatos_paciente(Hcl_poblacion);
        for(var c = 0; c < Lab_examenes_registroprogramacion.length; c++) {
            datosTablaLab.push( (Lab_examenes_registroprogramacion[c].idlab_examenes).toString() );
        }
        for(var c = 0; c < Ima_examenes_registroprogramacion.length; c++) {
            datosTabla.push( (Ima_examenes_registroprogramacion[c].idima_examenes).toString() );     
            ajaxExamen('', 0);       
        }
        
        // [ BAJA MÉDICA ]
        if(Hcl_registro_bajas_medicas.length == 1)
        {
            $('#txtHiddenBajaMedica').val(1);
            $('#spanBajaMedica').text('SI');
            $('#spanBajaMedica').attr('class', 'label label-success pull-right'); 

            $('#fechaDesde').val(Hcl_registro_bajas_medicas[0].fecha_inicio); 
            $('#fechaHasta').val(Hcl_registro_bajas_medicas[0].fecha_fin);

            desdeBaja = $('#fechaDesde').val();  
            hastaBaja = $('#fechaHasta').val(); 

            var splitDesde = desdeBaja.split('-');
            var splitHasta = hastaBaja.split('-'); 
            
            $('#fechaDesde').val(splitDesde[2]+'-'+splitDesde[1]+'-'+splitDesde[0]); 
            $('#fechaHasta').val(splitHasta[2]+'-'+splitHasta[1]+'-'+splitHasta[0]);

            $('#txtCantidadBajaMedica').val( Hcl_registro_bajas_medicas[0].cantidad_dias );
            $('#txtIdHcl_variables_bajas').val( Hcl_registro_bajas_medicas[0].idhcl_variables_bajas );
            $('#taDescripcion_formato_historia').val( Hcl_registro_bajas_medicas[0].descripcion )
        }
        else
        {
            $('#txtHiddenBajaMedica').val(0);
            $('#spanBajaMedica').text('NO');
            $('#spanBajaMedica').attr('class', 'label label-danger pull-right');
        }

        if(Hcl_registro_referencias_medicas.length == 1)
        {
            $('#txtHiddenReferenciaMedica').val(1);
            $('#spanReferenciaMedica').text('SI');
            $('#spanReferenciaMedica').attr('class', 'label label-success pull-right'); 
 
            $('#taDescripcion_examen_clinico').val( Hcl_registro_referencias_medicas[0].examen_clinico );
            // alert(Hcl_registro_referencias_medicas[0].idalmacenrec);
            $('#txtIdAlmacen').val( Hcl_registro_referencias_medicas[0].idalmacenrec );
            $('#txtIdHcl_cuaderno').val( Hcl_registro_referencias_medicas[0].idhcl_cuadernorec );
            $('#taDescripcion_tratamiento_inicial').val( Hcl_registro_referencias_medicas[0].tratamiento_inicial ); 
            $('#taDescripcion_recomendaciones').val( Hcl_registro_referencias_medicas[0].recomendaciones );
        }
        else
        {
            $('#txtHiddenReferenciaMedica').val(0);
            $('#spanReferenciaMedica').text('NO');
            $('#spanReferenciaMedica').attr('class', 'badge bg-danger float-right'); 
        }

        if(tipo_consulta == 1)
            $('input:radio[name=tipo_consulta]')[0].checked = true;
        else
            $('input:radio[name=tipo_consulta]')[1].checked = true;

        $('#checkHospitalizado').prop('checked', json_Informacion.hospitalizado == 1? true : false);
        $('#checkEmbarazo').prop('checked', json_Informacion.embarazada == 1? true : false);
        $('#checkEmergencia').prop('checked', json_Informacion.emergencia == 1? true : false);
        observacion_lab = json_Informacion.observaciones_laboratorio;
        observacion_ima = json_Informacion.observaciones_imagenologia;
        totalExamenSeleccionLab = datosTablaLab.length;        
        totalExamenSeleccion = datosTabla.length;
        $('#spanTotalExamenesLaboratorio').text( totalExamenSeleccionLab );
        $('#spanTotalExamenes_rx_visual').text( totalExamenSeleccion );
        $('#spanTotalExamenSeleccion').text( totalExamenSeleccion );
        if(json_Informacion.certificadoMedico != '')
        {
            $('#spanCertificadoMedico').text('SI');
            $('#spanCertificadoMedico').attr('class', 'label label-success pull-right');
            $('#txtHiddenCertificadoMedico').val(1);
            $('#taDescripcion_CertificadoMedico').val( json_Informacion.certificadoMedico );
        }
        $('#checkDiagnostico_seguro').prop('checked', json_Informacion.diagnostico_seguro == 1? true : false);
        $('#checkRespuesta_seguro').prop('checked', json_Informacion.respuesta_seguro == 1? true : false);

        // [ VARIABLES SIGNOS VITALES ]
        $('#tbody_Variables_signos_vitales').empty();
        for(var c = 0; c < Hcl_datos_signos_vitales.length; c++)
        {
            var arrayParametro = {
                idhcl_variables_signos_vitales: Hcl_datos_signos_vitales[c].idhcl_variables_signos_vitales,
                contenido_variable: Hcl_datos_signos_vitales[c].contenido_variable != undefined? Hcl_datos_signos_vitales[c].contenido_variable : '',
                variable: Hcl_datos_signos_vitales[c].variable,
                unidad: Hcl_datos_signos_vitales[c].unidad,
            };
            // alert(Hcl_datos_signos_vitales[c].idhcl_variables_signos_vitales);
            generarTuplaSignoVital(arrayParametro);
        }
        calculoIMC(TallaCajita, PesoCajita, IMCCajita);
        generarListaDiagnostico();        
    }
    
    if($('#txtScenario').val() == 'index')
    {
        // Pregunta si quiere imprimir el Resultado
        var datos_imprimir = $('#txtdatos_imprimir').val();
        if(datos_imprimir != '')
        {
            var valor = datos_imprimir.split('*|*');
            var paciente = valor[0];
            idhcl_cabecera_registro = valor[1];
            
            if(idhcl_cabecera_registro > 0)
            {
                bootbox.confirm({
                    title: "SALUD",
                    message: "Desea mostrar las opciones para imprimir de " + paciente + "?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> NO'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> SI'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.ajax({
                                url: 'hcl_cabecera_registro/verifica_opciones_reporte',
                                type: 'post',
                                data: {
                                    _token: $('#token').val(),
                                    dato: idhcl_cabecera_registro
                                },
                                success: function(dato) {
                                    var JSONString = dato;
                                    var data = JSON.parse(JSONString);

                                    if(data.receta > 0)
                                        $('#btnPrint_Receta').show();
                                    else
                                        $('#btnPrint_Receta').hide();

                                    if(data.laboratorio == 1)
                                        $('#btnPrint_Laboratorio').show();
                                    else
                                        $('#btnPrint_Laboratorio').hide();

                                    if(data.imagenologia == 1)
                                        $('#btnPrint_Imagenologia').show();
                                    else
                                        $('#btnPrint_Imagenologia').hide();

                                    if(data.certificado_medico == 1)
                                        $('#btnPrint_CertifMedico').show();
                                    else
                                        $('#btnPrint_CertifMedico').hide();

                                    if(data.baja_medica == 1)
                                        $('#btnPrint_BajaMedica').show();
                                    else
                                        $('#btnPrint_BajaMedica').hide(); 

                                    if(data.referencia_medica == 1)
                                        $('#btnPrint_ReferenciaMedica').show();
                                    else
                                        $('#btnPrint_ReferenciaMedica').hide();

                                    if(data.internacion_medica != null)
                                        $('#btnPrint_InternacionMedica').show();
                                    else
                                        $('#btnPrint_InternacionMedica').hide();

                                    $('#vImprimeDetalleAtencion').modal({ backdrop: 'static', keyboard: false });
                                },
                                error: function(xhr, ajaxOptions, thrownError) {
                                    alert('error! ');
                                }
                            });
                        }
                    }
                });
            }
        }
    }    
});

// ======================================== [ "SIGNOS VITALES" ] ========================================
$('#btnSignos_vitales').click(function() {  
    eventoPaneles(1);    
});
$('#btnRegistro_diagnostico').click(function() { 
    eventoPaneles(2);
});
$('#btnRegistro_medicamentos').click(function() { 
    eventoPaneles(3);
});
$('#btnRegistro_imagenologia').click(function() { 
    eventoPaneles(4);
});
$('#btnRegistro_certificado').click(function() { 
    eventoPaneles(5);
});

$('#btnRegistro_referencia').click(function() { 
    eventoPaneles(6);
});

var eventoPaneles = function(dato) {
    $('#divPanel_signosvitales').css({display: dato == 1? '' : 'none'}); 
    $('#divPanel_diagnostico').css({display: dato == 2? '' : 'none'}); 
    $('#divPanel_medicamentos').css({display: dato == 3? '' : 'none'});
    $('#divPanel_imagenologia').css({display: dato == 4? '' : 'none'}); 
    $('#divPanel_certificado').css({display: dato == 5? '' : 'none'}); 
    $('#divPanel_referencia').css({display: dato == 6? '' : 'none'}); 
}

// ======================================== [ "FIN SIGNOS VITALES" ] ========================================

// ======================================== [ "Paciente" ] ========================================
$('#txtBuscadorPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
        return false;
    }
});
$('#btnBuscarPaciente').click(function() {
    $.ajax({
        url: 'buscaPacientes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorPaciente').val()
        },
        success: function(dato) {
            $('#tbodyPaciente').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        numero_historia: data[i].numero_historia,
                        nombre: data[i].nombre,
                        primer_apellido: data[i].primer_apellido,
                        segundo_apellido: data[i].segundo_apellido,
                        datos: data[i]
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                $('#txtBuscadorPaciente').focus();
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacia...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaPaciente = function(arrayParametro) {
    var datos = arrayParametro['datos'];

    var id = datos.id;
    var numero_historia = datos.numero_historia;
    var nombre = datos.nombre;
    var primer_apellido = datos.primer_apellido;
    var segundo_apellido = datos.segundo_apellido;

    var datosPaciente = {
        id: datos.id,
        nombres: datos.nombre + ' ' + datos.primer_apellido + ' ' + datos.segundo_apellido,
        numero_historia: datos.numero_historia,
        documento: datos.documento,
        edad: datos.edad,
        idsexo: datos.sexo,
        sexo: datos.sexo == 1? 'Masculino' : 'Femenino',
        procedencia: 'FALTA...', //SUCRE-OROPEZA-CHUQUISACA
        residencia: 'FALTA...', // SUCRE
        ocupacion: 'NO HAY EL CAMPO', // CAJERO
    };
    var JSONdatosPaciente = JSON.stringify(datosPaciente); 
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClick('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdJSON_datosPaciente' + i + '">' + JSONdatosPaciente + '</td>'
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>'
          + '<td id="tdApellidos' + i + '">' + primer_apellido + ' ' + segundo_apellido + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick = function(id, idtabla) {
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) { 
    $('#divProgressBar').css({display: 'block'});
    $('#divDatosPaciente').css({display: 'none'});
    var objeto = JSON.parse( $('#tdJSON_datosPaciente' + id).text() );
    
    setTimeout(function()
    {
        muestraVariablesSignosVitales(objeto.edad);
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }

        setTimeout(function()
        {
            $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini');
            $('.progress-bar').css({width: '0%'});

            // [ Setea los datos del paciente ]
            seteaDatos_paciente(objeto);
            
            $('#divProgressBar').css({display: 'none'});
            $('#divContenedorSalud').css({display: ''});
            $('#btnGuardar').css({display: ''});
            $('#divContenedorDatos_paciente').css({display: ''});
            // $('#txtHcl_cie').focus();
            $('#txtValor-' + primeraCajita).focus();
        }, 1000);
    }, 1000);
}
var seteaDatos_paciente = function(objeto) {
    $('#txtidhcl_poblacion').val( objeto.id );
    $('#nombrePaciente').text( objeto.nombres );
    $('#spanNroAsegurado').text( objeto.numero_historia );
    $('#spanDocumento').text( objeto.documento );
    $('#spanEdad').text( objeto.edad );
    $('#spanSexo').text( objeto.sexo );
    $('#spanProcedencia').text( objeto.procedencia );
    $('#spanResidencia').text( objeto.residencia );
    $('#spanOcupacion').text( objeto.ocupacion );

    // alert(objeto.idsexo);   
    idsexo = objeto.idsexo;
    if(objeto.idsexo == 1) // MASCULINO
        $('#divEmbarazo').hide();
    else // FEMENINO
        $('#divEmbarazo').show();
}
// ====================================== [ FIN "Paciente" ] ======================================



// ================================= [ "Variables Signos Vitales" ] =================================
var muestraVariablesSignosVitales = function(edad) {
    var arrayDato = {
        idalmacen: $('#idalmacen').val(),
        edad: edad,
    };
    $.ajax({
        url: 'muestraVariables_signos_vitales' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: arrayDato
        },
        success: function(dato) {
            $('#tbody_Variables_signos_vitales').empty();
            var JSONString = dato;
            var data_signosVitales = JSON.parse(JSONString);
                    
            if(data_signosVitales.length > 0)
            {
                for(var i = 0; i < data_signosVitales.length; i++)
                {
                    var arrayParametro = {
                        idhcl_variables_signos_vitales: data_signosVitales[i].id,
                        contenido_variable: '',
                        variable: data_signosVitales[i].variable,
                        unidad: data_signosVitales[i].unidad,
                    };
                    generarTuplaSignoVital(arrayParametro);
                }
                // $('#txtValor-' + primeraCajita).focus();
            }
            else
                $('#tbody_Variables_signos_vitales').append(
                  '<tr>' + 
                      '<td>Lista Vacia...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarTuplaSignoVital = function(objeto) {
    var resultado = '';
    if(objeto['variable'] == 'IMC')
    {
        IMCCajita = i;
        var cajaTextoValor = '<input type="text" class="form-control input-sm" id="txtValorIMC-' + IMCCajita + '" disabled>';
        var opcionVariable = '<a type="button" onclick="clasificacionSalud()" class="btn btn-info btn-sm" title="Pulse Click para visualizar Clasificación de la Organización mundial de la salud">' +
                                objeto['variable'] + ' <span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span>'+
                              '</a>';
    }
    else
    {
        var cajaTextoValor = '<input type="text" class="form-control input-sm" id="txtValor-' + i + '" value="' + objeto['contenido_variable'] + '">';
        var opcionVariable = objeto['variable'];
    }
    
    if(objeto['variable'] == 'Talla')
        TallaCajita = i;
    if(objeto['variable'] == 'Peso')
        PesoCajita = i;
    if(primeraCajita == -1)
        primeraCajita = i;
    
    $('#tbody_Variables_signos_vitales').append(
       '<tr id="row-' + i + '">'
          + '<td style="display: none;" id="tdIdHcl_variables_signos_vitales' + i + '">' + objeto['idhcl_variables_signos_vitales'] + '</td>'
          + '<td id="tdHiddenVariable' + i + '" style="display: none;">' + objeto['variable'] + '</td>'
          + '<td id="tdVariable' + i + '">' + opcionVariable + '</td>'
          + '<td>' + cajaTextoValor + '</td>'
          + '<td id="tdResultado' + i + '">' + resultado + '</td>'
          + '<td id="tdUnidad' + i + '">' + objeto['unidad'] + '</td>'
       +'</tr>'
    );
    ultimaCajita = i;
    
    // $('#txtOrden' + i).blur(function() {
    //     if(!$.isNumeric(this.value))
    //         this.value = 1;
    // });
    $('#txtValor-' + i).keypress(function(e) {
        if(e.which == 13) {
            var identificador = ($(this).attr('id')).split('-')[1];
            var idNext = parseInt(identificador)+1;
            var variable = $('#tdHiddenVariable' + idNext).text();
            if(variable == 'IMC')
            {
                idNext++;
            }
            $('#txtValor-' + idNext).select();
            
            // ------------------------ [ Cálculo del IMC ] ------------------------
            // solamente cuando esté entre esas 2 cajitas de texto va a calcular el "IMC"
            if(identificador == TallaCajita || identificador == PesoCajita)
            {
                calculoIMC(TallaCajita, PesoCajita, IMCCajita);
            }
            // ---------------------------------------------------------------------

            if(idNext > ultimaCajita)
                $('#txtHcl_cie').focus();
        }
    });
    
    i++;
}
var calculoIMC = function(TallaCajita, PesoCajita, IMCCajita) {
    // IMC = Peso (kg) / altura (m)2
    // IMC [2 decimales] = Peso [2 decimales] / altura [4 decimales]
    var talla_altura = (($('#txtValor-' + TallaCajita).val()).toString()).replace(',', '.');
    var peso = (($('#txtValor-' + PesoCajita).val()).toString()).replace(',', '.');
    talla_altura = (parseFloat(talla_altura)/100).toFixed(4);
    peso = parseFloat(peso).toFixed(2);
    var IMC = parseFloat((parseFloat(peso/(talla_altura * talla_altura))).toFixed(2));
    if(!isNaN(IMC))
    {
        $('#txtValorIMC-' + IMCCajita).val( IMC );

        for(var j = 0; j < hcl_clasificacionsalud.length; j++)
        {
            var condicion = hcl_clasificacionsalud[j].condicion;
            if(condicion == '<' && hcl_clasificacionsalud[j].valorinferior == 0 && IMC < hcl_clasificacionsalud[j].valorsuperior)
            {
                $('#tdResultado' + IMCCajita).text( hcl_clasificacionsalud[j].descripcion );
                $("#row-" + IMCCajita).attr('class', 'warning');
                break;
            }
            else
            {
                if(condicion == '>' && IMC > hcl_clasificacionsalud[j].valorinferior && hcl_clasificacionsalud[j].valorsuperior == 0)
                {
                    $('#tdResultado' + IMCCajita).text( hcl_clasificacionsalud[j].descripcion );
                    $("#row-" + IMCCajita).attr('class', 'danger');
                    break;
                }
                else
                {
                    if(condicion == '' && IMC >= hcl_clasificacionsalud[j].valorinferior && IMC <= hcl_clasificacionsalud[j].valorsuperior)
                    {
                        $('#tdResultado' + IMCCajita).text( hcl_clasificacionsalud[j].descripcion );
                        $("#row-" + IMCCajita).attr('class', 'success');
                        break;
                    }
                }
            }
        }
    }
}
var clasificacionSalud = function() {
    $('#vModalClasificacionSalud').modal({
      backdrop: 'static',
      keyboard: true
    });

    $('#tbody_ClasificacionSalud').empty();
    for(var j = 0; j < hcl_clasificacionsalud.length; j++)
    {
        if(hcl_clasificacionsalud[j].valorinferior > 0 && hcl_clasificacionsalud[j].valorsuperior > 0)
        {
            var valorinferior = hcl_clasificacionsalud[j].valorinferior + ' - ';
            var valorsuperior = hcl_clasificacionsalud[j].valorsuperior;
        }
        else
        {
            var valorinferior = hcl_clasificacionsalud[j].valorinferior == 0? '' : hcl_clasificacionsalud[j].valorinferior;
            var valorsuperior = hcl_clasificacionsalud[j].valorsuperior == 0? '' : hcl_clasificacionsalud[j].valorsuperior;
        }
        var indice_Masa_Corporal = hcl_clasificacionsalud[j].condicion + ' ' + valorinferior + valorsuperior;

        $('#tbody_ClasificacionSalud').append(
            '<tr id="rowClasificacinSalud-' + i + '">' +
              '<td>' + indice_Masa_Corporal + '</td>'+
              '<td>' + hcl_clasificacionsalud[j].descripcion + '</td>'+
            '</tr>'
        );
    }
}
// =============================== [ FIN "Variables Signos Vitales" ] ===============================



// ====================================== [ "Baja Médica" ] ======================================
$('#btnBajaMedica').click(function() {
    $('#textoValidacion_BajaMedica').css({visibility: 'hidden'});
    if($('#txtHiddenBajaMedica').val() == 1)
    {
        $('#checkOpcion').prop('checked', true);
        $("#form_BajaMedica :input").prop("disabled", false);
    }
    else
    {
        $('#checkOpcion').prop('checked', false);
        $("#form_BajaMedica :input").prop("disabled", true);
    }

    $('#vModalBajaMedica').modal({
        backdrop: 'static',
        keyboard: true
    });
});
$('#checkOpcion').click(function(){
    if($("#checkOpcion").is(':checked'))
    {
        $('#txtHiddenBajaMedica').val(1);
        $("#form_BajaMedica :input").prop("disabled", false);

        $('#spanBajaMedica').text('SI');
        $('#spanBajaMedica').attr('class', 'label label-success pull-right');
        $('#fechaHasta').attr('disabled','true');
    }
    else
    {
        $('#txtHiddenBajaMedica').val(0);
        $("#form_BajaMedica :input").prop("disabled", true);

        $('#spanBajaMedica').text('NO');
        $('#spanBajaMedica').attr('class', 'label label-danger pull-right');
    }
}); 

$('#txtCantidadBajaMedica').keypress(function(e) { 
    if(e.which == 13) {
        cantidadDias();
    }
});

var cantidadDias = function() 
{  
 
    desdeBaja = $('#fechaDesde').val(); 
    var dias = $('#txtCantidadBajaMedica').val(); 
  
    var splitDesde = desdeBaja.split('-'); 
    // Month is zero-indexed so subtract one from the month inside the constructor
    var dateInicio = splitDesde[1]+'-'+splitDesde[0]+'-'+splitDesde[2]; //Y M D  

    var fecha = new Date(dateInicio);
    
    var fechaFin  =  sumarDias(fecha,dias);
    dia = fechaFin.getDate();
    mes = fechaFin.getMonth()+1;// +1 porque los meses empiezan en 0
    anio = fechaFin.getFullYear();

    $('#fechaHasta').val(dia+'-'+mes+'-'+anio); 
}

var sumarDias = function(fecha, dias){
  fecha.setDate(fecha.getDate()+ parseInt(dias-1) );
  return fecha;
}
// ==================================== [ FIN "Baja Médica" ] ====================================


// ====================================== [ "REFERENCIA Médica" ] ======================================
$('#btnReferenciaMedica').click(function() {
    $('#textoValidacion_ReferenciaMedica').css({visibility: 'hidden'});
    if($('#txtHiddenReferenciaMedica').val() == 1)
    {
        $('#checkReferenciaMedica').prop('checked', true);
        $("#form_ReferenciaMedica :input").prop("disabled", false);
    }
    else
    {
        $('#checkReferenciaMedica').prop('checked', false);
        $("#form_ReferenciaMedica :input").prop("disabled", true);
    }

    $('#vModalReferenciaMedica').modal({
        backdrop: 'static',
        keyboard: true
    });
});
$('#checkReferenciaMedica').click(function(){
    if($("#checkReferenciaMedica").is(':checked'))
    {
        $('#txtHiddenReferenciaMedica').val(1);
        $("#form_ReferenciaMedica :input").prop("disabled", false);

        $('#spanReferenciaMedica').text('SI');
        $('#spanReferenciaMedica').attr('class', 'badge bg-success float-right'); 
        $('#txtCantidadReferenciaMedica').attr('disabled','true'); 
        $('#divPanel_ref_show').css({display: ''});

    }
    else
    {
        $('#txtHiddenReferenciaMedica').val(0);
        $("#form_ReferenciaMedica :input").prop("disabled", true);

        $('#spanReferenciaMedica').text('NO');
        $('#spanReferenciaMedica').attr('class', 'badge bg-danger float-right'); 
         $('#divPanel_ref_show').css({display: 'none'});
    }
});

$('#txtIdAlmacen').click(function() {   
    var dato = $('#txtIdAlmacen').val();    
    if (dato != 0) {
        ajaxMuestraEspecialidad(dato);  
    };
     
});

var ajaxMuestraEspecialidad = function(id) {
    $.ajax({
        url: 'muestraEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: id
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#txtIdHcl_cuaderno').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {                    
                    var arrayParametro = {
                        id: data[j].id,
                        nombre: data[j].nombre, 
                    };
                    generarListaEspecialidad(arrayParametro);
                }              
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
} 

var generarListaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];    
    var nombre = arrayParametro['nombre']; 
    
    $('#txtIdHcl_cuaderno').append(
        '<option value="' + id + '" id = "' + id + '">'+ nombre +'</option>'        
    ); 
}
// ==================================== [ FIN "REFERENCIA Médica" ] ====================================


// ====================================== [ "HOJA DE INTERNACION" ] ======================================
$('#btnInternacion').click(function() {
    $('#textoValidacion_Internacion').css({visibility: 'hidden'});
    if($('#txtHiddenInternacionMedica').val() == 1)
    {
        $('#checkInternacionMedica').prop('checked', true);
        $("#txtDatosJsonInternacion :input").prop("disabled", false);
    }
    else
    {
        $('#checkInternacionMedica').prop('checked', false);
        $("#txtDatosJsonInternacion :input").prop("disabled", true);
    }

    $('#vModalInternacion').modal({
        backdrop: 'static',
        keyboard: true
    });
});

$('#checkInternacionMedica').click(function(){
    if($("#checkInternacionMedica").is(':checked'))
    {
        $('#txtHiddenInternacionMedica').val(1);
        $("#txtDatosJsonInternacion :input").prop("disabled", false);

        $('#spanInternacionMedica').text('SI');
        $('#spanInternacionMedica').attr('class', 'label label-success pull-right');
        
    }
    else
    {
        $('#txtHiddenInternacionMedica').val(0);
        $("#txtDatosJsonInternacion :input").prop("disabled", true);

        $('#spanInternacionMedica').text('NO');
        $('#spanInternacionMedica').attr('class', 'label label-danger pull-right');
    }
});

$('#txtIdAlmacenInternacion').click(function() {      
    ajaxMuestraEspecialidadInternacion(); 
});

var ajaxMuestraEspecialidadInternacion = function() {
    $.ajax({
        url: 'muestraEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtIdAlmacenInternacion').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#txtIdHcl_cuadernoInternacion').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {                    
                    var arrayParametro = {
                        id: data[j].id,
                        nombre: data[j].nombre, 
                    };
                    generarListaEspecialidadInternacion(arrayParametro);
                }              
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
} 

var generarListaEspecialidadInternacion = function(arrayParametro) {
    var id = arrayParametro['id'];    
    var nombre = arrayParametro['nombre']; 
    
    $('#txtIdHcl_cuadernoInternacion').append(
        '<option value="' + id + '" id = "' + id + '">'+ nombre +'</option>'        
    ); 
}
 
// ==================================== [ FIN "HOJA DE INTERNACION" ] ====================================



// =================================== [ "Certificado Médico" ] ===================================
$('#btnCertificadoMedico').click(function() {
    if($('#txtHiddenCertificadoMedico').val() == 1)
    {
        $('#checkCertificadoMedico').prop('checked', true);
        $("#form_CertificadoMedico :input").prop("disabled", false);       
    }
    else
    {
        $('#checkCertificadoMedico').prop('checked', false);
        $("#form_CertificadoMedico :input").prop("disabled", true);         
    }

    $('#vModalCertificadoMedico').modal({
        backdrop: 'static',
        keyboard: true
    });
});
$('#checkCertificadoMedico').click(function(){
    if($("#checkCertificadoMedico").is(':checked'))
    {
        $('#txtHiddenCertificadoMedico').val(1);
        $("#form_CertificadoMedico :input").prop("disabled", false);

        $('#spanCertificadoMedico').text('SI');
        $('#spanCertificadoMedico').attr('class', 'badge bg-success float-right');
        $('#divPanel_cert_show').css({display: ''}); 
    }
    else
    {
        $('#txtHiddenCertificadoMedico').val(0);
        $("#form_CertificadoMedico :input").prop("disabled", true);

        $('#spanCertificadoMedico').text('NO');
        $('#spanCertificadoMedico').attr('class', 'badge bg-danger float-right');
        $('#divPanel_cert_show').css({display: 'none'});
    }
});
// ================================= [ FIN "Certificado Médico" ] =================================



// ====================================== [ "Opciones" ] ======================================
$('#btnLaboratorio').click(function() {
    // opcionButton = 1;
    // abrirVentanaModal();
});
$('#btnImagenologia').click(function() {
    opcionButton = 2;
    abrirVentanaModal();
});
$('#btnCerrar_lab_ima').click(function() {
    if(opcionButton == 1)
        observacion_lab = $('#taObservaciones_lab_ima').val();
    else
        observacion_ima = $('#taObservaciones_lab_ima').val();
});
var abrirVentanaModal = function() {
    var titulo = '';
    var totalSeleccion = 0;
    if(opcionButton == 1)
    {
        titulo = 'EXÁMENES DE LABORATORIO';
        totalSeleccion = $('#spanTotalExamenesLaboratorio').text();
        $('#taObservaciones_lab_ima').val(observacion_lab);

        $('#divContenedor_GruposLaboratorio').show();
        $("#divContenedor_Informacion_Lab_Ima").attr('class', 'col-xs-12 col-sm-12 col-md-9 col-lg-9');
        ajaxMuestraLab_grupos();
    }
    else
    {
        titulo = 'EXÁMENES DE IMAGENOLOGÍA';
        totalSeleccion = $('#spanTotalExamenesImagenologia').text();
        $('#taObservaciones_lab_ima').val(observacion_ima);

        $('#divContenedor_GruposLaboratorio').hide();
        $("#divContenedor_Informacion_Lab_Ima").attr('class', 'col-xs-12.col-sm-12.col-md-12 col-lg-12');
    }
    $('#tituloModal').text(titulo);
    $('#spanTotalExamenSeleccion').text(totalSeleccion);
    
    $('#btnVisualizaExamenesSeleccionados').click();

    $('#vModalBuscadorExamen').modal({
      backdrop: 'static',
      keyboard: true
    });
}

// [lab_grupos]
var ajaxMuestraLab_grupos = function() {
    $.ajax({
        url: 'muestraLab_grupos' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idalmacen: $('#idalmacen').val()
        },
        success: function(dato) {
            $('#listaGrupo').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    $('#listaGrupo').append(
                        '<button type="button" class="list-group-item" onclick="lab_grupoSelecciondo(' + data[i].id + ')">' + data[i].nombre + ' <span class="badge">' + data[i].total_examenes + '</span></button>'
                    );
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
// ==================================== [ FIN "Opciones" ] ====================================



// ====================================== [ "Imagenología" ] ======================================
// ========================================= [ EXÁMENES ] =========================================
$('#txtBuscarExamen').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarExamen').click();
    }
});
$('#btnBuscarExamen').click(function() {
    var valor = $('#txtBuscarExamen').val();
    ajaxExamen(valor, 1);
    $('#txtBuscarExamen').focus();
});
$('#btnVisualizaExamenesSeleccionados').click(function() {
    ajaxExamen('', 0);
    $('#txtBuscarExamen').val('');
    $('#txtBuscarExamen').focus();
});
var ajaxExamen = function(valor, buscarTodo) {  
    $.ajax({
        url: 'buscaExamenes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            buscarTodo: buscarTodo,
            datosLab_Ima: JSON.stringify(datosTabla)
        },
        success: function(dato) {
            $('#tbodyExamenes').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);

            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        idima_examenes: data[i].idima_examenes,
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        idEspecialidad: data[i].idEspecialidad,
                        codigoEspecialidad: data[i].codigoEspecialidad,
                        nombreEspecialidad: data[i].nombreEspecialidad
                    };
                    generarListaExamenes(arrayParametro);
                }

                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                seleccionarExamenesNuevamente();
            }
            else
                $('#tbodyExamenes').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaExamenes = function(arrayParametro) {
    var idima_examenes = arrayParametro['idima_examenes'];
    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var idEspecialidad = arrayParametro['idEspecialidad'];
    var codigoEspecialidad = arrayParametro['codigoEspecialidad'];
    var nombreEspecialidad = arrayParametro['nombreEspecialidad'];

    var checkbox = '<div class="form-check">'
                    +   '<input type="checkbox" class="form-check-input" id="checkSeleccionar' + i + '">'
                    +   ' <label class="form-check-label" for="checkSeleccionar' + i + '" style="cursor: pointer;">'
                    +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                    +   ' </label>'
                 +  '</div>';
    
    $('#tbodyExamenes').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdima_examenes' + i + '">' + idima_examenes + '</td>'
          + '<td style="display: none;" id="tdIdEspecialidad' + i + '">' + idEspecialidad + '</td>'
          + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td id="tdEspecialidad' + i + '">' + nombreEspecialidad + ' (' + codigoEspecialidad + ')' + '</td>'
          + '<td>' + checkbox + '</td>'
       +'</tr>'
    );

    $('#checkSeleccionar' + i).click(function() {
        examenesSeleccionados();

        if($(this).is(':checked'))
            totalExamenSeleccion++;
        else
            totalExamenSeleccion--;

        $('#spanTotalExamenSeleccion').text(totalExamenSeleccion);
        $('#spanTotalExamenes_rx_visual').text(totalExamenSeleccion);
        $('#txtBuscarExamen').focus();
    });

    i++;
}
var examenesSeleccionados = function() {
    $("#tablaContenedorExamenes tbody tr").each(function (index) 
    {
        var identificador = ($(this).attr('id')).split('-')[1];

        if($.isNumeric(identificador))
        {
            var Idima_examenes = $('#tdIdima_examenes' + identificador).text();
            if($('#checkSeleccionar' + identificador).is(':checked'))
                datosTabla.push(Idima_examenes);
            else
                datosTabla = jQuery.grep(datosTabla, function(value) {
                  return value != Idima_examenes;
                });
        }
    });
}
var seleccionarExamenesNuevamente = function() {
    datosTabla = Array.from(new Set(datosTabla));
    if(datosTabla.length > 0)
    {
        for(i = 0; i < datosTabla.length; i++)
        {
            $("#tablaContenedorExamenes tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                if($.isNumeric(identificador))
                {
                    if($('#tdIdima_examenes' + identificador).text() == datosTabla[i])
                    {
                        $('#checkSeleccionar' + identificador).prop('checked', true);
                        return;
                    }
                }
            });
        }
    }
}
 

// ========================================= [ FIN EXÁMENES ] =========================================


$('#taImagenologia').click(function() {
    $('#btnBuscarImagenologia').click();
});
$('#txtBuscar_imagenologia').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarImagenologia').click();
    }
});
$('#btnBuscarImagenologia').click(function() {
   ajaxMuestraImagenologia(); 
});
var ajaxMuestraImagenologia = function() {
    var fechaDesde = $('#fechaDesde_imagenologia').val();
    var fechaHasta = $('#fechaHasta_imagenologia').val();
    var valor = $('#txtBuscar_imagenologia').val();
    $.ajax({
        url: 'muestraImagenologia' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            fechaDesde: fechaDesde,
            fechaHasta: fechaHasta,
            idhcl_poblacion: $('#txtidhcl_poblacion').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#divContenedor_Imagenologia').empty();

            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var medico_responsable = data[k].medico_responsable == null? '' : data[k].medico_responsable;
                    var observacion = data[k].observacion == null? '' : data[k].observacion;
                    var idestudio = data[k].idpacs;

                    if(idestudio > 0)
                        var botonDescargaDicom = '<button type="button" class="btn btn-success btn-lg" onclick="descargaArchivoDicom(' + idestudio + ')" title="Descargar Archivo Dicom">' +
                                                    '<span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span>' +
                                                  '</button>';
                    else
                        var botonDescargaDicom = '';
                    
                    if(data[k].respuesta == null)
                        var resultado = '<dd></dd>';
                    else
                        var resultado = '<dd style="border: 1px solid #374850">' +  data[k].respuesta + '</dd>';
                    
                    $('#divContenedor_Imagenologia').append(
                        '<li class="time-label">'+
                            '<span class="bg-' + data[k].color + '" title="' + data[k].tipo + '">'+ 
                                data[k].fechaprogramacion +
                            '</span>'+
                        '</li>'+
                        '<li>'+
                            '<i class="glyphicon glyphicon-align-right bg-blue"></i>'+
                            '<div class="timeline-item">'+ 
                                '<span class="time"><i class="fa fa-clock-o"></i> ' + data[k].horaprogramacion + ' </span>'+
                                '<h5 class="timeline-header" style="font-size: 12px; font-weight: bold;">'+
                                    'DETALLE '+
                                '</h5>'+
                                '<div class="timeline-body">'+
                                    '<dl class="dl-horizontal" style="font-size: 12px;">'+
                                        '<dt>DIAGNÓSTICO : </dt>'+
                                        '<dd>' + data[k].diagnostico + '</dd>'+

                                        '<dt>ESPECIALIDAD : </dt>'+
                                        '<dd>' + data[k].especialidad + '</dd>'+

                                        '<dt>MÉDICO SOLICITANTE : </dt>'+
                                        '<dd>' + data[k].medico_solicitante + '</dd>'+

                                        '<dt>OBSERVACIÓN : </dt>'+
                                        '<dd><pre style="background: transparent;">' + observacion + '</pre></dd>'+

                                        // '<dt>MEDICO RESPONSABLE : </dt>'+
                                        // '<dd>' + medico_responsable + '</dd>'+

                                        '<dt>DIAGNÓSTICO : </dt>'+
                                        '<dd><pre style="background: transparent;">' + (data[k].diagnostico == null? '' : data[k].diagnostico) + '</pre></dd>'+

                                        '<dt>RESULTADO : </dt>'+
                                        resultado + 
                                        
                                        '<dt>DICOM : </dt>'+
                                        '<dd>'+
                                            botonDescargaDicom + 
                                        '</dd>'+
                                    '</dl>'+
                                '</div>'+
                            '</div>'+
                        '</li>'
                    );
                }
            }
            else
                $('#divContenedor_Imagenologia').append(
                        '<li class="time-label">'+
                            '<span class="bg-red">SIN CONTENIDO</span>'+
                        '</li>');

            $('#divContenedor_Imagenologia').append(
                    '<li>'+
                        '<i class="fa fa-clock-o bg-gray"></i>'+
                    '</li>'
            );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var descargaArchivoDicom = function(idestudio) {
    $.ajax({
        url: 'descargaDicom' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idestudio: idestudio,
        },
        success: function(dato) {
            var a = document.createElement('a');
            a.href = 'http://172.18.0.3:8080/weasis-pacs-connector/viewer?studyUID=' + dato;
            a.click();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
// ==================================== [ FIN "Imagenología" ] ====================================



// ====================================== [ "Laboratorio" ] ======================================
$('#taLaboratorio').click(function() {
    $('#btnBuscarLaboratorio').click();
});
$('#txtBuscar_laboratorio').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarLaboratorio').click();
    }
});
$('#btnBuscarLaboratorio').click(function() {
   ajaxMuestraLaboratorio(); 
});
var ajaxMuestraLaboratorio = function() {
    var fechaDesde = $('#fechaDesde_laboratorio').val();
    var fechaHasta = $('#fechaHasta_laboratorio').val();
    var valor = $('#txtBuscar_laboratorio').val();
    $.ajax({
        url: 'muestraLaboratorio' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            fechaDesde: fechaDesde,
            fechaHasta: fechaHasta,
            idhcl_poblacion: $('#txtidhcl_poblacion').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#divContenedor_Laboratorio').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var medico_responsable = data[k].medico_responsable == null? '' : data[k].medico_responsable;
                    var observacion = data[k].observacion == null? '' : data[k].observacion;
                    // var idestudio = 77;
                    
                    $('#divContenedor_Laboratorio').append(
                        '<li class="time-label">'+
                            '<span class="bg-' + data[k].color + '" title="' + data[k].tipo + '">'+ 
                                data[k].fechaprogramacion +
                            '</span>'+
                        '</li>'+
                        '<li>'+
                            '<i class="glyphicon glyphicon-align-right bg-blue"></i>'+
                            '<div class="timeline-item">'+ 
                                '<span class="time"><i class="fa fa-clock-o"></i> ' + data[k].horaprogramacion + ' </span>'+
                                '<h5 class="timeline-header" style="font-size: 12px; font-weight: bold;">'+
                                    'DETALLE '+
                                '</h5>'+
                                '<div class="timeline-body">'+
                                    '<dl class="dl-horizontal" style="font-size: 12px;">'+
                                        '<dt>DIAGNÓSTICO : </dt>'+
                                        '<dd>' + data[k].diagnostico + '</dd>'+
                                        
                                        '<dt>ESPECIALIDAD : </dt>'+
                                        '<dd>' + data[k].especialidad + '</dd>'+
                                        
                                        '<dt>MÉDICO SOLICITANTE : </dt>'+
                                        '<dd>' + data[k].medico_solicitante + '</dd>'+
                                        
                                        // '<dt>MEDICO RESPONSABLE : </dt>'+
                                        // '<dd>' + medico_responsable + '</dd>'+
                                        
                                        // '<dt>RESULTADO : </dt>'+
                                        // '<dd><pre style="background: transparent;">' + respuesta + '</pre></dd>'+
                                        
                                        '<dt>OBSERVACIÓN : </dt>'+
                                        '<dd><pre style="background: transparent;">' + observacion + '</pre></dd>'+
                                        
                                        // '<dt>DICOM : </dt>'+
                                        // '<dd>'+
                                        //     '<a href="http://172.18.0.3:8080/weasis-pacs-connector/viewer?studyUID=' + idestudio + '" class="glyphicon glyphicon-download-alt" title="Descargar Archivo Dicom">'+
                                        //     '</a>'+
                                        // '</dd>'+
                                    '</dl>'+
                                '</div>'+
                            '</div>'+
                        '</li>'
                    );
                }
            }
            else
                $('#divContenedor_Laboratorio').append(
                        '<li class="time-label">'+
                            '<span class="bg-red">SIN CONTENIDO</span>'+
                        '</li>');
            
            $('#divContenedor_Laboratorio').append(
                    '<li>'+
                        '<i class="fa fa-clock-o bg-gray"></i>'+
                    '</li>'
            );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
// ==================================== [ FIN "Laboratorio" ] ====================================



// ====================================== [ "Medicamentos" ] ======================================
var Busqueda_multipleFarmacia = function() { 
    var dato = {};
    $("#indexSearchMedicamento :input").each(function(e) {
        var id = this.id;
        var tipoBusqueda = 1;
        var variable = (id).split('Search_')[1];
        eval("dato." + variable + " = '" + this.value + "' ");  
    });
    ajaxBusquedaFarmacia( JSON.stringify(dato) );
}
var ajaxBusquedaFarmacia = function(valor) {  
    $.ajax({
        url: 'buscaProducto' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor
        },
        success: function(dato) { 
            mostrarDatosFarmacia(dato, 0);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var mostrarDatosFarmacia = function(dato, principal) {
    var JSONString = dato;
    var data = JSON.parse(JSONString);
    $('#tbodyMedicamento').empty(); 
    if(data.length > 0)
    {
        for(var i = 0; i < data.length; i++)
        {
            if (data[i].dispensar_cuaderno == true) {
                var arrayParametro = {
                    id: data[i].id,
                    codificacion: data[i].codificacion,
                    descripcion: data[i].descripcion == null? '' : data[i].descripcion,
                    unidad: data[i].unidad == null? '' : data[i].unidad,
                    concentracion: data[i].concentracion == null? '' : data[i].concentracion,
                    cantidad_max_salidad: data[i].cantidad_max_salidad,
                    saldo: data[i].saldo,
                    indicacion: data[i].indicacion == null? '' : data[i].indicacion
                };          
                generarTuplaMedicamento(arrayParametro);
            }
        }
    }
    else
        $('#tbodyMedicamento').append(
          '<tr id="idListaVaciaPaciente">' + 
              '<td>Lista Vacía...</td>' +
          '</tr>'
        );
}

$('#btnFarmacia').click(function() {    
    abrirVentanaModalFarmacia();
});
var abrirVentanaModalFarmacia = function() {     
    $('#txtIdinv_articulo').val(null); 
    $.ajax({
        url: 'buscaProductoCuaderno' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorMedicamento').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyMedicamento').empty(); 
            if(data.length > 0)
            {  
                for(var i = 0; i < data.length; i++)
                {   
                    if (data[i].dispensar_cuaderno == true) {
                        var arrayParametro = {
                        id: data[i].id,
                        codificacion: data[i].codificacion,
                        descripcion: data[i].descripcion == null? '' : data[i].descripcion,
                        unidad: data[i].unidad == null? '' : data[i].unidad,
                        concentracion: data[i].concentracion == null? '' : data[i].concentracion,
                        cantidad_max_salidad: data[i].cantidad_max_salidad,
                        saldo: data[i].saldo,
                        indicacion: data[i].indicacion == null? '' : data[i].indicacion
                        };                    
                        generarTuplaMedicamento(arrayParametro);    
                    }                    
                }
                $('#txtBuscadorMedicamento').focus();
            }
            else
                $('#tbodyMedicamento').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error aqui! ');
        }
    });  

    $('#vModalFarmacia').modal({
      backdrop: 'static',
      keyboard: true
    });
}
$('#btnAgregarMedicamento').click(function(){    
    var id = $('#txtIdinv_articulo').val();  
    if (id  > 0) {
        generarMedicamento(id,0);
        $('#txtSearch_codigo').focus(); 
        $('#txtSearch_codigo').select();  
    }            
});
var generarMedicamento = function(idds,arrayParametro) { 

    var idinv_articulo = idds != 0? $('#idMedicamento' + idds).text() :arrayParametro['idfar_articulo_sucursal'];
    var idfar_cab_detalle =  idds != 0? 0 :arrayParametro['id'];
    var codigo = idds != 0?  $('#tdCodificacion' + idds).text() :arrayParametro['codificacion'];
    var descripcion =   idds != 0? $('#tdDescripcion' + idds).text() :arrayParametro['descripcion'];   
    var unidad =  idds != 0? $('#tdUnidad' + idds).text() : arrayParametro['unidad']+' '+arrayParametro['concentracion'];
    var indicacion = idds != 0? $('#txtIndicaciones').val() :arrayParametro['indicacion_articulo'];
    var cantidad =  idds != 0? $('#cantidad').val() :arrayParametro['cantidad']; 
    var cantidad_maxima_dispensar =  idds != 0? $('#tdcantidad_maxima' + idds).text() : 1;  
    var saldo = idds != 0? $('#tdSaldo' + idds).text() : 100; 
 
    j++;    

    if (cantidad_maxima_dispensar != 0 && saldo != 0) 
    {
        $('#tbodyMedicamentoListaReceta').append(
           '<tr id="rowMed-'+j+'">'
              + '<td style="display: none;" id="tdIdinv_articulo_Dispensar_' + j + '">' + idinv_articulo + '</td>'          
              + '<td style="display: none;" id="tdIdfar_cab_detalle_' + j + '">' + idfar_cab_detalle + '</td>'          
              + '<td id="tdCodigo_Dispensar_' + j + '">' + codigo + '</td>'
              + '<td id="tdDescripcion_Dispensar_' + j + '">' + descripcion + '</td>' 
              + '<td id="tdCantidad_Dispensar_' + j + '">' + cantidad  + '</td>'                    
              + '<td id="tdUnidad_Dispensar_' + j + '">' + unidad  + '</td>'                    
              + '<td id="tdIndicacion_Dispensar_' + j + '">' + indicacion  + '</td>'                    
              + '<td class="col-xs-1">' + 
                '<button type="button" class="btn btn-sm btn-danger"  onclick= "eliminarFilaReceta(' + j + ')" title="Eliminar el articulo seleccionado">' + 
                    '<span class="fa fa-trash" aria-hidden="true"></span>' +
                '</button>' +
                '</td>' +         
           +'</tr>'
        );    
    }   
    
    $('#spanTotalRecetas_visual').text(j); 
    
    $('#txtIdinv_articulo').val('');
    $('#medicamento').val('');    
    $('#txtIndicaciones').val('');
    $('#cantidad').val(0);  
} 

var eliminarFilaReceta = function(id) {   
    var idfar_cab_detalle = parseInt($('#tdIdfar_cab_detalle_' + id).text());
    $("#rowMed-"+id).remove(); 
    j--;  
    $('#spanTotalRecetas_visual').text(j);  
    if(idfar_cab_detalle >0)
    {        
        $.ajax({
            url: 'elminarReceta' + url,
            type: 'post',
            data: {
                _token: $('#token').val(),
                dato: idfar_cab_detalle
            }, 
            error: function(xhr, ajaxOptions, thrownError) {
                alert('error aqui! ');
            }
        }); 
    } 
} 

var generarTuplaMedicamento = function(arrayParametro) {

    var idMedicamento = arrayParametro['id'];    
    var codificacion = arrayParametro['codificacion'];
    var descripcion = arrayParametro['descripcion'];
    var unidad = arrayParametro['unidad']+' '+arrayParametro['concentracion'];
    var cantidad_max_salidad = arrayParametro['cantidad_max_salidad'];
    var saldo = arrayParametro['saldo'];
    var indicacion = arrayParametro['indicacion'];  

    $('#tbodyMedicamento').append(
        '<tr ondblclick="eventoDobleClick_Medicamento('+i+', ' + idMedicamento + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'       
            + '<td style="display: none;" id="idMedicamento' + i + '">' + idMedicamento + '</td>'            
            + '<td style="display: none;" id="indicacion' + i + '">' + indicacion + '</td>'            
            + '<td id="tdCodificacion' + i + '">' + codificacion + '</td>'
            + '<td id="tdDescripcion' + i + '">' + descripcion + '</td>'
            + '<td id="tdUnidad' + i + '">' + unidad + '</td>'
            + '<td id="tdSaldo' + i + '">' + saldo + '</td>'
            + '<td id="tdcantidad_maxima' + i + '">' + cantidad_max_salidad + '</td>'          
            +'</tr>'
        );  
    i++;
}
var eventoDobleClick_Medicamento = function(id, idtabla) {
    seleccionaMedicamento(id, idtabla);
}
var seleccionaMedicamento = function(id, idtabla) {    

    $('#txtIdinv_articulo').val(id);
    // $('#txtunidad').val($('#unidad' + id).text());
    $('#medicamento').val($('#tdCodificacion' + id).text()+ ' - ' + $('#tdDescripcion' + id).text()+ ' ' + $('#tdUnidad' + id).text());
    $('#cantidad').val(0);
    $('#txtIndicaciones').val($('#indicacion' + id).text());
    $('#cantidad').select();
    $('#cantidad').focus();
}
$('#txtIndicaciones').keypress(function(e) {
    if(e.which == 13) {
        var id = $('#txtIdinv_articulo').val(); 
        if (id != '') {            
            generarMedicamento(id,0); 
            $('#txtSearch_codigo').focus();   
            $('#txtSearch_codigo').select();
        }        
    }
});
// ==================================== [ FIN "Medicamentos" ] ====================================



// ======================================= [ "Diagnostico" ] =======================================
$('#txtBuscarCie').keypress(function(e) {
    if(e.which == 13)
        $('#btnBuscarDiagnostico').click();
});
$('#btnBuscarDiagnostico').click(function() {
    ajaxMuestraHcl_cie(1);
    $('#txtBuscarCie').focus();
});
var ajaxMuestraHcl_cie = function(buscarTodo) {
    $.ajax({
        url: 'muestraHcl_cie' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idalmacen: $('#idalmacen').val(),
            buscarTodo: buscarTodo,
            datosDiagnostico: JSON.stringify(datosDiagnostico),
            dato: $('#txtBuscarCie').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyDiagnostico').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {
                    var arrayParametro = {
                        id: data[j].id,
                        codigo: data[j].codigo,
                        descripcion: data[j].descripcion,
                    };
                    generarListaCie(arrayParametro);
                }
                seleccionarDiagnosticoNuevamente();
            }
            else
                $('#tbodyDiagnostico').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaCie = function(arrayParametro) {
    var id = arrayParametro['id'];
    // var codigo = arrayParametro['codigo'];
    var descripcion = arrayParametro['descripcion']+' ('+arrayParametro['codigo']+')';
      
    var checkbox = '<div class="form-check">'
                    +   '<input type="checkbox" class="form-check-input" id="checkSeleccionarCie' + i + '">'
                    +   ' <label class="form-check-label" for="checkSeleccionarCie' + i + '" style="cursor: pointer;">'
                    +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                    +   ' </label>'
                 +  '</div>';
    
    $('#tbodyDiagnostico').append(
       '<tr id="row-' + i + '">' + 
            '<td style="display: none;" id="tdIdhcl_cie' + i + '">' + id + '</td>' +
            '<td id="tdDescripcion' + i + '">' + descripcion + '</td>' + 
            '<td class="col-xs-1">' + checkbox + '</td>' + 
       '</tr>'
    );
    
    $('#checkSeleccionarCie' + i).click(function() {
        diagnosticoSeleccionados();

        if($(this).is(':checked'))
            totalDiagnosticoCieSeleccion++;
        else
            totalDiagnosticoCieSeleccion--;

        $('#spanTotalDiagnosticoCie').text(totalDiagnosticoCieSeleccion);
        $('#txtBuscarCie').focus();
    });
    
    i++;
}
var seleccionarDiagnosticoNuevamente = function() {
    var arrayDatosTabla = datosDiagnostico;
    
    arrayDatosTabla = Array.from(new Set(arrayDatosTabla));
    if(arrayDatosTabla.length > 0)
    {
        for(i = 0; i < arrayDatosTabla.length; i++)
        {
            $("#tablaContenedorDiagnostico tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                //console.log("--> "+identificador);
                if($.isNumeric(identificador))
                {
                    //console.log(parseInt($('#tdIdhcl_cie' + identificador).text()) +"=="+ arrayDatosTabla[i]);
                    if(parseInt($('#tdIdhcl_cie' + identificador).text()) == arrayDatosTabla[i])
                    {
                        $('#checkSeleccionarCie' + identificador).prop('checked', true);
                        return;
                    }
                }
            });
        }
    }
}
var diagnosticoSeleccionados = function() {
    $("#tablaContenedorDiagnostico tbody tr").each(function (index)
    {
        var identificador = ($(this).attr('id')).split('-')[1];
        if($.isNumeric(identificador))
        {
            var Idhcl_cie = parseInt($('#tdIdhcl_cie' + identificador).text());
            if($('#checkSeleccionarCie' + identificador).is(':checked'))
                datosDiagnostico.push(Idhcl_cie);
            else
            {
                datosDiagnostico = jQuery.grep(datosDiagnostico, function(value) {
                  return value != Idhcl_cie;
                });
            }
        }
    });
    datosDiagnostico = Array.from(new Set(datosDiagnostico));
}
$('#btnVisualizaCieSeleccionados').click(function() {
    ajaxMuestraHcl_cie(0);
    $('#txtBuscarCie').val('');
    $('#txtBuscarCie').focus();
});




// ================== >>>>>>>>>>>>>>>>>>>> ESTO ESTOY MODIFICANDO
var generarListaDiagnostico = function() {   
    var data = JSON.parse($('#txtcontenidoTablaDiagnosticos').val());    
    
    $('#tbodyDiagnostico').empty(); 
    if(data.length > 0)
    {         
        for(var i = 0; i < data.length; i++)
        {
            var arrayParametro = {
                id: data[i].idhcl_cie,
                idCIE: data[i].idhcl_cie,
                codigo: data[i].codigo,
                descripcion: data[i].diagnostico,
                band: 1
            };
            datosDiagnostico.push(data[i].idhcl_cie);
            generarListaCie(arrayParametro);
            
            totalDiagnosticoCieSeleccion++;
        }
        seleccionarDiagnosticoNuevamente();


        $('#spanTotalDiagnosticoCie').text(totalDiagnosticoCieSeleccion);
        /*$('#tbodyDiagnostico input').each(function(index) {
            $("#"+$(this).attr("id")).prop("checked", true);
        });*/
    }
    else
        $('#tbodyDiagnostico').append(
          '<tr id="tbodyDiagnostico">' + 
              '<td>Lista Vacía...</td>' +
          '</tr>'
        );
} 

// ================== >>>>>>>>>>>>>>>>>>>> ESTO ESTOY MODIFICANDO
// ===================================== [ FIN "Diagnostico" ] =====================================



// ====================================== [ "TAB Prescripciones" ] ======================================
$('#taFarmacia').click(function() {
    $('#btnBuscarPrescripcion').click();
});
$('#txtBuscar_prescripcion').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPrescripcion').click();
    }
});
$('#btnBuscarPrescripcion').click(function() {
   ajaxMuestraPrescripcion(); 
});
var ajaxMuestraPrescripcion = function() {
    var fechaDesde = $('#fechaDesde_prescripcion').val();
    var fechaHasta = $('#fechaHasta_prescripcion').val();
    var valor = $('#txtBuscar_prescripcion').val();
    $.ajax({
        url: 'muestraPrescripcion' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            fechaDesde: fechaDesde,
            fechaHasta: fechaHasta,
            idhcl_poblacion: $('#txtidhcl_poblacion').val()
        }, 
        success: function(dato) {
             
            var JSONString = dato;
            var data = JSON.parse(JSONString); 
            
            // LISTA DE DISPENSACION DE MEDICAMENTOS
            $('#tbodyPrescripcionLista').empty();
            if(data.length > 0)
            {                
                for(var i = 0; i < data.length; i++)
                {              
                    var arrayParametro = {
                        idinv_articulo: data[i].id,
                        fecha_articulo: data[i].fechadispensacion_detalle,
                        codificacion_articulo: data[i].codificacion_articulo,
                        descripcion_articulo: data[i].descripcion_articulo,
                        codigo_transaccion: data[i].codigo_transaccion,
                        cantidad: data[i].cantidad,
                        nombre_cuaderno: data[i].nombre_cuaderno,
                        nombre_medico: data[i].nombre_medico                            
                    };
                    generarListaPrescripcion(arrayParametro); 
                } 
            }
            else{ 
                $('#tbodyPrescripcionLista').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    }); 
}
var generarListaPrescripcion = function(arrayParametro) {
    var id_articulo = arrayParametro['idinv_articulo'];
    var fecha_articulo = arrayParametro['fecha_articulo'];
    var codigo_articulo = arrayParametro['codificacion_articulo'];
    var descripcion_articulo = arrayParametro['descripcion_articulo']; 
    var cantidad_articulo = arrayParametro['cantidad'];     
    var codigo_transaccion = arrayParametro['codigo_transaccion'];         
    var nombre_cuaderno = arrayParametro['nombre_cuaderno'];      
    var nombre_medico = arrayParametro['nombre_medico'];     
    
    $('#tbodyPrescripcionLista').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdId' + i + '">' + id_articulo + '</td>'          
          + '<td id="tdFecha_articulo' + i + '">' + fecha_articulo + '</td>'
          + '<td id="tdCodigo_articulo' + i + '">' + codigo_articulo + '</td>'
          + '<td id="tdDescripcion_articulo' + i + '">' + descripcion_articulo + '</td>' 
          + '<td id="tdCantidad_articulo' + i + '">' + cantidad_articulo + '</td>'          
          + '<td id="tdNombre_cuaderno' + i + '">' + nombre_cuaderno + '</td>'          
          + '<td id="tdDescripcion_almacen' + i + '">' + nombre_medico + '</td>'          
          + '<td id="tdCodigo_transaccion' + i + '">' + codigo_transaccion + '</td>'      
       +'</tr>'
    ); 
    i++;
}
var descargaArchivoDicom = function(idestudio) {
    $.ajax({
        url: 'descargaDicom' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idestudio: idestudio,
        },
        success: function(dato) {
            var a = document.createElement('a');
            a.href = 'http://172.18.0.3:8080/weasis-pacs-connector/viewer?studyUID=' + dato;
            a.click();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
} 
// ==================================== [ FIN "TAB Prescripciones" ] ====================================



// ====================================== [ Historia Clínico ] ======================================
$('#tabHistoriaClinico').click(function() {
    $('#btnBuscarHistorialClinico').click();
});
$('#txtBuscarHistorial').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarHistorialClinico').click();
    }
});
$('#btnBuscarHistorialClinico').click(function() {
    ajaxMuestraHistorialClinico();
});
var ajaxMuestraHistorialClinico = function() {
    var fechaDesde = $('#fechaDesde_historial').val();
    var fechaHasta = $('#fechaHasta_historial').val();
    var valor = $('#txtBuscarHistorial').val();
    $.ajax({
        url: 'muestraHistoriaClinico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            fechaDesde: fechaDesde,
            fechaHasta: fechaHasta,
            idhcl_poblacion: $('#txtidhcl_poblacion').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var jsonValor = JSON.parse(JSONString);

            var data = JSON.parse(jsonValor.salud);
            $('#divContenedor_HistoriaCilnico').empty();

            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var diagnostico = data[k].diagnostico == null? '' : data[k].diagnostico;
                    var medico = data[k].medico == null? '' : data[k].medico;
                    var consultorio = data[k].consultorio == null? '' : data[k].consultorio;
                    var tipo_consulta = data[k].tipo_consulta == 1? 'NUEVA' : 'REPETIDA';
                    var hospitalizado = data[k].hospitalizado == 1? 'SI' : 'NO';
                    var respuesta = data[k].respuesta == null? '' : data[k].respuesta;
                    var hora_registro = data[k].hora_registro == null? '' : data[k].hora_registro;
                    var btn_imagenologia = '';
                    var btn_certificadoMedico = '';
                    var btn_bajaMedica = '';
                    var btn_referenciaMedica = '';

                    if(data[k].imagenologia > 0)
                        btn_imagenologia = '<button type="button" title="Pulse Click para ver el reporte" onclick="muestraVistaPreviaReporte(1, ' + data[k].id + ')" class="btn btn-primary btn-sm"><i class="fa fa-heartbeat"></i> Imagenologia</button>';
                    if(data[k].descripcion_certificado_med != '')
                        btn_certificadoMedico = '<button type="button" title="Pulse Click para ver el reporte" onclick="muestraVistaPreviaReporte(2, ' + data[k].id + ')" class="btn btn-success btn-sm"><i class="fa fa-wheelchair"></i> Cert. Médico</button>';
                    if(data[k].baja_medica > 0)
                        btn_bajaMedica = '<button type="button" title="Pulse Click para ver el reporte" onclick="muestraVistaPreviaReporte(3, ' + data[k].id + ')" class="btn btn-warning btn-sm"><i class="fa fa-user-md"></i> Baja Médica</button>';
                    if(data[k].referencia_medica > 0)
                        btn_referenciaMedica = '<button type="button" title="Pulse Click para ver el reporte" onclick="muestraVistaPreviaReporte(4, ' + data[k].id + ')" class="btn btn-warning btn-sm"><i class="fa fa-user-md"></i> Referencia Médica</button>';

                    if(data[k].diagnostico_seguro == 1)
                        var textoDiagnostico = '<dd style="color: red; font-weight: bold;"> ' + textoNoMostrarContenido + '</dd>';
                    else
                        var textoDiagnostico = '<dd> ' + diagnostico + ' </dd>';

                    if(data[k].respuesta_seguro == 1)
                        var textoRespuesta = '<dd style="color: red; font-weight: bold;">' + textoNoMostrarContenido + '</dd>';
                    else
                        var textoRespuesta = respuesta;

                    $('#divContenedor_HistoriaCilnico').append( 
                        '<div class="time-label">'+
                            '<span class="bg-success">'+
                              data[k].fecha_registro+
                            '</span>'+
                        '</div>'+                        
                        '<div>' +
                            '<i class="fas fa-user bg-blue"></i>'+
                            '<div class="timeline-item">'+ 
                                '<span class="time"><i class="fas fa-clock"></i>'+hora_registro+'</span>'+
                                '<h3 class="timeline-header"><a href="#"> Especialidad: </a>'+consultorio+'<br><a href="#">Dr.: </a>'+medico+'</h3>'+

                                '<div class="row">'+
                                    '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">'+
                                    '</div>'+
                                    '<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">'+
                                        '<dt>DIAGNOSTICO : </dt>'+
                                        textoDiagnostico + 
                                    '</div>'+
                                '</div>'+
                                '<div class="row">'+
                                    '<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">'+
                                    '</div>'+
                                    '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">'+
                                        '<dt>TIPO ATENCION : </dt>'+
                                        '<dd>' + tipo_consulta + '</dd>'+
                                    '</div>'+
                                    '<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">'+
                                        '<dt>HOSPITALIZADO : </dt>'+
                                        '<dd>' + hospitalizado + '</dd>'+
                                    '</div>'+
                                '</div>'+ 
                                '<lavel><b>Evolución medica:</b></lavel><br>'+
                                '<div class="timeline-body"> '+
                                    textoRespuesta+
                                '</div>'+
                                '<div class="timeline-footer">'+
                                    btn_imagenologia +
                                    btn_certificadoMedico +
                                    btn_bajaMedica + 
                                    btn_referenciaMedica +
                                '</div>'+
                            '</div>'+  
                        '</div>'
                    );
                } 
            }
            else
            {
                $('#divContenedor_HistoriaCilnico').append(
                    '<label">'+
                        '<span class="bg-red">SIN CONTENIDO</span>'+
                    '</label>');

            } 
            

           
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var muestraVistaPreviaReporte = function(opcion, idhcl_cabecera_registro) {
    $('#vModalVisualizarReporte').modal({ backdrop: 'static', keyboard: false });

    if(opcion == 1) // IMAGENOLOGIA
    { 
        if($('#txtScenario').val() == 'create')
        {
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_Imagenologia/' + idhcl_cabecera_registro,
                css: {width: '100%', height: '100%'}
            }));
        }else{
             $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_Imagenologia/',
                css: {width: '100%', height: '100%'}
            })); 
        }        
    }
    else if(opcion == 2) // CERTIFICADO MEDICO
    {
        if($('#txtScenario').val() == 'create')
        {
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_CertificadoMedico/' + idhcl_cabecera_registro,
                css: {width: '100%', height: '100%'}
            }));
        }else{
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_CertificadoMedico/',
                css: {width: '100%', height: '100%'}
            })); 
        }          
    }
    else if(opcion == 3) // BAJA MEDICA
    {   
        if($('#txtScenario').val() == 'create')
        {
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_BajaMedica/' + idhcl_cabecera_registro,
                css: {width: '100%', height: '100%'}
            }));
        }else{
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_BajaMedica/',
                css: {width: '100%', height: '100%'}
            })); 
        }  
    }
    else if(opcion == 4) // REFERENCIA MEDICA
    {   
        if($('#txtScenario').val() == 'create')
        {
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_ReferenciaMedica/' + idhcl_cabecera_registro,
                css: {width: '100%', height: '100%'}
            }));
        }else{
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_ReferenciaMedica/',
                css: {width: '100%', height: '100%'}
            })); 
        } 
    }
    else if(opcion == 5) // INTERNACION MEDICA
    {
        if($('#txtScenario').val() == 'create')
        {
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_ReferenciaMedica/' + idhcl_cabecera_registro,
                css: {width: '100%', height: '100%'}
            }));
        }else{
            $('#secccionImprimeDetalleAtencion_estudios').html("").append($("<iframe></iframe>", {
                src: 'imprime_ReferenciaMedica/',
                css: {width: '100%', height: '100%'}
            })); 
        } 
    }
}
// ================================================================================================


// ========================================= [ Dicom ] =========================================
$('#taDicom').click(function() {
    Busqueda_multipleDICOM();
});
// =============================================================================================


// ======================================== [ SELECCIONAR ] ========================================
$('#btnBuscarPlantilla').click(function() {
    $('#vModalBuscadorPlantilla').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Plantilla').click();
});
$('#txtBuscador_Plantilla').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Plantilla').click();
    }
});
$('#btnBuscar_Plantilla').click(function() {
    $.ajax({
        url: 'buscaplantillas_salud' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Plantilla').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Plantilla').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre,
                        plantilla: data[k].plantilla,
                    };
                    generarTuplaPlantilla(arrayParametro);
                }
                $('#txtBuscador_Plantilla').focus();
            }
            else
                $('#tbody_Plantilla').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaPlantilla = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var plantilla = arrayParametro['plantilla'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaPlantilla('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Plantilla').append(
       '<tr ondblclick="eventoDobleClick_Plantilla('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowPlantilla-'+i+'" class="default">'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + '<td id="td_plantilla' + i + '">' + plantilla + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Plantilla = function(id, idtabla) {
    seleccionaPlantilla(id, idtabla);
}
var seleccionaPlantilla = function(id, idtabla) {
    $('#vModalBuscadorPlantilla').modal('toggle');
    var plantilla = $('#td_plantilla' + id).html();
    editor_historial.setData( plantilla );
    
    $('#txtBuscador_Plantilla').val('');
}
// ======================================== [ REGISTRAS LANTILLA ] ========================================
$('#btnRegistrarPlantilla').click(function() {
    $('#vModalRegistrarPlantilla').modal({
      backdrop: 'static',
      keyboard: true
    }); 
});  

$('#btnGuardarPlantilla').click(function() {        
    $.ajax({
        url: 'registrarplantilla_salud' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#nombrePlantilla').val(),
            descripcion: editor_plantilla.getData()
        }, 
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
// ===============================================================================================
 


// ========================================= [ GUARDAR ] =========================================
$('#btnGuardar').click( function(e) {
    // if($('#txtIdhcl_cie').val() == '')
    // {
    //     bootbox.alert('Llene el Diagnóstico, por favor! ');
    //     return;
    // }
    Swal.fire({
                icon: 'warning',
                title: 'Historial Clinico Digital.',
                text: 'Desea guardar la información?', 
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, deseo registrar!'
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Registro completo!', '', 'success')
                     $('#txtHiddenHora').val( $('#txtHora_Minuto_AmPM').val() );
                    //$('#txtDatosJsonBajaMedica').val('');

                    // [ BAJA MÉDICA ]
                    var jsonBajaMedica = '';
                    if($('#txtHiddenBajaMedica').val() == 1)
                    {
                        var datosFila = {};
                        datosFila.fechaDesde = $('#fechaDesde').val();
                        datosFila.fechaHasta = $('#fechaHasta').val();
                        datosFila.txtCantidadBajaMedica = $('#txtCantidadBajaMedica').val();
                        datosFila.txtIdHcl_variables_bajas = $('#txtIdHcl_variables_bajas').val();
                        datosFila.descripcion = $('#taDescripcion_formato_historia').val();

                        if(datosFila.fechaDesde == '' || datosFila.fechaHasta == '' || 
                           datosFila.txtCantidadBajaMedica == '' || datosFila.txtIdHcl_variables_bajas == 0)
                        {
                            $('#btnBajaMedica').click();
                            $('#textoValidacion_BajaMedica').css({visibility: 'visible'});
                            return;
                        }
                        else
                            jsonBajaMedica = JSON.stringify(datosFila);
                    }
                    
                    // [ VARIABLES SIGNOS VITALES ]
                    var arrayFilaDatosTabla = [];
                    var editadoVariablesSignosVitales = 0;
                    $("#tContenedor_Variables_signos_vitales tbody tr").each(function (index) 
                    {
                        if($(this).attr('id') != undefined)
                        {
                            var identificador = ($(this).attr('id')).split('-')[1];

                            var datosFila = {};
                            // alert($('#tdIdHcl_variables_signos_vitales' + identificador).text());
                            datosFila.idhcl_variables_signos_vitales = $('#tdIdHcl_variables_signos_vitales' + identificador).text();
                            datosFila.variable = $('#tdHiddenVariable' + identificador).text();
                            datosFila.contenido_variable = $('#txtValor-' + identificador).val();
                            datosFila.unidad = $('#tdUnidad' + identificador).text();
                            
                            if(datosFila.contenido_variable != '')
                                editadoVariablesSignosVitales +=1;

                            arrayFilaDatosTabla.push(datosFila);
                        }
                    }); 

                    var Descripcion_CertificadoMedico = '';
                    if($('#txtHiddenCertificadoMedico').val() == 1)                        
                        Descripcion_CertificadoMedico = editor_certificado.getData();
                        

                    // DATOS EXTRAS
                    var datoExtra = {};
                    // datoExtra.hospitalizado = $("#checkHospitalizado").is(':checked')? 1 : 0;
                    datoExtra.hospitalizado = $("#checkHospitalizado").is(':checked')? 1 : 0;
                    datoExtra.embarazada = $("#checkEmbarazo").is(':checked')? 1 : 0;
                    datoExtra.emergencia = $("#checkEmergencia").is(':checked')? 1 : 0;
                    datoExtra.seguridad = $("#checkSeguridad").is(':checked')? 1 : 0;
                    datoExtra.observaciones_imagenologia = observacion_ima;
                    datoExtra.observaciones_laboratorio = observacion_lab;
                    datoExtra.Descripcion_CertificadoMedico = Descripcion_CertificadoMedico; // Certificado Médico
                    datoExtra.editadoVariablesSignosVitales = editadoVariablesSignosVitales;
                    datoExtra.idsexo = idsexo;
                    datoExtra.diagnostico_seguro = $("#checkDiagnostico_seguro").is(':checked')? 1 : 0;
                    datoExtra.respuesta_seguro = $("#checkRespuesta_seguro").is(':checked')? 1 : 0;
                    
                    // [ DATOS A ENVIAR PARA GUARDAR... ]
                    var datosFila = {};
                    datosFila.datosTablaLab = Array.from(new Set(datosTablaLab)); // Laboratorio
                    datosFila.datosTabla = Array.from(new Set(datosTabla));       // Imagenología
                    datosFila.jsonBajaMedica = jsonBajaMedica;                    // Baja Médica
                    datosFila.jsonVariableSignosVitales = JSON.stringify(arrayFilaDatosTabla);
                    datosFila.jsonDatoExtra = JSON.stringify(datoExtra);
                    datosFila.datosDiagnostico = Array.from(new Set(datosDiagnostico));
                    $('#txtHiddenJSONdatos').val( JSON.stringify(datosFila) );


                    setearDatosParaGuardarPrescripcion();
                    setearDatosParaGuardarDiagnosticos();
                    setearDatosParaGuardarReferencia();
                    setearDatosParaGuardarInternacion();
                    $("#form").submit();
                }  
            })

    
    // bootbox.confirm('Desea guardar la información? ', function (confirmed) {
    //     if (confirmed) 
    //     {
    //         $('#txtHiddenHora').val( $('#txtHora_Minuto_AmPM').val() );
    //         //$('#txtDatosJsonBajaMedica').val('');

    //         // [ BAJA MÉDICA ]
    //         var jsonBajaMedica = '';
    //         if($('#txtHiddenBajaMedica').val() == 1)
    //         {
    //             var datosFila = {};
    //             datosFila.fechaDesde = $('#fechaDesde').val();
    //             datosFila.fechaHasta = $('#fechaHasta').val();
    //             datosFila.txtCantidadBajaMedica = $('#txtCantidadBajaMedica').val();
    //             datosFila.txtIdHcl_variables_bajas = $('#txtIdHcl_variables_bajas').val();
    //             datosFila.descripcion = $('#taDescripcion_formato_historia').val();

    //             if(datosFila.fechaDesde == '' || datosFila.fechaHasta == '' || 
    //                datosFila.txtCantidadBajaMedica == '' || datosFila.txtIdHcl_variables_bajas == 0)
    //             {
    //                 $('#btnBajaMedica').click();
    //                 $('#textoValidacion_BajaMedica').css({visibility: 'visible'});
    //                 return;
    //             }
    //             else
    //                 jsonBajaMedica = JSON.stringify(datosFila);
    //         }
            
    //         // [ VARIABLES SIGNOS VITALES ]
    //         var arrayFilaDatosTabla = [];
    //         var editadoVariablesSignosVitales = 0;
    //         $("#tContenedor_Variables_signos_vitales tbody tr").each(function (index) 
    //         {
    //             if($(this).attr('id') != undefined)
    //             {
    //                 var identificador = ($(this).attr('id')).split('-')[1];

    //                 var datosFila = {};
    //                 // alert($('#tdIdHcl_variables_signos_vitales' + identificador).text());
    //                 datosFila.idhcl_variables_signos_vitales = $('#tdIdHcl_variables_signos_vitales' + identificador).text();
    //                 datosFila.variable = $('#tdHiddenVariable' + identificador).text();
    //                 datosFila.contenido_variable = $('#txtValor-' + identificador).val();
    //                 datosFila.unidad = $('#tdUnidad' + identificador).text();
                    
    //                 if(datosFila.contenido_variable != '')
    //                     editadoVariablesSignosVitales +=1;

    //                 arrayFilaDatosTabla.push(datosFila);
    //             }
    //         }); 

    //         var Descripcion_CertificadoMedico = '';
    //         if($('#txtHiddenCertificadoMedico').val() == 1)
    //             Descripcion_CertificadoMedico = $('#taDescripcion_CertificadoMedico').val();

    //         // DATOS EXTRAS
    //         var datoExtra = {};
    //         // datoExtra.hospitalizado = $("#checkHospitalizado").is(':checked')? 1 : 0;
    //         datoExtra.hospitalizado = $("#checkHospitalizado").is(':checked')? 1 : 0;
    //         datoExtra.embarazada = $("#checkEmbarazo").is(':checked')? 1 : 0;
    //         datoExtra.emergencia = $("#checkEmergencia").is(':checked')? 1 : 0;
    //         datoExtra.seguridad = $("#checkSeguridad").is(':checked')? 1 : 0;
    //         datoExtra.observaciones_imagenologia = observacion_ima;
    //         datoExtra.observaciones_laboratorio = observacion_lab;
    //         datoExtra.Descripcion_CertificadoMedico = Descripcion_CertificadoMedico; // Certificado Médico
    //         datoExtra.editadoVariablesSignosVitales = editadoVariablesSignosVitales;
    //         datoExtra.idsexo = idsexo;
    //         datoExtra.diagnostico_seguro = $("#checkDiagnostico_seguro").is(':checked')? 1 : 0;
    //         datoExtra.respuesta_seguro = $("#checkRespuesta_seguro").is(':checked')? 1 : 0;
            
    //         // [ DATOS A ENVIAR PARA GUARDAR... ]
    //         var datosFila = {};
    //         datosFila.datosTablaLab = Array.from(new Set(datosTablaLab)); // Laboratorio
    //         datosFila.datosTabla = Array.from(new Set(datosTabla));       // Imagenología
    //         datosFila.jsonBajaMedica = jsonBajaMedica;                    // Baja Médica
    //         datosFila.jsonVariableSignosVitales = JSON.stringify(arrayFilaDatosTabla);
    //         datosFila.jsonDatoExtra = JSON.stringify(datoExtra);
    //         datosFila.datosDiagnostico = Array.from(new Set(datosDiagnostico));
    //         $('#txtHiddenJSONdatos').val( JSON.stringify(datosFila) );


    //         setearDatosParaGuardarPrescripcion();
    //         setearDatosParaGuardarDiagnosticos();
    //         setearDatosParaGuardarReferencia();
    //         setearDatosParaGuardarInternacion();
    //         $("#form").submit();
    //     }
    // });
});

var setearDatosParaGuardarReferencia = function() {
    var datosTablaReferencia = [];
    var valor = $('#txtHiddenReferenciaMedica').val();
    // alert(valor);
    if (valor == 1) 
    {
        var datosFila = {};        
        datosFila.idAlmacen = $('#txtIdAlmacen').val();
        datosFila.idEspecialidad = $('#txtIdHcl_cuaderno').val();
        datosFila.examen_clinico = $('#taDescripcion_examen_clinico').val();
        datosFila.tratamiento_inicial = $('#taDescripcion_tratamiento_inicial').val(); 
        datosFila.recomendaciones = $('#taDescripcion_recomendaciones').val();  
        
        datosTablaReferencia.push(datosFila);             
        $('#txtDatosJsonReferenciaMedica').val(JSON.stringify(datosTablaReferencia)); 
    }     
}

var setearDatosParaGuardarInternacion = function() {
    var datosTablaInternacion = [];
    var valor = $('#txtHiddenInternacionMedica').val();
    //alert(valor); 
    if (valor == 1) 
    {
        var datosFila = {};        
        datosFila.idAlmacen = $('#txtIdAlmacenInternacion').val();
        datosFila.idEspecialidad = $('#txtIdHcl_cuadernoInternacion').val();
        datosFila.familiar_datos = $('#txtFamiliar_datos').val(); 
        datosFila.familiar_movil = $('#txtFamiliar_movil').val();  
        datosFila.familiar_domicilio = $('#txtFamiliar_domicilio').val();  
        datosFila.familiar_descripcion = $('#taDescripcion_familiar').val(); 
        datosFila.recomendaciones = $('#taRecomendacion_medica').val(); 
        datosFila.hora_registro =  $('#txtHora_Minuto_AmPM').val(); 
        datosTablaInternacion.push(datosFila);             
        $('#txtDatosJsonInternacionMedica').val(JSON.stringify(datosTablaInternacion)); 
    }     
}

var setearDatosParaGuardarPrescripcion = function() {
    var datosTablaPrescripcion = [];
    $("#tablaContenedorListaRecetas tbody tr").each(function (index) 
    {
        if($(this).attr('id') != undefined)
        {
            var identificador = ($(this).attr('id')).split('-')[1]; 
            var idfar_cab_detalle = parseInt($('#tdIdfar_cab_detalle_' + identificador).text());  
            if(idfar_cab_detalle == 0)
            {      
                var datosFila = {}; 
                datosFila.Idproducto = $('#tdIdinv_articulo_Dispensar_' + identificador).text();
                datosFila.codigo = $('#tdCodigo_Dispensar_' + identificador).text();
                datosFila.nombre = $('#tdDescripcion_Dispensar_' + identificador).text();
                datosFila.cantidad = $('#tdCantidad_Dispensar_' + identificador).text(); 
                datosFila.unidad = $('#tdUnidad_Dispensar_' + identificador).text(); 
                datosFila.indicacion = $('#tdIndicacion_Dispensar_' + identificador).text(); 
                
                datosTablaPrescripcion.push(datosFila); 
            }            
        }
    });  
    $('#txtcontenidoTablaPrescripcion').val(JSON.stringify(datosTablaPrescripcion)); 
}

var setearDatosParaGuardarDiagnosticos = function() {
    var datosTablaDiagnosticos = [];
    $("#tablaContenedorDiagnostico tbody tr").each(function (index) 
    {
        if($(this).attr('id') != undefined)
        {
            var datosFila = {};
            var identificador = ($(this).attr('id')).split('-')[1];
            datosFila.IdCie = $('#tdIdhcl_cie' + identificador).text();  
            datosTablaDiagnosticos.push(datosFila);  
        }
    });   
    $('#txtcontenidoTablaDiagnosticos').val(JSON.stringify(datosTablaDiagnosticos));  
}

// ===============================================================================================
 

// ========================================= [ REPORTE ] =========================================
// $("table").delegate("button", "click", function() {
var imprimirSolicitudes = function(id) {   
    idhcl_cabecera_registro = id; 
    $.ajax({
        url: 'hcl_cabecera_registro/verifica_opciones_reporte',
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: idhcl_cabecera_registro
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.receta > 0)
                $('#btnPrint_Receta').show();
            else
                $('#btnPrint_Receta').hide();

            if(data.laboratorio == 1)
                $('#btnPrint_Laboratorio').show();
            else
                $('#btnPrint_Laboratorio').hide();

            if(data.imagenologia == 1)
                $('#btnPrint_Imagenologia').show();
            else
                $('#btnPrint_Imagenologia').hide();

            if(data.certificado_medico == 1)
                $('#btnPrint_CertifMedico').show();
            else
                $('#btnPrint_CertifMedico').hide();

            if(data.baja_medica == 1)
                $('#btnPrint_BajaMedica').show();
            else
                $('#btnPrint_BajaMedica').hide();

            if(data.referencia_medica == 1)
                $('#btnPrint_ReferenciaMedica').show();
            else
                $('#btnPrint_ReferenciaMedica').hide();

            if(data.internacion_medica != null)
                $('#btnPrint_InternacionMedica').show();
            else
                $('#btnPrint_InternacionMedica').hide();

            $('#vImprimeDetalleAtencion').modal({ backdrop: 'static', keyboard: false });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
};
$('#btnPrint_Salud').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_Salud/' + id,
        css: {width: '100%', height: '100%'}
    }));
});
$('#btnPrint_Receta').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_Receta/' + id,
        css: {width: '100%', height: '100%'}
    }));
});
$('#btnPrint_Laboratorio').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_Laboratorio/' + id,
        css: {width: '100%', height: '100%'}
    }));
});
$('#btnPrint_Imagenologia').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_Imagenologia/' + id,
        css: {width: '100%', height: '100%'}
    }));
});
$('#btnPrint_CertifMedico').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_CertificadoMedico/' + id,
        css: {width: '100%', height: '100%'}
    }));
});
$('#btnPrint_BajaMedica').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_BajaMedica/' + id,
        css: {width: '100%', height: '100%'}
    }));
});

$('#btnPrint_ReferenciaMedica').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_ReferenciaMedica/' + id,
        css: {width: '100%', height: '100%'}
    }));
});

$('#btnPrint_InternacionMedica').click(function() {
    var id = idhcl_cabecera_registro;
    $('#secccionImprimeDetalleAtencion').html("").append($("<iframe></iframe>", {
        src: 'hcl_cabecera_registro/imprime_InternacionMedica/' + id,
        css: {width: '100%', height: '100%'}
    }));
});
// ===============================================================================================