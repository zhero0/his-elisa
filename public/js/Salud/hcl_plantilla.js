$(document).ready(function() {
    
});
  
// ========================================= [ GUARDAR ] =========================================
$('#btnGuardar').click( function(e) {
	Swal.fire({
                icon: 'warning',
                title: 'Plantilla medica.',
                text: 'Desea guardar la información?', 
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, deseo registrar!'
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Registro completo!', '', 'success')
                    
                    // $('#txtJsonExamenes').val('');
                    $("#form").submit();
                }  
            })  
});
// ===============================================================================================
 