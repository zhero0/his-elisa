var url = '';
var i = 0;
var datosTabla = [];
var totalExamenSeleccion = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var SOLICITUD = 1;
var ANULADO = 3; 
$(document).ready(function() { 
    $('#divErrorPaciente').hide();
    $('#divErrorExamen').hide(); 
    if($('#txtScenario').val() == 'index')
    {
        $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini');
    }
    
    if($('#txtScenario').val() == 'create'){
        $('#btnAnular').attr('disabled','true');
        url = 'Create';
    }

    if($('#txtScenario').val() == 'edit')
    {    
        $('#txtHora_Minuto_AmPM').val( $('#txtHora_Programado').val() ); 
    }


    // if($('#txtScenario').val() == 'index')
    // {
    //     // "BUSCADOR MULTIPLE"
    //     var inputBuscador = '';
    //     $("#indexSearch input").each(function(e) {
    //         if(inputBuscador == '')
    //             inputBuscador += '#' + this.id;
    //         else
    //             inputBuscador += ', #' + this.id;
    //     });
    //     $(inputBuscador).keypress(function(e) {
    //         if(e.which == 13) {
    //             var ultimo_id_presionado = $(this).attr('id');
    //             Busqueda_index(ultimo_id_presionado);
    //         }
    //     });
    //     if($('#datosBuscador').val() != '') 
    //     {
    //         var buscador = JSON.parse($('#datosBuscador').val());

    //         $('#' + buscador.ultimo_id_presionado).select();
    //     }
    //     // FIN "BUSCADOR MULTIPLE"
    // }

});

// ==================================== [ BUSCADOR MULTIPLE ] ====================================
// var Busqueda_index = function(ultimo_id_presionado) {
//     var dato = {};
//     $("#indexSearch :input").each(function(e) {
//         var id = this.id;
//         var variable = (id).split('Search_')[1];
        
//         eval("dato." + variable + " = '" + this.value + "' ");
//     });
//     dato.ultimo_id_presionado = ultimo_id_presionado;
//     $('#datosBuscador').val( JSON.stringify(dato) );
//     $('#formBuscador').submit();
// }
// ===============================================================================================



// ========================================= [ PACIENTE ] =========================================
$('#txtBuscarInternado').keypress(function(e) {
    if(e.which == 13) {
        $('#btnModalPaciente').click();        
    }    
});
$('#txtMatricula').keypress(function(e) {
    if(e.which == 13) {
        $('#btnModalPaciente').click();        
    }    
});
$('#btnModalPaciente').click(function() {     
    $.ajax({
        url: 'buscaInternado' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            datoPersona: $('#txtBuscarInternado').val(),
            datoMatricula: $('#txtMatricula').val()
        },

        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);

            if(data.length == 1)
            {
                $('#txtidcabecera').val(data[0].res_idhcl_cabecera_registro);
                $('#datoNumeroPaciente').text(data[0].res_hcl_poblacion_matricula);
                $('#datoNombrePaciente').text(data[0].res_hcl_poblacion_nombrecompleto);
                $('#datoDiagnosticoPaciente').text(data[0].res_diagnostico);
                $('#datoServicio_solicitante').text(data[0].res_nombre);
                $('#datoMedico_solicitante').text(data[0].res_nombres+' '+data[0].res_apellidos);
                $('#datoRegistro').text(data[0].res_idhcl_cabecera_registro);                
            }
            else
            {
                $('#txtBuscadorInternado').val( $('#txtBuscarInternado').val() );
                $('#btnBuscarPacienteInternado').click();

                $('#vModalBuscadorInternado').modal({
                  backdrop: 'static',
                  keyboard: true
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            return('error! ');
        }
    });
   
});
$('#txtBuscadorInternado').keypress(function(e) {
    if(e.which == 13) {
         $('#btnBuscarPacienteInternado').click();
     }
});
$('#btnBuscarPacienteInternado').click(function() { 
    $.ajax({
        url: 'buscaInternado' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            datoPersona: $('#txtBuscadorInternado').val(),
            datoMatricula: ''
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyPaciente').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        numero_historia: data[i].res_hcl_poblacion_matricula,
                        nombre: data[i].res_hcl_poblacion_nombrecompleto,
                        hcl_cabecera_registro: data[i].res_idhcl_cabecera_registro,
                        especialidad:data[i].res_nombre,
                        medico_nombre: data[i].res_nombres,
                        medico_apellidos: data[i].res_apellidos
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                //$('#txtBuscadorPaciente').focus();
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaPaciente = function(arrayParametro) {  
    var id = arrayParametro['id'];
    var numero_historia = arrayParametro['numero_historia'];
    var nombre = arrayParametro['nombre']; 
    var registro = arrayParametro['hcl_cabecera_registro']; 
    var especialidad = arrayParametro['especialidad']; 
    var medico = arrayParametro['medico_nombre']+' '+arrayParametro['medico_apellidos']; 
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClick('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td style="display: none;" id="tdEspecialidad' + i + '">' + especialidad + '</td>'
          + '<td style="display: none;" id="tdMedico' + i + '">' + medico + '</td>'
          + '<td id="tdNumeroRegistro' + i + '">' + registro + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>' 
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick = function(id, idtabla) { 
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) {
    $('#vModalBuscadorInternado').modal('toggle');
    $('#datoNumeroPaciente').text( $('#tdNumeroHistoria' + id).text() );
    $('#datoNombrePaciente').text( $('#tdNombrePaciente' + id).text() );
    $('#txtidcabecera').val( $('#tdNumeroRegistro' + id).text() ); 
    $('#datoServicio_solicitante').text( $('#tdEspecialidad' + id).text() ); 
    $('#datoMedico_solicitante').text( $('#tdMedico' + id).text() ); 
    $('#datoRegistro').text( $('#tdNumeroRegistro' + id).text() );                
    $('#txtMatricula').val('');
    $('#txtBuscarInternado').val('');
}


$('#idServicio').click(function() {  
    if ($('#idServicio').val() !=  null) {
        ajaxMuestraSala();     
    };    
    
});

var ajaxMuestraSala = function() {

    $.ajax({
        url: 'muestraSala' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#idServicio').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#idSala').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {                    
                    var arrayParametro = {
                        id: data[j].id,
                        nombre: data[j].nombre, 
                    };
                    generarListaSalas(arrayParametro);
                }              
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
} 

var generarListaSalas = function(arrayParametro) {
    var id = arrayParametro['id'];    
    var nombre = arrayParametro['nombre']; 
    
    $('#idSala').append(
        '<option value="' + id + '" id = "' + id + '">'+ nombre +'</option>'        
    ); 
}

$('#idSala').click(function() {   
    if ($('#idSala').val() !=  null) {
        ajaxMuestraCama();    
    };   
      
});

var ajaxMuestraCama = function() {
    $.ajax({
        url: 'muestraCama' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#idSala').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#idCamas').empty();

            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {                    
                    var arrayParametro = {
                        id: data[j].id,
                        nombre: data[j].numero, 
                    };
                    generarListaCamas(arrayParametro);
                }              
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
} 

var generarListaCamas = function(arrayParametro) {
    var id = arrayParametro['id'];    
    var nombre = arrayParametro['nombre']; 
    
    $('#idCamas').append(
        '<option value="' + id + '" id = "' + id + '">'+ nombre +'</option>'        
    ); 
}
// ================================================================================================



// ========================================== [ DATOS ] ==========================================
// [ Especialidad ]
$('#btnModal_Especialidad').click(function() {
    $('#vModalBuscadorEspecialidad').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Especialidad').click();
});
$('#txtBuscador_Especialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Especialidad').click();
    }
});
$('#btnBuscar_Especialidad').click(function() {
    $.ajax({
        url: 'buscaEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Especialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscador_Especialidad').focus();
            }
            else
                $('#tbody_Especialidad').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaEspecialidad('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr ondblclick="eventoDobleClick_Especialidad('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick +'" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idhcl_cuaderno' + i + '">' + id + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Especialidad = function(id, idtabla) {
    seleccionaEspecialidad(id, idtabla);
}
var seleccionaEspecialidad = function(id, idtabla) {
    $('#vModalBuscadorEspecialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_nombre' + id).text() );
    $('#idhcl_cuaderno').val( idtabla );
    $('#txtBuscador_Especialidad').val('');
}

// [ Médico Solicitante ]
$('#btnModal_Medico').click(function() {
    $('#vModalBuscadorMedicos').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Medico').click();
});
$('#txtBuscador_Medico').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Medico').click();
    }
});
$('#btnBuscar_Medico').click(function() {
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Medico').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medico').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Medico').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr ondblclick="eventoDobleClick_Medico('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Medico = function(id, idtabla) {
    seleccionaMedico(id, idtabla);
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscadorMedicos').modal('toggle');
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtMedico').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Medico').val('');
}
// ===============================================================================================



// ========================================= [ EXÁMENES ] =========================================
$('#txtBuscarExamen').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarExamen').click();
    }
});
$('#btnBuscarExamen').click(function() {
    var valor = $('#txtBuscarExamen').val();
    ajaxExamen(valor, 1);
    $('#txtBuscarExamen').focus();
});
$('#btnVisualizaExamenesSeleccionados').click(function() {
    ajaxExamen('', 0);
    $('#txtBuscarExamen').val('');
    $('#txtBuscarExamen').focus();
});
var ajaxExamen = function(valor, buscarTodo) { 
    $.ajax({
        url: 'buscaExamenes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            buscarTodo: buscarTodo,
            datosLab_Ima: JSON.stringify(datosTabla)
        },
        success: function(dato) {
            $('#tbodyExamenes').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);

            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        idima_examenes: data[i].idima_examenes,
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        idEspecialidad: data[i].idEspecialidad,
                        codigoEspecialidad: data[i].codigoEspecialidad,
                        nombreEspecialidad: data[i].nombreEspecialidad
                    };
                    generarListaExamenes(arrayParametro);
                }

                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                seleccionarExamenesNuevamente();
            }
            else
                $('#tbodyExamenes').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaExamenes = function(arrayParametro) {
    var idima_examenes = arrayParametro['idima_examenes'];
    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var idEspecialidad = arrayParametro['idEspecialidad'];
    var codigoEspecialidad = arrayParametro['codigoEspecialidad'];
    var nombreEspecialidad = arrayParametro['nombreEspecialidad'];

    var checkbox = '<div class="form-check">'
                    +   '<input type="checkbox" class="form-check-input" id="checkSeleccionar' + i + '">'
                    +   ' <label class="form-check-label" for="checkSeleccionar' + i + '" style="cursor: pointer;">'
                    +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                    +   ' </label>'
                 +  '</div>';
    
    $('#tbodyExamenes').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdima_examenes' + i + '">' + idima_examenes + '</td>'
          + '<td style="display: none;" id="tdIdEspecialidad' + i + '">' + idEspecialidad + '</td>'
          + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td id="tdEspecialidad' + i + '">' + nombreEspecialidad + ' (' + codigoEspecialidad + ')' + '</td>'
          + '<td>' + checkbox + '</td>'
       +'</tr>'
    );

    $('#checkSeleccionar' + i).click(function() {
        examenesSeleccionados();

        if($(this).is(':checked'))
            totalExamenSeleccion++;
        else
            totalExamenSeleccion--;

        $('#spanTotalExamenSeleccion').text(totalExamenSeleccion);
        $('#txtBuscarExamen').focus();
    });

    i++;
}
var examenesSeleccionados = function() {
    $("#tablaContenedorExamenes tbody tr").each(function (index) 
    {
        var identificador = ($(this).attr('id')).split('-')[1];

        if($.isNumeric(identificador))
        {
            var Idima_examenes = $('#tdIdima_examenes' + identificador).text();
            if($('#checkSeleccionar' + identificador).is(':checked'))
                datosTabla.push(Idima_examenes);
            else
                datosTabla = jQuery.grep(datosTabla, function(value) {
                  return value != Idima_examenes;
                });
        }
    });
}
var seleccionarExamenesNuevamente = function() {
    datosTabla = Array.from(new Set(datosTabla));
    if(datosTabla.length > 0)
    {
        for(i = 0; i < datosTabla.length; i++)
        {
            $("#tablaContenedorExamenes tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                if($.isNumeric(identificador))
                {
                    if($('#tdIdima_examenes' + identificador).text() == datosTabla[i])
                    {
                        $('#checkSeleccionar' + identificador).prop('checked', true);
                        return;
                    }
                }
            });
        }
    }
}
// ================================================================================================



// ====================================== [ ESPECIALIDADES ] ======================================
$('#btnActualizaEspecialidad').click(function() {
    $.ajax({
        url: 'buscaProgramados' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: 77
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if(data.length > 0)
            {
                $('#divInformacionProgramados').empty();
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        total: data[i].total
                    };
                    InformacionProgramados(arrayParametro);
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var InformacionProgramados = function(arrayParametro) {
    var myArray = ['aqua', 'blue', 'orange'];
    var randomItem = myArray[Math.floor(Math.random()*myArray.length)];

    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var total = arrayParametro['total'];

    $('#divInformacionProgramados').append(
        '<div class="col-xs-4">' +
           '<div class="info-box" style="border: 1px solid #bbb2b2;">' + 
                '<span class="info-box-icon bg-' + randomItem + '">' + 
                    '<h1>' + total + '</h1>' + 
                '</span>' + 
                '<div class="info-box-content" style="font-size: 13px;">' + 
                    '(' + codigo + ') ' + nombre + 
                '</div>' + 
            '</div>' +
        '</div>'
    );
    i++;
}
// ================================================================================================

// ========================================= [ ANULAR ] =========================================
$('#btnAnular').click( function(e) {
    bootbox.confirm('Desea anular la información? ', function (confirmed) {
        if (confirmed) {
            $('#txtEstado').val(ANULADO);
            
            $("#form").submit();
        }
    }); 
});
// ===============================================================================================

// ========================================= [ GUARDAR ] =========================================
$('#btnGuardarExamen').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {
            $('#txtJsonExamenes').val('');
            $('#txtEstado').val(SOLICITUD);
            
            $('#txtHiddenHora').val( $('#txtHora_Minuto_AmPM').val() );
            if($('#txtidhcl_poblacion').val() == '')
            {
                $('#divErrorPaciente').show();
                return;
            }
            $("#form").submit();
        }
    });
});
// ===============================================================================================



// ========================================= [ REPORTE ] =========================================
$("table").delegate("button", "click", function() {
    var id = $(this).attr('id');
    $('#vImprimeRecepcion').modal({ backdrop: 'static', keyboard: false });
    
    $('.progress').show();
    $('#secccionImprimeRecepcion').html("").append($("<iframe></iframe>", {
        src: 'int_recepcion/imprimeRecepcion/' + id,
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
});
// ===============================================================================================