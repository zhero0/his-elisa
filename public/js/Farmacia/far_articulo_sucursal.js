var datosTablaArt = [];
var url = '';
$(document).ready(function() { 
    if($('#txtScenario').val() == 'create')
        url = 'Create';
});  
var guardar = function() {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) { 
            $('#datosTablaArt').val(JSON.stringify(datosTablaArt));
            $("#form").submit();
        }
    });
} 
//  ---------------->> "ARTICULOS"
var buscadorArticulos = function(e) { 
    searchArticulo(); 
}
var searchArticulo = function() {  
    $('#tbodyArticulos').addClass('loadingContent');  
    $.ajax({ 
        url: 'buscaArticulo' + url,
        type: 'POST',
        data: {
            _token: $('#token').val(),
            sucursal: $('#sucursal').val(),
            clasificacion: $('#clasificacion').val()
        },
        success: function(dato) {
            setTimeout(function() {
                $('#tbodyArticulosFar').removeClass('loadingContent');
                $('._articulos').html(dato.viewArticulo); 
                // _seleccionaExamenesNuevamente('Ima', datosTablaArt); 
            }, 300);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
function searchPaginacionArticulo(id_sucursal,id_clasificacion,id_token,id_pagina){ 

    //alert(id_sucursal+'-'+id_clasificacion+'-'+id_token+'-'+id_pagina);
    $('#tbodyArticulos').addClass('loadingContent');  
    $.ajax({ 
        url: 'buscaArticulo' + url,
        type: 'POST',
        data: {
            _token: id_token,
            sucursal: id_sucursal,
            clasificacion: id_clasificacion,            
            pagina:id_pagina
        },
        success: function(dato) {
            setTimeout(function() {
                $('#tbodyArticulosFar').removeClass('loadingContent');
                $('._articulos').html(dato.viewArticulo); 
            }, 300);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}

var checkSeleccionTodoArticulos = function(checked, modulo) {
    var respuesta = 0;

    if(checked)
        respuesta = 1;

    if(respuesta == 1)
        seleccionarTodo_ListaArticulos(1, modulo);
    else
        seleccionarTodo_ListaArticulos(0, modulo);
}

var seleccionarTodo_ListaArticulos = function(seleccionar, modulo) {
    $('#tbodyArticulos' + modulo + ' tr').each(function(index) {
        var identificador = ($(this).attr('id')).split('-');
        if(seleccionar == 1)
            $('#check_' + identificador[1]).prop('checked', true);
        else
            $('#check_' + identificador[1]).prop('checked', false);
    });

    articulosSeleccionadosFar();
}
var articulosSeleccionadosFar = function() {
    $("#tbodyArticulosFar tr").each(function() 
    {
        var id = ($(this).attr('id')).split('-')[1]; 
        
        if($.isNumeric(id))
        {
            if($('#check_' + id).is(':checked'))
                datosTablaArt.push(id);
            else
            {
                datosTablaArt = jQuery.grep(datosTablaArt, function(value) {
                    return value != id;
                });
            }
        }
    });

    datosTablaArt = Array.from(new Set(datosTablaArt));
    $('#spanTotalExamenSeleccionFar').text(datosTablaArt.length);
}
//  ---------------->> FIN "ARTICULOS" 

var modificarDatos = function(dato) {
    $('#vModalArticulo').modal('toggle');
     $('#codificacion_nacional').val('hola');
}