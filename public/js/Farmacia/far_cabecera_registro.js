var url = '';
var i = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! '; 
var cantidad_datos = 0;
var ola = '';
$( document ).ready(function() {
    $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini'); 
    url = 'Create'; 
    if($('#txtScenario').val() == 'index')
    {
        $('#btnImprimirDetalle').click(function() {
            var parametro = {};
            parametro.fechaDesde = '20-11-2018';
            parametro.fechaHasta = '22-11-2018';
            
            $('#seccionImprimeInformeDetalle').html("").append($("<iframe></iframe>", {
                src: 'notaIngreso/imprimeInformeIngresos/' + JSON.stringify(parametro),
                css: {width: '0', height: '0'}
            }));
        });
    }
    
    if($('#txtScenario').val() == 'view')
    {
        $("#form :input").prop("disabled", true);
    }
    
    if($('#txtScenario').val() == 'view' || $('#txtScenario').val() == 'edit')
    {
        url = '';
        
        var prov = $('#arrayProveedor').val(); 
        prov = jQuery.parseJSON(prov);
        $('#txtHiddenIdProveedor').val( prov.id );
        $('#txtBuscaProveedor').val( prov.nombre + ' (' + prov.nit + ')' ); 
        var dato = $('#arrayDatos').val();
        var dato = jQuery.parseJSON(dato);

 
        for(var i = 0; i < dato.length; i++)
        {
            var idTran = dato[i].idfar_cab_registro_detalle;
            var idproducto = dato[i].idproducto;
            var codigo = dato[i].codificacion;
            var nombre = dato[i].nombre;

            
            var cantidad = dato[i].cantidad;
            var precio = dato[i].costo;
            var lote = dato[i].lote;
            var saldo = dato[i].total;
            var fecha_vencimiento = dato[i].fecha_vencimiento;
            var subtotal = dato[i].cantidad * dato[i].costo;
            var parametros = {};
            parametros.idTran = idTran;
            parametros.idproducto = idproducto; 
            parametros.nombre = '('+codigo+') - '+ nombre;
            parametros.cantidad = cantidad;
            parametros.precio = precio;
            parametros.lote = lote; 
            parametros.fecha_vencimiento = fecha_vencimiento;
            parametros.subtotal = saldo;
            camposListaProductos(parametros,2);
        }
    }
});

// ========================================= [ BUSCADOR PROVEEDOR ] =========================================
$('#btnBuscadorProveedor').click(function() {
    $('#modalBuscaProveedor').modal({ backdrop: 'static', keyboard: false });
    $('#btnBuscarProveedor').click();
});
$('#btnBuscarProveedor').click(function() {

    $.ajax({
        url: 'buscaProveedor' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscarProveedor').val(),
        },
        success: function(dato) {
            $('#tbody_Proveedor').empty();
            var dato = jQuery.parseJSON(dato);
            
            if(dato.length == 0)
                $('#tbody_Proveedor').append('<h2 style="color: red;">LISTA VACIA...</h2>');
            else
            {
                for(var i = 0; i < dato.length; i++)
                {
                    var nit = dato[i].nit > 0? dato[i].nit : '';
                    $('#tbody_Proveedor').append(
                       '<tr ondblclick="eventoDobleClick_Proveedor('+i+', ' + dato[i].id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row'+i+'">' +
                            '<td style="display: none;" id="tdId_proveedor' + i + '">' + dato[i].id + '</td>' + 
                            '<td class="col-xs-2" id="tdNit_proveedor' + i + '">' + nit + '</td>' + 
                            '<td id="txtNombres_proveedor' + i + '">' + dato[i].nombre + '</td>' +
                            '<td class="col-xs-4" id="tdDepartamento_proveedor' + i + '">' + dato[i].departamento + '</td>' + 
                            '<td class="col-xs-1">' + 
                                "<button type='button' onclick='seleccionaProveedor("+i+", "+ dato[i].id+")' class='btn btn-success btn-sm' title='Pulse Click para Seleccionar! '>" + 
                                  "<span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span>" +
                                "</button>" +
                            '</td>' +
                        '</tr>'
                    );
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var eventoDobleClick_Proveedor = function(id, idtabla) {
    seleccionaProveedor(id, idtabla);
}
var seleccionaProveedor = function(id, idtabla) {
    $('#modalBuscaProveedor').modal('toggle');
    
    var idproveedor = $('#tdId_proveedor' + id).text();    
    var nit = $('#tdNit_proveedor' + id).text();
    var nombre = $('#txtNombres_proveedor' + id).text();

    $('#txtHiddenIdProveedor').val( idproveedor );
    $('#txtBuscaProveedor').val( nombre + ' (' + nit + ')' );

    $('#txtBuscarPaciente').val('');
}
// ==========================================================================================================



// ========================================= [ BUSCADOR PRODUCTO ] =========================================
$('#btnBuscadorProducto').click(function() {
    $('#modalBuscadorProducto').modal({ backdrop: 'static', keyboard: false });
    $('#btnBuscarProducto').click();
});
$('#btnBuscarProducto').click(function() {
    var valor = $('#txtBuscarProducto').val();
    buscadorProducto(valor, 0, 0,$('#sucursal').val(),$('#clasificacion').val());
});
$('#txtBuscarProducto').keyup(function(e) {
    if(e.keyCode == 13)
    {
        buscadorProducto(this.value, 0, 0,$('#sucursal').val(),$('#clasificacion').val());
    }
});

$('#txtValor').keyup(function(e) {
    if(e.keyCode == 13)
        buscadorProducto($('#txtValor').val(), 1, 1,$('#sucursal').val(),$('#clasificacion').val());
});
// $('#txtCantidad').keyup(function(e) {
//     if(e.keyCode == 13)
//     {
//         $('#txtCosto').select();
//     }
// });
$('#txtCantidad').keyup(function(e) {
    if(e.keyCode == 13)
    {
        var parametros = {};
        parametros.codigoBarra = $('#txtHiddenCodigoBarra').val();
        parametros.idproducto = $('#txtHiddenIdProducto').val();
        parametros.nombre = $('#txtValor').val();
        parametros.lote = $('#txtLote').val();
        parametros.fecha_vencimiento = $('#txtFecha_vencimiento').val();
        parametros.cantidad = $('#txtCantidad').val();
        parametros.saldo = $('#txtSaldo').text();
        parametros.subtotal = formatoMiles(parseFloat(parametros.cantidad));
        camposListaProductos(parametros,1);
    }
});
$('#btnAgregarTupla').click(function() {
    // alert('asdasdasd');
    var parametros = {};
    parametros.codigoBarra = $('#txtHiddenCodigoBarra').val();
    parametros.idproducto = $('#txtHiddenIdProducto').val();
    parametros.nombre = $('#txtValor').val();
    parametros.lote = $('#txtLote').val();
    parametros.fecha_vencimiento = $('#txtFecha_vencimiento').val();
    parametros.cantidad = $('#txtCantidad').val();
    parametros.saldo = $('#txtSaldo').val();
    parametros.subtotal = formatoMiles(parseFloat(parametros.cantidad)) ;
    camposListaProductos(parametros,1);
});
$('#btnLimpiarDatos').click(function() {
    $('#txtHiddenCodigoBarra').val('');
    $('#txtHiddenIdProducto').val('');
    $('#txtLote').val('');
    $('#txtFecha_vencimiento').val('');
    $('#txtValor').val('');
    $('#txtCantidad').val(0);
    $('#txtLote').val('');
    $('#txtSaldo').val('0');
    $('#txtValor').focus();
});
var buscadorProducto = function(valor, pantallaPrincipal, verificarExistenciaDatos,sucursal,clasificacion)
{  
    var muestraProductos = 1;
    $.ajax({
        url: 'buscaProducto' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            txtBuscador: valor,
            txtSucursal:sucursal,
            txtClasificacion:clasificacion,
        },
        success: function(dato) {
            var dato = jQuery.parseJSON(dato);
            // ola = dato;

            if(verificarExistenciaDatos == 1) // Verifica si existen datos al buscar en la caja de texto "txtValor"
            {
                if(dato.length == 1)
                {
                    // alert(dato[0].saldo);
                    $('#txtHiddenCodigoBarra').val( dato[0].codigo_barras );
                    $('#txtHiddenIdProducto').val( dato[0].id );
                    $('#txtValor').val( dato[0].descripcion );
                    $('#txtSaldo').val( dato[0].saldo );
                    // alert(dato[0].saldo); 
                    // $('#txtCosto').val( dato[0].precio );
                    $('#txtLote').select();
                    return;
                }
                else
                    $('#modalBuscadorProducto').modal({ backdrop: 'static', keyboard: false });
            }
            mostrarProductos(dato, pantallaPrincipal, valor);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var mostrarProductos = function(dato, pantallaPrincipal, valor)
{
    $('#tbodyProducto').empty();
    
    if(dato == 0)
        $('#tbodyProducto').append('<h2 style="color: red;">LISTA VACIA...</h2>');
    else
    {
        for(var i = 0; i < dato.length; i++)
        {
            var precio = dato[i].preciocompra > 0? dato[i].preciocompra : '';
            $('#tbodyProducto').append(
               '<tr ondblclick="eventoDobleClick_Producto('+i+', ' + dato[i].id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row'+i+'">' +
                    // '<td style="display: none;" id="tdCodigoBarra' + i + '">' + dato[i].codigo_barras + '</td>' + 
                    '<td style="display: none;" id="tdIdproducto' + i + '">' + dato[i].id + '</td>' + 
                    '<td style="display: none;" id="tdProducto_Saldo' + i + '">' + dato[i].saldo + '</td>' + 
                    '<td class="col-xs-2" id="tdCodigo' + i + '">' + dato[i].codificacion + '</td>' + 
                    '<td id="txtNombre' + i + '">' + dato[i].descripcion + '</td>' + 
                    // '<td class="col-xs-1" style="text-align: right;" id="txtPrecio' + i + '">' + precio + '</td>' + 
                    '<td class="col-xs-1">' + 
                        "<button type='button' id='btnAgregarProducto_" + i +"' class='btn btn-success btn-sm' title='Click Para Agregar a la Lista'>" + 
                          "<span class='glyphicon glyphicon-thumbs-up' aria-hidden='true'></span>" +
                        "</button>" +
                    '</td>' +
                '</tr>'
            );
            
            $('#btnAgregarProducto_' + i).click(function() {
                var identificador = (this.id).split('_');
                var id = identificador[1];
                agregarListaProductos(id, 0);
                $('#txtBuscarProducto').focus();
            });
        }
    }
    
    $('#txtBuscarProducto').focus();
    $('#txtBuscarProducto').val(valor);
}
var eventoDobleClick_Producto = function(id, idtabla) {
    agregarListaProductos(id, 0);
}
var agregarListaProductos = function(indice, pantallaPrincipal)
{

    var idproducto = $('#tdIdproducto' + indice).text();
    var codigo = $('#tdCodigo' + indice).text();
    var nombre = $('#txtNombre' + indice).text();
    var lote = $('#txtLote' + indice).text();
    var saldo = $('#tdProducto_Saldo' + indice).text();
    var fecha_vencimiento = $('#txtFecha_vencimiento' + indice).text(); 
    var cantidad = 1;
    // var precio = formatoMiles($('#txtPrecio' + indice).text());
    var subtotal = formatoMiles(parseFloat(cantidad));
    var parametros = {};

    parametros.idproducto = idproducto;
    parametros.codigo = codigo;
    parametros.lote = lote;
    parametros.fecha_vencimiento = fecha_vencimiento; 
    parametros.nombre = '('+codigo+') - '+ nombre;
    parametros.cantidad = formatoMiles(parseFloat(cantidad));
    parametros.saldo = saldo;
    parametros.subtotal = subtotal;

    $('#txtHiddenCodigoBarra').val( $('#tdCodigoBarra' + indice).text() );
    $('#txtHiddenIdProducto').val( idproducto );
    $('#txtValor').val( '('+codigo+') - '+ nombre );
    // $('#txtPrecio').val( precio );
    $('#txtSaldo').val(saldo);
    // $('#txtCosto').val( precio );
    
    $('#txtLote').select();
    $('#modalBuscadorProducto').modal('toggle');
}
var camposListaProductos = function(parametros, tipo) {
    codigoBarra = parametros.codigoBarra > 0? parametros.codigoBarra : '';
    idTran = parametros.idTran >0? parametros.idTran : 0 ;
    idproducto = parametros.idproducto;
    nombre = parametros.nombre;
    lote = parametros.lote;
    fecha_vencimiento = parametros.fecha_vencimiento; 
    nombre = parametros.nombre;

    cantidad = formatoMiles(parametros.cantidad); 
    subtotal = formatoMiles(parametros.subtotal);
    // alert(cantidad);
    $('#divMensajeAdvertencia').hide();

    if(idproducto > 0)
    {
        $('#idListaVacia').remove(); 

        $('#tbody_DocumentoProducto').append(
            '<tr id="row-'+i+'">' +
                '<td style="display: none;" id="principalIdproducto' + i + '">' + idproducto + '</td>' +
                '<td style="display: none;" id="idTran' + i + '">' + idTran+ '</td>' +
                // '<td style="display: none;" id="tipoTransaccion' + i + '">' + tipo + '</td>' +
                '<td id="principalNombre_' + i + '">' + nombre + '</td>' +
                '<td class="col-xs-2" id="principalLote_' + i + '" value="'+lote+'">' + lote + '</td>' + 
                // '<td class="col-xs-1" style="text-align: right;" id="principalFecha_vencimiento_' + i + '">' + fecha_vencimiento + '</td>' +

                '<td class="col-xs-1" style="text-align: right;">'+
                    '<input class="form-control" type="date" value="'+fecha_vencimiento+'" id="txtFecha_vencimiento_' + i + '">'+
                '</td>' +
                // '<td class="col-xs-2" id="principalCantidad_' + i + '" value="'+parametros.cantidad+'">' + parametros.cantidad + '</td>' + 
                '<td class="col-xs-2">' + 
                    '<input type="number" disabled="disabled" class="form-control input-xs" id="principalCantidad_' + i + '" value="' + parametros.cantidad + '">' +
                '</td>' +
                '<td class="col-xs-1" style="text-align: right;" id="principalSubtotal_' + i + '">' + subtotal + '</td>' + 
                '<td class="col-xs-1">' + 
                    "<button type='button' id='btnQuitarProducto_" + i +"' class='btn btn-sm btn-danger' title='Click Para Quitar de la Lista'>" + 
                        "<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>" +
                    "</button>" +
                '</td>' +
            '</tr>'
        );
    }
    else
    {
        $('#divMensajeAdvertencia').show();
        $('#lblMensajeError').text('No es posible añadir el producto a la lista. ');
        return;
    }

    // ================================================================================================
    $('#btnQuitarProducto_' + i).click(function() {
        var identificador = (this.id).split('_');
        var id = identificador[1];

        if (tipo == 2) {            
            var idTran = $('#idTran' + id).text();
            eliminarNota(idTran);
        }
        
        $('#row-' + id).remove();
        $('#txtValor').focus();
        // if($("#tContenedor_Detalleplacasutilizadas tbody tr").length == 1)
        calculaTotal();
    });
    $('#principalCantidad_' + i).click(function() {
        $('#' + this.id).select();
    });
    $('#principalCantidad_' + i).bind('blur keyup change', function() {
        var identificador = (this.id).split('_');
        var id = identificador[1];
        var precio = $('#principalPrecio_' + id).text();
        var subtotal = parseFloat(this.value) * parseFloat(precio);
        $('#principalSubtotal_' + id).text(formatoMiles(subtotal));       
        calculaTotal();
    });

    calculaTotal();
    i++;
    // ================================================================================================

    $('#txtHiddenCodigoBarra').val('');
    $('#txtHiddenIdProducto').val('');
    $('#txtValor').val('');
    $('#txtPrecio').val('');
    $('#txtSaldo').val('');
    $('#txtCantidad').val(0); 
    $('#txtLote').val('');
    $('#txtValor').focus();
}
var formatoMiles = function(numero)
{
    var valor = numero.toString();
    return parseFloat(valor.replace(/,/g, ""))
                .toFixed(2)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
var calculaTotal = function()
{
    var total = 0;
    var contador = 0;
    $("#table_DocumentoProducto tbody tr").each(function(index)
    {
        var id = ($(this).attr('id')).split('-')[1];
        var subtotal = ($('#principalSubtotal_' + id).text()).replace(',', '');
        total = (total.toString()).replace(',', '');
        total = (parseFloat(total) + parseFloat(subtotal)).toFixed(2);
        contador++;
    });

    $('#txtHiddenTotal').val(total);
    $('#txtTotal').text(formatoMiles(total));
    $('#idTotalItem').text(contador);
}

var eliminarNota = function(idTran)
{
    $.ajax({
        url: 'eliminarNotaIngreso' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            opcion: idTran, // el saldo en la tabla "producto" estaba mal...y corrige los movimientos por si acaso...
            // opcion: 2, // este caso ya no deberia..existir..pero será la opcion (2)
        },
        success: function(dato) {
            bootbox.alert(dato);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
// ================================================================================================



// ========================================= [ OPCIONES EXTRAS ] =========================================
$('#txtDescripcion').keyup(function() {
    $('#txtDescripcion_bd').val( this.value );
});
$('#txtMontoPagado').keyup(function(e) {
    var total = parseFloat($('#txtHiddenTotal').val());
    if(e.keyCode == 13)
    {
        var montoPendiente = parseFloat(parseFloat(total) - parseFloat(this.value));
        // alert(total +'>'+ montoPendiente +'&&'+ montoPendiente +'>'+ 0);
        if(parseFloat(this.value) > total)// && montoPendiente > 0)
        {
            bootbox.alert('NO es posible introducir un valor mayor al TOTAL! ');
            $('#spanMontoPendiente').text('0.00');
            this.value = 0;
        }
        else
        {
            $('#spanMontoPendiente').text(montoPendiente);
        }
        $('#txtMontoPagado_bd').val( this.value );
    }
});
$('#corregirKardexProducto').click(function() {
    $.ajax({
        url: 'corregirKardexProductos',
        type: 'post',
        data: {
            _token: $('#token').val(),
            opcion: 1, // el saldo en la tabla "producto" estaba mal...y corrige los movimientos por si acaso...
            // opcion: 2, // este caso ya no deberia..existir..pero será la opcion (2)
        },
        success: function(dato) {
            bootbox.alert(dato);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
// ================================================================================================



// ===================================== [ GUARDAR ] =====================================
$('#btnGuardarNota').click(function() {   
    if($('#txtHiddenIdProveedor').val() > 0)
    {
        bootbox.confirm('Desea guardar la Información? ', function (confirmed) {
            if (confirmed) {
                if($('#txtHiddenIdProducto').val() > 0)                    
                    $('#btnAgregarTupla').click(); 

                $('#txtHiddenIdIngreso').val($('#ingreso').val());    
                $('#txtHiddenIdSucursal').val($('#sucursal').val());   
                $('#txtHiddenIdClasificacion').val($('#clasificacion').val());   
                // alert($('#txtHiddenIdSucursal').val());         
                
                var datosTabla = [];

                $("#table_DocumentoProducto tbody tr").each(function (index) 
                {
                    var id = ($(this).attr('id')).split('-')[1];
                    if(id != undefined)
                    {
                        var datosFila = {};
                        // datosFila.codigoBarra = $('#principalCodigoBarra' + id).text();
                        datosFila.Idproducto = $('#principalIdproducto' + id).text();
                        datosFila.nombre = $('#principalNombre' + id).text();
                        datosFila.idTran = $('#idTran' + id).text();
                        datosFila.cantidad = $('#principalCantidad_' + id).val();
                        datosFila.lote = $('#principalLote_' + id).text();
                        datosFila.fecha_vto = $('#txtFecha_vencimiento_' + id).val();
                        // datosFila.precio = $('#principalPrecio_' + id).text();
                        datosFila.subtotal = $('#principalSubtotal_' + id).text();                      

                        datosTabla.push(datosFila);
                    }
                });
                if(datosTabla.length == 0)
                {
                    $('#divMensajeAdvertencia').show();
                    $('#lblMensajeError').text('Seleccione algún Producto. ');
                    return;
                }

                $('#arrayDatos').val(JSON.stringify(datosTabla));
                $("#form").submit();
            }
        });
    }
    else
    {
        $('#divMensajeAdvertencia').show();
        $('#lblMensajeError').text('Por favor Seleccione algún proveedor. ');
        // return;
        // bootbox.alert('Por favor Seleccione algún proveedor! ');
    }
});
// =======================================================================================

var calcularTotalCobranza = function(porcentaje, porcentajeBs, setearPorcentajeBs) {
    var total = $('#txtHiddenTotal').val();
    var bonificacion = (parseFloat(total * porcentajeBs)).toFixed(2);
    var descuento = (parseFloat(total * porcentaje)/100).toFixed(2);
    var cobrar = parseFloat(parseFloat(total) + parseFloat(bonificacion));
    cobrar = formatoMiles(cobrar);
    
    if(setearPorcentajeBs != 0)
        $('#txtPorcentajeBs').val(porcentajeBs);
    $('#txtPorcentaje').val(porcentaje);
    // $('#spanTotalBonificacion').text(bonificacion);
    $('#spanDescuentoBs').text(descuento);
    $('#spanTotalPago').text(cobrar);
    $('#txtTotalCobro').val(cobrar.replace(',', ''));
}


// $("table").delegate("button", "click", function() {
//     var id = $(this).attr('id');
//     $('#ventanaReporte').modal({ backdrop: 'static', keyboard: false });
    
//     $('.progress').show();
//     $('#secccionImprimeFactura').html("").append($("<iframe id='yyy'></iframe>", {
//         src: 'venta/imprimirDocumento/' + id,
//         css: {width: '100%', height: '100%'}
//     }));
//     setTimeout(function()
//     {
//         for(var i = 1; i <= 100; i++)
//         {
//             $('.progress-bar').css({width: i + '%'})
//         }
        
//         setTimeout(function()
//         {
//             $('.progress').hide();
//             $('.progress-bar').css({width: '0%'});
//         }, 1800);
//     }, 1000);
// });


// ========================================= [ ELIMINAR ] =========================================
$('#delete').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget);
    var idnota = button.data('idnota');
    var numero = button.data('minumero');
    var modal = $(this);

    modal.find('.modal-body #idnota').val( idnota );
    modal.find('.modal-body #txtNumero').val( numero );
});
$('#btnEliminarRegistro').click(function() {
    var descripcionanulacion = $('#descripcionanulacion').val();
    if(descripcionanulacion == '')
        bootbox.alert('Por favor complete la Descripción! ');
    else
        $('#formEliminar').submit();
});
// ================================================================================================

var ajaxLista_pedido = function(pmovimiento_id) {   
    $.ajax({ 
        url: 'buscaLista_pedido' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            popcion: 1,
            palmacen_id: 0,
            pmovimiento_id: pmovimiento_id
        },
         
        success: function(dato) {   
            var JSONString = dato;
            var data = JSON.parse(JSONString);  
            
            // LISTA DE DISPENSACION DE MEDICAMENTOS 
            $('#tbodyMedicamentoLista').empty(); 
            /////   datos de paciente historial
            if(data.model.length > 0)
            {                
                for(var i = 0; i < data.model.length; i++)
                {              
                    var arrayParametro = {
                        res_detalle_movimiento_id: data.model[i].res_detalle_movimiento_id,
                        res_articulo_id: data.model[i].res_articulo_id,
                        res_codigo: data.model[i].res_codigo,
                        res_descripcion: data.model[i].res_descripcion,
                        res_abreviatura_articulo: data.model[i].res_abreviatura_articulo,
                        res_cantidad: data.model[i].res_cantidad,
                        res_saldo_lote: data.model[i].res_saldo_lote,
                        res_precio_unitario: data.model[i].res_precio_unitario,
                        res_importe_total: data.model[i].res_importe_total,
                        res_numero_lote: data.model[i].res_numero_lote,
                        res_fecha_lote: data.model[i].res_fecha_lote, 
                        res_detalle_lote_id: data.model[i].res_detalle_lote_id
                    };
                    generarListaMedicamento(arrayParametro); 
                } 
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                // seleccionarExamenesNuevamente();
            }
            else{ 
                $('#tbodyMedicamentoLista').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
            } 
                 
        },
        error: function(xhr, ajaxOptions, thrownError) {
             alert('error! ');
        }
    });
}

var generarListaMedicamento = function(arrayParametro) {

    var res_detalle_movimiento_id = arrayParametro['res_detalle_movimiento_id'];  
    var res_codigo = arrayParametro['res_codigo'];
    var res_descripcion = arrayParametro['res_descripcion'];
    var res_abreviatura_articulo = arrayParametro['res_abreviatura_articulo'];
    var res_cantidad = arrayParametro['res_cantidad'];     
    var res_saldo_lote = arrayParametro['res_saldo_lote'];      
    
    $('#tbodyMedicamentoLista').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="res_detalle_movimiento_id' + i + '">' + res_detalle_movimiento_id + '</td>'  
          + '<td id="res_codigo' + i + '">' + res_codigo + '</td>'
          + '<td id="res_descripcion' + i + '">' + res_descripcion + '</td>' 
          + '<td id="res_abreviatura_articulo' + i + '">' + res_abreviatura_articulo + '</td>'          
          + '<td id="res_cantidad' + i + '">' + res_cantidad + '</td>'
          + '<td id="res_saldo_lote' + i + '">' + res_saldo_lote + '</td>'                    
       +'</tr>'
    ); 

    i++;
}

// ========================================= [ NUEVO CODIGO ] =========================================
$("table").delegate("button", "click", function() {
    var id = $(this).attr('id');     
    ajaxLista_pedido(id);
    $('#txtres_movimiento_id').val(id); 
    $('#txtPedido').val($('#txtres_movimiento_id').val());    
});
// ===============================================================================================

var guardar = function() {

    //setearDatosParaGuardar();

    //alert($('#txtPedido').val($('#txtres_movimiento_id').val()));

    $.ajax({
        url: 'guardar_Ingreso' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),            
            res_movimiento : $('#txtres_movimiento_id').val(),              
            idfar_sucursales : $('#idfar_sucursales').val()              
        },  
        success: function(dato) {
            bootbox.alert(dato);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });

    $('#txtres_movimiento_id').val('');
        

    // bootbox.confirm('Desea guardar la información? ', function (confirmed) {
    //     if (confirmed) { 
    //         $('#datosTablaArt').val(JSON.stringify(datosTablaArt));
    //         $("#form").submit();
    //     }
    // });
} 

// var setearDatosParaGuardar = function() {
//     var datosTabla = [];
//     $("#tablaContenedorMedicamentos tbody tr").each(function (index) 
//     { 
//         var id = ($(this).attr('id')).split('-')[1];
//         var datosFila = {}; 
        
//         datosFila.res_detalle_movimiento_id = $('#res_detalle_movimiento_id' + id).text();
//         datosFila.res_articulo_id = $('#res_articulo_id' + id).text();
//         datosFila.res_precio_unitario = $('#res_precio_unitario' + id).text();
//         datosFila.res_importe_total = $('#res_importe_total' + id).text();
//         datosFila.res_numero_lote = $('#res_numero_lote' + id).val();  
//         datosFila.res_fecha_lote = $('#res_fecha_lote' + id).val();  
//         datosFila.res_detalle_lote_id = $('#res_detalle_lote_id' + id).val();  
//         datosFila.res_codigo = $('#res_codigo' + id).val();  
//         datosFila.res_descripcion = $('#res_descripcion' + id).val();  
//         datosFila.res_abreviatura_articulo = $('#res_abreviatura_articulo' + id).val();  
//         datosFila.res_cantidad = $('#res_cantidad' + id).val();  
//         datosFila.res_saldo_lote = $('#res_saldo_lote' + id).val();  
        
//         datosTabla.push(datosFila);
//     });
//     cantidad_datos = datosTabla.length;
//     $('#txtcontenidoTabla').val(JSON.stringify(datosTabla)); 
// } 