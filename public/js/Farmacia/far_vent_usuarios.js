var url = '';
var i = 0;
var datosTabla = [];
var totalExamenSeleccion = 0;
$(document).ready(function() {
    
    $('#divErrorPaciente').hide();
    $('#divErrorExamen').hide();
    if($('#txtScenario').val() == 'create')
        url = 'Create';

    if($('#txtScenario').val() == 'edit')
    {
        var datosPoblacion = $('#txtJsonHcl_poblacion').val();
        var dato = JSON.parse(datosPoblacion);
        
        $('#txtidhcl_poblacion').val(dato.id);
        $('#datoNumeroPaciente').text(dato.numero_historia);
        $('#datoNombrePaciente').text(dato.nombre);
        $('#datoApellidosPaciente').text(dato.primer_apellido + ' ' + dato.segundo_apellido);

        $('#txtHora_Minuto_AmPM').val( $('#txtHiddenHora').val() );
    }
 
});
 

// ========================================== [ DATOS ] ==========================================
// [ Especialidad ]
$('#btnModal_Especialidad').click(function() {    
    $('#vModalBuscadorEspecialidad').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Especialidad').click();
});
$('#txtBuscador_Especialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Especialidad').click();
    }
});
$('#btnBuscar_Especialidad').click(function() {
    $.ajax({
        url: 'buscaVentanilla' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Especialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre,
                        sucursal: data[k].sucursal
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscador_Especialidad').focus();
            }
            else
                $('#tbody_Especialidad').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var sucursal = arrayParametro['sucursal'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaEspecialidad('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idfar_ventanillas' + i + '">' + id + '</td>'
          + '<td id="td_sucursal' + i + '">' + sucursal + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var seleccionaEspecialidad = function(id, idtabla) {
    $('#vModalBuscadorEspecialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_nombre' + id).text() );
    $('#idfar_ventanillas').val( idtabla );
    $('#txtBuscador_Especialidad').val('');
}

// [ Médico Solicitante ]
$('#btnModal_Medico').click(function() {
    $('#vModalBuscadorMedicos').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Medico').click();
});
$('#txtBuscador_Medico').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Medico').click();
    }
});
$('#btnBuscar_Medico').click(function() {    
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Medico').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medico').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Medico').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });

});
var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscadorMedicos').modal('toggle');
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtMedico').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Medico').val('');
}
// ===============================================================================================
 
 

// ========================================= [ GUARDAR ] =========================================
// $('#btnGuardar').click( function(e) {    
//     bootbox.confirm('Desea guardar la información? ', function (confirmed) {
//         if (confirmed) { 
//             $("#form").submit();
//         }
//     });
// });
// ===============================================================================================
