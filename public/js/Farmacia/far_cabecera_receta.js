var url = '';
var cantidad_datos = 0;
var i = 0;
var c = 0;
var datosTabla = []; 
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var valorManual = false;
var cantMedicamento = 0;
var idReceta = 0; 
var DISPENSACION = 1;
var ANULADO = 2;
var VIGENTE = 3;
var SISTEMA = 4;
var MANUAL = 5;
var TIPO_EXTERNA = 5;
var TIPO_INTERNACION = 6;
var TIPO_DOSIS = 7;

$(document).ready(function() {
    $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini'); 
    if($('#txtScenario').val() == 'index') 
        url = 'Index';

    if($('#txtScenario').val() == 'create'){
        $('#btnModalPaciente').attr('disabled','false');
        $('#btnModal_Especialidad').attr('disabled','false');
        $('#btnModal_Medico').attr('disabled','false');
        $('#btnModal_Cobertura').attr('disabled','false');
        $('#btnModal_Formulario').attr('disabled','false');
        $('#btnModal_Medicamento').attr('disabled','false');
        $('#txtBuscarPaciente').attr('disabled','true');
        $('#txtBuscar_Medicamento').attr('disabled','true');        
        $('#btnDispensar').hide();
        $('#btnCancelar').hide();
        $('#btnAnular').hide();
        var date = new Date();
        $('#txtFecha').val(date.toISOString().split('T')[0]);
        $('#txtFecha_Registro').val(date.toISOString().split('T')[0]);
        $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini');
        url = 'Create';
    }

    $("#btnSalirr").click(function () {
        location.href = "../dispensar";
    });
}); 


// ========================================= [ BUSCAR RECETAS ] =========================================
// PERMITE BUSCAR LA INFORMACION DE UNA RECETA MEDIANTE BARCODE

$('#txtsearchCode').keypress(function(e) {
    if(e.which == 13) {
        var codeReceta= $('#txtsearchCode').val();
        $('#vModalBuscadorReceta').modal({
          backdrop: 'static',
          keyboard: true
        });        
        ajaxReceta(codeReceta, 1); 
        $('#txtsearchCode').val('');
    }
});
var ajaxReceta = function(valor, buscarTodo) { 
    $.ajax({ 
        url: 'buscaReceta' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtsearchCode').val()
        },

        success: function(dato) {            
            idReceta = 0;
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            var codigoMedicamento = '';
            var tipo_transaccion = '';
            // alert(url);
            if(data.modelo.length = 1)
            {
                idReceta = data.modelo.id;                 
                $('#txtidhcl_poblacion').val(data.modelo.id);
                $('#txtBuscarPaciente').val(data.modelo.hcl_poblacion_matricula+ ' - ' +data.modelo.hcl_poblacion_nombrecompleto);
                $('#txtEspecialidad').val(data.modelo.especialidad);
                $('#txtMedico').val(data.modelo.matricula + ' - ' + data.modelo.nombres+ ' ' +data.modelo.apellidos);
                $('#diagnostico').val(data.modelo.diagnostico);
                $('#txtDocumento').val(data.modelo.hcl_poblacion_documento);
                $('#txtTelefono').val(data.modelo.hcl_poblacion_telefono);
                $('#txtDomicilio').val(data.modelo.hcl_poblacion_domicilio); 
                $('#txtCobertura').val(data.modelo.cobertura); 
                $('#txtFormulario').val(data.modelo.formulario); 
                $('#txtinv_tipo_transaccion').val(data.modelo.idFormulario); 
                tipo_transaccion = data.modelo.idFormulario;
                $('#btnDispensar').show();
                $('#btnAnular').show();
                $('#btnModal_Codigo').attr('disabled','true');
                $('#txtsearchCode').attr('disabled','true');
                $('#btnManual').attr('disabled','true');
            }
            
            // LISTA DE DISPENSACION DE MEDICAMENTOS
            $('#tbodyMedicamento').empty();
            $('#tbodyMedicamentoLista').empty();
            // if(data.modeloRecetaCabecera.length > 0)
            // {                
            //     for(var i = 0; i < data.modeloRecetaCabecera.length; i++)
            //     {      
            //         alert(data.modeloRecetaCabecera[i].idfar_cabecera_egreso);                  
            //         if (idReceta == data.modeloRecetaCabecera[i].idfar_cabecera_egreso) {
            //             var arrayParametro = {
            //                 idfar_articulo_sucursal: data.modeloRecetaCabecera[i].res_id,
            //                 codificacion_articulo: data.modeloRecetaCabecera[i].res_codificacion,                            
            //                 descripcion_articulo: data.modeloRecetaCabecera[i].res_descripcion,
            //                 indicacion_articulo: data.modeloRecetaCabecera[i].indicacion,
            //                 cantidad: data.modeloRecetaCabecera[i].cantidad,
            //                 saldo: data.modeloRecetaCabecera[i].saldo
            //             };
            //             codigoMedicamento = data.modeloRecetaCabecera[i].codificacion_articulo;
            //             generarMedicamento(arrayParametro,1);
            //         }else
            //         { 
            //             if (tipo_transaccion == TIPO_INTERNACION || tipo_transaccion == TIPO_EXTERNA) 
            //             {
            //                 if (data.modeloRecetaCabecera[i].idinv_estado != SISTEMA && codigoMedicamento == data.modeloRecetaCabecera[i].codificacion_articulo) 
            //                 {
            //                     var arrayParametro = {
            //                         idfar_articulo_sucursal: data.modeloRecetaCabecera[i].id,
            //                         fecha_articulo: data.modeloRecetaCabecera[i].fechadispensacion_detalle,
            //                         codificacion_articulo: data.modeloRecetaCabecera[i].codificacion_articulo,
            //                         descripcion_articulo: data.modeloRecetaCabecera[i].descripcion_articulo,
            //                         indicacion_articulo: data.modeloRecetaCabecera[i].indicacion_articulo,
            //                         cantidad: data.modeloRecetaCabecera[i].cantidad,
            //                         codigo_transaccion: data.modeloRecetaCabecera[i].codigo_transaccion,
            //                         descripcion_almacen: data.modeloRecetaCabecera[i].descripcion_almacen,
            //                         nombre_cuaderno: data.modeloRecetaCabecera[i].nombre_cuaderno                            
            //                     };
            //                     generarListaMedicamento(arrayParametro);
            //                 }
            //             }else{
            //                 if (data.modeloRecetaCabecera[i].idinv_estado != SISTEMA) 
            //                 {
            //                     var arrayParametro = {
            //                         idfar_articulo_sucursal: data.modeloRecetaCabecera[i].id,
            //                         fecha_articulo: data.modeloRecetaCabecera[i].fechadispensacion_detalle,
            //                         codificacion_articulo: data.modeloRecetaCabecera[i].codificacion_articulo,
            //                         descripcion_articulo: data.modeloRecetaCabecera[i].descripcion_articulo,
            //                         indicacion_articulo: data.modeloRecetaCabecera[i].indicacion_articulo,
            //                         cantidad: data.modeloRecetaCabecera[i].cantidad,
            //                         codigo_transaccion: data.modeloRecetaCabecera[i].codigo_transaccion,
            //                         descripcion_almacen: data.modeloRecetaCabecera[i].descripcion_almacen,
            //                         nombre_cuaderno: data.modeloRecetaCabecera[i].nombre_cuaderno                            
            //                     };
            //                     generarListaMedicamento(arrayParametro);
            //                 } 
            //             }                        
            //         }
            //     }

            //     // Vuelve a seleccionar nuevamente despues de buscar los examenes
            //     // seleccionarExamenesNuevamente();
            // }
            // else{
            //     $('#tbodyMedicamento').append(
            //         '<tr>' + 
            //             '<td>Vacío...</td>' +
            //         '</tr>'
            //     );
            //     $('#tbodyMedicamentoLista').append(
            //         '<tr>' + 
            //             '<td>Vacío...</td>' +
            //         '</tr>'
            //     );
            // }

            if(data.modeloRecetaSistema.length > 0)
            {                
                for(var i = 0; i < data.modeloRecetaSistema.length; i++)
                {              
                    var arrayParametro = {

                        idfar_articulo_sucursal: data.modeloRecetaSistema[i].res_id,
                        codificacion_articulo: data.modeloRecetaSistema[i].res_codificacion,                            
                        descripcion_articulo: data.modeloRecetaSistema[i].res_descripcion,
                        indicacion_articulo: data.modeloRecetaSistema[i].res_indicacion,
                        cantidad: data.modeloRecetaSistema[i].res_cantidad,
                        saldo: data.modeloRecetaSistema[i].res_saldo                         
                    };
                    generarMedicamento(arrayParametro,1);
                } 
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                // seleccionarExamenesNuevamente();
            }
            else{ 
                $('#tbodyMedicamento').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
            }


            /////   datos de paciente historial
            if(data.modeloRecetaCabecera.length > 0)
            {                
                for(var i = 0; i < data.modeloRecetaCabecera.length; i++)
                {              
                    var arrayParametro = {
                        idfar_articulo_sucursal: data.modeloRecetaCabecera[i].res_id,
                        fecha_articulo: data.modeloRecetaCabecera[i].res_fechadispensacion,
                        codificacion_articulo: data.modeloRecetaCabecera[i].res_codificacion,
                        descripcion_articulo: data.modeloRecetaCabecera[i].res_descripcion,
                        indicacion_articulo: data.modeloRecetaCabecera[i].res_descripcion,
                        cantidad: data.modeloRecetaCabecera[i].res_cantidad,
                        codigo_transaccion: data.modeloRecetaCabecera[i].res_unidad,
                        descripcion_almacen: data.modeloRecetaCabecera[i].res_sucursal,
                        nombre_cuaderno: data.modeloRecetaCabecera[i].res_cuaderno                            
                    };
                    generarListaMedicamento(arrayParametro); 
                } 
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                // seleccionarExamenesNuevamente();
            }
            else{ 
                $('#tbodyMedicamentoLista').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
            } 
                 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            // alert('error! ');
        }
    });
}

var formatoMiles = function(numero) {
    var valor = numero.toString();
    return parseFloat(valor.replace(/,/g, ""))
                .toFixed(2)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
// ===============================================================================================

var generarMedicamento = function(arrayParametro,opcion) {
    var idfar_articulo_sucursal = arrayParametro['idfar_articulo_sucursal'];
    var codigo = arrayParametro['codificacion_articulo'];
    var descripcion = arrayParametro['descripcion_articulo'];
    var indicacion = arrayParametro['indicacion_articulo'];
    var cantidad = arrayParametro['cantidad'];   
    var saldo = arrayParametro['saldo'];  
    var subtotal = cantidad > 0? parseFloat(saldo)-parseFloat(cantidad) : 0; 
    subtotal = formatoMiles(subtotal);  
    
    if (opcion == 1) {
        $('#tbodyMedicamento').append(
           '<tr id="row-'+i+'">'
              + '<td style="display: none;" id="tdidfar_articulo_sucursal' + i + '">' + idfar_articulo_sucursal + '</td>'          
              + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
              + '<td id="tdDescripcion' + i + '">' + descripcion + '</td>' 
              + '<td id="tdCantidad' + i + '">' + cantidad  + '</td>'          
              + '<td id="tdSaldo' + i + '">' + saldo  + '</td>'  
              + '<td class="col-xs-1" style="text-align: right;" id="principalSubtotal_' + i + '">' + subtotal + '</td>'         
           +'</tr>'
        ); 
        i++;    
    }    
    if (opcion == 2) {
        // var checkbox = '<div class="form-check">'
        //             +   '<input type="checkbox" class="form-check-input" id="checkSeleccionar' + i + '">'
        //             +   ' <label class="form-check-label" for="checkSeleccionar' + i + '" style="cursor: pointer;">'
        //             +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
        //             +   ' </label>'
        //          +  '</div>';

        $('#tbodyMedicamento').append(
           '<tr id="row-'+i+'">'
              + '<td style="display: none;" id="tdidfar_articulo_sucursal' + i + '">' + idfar_articulo_sucursal + '</td>'          
              + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
              + '<td id="tdDescripcion' + i + '">' + descripcion + '</td>' 
              + '<td id="tdCantidad' + i + '">' + cantidad  + '</td>'          
              + '<td id="tdSaldo' + i + '">' + saldo  + '</td>' 
              + '<td class="col-xs-1" style="text-align: right;" id="principalSubtotal_' + i + '">' + subtotal + '</td>' 
              + '<td class="col-xs-1">' + 
                "<button type='button' id='btnQuitarProducto_" + i +"' class='btn btn-sm btn-danger' title='Click Para Quitar de la Lista'>" + 
                    "<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>" +
                "</button>" +
            '</td>' +         
           +'</tr>'
        ); 
        $('#btnQuitarProducto_' + i).click(function() {
            var identificador = (this.id).split('_');
            var id = identificador[1];
            $('#row-' + id).remove();
            $('#txtBuscador_Medicamento').focus();
            calculaTotal();
        }); 
        i++;    
    }
}

var generarListaMedicamento = function(arrayParametro) {
    var id_articulo = arrayParametro['idfar_articulo_sucursal'];
    var fecha_articulo = arrayParametro['fecha_articulo'];
    var codigo_articulo = arrayParametro['codificacion_articulo'];
    var descripcion_articulo = arrayParametro['descripcion_articulo'];
    var indicacion_articulo = arrayParametro['indicacion_articulo'];
    var cantidad_articulo = arrayParametro['cantidad'];     
    var codigo_transaccion = arrayParametro['codigo_transaccion'];     
    var descripcion_almacen = arrayParametro['descripcion_almacen'];     
    var nombre_cuaderno = arrayParametro['nombre_cuaderno'];     
    
    $('#tbodyMedicamentoLista').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdId' + i + '">' + id_articulo + '</td>'          
          + '<td id="tdFecha_articulo' + i + '">' + fecha_articulo + '</td>'
          + '<td id="tdCodigo_articulo' + i + '">' + codigo_articulo + '</td>'
          + '<td id="tdDescripcion_articulo' + i + '">' + descripcion_articulo + '</td>' 
          + '<td id="tdCantidad_articulo' + i + '">' + cantidad_articulo + '</td>'          
          + '<td id="tdCodigo_transaccion' + i + '">' + codigo_transaccion + '</td>'          
          + '<td id="tdDescripcion_almacen' + i + '">' + descripcion_almacen + '</td>'          
          + '<td id="tdNombre_cuaderno' + i + '">' + nombre_cuaderno + '</td>'          
       +'</tr>'
    );

    // $('#checkSeleccionar' + i).click(function() {
    //     examenesSeleccionados();

    //     if($(this).is(':checked'))
    //         totalExamenSeleccion++;
    //     else
    //         totalExamenSeleccion--;

    //     $('#spanTotalExamenSeleccion').text(totalExamenSeleccion);
    //     $('#txtBuscarExamen').focus();
    // });

    i++;
}
// ================================================================================================


// ========================================= [ GUARDAR ] =========================================
$('#txtBuscarPaciente').click( function(e) { 
    $('#txtBuscarPaciente').select();
    // alert('Caps');
    // bootbox.confirm('Desea guardar la información? ', function (confirmed) {
    //     if (confirmed) {
    //         $('#txtJsonCuadernos').val('');
            
    //         $("#form").submit();
    //     }
    // });
});
// ===============================================================================================

 
// ========================================= [ PACIENTE ] =========================================
$('#txtBuscarPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnModalPaciente').click();
    } 
    // alert(e.which);
    if(e.which == 8 ||  e.which == 46){
        alert('asd');
    }
    
});

$('#txtBuscarPaciente').keydown(function(e) {
    if(e.which == 13) {
        $('#btnModalPaciente').click();
        validarDatos();
    }     
    if(e.which == 8 ||  e.which == 46){
        limpiarDatos();
    }
    
});

var limpiarDatos = function() {
    $('#txtidhcl_poblacion').val('');
    $('#tbodyMedicamento').empty();
    $('#tbodyMedicamentoLista').empty();
    $('#btnModal_Medicamento').attr('disabled','true');
    $('#txtBuscar_Medicamento').attr('disabled','true');    
}

var validarDatos = function() {
    var idhcl_poblacion = $('#txtidhcl_poblacion').val();    
    var idhcl_cobertura = $('#txtidcobertura').val();
    if (idhcl_poblacion != '' && idhcl_cobertura != '') 
    {        
        $('#btnModal_Medicamento').removeAttr('disabled','true');
        $('#txtBuscar_Medicamento').removeAttr('disabled','true');
        $('#tbodyMedicamento').empty();
        // $('#tbodyMedicamentoLista').empty();
    } 
}

$('#btnModalPaciente').click(function() { 
    $.ajax({
        url: 'buscaPacientes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscarPaciente').val()
        },

        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);

            if(data.length == 1)
            {                
                $('#txtidhcl_poblacion').val(data[0].id);
                $('#txtBuscarPaciente').val(data[0].codigo+ ' - ' +data[0].primer_apellido+ ' ' +data[0].segundo_apellido+ ' ' +data[0].nombre);
                $('#txtDocumento').val(data[0].documento);
                $('#txtTelefono').val(data[0].telefono);
                $('#txtDomicilio').val(data[0].domicilio); 
                $('#datoNumeroPaciente').text(data[0].numero_historia);
                $('#datoNombrePaciente').text(data[0].nombre);
                $('#datoApellidosPaciente').text(data[0].primer_apellido + ' ' + data[0].segundo_apellido);
                buscar_historial_Medicamentos(data[0].id);
            }
            else
            {
                $('#txtBuscadorPaciente').val( $('#txtBuscarPaciente').val() );
                $('#btnBuscarPaciente').click();

                $('#vModalBuscadorPaciente').modal({
                  backdrop: 'static',
                  keyboard: true
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });     
});

var buscar_historial_Medicamentos = function(idhcl_poblacion) {
    $.ajax({ 
        url: 'buscaDispensado' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idhcl_poblacion: idhcl_poblacion 
        },
        success: function(dato) {
             
            var JSONString = dato;
            var data = JSON.parse(JSONString);  
            // LISTA DE DISPENSACION DE MEDICAMENTOS
            // $('#tbodyMedicamento').empty();
            $('#tbodyMedicamentoLista').empty();
            if(data.modeloRecetaCabecera.length > 0)
            {                
                for(var i = 0; i < data.modeloRecetaCabecera.length; i++)
                {              
                    var arrayParametro = {
                        idfar_articulo_sucursal: data.modeloRecetaCabecera[i].res_id,
                        fecha_articulo: data.modeloRecetaCabecera[i].res_fechadispensacion,
                        codificacion_articulo: data.modeloRecetaCabecera[i].res_codificacion,
                        descripcion_articulo: data.modeloRecetaCabecera[i].res_descripcion,
                        indicacion_articulo: data.modeloRecetaCabecera[i].res_descripcion,
                        cantidad: data.modeloRecetaCabecera[i].res_cantidad,
                        codigo_transaccion: data.modeloRecetaCabecera[i].res_unidad,
                        descripcion_almacen: data.modeloRecetaCabecera[i].res_sucursal,
                        nombre_cuaderno: data.modeloRecetaCabecera[i].res_cuaderno                            
                    };
                    generarListaMedicamento(arrayParametro); 
                } 
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                // seleccionarExamenesNuevamente();
            }
            else{ 
                $('#tbodyMedicamentoLista').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
            } 
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}

$('#txtBuscadorPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    }
});

$('#btnBuscarPaciente').click(function() {
    $.ajax({
        url: 'buscaPacientes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorPaciente').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbodyPaciente').empty();
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {                    
                    var arrayParametro = {
                        id: data[i].id,
                        numero_historia: data[i].numero_historia,
                        nombre: data[i].nombre,
                        primer_apellido: data[i].primer_apellido,
                        segundo_apellido: data[i].segundo_apellido,
                        telefono: data[i].telefono,
                        domicilio: data[i].domicilio,
                        documento: data[i].documento
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                $('#txtBuscadorPaciente').focus();
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaPaciente = function(arrayParametro) {
    var id = arrayParametro['id'];    
    var numero_historia = arrayParametro['numero_historia'];
    var nombre = arrayParametro['nombre'];
    var primer_apellido = arrayParametro['primer_apellido'];
    var segundo_apellido = arrayParametro['segundo_apellido'];
    var telefono = arrayParametro['telefono'] == null? '' : arrayParametro['telefono'];
    var domicilio = arrayParametro['domicilio'] == null? '' : arrayParametro['domicilio'];
    var documento = arrayParametro['documento'] == null? '' : arrayParametro['documento']; 
    var botonSeleccion = ''; 
    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClick('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'       
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td style="display: none;" id="tdIdTelefono' + i + '">' + telefono + '</td>'
          + '<td style="display: none;" id="tdIdDomicilio' + i + '">' + domicilio + '</td>'
          + '<td style="display: none;" id="tdIdDocumento' + i + '">' + documento + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>'
          + '<td id="tdApellidos' + i + '">' + primer_apellido + ' ' + segundo_apellido + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick = function(id, idtabla) {
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) {
    // alert('--------->>>>' + $('#tdIdPaciente' + id).text());
    // $('#txtidhcl_poblacion').val(id);    
    // alert($('#tdIdPaciente').val());
    $('#vModalBuscadorPaciente').modal('toggle');
    $('#txtBuscarPaciente').val('');
    // $('#tdNumeroHistoria' + id).text()
    $('#txtidhcl_poblacion').val($('#tdIdPaciente' + id).text());
    $('#txtBuscarPaciente').val($('#tdNumeroHistoria' + id).text() + ' - ' + $('#tdNombrePaciente' + id).text() + ' ' + $('#tdApellidos' + id).text());            
    $('#txtDocumento').val($('#tdIdDocumento' + id).text());
    $('#txtTelefono').val($('#tdIdTelefono' + id).text());
    $('#txtDomicilio').val($('#tdIdDomicilio' + id).text());
    buscar_historial_Medicamentos($('#tdIdPaciente' + id).text());
    validarDatos();    
}
// ================================================================================================



// ========================================== [ DATOS ] ==========================================
// [ Especialidad ]
$('#btnModal_Especialidad').click(function() {
    $('#vModalBuscadorEspecialidad').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Especialidad').click();
});
$('#txtBuscador_Especialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Especialidad').click();
    }
});
$('#btnBuscar_Especialidad').click(function() {
    $.ajax({
        url: 'buscaEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Especialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscador_Especialidad').focus();
            }
            else
                $('#tbody_Especialidad').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaEspecialidad('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr ondblclick="eventoDobleClick_Especialidad('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick +'" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idhcl_cuaderno' + i + '">' + id + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Especialidad = function(id, idtabla) {
    seleccionaEspecialidad(id, idtabla);
}
var seleccionaEspecialidad = function(id, idtabla) {
    $('#vModalBuscadorEspecialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_nombre' + id).text() );
    $('#idhcl_cuaderno').val( idtabla );
    $('#txtBuscador_Especialidad').val('');
}

// [ Médico Solicitante ]
$('#btnModal_Medico').click(function() { 
    $('#vModalBuscadorMedicos').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Medico').click();
});
$('#txtBuscador_Medico').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Medico').click();
    }
});
$('#btnBuscar_Medico').click(function() { 
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Medico').val()
        },
        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medico').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos,
                        matricula: data[k].matricula
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Medico').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var matricula = arrayParametro['matricula'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr ondblclick="eventoDobleClick_Medico('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idmatricula' + i + '">' + matricula + '</td>'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Medico = function(id, idtabla) {
    seleccionaMedico(id, idtabla);
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscadorMedicos').modal('toggle');
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtMedico').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Medico').val('');
    $('#txtMatricula').val($('#td_idmatricula' + id).text());
}
// ===============================================================================================

 
// ========================================= [ COBERTURA ] =========================================
$('#btnModal_Cobertura').click(function() { 
    $('#vModalBuscadorCoberturas').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Cobertura').click();
});
$('#txtBuscador_Cobertura').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Cobertura').click();
    }
});
$('#btnBuscar_Cobertura').click(function() { 
    $.ajax({
        url: 'buscaCobertura' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Cobertura').val(),
        },
        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Cobertura').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                { 
                    var arrayParametro = {
                        id: data[k].id,
                        descripcion: data[k].descripcion
                    };
                    generarTuplaCobertura(arrayParametro);
                }
                $('#txtBuscador_Cobertura').focus();
            }
            else
                $('#tbody_Cobertura').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaCobertura = function(arrayParametro) {
    var id = arrayParametro['id'];
    var descripcion = arrayParametro['descripcion'];    
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaCobertura('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>'; 

    $('#tbody_Cobertura').append(
       '<tr ondblclick="eventoDobleClick_Cobertura('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowCobertura-'+i+'">'
          + '<td style="display: none;" id="td_id' + i + '">' + id + '</td>'
          + '<td id="td_descripciones' + i + '">' + descripcion + '</td>'          
          + botonSeleccion
       +'</tr>'
    );

    i++;
}
var eventoDobleClick_Cobertura = function(id, idtabla) {
    seleccionaCobertura(id, idtabla);
}
var seleccionaCobertura = function(id, idtabla) {
    $('#vModalBuscadorCoberturas').modal('toggle');
    
    $('#idhcl_cobertura').val( idtabla );
    var nombres = $('#td_descripciones' + id).text();    
    $('#txtCobertura').val( nombres );    
    $('#txtBuscador_Cobertura').val('');    
}
// ===============================================================================================



// ========================================= [ FORMULARIO ] =========================================
$('#btnModal_Formulario').click(function() { 
    $('#vModalBuscadorFormularios').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Formulario').click();
});
$('#txtBuscador_Formulario').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Formulario').click();
    }
});
$('#btnBuscar_Formulario').click(function() { 
    $.ajax({
        url: 'buscaFormulario' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Formulario').val(),
        },
        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Formulario').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                { 
                    var arrayParametro = {
                        id: data[k].id,
                        codigo: data[k].codigo,
                        descripcion: data[k].descripcion,
                        cantidad_medicamentos: data[k].cantidad_medicamentos
                    };
                    generarTuplaFormulario(arrayParametro);
                }
                $('#txtBuscador_Formulario').focus();
            }
            else
                $('#tbody_Formulario').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaFormulario = function(arrayParametro) {
    var id = arrayParametro['id'];
    var codigo = arrayParametro['codigo'];
    var descripcion = arrayParametro['descripcion'];    
    var cantidad_medicamentos = arrayParametro['cantidad_medicamentos'];    
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaFormulario('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>'; 

    $('#tbody_Formulario').append(
       '<tr ondblclick="eventoDobleClick_Formulario('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowFormulario-'+i+'">'
          + '<td style="display: none;" id="td_id' + i + '">' + id + '</td>'
          + '<td style="display: none;" id="td_cantidad' + i + '">' + cantidad_medicamentos + '</td>'
          + '<td id="td_codigo' + i + '">' + codigo + '</td>'          
          + '<td id="td_descripciones' + i + '">' + descripcion + '</td>'          
          + botonSeleccion
       +'</tr>'
    );

    i++;
}
var eventoDobleClick_Formulario = function(id, idtabla) {
    seleccionaFormulario(id, idtabla);
}
var seleccionaFormulario = function(id, idtabla) {
    $('#vModalBuscadorFormularios').modal('toggle');
    $('#txtinv_tipo_transaccion').val( $('#td_id' + id).text() );        
    
    cantMedicamento = $('#td_cantidad' + id).text();
    $('#txtidcobertura').val( $('#td_id' + id).text() );        
    var nombres = $('#td_descripciones' + id).text();
    $('#txtFormulario').val( nombres );    
    $('#txtBuscador_Formulario').val('');

    $('#idfar_adm_tipotransaccion').val( idtabla );
    c=0;
    validarDatos();
}
// ===============================================================================================


// ========================================= [ MEDICAMENTOS ] =========================================
$('#txtBuscar_Medicamento').keypress(function(e) {
    if(e.which == 13) {
        $('#txtBuscador_Medicamento').val($('#txtBuscar_Medicamento').val());
        $('#btnModal_Medicamento').click();
    }
});
$('#btnModal_Medicamento').click(function() { 
    $('#vModalBuscador_Medicamentos').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Medicamento').click();
}); 

$('#txtBuscador_Medicamento').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Medicamento').click();
    }
});

$('#btnBuscar_Medicamento').click(function() {  
    // alert('caps ---->>>>' + $('#txtidhcl_poblacion').val());
    $.ajax({
        url: 'productoDispensacion' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            txtBuscador: $('#txtBuscador_Medicamento').val(),
        }, 

        success: function(dato) { 
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medicamento').empty(); 
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {  
                    var arrayParametro = {
                        id: data[k].id,
                        codificacion: data[k].codificacion,
                        descripcion: data[k].descripcion,
                        unidad: data[k].unidad,
                        saldo: data[k].saldo,
                        cantidad: data[k].cantidad_max_salidad 
                    }; 
                    generarTuplaMedicamento(arrayParametro);
                }
                $('#txtBuscador_Medicamento').focus();
            }
            else
                $('#tbody_Medicamento').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});

var generarTuplaMedicamento = function(arrayParametro) {
    var id = arrayParametro['id'];
    var codificacion = arrayParametro['codificacion'];
    var descripcion = arrayParametro['descripcion'];    
    var unidad = arrayParametro['unidad'];    
    var saldo = arrayParametro['saldo'];     
    var cantidad = arrayParametro['cantidad'];     
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedicamento('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>'; 

    $('#tbody_Medicamento').append(
       '<tr ondblclick="eventoDobleClick_Medicamento('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowMedicamento-'+i+'">'
          + '<td style="display: none;" id="td_cantidadDispensar' + i + '">' + cantidad + '</td>'
          + '<td style="display: none;" id="td_id' + i + '">' + id + '</td>'
          + '<td id="td_codificacion' + i + '">' + codificacion + '</td>'          
          + '<td id="td_descripciones' + i + '">' + descripcion + '</td>'          
          + '<td id="td_saldo' + i + '">' + saldo + '</td>'          
          + botonSeleccion
       +'</tr>'
    );

    i++;
}

var eventoDobleClick_Medicamento = function(id, idtabla) {
    seleccionaMedicamento(id, idtabla);
}

var seleccionaMedicamento = function(id, idtabla) {
    $('#vModalBuscador_Medicamentos').modal('toggle');
    // generarMedicamento(arrayParametro,1);
    // var nombres = $('#td_descripciones' + id).text();
    // $('#txtFormulario').val( nombres );   
    // $('#tbodyMedicamento').empty(); 
    $('#txtBuscador_Medicamento').val('');
    var idfar_articulo_sucursal = $('#td_id' + id).text();
    var codigo = $('#td_codificacion' + id).text();
    var descripcion = $('#td_descripciones' + id).text();
    var indicacion = 'asd';
    var cantidad = $('#td_cantidadDispensar' + id).text();
    var saldo = $('#td_saldo' + id).text();  
    var subtotal = cantidad > 0? parseFloat(saldo)-parseFloat(cantidad) : 0;
    subtotal = formatoMiles(subtotal);

    $('#tbodyMedicamento').append(
           '<tr id="row-'+i+'">'
              + '<td style="display: none;" id="tdidfar_articulo_sucursal' + i + '">' + idfar_articulo_sucursal + '</td>'          
              + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
              + '<td id="tdDescripcion' + i + '">' + descripcion + '</td>' 
              + '<td class="col-xs-1">'  
              //+ '<input type="number" class="form-control" id="principalCantidad_' + i + '" value="' + cantidad + '">' 
              + '<input type="number" class="form-control principalCantidad" min = "1" id="principalCantidad_' + i + '" value="' + cantidad + '">'
              + '</td>'                       
              + '<td id="tdSaldo' + i + '">' + saldo  + '</td>' 
              + '<td class="col-xs-1" style="text-align: left;" id="principalSubtotal_' + i + '">' + subtotal + '</td>' 
              + '<td class="col-xs-1">' + 
                "<button type='button' id='btnQuitarProducto_" + i +"' class='btn btn-sm btn-danger' title='Click Para Quitar de la Lista'>" + 
                    "<span class='glyphicon glyphicon-trash' aria-hidden='true'></span>" +
                "</button>" +
                '</td>' +          
           + '</tr>'  
              
        );  
 
    // esta parte del codigo ya estoy quitando
    // ajaxRecetaHistorial(codigo, 2);

    $('#btnQuitarProducto_' + i).click(function() {
        var identificador = (this.id).split('_');
        var id = identificador[1];
        $('#row-' + id).remove();
        $('#txtBuscador_Medicamento').focus();
        // $('#tbodyMedicamento').empty();
        // $('#tbodyMedicamentoLista').empty();
        c--;
        validarMedicamentos();
        // calculaTotal();
    }); 
    $('#principalCantidad_' + i).click(function() {
        $('#' + this.id).select();
    });
    $('#principalCantidad_' + i).bind('blur keyup change', function() {
        var identificador = (this.id).split('_');
        var id = identificador[1];
        var saldos = $('#tdSaldo' + id).text();
        var subtotal = this.value > 0? parseFloat(saldos) - parseFloat(this.value) : 0;

        if (subtotal < 0) {
            $("#btnDispensar").attr("disabled", true);
            $('#principalSubtotal_' + id).css("color", "red");
        } else {
            $("#btnDispensar").attr("disabled", false);
            $('#principalSubtotal_' + id).css("color", "black");
        }

        $('#principalSubtotal_' + id).text(formatoMiles(subtotal));        
        // calculaTotal();
    });
    // alert(''+c);
    i++;
    c++;
    validarMedicamentos();
}

var validarMedicamentos = function() {        
    if (cantMedicamento<=c) {
        $('#btnModal_Medicamento').attr('disabled','true');
        $('#txtBuscar_Medicamento').attr('disabled','true');    
    }else{
        $('#btnModal_Medicamento').removeAttr('disabled','true');
        $('#txtBuscar_Medicamento').removeAttr('disabled','true'); 
    }    
}

// para omitir
// var ajaxRecetaHistorial = function(codigo, buscarTodo) {  
//     // esta parte del historial ya estoy omitiendo
//     $.ajax({ 
//         url: 'buscaRecetaLista' + url,
//         type: 'post',
//         data: {
//             _token: $('#token').val(),
//             idhcl_poblacion: $('#txtidhcl_poblacion').val(),
//             codificacion : codigo
//         },

//         success: function(dato) {
             
//             var JSONString = dato;
//             var data = JSON.parse(JSONString); 
            
//             // LISTA DE DISPENSACION DE MEDICAMENTOS
//             // $('#tbodyMedicamento').empty();
//             $('#tbodyMedicamentoLista').empty();
//             if(data.modeloRecetaCabecera.length > 0)
//             {                
//                 for(var i = 0; i < data.modeloRecetaCabecera.length; i++)
//                 {              
//                     var arrayParametro = {
//                         idfar_articulo_sucursal: data.modeloRecetaCabecera[i].id,
//                         fecha_articulo: data.modeloRecetaCabecera[i].fechadispensacion_detalle,
//                         codificacion_articulo: data.modeloRecetaCabecera[i].codificacion_articulo,
//                         descripcion_articulo: data.modeloRecetaCabecera[i].descripcion_articulo,
//                         indicacion_articulo: data.modeloRecetaCabecera[i].indicacion_articulo,
//                         cantidad: data.modeloRecetaCabecera[i].cantidad,
//                         codigo_transaccion: data.modeloRecetaCabecera[i].codigo_transaccion,
//                         descripcion_almacen: data.modeloRecetaCabecera[i].descripcion_almacen,
//                         nombre_cuaderno: data.modeloRecetaCabecera[i].nombre_cuaderno                            
//                     };
//                     generarListaMedicamento(arrayParametro); 
//                 } 
//                 // Vuelve a seleccionar nuevamente despues de buscar los examenes
//                 // seleccionarExamenesNuevamente();
//             }
//             else{ 
//                 $('#tbodyMedicamentoLista').append(
//                     '<tr>' + 
//                         '<td>Vacío...</td>' +
//                     '</tr>'
//                 );
//             } 
//         },
//         error: function(xhr, ajaxOptions, thrownError) {
//             alert('error! ');
//         }
//     });
// }

// var calculaTotal = function() {
//     var total = 0;
//     var contador = 0;
//     $("#tablaContenedorMedicamento tbody tr").each(function(index)
//     {
//         var id = ($(this).attr('id')).split('-')[1];
//         var subtotal = ($('#principalSubtotal_' + id).text()).replace(',', '');
//         total = (total.toString()).replace(',', '');
//         total = (parseFloat(total) + parseFloat(subtotal)).toFixed(2);
//         contador++;
//     }); 
// }
 
// ===============================================================================================

// ========================================= [ LIMPIAR CAMPOS ] =========================================
var limpiarCampos = function() {
    c=0;
    $('#txtidhcl_poblacion').val('');
    $('#txtBuscarPaciente').val('');
    $('#txtEspecialidad').val('');
    $('#txtMedico').val('');
    $('#diagnostico').val('');
    $('#txtDocumento').val('');
    $('#txtTelefono').val('');
    $('#txtDomicilio').val('');
    $('#txtCobertura').val('');
    $('#txtFormulario').val('');
    $('#btnDispensar').hide();
    $('#btnAnular').hide();    
    $('#tbodyMedicamento').empty();
    $('#tbodyMedicamentoLista').empty();
}
// ===============================================================================================

// ========================================= [ HABILITAR  BOTONES ] =========================================
var habilitarBotones = function(valor) {
    if (valor == false) {
        limpiarCampos(); 
        $('#btnModalPaciente').removeAttr('disabled','true');
        $('#btnModal_Especialidad').removeAttr('disabled','true');
        $('#btnModal_Medico').removeAttr('disabled','true');
        $('#btnModal_Cobertura').removeAttr('disabled','true');
        $('#btnModal_Formulario').removeAttr('disabled','true');        
        $('#btnModal_Codigo').attr('disabled','true');
        $('#btnManual').attr('disabled','true');
        $('#txtsearchCode').attr('disabled','true');
        $('#txtBuscarPaciente').removeAttr('disabled','true');        
        $('#btnCancelar').show();
        $('#btnDispensar').show();
        $('#btnSalirr').hide();
        valorManual = true;
    }else{
        limpiarCampos(); 
        $('#btnModalPaciente').attr('disabled','true');
        $('#btnModal_Especialidad').attr('disabled','true');
        $('#btnModal_Medico').attr('disabled','true');
        $('#btnModal_Cobertura').attr('disabled','true');
        $('#btnModal_Formulario').attr('disabled','true');
        $('#btnModal_Medicamento').attr('disabled','true');
        $('#btnManual').removeAttr('disabled','true');
        $('#btnModal_Codigo').removeAttr('disabled','true');
        $('#txtsearchCode').removeAttr('disabled','true');
        $('#txtBuscarPaciente').attr('disabled','true');
        $('#txtBuscar_Medicamento').attr('disabled','true'); 
        $('#txtDocumento').removeAttr('disabled','true');
        $('#txtTelefono').removeAttr('disabled','true');
        $('#txtDomicilio').removeAttr('disabled','true');       
        $('#btnCancelar').hide();
        $('#btnDispensar').hide();
        $('#btnSalirr').show();
        valorManual = false;
    }
}

var habilitarBotonesNuevaReceta = function() { 
    $('#btnModalPaciente').attr('disabled','true');
    $('#btnModal_Medicamento').removeAttr('disabled','true');
    $('#btnManual').attr('disabled','true');
    $('#txtDocumento').attr('disabled','true');
    $('#txtTelefono').attr('disabled','true');
    $('#txtDomicilio').attr('disabled','true');
    $('#btnModal_Codigo').attr('disabled','true');
    $('#txtsearchCode').attr('disabled','true');
    $('#txtBuscarPaciente').attr('disabled','true');
    $('#txtBuscar_Medicamento').removeAttr('disabled','true');  
    $('#tbodyMedicamento').empty();
    $('#tbodyMedicamentoLista').empty();      
    $('#btnCancelar').show();
    $('#btnDispensar').show();
    $('#btnSalirr').hide();
    c = 0;
}

// ===============================================================================================

// ========================================= [ ANULAR ] =========================================
$('#btnAnular').click( function(e) {
    bootbox.confirm('Desea anular la receta? ', function (confirmed) {
        if (confirmed) {
            $('#txtEstado').val(ANULADO);
            // alert(idReceta);
            habilitarBotones(true);
            $("#form").submit();
        }
    });
        
    // alert('asdasd');
    // bootbox.confirm('Desea guardar la información? ', function (confirmed) {
    //     if (confirmed) {
    //         $('#txtJsonCuadernos').val('');
            
    //         $("#form").submit();
    //     }
    // });
});
// ===============================================================================================

// ========================================= [ GUARDAR ] =========================================
$('#btnDispensar').click( function(e) { 
   
    if (valorManual) { 
        bootbox.confirm('Desea guardar la receta? ', function (confirmed) {
        if (confirmed) {
            $('#idfar_cabecera_egreso').val(0);

            var idhcl_poblacion = $('#txtidhcl_poblacion').val();
            var telf = $('#txtTelefono').val();
            var dom = $('#txtDomicilio').val();
            var doc = $('#txtDocumento').val();
            var esp = $('#idhcl_cuaderno').val();
            var med = $('#iddatosgenericos').val();
            var cob = $('#idhcl_cobertura').val();
            var form = $('#idfar_adm_tipotransaccion').val();

            setearDatosParaGuardar(); 
            var datos = $('#txtcontenidoTabla').val(); 

            if (idhcl_poblacion == '') {
                $('#divMensajeAdvertencia').show();
                $('#lblMensajeError').text('Seleccione paciente. ');
                return;
            };

            if (esp == '') {
                $('#divMensajeAdvertencia').show();
                $('#lblMensajeError').text('Seleccione especialidad. ');
                return;
            };

            if (med == '') {
                $('#divMensajeAdvertencia').show();
                $('#lblMensajeError').text('Seleccione medico. ');
                return;
            };

            if (cob == '') {
                $('#divMensajeAdvertencia').show();
                $('#lblMensajeError').text('Seleccione cobertura. ');
                return;
            };

            if (form == '') {
                $('#divMensajeAdvertencia').show();
                $('#lblMensajeError').text('Seleccione formulario. ');
                return;
            }; 
            // alert(cantidad_datos);

            if (0 >= cantidad_datos ) {
                $('#divMensajeAdvertencia').show();
                $('#lblMensajeError').text('Seleccione algunos items. ');
                return;
            };


            limpiarCampos();
            dispensarManual(idhcl_poblacion,telf,dom,doc,esp,med,cob,form,datos);
            habilitarBotonesNuevaReceta();

            
            // $('#txtidtelefono').val(telf);            
            // $('#txtiddomicilio').val(dom);            
            // $('#txtiddocumento').val(doc); 
            // $('#txtEstado').val(MANUAL);                  
                
  
            
            // bootbox.confirm('¿Atenderá al mismo paciente?', function (confirmed) {
            //     if (confirmed) {     
            //         $("#form").submit();
            //         habilitarBotonesNuevaReceta();                                                                             
            //     }else{                    
            //         $("#form").submit();
            //         habilitarBotones(true);                    
            //     }
            // });    
            
            
        }                
        });     
    }else{
        bootbox.confirm('Desea guardar la receta? ', function (confirmed) {
        if (confirmed) {            
            //$('#txtEstado').val(SISTEMA);
            // $('#idfar_cabecera_egreso').val(idReceta);

            dispensarSistema(idReceta);
            // alert(idReceta);
            habilitarBotones(true);            
            // alert($('#txtEstado').val());
            setearDatosParaGuardar();
            //$("#form").submit();
        }
    }); 
    } 
});

var dispensarManual = function(idhcl_poblacion,telf,dom,doc,esp,med,cob,form,datos) {
    
    $.ajax({
        url: 'guardar_Manual' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idfar_sucursales_recepcionado: 1, 
            idhcl_poblacion : idhcl_poblacion,
            telf : telf,
            dom : dom,
            doc : doc,
            idhcl_cuaderno : esp,
            iddatosgenericos : med,
            idhcl_cobertura : cob,
            idfar_adm_tipotransaccion : form,
            gridDocumentodetalle : datos,
        },  
        success: function(dato) {
            bootbox.alert(dato);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    }); 
} 

var dispensarSistema = function(idReceta) {
    
    $.ajax({
        url: 'guardar_Sistema' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idfar_cabecera_receta: idReceta,
            idfar_sucursales_recepcionado: 2,
        },  
        success: function(dato) {
            bootbox.alert(dato);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    }); 
} 

var setearDatosParaGuardar = function() {
    var datosTabla = [];
    $("#tablaContenedorMedicamento tbody tr").each(function (index) 
    {
        // alert('asdasd');
        var id = ($(this).attr('id')).split('-')[1];
        var datosFila = {};
        
        datosFila.Idproducto = $('#tdidfar_articulo_sucursal' + id).text();
        datosFila.codigo = $('#tdCodigo' + id).text();
        datosFila.nombre = $('#tdDescripcion' + id).text();
        datosFila.cantidad = $('#principalCantidad_' + id).val();  
        
        datosTabla.push(datosFila);
    });
    cantidad_datos = datosTabla.length;
    $('#txtcontenidoTabla').val(JSON.stringify(datosTabla)); 
}
// ===============================================================================================

// ========================================= [ RECETA MANUAL ] =========================================
$('#btnManual').click( function(e) { 
    habilitarBotones(valorManual);
    
    // alert('Caps');
    // bootbox.confirm('Desea guardar la información? ', function (confirmed) {
    //     if (confirmed) {
    //         $('#txtJsonCuadernos').val('');
            
    //         $("#form").submit();
    //     }
    // });
});

// ========================================= [ CANCELAR ] =========================================

$('#btnCancelar').click( function(e) { 
    habilitarBotones(valorManual); 
    
    // alert('Caps');
    // bootbox.confirm('Desea guardar la información? ', function (confirmed) {
    //     if (confirmed) {
    //         $('#txtJsonCuadernos').val('');
            
    //         $("#form").submit();
    //     }
    // });
});
// ===============================================================================================

// ========================================= [ LISTA DE MEDICAMENTOS INDEX ] =========================================
$("table").delegate("button", "click", function() {
    var id = $(this).attr('id');
    $('#vModalBuscadorReceta').modal({ backdrop: 'static', keyboard: false });

    // alert(id);
    
    // $('.progress').show();
    // $('#secccionImprimeFactura').html("").append($("<iframe></iframe>", {
    //     src: 'resultado/imprimeProgramacionResultado/' + id + '*|*0',
    //     css: {width: '100%', height: '100%'}
    // }));
    // setTimeout(function()
    // {
    //     for(var i = 1; i <= 100; i++)
    //     {
    //         $('.progress-bar').css({width: i + '%'})
    //     }
        
    //     setTimeout(function()
    //     {
    //         $('.progress').hide();
    //         $('.progress-bar').css({width: '0%'});
    //     }, 1800);
    // }, 1000);
});
// ===============================================================================================