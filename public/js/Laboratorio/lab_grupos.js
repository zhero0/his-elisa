var url = '';
var i = 0;
var datosTablaLab = [];
var totalExamenSeleccion = 0;
// var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var opcionButton = 1; //    1 => Laboratorio       2 => Imagenología
var totalExamenSeleccionLab = 0;
// var ordenVariable = 0;

$(document).ready(function() {
   url = 'Create';
   $('#divErrorExamen').hide();

    if($('#txtScenario').val() == 'edit')
    {
        url = '';
        // lista de "EXÁMENES"
        $('#tbodyExamenes').empty();
        var JSONString = $('#lab_examenesEspecialidad').val();
        var data = JSON.parse(JSONString);
        if(data.length > 0)
        {
            for(var i = 0; i < data.length; i++)
            {
                var arrayParametro = {
                    idima_examenes: data[i].idlab_examenes,
                    codigo: data[i].codigo,
                    nombre: data[i].nombre,
                    idEspecialidad: data[i].idEspecialidad,
                    codigoEspecialidad: data[i].codigoEspecialidad,
                    nombreEspecialidad: data[i].nombreEspecialidad
                };
                generarListaExamenes(arrayParametro);
            }
            
            if($('#txtScenario').val() == 'edit')
            {
                var JSONString = $('#lab_examenesEspecialidad').val();
                var data = JSON.parse(JSONString);
                for(var i = 0; i < data.length; i++)
                {
                    datosTablaLab.push((data[i].idlab_examenes).toString());
                    totalExamenSeleccion++;
                }
                totalExamenSeleccionLab = datosTablaLab.length;
                $('#spanTotalExamenSeleccion').text(datosTablaLab.length);
                
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                seleccionarExamenesNuevamente();
            }
        }
        else
            $('#tbodyExamenes').append(
                '<tr>' + 
                    '<td>Vacío...</td>' +
                '</tr>'
            );
    }
});


// ========================================= [ GUARDAR ] =========================================
$('#btnGuardar').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {
            $('#txtJsonExamenes').val('');
            
            // $('#txtHiddenHora').val( $('#txtHora_Minuto_AmPM').val() );
            // if($('#txtidhcl_poblacion').val() == '')
            // {
            //     $('#divErrorPaciente').show();
            //     return;
            // }
            
            if(datosTablaLab.length > 0)
            {
                datosTablaLab = Array.from(new Set(datosTablaLab));
                var jsonExamenes = JSON.stringify(datosTablaLab);
                $('#txtJsonExamenes').val(jsonExamenes);
            }
            else
            {
                $('#divErrorExamen').show();
                return;
            }
            
            $("#form").submit();
        }
    });
});
// ===============================================================================================