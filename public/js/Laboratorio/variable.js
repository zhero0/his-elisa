var url = '';
var i = 0;
var idlab_formula = 0;
var idFormula = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var editar = 0;

$(document).ready(function() {
    url = 'Create';

    if($('#txtScenario').val() == 'edit')
    {
        url = '';
        // editar = 1;

        var variable = ($('#variable').val()).split('-');
        var idVariable = variable[0];
        var permite_llenado = variable[1];
        var selecciona_formula = variable[2];
        // [3]
        if(permite_llenado == 1)
          $('#divListaVariable').show();
        if(selecciona_formula == 1)
            $('#divFormula_parametros').show();
        
        var JSONString = $('#txtListavariable').val();
        var data = JSON.parse(JSONString);
        for(var i = 0; i < data.length; i++)
        {
            var arrayParametro = {
                nombreTipo: data[i].lis_variable
            };
            generarTuplaVariable(arrayParametro);
        }


        // Muestra el detalle de las variable de una determinada fórmula
        var jsonDato = $('#txtHiddenJSONdatos').val();
        var data = JSON.parse(jsonDato);
        var formula = data.formula;
        var lab_formularespuesta = JSON.parse(data.lab_formularespuesta);

        $('#divFormulaSeleccionada').text(formula);
        if(lab_formularespuesta.length > 0)
        {
            editar = 1;
            for(var j = 0; j < lab_formularespuesta.length; j++)
            {
                generarVariablesFormula(lab_formularespuesta[j]);
            }
        }
        else
            editar = 0;
    }
    
    if($('#txtScenario').val() == 'create' && editar == 0 /* || $('#txtScenario').val() == 'edit'*/)
    {
        var JSONString = $('#txtLab_formula').val();
        var data = JSON.parse(JSONString);

        $('#tbody_Formula').empty();
        for(var i = 0; i < data.length; i++)
        {
            var arrayParametro = {
                id: data[i].id,
                formula: data[i].formula
            };
            generarTuplaFormula(arrayParametro);
        }
    }

});

$('#variable').change(function(){
    // => LISTA DE OPCIONES
    // => LISTA DE SELECCION MULTIPLE
    var variable = (this.value).split('-');
    var idVariable = variable[0];
    var permite_llenado = variable[1];
    var selecciona_formula = variable[2];

    if(permite_llenado == 1)
    {
        $('#divListaVariable').show();
        $('#txtNombreTipo').focus();
    }
    else
        $('#divListaVariable').hide();
    
    
    if(selecciona_formula == 1)
        $('#divFormula_parametros').show();
    else
        $('#divFormula_parametros').hide();
});
$('#btnAgregarTipo').click(function() {
    agregarTipoVariable();
});
$('#txtNombreTipo').keypress(function(e) {
    if(e.which == 13) {
        agregarTipoVariable();
    }
});
var agregarTipoVariable = function()
{
    var nombreTipo = $('#txtNombreTipo').val();
    var arrayParametro = {
        nombreTipo: nombreTipo
    };
    if(nombreTipo != '')
        generarTuplaVariable(arrayParametro);
    else
      bootbox.alert('NO puede agregar tipo de variable VACIO!!! ');
}
var generarTuplaVariable = function(arrayParametro)
{
    var botonDelete = '';
    var nombreTipo = arrayParametro['nombreTipo'];
    $('#idListaVacia').remove();

    if($('#txtScenario').val() != 'view')
        botonDelete = 
            '<td class="col-xs-1">'
                +'<span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="eliminaTupla('+i+')" style="cursor: pointer;" title="Eliminar"></span>'
            +'</td>';
    
    $('#tablaContenedorDatos').append(
       '<tr id="row-'+i+'">'
          + '<td class="col-xs-10" id="tdNombreTipoVariable' + i + '">' + nombreTipo.toUpperCase() + '</td>'
          + botonDelete
       +'</tr>'
    );
    $('#thCantidadTipoVariable').text( 'Tipo Variable (' + $("#tablaContenedorDatos tbody tr").length + ')' );
    $('#txtNombreTipo').val('');
    $("#txtNombreTipo").focus();
    
    i++;
}
var eliminaTupla = function(id)
{
    $('#row-' + id).remove();
    $('#thCantidadTipoVariable').text( 'Tipo Variable (' + $("#tablaContenedorDatos tbody tr").length + ')' );
    $("#txtNombreTipo").focus();

    if($("#tablaContenedorDatos tbody tr").length == 0)
      $('#tablaContenedorDatos').append(
          '<tr id="idListaVacia">' + 
              '<td>Lista Vacia...</td>' +
          '</tr>'
      );
}



// ========================================= [ FÓRMULAS ] =========================================
$('#btnMostrarFormulas').click(function() {
    $('#vModalBuscadorFormula').modal({
      backdrop: 'static',
      keyboard: true
    });
});
var generarTuplaFormula = function(arrayParametro) {
    var id = arrayParametro['id'];
    var formula = arrayParametro['formula'];
    var botonSeleccion = 
            '<td class="col-xs-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaFormula('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Formula').append(
       '<tr ondblclick="eventoDobleClick_Formula('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idlab_formula' + i + '">' + id + '</td>'
          + '<td id="td_formula' + i + '">' + formula + '</td>'
          + botonSeleccion
       +'</tr>'
    );    
    i++;
}
var eventoDobleClick_Formula = function(id, idtabla) {
    seleccionaFormula(id, idtabla);
}
var seleccionaFormula = function(id, idtabla) {
    $('#vModalBuscadorFormula').modal('toggle');
    
    idlab_formula = $('#td_idlab_formula' + id).text();
    var formula = $('#td_formula' + id).text();
    $('#divFormulaSeleccionada').text(formula);

    $.ajax({
        url: 'buscaParametroFormula' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idlab_formula: idlab_formula
        },
        success: function(dato) {
            $('#tbody_FormulaExamen').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.length > 0)
            {
                for(var j = 0; j < data.length; j++)
                {
                    generarVariablesFormula(data[j]);
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarVariablesFormula = function(dato) {
    var botonSeleccion = '';
    var id = dato.id;
    var idlab_examenes = '';
    var lab_examenes = '...';
    var variable = dato.variable;

    if(editar == 1)
    {
        // 0: {idlab_formulaparametro: 1, lab_parametro_variable: "VAR1", idlab_examenes: null, lab_examenes: "..."}
        // 1: {idlab_formulaparametro: 2, lab_parametro_variable: "VAR2", idlab_examenes: 17, lab_examenes: "ALBUMINA (Q20)"}
        // 2: {idlab_formulaparametro: 3, lab_parametro_variable: "VAR3", idlab_examenes: 31, lab_examenes: "SODIO (Q42)"}
        id = dato.idlab_formulaparametro;
        idlab_examenes = dato.idlab_examenes;
        lab_examenes = dato.lab_examenes;
        variable = dato.lab_parametro_variable;
    }
    
    botonSeleccion = 
            '<td class="col-xs-1">'
                +'<span class="glyphicon glyphicon-tasks" aria-hidden="true" onclick="seleccionaFormulaExamen('+i+', ' + id + ')" style="cursor: pointer;" title="Pulse Click para Seleccionar Exámen! "></span>'
            +'</td>';
    $('#tbody_FormulaExamen').append(
       '<tr ondblclick="eventoDobleClick_FormulaExamen('+i+', ' + id + ')" style="cursor: pointer;" title="Pulse Doble Click para Seleccionar Exámen! " id="row-'+i+'">'
            + '<td style="display: none;" id="td_idlab_formulaparametro' + i + '">' + id + '</td>'
            + '<td style="display: none;" id="td_idlab_examenes' + i + '">' + idlab_examenes + '</td>'
            + '<td id="td_lab_examenes' + i + '">' + lab_examenes + '</td>'
            + '<td id="td_variable' + i + '">' + variable + '</td>'
            + botonSeleccion
        +'</tr>'
    );
    i++;
}
var eventoDobleClick_FormulaExamen = function(id, idtabla) {
    seleccionaFormulaExamen(id, idtabla);
}
var seleccionaFormulaExamen = function(id, idtabla) {
    $('#vModalBuscadorExamen').modal({
        backdrop: 'static',
        keyboard: true
    });
    
    idFormula = id;
    $('#btnBuscarExamen').click();
    // ajaxMuestraLab_grupos();
}
// =====================================================================================================



// ========================================= [ EXÁMENES DE LABORATORIO ] =========================================
$('#txtBuscarExamen').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarExamen').click();
    }
});
$('#btnBuscarExamen').click(function() {
    var valor = $('#txtBuscarExamen').val();
    ajaxExamen('', 0, valor, 1);
    $('#txtBuscarExamen').focus();
});
// $('#btnVisualizaExamenesSeleccionados').click(function() {
//     ajaxExamen('', 0, '', 0);
//     $('#txtBuscarExamen').val('');
//     $('#txtBuscarExamen').focus();
// });
var ajaxExamen = function(valorURL, idlab_grupos, valor, buscarTodo) {
    if(valorURL == '')
        var ruta = 'buscaExamenesLab';
    else
        var ruta = valorURL;
    
    $.ajax({
        url: ruta + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            buscarTodo: buscarTodo,
            idlab_grupos: idlab_grupos
        },
        success: function(dato) {
            $('#tbodyExamenes').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);

            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        idlab_examenes: data[i].idlab_examenes,
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        idEspecialidad: data[i].idEspecialidad,
                        codigoEspecialidad: data[i].codigoEspecialidad,
                        nombreEspecialidad: data[i].nombreEspecialidad,
                        idlab_grupos: idlab_grupos
                    };
                    generarListaExamenes(arrayParametro);
                }
            }
            else
                $('#tbodyExamenes').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaExamenes = function(arrayParametro) {
    var idlab_examenes = arrayParametro['idlab_examenes'];
    var id = idlab_examenes;
    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var idEspecialidad = arrayParametro['idEspecialidad'];
    var codigoEspecialidad = arrayParametro['codigoEspecialidad'];
    var nombreEspecialidad = arrayParametro['nombreEspecialidad'];
    var idlab_grupos = arrayParametro['idlab_grupos'];
    var botonSeleccion = 
            '<td class="col-xs-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaExamen('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';
    
    $('#tbodyExamenes').append(
        '<tr ondblclick="eventoDobleClick_Examen('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowExamen-'+i+'">'
          + '<td style="display: none;" id="td_idlab_examenes' + i + '">' + idlab_examenes + '</td>'
          + '<td style="display: none;" id="tdIdEspecialidad' + i + '">' + idEspecialidad + '</td>'
          + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td id="tdEspecialidad' + i + '">' + nombreEspecialidad + ' (' + codigoEspecialidad + ')' + '</td>'
          + botonSeleccion
        +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Examen = function(id, idtabla) {
    seleccionaExamen(id, idtabla);
}
var seleccionaExamen = function(id, idtabla) {
    $('#vModalBuscadorExamen').modal('toggle');

    var idlab_examenes = $('#td_idlab_examenes' + id).text();
    var nombre = $('#tdNombre' + id).text();
    var codigo = $('#tdCodigo' + id).text();
    var examen = nombre + ' (' + codigo + ')';
    
    $('#td_idlab_examenes' + idFormula).text( idlab_examenes );
    $('#td_lab_examenes' + idFormula).text( examen );
}
// [lab_grupos]
// var ajaxMuestraLab_grupos = function() {
//     $.ajax({
//         url: 'muestraLab_grupos' + url,
//         type: 'post',
//         data: {
//             _token: $('#token').val(),
//             idalmacen: $('#idalmacen').val()
//         },
//         success: function(dato) {
//             $('#listaGrupo').empty();
//             var JSONString = dato;
//             var data = JSON.parse(JSONString);
//             alert(data.length);
//             if(data.length > 0)
//             {
//                 for(var i = 0; i < data.length; i++)
//                 {
//                     $('#listaGrupo').append(
//                         '<button type="button" class="list-group-item" onclick="lab_grupoSelecciondo(' + data[i].id + ')">' + data[i].nombre + ' <span class="badge">' + data[i].total_examenes + '</span></button>'
//                     );
//                 }
//             }
//         },
//         error: function(xhr, ajaxOptions, thrownError) {
//             alert('error! ');
//         }
//     });
// }
// var lab_grupoSelecciondo = function(idlab_grupos) {
//     ajaxExamen('buscaExamenesLab_perfil_examenes', idlab_grupos, '', 0);
// }
// =====================================================================================================



// ========================================= [ GUARDAR ] =========================================
$('#btnGuardarVariable').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {
            // => LISTA DE OPCIONES
            // => LISTA DE SELECCION MULTIPLE
            var variable = ($('#variable').val()).split('-');
            var idVariable = variable[0];
            var permite_llenado = variable[1];

            // $("#tablaContenedorDatos_FormulaExamen tbody tr").length
            var datosTabla = [];
            $("#tablaContenedorDatos_FormulaExamen tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                var idlab_formulaparametro = $('#td_idlab_formulaparametro' + identificador).text();
                var idlab_examenes = $('#td_idlab_examenes' + identificador).text();
                var lab_examenes = $('#td_lab_examenes' + identificador).text();
                var variable = $('#td_variable' + identificador).text();
                
                var datosFila = {};
                datosFila.idlab_formulaparametro = idlab_formulaparametro;
                datosFila.idlab_examenes = idlab_examenes;
                datosFila.lab_examenes = lab_examenes;
                datosFila.variable = variable;
                datosTabla.push(datosFila);
            });
            var jsonLab_formularespuesta = JSON.stringify(datosTabla);

            if(permite_llenado == 1)
            {
                var datosTabla = [];
                $("#tablaContenedorDatos tbody tr").each(function (index) 
                {
                    var identificador = ($(this).attr('id')).split('-')[1];
                    var nombreTipoVariable = $('#tdNombreTipoVariable' + identificador).text();
                    var datosFila = {};

                    if(nombreTipoVariable != '')
                    {
                        datosFila.nombreTipoVariable = nombreTipoVariable;
                        datosTabla.push(datosFila);
                    }
                });
                if(datosTabla.length > 0)
                {
                    var jsonListaTipoVariable = JSON.stringify(datosTabla);
                    // $('#txtListaTipoVariable').val(jsonListaTipoVariable);
                }
                else
                {
                    bootbox.alert('Debe Seleccionar Tipo de Variable! ');
                    return;
                }
            }
            else
            {
                // $('#txtListaTipoVariable').val('');
                var jsonListaTipoVariable = '';
                
                // -------------------------------- lo de abajo borrar... --------------------------------
                // var datosFila = {};
                // datosFila.idlab_formula = idlab_formula;
                // datosFila.idlab_parametro = $('#selectParametro').val();
                // var jsonDato = JSON.stringify(datosFila);
                // $('#txtParametro').val(jsonDato);
            }
            
            // [ DATOS A ENVIAR PARA GUARDAR... ]
            var datosFila = {};
            datosFila.jsonLab_formularespuesta = jsonLab_formularespuesta;
            datosFila.jsonListaTipoVariable = jsonListaTipoVariable;
            $('#txtHiddenJSONdatos').val( JSON.stringify(datosFila) );

            $("#form").submit();
        }
    });
});
// ===============================================================================================