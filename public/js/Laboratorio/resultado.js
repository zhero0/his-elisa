var url = '';
var i = 0;
var objeto = new Object();
var Lab_formula = '';
var modelExamenVariable = '';
var examen_resultado = [];
var parametroExamenesFormula = [];
var numeroCajaNumero = 0;
var idPrimeraCajitaRespuesta = '';



var datosTablaLab = [];
// var totalExamenSeleccion = 0;
var opcionButton = 1; //    1 => Laboratorio       2 => Imagenología
var totalExamenSeleccionLab = 0;
// var ordenVariable = 0;

$(document).ready(function() {
    
    if($('#txtScenario').val() == 'edit')
    {
        $('#body').attr('class','skin-blue fixed sidebar-collapse sidebar-mini');
        
        var JsonDatos = $('#txtJsonValores').val();
        var dato = JSON.parse(JsonDatos);
        var datosPaciente = JSON.parse(dato.datosPaciente);
        
        $('#txtidhcl_poblacion').val(datosPaciente.id);
        $('#datoNumeroPaciente').text(datosPaciente.hcl_poblacion_matricula);
        $('#datoNombrePaciente').text(datosPaciente.hcl_poblacion_nombrecompleto);
        $('#datoEdadPaciente').text(datosPaciente.edad + ' Años');
        
        muestra_Perfil();
        $('#txtValorResultado_X_' + idPrimeraCajitaRespuesta).select();
    }
    
});

// ========================================= [ MUESTRA ] =========================================
$('#registraMuestreo').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var numero = button.data('numero');
    var modal = $(this);
    
    $.ajax({
        url: 'resultado_lab/obtenerNroFolio' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idlab_resultados_programacion: id
        },
        success: function(dato) {
            var valor = JSON.parse(dato);
            $('#txtNroFolioAnterior').text('Nº FOLIO ANTERIOR: ' + valor.numeroFolioAnterior);
            $('#txtNumero_muestra').val( valor.numeroFolioPosterior );
            // $('#vResultadoProgramacion_Muestra').modal({ backdrop: 'static', keyboard: false });
            $('#txtNumero_muestra').focus();

            $('#tituloModalMuestreoLab').text('MUESTREO LABORATORIO Nº ' + numero);
            modal.find('.modal-body #idtabla').val( id );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
$('#btnGuardarMuestreo').click(function() {
    // var descripcionanulacion = $('#descripcionanulacion').val();
    // if(descripcionanulacion == '')
    //     bootbox.alert('Por favor complete la Descripción! ');
    
    var datosFila = {};
    datosFila.hora_muestra = $('#txtHora_Minuto_AmPM').val();
    
    $('#txtDatosExtras').val( JSON.stringify(datosFila) );
    $('#formMuestreo').submit();
});
// ===============================================================================================



// ========================================= [ RESULTADO ] =========================================
var muestra_Perfil = function() {
    var JsonDatos = $('#txtJsonValores').val();
    var dato = JSON.parse(JsonDatos);
    var arrayPerfil = JSON.parse(dato.arrayPerfil);
    var arrayExamen = JSON.parse(dato.arrayExamen);
    var arrayVariable = JSON.parse(dato.arrayVariable);
    var arrayListaVariable = JSON.parse(dato.arrayListaVariable);
    
    for (var j = 0; j < arrayPerfil.length; j++)
    {
        var panel_Perfil = 'id_panelPanel_Contenedor_Perfil' + '-' + j;
        var body_Perfil = 'id_panelBody_Contenedor_Perfil' + '-' + j;
        
        var jsonDato_Perfil = {};
        jsonDato_Perfil.idgrupo = arrayPerfil[j].idgrupo;
        jsonDato_Perfil.grupo = arrayPerfil[j].grupo;
        
        $('#divContenedor_Perfil_Examen_Variable').append(
           '<div id="' + panel_Perfil + '" class="panel panel-primary">' + 
                '<div class="panel-heading">' + 
                  '<div class="row">' + 
                    '<div class="col-xs-12 col-sm-12 col-md-3 col-lg-10">' + 
                      '<input type="hidden" id="txtHidden_idlab_grupos-' + j + '" value="' + arrayPerfil[j].idgrupo + '">' +
                      '<textarea id="txtHiddenlab_grupos-' + j + '" style="display:none;">' + JSON.stringify(jsonDato_Perfil) + '</textarea>' + 
                      '<button type="button" class="btn btn-warning btn-sm" onclick="agregaExamen(' + arrayPerfil[j].idgrupo + ')" aria-label="Left Align" title="Click para agregar Exámen! ">' + 
                          '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>' + 
                      '</button> ' + 
                      arrayPerfil[j].grupo + 
                    '</div>' + 
                  '</div>' + 
                '</div>' + 
                '<div id="' + body_Perfil + '" class="panel-body">' + 
              '</div>'
        );
        
        for(var e = 0; e < arrayExamen.length; e++)
        {
            if(arrayPerfil[j].idgrupo == arrayExamen[e].idgrupo)
            {
                var idExtra = j + '-' + e;
                var panel_Examen = 'id_panelPanel_Contenedor_Examen_X_' + idExtra;
                var body_Examen = 'id_panelBody_Contenedor_Examen_X_' + idExtra;
                var tableExamen_Responsive = 'tableResponsive_' + panel_Examen;
                var tableExamen = 'table_' + panel_Examen;
                var tbodyExamen = 'tbody_' + panel_Examen;
                var divExamen_listaVariables = 'divExamen' + panel_Examen;
                var cabeceraExamen = '';
                var panel = '';
                
                var jsonDato_Examen = {};
                jsonDato_Examen.idlab_examenes = arrayExamen[e].idexamen;
                jsonDato_Examen.codigo = arrayExamen[e].codigo;
                jsonDato_Examen.examen = arrayExamen[e].examen;
                jsonDato_Examen.idlab_examenes_resultado = arrayExamen[e].idlab_examenes_resultado;
                jsonDato_Examen.orden = arrayExamen[e].orden;
                jsonDato_Examen.respuesta = arrayExamen[e].respuesta; // ESTOY AQUI....
                var examenCabecera = arrayExamen[e].examen + ' (' + arrayExamen[e].codigo + ')';
                var examen_resultado_formula = arrayExamen[e].idlab_examenes_resultado;
                idlab_examenesArray.push(arrayExamen[e].idexamen);

                if(examen_resultado_formula > 0)
                {
                    cabeceraExamen = 
                        '<button type="button" class="btn btn-danger btn-md" onclick="mostrarDetalleFormula(' + arrayExamen[e].idlab_examenes_resultado + ')" title="Pulse Click para ver la Fórmula Aplicada! ">' +
                            '<span class="fa fa-flask" aria-hidden="true"></span> Ver Fórmula' + 
                        '</button> ' + examenCabecera;
                    panel = 'panel panel-success';
                }
                else
                {
                    cabeceraExamen = 
                        '<span class="glyphicon glyphicon-list-alt"></span> ' + examenCabecera;
                    panel = 'panel panel-default';
                }
                var metodoVerVariable = "mostrarDetalle_Variable('" + body_Examen + "', '" + tableExamen_Responsive + "', '" + idExtra + "')";
                if(idPrimeraCajitaRespuesta == '')
                    idPrimeraCajitaRespuesta = idExtra;
                
                $('#' + body_Perfil).append(
                    '<div id="' + panel_Examen + '" class="' + panel + '">' + 
                        '<div class="panel-heading">' + 
                          '<div class="row">' + 
                            '<div id="divEncabezadoExamen-' + idExtra + '" class="col-xs-12 col-sm-3 col-md-2 col-lg-2">' + 
                              '<input type="text" name="respuesta" id="txtValorResultado_X_' + idExtra + '" class="form-control input-sm" style="font-size: 14px;" value="' + formatoMiles(arrayExamen[e].respuesta) + '">' + 
                            '</div>' + 
                            '<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">' + 
                                '<input type="hidden" id="txtHidden_idlab_examenes_X_' + idExtra + '" value="' + arrayExamen[e].idexamen + '">' +
                                '<input type="hidden" id="txtHidden_examen_X_' + idExtra + '" value="' + examenCabecera + '">' +
                                '<input type="hidden" id="txtHidden_idlab_examenes_resultado_X_' + idExtra + '" value="' + arrayExamen[e].idlab_examenes_resultado + '">' + 

                                '<textarea id="txtHiddenlab_examenes_X_' + idExtra + '" style="display:none;">' + JSON.stringify(jsonDato_Examen) + '</textarea>' + 
                                cabeceraExamen + 
                            '</div>' + 
                            '<div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">' + 
                                '<input type="hidden" id="txtMuestraContenidoExamen_X_' + idExtra + '" class="form-control input-sm" value="0">' + 
                                '<button type="button" class="btn btn-primary btn-md" onclick="' + metodoVerVariable + '" title="Pulse Click para ver Detalle de Variables! ">' +
                                    '<span class="glyphicon glyphicon-tasks" aria-hidden="true"></span>' + 
                                '</button> ' + 
                            '</div>' + 
                          '</div>' + 
                        '</div>' + 
                        '<div id="' + body_Examen + '" class="panel-body">' + 

                            '<div id="' + tableExamen_Responsive + '" class="table-responsive" style="max-height: 250px; overflow-y: auto; border: 1px solid #374850; display:none;">' + 
                                '<table id="' + tableExamen + '" class="table table-bordered table-hover table-condensed">' + 
                                  '<thead style="background: rgb(55, 72, 80); color: rgb(243, 234, 240); font-size: 15px;">' + 
                                    '<tr>' + 
                                      '<th class="col-xs-5">Variable</th>' + 
                                      '<th class="col-xs-1" style="text-align: right;">Valor Inf.</th>' + 
                                      '<th class="col-xs-1" style="text-align: right;">Valor Sup.</th>' + 
                                      '<th class="col-xs-1" style="text-align: right;">Udd</th>' + 
                                      '<th>Método</th>' + 
                                    '</tr>' + 
                                  '</thead>' + 
                                  '<tbody id="' + tbodyExamen + '" class="table-success" style="font-size: 14px;">' + 
                                  '</tbody>' + 
                                '</table>' + 
                            '</div>' + 

                            '<div class="row">' + 
                                '<div class="col-xs-4"></div>' + 
                                '<div class="col-xs-8" id="' + divExamen_listaVariables + '">' + 
                                '</div>' + 
                            '</div>' + 

                        '</div>' + 
                      '</div>'
                );
                
                $('#txtValorResultado_X_' + idExtra).keypress(function(e) {
                    if(e.which == 13) {
                        var valorResultado = parseFloat(this.value);
                        var idValorResultado = $(this).attr('id');
                        var identificador = (idValorResultado).split('_X_')[1];
                        var tableExamen = 'table_' + 'id_panelPanel_Contenedor_Examen_X_' + identificador;
                        muestraResultadoVariableRango(valorResultado, idValorResultado, identificador, tableExamen, 1);
                    }
                });
                // $('#txtValorResultado_X_' + idExtra).blur(function() {
                //     var valorResultado = parseFloat(this.value);
                //     if(valorResultado > 0)
                //     {
                //         var valorResultado = parseFloat(this.value);
                //         var idValorResultado = $(this).attr('id');
                //         var identificador = (idValorResultado).split('_X_')[1];
                //         var tableExamen = 'table_' + 'id_panelPanel_Contenedor_Examen_X_' + identificador;
                //         muestraResultadoVariableRango(valorResultado, idValorResultado, identificador, tableExamen, 1);
                //         this.value = formatoMiles(this.value);
                //     }
                // });

                var ocultarBodyExamen = 1;
                for (var v = 0; v < arrayVariable.length; v++)
                {
                    if(arrayExamen[e].idexamen == arrayVariable[v].idexamen)
                    {
                        var metodo = arrayVariable[v].metodo == null? '' : arrayVariable[v].metodo;
                        var jsonVariable = JSON.stringify(arrayVariable[v]);
                        var rangoEdad = "Edad entre " + arrayVariable[v].edad_inicial + " y " + arrayVariable[v].edad_final;

                        $('#' + tbodyExamen).append(
                            '<tr id="rowVariable-' + i + '">' + 
                              '<td style="display: none;" id="td_datosVariable-' + i + '">' + jsonVariable + '</td>' + 
                              '<td style="display: none;" id="td_tipodato-' + i + '">' + arrayVariable[v].tipodato + '</td>' + 
                              '<td title="' + rangoEdad + '">' + arrayVariable[v].variable + '</td>' + 
                              '<td id="td_valorInf' + i + '" style="text-align: right;">' + arrayVariable[v].valor_inf + '</td>' + 
                              '<td id="td_valorSup' + i + '" style="text-align: right;">' + arrayVariable[v].valor_sup + '</td>' + 
                              '<td style="text-align: right;">' + arrayVariable[v].unidad + '</td>' + 
                              '<td>' + metodo + '</td>' + 
                            '</tr>'
                        );
                        // ------------------------------------------------------------------------------------------------
                        /*
                        opcion_simple   => 2
                        opcion_multiple => 3
                        */
                        var ocultarCajitaAnterior_Respuesta = 0;
                        if(arrayVariable[v].tipodato == 2)
                        {
                            $('#' + divExamen_listaVariables).append('<div id="contenedor_Radio-' + i + '"></div>');
                            for (var li = 0; li < arrayListaVariable.length; li++)
                            {
                                if(arrayVariable[v].idlab_variables == arrayListaVariable[li].idlab_variables)
                                {
                                    var lis_variable = arrayListaVariable[li].lis_variable;
                                    $('#contenedor_Radio-' + i).append(
                                        '<div class="radio">' + 
                                          '<label>' + 
                                            '<input type="radio" id="radio-' + i + '-' + li + '" value="' + lis_variable + '"> ' + lis_variable + 
                                          '</label>' + 
                                        '</div>'
                                    );
                                }
                            }
                            ocultarCajitaAnterior_Respuesta = 1;
                            ocultarBodyExamen = 0;
                        }
                        else if(arrayVariable[v].tipodato == 3)
                        {
                            $('#' + divExamen_listaVariables).append('<div id="contenedor_Check-' + i + '"></div>');
                            for (var li = 0; li < arrayListaVariable.length; li++)
                            {
                                if(arrayVariable[v].idlab_variables == arrayListaVariable[li].idlab_variables)
                                {
                                    var lis_variable = arrayListaVariable[li].lis_variable;
                                    $('#contenedor_Check-' + i).append(
                                        '<label class="checkbox-inline">' + 
                                            '<input type="checkbox" id="check-' + i + '-' + li + '" value="' + lis_variable + '"> ' + lis_variable + 
                                        '</label>' + 
                                        '<br>'
                                    );
                                }
                            }
                            ocultarCajitaAnterior_Respuesta = 1;
                            ocultarBodyExamen = 0;
                        }

                        if(ocultarCajitaAnterior_Respuesta == 1)
                        {
                            $('#divEncabezadoExamen-' + idExtra).html('');
                            numeroCajaNumero--;
                            // $('#txtValorResultado_X_' + idExtra).hide();
                        }
                        // ------------------------------------------------------------------------------------------------
                        i++;
                    }
                }
                
                if(examen_resultado_formula > 0)
                    numeroCajaNumero = numeroCajaNumero;
                else
                    numeroCajaNumero++;

                if(ocultarBodyExamen == 1)
                    $('#' + body_Examen).hide();
            }
        }
    }
}
var mostrarDetalle_Variable = function(body_Examen, tableExamen_Responsive, idExtra) {
    var ocultarContenidoExamen = $('#txtMuestraContenidoExamen_X_' + idExtra).val();

    if(ocultarContenidoExamen == 0)
    {
        $('#' + body_Examen).show();
        $('#' + tableExamen_Responsive).show();
        $('#txtMuestraContenidoExamen_X_' + idExtra).val(1);
    }
    else
    {
        $('#' + body_Examen).hide();
        $('#' + tableExamen_Responsive).hide();
        $('#txtMuestraContenidoExamen_X_' + idExtra).val(0);
    }
}
var agregaExamen = function(idgrupo) {
    // bootbox.alert('MOSTRAR...LOS EXAMENES EN UNA VENTANA UNICAMENTE... DE ESTE PERFIL...! ' + idgrupo);
    $('#vModalBuscadorExamen').modal({
        backdrop: 'static',
        keyboard: true
    });
    idlab_grupos_global = idgrupo;
}
$('#btnBuscarExamenGrupo').click(function() {
    /*
    parametros:
        edad
        sexo
        idgrupo
    */
    $.ajax({
        url: ruta + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            buscarTodo: buscarTodo,
            idlab_grupos: idlab_grupos,
            datosLab_Ima: datosLab_Ima,
            idlab_examenesArray: idlab_examenesArray, // este campo lo estoy usando para obtener todos los ides de examenes en "resultado"
        },
        success: function(dato) {
            $('#tbodyExamenes').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var id_examenes = opcionButton == 1? data[i].idlab_examenes : data[i].idima_examenes;
                    
                    var arrayParametro = {
                        idima_examenes: id_examenes,
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        idEspecialidad: data[i].idEspecialidad,
                        codigoEspecialidad: data[i].codigoEspecialidad,
                        nombreEspecialidad: data[i].nombreEspecialidad,
                        idlab_grupos: idlab_grupos
                    };
                    // generarListaExamenes(arrayParametro);
                    generarListaExamenesGrupo(arrayParametro);
                }
                /*
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                seleccionarExamenesNuevamente();
                
                // Aqui va a entrar solo cuando se pulse click en los botones de cada grupo de laboratorio
                if(idlab_grupos > 0)
                {
                    examenesSeleccionados();
                    datosTablaLab = Array.from(new Set(datosTablaLab));
                    totalExamenSeleccionLab = datosTablaLab.length;
                    $('#spanTotalExamenesLaboratorio, #spanTotalExamenSeleccion').text(totalExamenSeleccionLab);
                }
                */
            }
            else
                $('#tbodyExamenes').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarListaExamenesGrupo = function(arrayParametro) {
    var idima_examenes = arrayParametro['idima_examenes'];
    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var idEspecialidad = arrayParametro['idEspecialidad'];
    var codigoEspecialidad = arrayParametro['codigoEspecialidad'];
    var nombreEspecialidad = arrayParametro['nombreEspecialidad'];
    var idlab_grupos = arrayParametro['idlab_grupos'];
    
    var checkbox = '<div class="form-check">'
                    +   '<input type="checkbox" class="form-check-input" id="checkSeleccionar' + i + '">'
                    +   ' <label class="form-check-label" for="checkSeleccionar' + i + '" style="cursor: pointer;">'
                    +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                    +   ' </label>'
                 +  '</div>';
    
    $('#tbodyExamenes').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdima_examenes' + i + '">' + idima_examenes + '</td>'
          + '<td style="display: none;" id="tdIdEspecialidad' + i + '">' + idEspecialidad + '</td>'
          + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td id="tdEspecialidad' + i + '">' + nombreEspecialidad + ' (' + codigoEspecialidad + ')' + '</td>'
          + '<td>' + checkbox + '</td>'
       +'</tr>'
    );
    
    $('#checkSeleccionar' + i).click(function() {
        examenesSeleccionados();

        if($(this).is(':checked'))
            opcionButton == 1? totalExamenSeleccionLab++ : totalExamenSeleccion++;
        else
            opcionButton == 1? totalExamenSeleccionLab-- : totalExamenSeleccion--;

        opcionButton == 1?  $('#spanTotalExamenesLaboratorio, #spanTotalExamenSeleccion').text(totalExamenSeleccionLab) : 
                            $('#spanTotalExamenesImagenologia, #spanTotalExamenSeleccion').text(totalExamenSeleccion);
        $('#txtBuscarExamen').focus();
    });
    
    if(idlab_grupos > 0) // Al pulsar click por cada botón de grupo
    {
        $('#checkSeleccionar' + i).prop('checked', true);
    }
    
    i++;
}
$('#btnSeleccionar').click(function() {
    // btnBuscarExamen
    // alert(55);


    var idsLabExamenArray = [];
    $("#tbodyExamenes > tr").each(function() {
        var identificador = ($(this).attr('id')).split('-')[1];
        var idlab_examenes = $('#tdIdima_examenes' + identificador).text();
        // console.log(identificador + ' <==>> ' + idlab_examenes);
        idsLabExamenArray.push(idlab_examenes);
    });
    $.ajax({
        url: 'agregaExamenGrupo' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idsLabExamenArray: idsLabExamenArray
        },
        success: function(dato) {
            // $('#tbodyExamenes').empty();
            // var JSONString = dato;
            // var data = JSON.parse(JSONString);

            // if(data.length > 0)
            // {
            //     for(var i = 0; i < data.length; i++)
            //     {
            //         var arrayParametro = {
            //             idlab_examenes: data[i].idlab_examenes,
            //             codigo: data[i].codigo,
            //             nombre: data[i].nombre,
            //             idEspecialidad: data[i].idEspecialidad,
            //             codigoEspecialidad: data[i].codigoEspecialidad,
            //             nombreEspecialidad: data[i].nombreEspecialidad,
            //             idlab_grupos: idlab_grupos
            //         };
            //         generarListaExamenes(arrayParametro);
            //     }
            // }
            // else
            //     $('#tbodyExamenes').append(
            //         '<tr>' + 
            //             '<td>Vacío...</td>' +
            //         '</tr>'
            //     );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });



});
var mostrarDetalleFormula = function(idlab_examenes_resultado) {
    $('#vModalFormulaAplicadaResultadoExamenes').modal({
        // backdrop: 'static',
        keyboard: true
    });
    
    var formulaResultado = '';
    var valorResultado = 0;
    $('#contenidoExamenesVariables').empty();
    for(var j = 0; j < parametroExamenesFormula.length; j++)
    {
        if(parametroExamenesFormula[j].idlab_examenes_resultado == idlab_examenes_resultado)
        {
            var arrayFormula_Examenes = JSON.parse(parametroExamenesFormula[j].arrayFormula_Examenes);
            for(var k = 0; k < arrayFormula_Examenes.length; k++)
            {
                var parametro = arrayFormula_Examenes[k].lab_parametro_variable;
                var valor = formatoMiles(arrayFormula_Examenes[k].valor);

                if(arrayFormula_Examenes[k].idlab_examenes == idlab_examenes_resultado)
                {
                    formulaResultado = parametro;
                    valorResultado = valor;
                }
                else
                {
                    $('#contenidoExamenesVariables').append(
                        '<li class="list-group-item">' + 
                            arrayFormula_Examenes[k].nombreExamen + 
                            '<span class="badge">' + parametro + ' => ' + valor + '</span>' + 
                          '</li>'
                    );
                }
            }
            $('#textoFormula').html('<small>FÓRMULA: </small> ' + formulaResultado + ' = ' + valorResultado);
        }
    }
}
var muestraResultadoVariableRango = function(valorResultado, idValorResultado, identificador, idTable, moverScrollAbajo) {
    // idTable = (idTable).split('-')[0] + '-' + identificador; // EJEMPLO:     idTable => 'tablaContenedorVariable' + '-' + i;
    var NOexisteValorEntreRango = 0;

    $("#" + idTable + " tbody tr").each(function (index) 
    {
        var identif = ($(this).attr('id')).split('-')[1];
        var valor_inf = parseFloat($('#td_valorInf' + identif).text());
        var valor_sup = parseFloat($('#td_valorSup' + identif).text());
        
        if(valorResultado >= valor_inf && valorResultado <= valor_sup)
        {
            $("#rowVariable-" + identif).attr('class', 'success');
        }
        else
        {
            $("#rowVariable-" + identif).attr('class', 'default');
            NOexisteValorEntreRango++;
        }
    });
    if($("#" + idTable + " tbody tr").length == NOexisteValorEntreRango)
        $("#id_panelPanel_Contenedor_Examen_X_" + identificador).attr('class', 'panel panel-danger');
    else
        $("#id_panelPanel_Contenedor_Examen_X_" + identificador).attr('class', 'panel panel-default');


    // NEXT CAJA AL PRESIONAR ENTER...
    var nextIdCajita = '';
    var salir = 0;
    var ultimoPanel = 0;
    $("#divContenedor_Perfil_Examen_Variable input[name=respuesta]").each(function() {
        var identificador = $(this).attr('id');
        var id = (identificador).split('_X_')[1];
        var idlab_examenes_resultado = $('#txtHidden_idlab_examenes_resultado_X_' + id).val();

        var id_nuevo = (id).split('-');
        ultimoPanel = id_nuevo[0];
        
        if(salir == 1)
        {
            if(idlab_examenes_resultado == 'null')
            {
                nextIdCajita = identificador;
                return false;
            }
            else
                nextIdCajita = '';
        }
        else
        {
            if(idValorResultado == identificador)
                salir = 1;
        }
    });
    if(nextIdCajita != '')
        $('#' + nextIdCajita).select();
    
    
    if(moverScrollAbajo == 1)
    {
        var id_cajitaOriginal = (identificador).split('-');
        var bajarScroll = 0;
        if(id_cajitaOriginal[0] == ultimoPanel)
        {
            var posicionActual = $(window).scrollTop();
            var alturaPanelExamen = $('#id_panelPanel_Contenedor_Examen_X_' + identificador).height();
            bajarScroll = parseFloat(posicionActual) + parseFloat(alturaPanelExamen);
        }
        else
        {
            var totalAlto = 0;
            for(var h = 0; h < ultimoPanel; h++)
            {
                var altoPanel = $('#id_panelPanel_Contenedor_Perfil-' + h).height();
                totalAlto = parseFloat(totalAlto) + parseFloat(altoPanel);
            }
            bajarScroll = totalAlto + parseFloat( $('#divVistaDatos').height() ) + 100;
        }
        $("html, body").animate({scrollTop: bajarScroll + "px"});
    }
    
    // calculaFormula();
    calculaFormulaExamenes();
}
var formatoMiles = function(numero) {
    var valor = numero.toString();
    return parseFloat(valor.replace(/,/g, ""))
                .toFixed(2)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
var calculaFormulaExamenes = function() {
    var examen = [];
    $("#divContenedor_Perfil_Examen_Variable input[name=respuesta]").each(function() {
        var identificador = ($(this).attr('id')).split('_X_')[1];
        var datosFila = {};

        datosFila.idlab_examenes = $('#txtHidden_idlab_examenes_X_' + identificador).val();
        datosFila.nombreExamen = $('#txtHidden_examen_X_' + identificador).val();
        datosFila.idlab_examenes_resultado = $('#txtHidden_idlab_examenes_resultado_X_' + identificador).val();
        // datosFila.respuesta = ((this.value).toString()).replace(',', '');
        datosFila.respuesta = ((this.value).toString()).replace(/,/g, '');
        examen.push(datosFila);
    });
    $.ajax({
        url: 'resultadoFormulaExamenes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: JSON.stringify(examen)
        },
        success: function(dato) {
            var data = JSON.parse(dato);
            parametroExamenesFormula = data;

            $("#divContenedor_Perfil_Examen_Variable input[name=respuesta]").each(function() {
                var identificador = ($(this).attr('id')).split('_X_')[1];
                var datosFila = {};
                var idlab_examenes_resultado = $('#txtHidden_idlab_examenes_resultado_X_' + identificador).val();

                if(idlab_examenes_resultado > 0)
                {
                    for(var i = 0; i < data.length; i++)
                    {
                        var arrayFormula_Examenes = JSON.parse(data[i].arrayFormula_Examenes);
                        for(var k = 0; k < arrayFormula_Examenes.length; k++)
                        {
                            // if(data[k].idlab_examenes == data[k].idlab_examenes_resultado && data[k].idlab_examenes == idlab_examenes_resultado)
                            if(arrayFormula_Examenes[k].idlab_examenes == data[i].idlab_examenes_resultado && idlab_examenes_resultado == data[i].idlab_examenes_resultado)
                            {
                                $('#txtValorResultado_X_' + identificador).val( arrayFormula_Examenes[k].valor );
                                break;
                            }
                        }
                    }
                }
            });
        },
        error: function(xhr, ajaxOptions, thrownError) {
            
        }
    });
}
// ===============================================================================================



// ========================================= [ GUARDAR ] =========================================
$('#btnGuardar').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {

            var resultado_grupo = [];
            $("#divContenedor_Perfil_Examen_Variable > div").each(function() {
                var identificador = ($(this).attr('id')).split('-')[1];
                var idlab_grupos = $('#txtHidden_idlab_grupos-' + identificador).val();
                var lab_grupos = $('#txtHiddenlab_grupos-' + identificador).val();       

                var resultado_examen = [];
                $("#id_panelBody_Contenedor_Perfil-" + identificador + " > div").each(function() {
                    var id = $(this).attr('id');
                    var identificador = ($(this).attr('id')).split('_X_')[1];
                    // var idlab_examenes = $('#txtHidden_idlab_examenes_X_' + identificador).val();
                    
                    var resultado_variable = [];
                    $("#table_" + id + " tbody tr").each(function(index)
                    {
                        var identificador = ($(this).attr('id')).split('-')[1];
                        /*
                        opcion_simple   => 2
                        opcion_multiple => 3
                        */
                        if($('#td_tipodato-' + identificador).text() == 2) // Radio Button
                            var eachId = "contenedor_Radio-" + identificador + " > div > label > input";
                        else if($('#td_tipodato-' + identificador).text() == 3) // Checkbox
                            var eachId = "contenedor_Check-" + identificador + " > label > input";

                        var resultado_lista = [];
                        $("#" + eachId).each(function() {
                            if ($( '#' + $(this).attr("id") ).is(':checked'))
                                var respuesta = 1;
                            else
                                var respuesta = 0;
                            
                            var datosFila = {};
                            datosFila.lis_variable = $( '#' + $(this).attr("id") ).val();
                            datosFila.respuesta = respuesta;
                            resultado_lista.push(datosFila);
                        });

                        var datosFila = {};
                        datosFila.variable = $('#td_datosVariable-' + identificador).text();
                        datosFila.resultado_lista = resultado_lista;
                        resultado_variable.push(datosFila);
                    });
                    var datosFila = {};
                    datosFila.idlab_grupos = idlab_grupos;
                    datosFila.lab_examen = $('#txtHiddenlab_examenes_X_' + identificador).val();
                    datosFila.valorResultadoExamen = $('#txtValorResultado_X_' + identificador).val();
                    datosFila.resultado_variable = resultado_variable;
                    resultado_examen.push(datosFila);
                });
                var datosFila = {};
                datosFila.lab_grupos = lab_grupos;
                datosFila.resultado_examen = resultado_examen;
                resultado_grupo.push(datosFila);
            });

            $('#txtJsonDatosGuardar').val( JSON.stringify(resultado_grupo) );
            $("#form").submit();
        }
    });
});
// ===============================================================================================



// ========================================= [ REPORTE ] =========================================
var reporteResultado = function(id) {
    $('#vImprimeResultado').modal({ backdrop: 'static', keyboard: false });
    
    $('.progress').show();
    $('#secccionImprimeResultado').html("").append($("<iframe></iframe>", {
        src: 'resultado_lab/imprimeProgramacionResultado/' + id,
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
}
// ===============================================================================================