var url = '';
var i = 0;
var ordenVariable = 1;
var yyy = 0;

$(document).ready(function() {
    url = 'Create';

    $(document).keydown(function(tecla){ 
        if(tecla.which == 120) { // F9   (SIRVE PARA ABRIR EL MODAL DE BUSQUEDA DE VARIABLES)
            $('#btnBuscarVariable').click();
        }
    });

    if($('#txtScenario').val() == 'edit')
    {
        url = '';
        var JSONString = $('#txtListaVariables').val();
        var data = JSON.parse(JSONString);
        var listaGrupos = jQuery.parseJSON( $('#txtListagrupos').val() );
        yyy = data;
        for(var i = 0; i < data.length; i++)
        {
            var arrayParametro = {
                id: data[i].idlab_variables,
                nombre: data[i].nombre,
                idlab_grupos: data[i].idlab_grupos,
                permite_llenado: data[i].permite_llenado,
                listaGrupos: listaGrupos
            };
            generarListaVariable(arrayParametro, data[i].orden);
        }
    }
});

$('#btnBuscarVariable').click(function() {
    $('#divCheckBuscadorVariables').empty();

    // Esto es para que no se repitan en la lista de VARIABLES
    $('#txtHiddenListaVariables').val('');
    var datosTabla = [];
    $("#tablaContenedorDatos tbody tr").each(function (index) 
    {
        var identificador = ($(this).attr('id')).split('-')[1];

        if($.isNumeric(identificador))
            datosTabla.push($('#tdId' + identificador).text());
    });
    if(datosTabla.length > 0)
    {
        var jsonListaTipoVariable = JSON.stringify(datosTabla);
        $('#txtHiddenListaVariables').val(jsonListaTipoVariable);
    }

    $('#btnBuscador').click();

    $('#vModalBuscador').modal({
      backdrop: 'static',
      keyboard: true
    });

});
$('#txtBuscador').keypress(function(e) {

    if(e.which == 13) {
        $('#btnBuscador').click();
    }
});
$('#btnBuscador').click(function() {
    $.ajax({
        url: 'buscaVariables' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador').val(),
            idalmacen: $('#idalmacen').val()
        },
        success: function(dato) {
            $('#divCheckBuscadorVariables').empty();
            $("#checkVariable").prop('checked', false);
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            for(var i = 0; i < data.length; i++)
            {
                var sigla = data[i].sigla != ''? ' ('+ data[i].sigla+')' : '';
                var arrayParametro = {
                    id: data[i].id,
                    nombre: data[i].nombre + sigla,
                    permite_llenado: data[i].permite_llenado
                };
                generarTuplaVariable(arrayParametro);
            }
            $('#txtBuscador').focus();
        },
        error: function(xhr, ajaxOptions, thrownError) {
            bootbox.alert('Por actualize la página! ');
        }
    });
});
var generarTuplaVariable = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var permite_llenado = arrayParametro['permite_llenado'];
    
    $('#divCheckBuscadorVariables').append(
        '<label class="checkbox-inline">' +
            '<input type="checkbox" name="variable" value="' + id + '|*|' + nombre + '|*|' + permite_llenado + '" onClick="selecciona_individual_variable(this.checked);">' +
            nombre +
        '</label>'+
        '<br>'
    );
}
function selecciona_individual_variable(isChecked) {
    if($("input[name='variable']:checked").length == $("input[name='variable']").length)
        $("#checkVariable").prop('checked', true);
    else
        $("#checkVariable").prop('checked', false);
}
function selecciona_todas_las_variables(isChecked) {
    if(isChecked) {
        $('input[name="variable"]').each(function() { 
            this.checked = true; 
        });
    }
    else {
        $('input[name="variable"]').each(function() {
            this.checked = false;
        });
    }
}


$('#btnSeleccionarVariables').click(function() {
    var datosTabla = [];
    $.each($("input[name='variable']:checked"), function() {
        var dato = $(this).val();
        var dato = dato.split('|*|');

        datosTabla.push(dato[0]);
    });
    var JSONString = $('#txtHiddenListaVariables').val();
    var result = [];
    if(JSONString != '')
    {
        var data = JSON.parse(JSONString);
        result = $(datosTabla).not(data);
    }

    var listaGrupos = jQuery.parseJSON( $('#txtListagrupos').val() );
    $.each($("input[name='variable']:checked"), function() {
        var dato = $(this).val();
        var dato = dato.split('|*|');

        var arrayParametro = {
            id: dato[0],
            nombre: dato[1],
            permite_llenado: dato[2],
            listaGrupos: listaGrupos
        };

        if(result.length > 0)
        {
            for(var i = 0; i < result.length; i++)
            {
                if(result[i] == dato[0])
                {
                    generarListaVariable(arrayParametro, ordenVariable);
                    ordenVariable++;
                }
            }
        }
        else
        {
            generarListaVariable(arrayParametro, ordenVariable);
            ordenVariable++;
        }
    });
    $('#vModalBuscador').modal('toggle');
});
var generarListaVariable = function(arrayParametro, ordenVariable) {
    var botonDelete = '';
    var botonDetalleTipoVariable = '';
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var idlab_grupos = arrayParametro['idlab_grupos'];
    var permite_llenado = arrayParametro['permite_llenado'];
    var listaGrupos = arrayParametro['listaGrupos'];
    $('#idListaVacia').remove();
    
    var comboBoxGrupo = "<select id='selectIdGrupo" + i + "' class='form-control input-sm'>";
    for(var j = 0; j < listaGrupos.length; j++)
    {
        if(idlab_grupos == listaGrupos[j].id)
            comboBoxGrupo += "<option value='" + listaGrupos[j].id + "' selected>" + listaGrupos[j].nombre + "</option>";
        else
            comboBoxGrupo += "<option value='" + listaGrupos[j].id + "'>" + listaGrupos[j].nombre + "</option>";
    }
    comboBoxGrupo += "</select>";

    if($('#txtScenario').val() != 'view')
    {
        botonDelete = 
            '<td class="col-xs-1">'
                +'<span class="glyphicon glyphicon-trash" aria-hidden="true" onclick="eliminaTupla('+i+')" style="cursor: pointer;" title="Eliminar"></span>'
            +'</td>';
        if(permite_llenado == 1)
        {
            botonDetalleTipoVariable = 
                '<td class="col-xs-1">'
                    +'<span class="glyphicon glyphicon-list-alt" aria-hidden="true" onclick="verDetalleTipoVariable('+i+', ' + id + ')" style="cursor: pointer;" title="Ver Detalle Tipo Variable"></span>'
                +'</td>';
        }
    }
    
    $('#tablaContenedorDatos').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdId' + i + '">' + id + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td class="col-xs-4">'  + comboBoxGrupo + '</td>'
          + '<td class="col-xs-1">'
          +     '<input type="text" class="form-control input-sm" id="txtOrden' + i + '" value="' + ordenVariable + '">'
          + '</td>'
          + botonDelete
          + botonDetalleTipoVariable
       +'</tr>'
    );

    $('#txtOrden' + i).click(function() {
        $('#' + $(this).attr('id')).select();
    });
    $('#txtOrden' + i).blur(function() {
        if(!$.isNumeric(this.value))
            this.value = 1;
    });

    $('#thCantidadVariables').text( 'Tipo Variable (' + $("#tablaContenedorDatos tbody tr").length + ')' );
    
    i++;
}
var eliminaTupla = function(id) {
    $('#row-' + id).remove();
    $('#thCantidadVariables').text( 'Tipo Variable (' + $("#tablaContenedorDatos tbody tr").length + ')' );

    if($("#tablaContenedorDatos tbody tr").length == 0)
      $('#tablaContenedorDatos').append(
          '<tr id="idListaVacia">' + 
              '<td>Lista Vacia...</td>' +
          '</tr>'
      );
}
var verDetalleTipoVariable = function(id, idtabla) {
    var variable = $('#tdNombre' + id).text();
    $('#tituloModal').text(variable);
    $('#cuerpoTipoVariable').empty();

    $.ajax({
        url: 'buscaTipoVariables' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            idtabla: idtabla
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if(data.length > 0)
            {
                $('#idListaVaciaTipoVariable').remove();
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        nombre: data[i].lis_variable
                    };
                    generarTuplaTipoVariable(arrayParametro);
                }
            }
            else
              $('#tablaContenedorDatosTipoVariable').append(
                  '<tr id="idListaVaciaTipoVariable">' + 
                      '<td>Lista Vacia...</td>' +
                  '</tr>'
              );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });

    $('#vModalTipoVariables').modal({
      backdrop: 'static',
      keyboard: true
    });
}
var generarTuplaTipoVariable = function(arrayParametro) {
    var nombre = arrayParametro['nombre'];

    $('#tablaContenedorDatosTipoVariable').append(
       '<tr id="row-'+i+'">'
          + '<td id="tdNombreTipoVariable' + i + '">' + nombre + '</td>'
       +'</tr>'
    );
    $('#thCantidadTipoVariable').text( 'Tipo Variable (' + $("#tablaContenedorDatosTipoVariable tbody tr").length + ')' );
    i++;
}



// ========================================= [ GUARDAR ] =========================================
$('#btnGuardarExamen').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {
            $('#txtListaVariables').val('');
            
            // ------------------------------ LISTA DE VARIABLES ------------------------------
            var datosTabla = [];
            $("#tablaContenedorDatos tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];
                var datosFila = {};
                
                datosFila.id = $('#tdId' + identificador).text();
                if(datosFila.id > 0)
                {
                    datosFila.nombre = $('#tdNombre' + identificador).text();
                    datosFila.idlab_grupos = $('#selectIdGrupo' + identificador).val();
                    datosFila.orden = $('#txtOrden' + identificador).val();
                    datosTabla.push(datosFila);
                }
            });
            if(datosTabla.length > 0)
            {
                var jsonListaVariables = JSON.stringify(datosTabla);
                $('#txtListaVariables').val(jsonListaVariables);
            }
            
            $("#form").submit();
        }
    });
});
// ===============================================================================================