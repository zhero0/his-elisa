var url = '';
var i = 0;
var datosTablaLab = [];
var totalExamenSeleccion = 0;
var titleDobleClick = 'Presione Doble Click para Seleccionar! ';
var opcionButton = 1; //    1 => Laboratorio       2 => Imagenología
var totalExamenSeleccionLab = 0;

$(document).ready(function() {
    $('#divErrorPaciente').hide();
    $('#divErrorExamen').hide();
    
    if($('#txtScenario').val() == 'create')
    {
        url = 'Create';
    }

    if($('#txtScenario').val() == 'edit')
    {
        var datosPoblacion = $('#txtJsonHcl_poblacion').val();
        var dato = JSON.parse(datosPoblacion);
        
        $('#txtidhcl_poblacion').val(dato.id);
        $('#datoNumeroPaciente').text(dato.numero_historia);
        $('#datoNombrePaciente').text(dato.nombre);
        $('#datoApellidosPaciente').text(dato.primer_apellido + ' ' + dato.segundo_apellido);

        $('#txtHora_Minuto_AmPM').val( $('#txtHiddenHora').val() );
    }

    if($('#txtScenario').val() == 'create' || $('#txtScenario').val() == 'edit')
    {
        // GRUPOS
        var grupos = $('#grupos').val();
        var data = JSON.parse(grupos);
        $('#listaGrupo').empty();
        if(data.length > 0)
        {
            for(var i = 0; i < data.length; i++)
            {
                $('#listaGrupo').append(
                    '<button type="button" class="list-group-item" onclick="lab_grupoSelecciondo(' + data[i].id + ')">' + data[i].nombre + ' <span class="badge">' + data[i].total_examenes + '</span></button>'
                );
            }
        }

        // lista de "EXÁMENES"
        $('#tbodyExamenes').empty();
        var JSONString = $('#lab_examenesEspecialidad').val();
        var data = JSON.parse(JSONString);
        if(data.length > 0)
        {
            for(var i = 0; i < data.length; i++)
            {
                var arrayParametro = {
                    idima_examenes: data[i].idlab_examenes,
                    codigo: data[i].codigo,
                    nombre: data[i].nombre,
                    idEspecialidad: data[i].idEspecialidad,
                    codigoEspecialidad: data[i].codigoEspecialidad,
                    nombreEspecialidad: data[i].nombreEspecialidad
                };
                generarListaExamenes(arrayParametro);
            }

            if($('#txtScenario').val() == 'edit')
            {
                var JSONString = $('#examenesProgramados').val();
                var data = JSON.parse(JSONString);
                for(var i = 0; i < data.length; i++)
                {
                    datosTablaLab.push((data[i].idlab_examenes).toString());
                    totalExamenSeleccion++;
                }
                totalExamenSeleccionLab = datosTablaLab.length;
                $('#spanTotalExamenSeleccion').text(datosTablaLab.length);
                
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                seleccionarExamenesNuevamente();
            }
        }
        else
            $('#tbodyExamenes').append(
                '<tr>' + 
                    '<td>Vacío...</td>' +
                '</tr>'
            );
    }
    
    /*
    // ---------------------------- "ESPECIALIDADES" programados ----------------------------
    var JSONString = $('#especialidadProgramados').val();
    var data = JSON.parse(JSONString);
    if(data.length > 0)
    {
        $('#divInformacionProgramados').empty();
        for(var i = 0; i < data.length; i++)
        {
            var arrayParametro = {
                codigo: data[i].codigo,
                nombre: data[i].nombre,
                total: data[i].total
            };
            InformacionProgramados(arrayParametro);
        }
    }
    // --------------------------------------------------------------------------------------
    */
});

// ========================================= [ PACIENTE ] =========================================
$('#txtBuscarPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnModalPaciente').click();
    }
});
$('#btnModalPaciente').click(function() {
    $.ajax({
        url: 'buscaPacientes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscarPaciente').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);

            if(data.length == 1)
            {
                $('#txtidhcl_poblacion').val(data[0].id);
                $('#datoNumeroPaciente').text(data[0].numero_historia);
                $('#datoNombrePaciente').text(data[0].nombre);
                $('#datoApellidosPaciente').text(data[0].primer_apellido + ' ' + data[0].segundo_apellido);
            }
            else
            {
                $('#txtBuscadorPaciente').val( $('#txtBuscarPaciente').val() );
                $('#btnBuscarPaciente').click();

                $('#vModalBuscadorPaciente').modal({
                  backdrop: 'static',
                  keyboard: true
                });
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
$('#txtBuscadorPaciente').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarPaciente').click();
    }
});
$('#btnBuscarPaciente').click(function() {
    $.ajax({
        url: 'buscaPacientes' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscadorPaciente').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.length > 0)
            {
                $('#tbodyPaciente').empty();
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        id: data[i].id,
                        numero_historia: data[i].numero_historia,
                        nombre: data[i].nombre,
                        primer_apellido: data[i].primer_apellido,
                        segundo_apellido: data[i].segundo_apellido
                    };
                    generarTuplaPaciente(arrayParametro);
                }
                $('#txtBuscadorPaciente').focus();
            }
            else
                $('#tbodyPaciente').append(
                  '<tr id="idListaVaciaPaciente">' + 
                      '<td>Lista Vacia...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaPaciente = function(arrayParametro) {
    var id = arrayParametro['id'];
    var numero_historia = arrayParametro['numero_historia'];
    var nombre = arrayParametro['nombre'];
    var primer_apellido = arrayParametro['primer_apellido'];
    var segundo_apellido = arrayParametro['segundo_apellido'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaPaciente('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbodyPaciente').append(
       '<tr ondblclick="eventoDobleClick_Paciente('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdPaciente' + i + '">' + id + '</td>'
          + '<td id="tdNumeroHistoria' + i + '">' + numero_historia + '</td>'
          + '<td id="tdNombrePaciente' + i + '">' + nombre + '</td>'
          + '<td id="tdApellidos' + i + '">' + primer_apellido + ' ' + segundo_apellido + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Paciente = function(id, idtabla) {
    seleccionaPaciente(id, idtabla);
}
var seleccionaPaciente = function(id, idtabla) {
    $('#vModalBuscadorPaciente').modal('toggle');
    
    $('#txtidhcl_poblacion').val( $('#tdIdPaciente' + id).text() );
    $('#datoNumeroPaciente').text( $('#tdNumeroHistoria' + id).text() );
    $('#datoNombrePaciente').text( $('#tdNombrePaciente' + id).text() );
    $('#datoApellidosPaciente').text( $('#tdApellidos' + id).text() );

    $('#txtBuscarPaciente').val('');
}
// ================================================================================================



// ========================================== [ DATOS ] ==========================================
// [ Especialidad ]
$('#btnModal_Especialidad').click(function() {
    $('#vModalBuscadorEspecialidad').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Especialidad').click();
});
$('#txtBuscador_Especialidad').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Especialidad').click();
    }
});
$('#btnBuscar_Especialidad').click(function() {
    $.ajax({
        url: 'buscaEspecialidad' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Especialidad').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Especialidad').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombre: data[k].nombre
                    };
                    generarTuplaEspecialidad(arrayParametro);
                }
                $('#txtBuscador_Especialidad').focus();
            }
            else
                $('#tbody_Especialidad').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaEspecialidad = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombre = arrayParametro['nombre'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaEspecialidad('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Especialidad').append(
       '<tr ondblclick="eventoDobleClick_Especialidad('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_idhcl_cuaderno' + i + '">' + id + '</td>'
          + '<td id="td_nombre' + i + '">' + nombre + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Especialidad = function(id, idtabla) {
    seleccionaEspecialidad(id, idtabla);
}
var seleccionaEspecialidad = function(id, idtabla) {
    $('#vModalBuscadorEspecialidad').modal('toggle');
    
    $('#txtEspecialidad').val( $('#td_nombre' + id).text() );
    $('#idhcl_cuaderno').val( idtabla );
    $('#txtBuscador_Especialidad').val('');
}

// [ Médico Solicitante ]
$('#btnModal_Medico').click(function() {
    $('#vModalBuscadorMedicos').modal({
      backdrop: 'static',
      keyboard: true
    });
    $('#btnBuscar_Medico').click();
});
$('#txtBuscador_Medico').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscar_Medico').click();
    }
});
$('#btnBuscar_Medico').click(function() {
    $.ajax({
        url: 'buscaMedico' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: $('#txtBuscador_Medico').val()
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            $('#tbody_Medico').empty();
            
            if(data.length > 0)
            {
                for(var k = 0; k < data.length; k++)
                {
                    var arrayParametro = {
                        id: data[k].id,
                        nombres: data[k].nombres,
                        apellidos: data[k].apellidos
                    };
                    generarTuplaMedico(arrayParametro);
                }
                $('#txtBuscador_Medico').focus();
            }
            else
                $('#tbody_Medico').append(
                  '<tr>' + 
                      '<td>Lista Vacía...</td>' +
                  '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var generarTuplaMedico = function(arrayParametro) {
    var id = arrayParametro['id'];
    var nombres = arrayParametro['nombres'];
    var apellidos = arrayParametro['apellidos'];
    var botonSeleccion = '';

    botonSeleccion = 
            '<td class="col-lg-1">'
                +'<span class="glyphicon glyphicon-thumbs-up" aria-hidden="true" onclick="seleccionaMedico('+i+', ' + id + ')" style="cursor: pointer;" title="Click para Seleccionar"></span>'
            +'</td>';

    $('#tbody_Medico').append(
       '<tr ondblclick="eventoDobleClick_Medico('+i+', ' + id + ')" style="cursor: pointer;" title="' + titleDobleClick + '" id="rowEspecialidad-'+i+'">'
          + '<td style="display: none;" id="td_iddatosgenericos' + i + '">' + id + '</td>'
          + '<td id="td_nombres' + i + '">' + nombres + '</td>'
          + '<td id="td_apellidos' + i + '">' + apellidos + '</td>'
          + botonSeleccion
       +'</tr>'
    );
    
    i++;
}
var eventoDobleClick_Medico = function(id, idtabla) {
    seleccionaMedico(id, idtabla);
}
var seleccionaMedico = function(id, idtabla) {
    $('#vModalBuscadorMedicos').modal('toggle');
    
    var nombres = $('#td_nombres' + id).text() + ' ' + $('#td_apellidos' + id).text();
    $('#txtMedico').val( nombres );
    $('#iddatosgenericos').val( idtabla );
    $('#txtBuscador_Medico').val('');
}
// ===============================================================================================



// ====================================== [ ESPECIALIDADES ] ======================================
$('#btnActualizaEspecialidad').click(function() {
    $.ajax({
        url: 'buscaProgramados' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: 77
        },
        success: function(dato) {
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            if(data.length > 0)
            {
                $('#divInformacionProgramados').empty();
                for(var i = 0; i < data.length; i++)
                {
                    var arrayParametro = {
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        total: data[i].total
                    };
                    InformacionProgramados(arrayParametro);
                }
            }
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
});
var InformacionProgramados = function(arrayParametro) {
    var myArray = ['aqua', 'blue', 'orange'];
    var randomItem = myArray[Math.floor(Math.random()*myArray.length)];

    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var total = arrayParametro['total'];

    $('#divInformacionProgramados').append(
        '<div class="col-xs-4">' +
           '<div class="info-box" style="border: 1px solid #bbb2b2;">' + 
                '<span class="info-box-icon bg-' + randomItem + '">' + 
                    '<h1>' + total + '</h1>' + 
                '</span>' + 
                '<div class="info-box-content" style="font-size: 13px;">' + 
                    '(' + codigo + ') ' + nombre + 
                '</div>' + 
            '</div>' +
        '</div>'
    );
    i++;
}
// ================================================================================================



// ========================================= [ GUARDAR ] =========================================
$('#btnGuardarExamen').click( function(e) {
    bootbox.confirm('Desea guardar la información? ', function (confirmed) {
        if (confirmed) {
            $('#txtJsonExamenes').val('');
            
            $('#txtHiddenHora').val( $('#txtHora_Minuto_AmPM').val() );
            if($('#txtidhcl_poblacion').val() == '')
            {
                $('#divErrorPaciente').show();
                return;
            }
            
            if(datosTablaLab.length > 0)
            {
                datosTablaLab = Array.from(new Set(datosTablaLab));
                var jsonExamenes = JSON.stringify(datosTablaLab);
                $('#txtJsonExamenes').val(jsonExamenes);
            }
            else
            {
                $('#divErrorExamen').show();
                return;
            }
            
            $("#form").submit();
        }
    });
});
// ===============================================================================================



// ========================================= [ REPORTE ] =========================================
$("table").delegate("button", "click", function() {
    var id = $(this).attr('id');
    $('#vImprimeProgramacion').modal({ backdrop: 'static', keyboard: false });
    
    $('.progress').show();
    $('#secccionImprimeProgramacion').html("").append($("<iframe></iframe>", {
        src: 'programacion/imprimeProgramacion/' + id,
        css: {width: '100%', height: '100%'}
    }));
    setTimeout(function()
    {
        for(var i = 1; i <= 100; i++)
        {
            $('.progress-bar').css({width: i + '%'})
        }
        
        setTimeout(function()
        {
            $('.progress').hide();
            $('.progress-bar').css({width: '0%'});
        }, 1800);
    }, 1000);
});
// ===============================================================================================