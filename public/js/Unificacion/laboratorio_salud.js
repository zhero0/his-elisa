var idlab_grupos_global = 0;
var idlab_examenesArray = [];

$(document).ready(function() {
    
});

var lab_grupoSelecciondo = function(idlab_grupos) {
    ajaxExamen('buscaExamenesLab_perfil_examenes', idlab_grupos, '', 0);
}
var ajaxExamen = function(valorURL, idlab_grupos, valor, buscarTodo) {
    /*
        idlab_grupos_global => viene desde resultado de Laboratorio
        idlab_grupos => viene unicamente desde Salud
    */
    idlab_grupos = idlab_grupos > 0? idlab_grupos : idlab_grupos_global;
    
    var datosLab_Ima = opcionButton == 1? JSON.stringify(datosTablaLab) : JSON.stringify(datosTabla);
    var ruta = '';
    if(idlab_grupos_global > 0)
        ruta = 'buscaExamenesLab_perfil_examenes';
    else
    {
        if(valorURL == '')
            ruta = opcionButton == 1? 'buscaExamenesLab' :  'buscaExamenes';
        else
            ruta = valorURL;
    }
    
    $.ajax({
        url: ruta + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor,
            buscarTodo: buscarTodo,
            idlab_grupos: idlab_grupos,
            datosLab_Ima: datosLab_Ima,
            idlab_examenesArray: idlab_examenesArray, // este campo lo estoy usando para obtener todos los ides de examenes en "resultado"
        },
        success: function(dato) {
            $('#tbodyExamenes').empty();
            var JSONString = dato;
            var data = JSON.parse(JSONString);
            
            if(data.length > 0)
            {
                for(var i = 0; i < data.length; i++)
                {
                    var id_examenes = opcionButton == 1? data[i].idlab_examenes : data[i].idima_examenes;
                    
                    var arrayParametro = {
                        idima_examenes: id_examenes,
                        codigo: data[i].codigo,
                        nombre: data[i].nombre,
                        idEspecialidad: data[i].idEspecialidad,
                        codigoEspecialidad: data[i].codigoEspecialidad,
                        nombreEspecialidad: data[i].nombreEspecialidad,
                        idlab_grupos: idlab_grupos
                    };
                    generarListaExamenes(arrayParametro);
                }
                
                // Vuelve a seleccionar nuevamente despues de buscar los examenes
                seleccionarExamenesNuevamente();
                
                // Aqui va a entrar solo cuando se pulse click en los botones de cada grupo de laboratorio
                if(idlab_grupos > 0)
                {
                    examenesSeleccionados();
                    datosTablaLab = Array.from(new Set(datosTablaLab));
                    totalExamenSeleccionLab = datosTablaLab.length;
                    $('#spanTotalExamenesLaboratorio, #spanTotalExamenSeleccion').text(totalExamenSeleccionLab);
                }
            }
            else
                $('#tbodyExamenes').append(
                    '<tr>' + 
                        '<td>Vacío...</td>' +
                    '</tr>'
                );
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var generarListaExamenes = function(arrayParametro) {
    var idima_examenes = arrayParametro['idima_examenes'];
    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var idEspecialidad = arrayParametro['idEspecialidad'];
    var codigoEspecialidad = arrayParametro['codigoEspecialidad'];
    var nombreEspecialidad = arrayParametro['nombreEspecialidad'];
    var idlab_grupos = arrayParametro['idlab_grupos'];
    
    var checkbox = '<div class="form-check">'
                    +   '<input type="checkbox" class="form-check-input" id="checkSeleccionar' + i + '">'
                    +   ' <label class="form-check-label" for="checkSeleccionar' + i + '" style="cursor: pointer;">'
                    +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                    +   ' </label>'
                 +  '</div>';
    
    $('#tbodyExamenes').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdima_examenes' + i + '">' + idima_examenes + '</td>'
          + '<td style="display: none;" id="tdIdEspecialidad' + i + '">' + idEspecialidad + '</td>'
          + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td id="tdEspecialidad' + i + '">' + nombreEspecialidad + ' (' + codigoEspecialidad + ')' + '</td>'
          + '<td>' + checkbox + '</td>'
       +'</tr>'
    );
    
    $('#checkSeleccionar' + i).click(function() {
        examenesSeleccionados();

        if($(this).is(':checked'))
            opcionButton == 1? totalExamenSeleccionLab++ : totalExamenSeleccion++;
        else
            opcionButton == 1? totalExamenSeleccionLab-- : totalExamenSeleccion--;

        opcionButton == 1?  $('#spanTotalExamenesLaboratorio, #spanTotalExamenSeleccion').text(totalExamenSeleccionLab) : 
                            $('#spanTotalExamenesImagenologia, #spanTotalExamenSeleccion').text(totalExamenSeleccion);
        $('#txtBuscarExamen').focus();
    });
    
    if(idlab_grupos > 0) // Al pulsar click por cada botón de grupo
    {
        $('#checkSeleccionar' + i).prop('checked', true);
    }
    
    i++;
}
var seleccionarExamenesNuevamente = function() {
    var arrayDatosTabla = opcionButton == 1? datosTablaLab : datosTabla;
    
    arrayDatosTabla = Array.from(new Set(arrayDatosTabla));
    if(arrayDatosTabla.length > 0)
    {
        for(i = 0; i < arrayDatosTabla.length; i++)
        {
            $("#tablaContenedorExamenes tbody tr").each(function (index) 
            {
                var identificador = ($(this).attr('id')).split('-')[1];

                if($.isNumeric(identificador))
                {
                    if($('#tdIdima_examenes' + identificador).text() == arrayDatosTabla[i])
                    {
                        $('#checkSeleccionar' + identificador).prop('checked', true);
                        return;
                    }
                }
            });
        }
    }
}
var examenesSeleccionados = function() {
    $("#tablaContenedorExamenes tbody tr").each(function (index) 
    {
        var identificador = ($(this).attr('id')).split('-')[1];

        if($.isNumeric(identificador))
        {
            var Idima_examenes = $('#tdIdima_examenes' + identificador).text();
            if($('#checkSeleccionar' + identificador).is(':checked'))
                opcionButton == 1? datosTablaLab.push(Idima_examenes) : datosTabla.push(Idima_examenes);
            else
            {
                if(opcionButton == 1)
                    datosTablaLab = jQuery.grep(datosTablaLab, function(value) {
                      return value != Idima_examenes;
                    });
                else
                    datosTabla = jQuery.grep(datosTabla, function(value) {
                      return value != Idima_examenes;
                    });
            }
        }
    });
}

$('#btnVisualizaExamenesSeleccionados').click(function() {
    ajaxExamen('', 0, '', 0);
    $('#txtBuscarExamen').val('');
    $('#txtBuscarExamen').focus();
});
$('#btnBorrarExamenesSeleccionados').click(function() {
    datosTablaLab = [];
    $('#tbodyExamenes').html(
                        '<tr>'+
                            '<td>Vacío...</td>'+
                        '</tr>'
                      );
    $('#spanTotalExamenSeleccion').text(0);
});


$('#txtBuscarExamen').keypress(function(e) {
    if(e.which == 13) {
        $('#btnBuscarExamen').click();
    }
});
$('#btnBuscarExamen').click(function() {
    var valor = $('#txtBuscarExamen').val();
    ajaxExamen('', 0, valor, 1);
    $('#txtBuscarExamen').focus();
});