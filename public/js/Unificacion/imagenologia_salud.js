$(document).ready(function() {

    var scenario = $('#txtScenario').val();
    if(scenario == 'create' || scenario == 'edit')
    {
        // "BUSCADOR MULTIPLE" DICOM
        var inputBuscador = '';
        $("#indexSearch input").each(function(e) {
            if(inputBuscador == '')
                inputBuscador += '#' + this.id;
            else
                inputBuscador += ', #' + this.id;
        });
        $(inputBuscador).keypress(function(e) {
            if(e.which == 13) {
                Busqueda_multipleDICOM();
            }
        });
        // FIN "BUSCADOR MULTIPLE" DICOM
    }

});


var Busqueda_multipleDICOM = function() {
    var dato = {};
    $("#indexSearch :input").each(function(e) {
        var id = this.id;
        var variable = (id).split('Search_')[1];
        eval("dato." + variable + " = '" + this.value + "' ");
    });
    ajaxBusquedaDicom( JSON.stringify(dato) );
}
var ajaxBusquedaDicom = function(valor) {
    $.ajax({
        url: 'buscaDicom' + url,
        type: 'post',
        data: {
            _token: $('#token').val(),
            dato: valor
        },
        success: function(dato) {
            mostrarDatosDicom(dato, 0);
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert('error! ');
        }
    });
}
var mostrarDatosDicom = function(dato, principal) {
    $('#tbodyDicom').empty();
    var JSONString = dato;
    var datos = JSON.parse(JSONString);

    if(datos.data.length > 0)
    {
        for(var i = 0; i < datos.data.length; i++)
        {
            var arrayParametro = {
                id: datos.data[i].id,
                codigo: datos.data[i].codigo,
                nombre: datos.data[i].nombre,
                descripcion: datos.data[i].descripcion,
                tipo: datos.data[i].tipo,
                fecha: datos.data[i].fecha,
                idestudio: datos.data[i].idestudio,
                length: datos.data.length,
                principal: principal
            };
            generarListaDicom(arrayParametro);
        }
    }
    else
        $('#tbodyDicom').append(
            '<tr>' + 
                '<td>Vacío...</td>' +
            '</tr>'
        );
}
var generarListaDicom = function(arrayParametro) {
    var id = arrayParametro['id'];
    var codigo = arrayParametro['codigo'];
    var nombre = arrayParametro['nombre'];
    var descripcion = arrayParametro['descripcion'] == null? '' : arrayParametro['descripcion'];
    var tipo = arrayParametro['tipo'];
    var fecha = arrayParametro['fecha'];
    var idestudio = arrayParametro['idestudio'];
    var length = arrayParametro['length'];
    var principal = arrayParametro['principal'];
    var checked = (principal == 1 && length == 1)? 'checked' : '';
    var botonVerArchivoDicom = '';
    var botonSeleccion = '';
    
    botonSeleccion = '<div class="form-check">'
                    +   '<input type="radio" name="rdoSeleccion" class="form-check-input" id="checkSeleccionar-' + i + '" ' + checked + '>'
                    +   ' <label class="form-check-label" for="checkSeleccionar-' + i + '" style="cursor: pointer;">'
                    +       '<span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span>'
                    +   ' </label>'
                    +  '</div>';
    botonVerArchivoDicom = 
                 '<td class="col-xs-1">'
                +   '<a href="http://172.18.0.3:8080/weasis-pacs-connector/viewer?studyUID=' + idestudio + '" class="glyphicon glyphicon-download-alt" title="Descargar Archivo Dicom">'
                +   '</a>'
                + '</td>';

    $('#tbodyDicom').append(
       '<tr id="row-'+i+'">'
          + '<td style="display: none;" id="tdIdPacs' + i + '">' + id + '</td>'
          + '<td id="tdCodigo' + i + '">' + codigo + '</td>'
          + '<td id="tdNombre' + i + '">' + nombre + '</td>'
          + '<td id="tdDescripcion' + i + '">' + descripcion + '</td>'
          + '<td id="tdTipo' + i + '">' + tipo + '</td>'
          + '<td id="tdFecha' + i + '">' + fecha + '</td>'
          + '<td id="tdRdoSeleccion' + i + '">' + botonSeleccion + '</td>'
          + botonVerArchivoDicom
       +'</tr>'
    );
    $('#checkSeleccionar-' + i).click(function() {
        var id = $(this).attr('id');
        var identificador = id.split('-');
        var idpacs = $('#tdIdPacs' + identificador[1]).text();
        
        $('#txtHiddenIdpacs').val(idpacs);
    });

    i++;
}