$(document).ready(function() {    
    
    if($('#txtScenario').val() == 'index')
    {
        // "BUSCADOR MULTIPLE"
        var inputBuscador = '';
        $("#indexSearch input").each(function(e) {
            if(inputBuscador == '')
                inputBuscador += '#' + this.id;
            else
                inputBuscador += ', #' + this.id;
        });
        $(inputBuscador).keypress(function(e) {
            if(e.which == 13) {
                var ultimo_id_presionado = $(this).attr('id');
                Busqueda_index(ultimo_id_presionado);
            }
        });
        if($('#datosBuscador').val() != '') 
        {
            var buscador = JSON.parse($('#datosBuscador').val());
            $("#indexSearch :input").each(function(e) {
                var identificador = this.id;
                for(var item in buscador) {
                    if(identificador.split(item)[1] == '')
                        $('#' + identificador).val( eval("buscador." + item) );
                }
            });
            $('#selectSearch_paginacion').val( buscador.paginacion );
            $('#' + buscador.ultimo_id_presionado).select();
        }
        // FIN "BUSCADOR MULTIPLE"
    }
});


// ==================================== [ BUSCADOR MULTIPLE ] ====================================
var Busqueda_index = function(ultimo_id_presionado) {
    var dato = {};
    // eval("dato.paginacion = '" + $('#selectSearch_paginacion').val() + "' ");
    dato.paginacion = $('#selectSearch_paginacion').val();
    $("#indexSearch :input").each(function(e) {
        var id = this.id;
        var variable = (id).split('Search_')[1];
        eval("dato." + variable + " = '" + this.value + "' ");
    });
    dato.ultimo_id_presionado = ultimo_id_presionado;
    $('#datosBuscador').val( JSON.stringify(dato) );
    $('#formBuscador').submit();
}
// ===============================================================================================
