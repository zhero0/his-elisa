@extends('adminlte::layouts.app')
@section('contentheader_title', 'VER PROVEEDOR')
@section('htmlheader_title', 'VER PROVEEDOR')

@section('main-content')
<div class="row">
  <div class="col-xs-12 col-sm-10 col-md-9 col-lg-9">
    <div class="panel panel-primary">
      <div class="panel-body">
        {!! Form::model($model, ['route' => ['datosgenericos.update', $model->id], 'method' => 'PUT', 'id' => 'form']) !!}
          @include('datosgenericos.form')
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop