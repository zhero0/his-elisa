<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    @if(count($errors) >0 )
      @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
      @endforeach
    @endif
    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        {!! Form::label('nombres', 'Nombre Proveedor:', ['class' => 'label-control']) !!}
        {!! Form::text('nombres', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        {!! Form::label('nit', 'NIT:', ['class' => 'label-control']) !!}
        {!! Form::text('nit', null, ['class' => 'form-control', 'placeholder' => 'Escriba...']) !!}
      </div>
    </div> 

    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        {!! Form::label('direccion', 'Dirección:') !!}
        {!! Form::text('direccion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        {!! Form::label('telefono', 'Telefono:') !!}
        {!! Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => 'Escriba...']) !!}
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-9 col-lg-9">
        {!! Form::label('descripcion', 'Descripción:') !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...']) !!}
      </div>
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif
