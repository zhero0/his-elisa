@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR UNIDAD')
@section('htmlheader_title', 'EDITAR UNIDAD')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['unidad.update', $model->id], 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!} 
					@include('unidad.form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop