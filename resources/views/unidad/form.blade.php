<div class="form-group">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    @if(count($errors) >0 )
      @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
      @endforeach
    @endif 
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-md-9 col-lg-10">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'maxlength' => '30']) !!}
      </div>  
      
      <div class="col-xs-12 col-sm-10 col-md-9 col-lg-10">
        {!! Form::label('simbolo', 'Simbolo:') !!}
        {!! Form::text('simbolo', null, ['class' => 'form-control', 'placeholder' => 'Kg', 'style' => 'text-transform: uppercase;', 'maxlength' => '5']) !!} 
      </div>  
    </div>
</div>

<a href="/unidad" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif
