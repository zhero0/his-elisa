<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">   
    <input type="hidden" id="txtJsonCuadernos" name="txtJsonCuadernos">         
    <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">
    <input type="hidden" id="idFic_sala" name="idFic_sala" value="{{ $model->idFic_sala }}">
    <input type="hidden" id="idFic_ventanilla" name="idFic_ventanilla" value="{{ $model->idFic_ventanilla }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="row">
      <ul class="timeline">
         

        <!-- timeline DATOS -->
        <li>
          <i class="fa fa-file-text-o bg-yellow"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <h3 class="box-title">DATOS</h3>
                    </div>
                  </div>
                </div>
                <div class="box-body">

                  <div class="row"> 

                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <label>Sala</label>
                      <div class="input-group">
                        <input type="text" id="txtSala" name="txtSala" value="{{$model->sala}}" class="form-control" disabled="">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModal_Sala" title="Click para Buscar">
                            <span class="fa fa-search" aria-hidden="true"></span>.
                          </button>
                        </span>
                      </div>
                    </div>
                    
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                      <label>Ventanilla</label>
                      <div class="input-group">
                        <input type="text" id="txtVentanilla" name="txtVentanilla" value="{{$model->Ventanilla}}" class="form-control" disabled="">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModal_Ventanilla" title="Click para Buscar">
                            <span class="fa fa-search" aria-hidden="true"></span>.
                          </button>
                        </span>
                      </div>
                    </div> 

                  </div>

                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                      <label>Cuaderno</label>
                      <div class="input-group">
                        <input type="text" id="txtEspecialidad" name="txtEspecialidad" class="form-control" value="{{$model->especialidad}}" disabled="">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModal_Especialidad" title="Click para Buscar">
                            <span class="fa fa-search" aria-hidden="true"></span>.
                          </button>
                        </span>
                      </div>
                    </div>  
                  </div>

                </div>
              </div>
              
            </div>
            <div class="timeline-footer">
            </div>
          </div>
        </li>
        <!-- END timeline DATOS --> 
        
        <li>
          <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
        </li>
      </ul>  
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarCuaderno" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif