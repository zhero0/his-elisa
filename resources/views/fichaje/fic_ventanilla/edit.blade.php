@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR VENTANILLA')
@section('htmlheader_title', 'EDITAR VENTANILLA')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['ventanilla_fic.update', $model->id], 'method' => 'PUT', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop