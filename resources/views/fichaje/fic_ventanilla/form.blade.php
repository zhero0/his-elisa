<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    @if(count($errors) >0 )
      @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
      @endforeach
    @endif 

    <div class="row">      
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('descripcion', 'Descripcion:') !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control',  'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('tipo', 'Tipo:', ['class' => 'label-control']) !!}
        {!! Form::number('tipo', null, ['class' => 'form-control', 'placeholder' => 'Escriba...']) !!}
      </div>
    </div> 
    
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif
