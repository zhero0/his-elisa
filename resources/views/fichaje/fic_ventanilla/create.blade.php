@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVA VENTANILLA')
@section('htmlheader_title', 'NUEVA VENTANILLA')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
			<div class="panel-body">
				{!! Form::Open(['route' => 'ventanilla_fic.store', 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop