@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE SALAS')
@section('htmlheader_title', 'LISTA DE SALAS')

@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('ventanilla_fic.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr> 
	 					<th>ID</th>
	 					<th>Descripción</th>
	 					<th>Tipo</th>
	 					<th>Usuario</th>
	 					<th></th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-2">{{ $dato->id }}</td>
						<td class="col-xs-8">{{ $dato->descripcion }}</td> 
						<td class="col-xs-2">{{ $dato->tipo }}</td>						
						<td>{{ $dato->usuario }}</td> 
						<td>
							<button class="btn btn-default btn-xs">
								<a href="{{ route('ventanilla_fic.show', $dato->id) }}"> 
									<i class="glyphicon glyphicon-hand-left" title="VER"></i>
								</a>
							</button>
							<button class="btn btn-default btn-xs">
								<a href="{{ route('ventanilla_fic.edit', $dato->id) }}"> 
									<i class="glyphicon glyphicon-pencil" title="Editar"></i>
								</a>
							</button>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>
		
		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->render() !!}
	    </div>
 	</div>
</div>
@stop
 