@extends('layouts.admin')
@section('contentheader_title', ' ')
@section('htmlheader_title', ' ')
 
 <?php $message=Session::has('message')?>
@section('main-content')
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    @if($message == 'store' || $message == 'update')
      <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        {{Session::get('message')}}
        Se ha guardado correctamente!
      </div>
    @endif
</div>
<form enctype="multipart/form-data" action="/perfil" method="POST">
  <div class="box box-primary   box-gris" style="margin-bottom: 200px;">
    <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Acceso al sistema</strong></h3>
    </div><!-- /.box-header -->
    <div id="notificacion_E3" ></div>
    <div class="box-body">

      <div class="row">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input type="hidden" name="id_usuario" value=" ">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <label>Nombre</label>
              <input type="text" class="form-control" name="nombre" id="nombre" value="{{$user->name}}" disabled="">
            </div>

            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <label class="label-control">Contraseña Actual*</label>
                  <input type="password" class="form-control" id="mypassword" name="mypassword"  required >
                  <div class="text-danger">{{$errors->first('mypassword')}}</div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <label class="label-control">Nueva Contraseña*</label>
                <input type="password" class="form-control" id="password" name="password"  required >
                <div class="text-danger">{{$errors->first('password')}}</div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                <label class="label-control">Confirmar Contraseña*</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  required > 
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-5">
          <img src="/uploads/avatars/{{$user->avatar}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right: 25px;">      
          <div class="row"> 
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                <label> Subir Imagen de Perfil </label>
                <input type="file" name="avatar">  
                <!--<input type="hidden" name="_token" value="{{ csrf_token() }}">-->
              </div>
          </div>
         {{csrf_field()}}
        </div>
      </div>
      <br>

      <a href="/home" class="btn btn-primary" data-dismiss="modal">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Principal
      </a> 
      <button type="submit" class="btn btn-primary">Guardar</button>
                
    </div>
  </div>
</form>  
@stop
