<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">

    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>
    
    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Nombres:</label> <br>  
        <input type="text" class="form-control" id="nombres" name="nombres" style="text-transform:uppercase;" value="{{$model->nombres}}"> 
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Apellidos:</label> <br>  
        <input type="text" class="form-control" id="apellidos" name="apellidos" style="text-transform:uppercase;" value="{{$model->apellidos}}">  
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Dirección:</label> <br>  
        <input type="text" class="form-control" id="direccion" name="direccion" style="text-transform:uppercase;" value="{{$model->direccion}}"> 
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Carnet de Identidad:</label> <br>  
        <input type="text" class="form-control" id="ci" name="ci" style="text-transform:uppercase;" value="{{$model->ci}}">  
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Matricula:</label> <br>  
        <input type="text" class="form-control" id="matricula" name="matricula" style="text-transform:uppercase;" value="{{$model->matricula}}"> 
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Título:</label> <br>  
        <input type="text" class="form-control" id="titulo" name="titulo" style="text-transform:uppercase;" value="{{$model->titulo}}">  
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Telefono:</label> <br>  
        <input type="text" class="form-control" id="telefono" name="telefono" style="text-transform:uppercase;" value="{{$model->telefono}}">   
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label class="bmd-label">Descripción:</label> <br>  
        <input type="text" class="form-control" id="descripcion" name="descripcion" style="text-transform:uppercase;" value="{{$model->descripcion}}"> 
      </div>
    </div>    
    <div class="row">
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6"> 
        <label>TIPO DE PERSONAL</label>
        <br>
        <select name="id_generico" id="id_generico" class="form-control" onchange="seleccionarClasificacion()"> 
            <option disabled selected>Seleccionar</option> 
            @foreach($modelGenerico as $dato) 
              @if($model != null)
                <option value="{{$dato->id}}" {{ $model->id_generico == $dato->id ? 'selected="selected"' : ''  }} >{{$dato->nombre }}</option> 
              @else
                <option value="{{$dato->id}}">{{$dato->nombre}}</option> 
              @endif
            @endforeach
        </select>   
      </div>
      <div class="col-xs-12 col-sm-11 col-md-6 col-lg-6">
        <label>UNIDAD MEDICA</label>
        <br>
        <select name="idalmacen" id="idalmacen" class="form-control" onchange="seleccionarClasificacion()"> 
            <option disabled selected>Seleccionar</option> 
            @foreach($modelAlmacen as $dato) 
              @if($model != null)
                <option value="{{$dato->id}}" {{ $model->idalmacen == $dato->id ? 'selected="selected"' : ''  }} >{{$dato->nombre }}</option> 
              @else
                <option value="{{$dato->id}}">{{$dato->nombre}}</option> 
              @endif
            @endforeach
        </select>  
      </div>
    </div>    
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif