@foreach($model as $dato)
    <tr role="row" style="{{ config('app.styleTableRow') }}">
		<td class="col-xs-4">{{ $dato->nombres.' '.$dato->apellidos }}</td> 
		<td class="col-xs-1">{{ $dato->ci }}</td>
		<td class="col-xs-1">{{ $dato->titulo }}</td>
		<td class="col-xs-1">{{ $dato->matricula }}</td>
		<td class="col-xs-2">{{ $dato->direccion }}</td>
		<td class="col-xs-1">{{ $dato->telefono }}</td>  
		<td class="col-xs-1">
			<!-- <a href="{{ route('personal.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a> -->
			<a href="{{ route('personal.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
				<i class="fa fa-edit"></i>
			</a>
		</td>
	</tr>
@endforeach