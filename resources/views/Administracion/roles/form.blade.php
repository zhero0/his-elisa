<div class="form-group">
	<div class="row">
		<div class="col-xs-12 col-sm-10 col-md-5 col-lg-5">
	      	<div class="card">
		        <div class="card-body"> 
		          	<div class="user-block">
		            <img class="img-circle img-bordered-sm" src="/imagenes/farmacia_icono.jpg" alt="user image">
		            <span class="username">
		              <a href="#">Plataforma de atención a realizar</a>
		            </span>
		            <span class="description">Seleccione el tipo de atencion a realizarse.</span>
		          	</div> 
		          	<br>  
		          	<br>  
		          	<!-- timeline "TIPOS ATENCION" --> 
		            	 	      
		          	<!-- END "TIPOS ATENCION" -->
		        </div>
	     	</div>
	      	<div class="row" id="divContenedorDatos_paciente">
	         	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		          	<div class="card">  
		              	@if($model->scenario != 'view')
		                	<button type="button" id="btnGuardar" class="btn btn-success" style="display: none;">
		                  	<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
		                	</button>
		              	@endif 
		          	</div>
	       		</div> 
		        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		          	<div class="card"> 
		              	<a href="{{ URL::previous() }}" class="btn btn-primary">
		                	<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
		              	</a> 
		          	</div>
		        </div>
	      	</div>
	    </div> 
	    <div class="col-xs-12 col-sm-10 col-md-7 col-lg-7">
	      	<div class="card">
		        <div class="card-body"> 
		          	<div class="user-block">
		            <img class="img-circle img-bordered-sm" src="/imagenes/farmacia_icono.jpg" alt="user image">
		            <span class="username">
		              <a href="#">Plataforma de atención a realizar</a>
		            </span>
		            <span class="description">Seleccione el tipo de atencion a realizarse.</span>
		          	</div> 
		          	<br>  
		          	<br>  
		          	<!-- timeline "TIPOS ATENCION" --> 
		            @include($model->rutaview.'_dataContent') 	 	      	
		          	<!-- END "TIPOS ATENCION" -->
		        </div>
	     	</div> 
	    </div> 
	</div>
</div>