@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>REGISTRAR - ROL</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('roles.store') }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	@include($model->rutaview.'form')  	
</form>
@stop