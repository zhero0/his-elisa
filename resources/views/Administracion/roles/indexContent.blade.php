@foreach($model as $dato)
    <tr role="row" style="{{ config('app.styleTableRow') }}"> 
        <td class="text-left">{{ $dato->name }}</td>                      
        <td class="text-left">{{ $dato->description }}</td>          
        <td class="text-left">{{ $dato->created_at }}</td> 
        <td class="td-actions text-right"> 
            <div class="btn-group">                   
            </div>                                  
        </td> 
    </tr>
@endforeach
 