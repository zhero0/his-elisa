<table id="tableLista" class="table table-striped">
	<thead>
		<tr> 
			<th>Registros</th> 
			<th></th>
		</tr>
	</thead>
	<tbody>
		@foreach($modelMenu as $dato)	
			<tr data-widget="expandable-table" aria-expanded="true">
				<td>
					<i class="expandable-table-caret fas fa-caret-right fa-fw"></i>
					{{$dato->nombre.' || Estado' }}  
				</td>   
			</tr> 						
			<tr class="expandable-body">
				<td>
					<div class="p-0" style="">
						<table class="table table-hover">
							<tbody>
								@foreach($modelOpcion as $subDato)
									@if($subDato->idmenu == $dato->id)
										<tr> 
											<td>{{ $subDato->nombre }}</td>
											<td  class="col-xs-1">
												<div class="btn-group">
									                <div class="form-check">
									                    <input type="checkbox" class="form-check-input" id="checkSeleccionar{{$subDato->id}}">
									                    
									                </div>  
									            </div> 
											</td>
										</tr>
									@endif
								@endforeach 
							</tbody>
						</table>
					</div>
				</td>
			</tr>							  	 					
		@endforeach
	</tbody>
</table>