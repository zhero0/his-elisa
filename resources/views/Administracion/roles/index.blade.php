@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTADO - ROLES</h1>
@stop   

@section('content')
<div class="card"> 
	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
	<input type="hidden" id="txtAccion" value="{{ $model->accion }}"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('roles.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div>
		<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> -->
	                 
        <!-- </div> --> 
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Nombre</th> 								
					<th>Descripción</th> 	
					<th>Fecha</th> 				
					<th></th>
				</tr> 
			</thead>
			<tbody id="indexContent">
				@include($model->rutaview.'indexContent')
			</tbody>
		</table>
	</div>  
</div> 
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 