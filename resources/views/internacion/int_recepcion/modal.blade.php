<!-- VENTANA MODAL PARA BUSCAR PACIENTE -->
<div class="modal fade" id="vModalBuscadorInternado">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="favoritesModalLabel">DATOS PACIENTE</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscadorInternado" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscarPacienteInternado">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>

		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedorDatosPaciente" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
                        		<th class="col-xs-2">Nro Registro</th>
                        		<th class="col-xs-2">Nro Historia</th>
		                        <th class="col-xs-4">Nombre Completo</th> 
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
	                      <tr id="idListaVaciaPaciente">
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>

<!-- <div class="modal" id="vImprimeRecepcion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> RECEPCION DE INTERNACION </h4>
        </div>
        <div class="modal-body">
         	<div id="secccionImprimeRecepcion" style="height: 410px;"></div>

	 		<div class="progress">
			  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			  </div>
			</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div> -->