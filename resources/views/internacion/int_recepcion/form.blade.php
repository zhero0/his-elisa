<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}"> 
    <input type="hidden" id="txtHora_Programado" value="{{ $model->hora_programacion }}"> 
    <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-12">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>
  
  <div class="row">
      <ul class="timeline">
        <!-- timeline PACIENTE -->
        <li>
          <i class="fa fa-user bg-blue"></i>
          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                      <!-- <h3 class="box-title">PACIENTE</h3> -->
                      <h4 class="list-group-item-heading text-primary">
                        <i class="fa fa-user"></i>
                        PACIENTE
                      </h4>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                      <div class="input-group input-group-sm">
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          <label>Matricula</label>
                          <input type="text" id="txtMatricula" class="form-control" autofocus="autofocus" style="text-transform: uppercase;">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                          <label>Nombre Completo</label>
                          <div class="input-group">
                            <input type="text" id="txtBuscarInternado" class="form-control" style="text-transform: uppercase;">
                            <span class="input-group-btn">
                              <button class="btn btn-primary" type="button" id="btnModalPaciente" title="Click para Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>.
                              </button>
                            </span>
                          </div> 
                        </div>     
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                      <input type="hidden" id="txtidcabecera" name="txtidcabecera">
                      <dl class="dl-horizontal">
                        <dt>Nº MATRICULA:</dt>
                        <dd id="datoNumeroPaciente">{{ $model->matricula != null? $model->matricula:'' }}</dd>

                        <dt>PACIENTE:</dt>
                        <dd id="datoNombrePaciente">{{ $model->nombre_completo != null? $model->nombre_completo:'' }}</dd>

                        <dt>DIAGNOSTICO:</dt>
                        <dd id="datoDiagnosticoPaciente">{{ $model->diagnostico != null? $model->diagnostico:'' }}</dd>

                        <dt>SERV. SOLICITANTE:</dt>
                        <dd id="datoServicio_solicitante">{{ $model->especialidad != null? $model->especialidad:'' }}</dd>

                        <dt>MEDICO SOLICITANTE:</dt>
                        <dd id="datoMedico_solicitante">{{ $model->medico != null? $model->medico:'' }}</dd>
                        <dt>N° REGISTRO:</dt>
                        <dd id="datoRegistro">{{ $model->idhcl_cabecera_registro != null? $model->idhcl_cabecera_registro:'' }}</dd>
                      </dl>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="timeline-footer">
              <div class="alert alert-danger" id="divErrorPaciente" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Seleccione algún PACIENTE
              </div>
            </div>
          </div>
        </li>
        <!-- END timeline PACIENTE -->

        <!-- timeline DATOS -->
        <li>
          <i class="fa fa-file-text-o bg-yellow"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <h4 class="list-group-item-heading text-primary">
                        <i class="fa fa-file-o"></i>
                        DATOS
                      </h4> 
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="row">

                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <label for="estado">Estado de Internación</label>
                        <select id="estado" name="estado" class="form-control">
                          <option value="0"> :: Seleccionar</option>
                          @foreach($modelEstado as $estado) 
                            @if($model != null)
                              <option value="{{$estado->id}}" {{ $model->idint_estado_recepcion_ingreso == $estado->id ? 'selected="selected"' : ''  }} > {{$estado->nombre }}
                                  </option> 
                            @else
                              <option value="{{$estado->id}}">{{$estado->nombre}}</option> 
                            @endif
                          @endforeach 
                        </select> 
                      </div>
                      <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                        <label>Fecha</label>
                        <div class='input-group date' id='datepickerFecha'>
                          <input type='text' class="form-control" id="fecha" name="fechaprogramacion" value="{{  $model->fecha_ingreso }}">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <input type="hidden"  id="txtHiddenHora" name="txtHiddenHora">
                        <label>Hora</label>
                        @include('layouts.hora')
                      </div>  
                    </div>
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label for="servicio">SERVICIOS</label>
                        <select id="idServicio" name="idServicio" class="form-control">
                          <option value="0"> :: Seleccionar</option>
                          @foreach($modelServicio as $servicio) 
                            @if($model != null)
                              <option value="{{$servicio->id}}" {{ $model->idint_servicio == $servicio->id ? 'selected="selected"' : ''  }} > {{$servicio->nombre }}
                                  </option> 
                            @else
                              <option value="{{$servicio->id}}">{{$servicio->nombre}}</option> 
                            @endif
                          @endforeach 
                        </select> 
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label for="estado">SALAS</label>
                        <select id="idSala" name="idSala" class="form-control">
                          <option value="0"> :: Seleccionar</option>
                          @foreach($modelSala as $sala) 
                            @if($model != null)
                              <option value="{{$sala->id}}" disabled="true" {{ $model->idint_sala == $sala->id ? 'selected="selected"' : ''  }}> {{$sala->nombre }}
                                  </option> 
                            @endif 
                          @endforeach 
                        </select> 
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <label for="estado">CAMAS</label>
                        <select id="idCamas" name="idCamas" class="form-control">
                          <option value="0"> :: Seleccionar</option>
                          @foreach($modelCamas as $camas) 
                            @if($model != null)
                              <option value="{{$camas->id}}" disabled {{ $model->idint_camas == $camas->id ? 'selected="selected"' : ''  }} > {{$camas->numero }}
                                  </option>   
                            @endif
                          @endforeach 
                        </select> 
                      </div>
                    </div>
                    <div class="row">
                      <br>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {!! Form::label('descripcion_ingreso', 'Observaciones:', ['class' => 'label-control']) !!}
                        {!! Form::textarea('descripcion_ingreso', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'rows' => 2]) !!}
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
              
            </div>
            <div class="timeline-footer">
            </div>
          </div>
        </li>
        <!-- END timeline DATOS --> 

        <li>
          <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
        </li>
      </ul>  
    </div> 
</div>

<a href="/int_recepcion" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarExamen" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif