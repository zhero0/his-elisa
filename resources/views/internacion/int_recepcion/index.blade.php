@extends('layouts.admin')
@section('contentheader_title', 'REGISTROS DE INTERNACION')
@section('htmlheader_title', 'REGISTROS DE INTERNACION')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
 		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
 		<!-- "BUSCADOR MULTIPLE" -->
 		<div class="row">
	 		<form id="formBuscador" class="navbar-form navbar-right" action="/int_recepcion" autocomplete="off">
	 			<textarea class="form-control" id="datosBuscador" name="datosBuscador" style="display: none;"> {{ $model->datosBuscador }} </textarea>
	 		</form>
 		</div>
 		<!-- FIN "BUSCADOR MULTIPLE" --> 

		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('int_recepcion.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr> 
	 					<th>MATRICULA</th>
	 					<th>DOCUMENTO</th>
	 					<th>PACIENTE</th> 
	 					<th>F. NAC.</th>
	 					<th>SERVICIO</th>	 					
	 					<th>SALA</th>
	 					<th>CAMA</th>
	 					<th>FECHA INGRESO</th>
	 					<th>ESTADO INGRESO</th> 
	 					<th></th>
					</tr>
					<tr id="indexSearch" style="background: #ffffff;">
	 					<th>
	 						<input type="text" id="txtSearch_matricula" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<input type="text" id="txtSearch_documento" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th> 
	 					<th>
	 						<input type="text" id="txtSearch_nombres" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th></th>
	 					<th>
	 						<input type="text" id="txtSearch_servicio" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th></th>
	 					<th></th>
	 					<th></th>
	 					<th>
	 						<select id="selectSearch_estado" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
				            	<option value="0"> </option>
				                @foreach($modelEstado as $dato)
				                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
				                @endforeach
				            </select>
	 					</th> 
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}"> 
						<td>{{ $dato->matricula }}</td> 
						<td>{{ $dato->documento }}</td> 
						<td class="col-xs-3">{{ $dato->nombre_completo }}</td> 
						<td class="col-xs-1">{{ $dato->fecha_nacimiento }}</td> 
						@foreach($modelServicio as $servicio)
							@if($dato->idint_servicio == $servicio->id)
								<td class="col-xs-1">{{ $servicio->nombre }}</td> 
							@endif
		                @endforeach
		                @foreach($modelSala as $sala)
							@if($dato->idint_sala == $sala->id)
								<td>{{ $sala->nombre }}</td> 
							@endif
		                @endforeach 
						
						@foreach($modelCamas as $cama)
							@if($dato->idint_camas == $cama->id)
								<td>{{ $cama->numero }}</td> 
							@endif
		                @endforeach

						<td class="col-xs-2">{{ $dato->fecha_ingreso.' '.$dato->hora_ingreso }}</td> 
						@foreach($modelEstado as $ingreso)
							@if($dato->idint_estado_recepcion_ingreso == $ingreso->id)
								<td class="col-xs-2">{{ $ingreso->nombre }}</td> 
							@endif
		                @endforeach 
						<td class="col-xs-1"> 
							<a href="{{ route('int_recepcion.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
							<button type="button" id="{{$dato->id}}" class="btn btn-default btn-sm" title="Imprimir internación">
								<i class="glyphicon glyphicon-print"></i>
							</button>
						</td>

					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@include($model->rutaview.'modalReporte')
@stop 