@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVO REGISTRO BENEFICIARIO')
@section('htmlheader_title', 'NUEVO REGISTRO BENEFICIARIO')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
    	<div class="card">
			<div class="card-body"> 
				<form method="POST" action="{{ route('storeBeneficiario', $model->idpoblacion) }}" autocomplete="off" id="formBeneficiario"> 		
					@include($model->rutaview.'formBeneficiario')
				</form>
			</div>
		</div>
	</div>
</div>
@stop
