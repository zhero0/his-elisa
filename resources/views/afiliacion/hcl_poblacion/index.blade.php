@extends('adminlte::page')
@section('title', 'POBLACION ASEGURADA')
@section('content_header')
    <h1>LISTA DE POBLACION ASEGURADA Y BENEFICIARIA</h1>
@stop   

<?php
use Illuminate\Support\Facades\Input;
?>

@section('content')
<div class="card"> 
	<div class="card-header"> 
		<div class="row">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('hcl_poblacion.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left;"> 
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		        <form class="navbar-form navbar-right" roles="searchNuevo" action="/hcl_poblacion" autocomplete="off">
		        	<div class="row text-right">
		        		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		        			<input type="text" id="matricula" name="matricula" class="form-control" placeholder="Buscar por matricula" style="text-transform: uppercase;" autofocus="">    
			        	</div>
			        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			        		<input type="text" id="primer_apellido" name="primer_apellido" class="form-control" placeholder="AP. PATERNO" style="text-transform: uppercase;"> 
			        	</div>
			        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			        		<input type="text" id="segundo_apellido" name="segundo_apellido" class="form-control" placeholder="AP. MATERNO" style="text-transform: uppercase;">  
			        	</div>
			        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			        		<input type="text" id="nombres" name="nombres" class="form-control" placeholder="NOMBRES" style="text-transform: uppercase;"> 
			        	</div>
			        	<button type="submit" class="btn btn-primary"> Buscar </button>
		        	</div>  
		        </form>         
	        </div> 
		</div> 
	</div>  
		<div class="card-body"> 
			<table id="asegurados" class="table table-striped">
				<thead>
					<tr> 
	 					<th>MATRICULA</th>
	 					<th>GRUPO FAMILIAR</th> 
	 					<th>NOMBRE COMPLETO</th>  						 					
	 					<th>EMPRESA</th>	 					
	 					<th>FECHA NAC.</th>	 					
	 					<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($model as $dato)				
						<tr role="row">
							<td class="col-xs-1">{{ $dato->numero_historia }}</td>
							<td class="col-xs-2">{{ $dato->descripcion }}</td>
							<td class="col-xs-3">{{ $dato->primer_apellido }} {{ $dato->segundo_apellido }} {{ $dato->nombre }}</td> 			
							<td class="col-xs-3">{{ $dato->nombre_empresa }}</td>			
							<td class="col-xs-1">{{ $dato->fecha_nacimiento }}</td>			
							<td >
								<!-- <a href="{{ route('hcl_poblacion.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
									<span class="glyphicon glyphicon-eye-open"></span>
								</a> -->
								<a href="{{ route('hcl_poblacion.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
									<i class="fa fa-edit"></i>
								</a> 					
							</td>
						</tr>				
					@endforeach
				</tbody>
			</table>
		</div>  
</div> 
@stop 

@section('js')
	<script>
		$(document).ready( function () {
		    $('#asegurados').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection