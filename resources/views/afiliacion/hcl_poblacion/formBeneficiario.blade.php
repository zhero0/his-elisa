<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" id="txtListaPoblacion" name="txtListaPoblacion" value="{{$model->txtListaPoblacion}}">
  <input type="hidden" id="txtGenero" value="{{ $model->sexo }}">
  <input type="hidden" id="txtJsonHcl_afiliacion_parentesco" value="{{ $model->Hcl_afiliacion_parentesco }}">

  @if($model->scenario != 'createBeneficiario')
    <input type="hidden" id="txtIdparentesco" value="{{ $modelAfiliacion->idparentesco }}">
  @endif
  
  @if(count($errors) >0 )
    @foreach($errors->all() as $error)
      <div class="alert alert-danger">{{$error}}</div>
    @endforeach
  @endif 
    
     <div class="box box-primary   box-gris" style="margin-bottom: 5px;">
      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Datos de persona</strong></h3>
      </div><!-- /.box-header -->
      <div id="notificacion_E3" ></div>
      <div class="box-body"> 
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            {!! Form::label('primer_apellido', 'Apellido Paterno', ['class' => 'label-control']) !!}
            {!! Form::text('primer_apellido', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            {!! Form::label('segundo_apellido', 'Apellido Materno', ['class' => 'label-control']) !!}
            {!! Form::text('segundo_apellido', null, ['class' => 'form-control',  'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
          </div>
          <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
            {!! Form::label('nombre', 'Nombre') !!}
            {!! Form::text('nombre', null, ['class' => 'form-control',  'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
          </div>
          </div>
          <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            {!! Form::label('documento', 'Documento CI', ['class' => 'label-control']) !!}
            {!! Form::text('documento', null, ['class' => 'form-control', 'placeholder' => 'Escriba...']) !!}
          </div> 
          <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            {!! Form::label('genero', 'Género', ['class' => 'label-control']) !!}
            <br>
            <div class="radio" id="formulario">
              <label><input type="radio" id="masc" value="1" name="optradio">Masculino</label>
            </div> 
          </div>
          <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            <br>
            <div class="radio">
              <label><input type="radio" id="fem" value="2" name="optradio">Femenino</label>
            </div> 
          </div>  
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento') !!}
              {!! Form::date('fecha_nacimiento', null, ['class' => 'form-control',  'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
          </div>
          </div>
       

        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="parentesco">Parentesco</label>
            <select class="form-control" id="parentesco" name="parentesco"> 
            </select>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="departamento">Departamento</label>
            <select id="departamento" name="departamento" class="form-control" >
              <option value="0"> :: Seleccionar</option>
                @foreach($departamentos as $departamento)            
                  @if($model != null)
                    <option value="{{$departamento->id_municipio}}"
                    {{ $model->id_localidad == $departamento->id_municipio ? 'selected="selected"' : ''  }} > {{$departamento->dep_descripcion }} {{$departamento->prov_descripcion }} {{$departamento->mun_descripcion }}
                      </option>  
                   @else
                    <option value="{{$departamento->id_municipio}}">{{$departamento->dep_descripcion}}</option> 
                  @endif                        
                @endforeach
            </select> 
          </div>  
        </div>  
         <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            {!! Form::label('telefono', 'Telf. o Movil', ['class' => 'label-control']) !!}
            {!! Form::text('telefono', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!} 
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            {!! Form::label('domicilio', 'Domicilio', ['class' => 'label-control']) !!}
            {!! Form::text('domicilio', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!} 
          </div>
        </div>    
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! Form::label('comentarios', 'Comentarios', ['class' => 'label-control']) !!}
            {!! Form::text('comentarios', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
          </div>    
        </div> 
      </div>
    </div>
  </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view') 
  <button type="button" id="btnGuardarPoblacionBeneficiario" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif
