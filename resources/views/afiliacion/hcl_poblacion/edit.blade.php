@extends('adminlte::page') 
@section('title', 'EDITAR REGISTRO')  
@section('content_header')
    <h1>EDITAR REGISTRO DE ASEGURADO</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
    	<div class="panel panel-primary">
      		<div class="panel-body">  
				<form method="POST" action="{{ route('hcl_poblacion.update', $model->id) }}" autocomplete="off" id="form"> 		
					{{ csrf_field() }}
      				<input name="_method" type="hidden" value="PATCH"> 
					@include($model->rutaview.'form')
				</form>
			</div>
			</div>
		</div>
	</div>
</div>

@stop
