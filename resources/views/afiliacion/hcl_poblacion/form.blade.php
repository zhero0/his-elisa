<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" id="txtListaPoblacion" name="txtListaPoblacion" value="{{$model->txtListaPoblacion}}">
  <input type="hidden" id="txtGenero" value="{{ $model->sexo }}">
  <input type="hidden" id="txtJsonHcl_afiliacion_parentesco" value="{{ $model->Hcl_afiliacion_parentesco }}">
  <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">
  <input type="hidden" id="id_municipio" name="id_municipio" value="{{ $model->id_municipio }}">
  <input type="hidden" id="idempleador" name="idempleador" value="{{ $model->idempleador }}">
  <input type="hidden" id="idhcl_poblacion" name="idhcl_poblacion" value="{{ $model->idhcl_poblacion }}">
  <input type="hidden" name="json_poblacion_erp">


  @if($model->scenario != 'create')
    <input type="hidden" id="txtIdparentesco" value="{{ $modelAfiliacion->idparentesco }}">
  @endif
  
  @if(count($errors) >0 )
    @foreach($errors->all() as $error)
      <div class="alert alert-danger">{{$error}}</div>
    @endforeach
  @endif 
   
  <div id="divContenedorEmpresa">
     
    <div class="input-group">
      <input type="text" id="txtBuscadorEmpleador" class="form-control" placeholder="Buscar por numero o nombre de empleador" style="text-transform: uppercase;" autofocus="">
      <span class="input-group-btn">

        <button class="btn btn-warning" type="button" id="btnBuscadorEmpleador">
          <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
          Buscar
        </button>
      </span>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatos" class="table table-bordered table-hover table-condensed">
              <thead style="background: #374850; color: #f3eaf0;">
                  <tr>
                     <th class="col-xs-2">N° EMPLEADOR</th>
                     <th class="col-xs-8">NOMBRE EMPLEADOR</th>
                     <th></th>
                 </tr>
              </thead>
              <tbody class="table-success" id="tablaEmpleador">
                <tr id="idListaVacia">
                  <td>Lista Vacia...</td>
                </tr>
              </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div id="divContenedorDatosPersona">
    <div class="box box-primary   box-gris" style="margin-bottom: 10px;">
      <div class="box-header with-border my-box-header">
        <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
          <h3 class="box-title"><strong>Datos de empleador</strong></h3>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarEmp">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>          
        </div>
      </div><!-- /.box-header -->
      <div id="notificacion_E3" ></div>
      <div class="box-body">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label for="idempleador" data-required="*">EMPLEADOR</label>
            <input type="hidden" id="txtId_empleador" name="txtId_empleador">
            <h4 id="nro_emp"></h4>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h4 id="nomb_emp"></h4>
          </div>
        </div>
      </div> 
    </div> 
    <div class="box box-primary   box-gris" style="margin-bottom: 5px;">
      <div class="box-header with-border my-box-header">
        <h3 class="box-title"><strong>Datos de persona</strong></h3>
      </div><!-- /.box-header -->
      <div id="notificacion_E3" ></div>
      <div class="box-body">
         <div class="row">
    <div class="col-12 col-sm-6 col-md-6 col-lg-6 tarjeta">
      <!-- <label style="color: blue;">Con los siguientes parámetros podrá importar los datos del Paciente! </label> -->
      <div class="row">
        <div class="col-12 col-sm-4 col-md-4 col-lg-4">
          <div class="form-group">
            <label for="documento" data-required="*">Documento CI</label>
            <input type="text" class="form-control input-sm" name="documento" value="{{ $model->documento }}" autofocus>
          </div>
        </div>
        <div class="col-12 col-sm-5 col-md-5 col-lg-5">
          <div class="form-group">                  
            <label for="fecha_nacimiento" data-required="*">Fecha de Nacimiento</label> 
            <input type="date" class="form-control" name="fecha_nacimiento" value="{{ $model->fecha_nacimiento }}">
          </div>
        </div>
        <!-- <div class="col-6 col-sm-2 col-md-2 col-lg-2" rel="tooltip" title="Buscar Paciente">
          <button class="btn btn-default" type="button" id="btnSearch">BUSCAR</button>
        </div> -->
      </div>
      <font id="mensaje" style="display: none; color: blue;">Buscando...Espere por favor!</font>
      <font id="error" style="display: none; color: red">Ocurrió un Error. Por favor vuelva a intentar!</font>
      <label style="color: blue; display: none;" id="mensajeRespuesta"></label>
    </div>
  </div>
        <!-- <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <label for="nombre" class="col-sm-6 col-form-label">N° Matricula:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="numero_historia" name="numero_historia" value="{{ $model->numero_historia }}" placeholder="">
            </div>             
          </div> 
        </div> -->
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label for="nombre" class="col-sm-6 col-form-label">Apellido Paterno:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="primer_apellido" name="primer_apellido" value="{{ $model->primer_apellido }}" placeholder="">
            </div>  
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label for="nombre" class="col-sm-6 col-form-label">Apellido Materno:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" value="{{ $model->segundo_apellido }}" placeholder="">
            </div>  
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label for="nombre" class="col-sm-6 col-form-label">Nombre:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $model->nombre }}" placeholder="">
            </div>  
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label for="sexo" data-required="*">Sexo</label>
            <br> 
            <select name="idSexo" id="idSexo" class="form-control"> 
            <option disabled selected>Seleccionar</option>
              @foreach($modelDato as $dato)  
                <option value="{{$dato->id}}" {{ $model->sexo == $dato->id ? 'selected="selected"' : ''  }} >{{  $dato->nombre }}</option>  
              @endforeach
            </select>
          </div>
        </div> 
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="nombre" class="col-sm-6 col-form-label">Telf. o Movil:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="telefono" name="telefono" value="{{ $model->telefono }}" placeholder="">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="nombre" class="col-sm-6 col-form-label">Domicilio:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="domicilio" name="domicilio" value="{{ $model->domicilio }}" placeholder="">
            </div>            
          </div>
        </div> 
 
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <label for="nombre" class="col-sm-6 col-form-label">Comentarios:</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="comentarios" name="comentarios" value="{{ $model->comentarios }}" placeholder="">
            </div>             
          </div>    
        </div> 
      </div>
    </div>
  </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view') 
  <button type="button" id="btnGuardarPoblacion" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif
