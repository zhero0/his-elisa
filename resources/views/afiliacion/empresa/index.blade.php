@extends('adminlte::page')
@section('title', 'LISTA DE EMPRESAS')
@section('content_header')
    <h1>LISTA DE EMPRESAS AFILIADAS</h1>
@stop   

@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> --> 
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('empresa.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo registro
		        </a>
		    </div> 
		</div>
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr> 
 					<th>N° Empleador</th>
 					<th>Nombre o Razon social</th> 
 					<th>Representante legal</th>
 					<th>Telefono</th>
 					<th>Fecha Iniciacion</th>	 					
 					<th>Estado</th>	 						 					
 					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($model as $dato)					
					<tr role="row">
						<td class="col-xs-2">{{ $dato->nro_empleador }}</td>
						<td class="col-xs-3">{{ $dato->nombre }}</td>
						<td class="col-xs-2">{{ $dato->representante_legal }}</td> 
						<td class="col-xs-2">{{ $dato->telefono }}</td>			
						<td class="col-xs-1">{{ $dato->fecha_presentacion }}</td>		
						<td class="col-xs-1"> 	
						@foreach($estados as $estado)           
			              @if($dato->estado == $estado['id'] ) 			               
			                  {{$estado['nombre']}} 			              
			              @endif                        
			            @endforeach		
			            </td>	
						<td>
							<!-- <a href="{{ route('empresa.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a> -->
							<a href="{{ route('empresa.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>					
				@endforeach
			</tbody>
		</table>
	</div>  
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection