<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
   <!--  @if(count($errors) >0 )
      @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
      @endforeach
    @endif  -->  
        <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <label for="codigo" class="col-sm-6 col-form-label">N° del Empleador</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nro_empleador" name="nro_empleador" value="{{ $model->nro_empleador }}">
              </div> 
            </div> 
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <label for="codigo" class="col-sm-6 col-form-label">Nombre</label>
              <div class="col-sm-12">
                <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $model->nombre }}" placeholder="nombre de la empresa">
              </div> 
            </div>   
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <label for="codigo" class="col-sm-6 col-form-label">Zona</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="zona" name="zona" value="{{ $model->zona }}">
            </div> 
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <label for="codigo" class="col-sm-6 col-form-label">Calle</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="calle" name="calle" value="{{ $model->zona }}">
            </div> 
          </div>
          <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
            <label for="codigo" class="col-sm-6 col-form-label">Nro</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nro" name="nro" value="{{ $model->nro }}">
            </div> 
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label for="codigo" class="col-sm-6 col-form-label">Telefono</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="telefono" name="telefono" value="{{ $model->telefono }}">
            </div>  
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <label for="codigo" class="col-sm-9 col-form-label">Nombre del propietario o Representante legal</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="representante_legal" name="representante_legal" value="{{ $model->representante_legal }}" placeholder="Nombre del propietario o Representante legal">
            </div> 
          </div>
          <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <label for="codigo" class="col-sm-6 col-form-label">Fecha de iniciación</label>
            <div class="col-sm-12">
              <input type="date" class="form-control" id="fecha_presentacion" name="fecha_presentacion" value="{{ $model->fecha_presentacion }}">
            </div> 
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-11 col-md-4 col-lg-4">
            <label for="variable">Departamento</label>
            <select id="variable" name="variable" class="form-control" >
              <option value="0"> :: Seleccionar</option>
                @foreach($departamentos as $variable)            
                  @if($model != null)
                    <option value="{{$variable->id_municipio}}"
                    {{ $model->id_localidad == $variable->id_municipio ? 'selected="selected"' : ''  }} > {{$variable->dep_descripcion }} {{$variable->prov_descripcion }} {{$variable->mun_descripcion }}
                    </option>  
                  @else
                     <option value="{{$variable->id_municipio}}">{{$variable->dep_descripcion}}</option> 
                  @endif                        
                @endforeach
            </select> 
          </div> 
          
          <div class="col-xs-12 col-sm-11 col-md-4 col-lg-4">
            <label for="estado">Estado</label>
            <select id="estado" name="estado" class="form-control" >
              <option value="0"> :: Seleccionar</option>
                @foreach($estados as $estado)           
                  @if($model != null) 
                    <option value="{{$estado['id']}}"
                    {{ $model->estado == $estado['id'] ? 'selected="selected"' : ''  }} > {{$estado['nombre']}} 
                    </option>  
                  @else
                     <option value="{{$estado['id']}}">{{$estado['nombre']}}</option> 
                  @endif                        
                @endforeach
            </select> 
          </div> 
        </div>

</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif 
