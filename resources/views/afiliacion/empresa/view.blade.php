@extends('adminlte::page') 
@section('title', 'VISUALIZAR REGISTRO')  
@section('content_header')
    <h1>VISUALIZAR REGISTRO DE EMPLEADOR</h1>
@stop
@section('content')
<div class="row">
  <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
    <div class="card">
      <div class="card-body">
        {!! Form::model($model, ['route' => ['empresa.show', $model->id], 'method' => 'PUT', 'id' => 'form']) !!}
          @include($model->rutaview.'form')
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop