@extends('layouts.admin')
@section('contentheader_title', '')
@section('htmlheader_title', '')

@section('htmlheader_title')
    {{ trans('adminlte_lang::message.pagenotfound') }}
@endsection

@section('main-content')
<div class="box box-primary">
    <div class="box-header">
        <h1 class="text-yellow"> ACCESO DENEGADO </h1>
    </div>
    <div class="box-body">
        <p class="text-muted">
            Solicite privilegios con el Administrador, GRACIAS!
        </p>

        <?php
        if(isset($url))
        {
        ?>
        @if($url != '')
	        <a href="/{{ $url }} " class="btn btn-primary">
			  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
			</a>
		@endif
        <?php
        }
        ?>
    </div>
</div>
@endsection