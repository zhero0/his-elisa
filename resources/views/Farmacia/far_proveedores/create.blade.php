@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVO PROVEEDOR')
@section('htmlheader_title', 'NUEVO PROVEEDOR')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body"> 
	        	<form method="POST" action="{{ route('far_proveedor.store') }}" autocomplete="off" id="form">
      				{{ csrf_field() }}
      				@include($model->rutaview.'form')
      			</form>
			</div>
		</div>
	</div>
</div>
@stop