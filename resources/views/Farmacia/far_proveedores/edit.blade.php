@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR PLANTILLA')
@section('htmlheader_title', 'EDITAR PLANTILLA')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body">
	        	<form method="POST" action="{{ route('far_proveedor.update', $model->id) }}" autocomplete="off" id="form">
					{{ csrf_field() }}
      				<input name="_method" type="hidden" value="PATCH"> 
					@include($model->rutaview.'form')
				</form>
			</div>
		</div>
	</div>
</div>
@stop