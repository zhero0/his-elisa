@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE PROVEEDORES')
@section('htmlheader_title', 'LISTA DE PROVEEDORES')

@section('main-content') 

<?php
use Illuminate\Support\Facades\Input; 
?>
<div class="content">
	<div class="col-md-12">
	  	<div class="card">
	    	<div class="card-body">
	    		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
		 		<!-- "BUSCADOR MULTIPLE" -->
		 		<div class="row">
			 		<form id="formBuscador" class="navbar-form navbar-right" action="/far_proveedor" autocomplete="off">
			 			<textarea class="form-control" id="datosBuscador" name="datosBuscador" style="display: none;">{{ $model->datosBuscador }}</textarea>
			 		</form>
		 		</div>
		 		<!-- FIN "BUSCADOR MULTIPLE" -->


	    		<div class="row">
					<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
						<a type="button" class="btn btn-success btn-sm" rel="tooltip" href="{{ route('far_proveedor.create') }}" title="Nuevo Registro">
				            <i class="fa fa-plus" aria-hidden="true"></i> Nuevo
				        </a>
				    </div>
				</div>
			 	<div class="table-responsive table-bordered table-hover" style="{{ config('app.styleIndex') }}">
					<table class="table table-condensed">
						<thead style="{{ config('app.styleTableHead') }}">
							<tr>
							<th>Nombre</th> 
							<th>Descripción</th>												
							<th>Telf</th>												
							<th>Dirección</th>												
							<th>Estado</th>
							<th></th>
							</tr>
							<tr id="indexSearch" style="background: #ffffff;">								
			 					<th> 
					                <input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;"> 
			 					</th>
			 					<th> 
					                <input type="text" id="txtSearch_descripcion" class="form-control input-sm" style="text-transform: uppercase;"> 
			 					</th> 
			 					<th></th> 
			 					<th></th> 
			 					<th>
			 						<select id="selectSearch_estado" name="selectSearch_estado" class="form-control" onchange="Busqueda_index( $(this).attr('id') )" style="width: 170px;">
										<option value="2" selected>Seleccione Estado</option>
										@foreach($estados as $estado)           							            
								            <option value="{{ $estado['id'] }}"> {{ $estado['nombre'] }}  </option> 	
								        @endforeach	
									</select> 
			 					</th>
							</tr>
						</thead>
						<tbody>
							@foreach($model as $dato)
								<tr role="row" style="{{ config('app.styleTableRow') }}"> 
									<td class="text-left">{{ $dato->nombre }}</td>						
									<td>{{ $dato->descripcion }}</td>						
									<td class="text-left">{{ $dato->numero_movil }}</td>						
									<td class="text-left">{{ $dato->ubicacion }}</td>						
									<td class="td-actions text-right">
										<a href="{{ route('far_proveedor.edit', $dato->id) }}" class="btn btn-default btn-sm btn-round" rel="tooltip" title="Editar">
											<i class="fa fa-pencil"></i>								
										</a> 
										<a href="{{ route('far_proveedor.show', $dato->id) }}" class="btn btn-danger btn-sm btn-round" rel="tooltip" title="Cambiar estado">
											<i class="fa fa-trash"></i>								
										</a> 
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>

				<div class="text-center" style="{{ config('app.stylePaginacion') }}">
			      {!! $model->appends(Input::except('page'))->render() !!}
			    </div>
			</div>
	  	</div>
  	</div>
</div> 
@stop