<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}"> 

    <div class="col-md-12">
      <div class="card ">
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title">Datos</h4>
          </div>
        </div>
        <div class="card-body ">

          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <select name="departamento" class="form-control" >
                <option value="0">Seleccione Departamento</option>
                  @foreach($modelDepartamentos as $departamento)           
                      @if($model->idhcl_departamento == $departamento->id )                      
                          <option value="{{ $departamento->id }}" selected> {{ $departamento->dep_descripcion }}  </option>  
                      @else
                        <option value="{{ $departamento->id }}"> {{ $departamento->dep_descripcion }} </option>
                      @endif                        
                  @endforeach 
              </select> 
            </div>    
          </div>
          <br> 
          <br>
          <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
              <div class="form-group">          
                <label class="bmd-label-floating">Nombre o razon social</label> 
                  <input type="text" class="form-control input-sm" name="nombre" id="nombre" style="text-transform: uppercase;" value="{{ $model->nombre }}" required="true">                  
              </div>     
            </div>  
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <div class="form-group">           
                <label class="bmd-label-floating">Descripción</label>  
                <input type="text" class="form-control input-sm" name="descripcion" id="descripcion" style="text-transform: uppercase;" value="{{ $model->descripcion }}" required="true">                
              </div> 
            </div>     
          </div>

          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 
              <div class="form-group">
                <label class="bmd-label-floating">NIT</label>  
                <input type="text" class="form-control input-sm" name="nit" id="nit" style="text-transform: uppercase;" value="{{ $model->nit }}" required="true">
              </div>         
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
              <div class="form-group">
                <label class="bmd-label-floating">Número de Movil</label> 
                <input type="text" class="form-control input-sm" name="numero_movil" id="numero_movil" style="text-transform: uppercase;" value="{{ $model->numero_movil }}" required="true">
              </div>     
            </div>      

          </div> 
          <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
              <div class="form-group">         
                <label class="bmd-label-floating">Ubicación</label> 
                <input type="text" class="form-control input-sm" name="ubicacion" id="ubicacion" style="text-transform: uppercase;" value="{{ $model->ubicacion }}" required="true">
              </div>         
            </div>         
          </div>

        </div>
      </div>
    </div>  
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
   <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif