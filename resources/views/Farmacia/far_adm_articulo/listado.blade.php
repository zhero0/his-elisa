<div class="form-group"> 
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    <input type="hidden" id="idinv_idalmacen" value="{{ $model->idinv_idalmacen }}">
    <input type="hidden" id="inv_almacenesLista" value="{{ $model->inv_almacenesLista }}">
    <input type="hidden" id="especialidadProgramados" value="{{ $model->especialidadProgramados }}">
    <input type="hidden" id="txtJsonAlmacenes" name="txtJsonAlmacenes">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-12">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>
    
     <div class="row">
      <ul class="timeline"> 

        <!-- timeline item -->
        <li id="panelAlmacen">
          <i class="glyphicon glyphicon-tasks bg-red"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <!-- <h3 class="box-title"></h3> -->
                      <h4 class="list-group-item-heading text-primary">
                        <i class="fa fa-file-text-o"></i>
                        ALMACENES
                      </h4>
                    </div> 
                  </div>
                </div>
                <div class="box-body">
                  <div class="table-responsive" style="height: 300px; overflow-y: auto; border: 1px solid #374850;">
                    <table id="tablaContenedorAlmacenes" class="table table-bordered table-hover table-condensed">
                        <thead style="{{ config('app.styleTableHead') }}">
                            <tr>                              
                              <th>Nombre</th>
                              <th class="col-xs-5">Descripción</th>
                              <th class="col-xs-1"></th>
                           </tr>
                        </thead>
                        <tbody class="table-success" id="tbodyAlmacenes" style="{{ config('app.styleTableRow') }}">
                          <tr>
                            <td>Vacío...</td>
                          </tr>
                        </tbody>
                    </table>
                  </div>

                  <button class="btn btn-default" id="btnVisualizaAlmacenSeleccionados" type="button"  title="Click para visualizar los Exámenes Seleccionados">
                    Total Seleccionados <span class="badge" id="spanTotalAlmacenSeleccion">0</span>
                  </button>

                </div>
              </div>
            </div>
            <!-- <div class="timeline-footer">
              <div class="alert alert-danger" id="divErrorExamen" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Seleccione algún EXÁMEN
              </div>
            </div> -->
          </div>
        </li>
        <!-- END timeline item --> 
        <li>
          <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
        </li>
      </ul>  
    </div>
 
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a> 
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarAlmacen" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif