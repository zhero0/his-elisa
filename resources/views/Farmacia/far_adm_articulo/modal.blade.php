
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header card-header-rose card-header-text">
        <div class="card-text">
          <h4 class="card-title">Lista de articulos almacenes</h4>
        </div>
      </div>
      <div class="card-body ">

        <div class="box-body">
          <div class="table-responsive" style="{{ config('app.styleIndex') }}">
            <table class="table table-bordered table-hover table-condensed">
              <thead style="{{ config('app.styleTableHead') }}">
                <tr> 
                  <th>Código</th>
                  <th>Medicamento/Insumo</th>
                  <th>Unidad</th>
                  <th>Concetraciòn</th>
                  <th>Envase</th>                                          
                  <th>Psicotropico</th>
                  <th>Estupefaciente</th>
                  <th>Autorizado</th>   
                </tr>
              </thead>
              <tbody>
                @foreach($model as $dato) 
                  <tr role="row" style="{{ config('app.styleTableRow') }}"> 
                    <td class="text-left">{{ $dato->res_codificacion_nacional}}</td>           
                    <td class="text-left">{{ $dato->res_descripcion }}</td>            
                    <td class="text-left">{{ $dato->res_unidad }}</td>     
                    <td class="text-left">{{ $dato->res_concentracion }}</td>     
                    <td class="text-left">{{ $dato->res_tipo_envase }}</td>     
                    <td class="text-left">{{ $dato->res_psicotropico }}</td>     
                    <td class="text-left">{{ $dato->res_estupefaciente }}</td>     
                    @foreach($modelEstado as $estado)
                      @if($dato->res_estado == $estado->id)
                        <td class="text-left">{{ $estado->nombre }}</td>                             
                      @endif
                    @endforeach 
                  </tr>  
                @endforeach
              </tbody>
            </table>
          </div>
          
          <div class="text-center" style="{{ config('app.stylePaginacion') }}">
            {!! $model->appends(Input::except('page'))->render() !!}
          </div>
        </div> 
      </div>
    </div>
  </div>
 