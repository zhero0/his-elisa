@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTA DE ARTICULOS REGISTRADOS</h1>
@stop   

@section('content')
<div class="card"> 
	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">  
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('articulo.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div>
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Código</th>
					<th>Medicamento/Insumo</th>
					<th>Unidad</th>
					<th>Clasificacion</th>  											
					<th>Autorizado</th>												
					<th>Responsable</th>
					<th></th>
				</tr>
				<tr id="indexSearch" style="background: #ffffff;">
					<!-- <th></th>  -->
					<th> 
	                <input type="text" id="txtSearch_codigo" class="form-control input-sm" style="text-transform: uppercase;">
					</th>
					<th> 				                        
						<input type="text" id="txtSearch_descripcion" class="form-control input-sm" style="text-transform: uppercase;"> 
					</th>
					<th> 
						<input type="text" id="txtSearch_unidad" class="form-control input-sm" style="text-transform: uppercase;">
					</th>
					<th>
						<select id="selectSearch_clasificacion" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
		            	<option value="0" selected> :: Seleccionar</option>
		                @foreach($modelClasificacion as $dato)
		                	<option value="{{ $dato->id }}">{{ $dato->codificacion }}</option>
		                @endforeach
		            </select>
					</th> 
					<th>
						<select id="selectSearch_estado" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
		            	<option value="0" selected> :: Seleccionar</option>
		                @foreach($modelEstados as $dato)
		                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
		                @endforeach
		            </select>
					</th> 
					<th> 				                        
						<input type="text" id="txtSearch_responsable" class="form-control input-sm" style="text-transform: uppercase;">
					</th>  
				</tr> 
			</thead>
			<tbody id="indexContent">
				@include($model->rutaview.'indexContent')
			</tbody>
		</table>
	</div>  
</div> 
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 
 