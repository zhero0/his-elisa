<div class="form-group"> 
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}"> 
</div> 
<div class="row">
  <div class="col-xs-12 col-sm-10 col-md-6 col-lg-6">
    <div class="card">
      <div class="card-body">           
          <span class="username">
            <a href="#">Clasificación del articulo</a>
            <p>Escoger la clasificación del articulo que desea publicar.</p>
          </span>  
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                       
          <select name="idmed_clasificacion" id="idmed_clasificacion" class="form-control" onchange="seleccionarClasificacion()"> 
            <option disabled selected>Seleccionar</option> 
            @foreach($modelClasificacion as $dato) 
              @if($model != null)
                <option value="{{$dato->id}}" {{ $model->idmed_clasificacion == $dato->id ? 'selected="selected"' : ''  }} >{{$dato->descripcion }}</option> 
              @else
                <option value="{{$dato->id}}">{{$dato->descripcion}}</option> 
              @endif
            @endforeach
          </select> 
        </div> 
      </div>
    </div> 
    <div class="card">
      <div class="card-body">   
        <strong>Crear articulo</strong> 
        <p>Describe los detalles del articulo.</p>
        <div class="row">
          <div class="col-xs-2 col-sm-2 col-md-3 col-lg-3">                       
            <div class="form-group">                        
              <label class="bmd-label">Codificación externa</label>  <br> 
              <input type="text" class="form-control" id="txtCosto_tramite" value="{{$model->costo_tramite}}" name="costo_tramite" readonly="">
            </div> 
          </div>
          <div class="col-xs-2 col-sm-2 col-md-3 col-lg-3">                       
            <div class="form-group">                        
              <label class="bmd-label">Codificación interna</label>  <br> 
              <input type="text" class="form-control" id="txtCosto_tramite" value="{{$model->costo_tramite}}" name="costo_tramite" readonly="">
            </div>  
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">                        
              <label class="bmd-label">Detalle del articulo</label>  <br> 
              <input type="text" class="form-control" id="txtCosto_tramite" value="{{$model->costo_tramite}}" name="costo_tramite" readonly="">
            </div>  
          </div>
        </div>
        <div class="row">
           <div class="col-xs-2 col-sm-2 col-md-3 col-lg-3">                       
            <div class="form-group">                        
              <label class="bmd-label">Codificación externa</label>  <br> 
              <input type="text" class="form-control" id="txtCosto_tramite" value="{{$model->costo_tramite}}" name="costo_tramite" readonly="">
            </div> 
          </div>
          <div class="col-xs-2 col-sm-2 col-md-3 col-lg-3">                       
            <div class="form-group">                        
              <label class="bmd-label">Codificación interna</label>  <br> 
              <input type="text" class="form-control" id="txtCosto_tramite" value="{{$model->costo_tramite}}" name="costo_tramite" readonly="">
            </div>  
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="form-group">                        
              <label class="bmd-label">Detalle del articulo</label>  <br> 
              <input type="text" class="form-control" id="txtCosto_tramite" value="{{$model->costo_tramite}}" name="costo_tramite" readonly="">
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="col-md-1 col-lg-1"> 
  </div>
  <div class="col-xs-12 col-sm-10 col-md-4 col-lg-4">
    <div class="card">
      <div class="card-body"> 
        <div class="user-block">
          <img class="img-circle img-bordered-sm" src="../imagenes/farmacia_icono.jpg" alt="user image">
          <span class="username">
            <a href="#">Imagen del articulo</a>
          </span>
          <span class="description">Seleccione una imagen del articulo que desee incluir en la galeria.</span>
        </div> 
        <br>  
        <br>  
        @if($model->imagen != null)
          <img height="500" width="500" alt="" referrerpolicy="origin-when-cross-origin" src="../uploads/$model->imagen">
        @else
          <i alt="" data-visualcompletion="css-img" class="img" style="background-image: url(&quot;https://static.xx.fbcdn.net/rsrc.php/v3/yi/r/GiFSkTObexJ.png&quot;); background-position: 0px 0px; background-size: auto; width: 500px; height: 400px; background-repeat: no-repeat; display: inline-block;"></i>        
        @endif       
        
        <div class="row">          
          <div class="form-group">
            <div class="btn btn-default btn-file">
              <i class="fas fa-paperclip"></i> Attachment
              <input type="file" name="attachment">
            </div>
            <p class="help-block">Max. 2MB</p>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

 



<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a> 
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Actualizar articulos farmacia
  </button>
@endif