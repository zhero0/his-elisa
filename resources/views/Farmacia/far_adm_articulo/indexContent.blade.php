@foreach($model as $dato)
    <tr role="row" class="{{ $dato->eliminado == 1? 'danger':'default'}}" style="{{ config('app.styleTableRow') }}">
        <td class="text-left">{{ $dato->codificacion_nacional }}</td>						
		<td>{{ $dato->descripcion }}</td>
		<td class="text-left">{{ $dato->unidad }}</td>
		<td class="text-left">
		@foreach($modelClasificacion as $clase)
			@if($clase->id == $dato->idfar_adm_clasificacion)
        		{{ $clase->descripcion }}
        	@endif
        @endforeach	
        </td> 
        <td class="text-left">
		@foreach($modelEstados as $clase)
			@if($clase->id == $dato->idfar_adm_estado)
        		{{ $clase->nombre }}
        	@endif
        @endforeach	
        </td> 
        <td class="text-left">{{ $dato->usuario }}</td>  
    </tr>
@endforeach 