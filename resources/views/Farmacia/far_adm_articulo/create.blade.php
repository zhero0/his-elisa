@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>NUEVO REGISTRO DE ARTICULO</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('articulo.store') }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	@include($model->rutaview.'form') 	
</form>
@stop 