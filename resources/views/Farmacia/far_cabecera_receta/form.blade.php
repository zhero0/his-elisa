<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" id="txtEstado" name="txtEstado" value="{{ $model->estado }}">        
    <input type="hidden" id="txtJsonExamenes" name="txtJsonExamenes">    
    <input type="hidden" id="idfar_cabecera_receta" name="idfar_cabecera_receta">
    <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">
    <input type="hidden" id="idhcl_cobertura" name="idhcl_cobertura" value="{{ $model->idhcl_cobertura }}">
    <input type="hidden" id="idfar_adm_tipotransaccion" name="idfar_adm_tipotransaccion" value="{{ $model->idfar_adm_tipotransaccion }}">
    <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
    <input type="hidden" id="idalmacenes" value="{{ $model->idalmacenes }}">    
    <input type="hidden" id="txtcontenidoTabla" name="txtcontenidoTabla" value="{{ $model->documentodetalle }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div> 
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <!-- <div class="alert alert-warning alert-dismissible" id="divMensajeError" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>REVISE! </strong> Aqui el texto
      </div> -->

      <div id="divMensajeAdvertencia" class="alert alert-danger alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <!-- <label id="lblMensajeError"> Complete los datos requeridos! </label> -->
        <strong>REVISE! </strong> <label id="lblMensajeError"> Aqui el texto </label>
      </div>
    </div>
    </div>
    
    <div class="row">  
      <!-- <h1 style="color: red; font-size: 20px;">ULTIMO FOLIO DISPENSADO: {{$model->numeroFolio}}</h1> -->
        <!-- timeline MEDICAMENTOS -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
 
          <h4 class="list-group-item-heading text-primary">
            <i class="fa fa-barcode"></i>
            Código de barra
          </h4>
          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-success">
                <div class="box-header with-border">
                  <div class="row"> 
                      <!-- <h3 class="box-title">PACIENTE</h3> -->
                     
                    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"> 
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="input-group input-group-sm">
                        <input type="text" id="txtsearchCode" name="txtsearchCode" class="form-control" placeholder="Escriba..." autofocus="autofocus" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModal_Codigo" title="Click para Buscar">
                            <span class="fa fa-search" aria-hidden="true"></span>.
                          </button>
                        </span>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                      <div class="input-group input-group-sm">
                        <button type="button" id="btnManual" class="btn btn-sm btn-danger" >
                          <span class="fa fa-clone" aria-hidden="true"></span> 
                            RECETA MANUAL
                        </button>    
                      </div>
                    </div>
                  </div>
                </div> 
              </div>
            </div> 
          </div> 
        <!-- END timeline PACIENTE --> 
        <!-- timeline PACIENTE -->  
            <h4 class="list-group-item-heading text-primary">
              <i class="fa fa-user"></i>
                PACIENTE
            </h4>
            <div class="timeline-item">
              <div class="timeline-body">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 
                      </div>

                      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-7">
                        <div class="input-group input-group-sm">
                          <input type="text" id="txtBuscarPaciente" class="form-control" placeholder="Escriba..." autofocus="autofocus" style="text-transform: uppercase;">
                          <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="btnModalPaciente" title="Click para Buscar">
                              <span class="fa fa-search" aria-hidden="true"></span>.
                            </button>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
                        <input type="hidden" id="txtidhcl_poblacion" name="txtidhcl_poblacion">
                        <dl class="dl-horizontal">
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <dt>DOCUMENTO:</dt>
                              <input type="hidden" id="txtiddocumento" name="txtiddocumento">
                              <dd><input type="text" id="txtDocumento" class="form-control input-sm" style="text-transform: uppercase;" value=""></dd>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <dt>TELEFONO:</dt>
                              <input type="hidden" id="txtidtelefono" name="txtidtelefono">
                              <dd><input type="text" id="txtTelefono" class="form-control input-sm" style="text-transform: uppercase;" value=""></dd>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <dt>DOMICILIO:</dt>
                              <input type="hidden" id="txtiddomicilio" name="txtiddomicilio">
                              <dd><input type="text" id="txtDomicilio" class="form-control input-sm" style="text-transform: uppercase;" value=""></dd>
                            </div>                                  
                          </div>
                        </dl>
                      </div>
                    </div>                   
                      <div class="row"> 
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                          <label>Especialidad</label>
                          <div class="input-group">
                            <input type="text" id="txtEspecialidad" name="txtEspecialidad" class="form-control" value="" disabled="">
                            <span class="input-group-btn">
                              <button class="btn btn-primary" type="button" id="btnModal_Especialidad" title="Click para Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>.
                              </button>
                            </span>
                          </div>
                        </div> 
                        <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                          <label>Médico Solicitante</label>
                          <div class="input-group">                          
                            <input type="hidden" id="txtMatricula" name="txtMatricula">
                            <input type="text" id="txtMedico" name="txtMedico" class="form-control" disabled="">
                            <span class="input-group-btn">
                              <button class="btn btn-primary" type="button" id="btnModal_Medico" title="Click para Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>.
                              </button>
                            </span>
                          </div>
                        </div> 
                      </div>
                      <div class="row"> 
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                          <input type="hidden" id="txtidcobertura" name="txtidcobertura">
                          <input type="hidden" id="txtCant_medicamento" name="txtCant_medicamento">
                          <label>Cobertura</label>
                          <div class="input-group">
                            <input type="text" id="txtCobertura" name="txtCobertura" class="form-control" value="" disabled="">
                            <span class="input-group-btn">
                              <button class="btn btn-primary" type="button" id="btnModal_Cobertura" title="Click para Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>.
                              </button>
                            </span>
                          </div>
                        </div> 
                        <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                          <label>Formulario</label>
                          <div class="input-group">
                            <input type="hidden" id="txtinv_tipo_transaccion" name="txtinv_tipo_transaccion">
                            <input type="text" id="txtFormulario" name="txtFormulario" class="form-control" value="" disabled="">
                            <span class="input-group-btn">
                              <button class="btn btn-primary" type="button" id="btnModal_Formulario" title="Click para Buscar">
                                <span class="fa fa-search" aria-hidden="true"></span>.
                              </button>
                            </span>
                          </div> 
                        </div> 
                      </div>
                     <!--  <div class="row">  
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                          {!! Form::label('diagnostico', 'Diagnostico:', ['class' => 'label-control']) !!}
                          {!! Form::textarea('diagnostico', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'rows' => 2]) !!}
                        </div>
                      </div> -->
                  </div>
                </div>
              </div> 
            </div> 
        </div>
        <!-- END timeline MEDICAMENTOS --> 
        <!-- timeline MEDICAMENTOS DISPENSADOS --> 
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-danger"> 
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                      <h4 class="list-group-item-heading text-primary"> 
                        MEDICAMENTOS DISPENSADOS
                      </h4> 
                    </div>
                  </div>
                  <div class="table-responsive" style="max-height: 200px; overflow-y: auto; border: 1px solid #374850;">
                    <table id="tablaContenedorMedicamentos" class="table table-bordered table-hover table-condensed" style="overflow-y:scroll">
                        <thead style="{{ config('app.styleTableHead') }}">
                            <tr>
                              <th class="col-xs-2">FECHA</th>
                              <th class="col-xs-1">CODIGO</th>
                              <th>MEDICAMENTO/INSUMO</th>   
                              <th class="col-xs-1">CANT.</th>
                              <th class="col-xs-2">TIPO</th>
                              <th class="col-xs-3">UNIDAD</th>
                              <th class="col-xs-4">ESPECIALIDAD</th>
                           </tr>
                        </thead>
                        <tbody class="table-success" id="tbodyMedicamentoLista" style="{{ config('app.styleTableRow') }}">
                          <tr>
                            <td>Vacío...</td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
           
          <h4 class="list-group-item-heading text-primary"> 
            <i class="fa fa-medkit bg-red"></i>
            Medicamentos a dispensar
          </h4>
          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <div class="row"> 
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                      <div class="input-group input-group-sm">
                        <input type="text" id="txtBuscar_Medicamento" class="form-control" placeholder="Escriba..." autofocus="autofocus" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModal_Medicamento" title="Click para Buscar">
                            <span class="fa fa-search" aria-hidden="true"></span>.
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="table-responsive" style="max-height: 200px; overflow-y: auto; border: 1px solid #374850;">
                    <table id="tablaContenedorMedicamento" class="table table-bordered table-hover table-condensed" style="overflow-y:scroll">
                        <thead style="{{ config('app.styleTableHead') }}">
                            <tr>
                              <th class="col-xs-2">CÓDIGO</th>
                              <th>DESCRIPCIÓN</th>                                  
                              <th class="col-xs-1">CANTIDAD</th>
                              <th class="col-xs-2">SALDO</th>
                              <th class="col-xs-2">SUBTOTAL</th>
                              <th>Remover</th>
                           </tr>
                        </thead>
                        <tbody class="table-success" id="tbodyMedicamento" style="{{ config('app.styleTableRow') }}">
                          <tr>
                            <td>Vacío...</td>
                          </tr>
                        </tbody>
                    </table>
                  </div> 

                </div>
                
              </div>
            </div> 

          </div> 
        </div>
        <!-- DESCRIPCION DE DATOS -->
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                      <dt>Sucursal:</dt>
                      <input type="hidden" id="txtidsucursal" value="{{ $model->idfar_sucursales == null? 0:$model->idfar_sucursales }}" name="txtidsucursal">
                      <dd><input type="text" id="txtSucursal" class="form-control input-sm" style="text-transform: uppercase;" value="{{ $model->sucursal }}" disabled=""></dd>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                      <dt>Ventanilla:</dt>
                      <input type="hidden" id="txtidventanilla" name="txtidventanilla" value="{{ $model->idfar_ventanillas == null? 0 : $model->idfar_ventanillas }}">
                      <dd><input type="text" id="txtVentanilla" class="form-control input-sm" style="text-transform: uppercase;" value="{{ $model->ventanilla }}" disabled=""></dd>
                      
                    </div> 
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                      <dt>Fecha registro:</dt>
                      <input type="hidden" id="txtFecha" name="txtFecha">
                      <dd><input type="text" id="txtFecha_Registro" class="form-control input-sm" style="text-transform: uppercase;" value="" disabled=""></dd>
                    </div> 
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                      <dt>Usuario:</dt>
                      <input type="hidden" id="txtiddatosgenericos" name="txtiddatosgenericos" value="{{ $model->iddatosgenericos }}">
                      <dd><input type="text" id="txtUsuario" class="form-control input-sm" style="text-transform: uppercase;" value="{{ $model->usuario }}" disabled=""></dd>
                    </div>  
                  </div>
                </div>
                <!-- END  -->
      <!-- END timeline  MEDICAMENTOS DISPENSADOS -->
    </div> 
</div>

<!-- <a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a> -->
@if($model->scenario != 'view')
      <button type="button" id="btnDispensar" class="btn btn-sm btn-success">
        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> DISPENSAR
      </button>
      <button type="button" id="btnAnular" class="btn btn-sm btn-danger" >
        <span class="fa  fa-trash-o" aria-hidden="true"></span> 
          ANULAR
      </button>  
      <button type="button" id="btnSalirr" class="btn btn-sm btn-primary" data-dismiss="modal">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> 
          SALIR   
      </button> 
      <button type="button" id="btnCancelar" class="btn btn-sm btn-primary" data-dismiss="modal">
        <span class="fa  fa-trash-o" aria-hidden="true"></span> 
          CANCELAR   
      </button>  
@endif
