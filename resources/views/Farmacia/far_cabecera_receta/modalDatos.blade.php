<!-- VENTANA MODAL PARA BUSCAR ESPECIALIDAD -->
<div class="modal fade" id="vModalBuscadorEspecialidad">
	<div class="modal-dialog modal-xs">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">ESPECIALIDADES</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Especialidad" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Especialidad">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Especialidad" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-10">Nombre</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Especialidad" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>


<!-- VENTANA MODAL PARA BUSCAR MÉDICOS -->
<div class="modal fade" id="vModalBuscadorMedicos">
	<div class="modal-dialog modal-xs">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">MÉDICOS</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Medico" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Medico">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Medico" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-5">Nombres</th>
		                        <th class="col-xs-6">Apellidos</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Medico" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>
 

<!-- VENTANA MODAL PARA BUSCAR COBERTURA -->
<div class="modal fade" id="vModalBuscadorCoberturas">
	<div class="modal-dialog modal-xs">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">COBERTURAS</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Cobertura" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Cobertura">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Cobertura" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-9">Descripción</th>		                        
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Cobertura" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>



<!-- VENTANA MODAL PARA BUSCAR FORMULARIO -->
<div class="modal fade" id="vModalBuscadorFormularios">
	<div class="modal-dialog modal-xs">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">FORMULARIOS</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Formulario" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Formulario">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Formulario" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
	                        	<th class="col-xs-3">Código</th>		                        
		                        <th class="col-xs-6">Descripción</th>		                        
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Formulario" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>


<!-- MODAL PARA BUSCAR MEDICAMENTOS -->
<div class="modal" id="vModalBuscador_Medicamentos">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> Buscar Medicamento </h4>
        </div>
        <div class="modal-body">
          <!-- <form action="#" id="form_BajaMedica" method="post" autocomplete="off"> -->
            <div class="input-group">
              <input type="text" id="txtBuscador_Medicamento" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
              <span class="input-group-btn">
                <button class="btn btn-warning" type="button" id="btnBuscar_Medicamento">
                  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                  Buscar
                </button>
              </span>
            </div>

            <div class="table-responsive" style="max-height: 380px; overflow-y: auto; border: solid 1px #3c8dbc;">
              <table id="tablaBuscador_Medicamento" class="table table-bordered table-hover table-condensed">
                <thead style="{{ config('app.styleTableHead') }}">
                  <tr>
                    <th>Código</th>
                    <th>Descripción</th>
                    <th>Saldo</th>
                    <!-- <th style="text-align: right;">Costo</th> -->
                    <th></th>
                  </tr>
                </thead>
                <tbody class="table-success" id="tbody_Medicamento" style="{{ config('app.styleTableRow') }}">
                </tbody>
              </table>
            </div>
          <!-- </form> -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div>