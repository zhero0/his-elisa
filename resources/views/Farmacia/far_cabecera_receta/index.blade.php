@extends('layouts.admin')
@section('contentheader_title', 'DISPENSACION')
@section('htmlheader_title', 'DISPENSACION')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')


<div class="box box-primary">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}"> 

  <div class="row">
    <div class="container-fluid">
      <div class="col-xs-3 col-sm-3 col-md-1 col-lg-1" style="text-align: left;">
        <div class="row" style="height: 6px; color: transparent;">.</div>  
          <a type="button" class="btn btn-primary" href="{{ route('dispensar.create') }}">
              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
          </a> 
        </div> 
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3" style="text-align: left;">
        <div class="row" style="height: 6px; color: transparent;">.</div> 
          <!-- <input type="text" id="txtsearchCode" name="txtsearchCode" class="form-control" value="{{$model->searchCode}}" placeholder="CODIGO DE BARRAS..."> -->
        </div>  
 
        <div class="col-xs-9 col-sm-9 col-md-8 col-lg-8 text-right">   
          <form class="navbar-form navbar-right" action="/dispensar" autocomplete="off">
            
             

            <select name="estado" class="form-control" onchange="this.form.submit()" style="width: 170px;">
              <option value="0">Seleccione Estado</option>
              @foreach($estados as $estado)           
                @if($model->estado == $estado->id )                      
                    <option value="{{ $estado->id }}" selected> {{ $estado->nombre }}  </option>  
                @else
                  <option value="{{ $estado->id }}"> {{ $estado->nombre }} </option>
                @endif                        
              @endforeach 
            </select>  
            <select name="idfar_sucursales" class="form-control" onchange="this.form.submit()">       
              @foreach($modelAlmacenes as $dato) 
                <option value="{{$dato->id}}"> {{$dato->res_nombre_ventanilla}} </option> 
              @endforeach
            </select>
          </form>    
        </div>
    </div>
  </div>

  
  <div class="box-body">
    <div class="table-responsive" style="{{ config('app.styleIndex') }}">
      <table class="table table-bordered table-hover table-condensed">
        <thead style="{{ config('app.styleTableHead') }}">
          <tr>
            <th>Nº FOLIO</th>
            <th>H.C.</th>
            <th>PACIENTE</th>
            <th>ESPECIALIDAD</th>            
            <th>EMPRESA</th>
            <th>FECHA</th>
            <th>ESTADO</th>
            <th></th>
          </tr>
          <tr id="indexSearch" style="background: #ffffff;">
            <th>
              <input type="text" id="txtSearch_numero" class="form-control input-sm">
            </th>
            <th>
              <input type="text" id="txtSearch_numeroHistoria" class="form-control input-sm" style="text-transform: uppercase;">
            </th>
            <th>
              <input type="text" id="txtSearch_asegurado" class="form-control input-sm" style="text-transform: uppercase;">
            </th>
            <th> 
               <input type="text" id="txtSearch_especialidad" class="form-control input-sm" style="text-transform: uppercase;">
            </th>            
            <th>
              <input type="text" id="txtSearch_folio" class="form-control input-sm" style="text-transform: uppercase;">
            </th> 
            <th>
              
            </th> 
          </tr>
        </thead>
        @foreach($model as $dato)
        <tbody>
          <tr role="row" style="{{ config('app.styleTableRow') }}">
            <td>{{ $dato->res_folio }}</td>
            <td class="col-xs-1">{{ $dato->res_hcl_poblacion_cod }} </td>
            <td class="col-xs-3">{{ $dato->res_hcl_poblacion_nombrecompleto }}</td>
            <td class="col-xs-2">{{ $dato->res_nombre_cuaderno }}</td>
            <td class="col-xs-3">{{ $dato->res_hcl_empleador_empresa }}</td>
            <td class="col-xs-1">{{ $dato->res_fechadispensacion }}</td>
            <td class="col-xs-3">{{ $dato->res_nombre_estado }}</td> 
            <td class="col-xs-1">
              <!-- <a href="{{ route('consulta_externa.edit', $dato->res_id) }}" class="btn btn-default btn-sm" title="Editar">
                <i class="glyphicon glyphicon-pencil"></i>
              </a> -->
              <!-- <button type="button" id="{{$dato->res_id}}" class="btn btn-default btn-sm" title="LISTA DE MEDICAMENTOS">
                <i class="fa fa-medkit bg-red"></i>
              </button> -->
            </td>
          </tr>
        </tbody>
        @endforeach

      </table>
    </div>

    <div class="text-center" style="{{ config('app.stylePaginacion') }}">
      {!! $model->render() !!}
    </div>
  </div>
</div>
@stop


<div id="secccionImprimeAnalisis" style="visibility: hidden;"></div>

<div class="modal" id="vModalBuscadorReceta">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title"> DATOS RECETA </h4>
        </div>
        <div class="row">
        <ul class="timeline">
          <li>
            <i class="fa fa-file-text-o bg-yellow"></i>

            <div class="timeline-item">
              <div class="timeline-body">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <div class="row">
                      <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h3 class="box-title">DATOS PERSONA</h3>
                      </div>
                    </div>
                  </div>                         
                  <div class="box-body">
                    <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                        <input type="hidden" id="txtidhcl_poblacion" name="txtidhcl_poblacion">
                        <dl class="dl-horizontal">
                          <dt>PACIENTE:</dt>
                          <dd id="datoNumeroPaciente"></dd> 
                          <dt>ESPECIALIDAD:</dt>
                          <dd id="datoEspecialidad"></dd>
                          <dt>MEDICO:</dt>
                          <dd id="datoMedico"></dd>
                          <dt>DIAGNOSTICO:</dt>
                          <dd id="datoDiagnostico"></dd>

                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                              <dt>Documento:</dt>
                              <dd><input type="text" id="txtDocumento" class="form-control input-sm" style="text-transform: uppercase;"></dd>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                              <dt>Teléfono:</dt>
                              <dd><input type="text" id="txtTelefono" class="form-control input-sm" style="text-transform: uppercase;"></dd>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                              <dt>Domicilio:</dt>
                              <dd><input type="text" id="txtDomicilio" class="form-control input-sm" style="text-transform: uppercase;"></dd>
                            </div>                                  
                          </div> 
                        </dl>
                      </div>
                    </div>
                    <div class="box-header with-border">
                      <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                          <h4 class="list-group-item-heading text-primary"> 
                            MEDICAMENTOS
                          </h4> 
                        </div>
                      </div>
                    </div>
                    <div class="box-body">
                      <div class="table-responsive" style="max-height: 200px; overflow-y: auto; border: 1px solid #374850;">
                        <table id="tablaContenedorMedicamento" class="table table-bordered table-hover table-condensed">
                            <thead style="{{ config('app.styleTableHead') }}">
                                <tr>
                                  <th class="col-xs-2">Código</th>
                                  <th>Medicamento/Insumo</th>
                                  <th class="col-xs-2">Unidad</th>
                                  <th class="col-xs-2">Cantidad</th>
                                  <th class="col-xs-2">Saldo</th>
                               </tr>
                            </thead>
                            <tbody class="table-success" id="tbodyMedicamento" style="{{ config('app.styleTableRow') }}">
                              <tr>
                                <td>Vacío...</td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>

                    <div class="timeline-item">
                      <div class="timeline-body">
                        <div class="box box-danger"> 
                          <div class="box-body">
                            <div class="row">
                              <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                <h4 class="list-group-item-heading text-primary"> 
                                  MEDICAMENTOS DISPENSADOS
                                </h4> 
                              </div>
                            </div>
                            <div class="table-responsive" style="max-height: 200px; overflow-y: auto; border: 1px solid #374850;">
                              <table id="tablaContenedorMedicamentos" class="table table-bordered table-hover table-condensed">
                                  <thead style="{{ config('app.styleTableHead') }}">
                                      <tr>
                                        <th class="col-xs-1">Fecha</th>
                                        <th class="col-xs-2">Código</th>
                                        <th>Medicamento/Insumo</th>   
                                        <th class="col-xs-2">Cantidad</th>                               
                                        <th class="col-xs-3">Especialidad</th>
                                        <th class="col-xs-3">Diagnóstico</th>
                                     </tr>
                                  </thead>
                                  <tbody class="table-success" id="tbodyMedicamentos" style="{{ config('app.styleTableRow') }}">
                                    <tr>
                                      <td>Vacío...</td>
                                    </tr>
                                  </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> 
                </div>                  
              </div> 
            </div>
          </li> 
        </ul>
      </div>
    <div class="modal-footer">
      <!-- <button type="button" id="btnDispensar" class="btn btn-sm btn-success">
        <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> DISPENSAR
      </button> -->
      <button type="button" id="btnAnular" class="btn btn-sm btn-danger" >
        <span class="fa  fa-trash-o" aria-hidden="true"></span> 
          ANULAR
      </button>  
      <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">
        <span class="fa  fa-trash-o" aria-hidden="true"></span> 
          SALIR   
      </button>        
    </div>
    </div>
  </div>
</div>