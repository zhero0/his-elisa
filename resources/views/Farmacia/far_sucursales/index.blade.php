@extends('adminlte::page')
@section('title', 'LISTA DE SUCURSALES')
@section('content_header')
    <h1>LISTA DE SUCURSALES REGISTRADOS</h1>
@stop   

@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
	    		<input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-primary" href="{{ route('sucursal.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div>
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr> 
 					<th>Codigo</th> 
					<th>Nombre</th> 
					<th>Usuario</th>
				</tr>
			</thead>
			<tbody>
				@foreach($model as $dato)					
					<tr role="row">
						<td class="text-left">{{ $dato->codigo_almacen }}</td>						
						<td class="text-left">{{ $dato->nombre }}</td>						
						<td class="text-left">{{ $dato->usuario }}</td>  
						<td  class="col-xs-1">							
							<a href="{{ route('sucursal.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
						</td>
					</tr>					
				@endforeach
			</tbody>
		</table>
	</div>  
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection  