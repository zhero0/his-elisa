@extends('adminlte::layouts.app')
@section('contentheader_title', 'VER INGRESO')
@section('htmlheader_title', 'VER INGRESO')

@section('main-content')
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	    <div class="panel panel-primary">
	      	<div class="panel-body">
	      		<input type="hidden" id="txtAccion" value="{{ $model->accion }}">
				<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
				@include('layouts.datosDocumento')
				
				{!! Form::model($model, ['route' => ['notaIngreso.show', $model->id], 'method' => 'PUT', 'id' => 'form', 'autocomplete' => 'off']) !!} 
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop