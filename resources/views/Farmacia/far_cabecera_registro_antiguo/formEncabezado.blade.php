<div class="container-fluid" style="background: rgb(236, 240, 245); color: black; border: 1px solid rgb(48, 151, 209); border-radius: 7px;">
	<div class="row"> 
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<label class="label-control">
				SUCURSAL 
			</label>
			<select id="sucursal" class="form-control">
				@foreach($modelSucursales as $sucursal)
					@if($model != null)
						<option value="{{$sucursal->id}}" {{ $model->idfar_sucursales == $sucursal->id ?'selected="selected"' : '' }}  >{{$sucursal->nombre}}</option>
					@else
						 <option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option> 
					@endif 
                @endforeach 
			</select>  
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<label class="label-control">
				TIPO INGRESO 
			</label>
			<select id="ingreso" class="form-control">
				@foreach($modelIngresos as $ingreso) 
					<option value="{{$ingreso->id}}" >{{$ingreso->descripcion}}</option>
                @endforeach 
			</select>  
		</div>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
			<label class="label-control">
				CLASIFICACION 
			</label>
			<select id="clasificacion" class="form-control">
				@foreach($modelClasificacion as $clasificacion)
					@if($model != null)
						<option value="{{$clasificacion->id}}" {{ $model->idfar_adm_clasificacion == $clasificacion->id ?'selected="selected"' : '' }}  >{{$clasificacion->descripcion}}</option>
					@else
						 <option value="{{$clasificacion->id}}">{{$clasificacion->descripcion}}</option> 
					@endif 
                @endforeach 
			</select>  
		</div>

	    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	      	<label>PROVEEDOR</label>
	      	<div class="input-group">
	          <span class="input-group-btn">
	            <button class="btn btn-default" type="button" id="btnBuscadorProveedor" title="Click Para Buscar Proveedor! ">
	              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>.
	            </button>
	          </span>
	          <input type="text" id="txtBuscaProveedor" class="form-control" style="text-transform: uppercase;" disabled>
	        </div>
	    </div>
	    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
	        <label class="label-control">DETALLE</label>
	        <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion" placeholder="Escriba..." style="width: 100%;" value="{{ $model->descripcion }}">
	    </div>
    	
	    
	</div>
	
	@if($model->scenario == 'view' && $model->eliminado == true)
	<div class="row">
    	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-3">
    		<label class="label-control">USUARIO ELIMINADO</label>
    		<input type="text" class="form-control" value="{{ $model->usuario_destroy }}" disabled>
    	</div>
    	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-3">
    		<label class="label-control">FECHA ELIMINADO</label>
    		<input type="text" class="form-control" value="{{ $model->fecha_destroy }}" disabled>
    	</div>
    	<div class="col-xs-12 col-sm-5 col-md-5 col-lg-6">
    		<label class="label-control">MOTIVO ELIMINADO</label>
    		<input type="text" class="form-control" value="{{ $model->motivo_destroy }}" disabled>
    	</div>
    </div>
    @endif

  	<div class="row" style="height: 6px; color: transparent;">.</div>
</div>

<div class="row" style="height: 6px; color: transparent;">.</div>

<?php
// @include('layouts.datosDocumento')
?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
		<table id="tablaContenedorDatos" class="table table-bordered table-condensed">
          <thead >
              <tr>
                <th>Producto</th>
                <th>Lote</th>
                <th>Fecha de Vto</th>
                <th>Cantidad</th> 
                <th>Saldo</th> 
                <th></th>
             </tr>
          </thead>
          <tbody class="table-success">
            <tr>
              <td>
              	<input type="hidden" class="form-control" id="txtHiddenIdProducto">
              	<input type="hidden" class="form-control" id="txtHiddenCodigoBarra">

              	<div class="input-group">
		          <span class="input-group-btn">
		            <button class="btn btn-default" type="button" id="btnBuscadorProducto" title="Click Para Buscar Producto">
		              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>.
		            </button>
		          </span>
		          <input type="text" id="txtValor" class="form-control" placeholder="Escriba...Código, Nombre ó Código de Barra" style="text-transform: uppercase;" autofocus="true">
		        </div>
              </td>
              <td class="col-xs-2">
              	<input type="text" class="form-control" id="txtLote" >
              	<!-- <input type="text" class="form-control" id="txtPrecio" > -->
              </td>
              <td class="col-xs-2">
              	<!-- <input type="text" class="form-control" id="txtSaldo" disabled> -->
              	<!-- {!! Form::date('txtFecha_vencimiento', null, ['class' => 'form-control',  'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!} -->
              	<input class="form-control" type="date" value="2011-08-19" id="txtFecha_vencimiento">
              	<!-- <input type="text" id="txtFecha_vencimiento" value="1" class="form-control input-xs"> -->

              </td>
              <td class="col-xs-1">
              	<input type="text" id="txtCantidad" value="0" class="form-control input-xs"> 
              </td>
              <td class="col-xs-1">
              	<input type="text" id="txtSaldo" value="0" class="form-control input-xs" disabled> 
              </td>
              <!-- <td class="col-xs-1">
	  			<input type="text" class="form-control" id="txtCosto" value="0">
              </td> -->
              <td class="col-xs-1">
			  	<button class="btn btn-success btn-sm" id="btnAgregarTupla" aria-label="Left Align" title="Click para agregar a la lista de Item">
				    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</button>

				<button class="btn btn-danger btn-sm" id="btnLimpiarDatos" aria-label="Left Align" title="Limpiar datos">
				    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
              </td>
            </tr>
          </tbody>
      	</table>
		</div>
</div>