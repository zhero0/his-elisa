<?php
	use Illuminate\Support\Facades\Input;
	use \App\Models\Almacen\Datosgenericos;
	use \App\Models\Almacen\Tipodocumento;
?>

@extends('layouts.admin')
@section('contentheader_title', 'LISTADO DE INGRESOS')
@section('htmlheader_title', 'LISTADO DE INGRESOS')

@section('main-content')
<div class="box box-primary">
	<div class="row">
		<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
		
		<div class="container-fluid">
			<div class="col-xs-12 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('far_cabecera_registro.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		    <div class="col-xs-0 col-sm-3 col-md-5 col-lg-5">
		    	<!-- <div class="row" style="height: 6px; color: transparent;">.</div>
		    	<div class="btn-group">
                  	<button type="button" id="btnImprimirDetalle" class="btn btn-warning btn-md" title="Pulse Click para Imprimir el Detalle de Ingresos! ">
                  		<i class="glyphicon glyphicon-print"></i>
                  	</button>
                </div> -->
		    </div> 
		</div>
	</div>
	
 	<div class="box-body">
 		<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr>
	 					<th id="corregirKardexProducto">Nº</th>
	 					<th>Nro Nota</th> 
	 					<th>Documento</th>
	 					<th>Sucursal</th>
	 					<th>Proveedor</th>
	 					<th>Producto(s)</th>
	 					<th>Fecha</th>
	 					<th>Estado</th>
	 					<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($model as $dato)
						<tr role="row" style="{{ config('app.styleTableRow') }}">
							<td class="col-xs-0">{{ $dato->numero_nota }}</td>
							<td class="col-xs-2">{{ $dato->descripcion }}</td> 
							@foreach($modelTipo_transaccion as $tipo)
					            @if($dato->idfar_adm_tipotransaccion == $tipo->id)
					                <td class="col-xs-2">{{ $tipo->descripcion }}</td>
					            @endif
					        @endforeach
					        @foreach($modelSucursales as $sucursal)
					            @if($dato->idfar_sucursales == $sucursal->id)
					                <td class="col-xs-2">{{ $sucursal->nombre }}</td>
					            @endif
					        @endforeach
					        @foreach($modelProveedores as $proveedor)
					            <td class="col-xs-2">{{ $dato->idfar_proveedores == $proveedor->id? $proveedor->nombre : '' }}</td>
					        @endforeach
							<td>{{ $dato->producto != ''? $dato->producto : ''  }}</td>
							<td class="col-xs-1">{{ date('d-m-Y', strtotime( $dato->fecha )) }}</td> 
							@foreach($modelEstados as $estado)
					            @if($dato->idfar_estado == $estado->id)
					                <td class="col-xs-2">{{ $estado->nombre }}</td>
					            @endif
					        @endforeach
							<td class="col-xs-2">
								<!-- <a href="{{ route('far_cabecera_registro.show', $dato->id) }}" class="btn btn-default btn-sm" title="Ver">
									<i class="glyphicon glyphicon-eye-open"></i>
								</a> -->
								<a href="{{ route('far_cabecera_registro.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Ver">
									<i class="glyphicon glyphicon-pencil"></i>
								</a>
								<!-- <button class="btn btn-danger btn-sm" data-idnota="{{ $dato->id }}" data-minumero="{{ $dato->numero }}" data-toggle="modal" data-target="#delete" title="Eliminar">
									<i class="glyphicon glyphicon-trash"></i>
								</button> -->
							</td>
						</tr>					
					@endforeach
				</tbody>
			</table>
		</div> 
 	</div>
</div>
@stop
 