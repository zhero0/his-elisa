@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVO INGRESO')
@section('htmlheader_title', 'NUEVO INGRESO')

@section('main-content')


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	    <div class="panel panel-primary">
	      <div class="panel-body">

	      		@include($model->rutaview.'formEncabezado')

	        	{!! Form::Open(['route' => 'far_cabecera_registro.store', 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
				      	<a href="{{ URL::previous() }}" class="btn btn-primary">
				        	<span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
				      	</a>

				      	@if($model->scenario != 'view')
				        	<button id="btnGuardarNota" type="button" class="btn btn-success">
				          		<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
				        	</button>
				      	@endif
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<!-- <div class="alert alert-warning alert-dismissible" id="divMensajeError" role="alert">
						  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  	<span aria-hidden="true">&times;</span>
						  </button>
						  <strong>REVISE! </strong> Aqui el texto
						</div> -->

						<div id="divMensajeAdvertencia" class="alert alert-danger alert-dismissible" style="display: none;">
					        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					        <!-- <label id="lblMensajeError"> Complete los datos requeridos! </label> -->
					        <strong>REVISE! </strong> <label id="lblMensajeError"> Aqui el texto </label>
				      	</div>
					</div>
				  	<!-- <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				        <ul class="list-group">
				          <li class="list-group-item">
				            <div class="row">
				              <div class="col-xs-7">
				                Monto Pagado(Bs)
				              </div>
				              <div class="col-xs-5">
				                <span class="badge">
				                  <input type="number" class="form-control input-sm" id="txtMontoPagado" value="0.00" style="color: black; font-weight: bold;">
				                </span>
				              </div>
				            </div>
				          </li>
				          <li class="list-group-item">
				            Monto Pendiente(Bs)
				            <span class="badge" id="spanMontoPendiente" style="font-size: 18px;">0.00</span>
				          </li>
				        </ul>
				    </div> -->
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@include($model->rutaview.'modal')