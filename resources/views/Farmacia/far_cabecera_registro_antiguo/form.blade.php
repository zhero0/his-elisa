<div class="form-group">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="arrayDatos" name="arrayDatos" value="{{ $model->productoNota }}">
    <input type="hidden" id="arrayProveedor" name="arrayProveedor" value="{{ $model->proveedor }}">
    <input type="hidden" id="txtHiddenIdProveedor" name="txtHiddenIdProveedor">
    <input type="hidden" id="txtHiddenIdSucursal" name="txtHiddenIdSucursal">
    <input type="hidden" id="txtHiddenIdIngreso" name="txtHiddenIdIngreso">
    <input type="hidden" id="txtHiddenIdClasificacion" name="txtHiddenIdClasificacion">
    <!-- <input type="hidden" id="idalmacenes" value="{{ $model->idalmacenes }}"> -->

    <div class="table-responsive" style="max-height: 212px; overflow-y: auto; border: solid 1px #3c8dbc;">
      <table id="table_DocumentoProducto" class="table table-bordered table-hover table-condensed">
          <thead style="{{ config('app.styleTableHead') }}">
              <tr>
                <th>Codigo - Producto</th>
                <th>Lote</th>
                <th>Fecha Vto</th>
                <th>Cantidad</th>                
                <th>Sub Total</th>
                <th></th>
             </tr>
          </thead>
          <tbody class="table-success" id="tbody_DocumentoProducto" style="{{ config('app.styleTableRow') }}">
            <tr id="idListaVacia">
              <td>Lista Vacia...</td>
            </tr>
          </tbody>
      </table>
    </div>

    <div class="row">
      <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2" style="text-align: left; border-bottom: solid 1px #3c8dbc;">
        <strong>CANTIDAD ITEM: </strong>
        <span id='idTotalItem' class="badge" style="font-size: 15px; background: rgb(236, 240, 245); color: black;">
          0
        </span>
      </div>
      <div class="col-xs-0 col-sm-4 col-md-6 col-lg-7"></div>
      <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2" style="border-bottom: solid 1px #3c8dbc; text-align: right;">
        <strong>TOTAL: </strong>
        <input type="hidden" id="txtHiddenTotal" name="txtHiddenTotal">
        <span class="badge" style="font-size: 15px; background: rgb(236, 240, 245); color: black;" id='txtTotal'> 0.00
        </span>
        
        <input type="hidden" class="form-control" id="txtMontoPagado_bd" name="txtMontoPagado_bd">
        <input type="hidden" class="form-control" id="txtDescripcion_bd" name="txtDescripcion_bd">
      </div>
    </div>
</div>

