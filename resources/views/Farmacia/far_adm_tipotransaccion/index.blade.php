@extends('adminlte::page')
@section('title', 'LISTA DE TRANSACCIONES')
@section('content_header')
    <h1>LISTADO DE TIPO DE TRANSACCIONES</h1>
@stop   

@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-primary" href="{{ route('far_adm_tipotransaccion.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div>
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr> 
 					<th>ID</th> 
 					<th>Codificacion</th>
 					<th>Descripcion</th>	
 					<th>Cant. de articulos</th> 					
 					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($model as $dato)					
					<tr role="row">
						<td class="col-xs-1">{{ $dato->id }}</td> 
						<td class="col-xs-3">{{ $dato->codigo }}</td>
						<td class="col-xs-4">{{ $dato->descripcion }}</td> 
						<td class="col-xs-4">{{ $dato->cantidad_medicamentos }}</td> 
						<td class="col-xs-1"> 
							<a href="{{ route('far_adm_tipotransaccion.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
							<a href="{{ route('far_adm_tipotransaccion.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="CAMBIAR DE ESTADO">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>					
				@endforeach
			</tbody>
		</table>
	</div>  
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection  