<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" id="almacen" name="almacen" value="{{ $model->almacen }}"> 
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-12">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>     

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <label for="codigo" class="col-sm-6 col-form-label">Codificación:</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="codigo" name="codigo" value="{{ $model->codigo }}" placeholder="">
        </div>        
      </div>  
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <label for="descripcion" class="col-sm-6 col-form-label">Descripción:</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ $model->descripcion }}" placeholder="">
        </div>       
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        <label for="descripcion" class="col-sm-6 col-form-label">Cantidad de articulos:</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="cantidad_medicamentos" name="cantidad_medicamentos" value="{{ $model->cantidad_medicamentos }}" placeholder="">
        </div>       
      </div>
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif