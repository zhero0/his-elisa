@extends('layouts.admin')
@section('contentheader_title', 'APERTURA Y CIERRE DE CAJA')
@section('htmlheader_title', 'APERTURA Y CIERRE DE CAJA')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
				<!-- <button type="button" class="btn btn-default btn-round btn-sm" 
			          title="Click para Buscar"
			          onclick="abrirVentana()">
			        <i class="fa fa-male" aria-hidden="true"></i>
			          REGISTRAR (*)
			    </button> -->
		        <a type="button" class="btn btn-primary"  onclick="abrirVentana()">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr> 
						<th>Codigo</th>
	 					<th>Sucursal</th>
	 					<th>Ventanilla</th>
	 					<th>Apertura</th> 
	 					<th>Cierre</th>
	 					<th></th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}"> 
						<td class="col-xs-3">{{ $dato->res_codigo_almacen }}</td>
						<td class="col-xs-3">{{ $dato->res_sucursal }}</td>
						<td class="col-xs-1">{{ $dato->res_ventanilla }}</td>
						<td class="col-xs-6">{{ $dato->res_fecha_apertura.' '.$dato->res_hora_apertura }}</td>  
						<td class="col-xs-6">{{ $dato->res_fecha_cierre.' '.$dato->res_hora_cierre }}</td>  
						<td class="col-xs-1">{{ $dato->res_estado }}</td> 
						<td>
							<a href="{{ route('far_caja.show', $dato->res_id) }}" type="button" class="btn btn-default btn-sm" title="REMOVER PERSONAL {{ $dato->id }}">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@stop

<!-- VENTANA MODAL PARA MOSTRAR EXÁMENES -->
<div class="modal fade" id="vModalApertura">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">Apertura de Caja</h4>
		    </div>
		    <div class="modal-body">  
	              	<label for="muestra">Seleccionar Ventanilla</label>
	              	<select name="idfar_sucursales" class="form-control" onchange="this.form.submit()">       
		              	@foreach($modelAlmacenes as $dato) 
		                	<option value="{{$dato->id}}"> {{$dato->res_nombre_ventanilla}} </option> 
		              	@endforeach
		            </select>
	       
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-primary" id="btnSeleccionar">
			        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			       	Seleccionar
		       	</button>
		       	<button type="button" class="btn btn-danger" data-="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>