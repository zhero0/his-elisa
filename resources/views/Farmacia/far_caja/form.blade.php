<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">                 
    <input type="hidden" id="idfar_ventanillas" name="idfar_ventanillas" value="{{ $model->idfar_ventanillas }}">
    <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="row">
      <ul class="timeline">
          
        <!-- timeline DATOS -->
        <li>
          <i class="fa fa-file-text-o bg-yellow"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <h3 class="box-title">DATOS</h3>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row"> 
                 
                    <div class="row">
                      <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <label>Farmaceutico</label>
                        <div class="input-group">
                          <input type="text" id="txtMedico" name="txtMedico" value="{{$model->medico}}" class="form-control" disabled="">
                          <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="btnModal_Medico" title="Click para Buscar">
                              <span class="fa fa-search" aria-hidden="true"></span>.
                            </button>
                          </span>
                        </div>
                      </div>

                      <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                       
                        <label>Ventanilla</label>
                        <div class="input-group">
                          <input type="text" id="txtEspecialidad" name="txtEspecialidad" class="form-control" value="{{$model->especialidad}}" disabled="">
                          <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="btnModal_Especialidad" title="Click para Buscar">
                              <span class="fa fa-search" aria-hidden="true"></span>.
                            </button>
                          </span>
                        </div>
                      </div>
                    </div>
                       
                    </div> 

                  </div>
                </div>
              </div>
              
            </div>
            <div class="timeline-footer">
            </div>
          </div>
        </li>
        <!-- END timeline DATOS -->

        
        <li>
          <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
        </li>
      </ul>  
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Asignar personal
  </button>
@endif