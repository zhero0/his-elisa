@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR PERSONAL')
@section('htmlheader_title', 'EDITAR PERSONAL')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['personal_lab.update', $model->id], 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop
@include($model->rutaview.'modal')