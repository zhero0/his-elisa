<div class="table-responsive table-bordered table-hover">
	<table id="tablaContenedorDatos" class="table table-fixed table-condensed">
		<thead style="{{ config('app.styleTableHead') }}">
		    <tr>
		    	<th>N°</th>
		    	<th>Codigo Almacenes</th>
	      		<th>Codigo Farmacia</th>
	            <th>Medicamento</th>
	            <th>Unidad</th>
	            <th>Concentracion</th>
	            <th>Envase</th> 
		      	<th>
	              	<div class="form-check">
		                <label class="form-check-label">
		                	<input class="form-check-input" type="checkbox" onclick="checkSeleccionTodoArticulos( $(this).is(':checked'), 'Far' )">
		                    <span class="form-check-sign">
	                      		<span class="check"></span>
		                    </span>
		                </label>
	              	</div>
	          	</th>
		   </tr>
		</thead>


		<tbody id="tbodyArticulosFar" style="{{ config('app.styleTableRow') }}">
			


			@if(isset($modelo))
	
		    	@for($i = 0; $i < count($modelo); $i++) 
			       	<tr id="row-{{ $modelo[$i]->res_id }}" 
						ondblclick="dbclick_check_unCheck('Far', {{ $modelo[$i]->res_id }} )">
						<td>{{$i+1}}</td>
						<td class="text-left" style="font-size: 12px;">{{ $modelo[$i]->res_codificacion_nacional }}</td>
			          	<td class="text-left" style="font-size: 12px;">{{ $modelo[$i]->res_codificacion }}</td>
			          	<td class="text-left" style="font-size: 12px;">{{ $modelo[$i]->res_descripcion }}</td>
			          	<td class="text-left" style="font-size: 12px;">{{ $modelo[$i]->res_unidad }}</td>
			          	<td class="text-left" style="font-size: 12px;">{{ $modelo[$i]->res_concentracion }}</td>
			          	<td class="text-left" style="font-size: 12px;">{{ $modelo[$i]->res_tipo_envase }}</td>
			          	<td> 
			          		<div class="form-check">
				                <label class="form-check-label">
				                    <input type="checkbox" class="form-check-input" 
				                    	id="check_{{ $modelo[$i]->res_id }}" 
			                    		title="Click para Seleccionar"
			   							onclick="articulosSeleccionadosFar()">
				                    <span class="form-check-sign">
				                  		<span class="check"></span>
				                    </span>
				                </label>
				            </div>
			          	</td>
			       	</tr>
		       	@endfor
	       	@endif
		</tbody>
	</table>
</div>
<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	@if(isset($modelo))
	@if($modelo->paginacion)
<div class="row">
	<div class="col-sm-5">
		<div class="dataTables_info text-left p-5">Mostrando 
				{{$modelo->pagina_inicio}} 
			 a 
				{{$modelo->paginas_ranged}} 
			 de 
				{{$modelo->total}} 
			
		</div>
	</div>
	<div class="col-sm-7">
		<div class="dataTables_paginate paging_simple_numbers text-right" >
			<ul class="pagination">
				@for($i=1;$i<=$modelo->nro_paginas;$i++)
					@if($i==$modelo->pagina_actual)
						<!--<li class="paginate_button active "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">{{$i}}</a></li>-->
						<li class="paginate_button active "><button type="button" class="btn btn-info" onclick="searchPaginacionArticulo($modelo->sucursal,$modelo->clasificacion,{{ csrf_token() }},{{$i}})"> {{$i}} </button></li>
					@else
						<li class="paginate_button "><button type="button" class="btn btn-default" onclick="searchPaginacionArticulo({{$modelo->sucursal}},{{$modelo->clasificacion}},'{{ csrf_token() }}',{{$i}})">{{$i}} </button></li>
					@endif	
				@endfor
			</ul>
		</div>
	</div>
</div>
@endif
@endif

</div>

