@extends('adminlte::layouts.app')
@section('contentheader_title', 'ARTICULOS')
@section('htmlheader_title', 'ARTICULOS')
 
@section('main-content')
<div class="content">
	<div class="col-md-12">
		<div class="card">
	        <div class="card-body"> 
				{!! Form::model($model, ['route' => ['inv_articulo.update'], 'files'=>true, 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'listado')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop