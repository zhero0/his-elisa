<div class="form-group"> 
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" id="datosTablaArt" name="datosTablaArt">
  
  <div class="row">
    <div class="col-12 col-sm-9 col-md-12 col-lg-12">
      <div class="row">
        <label class="col-sm-1 col-form-label">Clasificación</label>
        <div class="col-sm-3">
          <div class="form-group"> 
            <select id="clasificacion" name="clasificacion" class="form-control"> 
              @foreach($modelClasificacion as $clasificacion)
              <div class="col-sm-6">
                @if($model != null)
                  <option value="{{$clasificacion->id}}"  {{ $model->idfar_adm_clasificacion == $clasificacion->id ? 'selected="selected"' : '' }} >{{$clasificacion->descripcion}}</option>    
                @else
                   <option value="{{$clasificacion->id}}">{{$clasificacion->descripcion}}</option> 
                @endif 
                </div>                     
              @endforeach 
            </select> 
          </div>
        </div>   
        <label class="col-sm-1 col-form-label">Sucursal</label>
        <div class="col-sm-3">
          <div class="form-group"> 
            <select id="sucursal" name="sucursal" class="form-control"> 
              @foreach($modelSucursales as $sucursal)
              <div class="col-sm-6">
                @if($model != null)
                  <option value="{{$sucursal->id}}"  {{ $model->idfar_sucursales == $sucursal->id ? 'selected="selected"' : '' }} >{{$sucursal->nombre}}</option>    
                @else
                   <option value="{{$sucursal->id}}">{{$sucursal->nombre}}</option> 
                @endif 
                </div>                     
              @endforeach 
            </select> 
          </div>
        </div>    
        <button type="button" class="btn btn-default btn-round btn-sm" onclick="buscadorArticulos()">
          <span class="fa fa-search" aria-hidden="true"></span>
          Buscar
        </button>
      </div>
    </div>
  </div>
  <br>

  <div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 _articulos">
      @include('Farmacia.far_articulo_sucursal._articulos') 
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12" style="text-align: right;">
      <button type="button" class="btn btn-default btn-round btn-sm" onclick="_visualizaExamenesSeleccionados('Far')" 
              rel="tooltip" title="Click para visualizar Exámenes Seleccionados">
        Seleccionados ( <span id="spanTotalExamenSeleccionFar" style="font-size: 15px;">0</span> )
      </button>
      <!-- <button type="button" class="btn btn-danger btn-round btn-sm" onclick="_borraExamenesSeleccionados('Far')" 
              rel="tooltip" title="Borrar Exámenes Seleccionados">
        <span class="fa fa-trash-o" aria-hidden="true"></span>
        Borrar Seleccionados
      </button> -->
    </div>
  </div>
</div> 
 
<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" class="btn btn-success btn-md" rel="tooltip" title="Guardar" onclick="guardar()">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> Guardar
  </button>
@endif
