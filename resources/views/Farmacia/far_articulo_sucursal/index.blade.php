@extends('layouts.admin')
@section('contentheader_title', 'INSUMOS - MEDICAMENTOS')
@section('htmlheader_title', 'INSUMOS - MEDICAMENTOS')

<?php
use Illuminate\Support\Facades\Input; 
?> 
@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('far_articulo_sucursal.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div>
	</div>
	
 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-condensed">
						<thead style="{{ config('app.styleTableHead') }}">
							<tr>
					      		<th>Código</th>
								<th>Medicamento/Insumo</th>
								<th>Unidad</th> 
								<th>Salida</th> 					
								<th>Saldo</th> 					
								<th>Stock Minimo</th> 	 												
								<th>Responsable</th> 
						      	<th>
					              	<div class="form-check">
						                <label class="form-check-label">
						                	<input class="form-check-input" type="checkbox" onclick="checkSeleccionTodoArticulos( $(this).is(':checked'), 'Far' )">
						                    <span class="form-check-sign">
					                      		<span class="check"></span>
						                    </span>
						                </label>
					              	</div>
					          	</th>
					          	<th></th>
						    </tr>
						</thead>
						<tbody>
							@foreach($model as $dato)
								<tr>
									<!-- <td class="col-lg-1">
										@if($dato->imagen == '')
											<img src="/uploads/inventario/defecto.jpg" style="width:60px; height:60px;" class="img-rounded"/>
										@else
											<img src="/uploads/inventario/{{$dato->imagen}}" style="width:60px; height:60px;" class="img-rounded"/>
										@endif
									</td> -->
									<td class="text-left">{{ $dato->codificacion }}</td>						
									<td>{{ $dato->descripcion }}</td>
									<td class="text-left">{{ $dato->unidad }}</td> 
						            <td class="text-left">{{ $dato->cantidad_max_salidad }}</td>
						            <td class="text-left">{{ $dato->saldo }}</td> 
						            <td class="text-left">{{ $dato->stock_minimo }}</td>  
						            <td class="text-left">{{ $dato->usuario }}</td>
						            <td class="text-center">
										<div class="form-check">
							                <label class="form-check-label">
							                  	<input class="form-check-input" type="checkbox" id="checkOpcion{{$dato->id}}" onClick="habilitarPerfil_consultorio( $(this).attr('id'), {{$dato->id}} )" {{$dato->habilitarconsultorio == 1? 'checked="checked"' : ''}}>
							                  	<span class="form-check-sign">
							                    	<span class="check"></span>
							                  	</span>
							                </label>
							            </div>
									</td>
				                    <td class="td-actions text-right"> 
										<button class="btn btn-default btn-round btn-sm" 
										onclick="modificarDatos( {{ $dato->id }} )" rel="tooltip" 
										title="Editar datos"
										{{ $dato->json_muestreo == ''? '' : 'disabled'}}>
										<i class="fa fa-eyedropper"></i>
										</button> 
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
		</div>
		
		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->render() !!}
	    </div>
 	</div>
</div>
@stop
 
 