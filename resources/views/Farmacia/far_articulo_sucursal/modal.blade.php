<!-- VENTANA MODAL PARA BUSCAR ARTICULOS -->
<div class="modal fade" id="vModalArticulo">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">Datos Articulo</h4>
		    </div>
		    <div class="modal-body">  
		    <div class="row"> 
	            <div class="form-group">          
	                <label class="bmd-label-floating">Cod. Nal. </label> 
	                <input type="text" class="form-control input-sm" name="codificacion_nacional" id="codificacion_nacional" style="text-transform: uppercase;" value="" required="true">                  
	            </div>        
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	              	<div class="form-group">
	                	<label class="bmd-label-floating">Codificación Interna</label>
	                	{!! Form::text('codificacion', null, ['class' => 'form-control', 'style' => 'text-transform: uppercase;']) !!}
	              	</div> 
	            </div>
		    </div>
		    <div class="row">
		    	<label class="col-sm-1 col-form-label"> </label>
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	              	<div class="form-group">
	                  	<label class="bmd-label-floating">Descripción</label>
	                  	{!! Form::text('descripcion', null, ['class' => 'form-control', 'style' => 'text-transform: uppercase;']) !!}
	              	</div> 
	            </div>
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	              	<div class="form-group">
	                  	<label class="bmd-label-floating">Codigo de barras</label>
	                  	{!! Form::text('codigo_barras', null, ['class' => 'form-control', 'style' => 'text-transform: uppercase;']) !!}
	              	</div> 
	            </div>
		    </div>

		    <div class="row">
		    	<label class="col-sm-1 col-form-label"> </label>
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	              	<div class="form-group">
	                  	<label class="bmd-label-floating">Unidad</label>
	                  	{!! Form::text('unidad', null, ['class' => 'form-control','style' => 'text-transform: uppercase;']) !!} 
	              	</div> 
	            </div>    
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	              	<div class="form-group">
	                	<label class="bmd-label-floating">Concentración</label>
	                	{!! Form::text('concentracion', null, ['class' => 'form-control','id'=>'concentracion', 'style' => 'text-transform: uppercase;']) !!} 
	              	</div> 
	            </div>
		    </div>
		    <div class="row">
		    	<label class="col-sm-1 col-form-label"> </label>
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	            <label class="bmd-label-floating">Tipo de envase</label>
	              	<div class="form-group">
	                  	
	                  	<input type="text" class="form-control input-sm" id="tipo_envase" name="sala" style="text-transform: uppercase;" >
	                  	
	              	</div> 
	            </div>
	            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
	              	<div class="form-group">
	                  	<label class="bmd-label-floating">Cantidad Max. Dispensar</label>
	                  	{!! Form::text('cantidad', null, ['class' => 'form-control','style' => 'text-transform: uppercase;']) !!} 
	              	</div> 
	            </div>
		    </div>

		    	  
	            
	            
	                  
 
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>