<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">    
    <input type="hidden" id="txtJsonExamenes" name="txtJsonExamenes">    
    <input type="hidden" id="idfar_sucursales" name="idfar_sucursales" value="{{ $model->idfar_sucursales }}">
    <input type="hidden" id="txtcontenidoTabla" name="txtcontenidoTabla"> 
    <input type="hidden" id="txtres_movimiento_id" name="txtres_movimiento_id">
     
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div> 
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
      <!-- <div class="alert alert-warning alert-dismissible" id="divMensajeError" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <strong>REVISE! </strong> Aqui el texto
      </div> -->

      <div id="divMensajeAdvertencia" class="alert alert-danger alert-dismissible" style="display: none;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <!-- <label id="lblMensajeError"> Complete los datos requeridos! </label> -->
        <strong>REVISE! </strong> <label id="lblMensajeError"> Aqui el texto </label>
      </div>
    </div>
    </div>
    
    <div class="row">  
      
        <!-- timeline MEDICAMENTOS -->
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
 
          <h4 class="list-group-item-heading text-primary">
            <i class="fa fa-barcode"></i>
            Busquedá
          </h4>
          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-success">
                <div class="box-header with-border">
                  <div class="row"> 
                      <!-- <h3 class="box-title">PACIENTE</h3> -->
                      

                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                      <div class="input-group input-group-sm">
                        <input type="text" id="txtsearchCode" name="txtsearchCode" class="form-control" placeholder="Escriba..." autofocus="autofocus" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModal_Codigo" title="Click para Buscar">
                            <span class="fa fa-search" aria-hidden="true"></span>.
                          </button>
                        </span>
                      </div>
                    </div> 
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">                      
                      <input type="text"  name="txtSucursal" class="form-control" value="{{ $modelSucursales->nombre }}" disabled="">                        
                    </div>
                  </div>
                </div> 
              </div>
            </div> 
          </div> 
        <!-- END timeline PACIENTE --> 
        <!-- timeline PACIENTE -->  
            <h4 class="list-group-item-heading text-primary">
              <i class="fa fa-user"></i>
                LISTA DE PEDIDOS
            </h4>
            <div class="timeline-item">
              <div class="timeline-body">
                <div class="box box-primary"> 
                  <div class="box-body"> 
                    <div class="box-body">
                      <div class="table-responsive" style="max-height: 200px; overflow-y: auto; border: 1px solid #374850;">
                        <table id="tablaContenedorMedicamento" class="table table-bordered table-hover table-condensed" style="overflow-y:scroll">
                            <thead style="{{ config('app.styleTableHead') }}">
                                <tr>
                                  <th class="col-xs-1">Nota</th>
                                  <th class="col-xs-4">DESCRIPCIÓN</th>                                  
                                  <th class="col-xs-1">OPERACION</th>
                                  <th class="col-xs-2">FECHA</th>
                                  <th class="col-xs-1">ESTADO</th>
                                  <th></th>
                               </tr>
                            </thead>
                            <tbody>
                              @foreach($model as $dato)
                                <tr role="row" style="{{ config('app.styleTableRow') }}">
                                  <td >{{ $dato->res_numero_nota }}</td>                                  
                                  <td class="col-xs-4">{{ $dato->res_descripcion }}</td> 
                                  <td class="col-xs-2">{{ $dato->res_operacion }}</td> 
                                  <td class="col-xs-2">{{ $dato->res_fecha }}</td> 
                                  <td class="col-xs-2">{{ $dato->res_estado == 1? 'AUTORIZADO':'ANULADO' }}</td> 
                                  <td class="col-xs-1">
                                    <button type="button" id="{{$dato->res_movimiento_id}}" class="btn btn-default btn-sm" title="Visualizar pedido">
                                      <i class="fa fa-list"></i>
                                    </button>
                                  </td>
                                </tr>         
                              @endforeach
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div> 
            </div> 
        </div>
        <!-- END timeline MEDICAMENTOS --> 
        <!-- timeline MEDICAMENTOS DISPENSADOS --> 
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
          <h4 class="list-group-item-heading text-primary">
            <i class="fa fa-user"></i>
              LISTA DE MEDICAMENTOS
          </h4>

          <div class="timeline-item">
            
            <div class="timeline-body">
              <div class="box box-danger"> 
                <div class="box-body">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                      
                    <input type="text" id="txtPedido"  name="txtPedido" class="form-control" disabled="">                        
                  </div>
                  <br>
                  <br> 
                  <div class="table-responsive" style="max-height: 200px; overflow-y: auto; border: 1px solid #374850;">
                    <table id="tablaContenedorMedicamentos" class="table table-bordered table-hover table-condensed" style="overflow-y:scroll">
                        <thead style="{{ config('app.styleTableHead') }}">
                            <tr> 
                              <th class="col-xs-2">CODIGO</th>
                              <th class="col-xs-6">DESCRIPCION</th>   
                              <th class="col-xs-1">UNIDAD</th>
                              <th class="col-xs-2">CANTIDAD</th>
                              <th class="col-xs-2">SALDO</th>                              
                           </tr>
                        </thead>
                        <tbody class="table-success" id="tbodyMedicamentoLista" style="{{ config('app.styleTableRow') }}">
                          <tr>
                            <td>Vacío...</td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div> 
        </div> 
    </div> 
</div>
 
@if($model->scenario != 'view')  
  <a href="{{ URL::previous() }}" class="btn btn-primary">
    <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
  </a> 
  <button type="button" class="btn btn-success btn-md" rel="tooltip" title="Guardar" onclick="guardar()">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> Ingresar articulos
  </button>
@endif
