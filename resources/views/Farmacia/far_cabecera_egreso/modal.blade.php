<!-- MODAL PARA BUSCAR PRODUCTOS -->
<div class="modal" id="modalBuscadorProducto">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> Buscar Producto </h4>
        </div>
        <div class="modal-body">
          <!-- <form action="#" id="form_BajaMedica" method="post" autocomplete="off"> -->
            <div class="input-group">
              <input type="text" id="txtBuscarProducto" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
              <span class="input-group-btn">
                <button class="btn btn-warning" type="button" id="btnBuscarProducto">
                  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                  Buscar
                </button>
              </span>
            </div>

            <div class="table-responsive" style="max-height: 380px; overflow-y: auto; border: solid 1px #3c8dbc;">
              <table id="tablaBuscadorProducto" class="table table-bordered table-hover table-condensed">
                <thead style="{{ config('app.styleTableHead') }}">
                  <tr>
                    <th>Código</th>
                    <th>Nombre</th>
                    <!-- <th>Cantidad</th> -->
                    <!-- <th style="text-align: right;">Costo</th> -->
                    <th></th>
                  </tr>
                </thead>
                <tbody class="table-success" id="tbodyProducto" style="{{ config('app.styleTableRow') }}">
                </tbody>
              </table>
            </div>
          <!-- </form> -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div>



<!-- MODAL PARA BUSCAR PROVEEDOR -->
<div class="modal" id="modalBuscaProveedor">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> Buscar Proveedor </h4>
        </div>
        <div class="modal-body">
          <!-- <form action="#" id="form_BajaMedica" method="post" autocomplete="off"> -->
            <div class="input-group">
              <input type="text" id="txtBuscarProveedor" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
              <span class="input-group-btn">
                <button class="btn btn-warning" type="button" id="btnBuscarProveedor">
                  <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                  Buscar
                </button>
              </span>
            </div>

            <div class="table-responsive" style="max-height: 380px; overflow-y: auto; border: solid 1px #3c8dbc;">
              <table id="table_Proveedor" class="table table-bordered table-hover table-condensed">
                <thead style="{{ config('app.styleTableHead') }}">
                  <tr>
                    <th>Nit</th>
                    <th>Nombres y Apellidos</th>
                    <th>Departamento</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody class="table-success" id="tbody_Proveedor" style="{{ config('app.styleTableRow') }}">
                </tbody>
              </table>
            </div>
          <!-- </form> -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div>
