@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR ESTABLECIMIENTO')
@section('htmlheader_title', 'EDITAR ESTABLECIMIENTO')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['far_adm_establecimiento.update', $model->id], 'files'=>true, 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop