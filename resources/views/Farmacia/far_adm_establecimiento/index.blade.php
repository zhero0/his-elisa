@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE ESTABLECIMIENTO')
@section('htmlheader_title', 'LISTA DE ESTABLECIMIENTO')

@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-12 col-sm-2 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('far_adm_establecimiento.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div>
	</div>

 	<div class="box-body">
		<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr>
						<th>Nombre</th> 
						<th>Descripción</th>												
						<th></th>
					</tr>
				</thead> 			 
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}"> 
						<td class="col-lg-3">{{ $dato->nombre }}</td>						
						<td>{{ $dato->descripcion }}</td>						
						<td class="col-lg-1"> 
							<a href="{{ route('far_adm_establecimiento.edit', $dato->id) }}" class="btn btn-default btn-sm" title="EDITAR">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
							<a href="{{ route('far_adm_establecimiento.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VIGENCIA DE ALMACEN">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>
		
		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
      		{!! $model->render() !!}
    	</div>

	</div>
</div>
@stop