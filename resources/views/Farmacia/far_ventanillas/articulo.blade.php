@extends('layouts.app')
@section('contentheader_title', 'ARTICULOS VARIABLES')
@section('htmlheader_title', 'ARTICULOS VENTANILLAS')
 
@section('main-content')
<div class="content">
  <div class="col-md-6">
    <div class="card">
          <div class="card-body">
        {!! Form::model($model, ['route' => ['ventanilla.update', $model->id], 'files'=>true, 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
          @include($model->rutaview.'formArticulo')
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop