<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="iddato" value="{{ $model->iddato }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-12">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div> 

    <div class="col-md-12">
      <div class="card ">
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title">Datos</h4>
          </div>
        </div>
        <div class="card-body ">

          <div class="row">
            <label class="col-sm-2 col-form-label">Seleccionar</label>
            <div class="col-sm-10">
              <div class="form-group"> 
                  <select id="dato" name="dato" class="form-control">
                    <!-- <option value="0"> :: Seleccionar</option>   -->
                    @foreach($modelSucursales as $dato)
                    <div class="col-sm-10">
                      @if($model != null)
                        <option value="{{$dato->id}}"  {{ $model->idfar_sucursales == $dato->id ? 'selected="selected"' : '' }} >{{  $dato->nombre }}</option>    
                      @else
                         <option value="{{$dato->id}}">{{  $dato->nombre }}</option> 
                      @endif 
                      </div>                     
                    @endforeach 
                  </select> 
              </div>
            </div>
          </div>
          <br>

          <div class="row"> 
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
              <div class="form-group">
                  <label class="bmd-label-floating">Nombre</label>
                  {!! Form::text('nombre', null, ['class' => 'form-control', 'style' => 'text-transform: uppercase;']) !!}
              </div>     
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            </div>
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
              <div class="form-group">
                <label class="bmd-label-floating">Descripción</label>
                {!! Form::text('descripcion', null, ['class' => 'form-control', 'style' => 'text-transform: uppercase;']) !!}
              </div>      
            </div>
          </div> 

        </div>
      </div>
    </div>
 
 
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="fa fa-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> Guardar
  </button>
@endif