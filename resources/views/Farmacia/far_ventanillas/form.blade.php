<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-12">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="row">
      <label class="col-sm-2 col-form-label">Seleccionar</label>
      <div class="col-sm-10">
        <div class="form-group"> 
            <select id="sucursal" name="sucursal" class="form-control">
              <!-- <option value="0"> :: Seleccionar</option>   -->
              @foreach($modelSucursales as $sucursal)
              <div class="col-sm-10">
                @if($model != null)
                  <option value="{{$sucursal->id}}"  {{ $model->idfar_sucursales == $sucursal->id ? 'selected="selected"' : '' }} >{{  $sucursal->nombre }}</option>    
                @else
                   <option value="{{$sucursal->id}}">{{  $sucursal->nombre }}</option> 
                @endif 
                </div>                     
              @endforeach 
            </select> 
        </div>
      </div>
    </div>

    <div class="row"> 
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('nombre', 'Nombre ventanilla:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('descripcion', 'Descripción:', ['class' => 'label-control']) !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif