@extends('adminlte::layouts.app')
@section('contentheader_title', 'REGISTRAR VENTANILLA')
@section('htmlheader_title', 'REGISTRAR VENTANILLA')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body">      			
      			{!! Form::Open(['route' => 'far_ventanillas.store', 'files'=>true, 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}			
			</div>
		</div>
	</div>
</div>
@stop