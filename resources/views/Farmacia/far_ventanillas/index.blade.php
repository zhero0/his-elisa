@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE VENTANILLAS')
@section('htmlheader_title', 'LISTA DE VENTANILLAS')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="content">
	<div class="col-md-12">
	  	<div class="card">
	    	<div class="card-body">
	    		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
		 		<!-- "BUSCADOR MULTIPLE" -->
		 		<div class="row">
			 		<form id="formBuscador" class="navbar-form navbar-right" action="/far_ventanillas" autocomplete="off">			 			
			 		</form>
		 		</div>
		 		<!-- FIN "BUSCADOR MULTIPLE" --> 

	    		<div class="row">
					<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
						<a type="button" class="btn btn-success btn-sm" rel="tooltip" href="{{ route('far_ventanillas.create') }}" title="Nuevo Registro">
				            <i class="fa fa-plus" aria-hidden="true"></i> Nuevo
				        </a>
				    </div>
				</div>
			 	<div class="table-responsive table-bordered table-hover" style="{{ config('app.styleIndex') }}">
					<table class="table table-condensed">
						<thead style="{{ config('app.styleTableHead') }}">
							<tr> 
							<th>Nombre</th> 
							<th>Descripción</th>
							<th>Establecimiento</th>																			
							<th></th>
							</tr>
							<tr id="indexSearch" style="background: #ffffff;">  
			 					<th> 
					                <input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;">
			 					</th>
			 					<th>
			 						<input type="text" id="txtSearch_descripcion" class="form-control input-sm" style="text-transform: uppercase;">
			 					</th> 
			 					<th>
			 						<select id="selectSearch_sucursal" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
						            	<option value="0" selected> :: Seleccionar</option>
						                @foreach($modelSucursal as $dato)
						                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
						                @endforeach
						            </select>
			 					</th>
			 					<th></th>
							</tr>
						</thead>
						<tbody>
							@foreach($model as $dato)
								<tr role="row" style="{{ config('app.styleTableRow') }}">  						
									<td class="text-left">{{ $dato->nombre }}</td>						
									<td class="text-left">{{ $dato->descripcion }}</td>
									<td class="text-left">
									@foreach($modelSucursal as $clase)
										@if($clase->id == $dato->idfar_sucursales)
						            		{{ $clase->nombre }}
						            	@endif
						            @endforeach	
						            </td>	
						            <td> 
										<a href="{{ route('far_ventanillas.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
											<i class="glyphicon glyphicon-pencil"></i>
										</a>
									</td>		 
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>

				<div class="text-center" style="{{ config('app.stylePaginacion') }}">
			      {!! $model->appends(Input::except('page'))->render() !!}
			    </div>
			</div>
	  	</div>
  	</div>
</div> 
@stop 