<div class="form-group">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" name="iddato" value="{{ $model->iddato }}">
  
  <div class="row">
    @if(count($errors) > 0)
      <div class="col-xs-12">
        <div class="panel panel-danger">
          <div class="panel-heading">VALIDACIONES</div>
            <div class="panel-body">
              <ul class="list-group">
            @foreach($errors->all() as $error)
              <h4 style="color: #ad0d0c;">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                {{ $error }}
              </h4>
            @endforeach
            </ul>
          </div>
        </div>
      </div>
    @endif
  </div> 

  <div class="col-md-12">
    <div class="card ">
      <div class="card-header card-header-rose card-header-text">
        <div class="card-text">
          <h4 class="card-title">Datos</h4>
        </div>
      </div>
      <div class="card-body ">

        <div class="row">
          <label class="col-sm-2 col-form-label">Seleccionar</label>
          <div class="col-sm-10">
            <div class="form-group"> 
              <!-- aqui comienza la tabla -->
              <div class="col-md-12 divContent">
                <div class="table-responsive table-bordered table-hover" style="max-height: 420px; overflow-y: auto; border: solid 1px #3c8dbc;">
                  <table class="table">
                    <thead class=" text-primary">
                      <tr>
                        <th>Codigo</th>
                        <th>Descripcion</th>
                        <th>Concentracion</th>
                        <th>Unidad</th>
                        <th></th>
                      </tr>  
                    </thead>
                    <tbody id="tbody_Medico"> 
                      @foreach($modelArticulos as $dato)
                        <tr ondblclick="seleccionaArticulo( {{ $dato->id }} )" style="cursor: pointer;" class="default">
                          <td style="display: none;" id="dataRow{{ $dato->id }}">{{ $dato->id }}</td>
                          <td class="text-left">{{ $dato->codificacion }}</td>
                          <td class="text-left">{{ $dato->descripcion }}</td>
                          <td class="text-left">{{ $dato->concentracion }}</td>
                          <td class="text-left">{{ $dato->unidad }}</td>
                          <td class="td-actions text-right">
                            <span class="fa fa-hand-o-left" aria-hidden="true" onclick="seleccionaArticulo( {{ $dato->id }} )" style="cursor: pointer;" title="Click para Seleccionar"></span>
                          </td>
                        </tr>
                      @endforeach 
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- aqui termina la tabla --> 
            </div>
          </div>
        </div> 
      </div>
    </div>
  </div>
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="fa fa-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> Guardar
  </button>
@endif 