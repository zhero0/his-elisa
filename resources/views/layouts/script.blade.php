    <script type="text/javascript" src="{!! asset('js/Administracion/usuario.js?v='.config('app.versionJs')) !!}"></script>
<!-- JS MODULOS -->
@if(isset($model->accion))
  @if(isset($model->rutaview))    
    <?php
      $modulo = explode('.', $model->rutaview)[0]; 
    ?> 
    <script type="text/javascript" src="{{ asset('js/'.$modulo.'/'.$model->accion.'.js?v='.config('app.versionJs')) }}"></script>
  @endif
@endif
<!-- FIN JS MODULOS -->

<!-- EDITOR DE TEXTO -->
<script src="{{ asset('vendor/ckeditor5/ckeditor.js') }}"></script>
<!-- EDITOR DE GRAFICOS -->
<!-- <script src="{{ asset('vendor/Highcharts/code/highcharts.js') }}"></script>
<script src="{{ asset('vendor/Highcharts/code/modules/exporting.js') }}"></script>
<script src="{{ asset('vendor/Highcharts/code/modules/export-data.js') }}"></script>
<script src="{{ asset('vendor/Highcharts/code/modules/accessibility.js') }}"></script> -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>


<script>
  $(document).ready(function() {
    // Initialise the wizard
    demo.initMaterialWizard();
    setTimeout(function() {
      $('.card.card-wizard').addClass('active');
    }, 600);
    
    
    //  Esto funciona para "FECHA Y HORA"
    md.initFormExtendedDatetimepickers();
    if ($('.slider').length != 0) {
      md.initSliders();
    }
  });
</script>

<script>
  ClassicEditor
  .create(document.querySelector('#summary-ckeditor'))
  .then(editor =>{
      console.log(editor);
    })
  .catch(error=>{
      console.error(error);
    }
    ); 
</script>