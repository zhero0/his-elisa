<div class="row">
  <div class="input-group input-group-xs">
      <span class="input-group-btn">
        <button class="btn btn-default" id="btnHora" type="button" title="Click para visualizar hora! ">
          <i class="glyphicon glyphicon-time"></i>.
        </button>
      </span>
      <!-- <input type="text" class="form-control" value="00:00 AM" disabled="" id="txtHora_Minuto_AmPM" name="txtHora_Minuto_AmPM"> -->
      <input type="text" class="form-control" value="{{ $model->hora_actual }}" disabled="" id="txtHora_Minuto_AmPM" name="txtHora_Minuto_AmPM">
  </div>
</div>

<div class="row" id="divHora">
    <table class="table table-bordered table-hover table-condensed" id="tableHora">
      <tbody>
        <tr>
          <td class="col-xs-4">
            <div class="input-group input-group-sm">
              <input type="text" class="form-control input-sm" value="00" id="txtHora" style="font-weight: bold;">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="btnHora_arriba">
                  <i class="glyphicon glyphicon-chevron-up"></i>
                </button>
              </span>

              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="btnHora_abajo">
                  <i class="glyphicon glyphicon-chevron-down"></i>
                </button>
              </span>
            </div>
          </td>
          <td class="col-xs-0" style="font-weight: bold; text-align: center;">:</td>
          <td class="col-xs-4">
            <div class="input-group input-group-sm">
              <input type="text" class="form-control input-sm" value="00" id="txtMinuto" style="font-weight: bold;">
              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="btnMinuto_arriba">
                  <i class="glyphicon glyphicon-chevron-up"></i>
                </button>
              </span>

              <span class="input-group-btn">
                <button class="btn btn-default" type="button" id="btnMinuto_abajo">
                  <i class="glyphicon glyphicon-chevron-down"></i>
                </button>
              </span>
            </div>
          </td>
          <td class="col-xs-3">
            <select class="form-control input-sm" id="selectAm_Pm">
              <option value="AM">AM</option>
              <option value="PM">PM</option>
            </select>
          </td>
        </tr>
      </tbody>
    </table>
</div>