<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    @if(count($errors) >0 )
      @foreach($errors->all() as $error)
        <div class="alert alert-danger">{{$error}}</div>
      @endforeach
    @endif 
    <div class="row"> 
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group row">
          <label for="codigo" class="col-sm-6 col-form-label">Codigo Sucursal</label>
          <div class="col-sm-12">
            <input type="number" class="form-control" id="codigo" name="codigo" value="{{ $model->codigo }}" placeholder="nombre del establecimiento de salud">
          </div>
        </div>
      </div> 
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group row">
          <label for="nombre" class="col-sm-6 col-form-label">Nombre establecimiento</label>
          <div class="col-sm-12">
            <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $model->nombre }}" placeholder="nombre del establecimiento de salud">
          </div>
        </div>
      </div> 
    </div> 
    <div class="row"> 
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group row">
          <label for="direccion" class="col-sm-6 col-form-label">Dirección establecimiento</label>
          <div class="col-sm-12">
            <input type="text" class="form-control" id="direccion" name="direccion" value="{{ $model->direccion }}" placeholder="nombre del establecimiento de salud">
          </div>
        </div>
      </div> 
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="form-group row">
          <label for="descripcion" class="col-sm-6 col-form-label">Descripción</label>
          <div class="col-sm-12">
            <input type="text" class="form-control" id="descripcion" value="{{ $model->descripcion }}" name="descripcion" placeholder="nombre del establecimiento de salud">
          </div>
        </div>
      </div> 
    </div>
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif
