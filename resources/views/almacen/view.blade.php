@extends('adminlte::page') 
@section('title', 'VISUALIZAR REGISTRO')  
@section('content_header')
    <h1>VISUALIZAR REGISTRO DE ESTABLECIMIENTO DE SALUD</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body"> 
				<form method="PUT" action="{{ route('almacen.show', $model->id) }}" autocomplete="off" id="form">
      				{{ csrf_field() }}
      				@include('almacen.form')
      			</form>
			</div>
		</div>
	</div>
</div>
@stop