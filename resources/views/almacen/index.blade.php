@extends('adminlte::page')
@section('title', 'ESTABLECIMIENTOS MEDICOS')
@section('content_header')
    <h1>LISTA DE ESTABLECIMIENTOS MEDICOS</h1>
@stop  

@section('content')
<div class="card"> 
	<div class="card-header"> 
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('almacen.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div> 
		</div> 	
 	</div>
		<div class="card-body"> 
			<table id="sucursales" class="table table-striped">
				<thead>
					<tr> 
		 				<th>N° Sucursal</th>
	 					<th>Nombre</th>
	 					<th>Dirección</th>
	 					<th>Descripción</th>
	 					<th>Usuario</th>
	 					<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($model as $dato)					
						<tr role="row">
							<td class="col-xs-1">{{ $dato->codigo }}</td>
							<td class="col-xs-2">{{ $dato->nombre }}</td>
							<td class="col-xs-3">{{ $dato->direccion }}</td>
							<td class="col-xs-4">{{ $dato->descripcion }}</td>
							<td>{{ $dato->usuario }}</td> 
							<td>
								<button class="btn btn-default btn-xs">
									<a href="{{ route('almacen.edit', $dato->id) }}"> 
										<i class="fa fa-fw fa-pencil-square-o" title="Editar"></i> 
									</a>
								</button>
								<button class="btn btn-default btn-xs">
									<a href="{{ route('almacen.show', $dato->id) }}"> 
										<i class="glyphicon glyphicon-hand-left" title="VER"></i>
									</a>
								</button>						
							</td>
						</tr>					
					@endforeach
				</tbody>
			</table> 
	    </div> 
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#sucursales').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection