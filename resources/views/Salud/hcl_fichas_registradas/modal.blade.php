<div class="modal fade" id="vModalBuscador_poblacion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Datos Personal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>           
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input type="text" id="txtFindPaterno" class="form-control" placeholder="PATERNO" style="text-transform: uppercase;">
          <input type="text" id="txtFindMaterno" class="form-control" placeholder="MATERNO" style="text-transform: uppercase;">
          <input type="text" id="txtFindNombres" class="form-control" placeholder="NOMBRES" style="text-transform: uppercase;">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarPaciente">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>
        </div> 
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatosPaciente" class="table table-bordered table-hover table-condensed">
            <thead style="{{ config('app.styleTableHead') }}">
              <tr>
                <th>Nro Historial</th>
                <th>Documento</th>
                <th>Apellidos</th> 
                <th>Nombres</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="table-success" id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
              <tr id="idListaVaciaPaciente">
                <td>Lista Vacía...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div> 

<div class="modal fade" id="vImprimeSolicitud">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Imprimir</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>           
      </div>
      <div class="modal-body">
        <div id="imprimeAtencion" style="height: 370px;"></div>
        
        <div class="progress" style="display: none;">
          <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div>  

<div class="modal fade" id="vModalRegistrar">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Datos Personal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>           
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input type="text" id="txtFindPaterno" class="form-control" placeholder="PATERNO" style="text-transform: uppercase;">
          <input type="text" id="txtFindMaterno" class="form-control" placeholder="MATERNO" style="text-transform: uppercase;">
          <input type="text" id="txtFindNombres" class="form-control" placeholder="NOMBRES" style="text-transform: uppercase;">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarPaciente">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div> 