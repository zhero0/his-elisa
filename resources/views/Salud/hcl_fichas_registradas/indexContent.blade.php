@foreach($model as $dato)
    <tr role="row" style="{{ config('app.styleTableRow') }}"> 
        <td class="text-left">{{ $dato->sigla }}</td>                      
        <td class="text-left">{{ $dato->nombre }}</td>  
        <td class="text-left">{{ $dato->apellidos.' '.$dato->nombres }}</td> 
        <td class="text-left">{{ $dato->cantidad_fichas }}</td>
        <td class="text-left">{{ $dato->hora_inicio }}</td>
        <td class="text-left">{{ $dato->hora_intervalo }}</td>        
        <td>
            Estado
            <small class="{{ $dato->eliminado == 0? 'text-success' : 'text-danger' }} mr-1">
            <i class="fas {{ $dato->eliminado == 0? 'fa-arrow-up' : 'fa-arrow-down' }} "></i>
            {{ $dato->eliminado == 0? 'Activo' : 'Inactivo' }}
            </small>
        </td>                   
        <td class="td-actions text-right"> 
            <div class="btn-group">
                <a href="{{ route('hcl_fichas_registradas.edit', $dato->id) }}" style="display: {{ $dato->eliminado == 1? 'none':'' }};" class="btn {{ $dato->eliminado == true? 'btn-outline-danger':'btn-outline-warning' }} btn-sm" rel="tooltip" title="{{ $dato->res_eliminado == true? 'eliminar':'editar' }}">
                    <i class="{{ $dato->eliminado == true? 'fa fa-trash':'fa fa-edit' }}"></i>                                
                </a>                  
            </div>                                  
        </td> 
    </tr>
@endforeach
 