@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>NUEVO REGISTRO DAT</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-body">   
				<form method="POST" action="{{ route('hsi_registro_datos.store') }}" autocomplete="off" id="form">
					{{ csrf_field() }}
					@include($model->rutaview.'form')
					@include($model->rutaview.'modal')
				</form>
			</div>
		</div>
	</div>
</div>
@stop
