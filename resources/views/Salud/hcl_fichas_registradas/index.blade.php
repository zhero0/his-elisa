@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTADO - ESPECIALIDADES</h1>
@stop   

@section('content')
<div class="card"> 
	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
	<input type="hidden" id="txtAccion" value="{{ $model->accion }}">   
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Sigla</th> 
					<th>Especialidad</th> 								
					<th>Medico</th>
					<th>Fichas</th>
					<th>Hora Inicio</th>
					<th>Tiempo de Atención</th>
					<th>Vigente</th>					
					<th></th>
				</tr> 
			</thead>
			<tbody id="indexContent">
				@include($model->rutaview.'indexContent')
			</tbody>
		</table>
	</div>  
</div> 
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 