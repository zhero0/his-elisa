<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
  <input type="hidden" id="idhcl_empleador" name="idhcl_empleador" value="{{ $model->idhcl_empleador }}">
  <input type="hidden" id="idhcl_poblacion" name="idhcl_poblacion" value="{{ $model->idhcl_poblacion }}"> 
  <input type="hidden" id="dateHoy" value="{{ $model->dateHoy }}"> 
  <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}"> 
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">   

  <div class="card-header">
    <div class="row"> 

      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <input type="date" class="form-control datepicker" id="dateFicha" name="dateFicha">
      </div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <input type="text" id="txtDocumento" name="txtDocumento" class="form-control" placeholder="DOCUMENTO" style="text-transform: uppercase;"> 
      </div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <input type="text" id="txtPaterno" name="txtPaterno" class="form-control" placeholder="AP. PATERNO" style="text-transform: uppercase;"> 
      </div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <input type="text" id="txtMaterno" name="txtMaterno" class="form-control" placeholder="AP. MATERNO" style="text-transform: uppercase;">  
      </div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <input type="text" id="txtNombres" name="txtNombres" class="form-control" placeholder="NOMBRES" style="text-transform: uppercase;"> 
      </div>       
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">           
        <a id="btnBuscar" type="button" class="btn btn-block btn-outline-primary">
          <i class="fa fa-search"></i> buscar
        </a>
      </div> 
       
    </div> 
  </div>
  <div class="row">
    <div class="col-md-3"> 
      <div class="card card-primary card-outline">
        <div class="card-body box-profile">
          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="/imagenes/icono_medico.jpg" alt="User profile picture">
          </div>
          <h3 class="profile-username text-center" id="idMedico_titulo">{{ $model->medico_titulo }}</h3>
          <p class="text-muted text-center">{{ $model->especialidad_titulo }}</p>
            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
              <b>Fecha</b> <a class="float-right"><p  id="fechaText"></p></a>
              </li>
              <li class="list-group-item">
              <b>Especialidad</b> <a class="float-right">-</a>
              </li>
              <li class="list-group-item">
              <b>Disponibles</b> <a class="float-right" >{{ $model->fichas_disponibles }}</a>
              </li>
              <li class="list-group-item">
              <b>Estado</b> 
              <a class="float-right">

              <small class="{{ $model->eliminado == 0? 'text-success' : 'text-danger' }} mr-1">
              <i class="fas {{ $model->eliminado == 0? 'fa-arrow-up' : 'fa-arrow-down' }} "></i>
              {{ $model->eliminado == 0? 'Activo' : 'Inactivo' }}
              </small>
              </a>
              </li>
            </ul> 
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">   
                <a href="{{ URL::previous() }}" type="button" class="btn btn-block btn-outline-primary">
                  <span class="fa fa-undo" aria-hidden="true"></span> Regresar atras
                </a> 
              </div> 
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">   
                <a onclick="imprimirLista()" type="button" class="btn btn-block btn-outline-info">
                  <span class="fa fa-print" aria-hidden="true"></span> Imprimir Lista
                </a> 
              </div> 
            </div> 
        </div>  
      </div> 
    </div>
    <div class="col-md-8">
      <div class="card">
        <div class="card-body">           
            <span class="username">
              <a href="#">Fichas</a>
              <p>Lista de fichas publicadas para la especialidad, permite registrar, eliminar e imprimir un ticket de cita medica.</p>
            </span>  
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">    
            <table id="tableContent" class="table table-striped">
              <thead>
                <tr>
                  <th>N°</th> 
                  <th>Hora</th> 
                  <th>Fecha</th>                 
                  <th>Medico</th>
                  <th>Documento</th>
                  <th>Nombre Completo</th> 
                  <th>Estado de atención</th>                  
                  <th></th>
                </tr> 
              </thead>
              <tbody id="tbodyContent"> 
                @foreach($modelFichas as $dato) 
                  <tr role="row" style="{{ config('app.styleTableRow') }}"> 
                    <td>{{ $dato->res_numero }}</td> 
                    <td>{{ $dato->res_hora_registro }}</td>                                                   
                    <td class="text-left">{{ $dato->res_fecha_registro }}</td>                              
                    <td class="text-left" id="tdMedico_fic{{$dato->id}}">{{ $dato->res_ap_medico.' '.$dato->res_nombres_medico }}</td> 
                    <td class="text-left" id="tdDocumento_fic{{$dato->id}}">{{ $dato->res_documento }}</td>                    
                    <td class="text-left" id="tdPersona_fic{{$dato->id}}">{{ $dato->res_primer_apellido.' '.$dato->res_segundo_apellido.' '.$dato->res_nombre }}</td>
                    <td class="text-left" id="tdReserva_fic{{$dato->id}}">{{ $dato->res_ficha_nombre }}</td>                      
                    <td class="td-actions text-right">
                      <div class="btn-group"> 
                        <a onclick="solicitarAtencion( {{ $dato->id }} )" id="btnAtencion{{$dato->id}}" style="display:{{ ($dato->res_ficha_id == 2 ||  $dato->res_ficha_id == 4)? 'none':'' }};"  class="btn btn-outline-info btn-sm" rel="tooltip" title="Registrar reserva">
                          <i class="fa fa-user"></i>                                
                        </a>  
                        
                        <a onclick="eliminarAtencion( {{ $dato->id }} )" id="btnLiberar{{$dato->id}}"  class="btn btn-outline-danger btn-sm" style="display:{{ ($dato->res_ficha_id == 1 ||  $dato->res_ficha_id == 4)? 'none':'' }};"  rel="tooltip" title="Eliminar reserva">
                          <i class="fa fa-trash"></i>                                
                        </a> 
                        <a onclick="imprimirAtencion( {{ $dato->id }} )"  id="btnImpresion{{$dato->id}}" class="btn btn-block {{ $dato->res_ficha_id == 2? 'btn-outline-success':'btn-outline-danger' }} btn-sm" style="display:{{ $dato->res_ficha_id == 2? '':'none' }};"  rel="tooltip" title="Imprimir reserva">
                          <i class="fa fa-print"></i>                                
                        </a>  
                        <a onclick="imprimirEstudios( {{ $dato->id }} )"  id="btnEstudios{{$dato->id}}" class="btn   {{ $dato->res_ficha_id == 2? 'btn-outline-dark':'btn-outline-danger' }} btn-sm" style="display:{{ $dato->res_ficha_id == 2? '':'none' }};"  rel="tooltip" title="Imprimir estudios">
                          <i class="fa fa-print"></i>                                
                        </a>  
                      </div>                              
                    </td>
                  </tr>                           
                @endforeach    
              </tbody>
            </table> 
          </div> 
        </div>
      </div>
    </div>
  </div>  
</div>
 