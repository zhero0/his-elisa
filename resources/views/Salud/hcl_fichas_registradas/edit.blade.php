@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>RESERVAS DE ATENCION MEDICA</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('hcl_fichas_registradas.update', $model->id) }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	<input name="_method" type="hidden" value="PATCH"> 
	@include($model->rutaview.'form')  

</form>
@stop 
@include($model->rutaview.'modal')