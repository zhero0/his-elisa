<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	  <!-- <h4 class="list-group-item-heading text-primary">
	    <i class="fa fa-fw fa-stethoscope"></i>
	    Signos Vitales
	  </h4> -->
	  <div class="table-responsive" style="height: 490px; overflow-y: auto; border: 1px solid #374850;">
		<table id="tContenedor_Variables_signos_vitales" class="table table-sm">
			<thead style="{{ config('app.styleTableHead') }}">
				<tr>
					<th class="col-xs-5">Variable</th>
		            <th>Valor</th>
		            <th class="col-xs-3">Resultado</th>
		            <th class="col-xs-1">Udd</th>
				</tr> 
			</thead>
			<tbody id="tbody_Variables_signos_vitales" style="{{ config('app.styleTableRow') }}">
				<tr>
		         	<td>Vacío...</td>
		        </tr>
			</tbody>
		</table>
	  </div>
	</div>
</div>
<br> 