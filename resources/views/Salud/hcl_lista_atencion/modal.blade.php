<!-- VENTANA MODAL PARA REGISTRAR "BAJA MÉDICA" -->
<div class="modal fade" id="vModalBajaMedica">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">
		        	BAJA MÉDICA
		        </h4>

		        <div class="onoffswitch">
				    <input type="checkbox" name="checkOpcion" class="onoffswitch-checkbox" id="checkOpcion">
				    <label class="onoffswitch-label" for="checkOpcion">
				        <span class="onoffswitch-inner"></span>
				        <span class="onoffswitch-switch"></span>
				    </label>
				</div>
		    </div>
		    <div class="modal-body">
		    	<div class="container-fluid">
		    		<form action="#" id="form_BajaMedica" method="post" autocomplete="off">
				    	<div class="row">
				    		<div class="col-xs-12 col-sm-6 col-md-2 col-lg-4">
		                        <label>Fecha Desde</label>
		                        <div class='input-group date' id='datetimepickerDesde'>
		                            <input type='text' class="form-control" id="fechaDesde">
		                            <span class="input-group-addon">
		                                <span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>
		                    <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
		                        <label>Cantidad(días)</label>
		                        <input type='text' class="form-control" id="txtCantidadBajaMedica">
		                    </div>
		                    <div class="col-xs-12 col-sm-6 col-md-2 col-lg-4">
		                        <label>Fecha Hasta</label>
		                        <div class='input-group date' id='datetimepickerHasta'>
		                            <input type='text' class="form-control" id="fechaHasta">
		                            <span class="input-group-addon">
		                                <span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div> 
				    	</div>

				    	<div class="row">
				    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
		                        <label>Baja Médica</label>
		                        <select id="txtIdHcl_variables_bajas" class="form-control">
		                          <option value="0">Seleccione! </option>
		                            @foreach($modelHcl_variables_bajas as $dato)
		                              @if($model->iddatosgenericos == $dato->id)
		                                 <option value="{{$dato->id}}" selected>{{ $dato->nombre}}</option> 
		                              @else
		                                <option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
		                              @endif
		                            @endforeach
		                        </select>
	                      	</div>
	                    </div>

	                    <label>Descripción</label>
	            		<textarea class="form-control" id="taDescripcion_formato_historia" rows="3"></textarea>
            		</form>

            		<h4 style="color: red; visibility: hidden;" id="textoValidacion_BajaMedica">COMPLETE LOS DATOS, POR FAVOR! </h4>
		    	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>



<!-- VENTANA MODAL PARA REGISTRAR "CERTIFICADO MÉDICO" -->
<div class="modal fade" id="vModalCertificadoMedico">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">
		        	CERTIFICADO MÉDICO
		        </h4>

		        <div class="onoffswitch">
		        	<!-- <input type="checkbox" name="checkOpcion" class="onoffswitch-checkbox" id="checkOpcion"> -->
				    <input type="checkbox" class="onoffswitch-checkbox" id="checkCertificadoMedico">
				    <label class="onoffswitch-label" for="checkCertificadoMedico">
				        <span class="onoffswitch-inner"></span>
				        <span class="onoffswitch-switch"></span>
				    </label>
				</div>
		    </div>
		    <div class="modal-body">
		    	<div class="container-fluid">
		    		<form action="#" id="form_CertificadoMedico" method="post" autocomplete="off">
	                    <label>Descripción</label>
	            		<textarea class="form-control" id="taDescripcion_CertificadoMedico" rows="9" placeholder="Esbriba..."></textarea>
            		</form>
		    	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div> 
<!-- VENTANA MODAL PARA VISUALIZAR REPORTE -->
<div class="modal fade" id="vModalVisualizarReporte">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header"> 
		        <h4 class="modal-title" id="tituloModal">
		        	REPORTE
		        </h4>
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close"> 
		        <span aria-hidden="true">&times;</span></button>
		    </div>
		    <div class="modal-body">
		    	<div id="secccionImprimeDetalleAtencion_estudios" style="height: 410px;"></div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>
 
<!-- VENTANA MODAL PARA BUSCAR PLANTILLA -->
<div class="modal fade" id="vModalBuscadorPlantilla">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
	      	<h4 class="modal-title">PLANTILLA(S) MEDICAS</h4>
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		        <span aria-hidden="true">&times;</span></button>	 
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Plantilla" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Plantilla">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div> 
		    	<div class="table-responsive" style="height: 400px; overflow-y: auto;">
	                <table id="tablaContenedor_Plantilla" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-2">Nombre</th>
		                        <th class="col-xs-8">Plantilla</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody id="tbody_Plantilla" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-danger" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div> 

<!-- VENTANA MODAL PARA REGISTRAR PLANTILLA -->
<div class="modal fade" id="vModalRegistrarPlantilla">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
	      	<h4 class="modal-title">REGISTRAR NUEVA PLANTILLA</h4>
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		        <span aria-hidden="true">&times;</span></button>	 
		    </div>
		    <div class="modal-body">
		    	<div class="row"> 
			      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				        <label for="nombre" class="col-sm-6 col-form-label">Registrar nombre de plantilla medica.</label>
				        <div class="col-sm-12">
				          <input type="text" class="form-control" id="nombrePlantilla" name="nombrePlantilla" value="{{ $model->nombre }}">
				        </div> 
			      	</div>
			      	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			          	<label for="nombre" class="col-sm-6 col-form-label">Personalizar detalle de la plantilla</label> 
			          	<textarea class="form-control" id="ckeditor_plantilla" name="detallePlantilla" value="Escriba..." rows="12">{{ $model->plantilla }}</textarea>
			        </div>
			    </div>  
	      	</div>
	      	<div class="modal-footer">
	      	<button type="button" class="btn btn-success" id="btnGuardarPlantilla" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Registrar plantilla
		       	</button>
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar registro
		       	</button>
		    </div>
	    </div>
	</div>
</div>

<!-- VENTANA MODAL PARA VISUALIZAR CLASIFICACIÓN DE SALUD -->
<div class="modal fade" id="vModalClasificacionSalud">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
	      		<h4 class="modal-title">Clasificación de Masa Corporal</h4>
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        
		    </div>
		    <div class="modal-body">
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_ClasificacionSalud" class="table table-striped">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-5">ÍNDICE MASA CORPORAL</th>
		                        <th>CLASIFICACIÓN</th>
	                       </tr>
	                    </thead>
	                    <tbody id="tbody_ClasificacionSalud" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	            
	            <h4>IMC = Peso (kg) / altura (m)2</h4>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			        Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div> 
  
<!-- VENTANA MODAL PARA REGISTRAR "REFERENCIA MÉDICA" -->
<div class="modal fade" id="vModalReferenciaMedica">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">
		        	REFERENCIA MÉDICA
		        </h4>

		         <div class="onoffswitch">
		        	<!-- <input type="checkbox" name="checkOpcion" class="onoffswitch-checkbox" id="checkOpcion"> -->
				    <input type="checkbox" class="onoffswitch-checkbox" id="checkReferenciaMedica">
				    <label class="onoffswitch-label" for="checkReferenciaMedica">
				        <span class="onoffswitch-inner"></span>
				        <span class="onoffswitch-switch"></span>
				    </label>
				</div>
		    </div>
		    <div class="modal-body">
		    	<div class="container-fluid">
		    		<form action="#" id="form_ReferenciaMedica" method="post" autocomplete="off">

				    	<div class="row">
				    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
		                        <label>Establecimiento</label>
		                        <select id="txtIdAlmacen" class="form-control">
		                          	<option value="0">Seleccione! </option>
		                          	@foreach($modelEstablecimiento as $dato)
		                            	<option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
		                            @endforeach  
		                        </select>
	                      	</div>
	                      	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
		                        <label>Especialidad</label>
		                        <select id="txtIdHcl_cuaderno" class="form-control">
		                          <option value="0">Seleccione! </option> 
		                          	@foreach($modelEspecialidad as $dato)
		                            	<option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
		                            @endforeach 
		                        </select>
	                      	</div>
	                    </div>
 

	                    <label>RESUMEN DE ANANMESIS Y EXAMEN CLINICO</label>
	            		<textarea class="form-control" id="taDescripcion_examen_clinico" rows="3"></textarea>
	            		<label>TRATAMIENTO</label>
	            		<textarea class="form-control" id="taDescripcion_tratamiento_inicial" rows="3"></textarea>
	            		<label>RECOMENDACIONES</label>
	            		<textarea class="form-control" id="taDescripcion_recomendaciones" rows="3"></textarea>
            		</form>

            		<h4 style="color: red; visibility: hidden;" id="textoValidacion_ReferenciaMedica">COMPLETE LOS DATOS, POR FAVOR! </h4>
		    	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>





<!-- VENTANA MODAL PARA REGISTRAR "HOJA DE INTERNACION" -->
<div class="modal fade" id="vModalInternacion">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">
		        	HOJA DE INTERNACIÓN
		        </h4>

		         <div class="onoffswitch">
		        	<!-- <input type="checkbox" name="checkOpcion" class="onoffswitch-checkbox" id="checkOpcion"> -->
				    <input type="checkbox" class="onoffswitch-checkbox" id="checkInternacionMedica">
				    <label class="onoffswitch-label" for="checkInternacionMedica">
				        <span class="onoffswitch-inner"></span>
				        <span class="onoffswitch-switch"></span>
				    </label>
				</div>
		    </div>
		    <div class="modal-body">
		    	<div class="container-fluid">
		    		<form action="#" id="form_Internacion" method="post" autocomplete="off">

		    			<div class="row"> 
		    				<div class="col-xs-12 col-sm-6 col-md-1 col-lg-1">
		    					
		    				</div>
			              	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
		                        <label>Fecha</label>
		                        <div class='input-group date' id='datepickerFecha'>
		                          <input type='text' class="form-control" id="fecha" name="fechaprogramacion" value="{{  $model->fecha_registro }}">
		                            <span class="input-group-addon">
		                                <span class="glyphicon glyphicon-calendar"></span>
		                            </span>
		                        </div>
		                    </div>

		                    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
		                        <input type="hidden" id="txtHiddenHora" name="txtHiddenHora" value="{{ $model->hora_registro }}">
		                        <label>Hora</label>
		                        @include('layouts.hora')
		                    </div>
		    			</div>	
		    				 
				    	<div class="row">
				    		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
		                        <label>Establecimiento</label>
		                        <select id="txtIdAlmacenInternacion" class="form-control">
		                          	<option value="0">Seleccione! </option>
		                          	@foreach($modelEstablecimiento as $dato)
		                            	<option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
		                            @endforeach  
		                        </select>
	                      	</div>
	                      	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
		                        <label>Especialidad</label>
		                        <select id="txtIdHcl_cuadernoInternacion" class="form-control">
		                          <option value="0">Seleccione! </option> 
		                          	@foreach($modelEspecialidad as $dato)
		                            	<option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
		                            @endforeach 
		                        </select>
	                      	</div>
	                    </div> 
	                    <div class="row">
		    				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
			                	<label class="bmd-label">Datos Familiar</label> <br>  
			                	<input type="text" id="txtFamiliar_datos" name="txtFamiliar_datos" class="form-control" value="" >			               		
			              	</div>
			              	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
			                	<label class="bmd-label">Nro. movil</label> <br>
			                	<input type="text" id="txtFamiliar_movil" name="txtFamiliar_movil" class="form-control" value="" >			               		
			              	</div>
			              	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			              		<label class="bmd-label">Datos domicilio</label> <br>
			                	<input type="text" id="txtFamiliar_domicilio" name="txtFamiliar_domicilio" class="form-control" value="" >			               		
			              	</div>
			              	<br>		    			
		    				<label>Descripción familiar</label>
	            			<textarea class="form-control" id="taDescripcion_familiar" name="taDescripcion_familiar" rows="3"></textarea>
	            			<br>
	            			<label>Recomendaciones medicas</label>
	            			<textarea class="form-control" id="taRecomendacion_medica" name="taRecomendacion_medica" rows="3"></textarea> 
		    			</div>
            		</form>

            		<h4 style="color: red; visibility: hidden;" id="textoValidacion_Internacion">COMPLETE LOS DATOS, POR FAVOR! </h4>
		    	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>
