<ul class="nav nav-pills flex-column">
  <li class="nav-item active">
    <a href="#" class="nav-link" id="btnRegistro_medicamentos">
      	<i class="fas fa-inbox"></i> Prescripcion de Medicamentos      
      	<span class="badge bg-success float-right" id="spanTotalRecetas_visual">0</span>
    </a> 
  </li>
  <!-- <li class="nav-item" >
    <a href="#" class="nav-link"> 
      	<i class="far fa-file-alt"></i> Laboratorio Clinico
      	<span class="badge bg-danger float-right">2</span> 
    </a> 
  </li> -->
  <li class="nav-item">
    <a href="#" class="nav-link" id="btnRegistro_imagenologia">    
      	<i class="far fa-file-alt"></i> Imagenología - Rayos X 
      	<span class="badge bg-warning float-right"  id="spanTotalExamenes_rx_visual">0</span>
    </a>
  </li> 
  <li class="nav-item">
    <a href="#" class="nav-link" id="btnRegistro_certificado">      
      	<i class="far fa-file-alt"></i> Certificado Médico  
      	<span class="badge bg-danger float-right" id="spanCertificadoMedico">No</span>
    </a>
  </li> 
	<!-- <li class="nav-item">
    <a href="#" class="nav-link">	      	
      	<i class="fas fa-filter"></i> Baja Médica 
      	<span class="badge bg-danger float-right" id="spanBajaMedica">No</span>
    </a>
	</li> -->
	<li class="nav-item">
	    <a href="#" class="nav-link" id="btnRegistro_referencia">	      	
	      	<i class="fas fa-filter"></i> Referencia médica 
	      	<span class="badge bg-danger float-right" id="spanReferenciaMedica">No</span>
	    </a>
	</li>
</ul>


 