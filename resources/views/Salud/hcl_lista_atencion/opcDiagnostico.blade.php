<?php
// $model->nivel_seguridad = 1;
?>
<!-- <div class="row">
    <div class="col-xs-12">
      <h4 class="list-group-item-heading text-primary">
        <i class="fa fa-fw fa-user-md"></i>
        Diagnóstico
      </h4>
    </div>
</div> -->

<div class="col-xs-12"> 
  	<div class="input-group">
        	<input type="text" id="txtBuscarCie" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
        	<span class="input-group-btn">
          	<button class="btn btn-danger" type="button" id="btnBuscarDiagnostico">
            	<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            	Buscar
          	</button>
        	</span>
      </div>
    	<div class="table-responsive" style="height: 200px; overflow-y: auto; border: 1px solid #374850;">
        <table id="tablaContenedorDiagnostico" class="table table-sm">
            <thead style="{{ config('app.styleTableHead') }}">
                <tr>
                  <th>Diagnostico - Código CIE</th>
                  <th></th>
               </tr>
            </thead>
            <tbody id="tbodyDiagnostico" style="{{ config('app.styleTableRow') }}">
              <tr>
                <td>Vacío...</td>
              </tr>
            </tbody>
        </table>
    	</div>

    	<div class="row">
      	<div class="col-xs-4">
              <button class="btn btn-default" id="btnVisualizaCieSeleccionados" type="button"  title="Click para visualizar los Diagnosticos Seleccionados">
                  Total Seleccionados <span class="badge" id="spanTotalDiagnosticoCie">0</span>
              </button>
          </div>
      </div> 
</div>
@if($model->nivel_seguridad == 1)
<div class="col-xs-2">
    <h4 class="list-group-item-heading text-primary">
      <i class="fa fa-lock"></i>
      Seguridad
    </h4>
    <div class="onoffswitch">
      <input type="checkbox" class="onoffswitch-checkbox" id="checkDiagnostico_seguro">
      <label class="onoffswitch-label" for="checkDiagnostico_seguro">
          <span class="onoffswitch-inner"></span>
          <span class="onoffswitch-switch"></span>
      </label>
    </div>
  </div>
@endif 