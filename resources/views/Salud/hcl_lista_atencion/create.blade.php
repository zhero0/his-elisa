@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>REGISTRAR ATENCIÓN MEDICA - ESPECIALIDAD: {{ $model->hcl_cuaderno }}</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('hcl_lista_atencion.store') }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	@include($model->rutaview.'form')
	@include($model->rutaview.'modal')  	
</form>
@stop