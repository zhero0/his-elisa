<div class="col-md-12">
  <div class="card">
    <div class="card-body">           
        <span class="username">
          <a href="#">Lista de atención.</a>
          <p>Lista de fichas publicadas para la especialidad, permite registrar e imprimir una cita medica.</p>
        </span>  
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">    
        <table id="tableContent" class="table table-striped">
          <thead>
            <tr>
              <th>N°</th> 
              <th>Hora</th> 
              <th>Fecha</th>                 
              <th>Medico</th>
              <th>Documento</th>
              <th>Nombre Completo</th> 
              <th>Estado de atención</th>                  
              <th></th>
            </tr> 
          </thead>
          <tbody id="tbodyContent"> 
            @foreach($model as $dato) 
              <tr role="row" style="{{ config('app.styleTableRow') }}"> 
                <td>{{ $dato->res_numero }}</td> 
                <td>{{ $dato->res_hora_registro }}</td>                                                   
                <td class="text-left">{{ $dato->res_fecha_registro }}</td>                              
                <td class="text-left" id="tdMedico_fic{{$dato->id}}">{{ $dato->res_ap_medico.' '.$dato->res_nombres_medico }}</td> 
                <td class="text-left" id="tdDocumento_fic{{$dato->id}}">{{ $dato->res_documento }}</td>                    
                <td class="text-left" id="tdPersona_fic{{$dato->id}}">{{ $dato->res_primer_apellido.' '.$dato->res_segundo_apellido.' '.$dato->res_nombre }}</td>
                <td class="text-left" id="tdReserva_fic{{$dato->id}}">{{ $dato->res_ficha_nombre }}</td>                      
                <td class="td-actions text-right">
                  <div class="btn-group">  
                    <a href="{{ route('hcl_lista_atencion.show', $dato->id) }}" style="display:{{ $dato->res_ficha_id == 4? 'none':'' }};" class="btn btn-outline-info btn-sm" rel="tooltip" title="Editar">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ route('hcl_lista_atencion.edit', $dato->id) }}" style="display:{{ $dato->res_ficha_id == 4? '':'none' }};" class="btn btn-outline-warning btn-sm" rel="tooltip" title="Editar">
                        <i class="fa fa-edit"></i>
                    </a> 
                    <button type="button" onclick="imprimirSolicitudes( {{ $dato->idhcl_cabecera_registro }} )" style="display:{{ $dato->res_ficha_id == 4? '':'none' }};" class="btn btn-outline-success btn-sm" rel="tooltip" title="Opciones Reporte Salud">
                      <i class="fa fa-print"></i>
                    </button>
                  </div>                              
                </td>
              </tr>                           
            @endforeach    
          </tbody>
        </table> 
      </div> 
    </div>
  </div>
</div>