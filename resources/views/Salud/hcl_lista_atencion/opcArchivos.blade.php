<div class="form-group">
  <ul class="list-group">
      <li class="list-group-item">
        <i class="fas fa-paperclip"></i>
        <input type="file" name="archivo[]" multiple>
      </li>
    </ul>
  <p class="help-block">Max. 2MB</p>
</div>  
<div class="row">
  <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
    <div class="list-group">
      @if(count($modelHcl_cabecera_registro_documento) > 0)
        <div class="list-group-item">
          <h4 class="list-group-item-heading text-primary">
            <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
            DESCARGAR ARCHIVOS
          </h4>
        </div>
        <ul class="list-group">
          @foreach ($modelHcl_cabecera_registro_documento as $value)
            <?php
              $valor = explode('_', $value->archivo);
              $archivo = substr($value->archivo, strlen($valor[0])+1, strlen($value->archivo));
            ?>
            <a href="{{ url('descargarArchivo?path='.config('app.rutaArchivosSalud').'/'.$value->archivo) }}" class="list-group-item list-group-item-action">{{ $archivo }}</a>
          @endforeach
        </ul>
      @endif
    </div>
  </div>
</div>