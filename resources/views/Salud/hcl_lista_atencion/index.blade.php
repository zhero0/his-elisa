@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTA DE CONSULTAS MEDICAS</h1>
@stop   

@section('content') 
<div class="card">  
	<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  	<input type="hidden" id="txtdatos_imprimir" value="{{ $model->datos_imprimir }}">
	<div class="card-header">
		<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
			<!-- <div class="row" style="height: 6px; color: transparent;">.</div> --> 
	    </div>
		<form class="navbar-form navbar-right" roles="searchNuevo" action="/hcl_lista_atencion" autocomplete="off">
        	<div class="row text-right">
        		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        			    
	        	</div>
	        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	        		<input type="date" class="form-control datepicker" id="fechaRegistro" name="fechaRegistro">  
	        	</div>
	        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	        		<select name="hcl_cuaderno_personal" class="form-control" onchange="this.form.submit()"> 
			          @foreach($cuaderno_personal as $dato)
			          	@if($model->hcl_cuaderno_personal == $dato->id)
			          		<option value="{{$dato->id}}" selected="selected"> {{$dato->nombre}} </option>
			          	@else
			          		<option value="{{$dato->id}}"> {{$dato->nombre}} </option>
			          	@endif
			          @endforeach
			        </select>  
	        	</div> 
	        	<button type="submit" class="btn btn-primary"> Buscar </button>
        	</div>  
        </form>  
	</div>
</div>
 	<?php $fecha_actual = date('Y-m-d'); ?> 	 
	@include($model->rutaview.'indexContent')							 	  

@stop 
 
<div class="modal" id="vImprimeDetalleAtencion">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
        	<h4 class="modal-title"> IMPRESIONES DE CONSULTA MEDICA </h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>            
        </div>
        <div class="modal-body"> 
        	<div class="row">
				<div class="col-md-3">
					<div class="card card-info">
						<div class="card-header">
							<h3 class="card-title">Reportes de impresión</h3>
						</div>
						<div class="card-body"> 
							<ul>
				              	<li>
				              		<a href="#" id="btnPrint_Salud"><i class="glyphicon glyphicon-print"></i> Historia Clinica </a>
				              	</li>
				              	<li>
				              		<a href="#" id="btnPrint_Receta"><i class="glyphicon glyphicon-print"></i> Prescripción </a>
				              	</li>
				              	<li>
				              		<a href="#" id="btnPrint_Laboratorio"><i class="glyphicon glyphicon-print"></i> Ex. Laboratorio </a>
				              	</li>
				              	<li>
				              		<a href="#" id="btnPrint_Imagenologia"><i class="glyphicon glyphicon-print"></i> Ex. Imagenología </a>
				              	</li>
				                <li>
				                	<a href="#" id="btnPrint_CertifMedico"><i class="glyphicon glyphicon-print"></i> Certif. Médico </a>
				                </li>
				                <li>
				                	<a href="#" id="btnPrint_BajaMedica"><i class="glyphicon glyphicon-print"></i> Baja Médica </a>
				                </li>
				                <li>
				                	<a href="#" id="btnPrint_ReferenciaMedica"><i class="glyphicon glyphicon-print"></i> Referencia Médica </a>
				                </li>
				                <li>
				                	<a href="#" id="btnPrint_InternacionMedica"><i class="glyphicon glyphicon-print"></i> Sol. Internación </a>
				                </li>
			              	</ul>
						</div>
					</div> 
				</div> 
				<div class="col-md-9">
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title">Visor de impresión</h3>
						</div>  
						<div class="card-body">
							<div id="secccionImprimeDetalleAtencion" style="height: 410px;"></div>	
						</div>
						
						<!-- <div class="card-footer">
							Visit <a href="https://getdatepicker.com/5-4/">tempusdominus </a> for more examples and information about
							the plugin.
						</div>  -->
						<div class="card-footer">
							Descarga e impresión de resultados
						</div> 
					</div> 
				</div> 
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div>