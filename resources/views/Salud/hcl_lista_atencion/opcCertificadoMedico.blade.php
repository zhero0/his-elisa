<!-- VENTANA MODAL PARA REGISTRAR "CERTIFICADO MÉDICO" -->
<h5 class="list-group-item-heading text-primary"> 
    CERTIFICADO MÉDICO
</h5> 
<div class="form-group">
	<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
		<input type="checkbox" class="custom-control-input" id="checkCertificadoMedico">
		<label class="custom-control-label" for="checkCertificadoMedico">Habilitar la edición del certificado.</label>
	</div>
</div>

<div id="divPanel_cert_show" style="display: none;">
	<label>Redacción del informe medico.</label>
	<textarea class="form-control" id="taDescripcion_CertificadoMedico" rows="9" placeholder="Esbriba..."></textarea>	
</div>