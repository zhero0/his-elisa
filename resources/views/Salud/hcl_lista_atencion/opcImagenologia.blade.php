<div class="col-xs-12"> 
  <div class="input-group">
      <input type="text" class="form-control input-xs" id="txtBuscarExamen" placeholder="Escriba...">
      <span class="input-group-btn">
        <button class="btn btn-danger" type="button" id="btnBuscarExamen">
          <span class="fa fa-search" aria-hidden="true"></span>
          Buscar
        </button>
      </span>
  </div>
  <div class="table-responsive" style="height: 200px; overflow-y: auto; border: 1px solid #374850;">
    <table id="tablaContenedorExamenes" class="table table-striped"> 
        <thead style="{{ config('app.styleTableHead') }}">
            <tr>
              <th class="col-xs-2">Código</th>
              <th>Nombre</th>
              <th class="col-xs-5">Especialidad</th>
              <th class="col-xs-1"></th>
            </tr>
        </thead>
        <tbody id="tbodyExamenes"> 
          <tr>
            <td>Vacío...</td>
          </tr>
        </tbody>
    </table>
  </div>

  <div class="row">
    <div class="col-xs-4">
          <button class="btn btn-default" id="btnVisualizaExamenesSeleccionados" type="button"  title="Click para visualizar los Diagnosticos Seleccionados">
              Total Seleccionados <span class="badge" id="spanTotalExamenSeleccion">0</span>
          </button>
      </div>
  </div> 
</div>  