@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>EDITAR REGISTRO DE ATENCIÓN MEDICA - ESPECIALIDAD: {{ $model->hcl_cuaderno }}</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('hcl_cabecera_registro.update', $model->id) }}"  enctype = "multipart/form-data" autocomplete="off" id="form">
	{{ csrf_field() }}
		<input name="_method" type="hidden" value="PATCH"> 
	@include($model->rutaview.'form')
	@include($model->rutaview.'modal')    	
</form>
@stop