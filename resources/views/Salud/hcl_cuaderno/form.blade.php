<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    <input type="hidden" id="txtHiddenJSONdatos" name="txtHiddenJSONdatos" value="{{ $model->txtHiddenJSONdatos }}">
    
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3"> 
        <label for="codigo" class="col-sm-6 col-form-label">Sigla</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="sigla" name="sigla" value="{{ $model->sigla }}" >
        </div> 
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <label for="codigo" class="col-sm-6 col-form-label">Nombre:</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $model->nombre }}" >
        </div> 
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
      <label>TIPO DE ESPECIALIDAD</label>
          <br>
          <select name="id_especialidad" id="id_especialidad" class="form-control" onchange="habilitaRegistro()"> 
              <option disabled selected>Seleccionar</option> 
              @foreach($modelHcl_especialidad as $dato) 
                @if($model != null)
                  <option value="{{$dato->id}}" {{ $model->id_especialidad == $dato->id ? 'selected="selected"' : ''  }} >{{$dato->descripcion }}</option> 
                @else
                  <option value="{{$dato->id}}">{{$dato->descripcion}}</option> 
                @endif
              @endforeach
          </select>  
      </div>
      <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
         
      </div>
    </div>
    
    <div class="row"> 
      <div class="col-xs-1 col-sm-3 col-md-2 col-lg-2">
        <label for="codigo" class="col-sm-6 col-form-label">Minutos:</label>
        <div class="col-sm-12">
          <input type="time" class="form-control" id="escala_tiempo" name="escala_tiempo" value="{{ $model->escala_tiempo }}">
        </div>   
      </div> 
      <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2"> 
        <label for="codigo" class="col-sm-6 col-form-label">Hora Inicio:</label>
        <div class="col-sm-12">
          <input type="time" class="form-control" id="hora_inicio" name="hora_inicio" value="{{ $model->hora_inicio }}">
        </div>    
      </div>
      <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">  
        <label for="codigo" class="col-sm-6 col-form-label">Hora Fin:</label>
        <div class="col-sm-12">
          <input type="time" class="form-control" id="hora_fin" name="hora_fin" value="{{ $model->hora_fin }}">
        </div>   
      </div>
    </div> 
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <br>
          <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
              <br>
              <label for="respuesta" class="col-sm-6 col-form-label">Descripción</label> 
            </div>

            <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
              <h4 class="list-group-item-heading text-primary">
                <i class="fa fa-lock"></i>
                Seguridad
              </h4>
              <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox" id="checkSeguridad">
                <label class="onoffswitch-label" for="checkSeguridad">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
                </label>
              </div>
            </div>
          </div>

          <textarea class="form-control" id="summary-ckeditor" name="formato_historia" value="Escriba..." rows="12">{{ $model->formato_historia }}</textarea>
        </div>
    </div>
</div>

<a href="/cuaderno" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardar" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif