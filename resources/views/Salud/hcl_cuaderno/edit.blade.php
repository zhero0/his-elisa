@extends('adminlte::page') 
@section('title', 'EDITAR REGISTRO')  
@section('content_header')
    <h1>EDITAR REGISTRO DE ESPECIALIDAD</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-body"> 
				<form method="POST" action="{{ route('cuaderno.update', $model->id) }}" autocomplete="off" id="form"> 
					<input name="_method" type="hidden" value="PATCH"> 
					@include($model->rutaview.'form')
				</form> 
			</div>
		</div>
	</div>
</div>
@stop