@extends('adminlte::page')
@section('title', 'LISTA DE ESPECIALIDADES')
@section('content_header')
    <h1>LISTA DE ESPECIALIDADES</h1>
@stop  


@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div> 
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('cuaderno.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo registro
		        </a>
		    </div> 
		</div>
	</div>  
	<div class="card-body"> 
		<table id="table_cuaderno" class="table table-striped">
			<thead>
				<tr> 
 					<th>Sigla</th>
 					<th>Nombre</th>
 					<th>Minutos</th>
 					<th>Hora Inicio</th>
 					<th>Hora Fin</th>
 					<th>Usuario</th>
 					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($model as $dato)					
					<tr role="row">
						<td class="col-xs-2">{{ $dato->sigla }}</td>
						<td>{{ $dato->nombre }}</td>
						<td class="col-xs-1">{{ $dato->escala_tiempo }}</td>
						<td class="col-xs-1">{{ $dato->hora_inicio }}</td>
						<td class="col-xs-1">{{ $dato->hora_fin }}</td>
						<td class="col-xs-1">{{ $dato->usuario }}</td>
						<td  class="col-xs-1">
							<a href="{{ route('cuaderno.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="fa fa-edit"></i>
							</a>
						</td>
					</tr>					
				@endforeach
			</tbody>
		</table>
	</div> 
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#table_cuaderno').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection