<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
    <label for="especialidad">Especialidad</label>
    <select id="especialidadForm" name="especialidadForm" class="form-control">      
      @foreach($modelEspecialidad as $especialidad)
        @if($model != null)
          <option value="{{$especialidad->id}}"  {{ $model->idima_especialidad == $especialidad->id ? 'selected="selected"' : '' }} >{{$especialidad->nombre}}</option>    
        @else
           <option value="{{$especialidad->id}}">{{$especialidad->nombre}}</option> 
        @endif                      
      @endforeach 
    </select>
  </div> 
  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <br>
    <div class="checkbox">
      <label><input type="checkbox" value="" id="particular" name="particular">PARTICULAR</label> 
    </div>
  </div>
</div> 
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">         
      <label class="bmd-label">Fecha reporte</label> <br>   
      <input type="date" class="form-control datepicker" id="fechaForm" name="fechaForm">  
  </div>
</div>