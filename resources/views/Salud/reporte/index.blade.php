@extends('adminlte::page')
@section('title', 'INFORMES')
@section('content_header')
    <h1>GRAFICOS Y REPORTES ESTADISTICOS</h1>
@stop  
@section('content') 
<input id="token" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
<input type="hidden" id="data" name="data" value="{{ $model->data }}">
<input type="hidden" id="data2" name="data2" value="{{ $model->data2 }}">
<input type="hidden" id="data3" name="data3" value="{{ $model->data3 }}">
<input type="hidden" id="data4" name="data4" value="{{ $model->data4 }}">
 
    <!-- <div class="box-header">
        <h3 class="box-title">Box Tools</h3>
    </div> -->
<div class="row">
  <div class="col-12 col-sm-12">
    <div class="card card-primary card-outline card-outline-tabs">
      <div class="card-header p-0 border-bottom-0">
        <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Graficas</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Reportes e informes</a>
          </li>          
        </ul>
      </div>
      <div class="card-body">
        <div class="tab-content" id="custom-tabs-four-tabContent">
          <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
       
              <form class="navbar-form navbar-left" roles="searchNuevo" action="/hcl_reporte" autocomplete="off"> 
                <div class="row">  
                  <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">    
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">  
                    <label class="bmd-label">DESDE</label>     
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">  
                    <label class="bmd-label">HASTA</label>   
                  </div>       
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 
                  </div>        
                </div>  
                <div class="row">  
                  <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">  
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">  
                    <input type="date" class="form-control datepicker" id="dateDesde" name="dateDesde">  
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 
                    <input type="date" class="form-control datepicker" id="dateHasta" name="dateHasta"> 
                  </div>       
                  <br>
                  <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3"> 
                    <button type="submit" class="btn btn-primary">
                      <i class="fa fa-search"></i> buscar
                    </button>
                  </div>        
                </div> 
              </form>    
            <br>
            <div class="row">
              <div class="col-lg-6">
                <div class="card">
                  <!-- <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                      <h3 class="card-title">Atenciones realizadas</h3>
                      <a href="javascript:void(0);">View Report</a>
                    </div>
                  </div> -->
                  <div class="card-body">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id="spanTotal">0</span>
                        <span>Total atenciones realizadas</span>
                      </p>
                      <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> {{ $model->promedioCantidad }}
                        </span>
                        <!-- <span class="text-muted">Since last week</span> -->
                      </p>
                    </div>
                    <div id="container"></div>
                    <div class="d-flex flex-row justify-content-end">
                      <span class="mr-2">
                        <i class="fas fa-square text-primary"></i> Atenciones nuevas
                      </span>
                      <span>
                        <i class="fas fa-square text-gray"></i> Atenciones repetidas
                      </span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <!-- <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                      <h3 class="card-title">Atenciones realizadas</h3>
                      <a href="javascript:void(0);">View Report</a>
                    </div>
                  </div> -->
                  <div class="card-body">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id="spanTotal">0</span>
                        <span>Atenciones realizadas</span>
                      </p>
                      <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> 12.5%
                        </span>
                        <!-- <span class="text-muted">Since last week</span> -->
                      </p>
                    </div>
                    <div id="container3"></div> 
                  </div>
                </div>
                <!-- <div class="card">
                  <div class="card-header border-0">
                    <h3 class="card-title">Products</h3>
                    <div class="card-tools">
                      <a href="#" class="btn btn-tool btn-sm">
                      <i class="fas fa-download"></i>
                      </a>
                      <a href="#" class="btn btn-tool btn-sm">
                      <i class="fas fa-bars"></i>
                      </a>
                    </div>
                  </div>
                  <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Price</th>
                          <th>Sales</th>
                          <th>More</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Some Product
                          </td>
                          <td>$13 USD</td>
                          <td>
                          <small class="text-success mr-1">
                          <i class="fas fa-arrow-up"></i>
                          12%
                          </small>
                          12,000 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Another Product
                          </td>
                          <td>$29 USD</td>
                          <td>
                          <small class="text-warning mr-1">
                          <i class="fas fa-arrow-down"></i>
                          0.5%
                          </small>
                          123,234 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Amazing Product
                          </td>
                          <td>$1,230 USD</td>
                          <td>
                          <small class="text-danger mr-1">
                          <i class="fas fa-arrow-down"></i>
                          3%
                          </small>
                          198 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Perfect Item
                          <span class="badge bg-danger">NEW</span>
                          </td>
                          <td>$199 USD</td>
                          <td>
                          <small class="text-success mr-1">
                          <i class="fas fa-arrow-up"></i>
                          63%
                          </small>
                          87 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div> -->
              </div>
              <div class="col-lg-6">
                <div class="card">
                  <!-- <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                      <h3 class="card-title">Atenciones realizadas</h3>
                      <a href="javascript:void(0);">View Report</a>
                    </div>
                  </div> -->
                  <div class="card-body">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id="spanTotal4">0</span>
                        <span>Total atenciones realizadas</span>
                      </p> 
                    </div>
                    <div id="container4"></div> 
                  </div>
                </div>
                <div class="card">
                  <!-- <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                      <h3 class="card-title">Atenciones realizadas</h3>
                      <a href="javascript:void(0);">View Report</a>
                    </div>
                  </div> -->
                  <div class="card-body">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id="spanTotal5">0</span>
                        <span>Atenciones realizadas</span>
                      </p>
                      <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> {{ $model->promedioGenero }}
                        </span>
                        <!-- <span class="text-muted">Since last week</span> -->
                      </p>
                    </div>
                    <div id="container5"></div> 
                  </div>
                </div>
                <!-- <div class="card">
                  <div class="card-header border-0">
                    <h3 class="card-title">Products</h3>
                    <div class="card-tools">
                      <a href="#" class="btn btn-tool btn-sm">
                      <i class="fas fa-download"></i>
                      </a>
                      <a href="#" class="btn btn-tool btn-sm">
                      <i class="fas fa-bars"></i>
                      </a>
                    </div>
                  </div>
                  <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                      <thead>
                        <tr>
                          <th>Product</th>
                          <th>Price</th>
                          <th>Sales</th>
                          <th>More</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Some Product
                          </td>
                          <td>$13 USD</td>
                          <td>
                          <small class="text-success mr-1">
                          <i class="fas fa-arrow-up"></i>
                          12%
                          </small>
                          12,000 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Another Product
                          </td>
                          <td>$29 USD</td>
                          <td>
                          <small class="text-warning mr-1">
                          <i class="fas fa-arrow-down"></i>
                          0.5%
                          </small>
                          123,234 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Amazing Product
                          </td>
                          <td>$1,230 USD</td>
                          <td>
                          <small class="text-danger mr-1">
                          <i class="fas fa-arrow-down"></i>
                          3%
                          </small>
                          198 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                          <img src="dist/img/default-150x150.png" alt="Product 1" class="img-circle img-size-32 mr-2">
                          Perfect Item
                          <span class="badge bg-danger">NEW</span>
                          </td>
                          <td>$199 USD</td>
                          <td>
                          <small class="text-success mr-1">
                          <i class="fas fa-arrow-up"></i>
                          63%
                          </small>
                          87 Sold
                          </td>
                          <td>
                          <a href="#" class="text-muted">
                          <i class="fas fa-search"></i>
                          </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div> -->
              </div>              
              <div class="col-lg-6">
                <div class="card">
                  <!-- <div class="card-header border-0">
                    <div class="d-flex justify-content-between">
                      <h3 class="card-title">Sales</h3>
                      <a href="javascript:void(0);">View Report</a>
                    </div>
                  </div> -->
                  <div class="card-body">
                    <div class="d-flex">
                      <p class="d-flex flex-column">
                        <span class="text-bold text-lg" id="spanTotal2">0</span>
                        <span>Total atenciones realizadas</span>
                      </p>
                      <p class="ml-auto d-flex flex-column text-right">
                        <span class="text-success">
                        <i class="fas fa-arrow-up"></i> {{ $model->promedioCantidad }}
                        </span>
                        <!-- <span class="text-muted">Since last month</span> -->
                      </p>
                    </div>
                    <div id="container2"></div>
                    <div class="d-flex flex-row justify-content-end">
                      <span class="mr-2">
                        <i class="fas fa-square text-primary"></i> Atenciones nuevas
                      </span>
                      <span>
                        <i class="fas fa-square text-gray"></i> Atenciones repetidas
                      </span>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header border-0">
                    <h3 class="card-title">Online Store Overview</h3>
                    <div class="card-tools">
                      <a href="#" class="btn btn-sm btn-tool">
                        <i class="fas fa-download"></i>
                      </a>
                      <a href="#" class="btn btn-sm btn-tool">
                        <i class="fas fa-bars"></i>
                      </a>
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                      <p class="text-success text-xl">
                        <i class="ion ion-ios-refresh-empty"></i>
                      </p>
                      <p class="d-flex flex-column text-right">
                        <span class="font-weight-bold">
                          <i class="ion ion-android-arrow-up text-success"></i> 12%
                        </span>
                        <span class="text-muted">CONVERSION RATE</span>
                      </p>
                    </div> 
                    <div class="d-flex justify-content-between align-items-center border-bottom mb-3">
                      <p class="text-warning text-xl">
                        <i class="ion ion-ios-cart-outline"></i>
                      </p>
                      <p class="d-flex flex-column text-right">
                      <span class="font-weight-bold">
                        <i class="ion ion-android-arrow-up text-warning"></i> 0.8%
                      </span>
                      <span class="text-muted">SALES RATE</span>
                      </p>
                    </div> 
                    <div class="d-flex justify-content-between align-items-center mb-0">
                      <p class="text-danger text-xl">
                        <i class="ion ion-ios-people-outline"></i>
                      </p>
                      <p class="d-flex flex-column text-right">
                        <span class="font-weight-bold">
                        <i class="ion ion-android-arrow-down text-danger"></i> 1%
                        </span>
                        <span class="text-muted">REGISTRATION RATE</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
          </div>
          <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
            <div class="tab-pane" id="reportes">  
              <form id="form" action="/reporte" method="GET" target='_blank'>
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <?php
                    $estilo1 = 'style="color: rgb(0, 166, 90); font-weight: bold; font-size: 15px; cursor: pointer;"';
                    $estilo2 = 'style="color: rgba(42, 64, 63, 0.88); font-size: 15px; cursor: pointer;"';
                    $estilo3 = 'style="color: rgba(42, 64, 63, 0.88); font-size: 15px; cursor: pointer;"';
                    ?>
                    <h4 style="text-decoration: underline; font-weight: bold;">Lista de Reportes</h4>
                    <div id="contenedorReportes" style="height: 410px; overflow-y: scroll; border: 1px solid #222d32;">
                        <a <?=$estilo1 ?> value='1'>
                            <span class="glyphicon glyphicon-ok"></span>
                            Detalle de atenciones realizadas - Registro médico
                        </a>
                        <br>
                        <a <?=$estilo2 ?> value='2'>
                            <span class="glyphicon glyphicon-minus"></span>
                            Informe diario consulta externa - Registro médico 
                        </a>
                        <br>
                        <a <?=$estilo3 ?> value='3'>
                            <span class="glyphicon glyphicon-minus"></span>
                            Lista de registros por usuario - Modulo Fichaje
                        </a>
                        <br>
                        <a <?=$estilo2 ?> value='4'>
                            <span class="glyphicon glyphicon-minus"></span>
                            Lista de formularios registrados - Modulo MDT
                        </a>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                    <h4 style="text-decoration: underline; font-weight: bold;">Parámetros</h4> 
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" style="height: 410px; overflow-y: scroll; border: 1px solid #222d32;">
                      <div id="div1">
                        @include($model->rutaview.'detalleFormulario_lista')
                      </div>
                      <div id="div2">
                        @include($model->rutaview.'detalleForm202')
                      </div>
                      <div id="div3">
                        @include($model->rutaview.'detalleRegistros')
                      </div>
                      <div id="div4">
                        @include($model->rutaview.'detalleFormulario_mdt')
                      </div>
                      <?php
                      /*
                      <div id="div2">
                        @include('reporte.personalView')
                      </div>
                      <div id="div3">
                        @include('reporte.productoView')
                      </div>
                      <div id="div4">
                        @include('reporte.ingresoView')
                      </div>
                      */
                      ?> 
                    </div>
                  </div>
                </div> 
                <br>
                <div class="box-footer">
                  <button type='reset' class='btn btn-danger'>
                    <span class='glyphicon glyphicon-ban-circle' style='vertical-align: middle;'></span> Limpiar Campos
                  </button>
                  <button type="button" id="btnImprimir" class="btn btn-primary">
                    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Imprimir Reporte
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="tab-pane fade" id="custom-tabs-four-messages" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
            Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna.
          </div>
          <div class="tab-pane fade" id="custom-tabs-four-settings" role="tabpanel" aria-labelledby="custom-tabs-four-settings-tab">
            Pellentesque vestibulum commodo nibh nec blandit. Maecenas neque magna, iaculis tempus turpis ac, ornare sodales tellus. Mauris eget blandit dolor. Quisque tincidunt venenatis vulputate. Morbi euismod molestie tristique. Vestibulum consectetur dolor a vestibulum pharetra. Donec interdum placerat urna nec pharetra. Etiam eget dapibus orci, eget aliquet urna. Nunc at consequat diam. Nunc et felis ut nisl commodo dignissim. In hac habitasse platea dictumst. Praesent imperdiet accumsan ex sit amet facilisis.
          </div>
        </div>
      </div> 
    </div>
  </div>
</div>
     

@stop 

<div class="modal" id="vImprimeProgramacionResultado">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">REPORTE</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>            
        </div>
        <div class="modal-body">
          <div id="secccionImprimeDocumento" style="height: 410px;"></div>

      <div class="progress">
        <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        </div>
      </div>

        </div>
        <div class="modal-footer">
          <button type="button" id="yyyy" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div> 
