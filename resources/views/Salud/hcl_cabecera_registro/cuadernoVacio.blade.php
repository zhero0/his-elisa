@extends('adminlte::layouts.app')
@section('contentheader_title', 'ATENCIÓN')
@section('htmlheader_title', 'ATENCIÓN')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-primary">
      		<div class="panel-body">
      			<h4>USTED NO CUENTA CON NINGÚN CUADERNO ASIGNADO! </h4>
      			<h4>Ó </h4>
      			<h4>HA FINALIZADO LA SESIÓN Y DEBERÁ SELECCIONAR NUEVAMENTE EL CUADERNO, POR FAVOR...! </h4>
      			
      			<a href="/hcl_cabecera_registro" class="btn btn-primary">
				  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
				</a>
			</div>
		</div>
	</div>
</div>
@stop