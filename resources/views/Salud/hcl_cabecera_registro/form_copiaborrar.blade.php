<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
  <input type="hidden" id="txtHiddenJSONdatos" name="txtHiddenJSONdatos" value="{{ $model->txtHiddenJSONdatos }}">
  <input type="hidden" id="txtHidden_hcl_clasificacionsalud" value="{{ $model->hcl_clasificacionsalud }}">
  <!-- BAJA MEDICA -->
  <input type="hidden" id="txtHiddenBajaMedica" name="txtHiddenBajaMedica">
  <input type="hidden" id="txtDatosJsonBajaMedica" name="txtDatosJsonBajaMedica">
  <!-- REFERENCIA MEDICA -->
  <input type="hidden" id="txtHiddenReferenciaMedica" name="txtHiddenReferenciaMedica">
  <input type="hidden" id="txtDatosJsonReferenciaMedica" name="txtDatosJsonReferenciaMedica">
  <!-- INTERNACION MEDICA -->
  <input type="hidden" id="txtHiddenInternacionMedica" name="txtHiddenInternacionMedica">
  <input type="hidden" id="txtDatosJsonInternacionMedica" name="txtDatosJsonInternacionMedica">
  <!-- CERTIFICADO MÉDICO -->
  <input type="hidden" id="txtHiddenCertificadoMedico" name="txtHiddenCertificadoMedico">
  <!-- PRESCRIPCION RECETAS -->
  <input type="hidden" id="txtcontenidoTablaPrescripcion" name="txtcontenidoTablaPrescripcion" value="{{ $model->documentodetallePrescripcion }}">
  <!-- DIAGNOSTICOS -->
  <input type="hidden" id="txtcontenidoTablaDiagnosticos" name="txtcontenidoTablaDiagnosticos" value="{{ $model->documentodetalleDiagnostico }}">

  <input type="hidden" id="txtcontenidoEstablecimiento" name="txtcontenidoEstablecimiento" value="{{ $model->documentodetalleEstablecimiento }}">
  <input type="hidden" id="txtcontenidoCuaderno" name="txtcontenidoCuaderno" value="{{ $model->documentodetalleCuaderno }}">  


  <div class="row" id="divContenedorDatos_paciente" style="display: none;">
    <div class="col-xs-12 col-sm-10 col-md-7 col-lg-7">
      <div class="card">
        <div class="card-body"> 
          <span class="username">
            <a href="#">Registro de la historia clinica digital</a>
            <p>Datos del paciente.</p>
          </span>           
          <div class="col-2">  
            <h4><i class="fa fa-user"> </i> <span id="spanNroAsegurado"></span></h4>
          </div>  
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <p class="text-muted well well-sm shadow-none" id="nombrePaciente" style="margin: 1px;"></p>
              <address>
              <strong>Paciente, AVC</strong><br>
              <strong>Edad: </strong><span class="description-text" id="spanEdad"></span><br>
              Documento: <span class="description-text" id="spanDocumento"></span><br>
              Sexo: <span class="description-text" id="spanSexo"></span><br>
              <!-- Email: info@almasaeedstudio.com -->
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              To
              <address>
              <strong>Empleador</strong><br>
              Activo<br>
              Ocupación: <br>
              Phone: (555) 539-1037<br>
              <!-- Email: john.doe@example.com -->
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              <b>Invoice #007612</b><br>
              <br>
              <b>Order ID:</b> <input id="txtidhcl_poblacion" name="txtidhcl_poblacion" readonly=""><br>
              <b>Fecha Registro:</b> 2/22/2014<br>
              <b>Medico:</b> Dr. Sanchez
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="col-xs-12 col-sm-10 col-md-5 col-lg-5">
      <div class="card">
        <div class="card-body"> 
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="../imagenes/farmacia_icono.jpg" alt="user image">
            <span class="username">
              <a href="#">Plataforma de atención a realizar</a>
            </span>
            <span class="description">Seleccione el tipo de atencion a realizarse.</span>
          </div> 
          <br>  
          <br>  
          <!-- timeline "TIPOS ATENCION" --> 
            @include($model->rutaview.'opcTipoAtencion')              
          <!-- END "TIPOS ATENCION" -->
        </div>
      </div>
    </div>
  </div>

  <!-- timeline "BUSCADOR DE PACIENTES" --> 
  <div class="row" id="divDatosPaciente" style="display: block;">     
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      <div class="input-group">
        <input type="text" id="txtBuscadorPaciente" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;" autofocus="true">
        <span class="input-group-btn">
          <button class="btn btn-warning" type="button" id="btnBuscarPaciente">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            Buscar
          </button>
        </span>
      </div>

      <div class="table-responsive" style="height: 400px; overflow-y: auto; border: 1px solid #374850;">
        <table id="tablaContenedorDatosPaciente" class="table table-striped">
          <thead style="{{ config('app.styleTableHead') }}">
            <tr>
              <th class="col-xs-2">Nro Historia</th>
              <th class="col-xs-4">Nombres</th>
                <th>Apellidos</th>
                <th></th>
           </tr>
          </thead>
          <tbody id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
            <tr id="idListaVaciaPaciente">
              <td>Lista Vacia...</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- END "BUSCADOR DE PACIENTES" -->


  <div class="progress" id="divProgressBar" style="display: none;">
  <!-- <div class="progress" id="divProgressBar" style="display: block;"> -->
    <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
    </div>
  </div> 


  <div class="row" id="divContenedorDatos_paciente" style="display: none;">
    <div class="col-xs-12 col-sm-10 col-md-7 col-lg-7">
      <div class="card">
        <div class="card-body"> 
          <span class="username">
            <a href="#">Registro de la historia clinica digital</a>
            <p>Datos del paciente.</p>
          </span>           
          <div class="col-2">  
            <h4><i class="fa fa-user"> </i> <span id="spanNroAsegurado"></span></h4>
          </div>  
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <p class="text-muted well well-sm shadow-none" id="nombrePaciente" style="margin: 1px;"></p>
              <address>
              <strong>Paciente, AVC</strong><br>
              <strong>Edad: </strong><span class="description-text" id="spanEdad"></span><br>
              Documento: <span class="description-text" id="spanDocumento"></span><br>
              Sexo: <span class="description-text" id="spanSexo"></span><br>
              <!-- Email: info@almasaeedstudio.com -->
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              To
              <address>
              <strong>Empleador</strong><br>
              Activo<br>
              Ocupación: <br>
              Phone: (555) 539-1037<br>
              <!-- Email: john.doe@example.com -->
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              <b>Invoice #007612</b><br>
              <br>
              <b>Order ID:</b> <input id="txtidhcl_poblacion" name="txtidhcl_poblacion" readonly=""><br>
              <b>Fecha Registro:</b> 2/22/2014<br>
              <b>Medico:</b> Dr. Sanchez
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="col-xs-12 col-sm-10 col-md-5 col-lg-5">
      <div class="card">
        <div class="card-body"> 
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="../imagenes/farmacia_icono.jpg" alt="user image">
            <span class="username">
              <a href="#">Plataforma de atención a realizar</a>
            </span>
            <span class="description">Seleccione el tipo de atencion a realizarse.</span>
          </div> 
          <br>  
          <br>  
          <!-- timeline "TIPOS ATENCION" --> 
            @include($model->rutaview.'opcTipoAtencion')              
          <!-- END "TIPOS ATENCION" -->
        </div>
      </div>
    </div>
  </div>
  <div class="row" id="divContenedorSalud" style="display: none;">
    <div class="container-fluid">
      <div class="row"> 
        <div class="col-12">
          <div class="callout callout-info" style="display: none;">
            <h5><i class="fas fa-info" ></i> Note:</h5>
            This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
          </div> 
          <div class="invoice p-3 mb-3">                
            
            <div class="row">
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header p-2">
                    <ul class="nav nav-pills">
                      <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Dashboard</a></li>
                      <li class="nav-item"><a class="nav-link" id="tabHistoriaClinico" href="#historiaClinica" data-toggle="tab">Historial Clinico</a></li>
                      <li class="nav-item"><a class="nav-link" href="#farmacia" id="taFarmacia" data-toggle="tab">Prescripción Medica</a></li>
                    </ul>
                  </div>
                  <div class="card-body">
                    <div class="tab-content">
                      <div class="active tab-pane" id="activity"> 
                        <!-- timeline "SIGNOS VITALES" -->
                        <div class="post">
                          <div class="user-block">
                            <img src="/uploads/pob_afiliacion/corazon.jpg" alt="user-avatar" class="img-circle img-fluid">
                            <span class="username">
                              <h5 class="list-group-item-heading text-primary"> 
                                Registro de Signos Vitales
                              </h5> 
                              <!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
                            </span>
                          <span class="description">Ritmo cardiaco, frecuencia respiratorio, temperatura, presión arterial.</span>
                          </div>
                          <div class="box-header with-border">
                            @include($model->rutaview.'opcSignosVitales')
                          </div>   
                        </div> 
                        <!-- END timeline "SIGNOS VITALES" --> 
                        <!-- timeline DIAGNOSTICOS -->
                        <div class="post clearfix">
                          <div class="user-block">
                            <img src="/uploads/pob_afiliacion/registrar_diagnostico.jpg" alt="user-avatar" class="img-circle img-fluid">
                            <span class="username">
                              <h5 class="list-group-item-heading text-primary"> 
                                Registrar Diagnóstico
                              </h5>
                            </span>
                            <span class="description">1-Diagnostico presuntivo/2-Diagnosticos de base</span>
                          </div> 
                            @include($model->rutaview.'opcDiagnostico') 
                        </div>
                        <!-- END timeline DIAGNOSTICOS --> 
                       
                        <!-- timeline DESCRIPCION DE DIAGNOSTICO -->
                        <div class="post">
                          <div class="user-block">
                            <img src="/uploads/pob_afiliacion/tipo_atencion.jpg" alt="user-avatar" class="img-circle img-fluid">
                            <span class="username">
                              <h5 class="list-group-item-heading text-primary"> 
                                Descripción diagnostico
                              </h5>
                            </span>
                            <span class="description">Posted 5 photos - 5 days ago</span>
                          </div> 
                            @include($model->rutaview.'opcDescripcionDiagnostico') 
                        </div> 
                        <!-- END timeline DESCRIPCION DE DIAGNOSTICO --> 

                        <!-- timeline "EXAMENES COMPLEMENTARIOS" -->
                        <div class="post">
                          <div class="user-block">
                            <img src="/uploads/pob_afiliacion/tipo_atencion.jpg" alt="user-avatar" class="img-circle img-fluid">
                            <span class="username">
                              <h5 class="list-group-item-heading text-primary"> 
                                Examenes complementarios
                              </h5>
                            </span>
                            <span class="description">Posted 5 photos - 5 days ago</span>
                          </div> 
                           <div class="row">
                            <div class="col-lg-12">
                              @include($model->rutaview.'opcExamenComplementario')
                            </div>  
                          </div> 
                        </div>  
                        <!-- END timeline "EXAMENES COMPLEMENTARIOS" -->  

                      </div>                     
                      <!-- ============================== [ "Historia Clínico" ] ============================== -->
                      <div class="tab-pane" id="historiaClinica"> 
                        <div class="timeline timeline-inverse"> 
                          <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                              <div class='input-group date' id='datetimepickerDesde'>
                                  <input type='text' class="form-control" id="fechaDesde_historial" value="{{ $model->fechaDesde }}">
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                              <div class='input-group date' id='datetimepickerHasta'>
                                  <input type='text' class="form-control" id="fechaHasta_historial" value="{{ $model->fechaHasta }}">
                                  <span class="input-group-addon">
                                      <span class="glyphicon glyphicon-calendar"></span>
                                  </span>
                              </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                              <div class="input-group input-group-xs">
                                <input type="text" id="txtBuscarHistorial" class="form-control" placeholder="Escriba...">
                                <span class="input-group-btn">
                                  <button class="btn btn-primary" type="button" id="btnBuscarHistorialClinico" title="Click para Buscar">
                                    <span class="fa fa-search" aria-hidden="true"></span>
                                    Buscar
                                  </button>
                                </span>
                              </div>
                            </div>
                          </div>
                          <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
                            <i class="fas fa-envelope bg-primary"></i>
                            <div class="timeline-item" id="divContenedor_HistoriaCilnico"> 
                            </div> 
                          </div>
                          <!-- <div class="time-label">
                            <span class="bg-danger">
                            10 Feb. 2014
                            </span>
                          </div> 
                          <div>
                            <i class="fas fa-envelope bg-primary"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="far fa-clock"></i> 12:05</span>
                              <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
                              <div class="timeline-body">
                                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                quora plaxo ideeli hulu weebly balihoo...
                              </div>
                              <div class="timeline-footer">
                                <a href="#" class="btn btn-primary btn-sm">Read more</a>
                                <a href="#" class="btn btn-danger btn-sm">Delete</a>
                              </div>
                            </div>
                          </div> 
                          <div>
                            <i class="fas fa-user bg-info"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="far fa-clock"></i> 5 mins ago</span>
                              <h3 class="timeline-header border-0"><a href="#">Sarah Young</a> accepted your friend request
                              </h3>
                            </div>
                          </div>
                          <div>
                            <i class="fas fa-comments bg-warning"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="far fa-clock"></i> 27 mins ago</span>
                              <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>
                              <div class="timeline-body">
                                Take me to your leader!
                                Switzerland is small and neutral!
                                We are more like Germany, ambitious and misunderstood!
                              </div>
                              <div class="timeline-footer">
                                <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
                              </div>
                            </div>
                          </div>
                          <div class="time-label">
                            <span class="bg-success">
                              3 Jan. 2014
                            </span>
                          </div>
                          <div>
                            <i class="fas fa-camera bg-purple"></i>
                            <div class="timeline-item">
                              <span class="time"><i class="far fa-clock"></i> 2 days ago</span>
                              <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>
                              <div class="timeline-body">
                                <img src="https://placehold.it/150x100" alt="...">
                                <img src="https://placehold.it/150x100" alt="...">
                                <img src="https://placehold.it/150x100" alt="...">
                                <img src="https://placehold.it/150x100" alt="...">
                              </div>
                            </div>
                          </div>
                          <div>
                            <i class="far fa-clock bg-gray"></i>
                          </div> -->
                        </div>
                      </div> 
                      <!-- ============================ [ FIN "Historia Clínico" ] ============================ -->
                      <div class="tab-pane" id="farmacia">
                        <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
                          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class='input-group date' id='datetimepickerDesde'>
                                <input type='text' class="form-control" id="fechaDesde_prescripcion" value=" ">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                            <div class='input-group date' id='datetimepickerHasta'>
                                <input type='text' class="form-control" id="fechaHasta_prescripcion" value=" ">
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                          </div>
                          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <div class="input-group input-group-xs">
                              <input type="text" id="txtBuscar_prescripcion" class="form-control" placeholder="Escriba...">
                              <span class="input-group-btn">
                                <button class="btn btn-primary" type="button" id="btnBuscarPrescripcion" title="Click para Buscar">
                                  <span class="fa fa-search" aria-hidden="true"></span>
                                  Buscar
                                </button>
                              </span>
                            </div>
                          </div>
                        </div>

                        <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
                          
                          <div class="card-body">
                                <table id="tablaContenedorPrescripcion" class="table table-striped">
                                    <thead style="{{ config('app.styleTableHead') }}">
                                        <tr>                            
                                          <th class="col-xs-1">FECHA</th>                            
                                          <th class="col-xs-1">CODIGO</th>
                                          <th>MEDICAMENTO/INSUMO</th>   
                                          <th class="col-xs-1">CANT.</th>                            
                                          <th class="col-xs-2">ESP.</th>
                                          <th class="col-xs-3">MEDICO</th>
                                          <th class="col-xs-1">TIPO</th> 
                                      </tr> 
                                  </thead>
                                  <tbody id="tbodyPrescripcionLista" style="{{ config('app.styleTableRow') }}">
                                    <tr>
                                      <td>Vacío...</td>
                                    </tr>
                                  </tbody>
                              </table>
                            </div>
                        </div>
                        <!-- <form class="form-horizontal">
                          <div class="form-group row">
                            <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                              <input type="email" class="form-control" id="inputName" placeholder="Name">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                              <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputName2" placeholder="Name">
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                            <div class="col-sm-10">
                              <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                            </div>
                          </div>
                          <div class="form-group row">
                            <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                              <div class="checkbox">
                                <label>
                                  <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                </label>
                              </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="offset-sm-2 col-sm-10">
                              <button type="submit" class="btn btn-danger">Submit</button>
                            </div>
                          </div>
                        </form> -->
                      </div> 
                    </div> 
                  </div>
                </div> 
              </div>
            </div>
            <!-- <div class="row">
              <div class="col-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Qty</th>
                      <th>Product</th>
                      <th>Serial #</th>
                      <th>Description</th>
                      <th>Subtotal</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Call of Duty</td>
                      <td>455-981-221</td>
                      <td>El snort testosterone trophy driving gloves handsome</td>
                      <td>$64.50</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>Need for Speed IV</td>
                      <td>247-925-726</td>
                      <td>Wes Anderson umami biodiesel</td>
                      <td>$50.00</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>Monsters DVD</td>
                      <td>735-845-642</td>
                      <td>Terry Richardson helvetica tousled street art master</td>
                      <td>$10.70</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>Grown Ups Blue Ray</td>
                      <td>422-568-642</td>
                      <td>Tousled lomo letterpress</td>
                      <td>$25.99</td>
                    </tr>
                  </tbody>
                </table>
              </div> 
            </div> -->
            <!-- <div class="row"> 
              <div class="col-6">
                <p class="lead">Payment Methods:</p>
                <img src="../../dist/img/credit/visa.png" alt="Visa">
                <img src="../../dist/img/credit/mastercard.png" alt="Mastercard">
                <img src="../../dist/img/credit/american-express.png" alt="American Express">
                <img src="../../dist/img/credit/paypal2.png" alt="Paypal">
                <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>
              </div> 
              <div class="col-6">
                <p class="lead">Amount Due 2/22/2014</p>
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>$250.30</td>
                      </tr>
                      <tr>
                        <th>Tax (9.3%)</th>
                        <td>$10.34</td>
                      </tr>
                      <tr>
                        <th>Shipping:</th>
                        <td>$5.80</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>$265.24</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div> 
            </div> --> 
            <!-- <div class="row no-print">
              <div class="col-12">
                <a href="invoice-print.html" rel="noopener" target="_blank" class="btn btn-default"><i class="fas fa-print"></i>Print
                </a>
                <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                Payment 
                </button>
                <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                <i class="fas fa-download"></i> Generate PDF
                </button>
              </div>
            </div> -->
          </div> 
        </div>
      </div>
    </div>
  </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardar" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif