<!-- VENTANA MODAL PARA REGISTRAR "BAJA MÉDICA" -->
<h5 class="list-group-item-heading text-primary"> 
	BAJA MÉDICA
</h5> 
<div class="form-group">
	<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
		<input type="checkbox" class="custom-control-input" id="checkOpcion">
		<label class="custom-control-label" for="checkOpcion">Habilite la escritura</label>
	</div>
</div>
   
<form action="#" id="form_BajaMedica" method="post" autocomplete="off">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-2 col-lg-4">
			<div class="form-group">
				<label>Fecha Desde:</label>
				<div class="input-group date" id="datetimepicker" data-target-input="nearest">
					<input type="text" id="fechaDesde" class="form-control datetimepicker-input" data-target="#datetimepicker">
					<div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					</div>
				</div>
			</div> 
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-3">
            <label>Cantidad(días)</label>
            <input type='text' class="form-control" id="txtCantidadBajaMedica">
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-4"> 
        	<div class="form-group">
				<label>Fecha Hasta:</label>
				<div class="input-group date" id="datetimepickerHasta" data-target-input="nearest">
					<input type="text" id="fechaHasta" class="form-control datetimepicker-input" data-target="#datetimepickerHasta">
					<div class="input-group-append" data-target="#datetimepickerHasta" data-toggle="datetimepicker">
						<div class="input-group-text"><i class="fa fa-calendar"></i></div>
					</div>
				</div>
			</div> 
        </div> 
	</div>

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
            <label>Baja Médica</label>
            <select id="txtIdHcl_variables_bajas" class="form-control">
              <option value="0">Seleccione! </option>
                @foreach($modelHcl_variables_bajas as $dato)
                  @if($model->iddatosgenericos == $dato->id)
                     <option value="{{$dato->id}}" selected>{{ $dato->nombre}}</option> 
                  @else
                    <option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
                  @endif
                @endforeach
            </select>
      	</div>
    </div>

    <label>Descripción</label>
	<textarea class="form-control" id="taDescripcion_formato_historia" rows="3"></textarea>
</form>
<h4 style="color: red; visibility: hidden;" id="textoValidacion_BajaMedica">COMPLETE LOS DATOS, POR FAVOR! </h4> 