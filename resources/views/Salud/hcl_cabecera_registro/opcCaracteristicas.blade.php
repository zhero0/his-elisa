<!-- VENTANA MODAL PARA REGISTRAR "CERTIFICADO MÉDICO" -->
<div class="auto" id="divCertificadoMedico" style="display: none"> 
  <div class="modal-dialog modal-md">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" 
              data-dismiss="modal" 
              aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="tituloModal">
              CERTIFICADO MÉDICO
            </h4>

            <div class="onoffswitch">              
              <input type="checkbox" class="onoffswitch-checkbox" id="checkCertificadoMedico">
              <label class="onoffswitch-label" for="checkCertificadoMedico">
                  <span class="onoffswitch-inner"></span>
                  <span class="onoffswitch-switch"></span>
              </label>
            </div>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <form action="#" id="form_CertificadoMedico" method="post" autocomplete="off">
                      <label>Descripción</label>
                  <textarea class="form-control" id="taDescripcion_CertificadoMedico" rows="9" placeholder="Esbriba..."></textarea>
                </form>
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
              <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
              Cerrar
            </button>
        </div>
      </div>
  </div>
</div>