@foreach($model as $dato)
<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
    <div class="card bg-light d-flex flex-fill">
        <div class="card-header text-muted border-bottom-0">
            CNS
        </div>
        <div class="card-body pt-0">
            <div class="row">
                <div class="col-7">
                <p class="text-muted text-sm"><b><h4>AVC: </b> {{ $dato->hcl_poblacion_matricula }} </h4></p>
                <h2 class="lead"><b>{{ $dato->hcl_poblacion_nombrecompleto }}</b></h2> 
                    <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small">
                            <span class="fa-li"><i class="fas fa-lg fa-building"></i>
                            </span> Edad: {{ $dato->edad }} Años
                        </li>
                        <li class="small">
                            <span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Hora de Atención: {{ $dato->hora_registro }}
                        </li>
                    </ul>
                </div>
                <div class="col-5 text-center">
                    <img src="/uploads/pob_afiliacion/default.jpg" alt="user-avatar" class="img-circle img-fluid"> 
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="text-right">
                <!-- <a href="#" class="btn btn-sm bg-teal">
                    <i class="fas fa-comments"></i>
                </a> --> 
                    <a href="{{ route('hcl_cabecera_registro.edit', $dato->id) }}" class="btn btn-outline-info btn-sm" rel="tooltip" title="Editar">
                        <i class="fa fa-edit"></i>
                    </a> 
                <button type="button" onclick="imprimirSolicitudes( {{ $dato->id }} )" class="btn btn-outline-success btn-sm" rel="tooltip" title="Opciones Reporte Salud">
                    <i class="fa fa-print"></i>
                </button>
            </div>
        </div>
    </div> 
</div> 
@endforeach