<input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">
 
	<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	  <h4 class="list-group-item-heading text-primary">
	    <i class="fa fa-fw fa-heartbeat"></i>
	    Descripción
	  </h4> 
	</div> -->
	<textarea class="form-control"  id="desc_historial" rows="20" style="height: 400px; overflow-y: auto;" name="taDescripcion_formato_historia" >{{ $model->taDescripcion_formato_historia }}</textarea> 
  <br>
  <div class="row float-right">  
    <div class="form-group">
      <div class="btn btn-default btn-file" id="btnBuscarPlantilla" title="Seleccionar una plantilla de historial clinico.">
        <i class="far fa-file-alt"></i> Plantilla medica
      </div>
      <div class="btn btn-default btn-file float-right" id="btnRegistrarPlantilla"  title="Registrar nueva plantilla medica">
        <i class="fas fa-paperclip"></i> + 
      </div>
      <p class="help-block">Listado de plantillas medicas.</p>
    </div> 
  </div>
<br>
<div class="row">  
  @if($model->nivel_seguridad == 1)
    <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
      <h4 class="list-group-item-heading text-primary">
        <i class="fa fa-lock"></i>
        Seguridad
      </h4>
      <div class="onoffswitch">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkRespuesta_seguro">
        <label class="onoffswitch-label" for="checkRespuesta_seguro">
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </div>
  @endif
</div>
 