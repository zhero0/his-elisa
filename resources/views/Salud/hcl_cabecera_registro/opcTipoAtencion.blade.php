<ul class="nav nav-pills flex-column">
  <li class="nav-item active">
    <a href="#" class="nav-link">
      <i class="fas fa-inbox"></i> Atención en curso
      <label class="radio-inline float-right"><input type="radio" name="tipo_consulta" value="2">Consulta medica repetida</label>
      <label class="radio-inline float-right"><input type="radio" name="tipo_consulta" value="1" checked="">Iniciar nueva consulta</label>
    </a>
  </li>
  <li class="nav-item">
    <a href="#" class="nav-link">
      <span class="badge bg-danger float-middle">1</span>
      <i class="far fa-file-alt"></i> Emergencia
      <span class="badge bg-danger float-right"></span>
      <div class="onoffswitch float-right">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkEmergencia">
        <label class="onoffswitch-label" for="checkEmergencia"> Seleccionar estado en emergencia
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </a> 
  </li>
  <li class="nav-item">
    <a href="#" class="nav-link">
    <span class="badge bg-warning float-middle">2</span>
      <i class="far fa-envelope"></i> Hospitalización

      <div class="onoffswitch float-right">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkHospitalizado">
        <label class="onoffswitch-label" for="checkHospitalizado"> Seleccionar paciente hospitalizado
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </a>
  </li> 
  <li class="nav-item">
    <a href="#" class="nav-link">
      <span class="badge bg-success float-middle">3</span>
      <i class="fas fa-filter"></i> Embarazo 
      <div class="onoffswitch float-right">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkEmbarazo">
        <label class="onoffswitch-label" for="checkEmbarazo"> Seleccionar paciente en gestación
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </a>
  </li> 
</ul>