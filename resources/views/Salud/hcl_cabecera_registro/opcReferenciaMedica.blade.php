<!-- VENTANA MODAL PARA REGISTRAR "REFERENCIA MÉDICA" -->
<h5 class="list-group-item-heading text-primary"> 
	REFERENCIA MÉDICA
</h5> 
<div class="form-group">
	<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
		<input type="checkbox" class="custom-control-input" id="checkReferenciaMedica">
		<label class="custom-control-label" for="checkReferenciaMedica">Habilite la solicitud de referencia</label>
	</div>
</div>
<div id="divPanel_ref_show" style="display: none;"> 
	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
	        <label>Establecimiento</label>
	        <select id="txtIdAlmacen" class="form-control">
	          	<option value="0">Seleccione! </option>
	          	@foreach($modelEstablecimiento as $dato)
	            	<option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
	            @endforeach  
	        </select>
	  	</div>
	  	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
	        <label>Especialidad</label>
	        <select id="txtIdHcl_cuaderno" class="form-control">
	          <option value="0">Seleccione! </option> 
	          	@foreach($modelEspecialidad as $dato)
	            	<option value="{{$dato->id}}">{{ $dato->nombre }}</option> 
	            @endforeach 
	        </select>
	  	</div>
	</div>
	<label>RESUMEN DE ANANMESIS Y EXAMEN CLINICO</label>
	<textarea class="form-control" id="taDescripcion_examen_clinico" rows="3"></textarea>
	<label>TRATAMIENTO</label>
	<textarea class="form-control" id="taDescripcion_tratamiento_inicial" rows="3"></textarea>
	<label>RECOMENDACIONES</label>
	<textarea class="form-control" id="taDescripcion_recomendaciones" rows="3"></textarea>
	<h4 style="color: red; visibility: hidden;" id="textoValidacion_ReferenciaMedica">COMPLETE LOS DATOS, POR FAVOR! </h4>
</div> 