<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
  <input type="hidden" id="txtHiddenJSONdatos" name="txtHiddenJSONdatos" value="{{ $model->txtHiddenJSONdatos }}">
  <input type="hidden" id="txtHidden_hcl_clasificacionsalud" value="{{ $model->hcl_clasificacionsalud }}">
  <!-- BAJA MEDICA -->
  <input type="hidden" id="txtHiddenBajaMedica" name="txtHiddenBajaMedica">
  <input type="hidden" id="txtDatosJsonBajaMedica" name="txtDatosJsonBajaMedica">
  <!-- REFERENCIA MEDICA -->
  <input type="hidden" id="txtHiddenReferenciaMedica" name="txtHiddenReferenciaMedica">
  <input type="hidden" id="txtDatosJsonReferenciaMedica" name="txtDatosJsonReferenciaMedica">
  <!-- INTERNACION MEDICA -->
  <input type="hidden" id="txtHiddenInternacionMedica" name="txtHiddenInternacionMedica">
  <input type="hidden" id="txtDatosJsonInternacionMedica" name="txtDatosJsonInternacionMedica">
  <!-- CERTIFICADO MÉDICO -->
  <input type="hidden" id="txtHiddenCertificadoMedico" name="txtHiddenCertificadoMedico">
  <!-- PRESCRIPCION RECETAS -->
  <input type="hidden" id="txtcontenidoTablaPrescripcion" name="txtcontenidoTablaPrescripcion" value="{{ $model->documentodetallePrescripcion }}">
  <!-- DIAGNOSTICOS -->
  <input type="hidden" id="txtcontenidoTablaDiagnosticos" name="txtcontenidoTablaDiagnosticos" value="{{ $model->documentodetalleDiagnostico }}">

  <input type="hidden" id="txtcontenidoEstablecimiento" name="txtcontenidoEstablecimiento" value="{{ $model->documentodetalleEstablecimiento }}">
  <input type="hidden" id="txtcontenidoCuaderno" name="txtcontenidoCuaderno" value="{{ $model->documentodetalleCuaderno }}">  



  <div class="row" id="divDatosPaciente" style="display: block;">
  <!-- <div class="row" id="divDatosPaciente" style="display: none;"> -->
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
      <div class="input-group">
        <input type="text" id="txtBuscadorPaciente" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;" autofocus="true">
        <span class="input-group-btn">
          <button class="btn btn-warning" type="button" id="btnBuscarPaciente">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            Buscar
          </button>
        </span>
      </div>

      <div class="table-responsive" style="height: 400px; overflow-y: auto; border: 1px solid #374850;">
        <table id="tablaContenedorDatosPaciente" class="table table-bordered table-hover table-condensed">
          <thead style="{{ config('app.styleTableHead') }}">
            <tr>
              <th class="col-xs-2">Nro Historia</th>
              <th class="col-xs-4">Nombres</th>
                <th>Apellidos</th>
                <th></th>
           </tr>
          </thead>
          <tbody class="table-success" id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
            <tr id="idListaVaciaPaciente">
              <td>Lista Vacia...</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <div class="progress" id="divProgressBar" style="display: none;">
  <!-- <div class="progress" id="divProgressBar" style="display: block;"> -->
    <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
    </div>
  </div>


  <div class="row" id="divContenedorSalud" style="display: none;">
    
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#hojaEvolucion" data-toggle="tab" aria-expanded="true">Hoja de Evolución</a></li>
          <li class="">
            <a href="#historiaClinica" data-toggle="tab" aria-expanded="false" id="tabHistoriaClinico" style="cursor: pointer;">Historia Clínico</a>
          </li>
          <li class="">
            <a href="#farmacia" data-toggle="tab" aria-expanded="false" id="taFarmacia" style="cursor: pointer;">Prescripciones</a>
          </li>
          <!-- <li class="">
            <a href="#imagenologia" data-toggle="tab" aria-expanded="false" id="taImagenologia" style="cursor: pointer;">Imagenología</a>
          </li>
          <li class="">
            <a href="#laboratorio" data-toggle="tab" aria-expanded="false" id="taLaboratorio" style="cursor: pointer;">Laboratorio</a>
          </li>
          <li class="">
            <a href="#dicom" data-toggle="tab" aria-expanded="false" id="taDicom" style="cursor: pointer;">Dicom</a>
          </li> -->
        </ul>
        <div class="tab-content">
          <!-- ============================== [ "Hoja de Evolución" ] ============================== -->
          <div class="tab-pane active" id="hojaEvolucion">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-widget widget-user">
                  <div class="widget-user-header bg-aqua-active">
                    <input type="hidden" id="txtidhcl_poblacion" name="txtidhcl_poblacion">
                    <h3 class="widget-user-username" id="nombrePaciente">Juan Perez</h3>
                    N° Seguro <h5 class="widget-user-desc" id="spanNroAsegurado"></h5>
                  </div>
                  <div class="widget-user-image">
                    <img class="img-circle" src="/uploads/pob_afiliacion/default.jpg" alt="User Avatar">
                  </div>
                  <div class="box-footer">
                    <div class="row">
                      <div class="col-sm-4 border-right">
                        <div class="description-block">
                          <h5 class="description-header">EDAD/Años</h5>
                          <span class="description-text" id="spanEdad"></span>
                        </div>
                      </div>
                      <div class="col-sm-4 border-right">
                        <div class="description-block">
                          <h5 class="description-header">DOCUMENTO</h5>
                          <span class="description-text" id="spanDocumento"></span>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="description-block">
                          <h5 class="description-header">SEXO</h5>
                          <span class="description-text" id="spanSexo"></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="row">
              <ul class="timeline">
                <!-- timeline "SIGNOS VITALES" -->
                <li>
                 <i class="fa fa-sticky-note-o bg-green"></i>
                  <div class="timeline-item">
                    <div class="timeline-body">
                      <div class="box box-success">
                        <div class="box-header with-border">
                          @include($model->rutaview.'opcSignosVitales')
                        </div> 
                      </div>
                    </div> 
                  </div>
                </li>
                <!-- END timeline "SIGNOS VITALES" --> 


                <!-- timeline DIAGNOSTICOS -->
                <li>
                  <i class="fa fa-user bg-blue"></i>
                  <div class="timeline-item">
                    <div class="timeline-body">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          @include($model->rutaview.'opcDiagnostico')
                        </div>
                      </div>
                  </div>
                </li>
                <!-- END timeline DIAGNOSTICOS -->


                <!-- timeline "TIPOS ATENCION" -->
                <li>
                  <i class="fa  fa-bars bg-red"></i>
                  <div class="timeline-item">
                    <div class="timeline-body">
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          @include($model->rutaview.'opcTipoAtencion')
                          <br>
                        </div> 
                      </div>
                    </div> 
                  </div>
                </li>
                <!-- END "TIPOS ATENCION" --> 


                <!-- timeline DESCRIPCION DE DIAGNOSTICO -->
                <li>
                  <i class="fa  fa-bars bg-maroon"></i>
                  <div class="timeline-item">
                    <div class="timeline-body">
                      <div class="box box-success">
                        <div class="box-header with-border">
                          @include($model->rutaview.'opcDescripcionDiagnostico')
                        </div> 
                      </div>
                    </div> 
                  </div>
                </li>
                <!-- END timeline DESCRIPCION DE DIAGNOSTICO --> 


                <!-- timeline "EXAMENES COMPLEMENTARIOS" -->
                <li>
                  <i class="fa  fa-bars bg-red"></i>
                  <div class="timeline-item">
                    <div class="timeline-body">
                      <div class="box box-danger">
                        <div class="box-header with-border">
                          @include($model->rutaview.'opcExamenComplementario')
                        </div> 
                      </div>
                    </div> 
                  </div>
                </li>
                <!-- END timeline "EXAMENES COMPLEMENTARIOS" -->

                <li>
                  <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
                </li>
              </ul>  
            </div>
          </div>
          <!-- ============================ [ FIN "Hoja de Evolución" ] ============================ -->


          <!-- ============================== [ "Historia Clínico" ] ============================== -->
          <div class="tab-pane" id="historiaClinica">
            <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerDesde'>
                    <input type='text' class="form-control" id="fechaDesde_historial" value="{{ $model->fechaDesde }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerHasta'>
                    <input type='text' class="form-control" id="fechaHasta_historial" value="{{ $model->fechaHasta }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="input-group input-group-xs">
                  <input type="text" id="txtBuscarHistorial" class="form-control" placeholder="Escriba...">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" id="btnBuscarHistorialClinico" title="Click para Buscar">
                      <span class="fa fa-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
              <ul class="timeline timeline-inverse" id="divContenedor_HistoriaCilnico"></ul>
            </div>
          </div>
          <!-- ============================ [ FIN "Historia Clínico" ] ============================ -->

          
          <!-- =============================== [ "Prescripciones" ] =============================== -->
          <div class="tab-pane" id="farmacia">
            <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerDesde'>
                    <input type='text' class="form-control" id="fechaDesde_prescripcion" value=" ">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerHasta'>
                    <input type='text' class="form-control" id="fechaHasta_prescripcion" value=" ">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="input-group input-group-xs">
                  <input type="text" id="txtBuscar_prescripcion" class="form-control" placeholder="Escriba...">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" id="btnBuscarPrescripcion" title="Click para Buscar">
                      <span class="fa fa-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
              <ul class="timeline timeline-inverse" id="divContenedor_Prescripcion"></ul>
              <div class="table-responsive" style="max-height: 600px; overflow-y: auto; border: 1px solid #374850;">
                    <table id="tablaContenedorPrescripcion" class="table table-bordered table-hover table-condensed">
                        <thead style="{{ config('app.styleTableHead') }}">
                            <tr>                            
                              <th class="col-xs-1">FECHA</th>                            
                              <th class="col-xs-1">CODIGO</th>
                              <th>MEDICAMENTO/INSUMO</th>   
                              <th class="col-xs-1">CANT.</th>                            
                              <th class="col-xs-2">ESP.</th>
                              <th class="col-xs-3">MEDICO</th>
                              <th class="col-xs-1">TIPO</th> 
                          </tr> 
                      </thead>
                      <tbody class="table-success" id="tbodyPrescripcionLista" style="{{ config('app.styleTableRow') }}">
                        <tr>
                          <td>Vacío...</td>
                        </tr>
                      </tbody>
                  </table>
                </div>
            </div>
          </div>
          <!-- ============================= [ FIN "Prescripciones" ] ============================= -->


          <!-- =============================== [ "Imagenología" ] =============================== -->
          <div class="tab-pane" id="imagenologia">
            <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerDesde'>
                    <input type='text' class="form-control" id="fechaDesde_imagenologia" value="{{ $model->fechaDesde }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerHasta'>
                    <input type='text' class="form-control" id="fechaHasta_imagenologia" value="{{ $model->fechaHasta }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="input-group input-group-xs">
                  <input type="text" id="txtBuscar_imagenologia" class="form-control" placeholder="Escriba...">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" id="btnBuscarImagenologia" title="Click para Buscar">
                      <span class="fa fa-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
              <ul class="timeline timeline-inverse" id="divContenedor_Imagenologia"></ul>
            </div>
          </div>
          <!-- ============================= [ FIN "Imagenología" ] ============================= -->


          <!-- ================================= [ "Laboratorio" ] ================================= -->
          <div class="tab-pane" id="laboratorio">
            <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerDesde'>
                    <input type='text' class="form-control" id="fechaDesde_laboratorio" value="{{ $model->fechaDesde }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class='input-group date' id='datetimepickerHasta'>
                    <input type='text' class="form-control" id="fechaHasta_laboratorio" value="{{ $model->fechaHasta }}">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="input-group input-group-xs">
                  <input type="text" id="txtBuscar_laboratorio" class="form-control" placeholder="Escriba...">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="button" id="btnBuscarLaboratorio" title="Click para Buscar">
                      <span class="fa fa-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
              <ul class="timeline timeline-inverse" id="divContenedor_Laboratorio"></ul>
            </div>
          </div>
          <!-- =============================== [ FIN "Laboratorio" ] =============================== -->
          
          
          <!-- ==================================== [ "Dicom" ] ==================================== -->
          <div class="tab-pane" id="dicom">
            <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
              <div class="table-responsive" style="{{ config('app.styleIndex') }}">
                <table class="table table-bordered table-hover table-condensed">
                  <thead style="{{ config('app.styleTableHead') }}">
                    <tr>
                      <th class="col-xs-1">ID</th>
                      <th class="col-xs-3">Nombre</th>
                      <th>Descripción</th>
                      <th class="col-xs-1">Tipo</th>
                      <th class="col-xs-1">Fecha</th>
                      <th class="col-xs-1"></th>
                      <th class="col-xs-0"></th>
                    </tr>
                    <tr id="indexSearch" style="background: #ffffff;">
                      <th>
                        <input type="text" id="txtSearch_id" class="form-control input-sm" style="text-transform: uppercase;">
                      </th>
                      <th>
                        <input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;">
                      </th>
                      <th>
                        <input type="text" id="txtSearch_descripcion" class="form-control input-sm" style="text-transform: uppercase;">
                      </th>
                      <th>
                        <input type="text" id="txtSearch_tipo" class="form-control input-sm" style="text-transform: uppercase;">
                      </th>
                      <th>
                        <input type="date" id="txtSearch_fecha" onchange="Busqueda_multipleDICOM()" class="form-control input-sm">
                      </th>
                    </tr>
                  </thead>
                  <tbody class="table-success" id="tbodyDicom" style="{{ config('app.styleTableRow') }}">
                    <tr>
                      <td>Vacío...</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- ================================== [ FIN "Dicom" ] ================================== -->
          
        </div>
      </div>
    </div>
    
  </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardar" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif



