<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
  <input type="hidden" id="txtHiddenJSONdatos" name="txtHiddenJSONdatos" value="{{ $model->txtHiddenJSONdatos }}">
  <input type="hidden" id="txtHidden_hcl_clasificacionsalud" value="{{ $model->hcl_clasificacionsalud }}">
  <!-- BAJA MEDICA -->
  <input type="hidden" id="txtHiddenBajaMedica" name="txtHiddenBajaMedica">
  <input type="hidden" id="txtDatosJsonBajaMedica" name="txtDatosJsonBajaMedica">
  <!-- REFERENCIA MEDICA -->
  <input type="hidden" id="txtHiddenReferenciaMedica" name="txtHiddenReferenciaMedica">
  <input type="hidden" id="txtDatosJsonReferenciaMedica" name="txtDatosJsonReferenciaMedica">
  <!-- INTERNACION MEDICA -->
  <input type="hidden" id="txtHiddenInternacionMedica" name="txtHiddenInternacionMedica">
  <input type="hidden" id="txtDatosJsonInternacionMedica" name="txtDatosJsonInternacionMedica">
  <!-- CERTIFICADO MÉDICO -->
  <input type="hidden" id="txtHiddenCertificadoMedico" name="txtHiddenCertificadoMedico">
  <!-- PRESCRIPCION RECETAS -->
  <input type="hidden" id="txtcontenidoTablaPrescripcion" name="txtcontenidoTablaPrescripcion" value="{{ $model->documentodetallePrescripcion }}">
  <!-- DIAGNOSTICOS -->
  <input type="hidden" id="txtcontenidoTablaDiagnosticos" name="txtcontenidoTablaDiagnosticos" value="{{ $model->documentodetalleDiagnostico }}">

  <input type="hidden" id="txtcontenidoEstablecimiento" name="txtcontenidoEstablecimiento" value="{{ $model->documentodetalleEstablecimiento }}">
  <input type="hidden" id="txtcontenidoCuaderno" name="txtcontenidoCuaderno" value="{{ $model->documentodetalleCuaderno }}">  
 
  <!-- timeline "BUSCADOR DE PACIENTES" --> 
  <div class="row" id="divDatosPaciente" style="display: block;">     
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
      <div class="input-group">
        <input type="text" id="txtBuscadorPaciente" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;" autofocus="true">
        <span class="input-group-btn">
          <button class="btn btn-danger" type="button" id="btnBuscarPaciente">
            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            Buscar
          </button>
        </span>
      </div>

      <div class="table-responsive" style="height: 400px; overflow-y: auto; border: 1px solid #374850;">
        <table id="tablaContenedorDatosPaciente" class="table table-striped">
          <thead style="{{ config('app.styleTableHead') }}">
            <tr>
              <th class="col-xs-2">Nro Historia</th>
              <th class="col-xs-4">Nombres</th>
                <th>Apellidos</th>
                <th></th>
           </tr>
          </thead>
          <tbody id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
            <tr id="idListaVaciaPaciente">
              <td>Lista Vacia...</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- END "BUSCADOR DE PACIENTES" -->
 
  <div class="row" id="divContenedorDatos_paciente" style="display: none;">
    <div class="col-xs-12 col-sm-10 col-md-7 col-lg-7">
      <div class="card">
        <div class="card-body"> 
          <span class="username">
            <a href="#">Registro de la historia clinica digital</a>
            <p>Datos del paciente.</p>
          </span>           
          <div class="col-2">  
            <h4><i class="fa fa-user"> </i> <span id="spanNroAsegurado"></span></h4>
          </div>  
          <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
              <p class="text-muted well well-sm shadow-none" id="nombrePaciente" style="margin: 1px;"></p>
              <address>
              <strong>Paciente, AVC</strong><br>
              <strong>Edad: </strong><span class="description-text" id="spanEdad"></span><br>
              Documento: <span class="description-text" id="spanDocumento"></span><br>
              Sexo: <span class="description-text" id="spanSexo"></span><br>
              <!-- Email: info@almasaeedstudio.com -->
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              To
              <address>
              <strong>Empleador</strong><br>
              Activo<br>
              Ocupación: <br>
              Phone: (555) 539-1037<br>
              <!-- Email: john.doe@example.com -->
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              <b>Invoice #007612</b><br>
              <br>
              <b>Order ID:</b> <input id="txtidhcl_poblacion" name="txtidhcl_poblacion" readonly=""><br>
              <b>Fecha Registro:</b> 2/22/2014<br>
              <b>Medico:</b> Dr. Sanchez
            </div> 
          </div>
        </div>
      </div>
    </div> 
    <div class="col-xs-12 col-sm-10 col-md-5 col-lg-5">
      <div class="card">
        <div class="card-body"> 
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="/imagenes/farmacia_icono.jpg" alt="user image">
            <span class="username">
              <a href="#">Plataforma de atención a realizar</a>
            </span>
            <span class="description">Seleccione el tipo de atencion a realizarse.</span>
          </div> 
          <br>  
          <br>  
          <!-- timeline "TIPOS ATENCION" --> 
            @include($model->rutaview.'opcTipoAtencion')              
          <!-- END "TIPOS ATENCION" -->
        </div>
      </div>
      <div class="row" id="divContenedorDatos_paciente">
         <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
          <div class="card">  
              @if($model->scenario != 'view')
                <button type="button" id="btnGuardar" class="btn btn-success" style="display: none;">
                  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
                </button>
              @endif 
          </div>
        </div> 
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
          <div class="card"> 
              <a href="{{ URL::previous() }}" class="btn btn-primary">
                <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
              </a> 
          </div>
        </div>
      </div>
    </div> 
  </div>

  <div class="row" id="divContenedorSalud" style="display: none;">
    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Dashboard</a></li>
            <li class="nav-item"><a class="nav-link" id="tabHistoriaClinico" href="#historiaClinica" data-toggle="tab">Historial Clinico</a></li>
            <li class="nav-item"><a class="nav-link" href="#farmacia" id="taFarmacia" data-toggle="tab">Prescripción Medica</a></li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content">
            <div class="active tab-pane" id="activity">
              <!-- timeline "REFERENCIA MEDICA" -->
              <div id="divPanel_referencia" style="display: none;">
                <div class="post"> 
                  <div class="user-block">
                    <img src="/imagenes/icono_certificado.jpg" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Registro de referencia medica
                      </h5> 
                      <!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
                    </span>
                  <span class="description">Registro de solucitud complementaria.</span>
                  </div>
                  <div class="box-header with-border"> 
                    @include($model->rutaview.'opcReferenciaMedica')
                  </div> 
                </div>  
              </div>
              <!-- timeline "FIN REFERENCIA MEDICA" -->
              <!-- timeline "CERTIFICADO MEDICO" -->
              <div id="divPanel_certificado" style="display: none;">
                <div class="post"> 
                  <div class="user-block">
                    <img src="/imagenes/icono_certificado.jpg" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Registro de certificado medico
                      </h5> 
                      <!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
                    </span>
                  <span class="description">Registro de solucitud complementaria.</span>
                  </div>
                  <div class="box-header with-border"> 
                    @include($model->rutaview.'opcCertificadoMedico')
                  </div> 
                </div>  
              </div>
              <!-- timeline "FIN CERTIFICADO MEDICO" --> 
              <!-- timeline "IMAGENOLOGIA" -->
              <div id="divPanel_imagenologia" style="display: none;">
                <div class="post"> 
                  <div class="user-block">
                    <img src="/imagenes/Imagenologia.png" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Registro de solicitud RX
                      </h5> 
                      <!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
                    </span>
                  <span class="description">Registro de solucitud complementaria.</span>
                  </div>
                  <div class="box-header with-border">
                    @include($model->rutaview.'opcImagenologia')
                  </div> 
                </div>  
              </div>
              <!-- timeline "FIN IMAGENOLOGIA" -->
              <!-- timeline "FARMACIA" -->
              <div id="divPanel_medicamentos" style="display: none;">
                <div class="post"> 
                  <div class="user-block">
                    <img src="/imagenes/icono_medicamentos.jpg" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Registro de orden medica
                      </h5> 
                      <!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
                    </span>
                  <span class="description">Registro de recetas medicas.</span>
                  </div>
                  <div class="box-header with-border">
                    @include($model->rutaview.'opcFarmacia')
                  </div> 
                </div>  
              </div>
              <!-- timeline "FIN FARMACIA" -->
              <div id="divPanel_signosvitales" style="display: none;">
                <div class="post"> 
                  <div class="user-block">
                    <img src="/uploads/pob_afiliacion/corazon.jpg" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Registro de Signos Vitales
                      </h5> 
                      <!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
                    </span>
                  <span class="description">Ritmo cardiaco, frecuencia respiratorio, temperatura, presión arterial.</span>
                  </div>
                  <div class="box-header with-border">
                    @include($model->rutaview.'opcSignosVitales')
                  </div> 
                </div>  
              </div> 
              <!-- END timeline "SIGNOS VITALES" --> 
              <!-- timeline DIAGNOSTICOS -->
              <div id="divPanel_diagnostico">
                <div class="post clearfix">
                  <div class="user-block">
                    <img src="/uploads/pob_afiliacion/registrar_diagnostico.jpg" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Registrar Diagnóstico
                      </h5>
                    </span>
                    <span class="description">1-Diagnostico presuntivo/2-Diagnosticos de base</span>
                  </div> 
                    @include($model->rutaview.'opcDiagnostico') 
                </div>
                <!-- END timeline DIAGNOSTICOS -->  
                <!-- timeline DESCRIPCION DE DIAGNOSTICO -->
                <div class="post">
                  <div class="user-block">
                    <img src="/uploads/pob_afiliacion/tipo_atencion.jpg" alt="user-avatar" class="img-circle img-fluid">
                    <span class="username">
                      <h5 class="list-group-item-heading text-primary"> 
                        Descripción diagnostico
                      </h5>
                    </span>
                    <span class="description">Posted 5 photos - 5 days ago</span>
                  </div> 
                    @include($model->rutaview.'opcDescripcionDiagnostico') 
                </div> 
                <!-- END timeline DESCRIPCION DE DIAGNOSTICO -->  
              </div>
              <!-- END timeline DIAGNOSTICO -->  
            </div>                     
            <!-- ============================== [ "Historia Clínico" ] ============================== -->
            <div class="tab-pane" id="historiaClinica">  
                <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
                  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class='input-group date' id='datetimepickerDesde'>
                        <input type='text' class="form-control" id="fechaDesde_historial" value="{{ $model->fechaDesde }}">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                    <div class='input-group date' id='datetimepickerHasta'>
                        <input type='text' class="form-control" id="fechaHasta_historial" value="{{ $model->fechaHasta }}">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="input-group input-group-xs">
                      <input type="text" id="txtBuscarHistorial" class="form-control" placeholder="Escriba...">
                      <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" id="btnBuscarHistorialClinico" title="Click para Buscar">
                          <span class="fa fa-search" aria-hidden="true"></span>
                          Buscar
                        </button>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="row" style="border: 1px solid #374850; height: 800px; overflow-y: auto; border-radius: 5px;">
                  <div class="timeline" id="divContenedor_HistoriaCilnico"> 
                    
                  </div> 
                </div> 
            </div> 
            <!-- ============================ [ FIN "Historia Clínico" ] ============================ -->
            <div class="tab-pane" id="farmacia">
              <div class="row" style="border: 1px solid #374850; border-radius: 5px;">
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                  <div class='input-group date' id='datetimepickerDesde'>
                      <input type='text' class="form-control" id="fechaDesde_prescripcion" value=" ">
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                  <div class='input-group date' id='datetimepickerHasta'>
                      <input type='text' class="form-control" id="fechaHasta_prescripcion" value=" ">
                      <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div class="input-group input-group-xs">
                    <input type="text" id="txtBuscar_prescripcion" class="form-control" placeholder="Escriba...">
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="button" id="btnBuscarPrescripcion" title="Click para Buscar">
                        <span class="fa fa-search" aria-hidden="true"></span>
                        Buscar
                      </button>
                    </span>
                  </div>
                </div>
              </div>

              <div class="row" style="border: 1px solid #374850; height: 400px; overflow-y: auto; border-radius: 5px;">
                
                <div class="card-body">
                      <table id="tablaContenedorPrescripcion" class="table table-striped">
                          <thead style="{{ config('app.styleTableHead') }}">
                              <tr>                            
                                <th class="col-xs-1">FECHA</th>                            
                                <th class="col-xs-1">CODIGO</th>
                                <th>MEDICAMENTO/INSUMO</th>   
                                <th class="col-xs-1">CANT.</th>                            
                                <th class="col-xs-2">ESP.</th>
                                <th class="col-xs-3">MEDICO</th>
                                <th class="col-xs-1">TIPO</th> 
                            </tr> 
                        </thead>
                        <tbody id="tbodyPrescripcionLista" style="{{ config('app.styleTableRow') }}">
                          <tr>
                            <td>Vacío...</td>
                          </tr>
                        </tbody>
                    </table>
                  </div>
              </div> 
            </div> 
          </div> 
        </div>
      </div>
    </div> 
    <div class="col-xs-12 col-sm-10 col-md-3 col-lg-3">
      <!-- timeline "SIGNOS VITALES" -->
      <div class="card">
        <div class="card-body"> 
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="/imagenes/signos_vitales.jpg" alt="user image">
            <span class="username">
              <a href="#">Registro historial clinico</a>
            </span>
            <span class="description">Registro de atención medica.</span>
          </div> 
          <br>  
          <br>  
          <ul class="nav nav-pills flex-column">
            <li class="nav-item active">
              <a href="#" class="nav-link" id="btnSignos_vitales">
                  <i class="fas fa-inbox"></i>Registro de signos vitales.      
                  <span class="badge bg-info float-right">1</span>
              </a> 
            </li>
            <li class="nav-item active">
              <a href="#" class="nav-link" id="btnRegistro_diagnostico">
                  <i class="fas fa-inbox"></i>Registro de evolución clinica.      
                  <span class="badge bg-primary float-right">2</span>
              </a> 
            </li> 
          </ul>
        </div>
      </div>
      <!-- timeline "SIGNOS VITALES" -->
      <!-- timeline "EXAMENES COMPLEMENTARIOS" --> 
      <div class="card">
        <div class="card-body"> 
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="/imagenes/farmacia_icono.jpg" alt="user image">
            <span class="username">
              <a href="#">Plataforma solicitudes</a>
            </span>
            <span class="description">Seleccione las solicitud a realizarse.</span>
          </div> 
          <br>  
          <br>  
            @include($model->rutaview.'opcExamenComplementario')  
        </div>
      </div>
      <!-- END "EXAMENES COMPLEMENTARIOS" --> 
      <!-- timeline "ANEXO ARCHIVOS" --> 
      <div class="card">
        <div class="card-body"> 
          <div class="user-block">
            <img class="img-circle img-bordered-sm" src="/imagenes/farmacia_icono.jpg" alt="user image">
            <span class="username">
              <a href="#">Archivos</a>
            </span>
            <span class="description">Anexar archivos de otras consultas medicas.</span>
          </div> 
          <br>  
          <br>  
              @include($model->rutaview.'opcArchivos')             
        </div>
      </div>
      <!-- timeline "ANEXO ARCHIVOS" --> 
    </div>
  </div>
  <div class="progress" id="divProgressBar" style="display: none;">
  <!-- <div class="progress" id="divProgressBar" style="display: block;"> -->
    <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
    </div>
  </div>  
</div> 