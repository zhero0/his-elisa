<!--  LISTA PARA DISPENSAR -->  
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
    <h5 class="list-group-item-heading text-primary"> 
      BUSCAR MEDICAMENTOS
    </h5> 
  </div>
</div>
<div class="table-responsive" style="max-height: 400px; overflow-y: 300; border: 1px solid #374850;">
  <table id="tablaContenedorMedicamento" class="table table-bordered table-hover table-condensed">
      <thead style="{{ config('app.styleTableHead') }}">
          <tr>
            	<th class="col-xs-2">Codificación</th>
            	<th>Medicamento/Insumo</th>                                  
            	<th class="col-xs-2">Unidad</th>
            	<th class="col-xs-2">Saldo</th>
            	<th class="col-xs-2">Cant. Máxima</th>                          
          </tr>
          <tr id="indexSearchMedicamento" style="background: #ffffff;">
          <th>
          <input type="text" id="txtSearch_codigo" onchange="Busqueda_multipleFarmacia()" class="form-control input-sm" style="text-transform: uppercase;">
          </th>
          <th>
          <input type="text" id="txtSearch_medicamento" onchange="Busqueda_multipleFarmacia()" class="form-control input-sm" style="text-transform: uppercase;">
          </th> 
      	</tr>
      </thead>
      <tbody id="tbodyMedicamento" style="{{ config('app.styleTableRow') }}">
        <tr>
          <!-- <td>Vacío...</td> -->
        </tr>
      </tbody>
  </table>
</div>
<div class="row"> 
  	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> 
  		<label class="bmd-label">Medicamento</label> <br>  
		<input type="text" class="form-control" id="medicamento" name="medicamento"> 
  	</div> 
  	<div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
  		<label class="bmd-label">Cantidad</label> <br>  
		<input type="text" class="form-control" id="cantidad" name="cantidad">  
  	</div> 
</div>
<div class="row"> 
  <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
      <label>Indicaciones</label>
      <div class="input-group">
      	<input type="hidden" id="txtIdinv_articulo" name="txtIdinv_articulo">
      	<input type="hidden" id="txtunidad" name="txtunidad">
      	<input type="text" id="txtIndicaciones" name="txtIndicaciones" class="form-control">
        <div class="col-md-1 col-lg-1">
        </div>
      	<span class="input-group-btn float-right">
          <button class="btn btn btn-info" type="button" id="btnAgregarMedicamento" title="Click para agregar">
          <span>Registrar orden medica</span>.
          </button>
        </span>
      </div>
  </div> 
</div> 
<br>
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
    <h5 class="list-group-item-heading text-success"> 
      MEDICAMENTOS A DISPENSAR
    </h5> 
  </div>
</div>
<div class="table-responsive" style="max-height: 250px; overflow-y: auto; border: 1px solid #374850;">
  <table id="tablaContenedorListaRecetas" class="table table-bordered table-hover table-condensed">
    <thead style="{{ config('app.styleTableHead') }}">
        <tr>                            
          <th class="col-xs-1">CODIFICACION</th>
          <th>MEDICAMENTO/INSUMO</th>   
          <th class="col-xs-1">CANT.</th>                            
          <th class="col-xs-1">UNIDAD</th>
          <th class="col-xs-4">INDICACIÓN</th>
          <th></th>
      </tr> 
    </thead>                      
    <tbody class="table-success" id="tbodyMedicamentoListaReceta" style="{{ config('app.styleTableRow') }}">
      <tr>
        <!-- <td>Vacío...</td> -->
      </tr>
    </tbody>
  </table>
</div>   
<!-- --------------------- -->