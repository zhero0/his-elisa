<ul class="nav nav-pills flex-column">
  <li class="nav-item active">
    <a href="#" class="nav-link">
      <i class="fas fa-inbox"></i> Prescripcion de Medicamentos
      <label class="radio-inline float-right"><input type="radio" name="tipo_consulta" value="2">Consulta medica repetida</label>
      <label class="radio-inline float-right"><input type="radio" name="tipo_consulta" value="1" checked="">Iniciar nueva consulta</label>
    </a>
  </li>
  <li class="nav-item">
    <a href="#" class="nav-link">
      <span class="badge bg-danger float-middle">1</span>
      <i class="far fa-file-alt"></i> Laboratorio Clinico
      <span class="badge bg-danger float-right"></span>
      <div class="onoffswitch float-right">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkEmergencia">
        <label class="onoffswitch-label" for="checkEmergencia"> Seleccionar estado en emergencia
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </a> 
  </li>
  <li class="nav-item">
    <a href="#" class="nav-link">
    <span class="badge bg-warning float-middle">2</span>
      <i class="far fa-envelope"></i> Imagenología - Rayos X

      <div class="onoffswitch float-right">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkHospitalizado">
        <label class="onoffswitch-label" for="checkHospitalizado"> Seleccionar paciente hospitalizado
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </a>
  </li> 
  <li class="nav-item">
    <a href="#" class="nav-link">
      <span class="badge bg-success float-middle">3</span>
      <i class="fas fa-filter"></i> Certificado Médico 
      <div class="onoffswitch float-right">
        <input type="checkbox" class="onoffswitch-checkbox" id="checkEmbarazo">
        <label class="onoffswitch-label" for="checkEmbarazo"> Seleccionar paciente en gestación
            <span class="onoffswitch-inner"></span>
            <span class="onoffswitch-switch"></span>
        </label>
      </div>
    </a>
  </li> 
  	<li class="nav-item">
	    <a href="#" class="nav-link">
	      	<span class="badge bg-success float-middle">3</span>
	      	<i class="fas fa-filter"></i> Baja Médica
	      	<div class="onoffswitch float-right">
		        <input type="checkbox" class="onoffswitch-checkbox" id="checkEmbarazo">
		        <label class="onoffswitch-label" for="checkEmbarazo"> Seleccionar paciente en gestación
		            <span class="onoffswitch-inner"></span>
		            <span class="onoffswitch-switch"></span>
		        </label>
	      	</div>
	    </a>
	</li>
	<li class="nav-item">
	    <a href="#" class="nav-link">
	      	<span class="badge bg-success float-middle">3</span>
	      	<i class="fas fa-filter"></i> Referencia médica
	      	<div class="onoffswitch float-right">
		        <input type="checkbox" class="onoffswitch-checkbox" id="checkEmbarazo">
		        <label class="onoffswitch-label" for="checkEmbarazo"> Seleccionar paciente en gestación
		            <span class="onoffswitch-inner"></span>
		            <span class="onoffswitch-switch"></span>
		        </label>
	      	</div>
	    </a>
	</li>
</ul>



<div class="card"> 
	<div class="card-body">		
		<div class="row">
			<div class="col-5 col-sm-4">
				<div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">					
					<a class="nav-link active" id="vert-tabs-recetas-tab" data-toggle="pill" href="#vert-tabs-recetas" role="tab" aria-controls="vert-tabs-recetas" aria-selected="true">
 						Prescripcion de Medicamentos - 	       				
						<span class="font-weight-bold">
							<span class="text-muted" id="spanTotalRecetasFarmacia">0</span>
						</span> 
				 	</a> 
					<a class="nav-link" id="vert-tabs-laboratorio-tab" data-toggle="pill" href="#vert-tabs-laboratorio" role="tab" aria-controls="vert-tabs-laboratorio" aria-selected="false">
						Laboratorio Clinico - 
						<span class="font-weight-bold">
							<span class="text-muted" id="spanTotalExamenesLaboratorio">0</span>
						</span>  
					</a> 
					<a class="nav-link" id="vert-tabs-imagenologia-tab" data-toggle="pill" href="#vert-tabs-imagenologia" role="tab" aria-controls="vert-tabs-imagenologia" aria-selected="false">
						Imagenología - Rayos X
						<span class="font-weight-bold">
							<span class="text-muted" id="spanTotalExamenesImagenologia">0</span>
						</span> 
					</a>
					<a class="nav-link" id="vert-tabs-cert_medico-tab" data-toggle="pill" href="#vert-tabs-cert_medico" role="tab" aria-controls="vert-tabs-cert_medico" aria-selected="false">
						Certificado Médico - 
						<span class="font-weight-bold">
							<span class="text-muted" id="spanCertificadoMedico">NO</span>
						</span> 
					</a>
					<a class="nav-link" id="vert-tabs-baja_medica-tab" data-toggle="pill" href="#vert-tabs-baja_medica" role="tab" aria-controls="vert-tabs-baja_medica" aria-selected="false">
						Baja Médica - 
						<span class="font-weight-bold">
							<span class="text-muted" id="spanBajaMedica">NO</span>
						</span> 
					</a>
					<a class="nav-link" id="vert-tabs-referencia_medica-tab" data-toggle="pill" href="#vert-tabs-referencia_medica" role="tab" aria-controls="vert-tabs-referencia_medica" aria-selected="false">
						Referencia médica - 
						<span class="font-weight-bold">
							<span class="text-muted" id="spanReferenciaMedica">NO</span>
						</span> 
					</a>
				</div>
			</div>
			<div class="col-7 col-sm-8">
				<div class="tab-content" id="vert-tabs-tabContent">
					<div class="tab-pane text-left fade show active" id="vert-tabs-recetas" role="tabpanel" aria-labelledby="vert-tabs-recetas-tab">
						@include($model->rutaview.'opcFarmacia')
					</div>
					<div class="tab-pane fade" id="vert-tabs-laboratorio" role="tabpanel" aria-labelledby="vert-tabs-laboratorio-tab">
					Mauris tincidunt mi at erat gravida, eget tristique urna bibendum. Mauris pharetra purus ut ligula tempor, et vulputate metus facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas sollicitudin, nisi a luctus interdum, nisl ligula placerat mi, quis posuere purus ligula eu lectus. Donec nunc tellus, elementum sit amet ultricies at, posuere nec nunc. Nunc euismod pellentesque diam.
					</div>
					<div class="tab-pane fade" id="vert-tabs-imagenologia" role="tabpanel" aria-labelledby="vert-tabs-imagenologia-tab">
					Morbi turpis dolor, vulputate vitae felis non, tincidunt congue mauris. Phasellus volutpat augue id mi placerat mollis. Vivamus faucibus eu massa eget condimentum. Fusce nec hendrerit sem, ac tristique nulla. Integer vestibulum orci odio. Cras nec augue ipsum. Suspendisse ut velit condimentum, mattis urna a, malesuada nunc. Curabitur eleifend facilisis velit finibus tristique. Nam vulputate, eros non luctus efficitur, ipsum odio volutpat massa, sit amet sollicitudin est libero sed ipsum. Nulla lacinia, ex vitae gravida fermentum, lectus ipsum gravida arcu, id fermentum metus arcu vel metus. Curabitur eget sem eu risus tincidunt eleifend ac ornare magna.
					</div>
					<div class="tab-pane fade" id="vert-tabs-cert_medico" role="tabpanel" aria-labelledby="vert-tabs-cert_medico-tab">
						@include($model->rutaview.'opcCertificadoMedico')
					</div>
					<div class="tab-pane fade" id="vert-tabs-baja_medica" role="tabpanel" aria-labelledby="vert-tabs-baja_medica-tab">
						@include($model->rutaview.'opcBaja_Medica')
					</div>
					<div class="tab-pane fade" id="vert-tabs-referencia_medica" role="tabpanel" aria-labelledby="vert-tabs-referencia_medica-tab">
						@include($model->rutaview.'opcReferencia_Medica')
					</div>
				</div>
			</div>
		</div> 
	</div>
</div>
 