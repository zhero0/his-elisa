@section('main-content')
@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>REGISTRAR MEDICO DE CONSULTORIO</h1>
@stop 
 
@section('content')
<form method="POST" action="{{ route('cuaderno_personal.store') }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	@include($model->rutaview.'form') 
	@include($model->rutaview.'modal')
</form>
@stop 