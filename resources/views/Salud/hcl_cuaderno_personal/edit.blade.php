@extends('adminlte::page') 
@section('title', 'EDITAR REGISTRO')  
@section('content_header')
    <h1>EDITAR REGISTRAR MEDICO DE CONSULTORIO</h1>
@stop

@section('content')
<form method="POST" action="{{ route('cuaderno_personal.update', $model->id) }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	<input name="_method" type="hidden" value="PATCH"> 
	@include($model->rutaview.'form')  
	@include($model->rutaview.'modal')
</form>
@stop 