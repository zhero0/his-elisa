<div class="callout callout-danger"> 
  <div class="row"> 
    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
      <label>Médico</label>
      <div class="input-group">
        <input type="text" id="txtMedico" name="txtMedico" value="{{$model->medico}}" class="form-control" disabled="">
        <span class="input-group-btn">
          <button class="btn btn-primary" type="button" id="btnModal_Medico" title="Click para Buscar">
            <span class="fa fa-search" aria-hidden="true"></span>.
          </button>
        </span>
      </div>
    </div> 
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
      <label>Cuaderno</label>
      <div class="input-group">
        <input type="text" id="txtEspecialidad" name="txtEspecialidad" class="form-control" value="{{$model->especialidad}}" disabled="">
        <span class="input-group-btn">
          <button class="btn btn-primary" type="button" id="btnModal_Especialidad" title="Click para Buscar">
            <span class="fa fa-search" aria-hidden="true"></span>.
          </button>
        </span>
      </div>
    </div> 
  </div>
</div>   