<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">   
    <input type="hidden" id="txtJsonHcl_poblacion" name="txtJsonHcl_poblacion" value="{{ $model->txtJsonHcl_poblacion }}"> 
    <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">
    <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">  
 
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-1 col-lg-1">
    </div>
    <div class="col-xs-12 col-sm-11 col-md-10 col-lg-10">
      <div class="card">
        <div class="card-body">           
            <span class="username">
              <a href="#">Registro</a>
              <p>Escoger los parametros de registro para atención medica.</p>
            </span>  
          <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">    
            @include($model->rutaview.'_dataContent') 
          </div> 
        </div>
      </div>
    </div>
  </div>  
</div>

<a href="/cuaderno_personal" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarExamen" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif