<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    
    <div class="row"> 
      <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <label for="nombre" class="col-sm-6 col-form-label">Nombre:</label>
        <div class="col-sm-12">
          <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $model->nombre }}" placeholder="nombre del establecimiento de salud">
        </div> 
      </div>
    </div>  
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <label for="nombre" class="col-sm-6 col-form-label">Personalizar plantilla</label> 
          <textarea class="form-control" id="summary-ckeditor" name="plantilla" value="Escriba..." rows="12">{{ $model->plantilla }}</textarea>
        </div>
    </div>
</div>

<a href="/plantilla_personal" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardar" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif