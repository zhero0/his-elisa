@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>NUEVO REGISTRO DE PLANTILLA PARA HISTORIAL CLINICO</h1>
@stop

@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="card">
			<div class="card-body">   
				<form method="POST" action="{{ route('plantilla_personal.store') }}" autocomplete="off" id="form"> 
					@include($model->rutaview.'form')
				</form>  		
			</div>
		</div>
	</div>
</div>
@stop