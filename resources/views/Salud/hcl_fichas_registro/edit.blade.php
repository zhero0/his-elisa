@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>RESERVAS DE ATENCION MEDICA</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('hcl_fichas_registro.update', $model->id) }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	<input name="_method" type="hidden" value="PATCH"> 
	@include($model->rutaview.'form')  
	@include($model->rutaview.'modal')
</form>
@stop 