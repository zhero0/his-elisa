<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
  <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">  
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">   
 
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-md-1 col-lg-1">
    </div>
    <div class="col-xs-12 col-sm-11 col-md-10 col-lg-10">
      <div class="card">
        <div class="card-body">           
            <span class="username">
              <a href="#">Registro</a>
              <p>Escoger los parametros de registro para atención medica.</p>
            </span>  
          <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">    
            @include($model->rutaview.'_dataContent') 
          </div> 
        </div>
      </div>
    </div>
  </div>  
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="fa fa-undo" aria-hidden="true"></span> Volver
</a>
@if($model->scenario == 'create')
  <button type="button" class="btn {{ $model->scenario == 'create'? 'btn-success':'btn-warning' }}  btn-md" rel="tooltip" title="Guardar" id="btnGuardar" onclick="guardar()">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> {{ $model->scenario == 'create'? 'Guardar':'Editar' }} 
  </button> 
@endif  
@if($model->scenario == 'edit')
  <button type="button" class="btn btn-danger btn-md" rel="tooltip" style="display: '{{ $model->eliminar == 0? 'visible':'none' }}' " title="eliminar" id="btnEliminar" onclick="eliminarRegistro()">
    <span class="fa fa-trash" aria-hidden="true"></span> Eliminar
  </button> 
@endif  