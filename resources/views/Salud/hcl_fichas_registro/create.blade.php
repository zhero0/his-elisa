@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>REGISTRAR DATOS DE ATENCION MEDICA</h1>
@stop
 
@section('content')
<form method="POST" action="{{ route('hcl_fichas_registro.store') }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	@include($model->rutaview.'form') 
	@include($model->rutaview.'modal')
</form>
@stop 