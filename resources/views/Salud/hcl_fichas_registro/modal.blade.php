<div class="modal fade" id="vModalBuscador_especialidad">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Datos Especialidad</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>           
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input type="text" id="txtBuscadorEspecialidad" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarEspecialidad">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>
        </div> 
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatosEspecialidad" class="table table-bordered table-hover table-condensed">
            <thead style="{{ config('app.styleTableHead') }}">
              <tr>
                <th>Sigla</th>
                <th>Especialidad</th> 
                <th></th>
              </tr>
            </thead>
            <tbody class="table-success" id="tbody_Especialidad" style="{{ config('app.styleTableRow') }}">
              <tr id="idListaVaciaEspecialidad">
                <td>Lista Vacía...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div>
 
<div class="modal fade" id="vModalBuscador_medico">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Datos Empleador</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>           
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input type="text" id="txtBuscadorMedico" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarMedico">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>
        </div>              
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatosMedico" class="table table-bordered table-hover table-condensed">
            <thead style="{{ config('app.styleTableHead') }}">
              <tr>
                <th class="col-xs-2">Matricula</th>
                <th class="col-xs-4">Apellidos</th> 
                <th class="col-xs-4">Nombres</th> 
                <th></th>
              </tr>
            </thead>
            <tbody class="table-success" id="tbody_Medico" style="{{ config('app.styleTableRow') }}">
              <tr id="idListaVaciaMedico">
                <td>Lista Vacía...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div> 