<?php
use \App\Models\Medicina_trabajo\Hsi_tipo_registro;
?>
<div class="callout callout-danger"> 
  <div class="row">  
    <div class="col-12 col-sm-6 col-md-6 col-lg-6">  
      <div class="panel panel-default" id="divEmpleador">
          <button type="button" class="btn btn-default btn-round btn-sm" 
              title="Click para Buscar"
              onclick="abrirVentanaMed(1, 'ESPECIALIDAD', 'buscaEmpleador')">
            <i class="fa fa-users" aria-hidden="true"></i>
            ESPECIALIDAD (*)
          </button>
          <div class="form-group">
            <input type="text" class="form-control" id="txtEspecialidad" name="txtEspecialidad" 
              value="{{ $model->especialidad }}" readonly="" onclick="abrirVentanaMed(1, 'ESPECIALIDAD', 'buscaEmpleador')">
          </div>
      </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-6">
      <div class="panel panel-default">
          <button type="button" class="btn btn-default btn-round btn-sm" 
              title="Click para Buscar"
              onclick="abrirVentanaMed(2, 'MEDICO', 'buscaPoblacion')">
          <i class="fa fa-user-o" aria-hidden="true"></i>
            MEDICO (*)
          </button>
          <div class="form-group">
            <input type="text" class="form-control" id="txtMedico" name="txtMedico" 
              value="{{ $model->medico }}" readonly="" onclick="abrirVentanaMed(2, 'MEDICO', 'buscaPoblacion')">
          </div>
      </div>  
    </div>      
  </div>
</div>  
<div class="callout callout-info">
  <h5>Registrar - Fechas de atención</h5>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label class="bmd-label">DESDE</label> <br>  
      <input type="date" class="form-control datepicker" id="fecha_desde" name="fecha_desde" value="{{  $model != null? $model->fecha_desde : '' }}">
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label class="bmd-label">HASTA</label> <br>  
      <input type="date" class="form-control datepicker" id="fecha_hasta" name="fecha_hasta" value="{{  $model != null? $model->fecha_hasta : '' }}"> 
    </div> 
  </div> 
  <br>
  <h5>Registrar - Intervalo de tiempo</h5>
  <div class="row"> 
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
    <div class="col-12 col-sm-4 col-md-4 col-lg-4">
      <label for="codigo" class="col-sm-6 col-form-label">Cantidad de fichas</label>
      <div class="col-sm-12">
        <input type="text" class="form-control" id="cantidad_fichas" name="cantidad_fichas" value=" {{ $model->cantidad_fichas }} " >
      </div>  
    </div>
    <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3"> 
      <label for="codigo" class="col-sm-6 col-form-label">Hora Inicio</label>
      <div class="col-sm-12">
        <input type="time" class="form-control" id="hora_inicio" name="hora_inicio" value="{{ $model->hora_inicio }}">
      </div>    
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <label for="codigo" class="col-sm-6 col-form-label">Minutos de atención</label>
      <div class="col-sm-12">
        <input type="time" class="form-control" id="hora_intervalo" name="hora_intervalo" value="{{ $model->hora_intervalo }}">
      </div>   
    </div>  
  </div> 
</div>  