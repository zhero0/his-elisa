@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTA DE CLASIFICACIONES</h1>
@stop 

@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('hsi_tipo_registro.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo registro
		        </a>
		    </div>  
		    <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">				
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('hsi_tipo.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Registrar agrupación
		        </a>
		    </div> 
		</div>
	</div>  
	<div class="card-body"> 
		<table id="tableLista" class="table table-striped">
			<thead>
				<tr> 
 					<th>Registros</th> 
 					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($model as $dato)	
					<tr data-widget="expandable-table" aria-expanded="true">
						<td>
							<i class="expandable-table-caret fas fa-caret-right fa-fw"></i>
							{{ $dato->codigo.' || '.$dato->nombre.' || Estado' }} 
							<small class="{{ $dato->eliminado == 0? 'text-success' : 'text-danger' }} mr-1">
							<i class="fas {{ $dato->eliminado == 0? 'fa-arrow-up' : 'fa-arrow-down' }} "></i>
							{{ $dato->eliminado == 0? 'Activo' : 'Inactivo' }}
							</small>
							{{ ' || '.$dato->usuario }}
						</td>  
						<td  class="col-xs-1">
							<div class="btn-group">
				                <a href="{{ route('hsi_tipo_registro.edit', $dato->id) }}" style="display: {{ $dato->eliminado == 1? 'none':'' }};" class="btn btn-outline-warning btn-sm" rel="tooltip" title="Editar actividad">
				                    <i class="fa fa-edit"></i>                                
				                </a>  
				            </div> 
						</td>
					</tr> 						
					<tr class="expandable-body">
						<td>
							<div class="p-0" style="">
								<table class="table table-hover">
									<tbody>
										@foreach($modelTipo as $subDato)
											@if($subDato->idhsi_tipo_registro == $dato->id)
												<tr>
													<td>{{ $subDato->codigo }}</td>
													<td>{{ $subDato->nombre }}</td>
													<td>
														Estado
														<small class="{{ $subDato->eliminado == 0? 'text-success' : 'text-danger' }} mr-1">
														<i class="fas {{ $subDato->eliminado == 0? 'fa-arrow-up' : 'fa-arrow-down' }} "></i>
														{{ $subDato->eliminado == 0? 'Activo' : 'Inactivo' }}
														</small>
													</td>
													<td class="col-xs-1">{{ $subDato->usuario }}</td> 
													<td  class="col-xs-1">
														<div class="btn-group">
											                <a href="{{ route('hsi_tipo.edit', $subDato->id) }}" style="display: {{ $subDato->eliminado == 1? 'none':'' }};" class="btn btn-outline-warning btn-sm" rel="tooltip" title="Editar actividad">
											                    <i class="fa fa-edit"></i>                                
											                </a>  
											            </div> 
													</td>
												</tr>
											@endif
										@endforeach 
									</tbody>
								</table>
							</div>
						</td>
					</tr>							  	 					
				@endforeach
			</tbody>
		</table>
	</div> 
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#tableLista').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection
 