@extends('layouts.app')
@section('contentheader_title', 'EDITAR REGISTRO DE FORMULARIO')
@section('htmlheader_title', 'EDITAR REGISTRO DE FORMULARIO')

@section('main-content')
<div class="content">
	<div class="col-md-12">
		<div class="card">
	        <div class="card-body">    
      			{!! Form::model($model, ['route' => ['med_programacion.update', $model->id], 'files'=>true, 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form') 
					@include($model->rutaview.'modal')
				{!! Form::close() !!}			
			</div>
		</div>
	</div>
</div>
@stop