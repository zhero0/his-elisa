@extends('layouts.app')
@section('contentheader_title', 'REGISTRO PROGRAMACION DE ATENCION')
@section('htmlheader_title', 'REGISTRO PROGRAMACION DE ATENCION')
 
@section('main-content') 
<div class="content">
	<div class="col-md-12">
	  	<div class="card">
	    	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    		<input type="hidden" id="txtAccion" value="{{ $model->accion }}">

    		@include($model->rutaview.'search')

    		<div class="card-header card-header-white cabeceraIndex">
				<div class="row">
					<div class="col-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
						<br>
					<a type="button" class="btn btn-success btn-sm" rel="tooltip"
								href="{{ route('med_programacion.create') }}" title="Nuevo Registro">
						<i class="fa fa-plus" aria-hidden="true"></i> Nuevo
					</a>
					</div>
				</div>
			</div>

			<div class="card-body" loader="/assets/img/oval.svg">
				<div class="table-responsive table-bordered table-hover" style="{{ config('app.styleIndex') }}">
					<table class="table table-condensed">
						<thead style="{{ config('app.styleTableHead') }}">
							<tr>
								<th>Hora</th> 								 
								<th>Fecha</th>
								<th>Documento</th>
								<th>Apellido Paterno</th>
								<th>Apellido Materno</th>
								<th>Nombres</th> 						
								<th>Empleador</th> 
								<th>Estado</th>								
								<th></th>
							</tr>
						</thead>
						<tbody id="indexContent">
							@include($model->rutaview.'indexContent')
						</tbody>
					</table>
				</div>
			</div>
	  	</div>
  	</div>
</div>  
@include($model->rutaview.'indexModal')
@stop