<!-- BUSCADOR "PACIENTE", "EMPLEADOR" -->
<div class="modal fade" id="vModalBuscador">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title">...</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" id="txtHiddenData">
          
          <div class="input-group">
                  <input type="text" id="txtBuscador" class="form-control" 
                      onkeyup="searchVentanaProg(event)" placeholder="Buscar..." 
                      style="text-transform: uppercase;">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-default btn-round btn-sm" onclick="searchVentanaProg();">
                          <span class="fa fa-search" aria-hidden="true"></span>
                          Buscar
                      </button>
                    </span>
                </div>
                
                <div class="__modalContent"></div>
          </div>
          <div class="modal-footer">
            @include('layouts.extra.botonCerrar')
        </div>
      </div>
  </div>
</div>