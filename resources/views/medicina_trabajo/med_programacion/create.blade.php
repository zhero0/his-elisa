@extends('layouts.app')
@section('contentheader_title', 'REGISTRO DE ATENCIÓN')
@section('htmlheader_title', 'REGISTRO DE ATENCIÓN')

@section('main-content')
<div class="content">
	<div class="col-md-12">
		<div class="card">
	        <div class="card-body">     			
      			{!! Form::Open(['route' => 'med_programacion.store', 'files'=>true, 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form') 
					@include($model->rutaview.'modal')
				{!! Form::close() !!}			
			</div>
		</div>
	</div>
</div>
@stop