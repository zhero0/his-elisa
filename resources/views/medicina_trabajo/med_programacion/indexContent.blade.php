@foreach($model as $dato)
    <tr role="row" style="{{ config('app.styleTableRow') }}"> 
        <td class="text-left">{{ $dato->res_hora_registro }}</td>                      
        <td class="text-left">{{ $dato->res_fecha_registro }}</td>                              
        <td class="text-left">{{ $dato->res_documento }}</td>                              
        <td class="text-left">{{ $dato->res_primer_apellido }}</td>
        <td class="text-left">{{ $dato->res_segundo_apellido }}</td>
        <td class="text-left">{{ $dato->res_nombres }}</td>
        <td class="text-left">{{ $dato->res_empleador_nombre }}</td>
        <td class="text-left">{{ $dato->res_nombre_estado }}</td> 
        <td class="td-actions text-right"> 
            <a href="{{ route('med_programacion.edit', $dato->id) }}" class="btn btn-default btn-sm btn-round" rel="tooltip" title="Editar">
                    <i class="fa fa-pencil"></i>                                
            </a> 
            <button class="btn btn-default btn-round btn-sm" onclick="imprimirSolicitudPro( {{ $dato->id }} )" rel="tooltip" title="Imprimir Resultado">
                <i class="fa fa-print"></i>
            </button>                      
        </td>
    </tr>
@endforeach