<div class="modal fade" id="vImprimeSolicitud">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">REPORTE RESULTADO</h4>
      </div>
      <div class="modal-body">
        <div id="imprimeSolicitud" style="height: 370px;"></div>
        
        <div class="progress" style="display: none;">
          <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        @include('layouts.extra.botonCerrar')
      </div>
    </div>
  </div>
</div>