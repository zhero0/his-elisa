<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="idmed_registro" name="idmed_registro" value="{{ $model->idmed_registro }}"> 
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
  
    <div class="row">  
    	<div class="col-6 col-sm-3 col-md-2 col-lg-2">
        <div class="row">
          <label class="col-sm-4 col-form-label" for="hora_registro" data-required="*">HORA</label>
          <div class="col-sm-8">
            <br>
            <div class="form-group">
              <input type="text" class="form-control timepicker" id="hora_registro" name="hora_registro" value="{{ $model->hora_registro }}">
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <br>
        <div class="form-group">                  
          <label for="fecha_registro" data-required="*">FECHA</label> 
          <input type="text" class="form-control datepicker" id="fecha_registro" name="fecha_registro" value="{{ $model->fecha_registro }}">
        </div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <div class="form-group" >      
          <label class="bmd-label">Número de contacto</label> <br>  
          <input type="text" class="form-control" id="txtNro_movil" name="nro_movil" value="{{$model->nro_movil}}">
        </div>
      </div> 
      <div class="col-12 col-sm-12 col-md-4 col-lg-4">
        <label>TIPO DE ATENCIÓN</label>
        <br>
        <select name="idmed_estado" id="idmed_estado" class="selectpicker" data-size="10" data-style="btn btn-primary btn-round btn-sm" title="Single Select"> 
            <option disabled selected>Seleccionar</option> 
            @foreach($modelEstados as $dato) 
              @if($model != null)
                <option value="{{$dato->id}}" {{ $model->idmed_estado == $dato->id ? 'selected="selected"' : ''  }} >{{$dato->nombre }}</option> 
              @else
                <option value="{{$dato->id}}">{{$dato->nombre}}</option> 
              @endif
            @endforeach
        </select>
      </div>
    </div>
    <br>

    <div class="row">
    	@include($model->rutaview.'_dataContent')
    </div>    
@include('layouts.extra.botonesForm')
</div>