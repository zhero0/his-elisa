@extends('_general._search')
@section('search')
<div class="row">
	<div class="col-12 col-sm-3 col-md-1 col-lg-1"> 
	</div>
	<div class="col-12 col-sm-3 col-md-1 col-lg-1">
		<div class="form-group">
			<label>Documento</label>
			<input type="text" name="nro_documento" class="form-control" style="text-transform: uppercase;">
		</div>
	</div>  
	<div class="col-12 col-sm-3 col-md-2 col-lg-2">
		<div class="form-group">
			<label>Ap. Paterno</label>
			<input type="text" name="paterno" class="form-control" style="text-transform: uppercase;">
		</div>
	</div>
	<div class="col-12 col-sm-3 col-md-2 col-lg-2">
		<div class="form-group">
			<label>Ap. Materno</label>
			<input type="text" name="materno" class="form-control" style="text-transform: uppercase;">
		</div>
	</div>
	<div class="col-12 col-sm-4 col-md-2 col-lg-2">
		<div class="form-group">
			<label>Nombres</label>
			<input type="text" name="nombre" class="form-control" style="text-transform: uppercase;">
		</div>
	</div>
	
	<div class="col-12 col-sm-4 col-md-2 col-lg-2">
        <select id="estado" name="estado"  class="form-control input-sm">
	      	<option value="0" selected> :: Seleccionar</option>  
	      	@foreach($modelEstados as $dato)
	        	@if($model != null)
	        	<option value="{{$dato->id}}">{{$dato->nombre}}</option>     
	        	@endif                      
	      	@endforeach 
    	</select>
	</div>   
</div>
@stop
