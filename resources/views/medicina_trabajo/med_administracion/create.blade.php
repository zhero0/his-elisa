@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>NUEVO REGISTRO</h1>
@stop
 
@section('content')   
<form method="POST" action="{{ route('med_administracion.store') }}" autocomplete="off" id="form">
	{{ csrf_field() }}
	@include($model->rutaview.'form')
</form> 
@stop
