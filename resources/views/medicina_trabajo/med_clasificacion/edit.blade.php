@extends('adminlte::page') 
@section('title', 'EDITAR REGISTRO')  
@section('content_header')
    <h1>EDITAR REGISTRO DE CLASIFICACION</h1>
@stop
 
@section('content')  
<form method="POST" action="{{ route('med_clasificacion.update', $model->id) }}" autocomplete="off" id="form">
	{{ csrf_field() }}
		<input name="_method" type="hidden" value="PATCH"> 
	@include($model->rutaview.'form')
</form> 
@stop