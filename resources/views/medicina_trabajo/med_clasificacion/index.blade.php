@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTA DE REGISTROS</h1>
@stop   

@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;"> 
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('med_clasificacion.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo registro
		        </a>
		    </div> 
		</div>
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Nombre</th> 								
					<th>Descripción</th>
					<th>Costo tramite</th>						
					<th>Costo formulario</th>
					<th>Usuario</th>
					<th></th>
				</tr>
				<!-- <tr id="indexSearch" style="background: #ffffff;">								
 					<th>
 						<input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;">
 					</th>
 					<th>
 						<input type="text" id="txtSearch_descripcion" class="form-control input-sm" style="text-transform: uppercase;">
 					</th>
				</tr> -->
			</thead>
			<tbody>
				@foreach($model as $dato)					
					<tr role="row">
						<td class="text-left">{{ $dato->nombre }}</td>						
						<td class="text-left">{{ $dato->descripcion }}</td>						
						<td class="text-left">{{ $dato->costo_tramite }}</td>						
						<td class="text-left">{{ $dato->costo_formulario }}</td>						
						<td class="text-left">{{ $dato->usuario }}</td>						
						<td class="td-actions text-right"> 
							<a href="{{ route('med_clasificacion.edit', $dato->id) }}" class="btn btn-default btn-sm btn-round" rel="tooltip" title="Editar">
									<i class="fa fa-edit"></i>								
							</a>   					
						</td>
					</tr>					
				@endforeach
			</tbody>
		</table>
	</div>  
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 