@section('main-content')
@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>NUEVO REGISTRO</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
    	<div class="card">
			<div class="card-body">    	
				<form method="POST" action="{{ route('hsi_tipo.store') }}" autocomplete="off" id="form">
					{{ csrf_field() }}
					@include($model->rutaview.'form') 
				</form>		
			</div>
		</div>
	</div>
</div>
@stop 