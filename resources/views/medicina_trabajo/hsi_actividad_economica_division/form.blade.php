<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}"> 
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}"> 
 
  <div class="row">  
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
        <div class="form-group" >      
          <label class="bmd-label">Codificación</label> <br>  
          <input type="text" class="form-control" id="txtCodifcacion" name="codificacion" value="{{$model->codificacion}}">
        </div>
    </div>  
    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3" id="divPanelDatos">
      <div class="form-group">                        
        <label class="bmd-label">Nombre de actividad</label>  <br> 
        <input type="text" class="form-control" id="txtNombre" name="nombre" value="{{$model->nombre}}" >
      </div>  
    </div> 
  </div>    
</div> 
 
<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="fa fa-undo" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view') 
  <button type="button" class="btn btn-success btn-md" rel="tooltip" title="Guardar"  id="btnGuardar">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> Guardar
  </button>
@endif