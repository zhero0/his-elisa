<div class="col-12 col-sm-6 col-md-6 col-lg-6">
  <div class="panel panel-default">
      <button type="button" class="btn btn-default btn-round btn-sm" 
          title="Click para Buscar"
          onclick="abrirVentana(1, 'MEDICO', 'buscaPoblacion')">
      <i class="fa fa-user-o" aria-hidden="true"></i>
        MEDICO (*)
      </button>
      <div class="form-group">
        <input type="text" class="form-control" id="poblacion" name="poblacion" 
          value="{{ $model->paciente }}" readonly="" onclick="abrirVentana(1, 'MEDICO', 'buscaPoblacion')">
      </div>
  </div>  
</div>
<div class="col-12 col-sm-6 col-md-6 col-lg-6"> 
  <div class="panel panel-default" id="divEmpleador">
      <button type="button" class="btn btn-default btn-round btn-sm" 
          title="Click para Buscar"
          onclick="abrirVentana(2, 'ESPECIALIDAD', 'buscaEspecialidad')">
        <i class="fa fa-users" aria-hidden="true"></i>
        ESPECIALIDAD (*)
      </button>
      <div class="form-group">
        <input type="text" class="form-control" id="txtEspecialidad" name="txtEspecialidad" 
          value="{{ $model->hcl_empleador_nombre }}" readonly="" onclick="abrirVentana(2, 'ESPECIALIDAD', 'buscaEspecialidad')">
      </div>
  </div>
</div>