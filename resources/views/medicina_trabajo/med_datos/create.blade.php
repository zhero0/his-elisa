@extends('layouts.app')
@section('contentheader_title', 'NUEVO REGISTRO DE FORMULARIO')
@section('htmlheader_title', 'NUEVO REGISTRO DE FORMULARIO')

@section('main-content')
<div class="content">
	<div class="col-md-12">
		<div class="card">
	        <div class="card-body">     			
      			{!! Form::Open(['route' => 'med_datos.store', 'files'=>true, 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}			
			</div>
		</div>
	</div>
</div>
@stop