<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
      
  <div class="col-md-12">
      <div class="card ">
        <div class="card-header card-header-rose card-header-text">
          <div class="card-text">
            <h4 class="card-title">Datos</h4>
          </div>
        </div>
        <div class="card-body ">

          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
              <div class="form-group">      
                <label class="bmd-label-floating">Observación</label> 
                {!! Form::text('observacion', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>    
          </div>
          
          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">      
                <label class="bmd-label-floating">1° Cargo</label> 
                {!! Form::text('cargo_1', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">                        
                <label class="bmd-label-floating">Descripción</label> 
              {!! Form::text('nombre_1', null, ['class' => 'form-control', 'style' => '']) !!} 
              </div>  
            </div>   
          </div> 
          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">      
                <label class="bmd-label-floating">2° Cargo</label> 
                {!! Form::text('cargo_2', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">                        
                <label class="bmd-label-floating">Descripción</label> 
              {!! Form::text('nombre_2', null, ['class' => 'form-control', 'style' => '']) !!} 
              </div>  
            </div>   
          </div>
          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">      
                <label class="bmd-label-floating">3° Cargo</label> 
                {!! Form::text('cargo_3', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">                        
                <label class="bmd-label-floating">Descripción</label> 
              {!! Form::text('nombre_3', null, ['class' => 'form-control', 'style' => '']) !!} 
              </div>  
            </div>   
          </div>
          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">      
                <label class="bmd-label-floating">4° Cargo</label> 
                {!! Form::text('cargo_4', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">                        
                <label class="bmd-label-floating">Descripción</label> 
              {!! Form::text('nombre_4', null, ['class' => 'form-control', 'style' => '']) !!} 
              </div>  
            </div>   
          </div>
          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">      
                <label class="bmd-label-floating">5° Cargo</label> 
                {!! Form::text('cargo_5', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">                        
                <label class="bmd-label-floating">Descripción</label> 
              {!! Form::text('nombre_5', null, ['class' => 'form-control', 'style' => '']) !!} 
              </div>  
            </div>   
          </div>
          <div class="row">  
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">      
                <label class="bmd-label-floating">6° Cargo</label> 
                {!! Form::text('cargo_6', null, ['class' => 'form-control', 'style' => '']) !!}
              </div>  
            </div>     
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
              <div class="form-group">                        
                <label class="bmd-label-floating">Descripción</label> 
              {!! Form::text('nombre_6', null, ['class' => 'form-control', 'style' => '']) !!} 
              </div>  
            </div>   
          </div> 
        </div>
      </div>
  </div> 
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif