@extends('layouts.app')
@section('contentheader_title', 'EDITAR REGISTRO DE FORMULARIO')
@section('htmlheader_title', 'EDITAR REGISTRO DE FORMULARIO')
 
@section('main-content')
<div class="content">
	<div class="col-md-12">
		<div class="card">
	        <div class="card-body"> 
				<form method="POST" action="{{ route('med_datos.update', $model->id) }}" autocomplete="off" id="form">
					{{ csrf_field() }}
      				<input name="_method" type="hidden" value="PATCH"> 
					@include($model->rutaview.'form')
				</form>
			</div>
		</div>
	</div>
</div>
@stop