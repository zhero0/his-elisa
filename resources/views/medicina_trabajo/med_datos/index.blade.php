@extends('layouts.app')
@section('contentheader_title', 'REGISTRO PERSONAL AUTORIZADO')
@section('htmlheader_title', 'REGISTRO PERSONAL AUTORIZADO')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="content">
	<div class="col-md-12">
	  	<div class="card">
	    	<div class="card-body">
	    		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
		 		<!-- "BUSCADOR MULTIPLE" -->
		 		<div class="row">
			 		<form id="formBuscador" class="navbar-form navbar-right" action="/med_datos" autocomplete="off">
			 			<textarea class="form-control" id="datosBuscador" name="datosBuscador" style="display: none;">{{ $model->datosBuscador }}</textarea>
			 		</form>
		 		</div>
		 		<!-- FIN "BUSCADOR MULTIPLE" --> 

	    		<div class="row">
					<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
						<a type="button" class="btn btn-success btn-sm" rel="tooltip" href="{{ route('med_datos.create') }}" title="Nuevo Registro">
				            <i class="fa fa-plus" aria-hidden="true"></i> Nuevo
				        </a>
				    </div>
				</div>
			 	<div class="table-responsive table-bordered table-hover" style="{{ config('app.styleIndex') }}">
					<table class="table table-condensed">
						<thead style="{{ config('app.styleTableHead') }}">
							<tr>
								<th>Observación</th> 								
								<th>1° Cargo</th>
								<th>Personal</th>						
								<th>2° Cargo</th>
								<th>Personal</th>						
								<th>3° Cargo</th>
								<th>Personal</th>						
								<th>4° Cargo</th>
								<th>Personal</th>						
								<th>5° Cargo</th>
								<th>Personal</th>						
								<th>Vigente</th>
								<th></th>
							</tr> 
							<tr id="indexSearch" style="background: #ffffff;">								
			 					<th>
			 						<input type="text" id="txtSearch_observacion" class="form-control input-sm" style="text-transform: uppercase;">
			 					</th> 
							</tr>
						</thead>
						<tbody>
							@foreach($model as $dato)
								<tr role="row" style="{{ config('app.styleTableRow') }}"> 
									<td>{{ $dato->observacion }}</td>						
									<td class="text-left">{{ $dato->cargo_1 }}</td>						
									<td class="text-left">{{ $dato->nombre_1 }}</td>						
									<td class="text-left">{{ $dato->cargo_2 }}</td>						
									<td class="text-left">{{ $dato->nombre_2 }}</td>						
									<td class="text-left">{{ $dato->cargo_3 }}</td>						
									<td class="text-left">{{ $dato->nombre_3 }}</td>						
									<td class="text-left">{{ $dato->cargo_4 }}</td>						
									<td class="text-left">{{ $dato->nombre_4 }}</td>						
									<td class="text-left">{{ $dato->cargo_5 }}</td>						
									<td class="text-left">{{ $dato->nombre_5 }}</td>						
									<td class="text-left">{{ $dato->eliminado == 0? 'Autorizado':'No vigente' }}</td>	 
									<td class="td-actions text-right"> 
										<a href="{{ route('med_datos.edit', $dato->id) }}" class="btn btn-default btn-sm btn-round" rel="tooltip" title="Editar">
												<i class="fa fa-pencil"></i>								
										</a>   					
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div> 
				<div class="text-center" style="{{ config('app.stylePaginacion') }}">
			      {!! $model->appends(Input::except('page'))->render() !!}
			    </div>
			</div>
	  	</div>
  	</div>
</div> 
@stop