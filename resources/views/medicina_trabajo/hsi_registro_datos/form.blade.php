<div class="form-group">
  <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
  <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
  <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
  <input type="hidden" id="idhcl_empleador" name="idhcl_empleador" value="{{ $model->idhcl_empleador }}">
  <input type="hidden" id="idhcl_poblacion" name="idhcl_poblacion" value="{{ $model->idhcl_poblacion }}"> 
  <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
  <input type="hidden" id="txtScenario" value="{{ $model->scenario }}"> 
  <input type="hidden" id="idHsi_agrupacion" value="{{ $model->idHsi_agrupacion }}">
  <input type="hidden" id="idhsi_division" value="{{ $model->idhsi_division }}">
  <!-- DIAGNOSTICOS -->
  <input type="hidden" id="txtcontenidoTablaC" name="txtcontenidoTablaC" value="{{ $model->documentodetalleC }}">

  <div class="row">  
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <label class="bmd-label">FECHA DE SOLICITUD</label> <br>  
      <input type="date" class="form-control datepicker" id="idfecha_registro" name="fecha_registro" value="{{  $model != null? $model->fecha_registro : '' }}">  
    </div> 
     <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      @if($model->eliminado == 1)
      <br>
        <label class="bmd-label"> <FONT COLOR="red"><h2>ELIMINADO</h2></FONT></label> <br>    
      @endif  
    </div>     
  </div>
  <br> 
  	@include($model->rutaview.'_dataContent')  
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="fa fa-undo" aria-hidden="true"></span> Volver
</a>
@if($model->validar == false)
  <button type="button" class="btn {{ $model->scenario == 'create'? 'btn-success':'btn-warning' }}  btn-md" rel="tooltip" title="Guardar" id="btnGuardar" onclick="guardar()">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> {{ $model->scenario == 'create'? 'Guardar':'Editar' }} 
  </button>
  @if($model->scenario != 'create') 
  <button type="button" class="btn btn-success btn-md" rel="tooltip" title="Guardar" id="btnGuardarValidar" onclick="validarRegistro()">
    <span class="fa fa-floppy-o" aria-hidden="true"></span> Validar
  </button>
  @endif     
@endif  
<button type="button" class="btn btn-danger btn-md" rel="tooltip" style="display: {{ $model->validar == false? 'none':'visible' }} " title="eliminar" id="btnEliminar" onclick="eliminarRegistro()">
  <span class="fa fa-trash" aria-hidden="true"></span> Eliminar
</button> 