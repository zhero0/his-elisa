@foreach($model as $dato)
    <tr role="row" style="{{ config('app.styleTableRow') }}"> 
        <td class="text-left">{{ $dato->res_fecha_registro }}</td>                      
        <td class="text-left">{{ $dato->res_documento }}</td>  
        <td class="text-left">{{ $dato->res_numero_historia.'-'.$dato->res_primer_apellido.' '.$dato->res_segundo_apellido.' '.$dato->res_nombre }}</td> 
        <td class="text-left">{{ $dato->res_edad }}</td>
        <td class="text-left">{{ $dato->res_nro_empleador.'-'.$dato->res_nombre_empresa }}</td>
        <td class="text-left">{{ $dato->res_especialidad }}</td>
        <td class="text-left">{{ $dato->res_apellidos.' '.$dato->res_nombres }}</td>         
        <td>
            Estado
            <small class="{{ $dato->res_eliminado == 0? 'text-success' : 'text-danger' }} mr-1">
            <i class="fas {{ $dato->res_eliminado == 0? 'fa-arrow-up' : 'fa-arrow-down' }} "></i>
            {{ $dato->res_eliminado == 0? 'Activo' : 'Inactivo' }}
            </small>
        </td>                   
        <td class="td-actions text-right"> 
            <div class="btn-group">
                <a href="{{ route('hsi_registro_datos.edit', $dato->id) }}" style="display: {{ $dato->res_eliminado == 1? 'none':'' }};" class="btn {{ $dato->res_eliminado == true? 'btn-outline-danger':'btn-outline-warning' }} btn-sm" rel="tooltip" title="{{ $dato->res_eliminado == true? 'eliminar':'editar' }}">
                    <i class="{{ $dato->res_eliminado == true? 'fa fa-trash':'fa fa-edit' }}"></i>                                
                </a>                  
            </div>                                  
        </td> 
    </tr>
@endforeach
 