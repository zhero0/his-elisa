<?php
use \App\Models\Medicina_trabajo\Hsi_tipo_registro;
?>
<div class="callout callout-danger">
<h5>Datos del Paciente</h5>
  <div class="row">
    <input type="hidden" id="sexo" name="sexo" value="{{ $modelPoblacion->sexo }}"> 
      <div class="col-12 col-sm-6 col-md-6 col-lg-6">
        <div class="panel panel-default">
            <button type="button" class="btn btn-default btn-round btn-sm" 
                title="Click para Buscar"
                onclick="abrirVentanaMed(1, 'POBLACION', 'buscaPoblacion')">
            <i class="fa fa-user-o" aria-hidden="true"></i>
              PACIENTE (*)
            </button>
            <div class="form-group">
              <input type="text" class="form-control" id="poblacion" name="poblacion" 
                value="{{ $model->paciente }}" readonly="" onclick="abrirVentanaMed(1, 'POBLACION', 'buscaPoblacion')">
            </div>
        </div>  
      </div>
      <div class="col-12 col-sm-6 col-md-6 col-lg-6">
         <label class="bmd-label">Matricula</label> <br>  
          <input type="text" class="form-control" id="res_numero_historia" name="res_numero_historia" value="{{ $modelPoblacion->numero_historia }} " readonly="">
      </div>
      <div class="col-12 col-sm-4 col-md-4 col-lg-4">
         <label class="bmd-label">Fecha de nacimiento</label> <br>  
          <input type="text" class="form-control" id="res_fecha_nacimiento" name="res_fecha_nacimiento" value=" {{ $modelPoblacion->fecha_nacimiento }} " readonly="">
      </div>
      <div class="col-12 col-sm-4 col-md-4 col-lg-4">
         <label class="bmd-label">Edad</label> <br>  
          <input type="text" class="form-control" id="res_edad" name="res_edad" value=" {{ $modelPoblacion->edad }} " readonly="">
      </div>
      <div class="col-12 col-sm-4 col-md-4 col-lg-4">
         <label class="bmd-label">Sexo</label> <br>  
          <input type="text" class="form-control" id="res_sexo" name="res_sexo" value=" {{ $modelPoblacion->sexo == 1? 'MASCULINO':'FEMENINO' }} " readonly="">
      </div>

      <div class="col-12 col-sm-6 col-md-6 col-lg-6"> 
        <br>
        <div class="panel panel-default" id="divEmpleador">
            <button type="button" class="btn btn-default btn-round btn-sm" 
                title="Click para Buscar"
                onclick="abrirVentanaMed(2, 'EMPLEADOR', 'buscaEmpleador')">
              <i class="fa fa-users" aria-hidden="true"></i>
              EMPLEADOR (*)
            </button>
            <div class="form-group">
              <input type="text" class="form-control" id="empleador" name="empleador" 
                value="{{ $modelEmpleador->nombre }}" readonly="" onclick="abrirVentanaMed(2, 'EMPLEADOR', 'buscaEmpleador')">
            </div>
        </div>
      </div>

      <div class="col-12 col-sm-6 col-md-6 col-lg-6">
        <br>
         <label class="bmd-label">Nro. Empleador</label> <br>  
          <input type="text" class="form-control" id="res_nro_empleador" name="res_nro_empleador" value=" {{ $modelEmpleador->nro_empleador }} " readonly="">
      </div>
      <div class="col-12 col-sm-4 col-md-4 col-lg-4">
         <div class="panel panel-default" id="divEmpleador">
            <button type="button" class="btn btn-default btn-round btn-sm" 
                title="Click para Buscar"
                onclick="abrirVentanaMed(3, 'Datos actividad economica', 'buscaActividad')">
              <i class="fa fa-users" aria-hidden="true"></i>
              RUBRO (*)
            </button>
            <div class="form-group">
              <input type="text" class="form-control" id="datoActividad" name="datoActividad" 
                value="{{ $model->rubro }}" readonly="" onclick="abrirVentanaMed(3, 'Datos actividad economica', 'buscaActividad')">
            </div>
        </div>
      </div>
      <div class="col-12 col-sm-4 col-md-4 col-lg-4">
         <label class="bmd-label">Actividad economica</label> <br>  
          <input type="text" class="form-control" id="datoAgrupacion" name="datoAgrupacion" value=" {{ $model->actividad_economica }} " readonly="">
      </div>
      <div class="col-12 col-sm-8 col-md-4 col-lg-4">
         <label class="bmd-label">Ocupación</label> <br>  
          <input type="text" class="form-control" id="txtglosa" name="txtglosa" value=" {{ $model->glosa }}">
      </div>
  </div>
</div>  
<div class="callout callout-info">
  <h5>Registro DAT - Datos del Accidente</h5>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-3"></div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label class="bmd-label">FECHA</label> <br>  
      <input type="date" class="form-control datepicker" id="fecha_accidente" name="fecha_accidente" value="{{  $model != null? $model->fecha_accidente : '' }}">
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label>CAUSA</label>
      <br>
      <select name="idhsi_causa" id="idhsi_causa" class="form-control"> 
          <option disabled selected>Seleccionar</option> 
          @foreach($modelCausa as $dato) 
            @if($model != null)
              <option value="{{$dato->id}}" {{ $model->idhsi_causa == $dato->id ? 'selected="selected"' : ''  }} >{{ '('.$dato->codigo.')'.$dato->nombre }}</option> 
            @else
              <option value="{{$dato->id}}">{{ '('.$dato->codigo.')'.$dato->nombre }}</option> 
            @endif
          @endforeach
      </select>   
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label>LUGAR</label>
      <br>
      <select name="idhsi_area" id="idhsi_area" class="form-control"> 
          <option disabled selected>Seleccionar</option> 
          @foreach($modelArea as $dato) 
            @if($model != null)
              <option value="{{$dato->id}}" {{ $model->idhsi_area == $dato->id ? 'selected="selected"' : ''  }} >{{ '('.$dato->codigo.')'.$dato->nombre }}</option> 
            @else
              <option value="{{$dato->id}}">{{ '('.$dato->codigo.')'.$dato->nombre }}</option> 
            @endif
          @endforeach
      </select>   
    </div> 
  </div> 
  <br>
  <h5>Registro - Vigencia de Derechos</h5>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-3"></div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
      <label class="bmd-label">Fecha</label> <br>  
      <input type="date" class="form-control datepicker" id="idfecha_registro" name="fecha_registro" value="{{  $model != null? $model->fecha_registro : '' }}">
    </div> 
    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
      <div class="form-group">
        <label class="bmd-label">Fatalidad</label> <br>  
        <div class="form-check">
          <input class="form-check-input" type="radio" name="radio" id="radioSi" value='1'>
          <label class="form-check-label">Si</label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="radio" id="radioNo" checked="" value="2">
          <label class="form-check-label">No</label>
        </div> 
      </div>
    </div>
  </div>
</div> 
<div class="card card-info">
  <div class="card-header">
    <h3 class="card-title">Recepción en Higiene y Seguridad industrial</h3>
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <label class="bmd-label">Fecha</label> <br>  
        <input type="date" class="form-control datepicker" id="fecha_vigencia" name="fecha_vigencia" value="{{  $model != null? $model->fecha_vigencia : '' }}">
      </div> 
      <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1"></div>
      <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
        <div class="form-group">
          <label class="bmd-label">Plazo</label> <br>  
          <div class="form-check">
            <input class="form-check-input" type="radio" name="radioPlazo" id="plazoSi" value='1'>
            <label class="form-check-label">Si</label>
          </div>
          <div class="form-check">
            <input class="form-check-input" type="radio" name="radioPlazo" id="plazoNo" checked="" value="2">
            <label class="form-check-label">No</label>
          </div> 
        </div>
      </div>
      <div class="col-12 col-sm-8 col-md-4 col-lg-4">
         <label class="bmd-label">Diagnostico</label> <br>  
          <input type="text" class="form-control" id="glosa_diagnostico" name="glosa_diagnostico" value=" {{ $model->glosa_diagnostico }}">
      </div>
    </div>
     
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
        @foreach($modelHsi_tipo_registro as $datoA) 
          @if( $datoA->id == Hsi_tipo_registro::A )
            <h5>{{ '('.$datoA->codigo.') '.$datoA->nombre }}</h5>
          @endif
        @endforeach
        <select name="idhsi_area" id="idhsi_area" class="form-control"> 
          <option disabled selected>Seleccionar</option> 
          @foreach($modelHsi_tipo as $datoTipo) 
            @if( $datoTipo->idhsi_tipo_registro == Hsi_tipo_registro::A )
              <option value="{{$datoTipo->id}}" {{ $model->idhsi_area == $datoTipo->id ? 'selected="selected"' : ''  }} >{{ '('.$datoTipo->codigo.')'.$datoTipo->nombre }}</option>  
            @endif
          @endforeach
        </select>  
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
        @foreach($modelHsi_tipo_registro as $datoA) 
          @if( $datoA->id == Hsi_tipo_registro::B )
            <h5>{{ '('.$datoA->codigo.') '.$datoA->nombre }}</h5>
          @endif
        @endforeach
        <select name="idhsi_area" id="idhsi_area" class="form-control"> 
          <option disabled selected>Seleccionar</option> 
          @foreach($modelHsi_tipo as $datoTipo) 
            @if( $datoTipo->idhsi_tipo_registro == Hsi_tipo_registro::B )
              <option value="{{$datoTipo->id}}" {{ $model->idhsi_area == $datoTipo->id ? 'selected="selected"' : ''  }} >{{ '('.$datoTipo->codigo.')'.$datoTipo->nombre }}</option>  
            @endif
          @endforeach
        </select>  
      </div>      
    </div>
    <br>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
        @foreach($modelHsi_tipo_registro as $datoA) 
          @if( $datoA->id == Hsi_tipo_registro::C )
            <h5>{{ '('.$datoA->codigo.') '.$datoA->nombre }}</h5>
          @endif
        @endforeach 
        <div class="col-xs-12"> 
          <div class="input-group">
            <input type="text" id="txtBuscarC" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
            <span class="input-group-btn">
              <button class="btn btn-danger" type="button" id="btnBuscarC" value="{{ Hsi_tipo_registro::C }}">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                Buscar
              </button>
            </span>
          </div>
          <div class="table-responsive" style="height: 200px; overflow-y: auto; border: 1px solid #374850;">
            <table id="tablaContenedorC" class="table table-sm">
                <thead style="{{ config('app.styleTableHead') }}">
                    <tr>
                      <th>Datos</th>
                      <th></th>
                   </tr>
                </thead>
                <tbody id="tbodyC" style="{{ config('app.styleTableRow') }}">
                  <tr>
                    <td>Vacío...</td>
                  </tr>
                </tbody>
            </table>
          </div> 
          <div class="row">
            <div class="col-xs-4">
              <button class="btn btn-default" id="btnVisualizaC" type="button"  title="Click para visualizar los datos seleccionados">
                  Total Seleccionados <span class="badge" id="spanTotalC">0</span>
              </button>
            </div>
          </div> 
        </div>  
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
        @foreach($modelHsi_tipo_registro as $datoA) 
          @if( $datoA->id == Hsi_tipo_registro::D )
            <h5>{{ '('.$datoA->codigo.') '.$datoA->nombre }}</h5>
          @endif
        @endforeach 
        <div class="col-xs-12"> 
          <div class="input-group">
            <input type="text" id="txtBuscarD" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
            <span class="input-group-btn">
              <button class="btn btn-danger" type="button" id="btnBuscarD" value="{{ Hsi_tipo_registro::D }}">
                <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                Buscar
              </button>
            </span>
          </div>
          <div class="table-responsive" style="height: 200px; overflow-y: auto; border: 1px solid #374850;">
            <table id="tablaContenedorD" class="table table-sm">
                <thead style="{{ config('app.styleTableHead') }}">
                    <tr>
                      <th>Datos</th>
                      <th></th>
                   </tr>
                </thead>
                <tbody id="tbodyD" style="{{ config('app.styleTableRow') }}">
                  <tr>
                    <td>Vacío...</td>
                  </tr>
                </tbody>
            </table>
          </div> 
          <div class="row">
            <div class="col-xs-4">
              <button class="btn btn-default" id="btnVisualizaD" type="button"  title="Click para visualizar los datos seleccionados">
                  Total Seleccionados <span class="badge" id="spanTotalD">0</span>
              </button>
            </div>
          </div> 
        </div>   
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2"></div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
        @foreach($modelHsi_tipo_registro as $datoA) 
          @if( $datoA->id == Hsi_tipo_registro::E )
            <h5>{{ '('.$datoA->codigo.') '.$datoA->nombre }}</h5>
          @endif
        @endforeach
        <select name="idhsi_area" id="idhsi_area" class="form-control"> 
          <option disabled selected>Seleccionar</option> 
          @foreach($modelHsi_tipo as $datoTipo) 
            @if( $datoTipo->idhsi_tipo_registro == Hsi_tipo_registro::E )
              <option value="{{$datoTipo->id}}" {{ $model->idhsi_area == $datoTipo->id ? 'selected="selected"' : ''  }} >{{ '('.$datoTipo->codigo.')'.$datoTipo->nombre }}</option>  
            @endif
          @endforeach
        </select>  
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"> 
        @foreach($modelHsi_tipo_registro as $datoA) 
          @if( $datoA->id == Hsi_tipo_registro::F )
            <h5>{{ '('.$datoA->codigo.') '.$datoA->nombre }}</h5>
          @endif
        @endforeach
        <select name="idhsi_area" id="idhsi_area" class="form-control"> 
          <option disabled selected>Seleccionar</option> 
          @foreach($modelHsi_tipo as $datoTipo) 
            @if( $datoTipo->idhsi_tipo_registro == Hsi_tipo_registro::F )
              <option value="{{$datoTipo->id}}" {{ $model->idhsi_area == $datoTipo->id ? 'selected="selected"' : ''  }} >{{ '('.$datoTipo->codigo.')'.$datoTipo->nombre }}</option>  
            @endif
          @endforeach
        </select>  
      </div>
    </div>
  </div> 
</div>