<div class="modal fade" id="vModalBuscador_poblacion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title">Datos Personal</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>           
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input type="text" id="txtBuscadorPaciente" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarPaciente">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>
        </div> 
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatosPaciente" class="table table-bordered table-hover table-condensed">
            <thead style="{{ config('app.styleTableHead') }}">
              <tr>
                <th>Nro Historial</th>
                <th>Documento</th>
                <th>Apellidos</th> 
                <th>Nombres</th>
                <th></th>
              </tr>
            </thead>
            <tbody class="table-success" id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
              <tr id="idListaVaciaPaciente">
                <td>Lista Vacía...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div>
 
<div class="modal fade" id="vModalBuscador_empleador">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Datos Empleador</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>           
      </div>
      <div class="modal-body">
        <div class="input-group">
          <input type="text" id="txtBuscadorEmpleador" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
          <span class="input-group-btn">
            <button class="btn btn-warning" type="button" id="btnBuscarEmpleador">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              Buscar
            </button>
          </span>
        </div>              
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatosEmpleador" class="table table-bordered table-hover table-condensed">
            <thead style="{{ config('app.styleTableHead') }}">
              <tr>
                <th class="col-xs-2">Nro Empleador</th>
                <th class="col-xs-4">Nombre</th> 
                <th></th>
              </tr>
            </thead>
            <tbody class="table-success" id="tbodyEmpleador" style="{{ config('app.styleTableRow') }}">
              <tr id="idListaVaciaEmpleador">
                <td>Lista Vacía...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div>

 
<div class="modal fade" id="vModalBuscador_actividad">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Datos actividad economica</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>           
      </div>
      <div class="modal-body"> 
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">    
          <label>Seleccione actividad economica</label>
          <br>
          <select name="idhsi_actividad_economica_agrupacion" id="idhsi_actividad_economica_agrupacion" class="form-control" onchange="seleccionarClasificacion()"> 
              <option disabled selected>Seleccionar</option> 
              @foreach($modelActividad as $dato)  
                  <option value="{{$dato->id}}">{{$dato->codificacion.' - '.$dato->nombre}}</option>  
              @endforeach
          </select>   
        </div> 
        <br>     
        <div class="table-responsive" style="height: 350px; overflow-y: auto;">
          <table id="tablaContenedorDatosEmpleador" class="table table-bordered table-hover table-condensed">
            <thead style="{{ config('app.styleTableHead') }}">
                <tr> 
                  <th></th>
                  <th></th>
                </tr>
            </thead>
            <tbody class="table-success" id="tbodyActividad" style="{{ config('app.styleTableRow') }}">
              <tr id="idListaVaciaActividad">
                <td>Lista Vacía...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">
          <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
          Cerrar
        </button>
      </div>
    </div>
  </div>
</div>