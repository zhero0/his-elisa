@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTADO - DENUNCIA DE ACCIDENTE DE TRABAJO (DAT)</h1>
@stop   

@section('content')
<div class="card"> 
	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
	<input type="hidden" id="txtAccion" value="{{ $model->accion }}"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('hsi_registro_datos.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left;"> 
		    </div>
		    <form class="navbar-form navbar-right" roles="searchNuevo" action="/hsi_registro_datos" autocomplete="off">
	        	<div class="row text-right">
	        		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
	        			<input type="text" id="matricula" name="matricula" class="form-control" placeholder="Matricula" style="text-transform: uppercase;" autofocus="">    
		        	</div>
		        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		        		<input type="text" id="primer_apellido" name="primer_apellido" class="form-control" placeholder="AP. PATERNO" style="text-transform: uppercase;"> 
		        	</div>
		        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		        		<input type="text" id="segundo_apellido" name="segundo_apellido" class="form-control" placeholder="AP. MATERNO" style="text-transform: uppercase;">  
		        	</div>
		        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		        		<input type="text" id="nombres" name="nombres" class="form-control" placeholder="NOMBRES" style="text-transform: uppercase;"> 
		        	</div> 
		        	<button type="submit" class="btn btn-primary"> Buscar </button>
	        	</div>  
	        </form> 
		</div>
		<!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> -->
	                 
        <!-- </div> --> 
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Fecha</th> 								
					<th>Documento</th>
					<th>Paciente</th>
					<th>Edad</th>
					<th>Empleador</th>
					<th>Especialidad</th>
					<th>Medico</th>
					<th>Vigente</th>					
					<th></th>
				</tr> 
			</thead>
			<tbody id="indexContent">
				@include($model->rutaview.'indexContent')
			</tbody>
		</table>
	</div>  
</div>
@include($model->rutaview.'indexModal')
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 