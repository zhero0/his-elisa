<div class="modal fade" id="vImprimeSolicitud">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">IMPRESIÓN</h4>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>        
      </div>
      <div class="modal-body">
       	<div id="imprimeSolicitud" style="height: 370px;"></div>
        
  	 		<div class="progress" style="display: none;">
  			  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
  			  </div>
  			</div>
      </div>
      <div class="modal-footer"> 
      </div>
    </div>
  </div>
</div>