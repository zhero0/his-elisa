<div id="tableContent" class="table-responsive table-bordered table-hover" style="max-height: 330px; overflow-y: auto;">
    <table class="table table-condensed">
        <thead style="{{ config('app.styleTableHead') }}">
            <tr>
                <th>Nro Historial</th>
                <th>Documento</th>
                <th>Apellido paterno</th>
                <th>Apellido materno</th>
                <th>Nombres</th>
                <th></th>
           </tr>
        </thead>
        <tbody id="tbodyContent">
            @if($modelo != null)
                @for($i = 0; $i < count($modelo); $i++)
                    <tr id="row{{ $i }}" 
                        ondblclick="modalSeleccion( {{ $i }} )" 
                        style="{{ config('app.styleTableRow') }}">
                        <td style="display: none;" id="data{{ $i }}">{{ json_encode($modelo[$i]) }}</td>                    
                        <td class="text-left">{{ $modelo[$i]->numero_historia }}</td>
                        <td class="text-left">{{ $modelo[$i]->documento }}</td>
                        <td class="text-left">{{ $modelo[$i]->primer_apellido }}</td>
                        <td class="text-left">{{ $modelo[$i]->segundo_apellido }}</td>
                        <td class="text-left">{{ $modelo[$i]->nombre }}</td>
                        <td class="td-actions text-right">
                            <span class="fa fa-thumbs-up" aria-hidden="true" style="cursor: pointer;" 
                                    title="Click para Seleccionar" 
                                    onclick="modalSeleccion( {{ $i }} )"></span>
                        </td>
                    </tr>
                @endfor
            @endif            
        </tbody>
    </table>
</div>