<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">    
  <label>TIPO DE CANCELACION</label>
  <br>
  <select name="idmed_tipo_cancelacion" id="idmed_tipo_cancelacion" class="form-control" onchange="habilitaRegistro()"> 
      <option disabled selected>Seleccionar</option> 
      @foreach($modelCancelacion as $dato) 
        @if($model != null)
          <option value="{{$dato->id}}" {{ $model->idmed_tipo_cancelacion == $dato->id ? 'selected="selected"' : ''  }} >{{$dato->nombre }}</option> 
        @else
          <option value="{{$dato->id}}">{{$dato->nombre}}</option> 
        @endif
      @endforeach
  </select> 	
</div>   
<br>
<div class="row" id="contenedor_cancelacion">
	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">   
	  	<label class="bmd-label">Nro Deposito</label> <br>  
		<input type="text" class="form-control" id="txtnro_deposito" name="nro_deposito" value="{{ $modelDeposito != null? $modelDeposito->nro_deposito : '' }}" readonly="">	
	</div>
	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">        
	  	<label class="bmd-label">Banco Deposito</label> <br>  
		<input type="text" class="form-control" id="txtbanco_deposito" name="banco_deposito" value="{{ $modelDeposito != null? $modelDeposito->banco_deposito : '' }}" readonly="">	
	</div>
	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">         
	  	<label class="bmd-label">Fecha Deposito</label> <br>  
		<input type="date" class="form-control datepicker" id="txtfecha_deposito" name="fecha_deposito" value="{{  $modelDeposito != null? $modelDeposito->fecha_deposito : '' }}" readonly=""> 
	</div>
	<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">        
	  	<label class="bmd-label">Persona Depositante</label> <br>  
		<input type="text" class="form-control" id="txtidentificacion" name="identificacion" value="{{  $modelDeposito != null?$modelDeposito->identificacion : '' }}" readonly="">	
	</div>
</div>
