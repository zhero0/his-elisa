@foreach($model as $dato)
    <tr role="row" style="{{ config('app.styleTableRow') }}"> 
        <td class="text-left">{{ $dato->res_fecha_registro }}</td>                      
        <td class="text-left">{{ $dato->res_nro_folio }}</td> 
        <td class="text-left">{{ $dato->res_primer_apellido }}</td>
        <td class="text-left">{{ $dato->res_segundo_apellido }}</td>
        <td class="text-left">{{ $dato->res_nombre_paciente }}</td>
        <td class="text-left">{{ $dato->res_documento }}</td>
        <td class="text-left">{{ $dato->res_nro_empleador }}</td>
        <td class="text-left">{{ $dato->res_nombre_empresa }}</td>
        <td class="text-left">{{ $dato->res_nombre_clasificacion }}</td>
        <td class="text-left">{{ $dato->res_tipo_pago }}</td>          
        <td class="text-left">{{ $dato->res_usuario }}</td>                     
        <td class="td-actions text-right"> 
            <div class="btn-group">
                <a href="{{ route('med_registro.edit', $dato->id) }}" style="display: {{ $dato->res_eliminado == 1? 'none':'' }};" class="btn {{ $dato->res_validar == true? 'btn-outline-danger':'btn-outline-warning' }} btn-sm" rel="tooltip" title="{{ $dato->res_validar == true? 'eliminar':'editar' }}">
                    <i class="{{ $dato->res_validar == true? 'fa fa-trash':'fa fa-edit' }}"></i>                                
                </a> 
                <button class="btn btn-block {{ $dato->res_eliminado == 1? 'btn-outline-danger':'btn-outline-success' }} btn-sm" {{ $dato->res_validar == true? '':'disabled' }} onclick="imprimirSolicitud( {{ $dato->id }} )"  rel="tooltip" >
                <i class="fa fa-print"></i>
                </button>
            </div>                                  
        </td> 
    </tr>
@endforeach