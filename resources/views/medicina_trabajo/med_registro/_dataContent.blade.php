<div class="col-12 col-sm-6 col-md-6 col-lg-6">
  <div class="panel panel-default">
      <button type="button" class="btn btn-default btn-round btn-sm" 
          title="Click para Buscar"
          onclick="abrirVentanaMed(1, 'POBLACION', 'buscaPoblacion')">
      <i class="fa fa-user-o" aria-hidden="true"></i>
        PACIENTE (*)
      </button>
      <div class="form-group">
        <input type="text" class="form-control" id="poblacion" name="poblacion" 
          value="{{ $model->paciente }}" readonly="" onclick="abrirVentanaMed(1, 'POBLACION', 'buscaPoblacion')">
      </div>
  </div>  
</div>
<div class="col-12 col-sm-6 col-md-6 col-lg-6"> 
  <div class="panel panel-default" id="divEmpleador">
      <button type="button" class="btn btn-default btn-round btn-sm" 
          title="Click para Buscar"
          onclick="abrirVentanaMed(3, 'EMPLEADOR', 'buscaEmpleador')">
        <i class="fa fa-users" aria-hidden="true"></i>
        EMPLEADOR (*)
      </button>
      <div class="form-group">
        <input type="text" class="form-control" id="empleador" name="empleador" 
          value="{{ $model->hcl_empleador_nombre }}" readonly="" onclick="abrirVentanaMed(3, 'EMPLEADOR', 'buscaEmpleador')">
      </div>
  </div>
</div>