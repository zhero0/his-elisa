@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTA DE REGISTROS DE FORMULARIOS</h1>
@stop   

@section('content')
<div class="card"> 
	<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
	<input type="hidden" id="txtAccion" value="{{ $model->accion }}"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('med_registro.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo registro
		        </a>
		    </div> 
		    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1" style="text-align: left;"> 
		    </div>
		    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		        <form class="navbar-form navbar-right" roles="searchNuevo" action="/med_registro" autocomplete="off">
		        	<div class="row text-right">
		        		<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
		        			<input type="text" id="documento" name="documento" class="form-control" placeholder="Nro. de documento" style="text-transform: uppercase;" autofocus="">    
			        	</div>
			        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			        		<input type="text" id="primer_apellido" name="primer_apellido" class="form-control" placeholder="AP. PATERNO" style="text-transform: uppercase;"> 
			        	</div>
			        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			        		<input type="text" id="segundo_apellido" name="segundo_apellido" class="form-control" placeholder="AP. MATERNO" style="text-transform: uppercase;">  
			        	</div>
			        	<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
			        		<input type="text" id="nombres" name="nombres" class="form-control" placeholder="NOMBRES" style="text-transform: uppercase;"> 
			        	</div>
			        	<button type="submit" class="btn btn-primary"> Buscar </button>
		        	</div>  
		        </form>         
	        </div> 
		</div>
	</div>
 
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Fecha</th> 								
					<th>Nro Folio</th>
					<th>Apellido Paterno</th>
					<th>Apellido Materno</th>
					<th>Nombres</th>
					<th>Nro Documento</th>
					<th>Nro Empleador</th>						
					<th>Empleador</th> 
					<th>Formulario</th>
					<th>Cancelación</th>
					<th>Responsable</th>
					<th></th>
				</tr> 
			</thead>
			<tbody id="indexContent">
				@include($model->rutaview.'indexContent')
			</tbody>
		</table>
	</div>  
</div>
@include($model->rutaview.'indexModal')
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 