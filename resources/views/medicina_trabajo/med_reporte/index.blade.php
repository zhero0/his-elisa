@extends('layouts.app')
@section('contentheader_title', ' ')
@section('htmlheader_title', ' ')

{!! Charts::assets() !!}
{!! Charts::styles() !!} 

@section('main-content')
<div class="content">
  <div class="col-md-12">
      <div class="card">
        <div class="card-body">
          <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="idalmacen" id="idalmacen" value="{{ $model->idalmacen }}">
          
          <ul id="idUl_opcionesFlotante" class="nav nav-pills nav-pills-rose" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#tab_reportes" onclick="scroll_Inicio()" role="tablist">REPORTES</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#tab_graficas" onclick="scroll_Inicio()" role="tablist">GRÁFICAS</a>
            </li>
          </ul>
          
          <div class="tab-content tab-space">
            <div class="tab-pane active" id="tab_reportes">
              @include($model->rutaview.'tabReportes')
            </div>
            <div class="tab-pane" id="tab_graficas">
              @include($model->rutaview.'tabGraficas')
            </div>
          </div>
        </div>
      </div>
    </div>
</div>


@include($model->rutaview.'indexModal')
@stop 


{!! Charts::scripts() !!}
{!! $chart->script() !!}
{!! $chart2->script() !!}
{!! $chart3->script() !!}
{!! $chart4->script() !!}