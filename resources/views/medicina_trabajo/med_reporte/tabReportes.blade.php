<?php
	use \App\Models\Imagenologia\Ima_reporte;
  
	$estilo1 = 'style="color: rgb(0, 166, 90); font-weight: bold; font-size: 15px; cursor: pointer;"';
	$estilo2 = 'style="color: rgba(42, 64, 63, 0.88); font-size: 15px; cursor: pointer;"';
?>
<div class="row panelIndex">
	<div class="col-md-3 col-lg-3"></div>
	<div class="col-md-6 col-lg-6">
		<button type="button" id="btnImprimir" class="btn btn-success">
	  	<span class="fa fa-print" aria-hidden="true"></span> Visualizar Reporte
		</button>
		<button type='button' id="btnLimpiarDatos" class='btn btn-danger'>
      <span class='fa fa-ban' style='vertical-align: middle;'></span> Limpiar Campos
    </button>
	</div>
</div>

<div class="row" id="espacio_panelIndex"></div>
<div class="row">
	<div class="col-md-4 col-lg-4" id="contenedorReportes">
    <a <?= $estilo1 ?> value='{{ Ima_reporte::opc_placasUtilizadas }}'>
      <span class="fa fa-check"></span>
    	Detalle de Placas Utilizadas
    </a>
    <br>
    <a <?= $estilo2 ?> value='{{ Ima_reporte::opc_tipoPlaca }}'>
      <span class="fa fa-minus"></span>
    	Tipo Placa
    </a>
    <br>
  </div>
  
	<div class="col-md-8 col-lg-8 panelIndex">
    <font style="font-size: 17px; font-weight: bold;">PARÁMETROS BÚSQUEDA</font>
		<div id="div{{ Ima_reporte::opc_placasUtilizadas }}" style="display: block;">
      <form id="form{{ Ima_reporte::opc_placasUtilizadas }}">
        @include($model->rutaview.'_detallePlacas')
      </form>
    </div>
    <div id="div{{ Ima_reporte::opc_tipoPlaca }}" style="display: none;">
      <form id="form{{ Ima_reporte::opc_tipoPlaca }}">
        @include($model->rutaview.'_tipoPlaca')
      </form>
    </div>
	</div>
</div>