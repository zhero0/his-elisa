<div class="row">
  <div class="col-md-6 col-lg-6">
    <label>Tipo Placa</label>
    <div class="form-group">
      <select class="form-control bordeado" name="tipoPlaca">
        <option value="0"></option>
        @foreach($model->Ima_tipo_placas as $dato)
          <option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
        @endforeach 
      </select>
    </div>
  </div>
</div>

<br><br>
<div class="row">
  <div class="col-md-4 col-lg-4">
    <div class="form-group">
      <label>Fecha Desde</label>
      <input type="text" class="form-control datepicker" name="fechaDesde" value="{{ $model->fechaInicio }}">
    </div>
  </div>
  <div class="col-md-4 col-lg-4">
    <div class="form-group">
      <label>Fecha Hasta</label>
      <input type="text" class="form-control datepicker" name="fechaHasta" value="{{ $model->fechaFin }}">
    </div>
  </div>
</div>