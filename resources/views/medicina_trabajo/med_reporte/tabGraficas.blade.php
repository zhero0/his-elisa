<ul class="timeline timeline-inverse">
  <!-- timeline time label -->
  <?php $fecha_resultado = date("d").'-'.date("m").'-'.date("Y"); ?>
  <li class="time-label">
        <span class="bg-red">
         {{ $fecha_resultado }}
        </span>
  </li>
  
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			{!! $chart->render() !!}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			{!! $chart2->render() !!}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			{!! $chart3->render() !!}
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
			{!! $chart4->render() !!}
		</div>
	</div> 
</ul>