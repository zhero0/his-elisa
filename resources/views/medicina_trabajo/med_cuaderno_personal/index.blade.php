@extends('adminlte::page')
@section('title', 'LISTA DE ESPECIALIDADES')
@section('content_header')
    <h1>LISTA DE ESPECIALIDADES</h1>
@stop  
 
@section('content')
<div class="card"> 
	<div class="card-header">
	 	<div class="row"> 
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<!-- <div class="row" style="height: 6px; color: transparent;">.</div> -->
		        <a type="button" class="btn btn-block btn-outline-primary" href="{{ route('med_cuaderno_personal.create') }}">
		            <span class="fa fa-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>  
		</div>
	</div>  
	<div class="card-body"> 
		<table id="table_cuaderno" class="table table-striped">
			<thead>
				<tr> 
 					<th>Especialidad</th>
 					<th>Medico</th> 
 					<th>Vigente</th>
 					<th>Usuario</th>
 					<th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($model as $dato)					
					<tr role="row">
						<td class="col-xs-2">{{ $dato->res_especialidad }}</td>
						<td class="col-xs-6">{{ $dato->res_apellidos.' '.$dato->res_nombres }}</td>  
						<td>
							Estado
							<small class="{{ $dato->res_eliminado == 0? 'text-success' : 'text-danger' }} mr-1">
							<i class="fas {{ $dato->res_eliminado == 0? 'fa-arrow-up' : 'fa-arrow-down' }} "></i>
							{{ $dato->res_eliminado == 0? 'Activo' : 'Inactivo' }}
							</small>							
						</td>
						<td class="col-xs-1">{{ $dato->res_usuario }}</td> 
						<td  class="col-xs-1">
							<div class="btn-group">
				                <a href="{{ route('med_cuaderno_personal.show', $dato->id) }}" style="display: {{ $dato->res_eliminado == 1? 'none':'' }};" class="btn btn-outline-danger btn-sm" rel="tooltip" title="REMOVER MEDICO">
				                    <i class="fa fa-trash"></i>                                
				                </a>  
				            </div> 
						</td>
					</tr>					
				@endforeach
			</tbody>
		</table>
	</div> 
</div>
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#table_cuaderno').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection
 