@extends('adminlte::page') 
@section('title', 'EDITAR REGISTRO')  
@section('content_header')
    <h1>EDITAR REGISTRAR MEDICO DE CONSULTORIO</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="panel panel-primary">
			<div class="panel-body">   
				{!! Form::model($model, ['route' => ['cuaderno_personal.update', $model->id], 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@include($model->rutaview.'modal')