@section('main-content')
@extends('adminlte::page') 
@section('title', 'NUEVO REGISTRO')  
@section('content_header')
    <h1>REGISTRAR MEDICO DE CONSULTORIO</h1>
@stop
 
@section('content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
    	<div class="card">
			<div class="card-body">    	
				<form method="POST" action="{{ route('med_cuaderno_personal.store') }}" autocomplete="off" id="form">
					{{ csrf_field() }}
					@include($model->rutaview.'form')
					@include($model->rutaview.'modal')
				</form>		
			</div>
		</div>
	</div>
</div>
@stop 