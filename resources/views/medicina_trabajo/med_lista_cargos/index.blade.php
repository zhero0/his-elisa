@extends('adminlte::page')
@section('title', 'LISTA DE REGISTROS')
@section('content_header')
    <h1>LISTA DE REGISTROS</h1>
@stop   

@section('content')
<div class="card">  
 	<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
	<div class="card-body"> 
		<table id="empleador" class="table table-striped">
			<thead>
				<tr>
					<th>Cargo</th> 								
					<th>Persona</th> 													
					<th>Fecha</th>						
					<th>Usuario</th> 
					<th></th>
				</tr> 
			</thead>
			<tbody id="tbodyRegistros">
				@include($model->rutaview.'indexContent')
			</tbody>
		</table>
	</div>  
</div>
	@include($model->rutaview.'modal')
@stop

@section('js')
	<script>
		$(document).ready( function () {
		    $('#empleador').DataTable({
		    	"language":{
		    		"search": "Buscar",
		    		"lengthMenu": "Mostrar _MENU_ registros por página",
		    		"info": "Mostrando página _PAGE_ de _PAGES_",
		    		"paginate":{
			    		"previous": "Anterior",
			    		"next": "Siguiente",
			    		"first": "Primero",
			    		"last": "Ultimo"
			    	}
		    	}
		    });
		} );
	</script>	
@endsection 