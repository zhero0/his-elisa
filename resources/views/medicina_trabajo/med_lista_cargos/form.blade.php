<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
      
  <div class="col-md-12">
    <div class="card ">
      <div class="card-header card-header-rose card-header-text">
        <div class="card-text">
          <h4 class="card-title">Datos</h4>
        </div>
      </div>
      <div class="card-body ">
        
        <div class="row">  
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="nombre" class="col-sm-6 col-form-label">Nombre</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="nombre" name="nombre" value="{{ $model->nombre }}" placeholder="nombre">
            </div> 
          </div>     
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6"> 
            <label for="descripcion" class="col-sm-6 col-form-label">Descripción</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="descripcion" name="descripcion" value="{{ $model->descripcion }}" placeholder="">              
            </div>  
          </div>   
        </div>

        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="costo_tramite" class="col-sm-6 col-form-label">Costo tramite</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="costo_tramite" name="costo_tramite" value="{{ $model->costo_tramite }}" placeholder="">              
            </div>  
          </div>
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <label for="costo_formulario" class="col-sm-6 col-form-label">Costo formulario</label>
            <div class="col-sm-12">
              <input type="text" class="form-control" id="costo_formulario" name="costo_formulario" value="{{ $model->costo_formulario }}" placeholder="">              
            </div> 
          </div>     
        </div>
      </div>
    </div>
  </div> 
</div> 

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif