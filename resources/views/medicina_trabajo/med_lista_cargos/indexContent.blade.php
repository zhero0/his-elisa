@foreach($model as $dato)
    <tr role="row" id="rowBody-{{ $dato->id }}">                         
        <td class="text-left">{{ $dato->nombre }}</td>
            @if ($modelDatos == null)
                <td class="text-left">
                    <input type="text" id="txtDatos-{{ $dato->id }}" readonly=""  class="form-control input-sm" style="text-transform: uppercase;">
                </td> 
                <td class="text-left">
                    <input type="text" id="txtFecha" readonly="" class="form-control input-sm" style="text-transform: uppercase;">
                </td>                      
                <td class="text-left">
                    <input type="text" id="txtUsuario" readonly="" class="form-control input-sm" style="text-transform: uppercase;">
                </td> 
            @else
                @foreach($modelDatos as $datoRegistrado)    
                    @if($dato->id == $datoRegistrado->res_idmed_administracion)
                        <td class="text-left">
                            <input type="text" id="txtDatos-{{ $dato->id }}" readonly=""  value="{{ $datoRegistrado->res_matricula.'-'.$datoRegistrado->res_apellidos.'-'.$datoRegistrado->res_nombres }}" class="form-control input-sm" style="text-transform: uppercase;">
                        </td> 
                        <td class="text-left">
                            <input type="text" id="txtFecha" readonly="" value="{{ $datoRegistrado->res_fecha }}" class="form-control input-sm" style="text-transform: uppercase;">
                        </td>                      
                        <td class="text-left">
                            <input type="text" id="txtUsuario" readonly="" value="{{ $datoRegistrado->res_usuario }}" class="form-control input-sm" style="text-transform: uppercase;">
                        </td>   
                    @endif                           
                @endforeach  
            @endif                        
        <td class="td-actions text-right"> 
            <button class="btn btn-default btn-sm" onclick="asignaCargo( {{ $dato->id }} )" rel="tooltip" title="Asignar personal">
                    <i class="fa fa-user"></i>
                </button>
            <a href="{{ route('med_lista_cargos.edit', $dato->id) }}" class="btn btn-default btn-sm btn-round" rel="tooltip" title="Eliminar registro">
                    <i class="fa fa-trash"></i>                                
            </a>                    
        </td>
    </tr> 
@endforeach 