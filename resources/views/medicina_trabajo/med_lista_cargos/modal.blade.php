<div class="modal fade" id="vModalBuscador">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
	      		<h4 class="modal-title">Datos Personal</h4>
	      		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>		      	
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Persona" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Persona">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Medico" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-5">Nombres</th>
		                        <th class="col-xs-6">Apellidos</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Medico" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>

 