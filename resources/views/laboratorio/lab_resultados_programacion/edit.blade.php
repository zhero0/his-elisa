@extends('adminlte::layouts.app')
@section('contentheader_title', '')
@section('htmlheader_title', 'REGISTRA RESULTADO')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-11 col-lg-12">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['resultado_lab.update', $model->id], 'files'=>true, 'method' => 'PUT', 'id' => 'form', 'enctype' => 'multipart/form-data', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop
@include($model->rutaview.'modal')