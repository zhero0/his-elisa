<div class="form-group">
    <!-- [ BUSCADOR ACTUAL]       3) En una cajita de texto el valor JSON de los parametros de la URL -->
    <input type="hidden" name="txt_parametrosURL" value="{{ $model->parametrosURL }}">
    
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    <input type="hidden" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
    
    <input type="hidden" id="txtJsonValores" name="txtJsonValores" value="{{ $model->JsonValores }}">
    
    <!-- En formato JSON envio los datos a GUARDAR... -->
    <input type="hidden" id="txtJsonDatosGuardar" name="txtJsonDatosGuardar">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>
    
    <div class="row" id="divVistaDatos">
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                <h3 class="box-title">PACIENTE</h3>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                <input type="hidden" id="txtidhcl_poblacion" name="txtidhcl_poblacion">
                <dl class="dl-horizontal">
                  <dt>Nº SEGURO:</dt>
                  <dd id="datoNumeroPaciente"></dd>

                  <dt>NOMBRE COMPLETO:</dt>
                  <dd id="datoNombrePaciente"></dd>

                  <!-- <dt>APELLIDOS:</dt>
                  <dd id="datoApellidosPaciente"></dd> -->

                  <dt>EDAD:</dt>
                  <dd id="datoEdadPaciente"></dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-6">
        <div class="box box-warning">
          <div class="box-header with-border">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <h3 class="box-title">DATOS PROGRAMACIÓN</h3>
              </div>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                <dl class="dl-horizontal">
                  <dt>HOSPITALIZADO:</dt>
                  <dd>{{ $model->hospitalizado == 1? "SI" : "NO" }}</dd>
                  
                  <dt>DIAGNÓSTICO:</dt>
                  <dd>{{ $model->diagnostico }}</dd>

                  <dt>TIPO:</dt>
                  <dd>{{ $model->especialidad }}</dd>

                  <dt>ESPECIALIDAD:</dt>
                  <dd>{{ $model->cuaderno }}</dd>

                  <dt>MÉDICO SOLICITANTE:</dt>
                  <dd>{{ $model->personal }}</dd>

                  <dt>FECHA:</dt>
                  <dd>{{ $model->fechaprogramacion }}</dd>

                  <dt>HORA:</dt>
                  <dd>{{ $model->horaprogramacion }}</dd>

                  <dt>OBSERVACIÓN:</dt>
                  <dd><pre style="background: transparent;">{{ $model->observacion }}</pre></dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="row">
      <ul class="timeline">
        
        <!-- timeline RESULTADO -->
        <li>
          <i class="glyphicon glyphicon-tasks bg-red"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                      <h3 class="box-title">RESULTADO</h3>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <!-- <div id="divContenedorResultado"></div>
                  <div id="divContenedorResultado_Formula"></div> -->
                  
                  <div id="divContenedor_Perfil_Examen_Variable"></div>
                  
                </div>
              </div>
            </div>
          </div>
        </li>
        <!-- END timeline RESULTADO -->


        <!-- timeline RESPUESTA -->
        <li>
          <i class="glyphicon glyphicon-pencil bg-green"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-success">
                <div class="box-body">
                  <label>Descripción</label>
                  <textarea class="form-control" id="summary-ckeditor" name="taRespuesta">{{$model->respuesta}}</textarea>

                  <div class="row">
                    <div class="col-xs-9 col-sm-10 col-md-10 col-lg-10">
                      <div class="list-group-item">
                        <h4 class="list-group-item-heading text-primary">
                          <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                          SUBIR ARCHIVOS
                        </h4>
                      </div>
                      <ul class="list-group">
                        <li class="list-group-item">
                          <input type="file" name="archivo[]" multiple>
                        </li>
                      </ul>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                      <div class="list-group">
                        @if(count($modelLab_resultados_documento) > 0)
                          <div class="list-group-item">
                            <h4 class="list-group-item-heading text-primary">
                              <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                              DESCARGAR ARCHIVOS
                            </h4>
                          </div>
                          <ul class="list-group">
                            @foreach ($modelLab_resultados_documento as $value)
                              <?php
                                $valor = explode('_', $value->archivo);
                                $archivo = substr($value->archivo, strlen($valor[0])+1, strlen($value->archivo));
                              ?>
                              <a href="{{ url('descargarArchivo?path='.config('app.rutaArchivosLaboratorio').'/'.$value->archivo) }}" class="list-group-item list-group-item-action">{{ $archivo }}</a>
                            @endforeach
                          </ul>
                        @endif
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </li>
        <!-- END timeline RESPUESTA -->

        <li>
          <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
        </li>
      </ul>  
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardar" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif