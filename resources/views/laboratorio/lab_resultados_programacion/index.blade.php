@extends('layouts.admin')
@section('contentheader_title', 'RESULTADOS LABORATORIO')
@section('htmlheader_title', 'RESULTADOS LABORATORIO')

<?php
use Illuminate\Support\Facades\Input; 
use \App\Models\Laboratorio\Lab_estado;
use \App\Models\Afiliacion\Hcl_especialidad;
?>

@section('main-content')
<div class="box box-primary">
	<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">

 	<div class="row">		
		<div class="container-fluid">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="text-align: left;">
		    </div>
		    <div class="col-xs-11 col-sm-11 col-md-10 col-lg-10 text-right">
		        <form class="navbar-form navbar-right" roles="searchNuevo" action="/resultado_lab" autocomplete="off">
		        	@if($model->buscarFecha == 1)
		              <label class="checkbox-inline"><input type="checkbox" onclick="this.form.submit()"  name="buscarFecha" value="{{ $model->buscarFecha = 1? 1 : 0 }}" checked>Todos</label> 
		            @else
		               <label class="checkbox-inline"><input type="checkbox" onclick="this.form.submit()"  name="buscarFecha" value="1"  >Todos</label> 
		            @endif      
		            <select id="especialidad" name="especialidad" class="form-control" onchange="this.form.submit()">
			          <!-- <option value="0"> :: Seleccionar</option>   -->
			          @foreach($modelEspecialidad as $especialidad)
			            @if($model != null)
			              <option value="{{$especialidad->id}}"  {{ $model->idlab_especialidad == $especialidad->id ? 'selected="selected"' : '' }} >{{$especialidad->nombre}}</option>    
			            @else
			               <option value="{{$especialidad->id}}">{{$especialidad->nombre}}</option> 
			            @endif                      
			          @endforeach 
			        </select>                       	   
		        	{{ Form::input('date', 'fechaRegistro', $model->fechaRegistro, ['class'=>'datepicker']) }}
		          	{!! Form::text('searchNuevo', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
		          <button type="submit" class="btn btn-primary"> Buscar </button>
		        </form>         
	        </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr>
	 					<th>Nº</th>
	 					<!-- <th>Nº Historia</th> -->
	 					<th>Asegurado</th>
	 					<th>Especialidad</th>
	 					<th>Médico</th>
	 					<th></th>
	 					<th>Fecha Prog.</th>
	 					<th>Nº Folio</th>
	 					<th>Estado</th>
	 					<th></th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<?php
						if($dato->idhcl_especialidad == Hcl_especialidad::Embarazo && $dato->idlab_estado == Lab_estado::PROGRAMACION)
			 				$table_tr_class = 'info';
						elseif($dato->idhcl_especialidad == Hcl_especialidad::Emergencias && $dato->idlab_estado == Lab_estado::PROGRAMACION)
			 				$table_tr_class = 'danger';
						elseif($dato->idlab_estado == Lab_estado::PROGRAMACION)
			 				$table_tr_class = 'active';
			 			elseif($dato->idlab_estado == Lab_estado::ENTREGADO)
			 				$table_tr_class = 'success';
			 			else
			 				$table_tr_class = 'default';
			 		?>
					<tr role="row" class="{{$table_tr_class}}" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-0">{{ $dato->numero }}</td>
						<?php // <td class="col-xs-1">{{ $dato->hcl_poblacion_matricula }}</td> ?>
						<td>{{ $dato->hcl_poblacion_nombrecompleto }}</td>
						<td class="col-xs-2">{{ $dato->cuaderno }}</td>
						<td class="col-xs-2">{{ $dato->nombres.' '.$dato->apellidos }}</td>
						<td class="col-xs-0">
							@if($dato->idhcl_cabecera_registro > 0)
								<?php
								$fecha = date('d-m-Y', strtotime($dato->fechaprogramacion_sistema));
								$hora = $dato->horaprogramacion_sistema;
								$glosa = 'Fecha: '.$fecha.', hora: '.$hora;
								?>
								<i class="fa fa-stethoscope" title="Programado desde SALUD. {{ $glosa }}" style="cursor: help;"></i>
							@endif
						</td>
						<td class="col-xs-1">{{ date('d-m-Y', strtotime($dato->fechaprogramacion)) }}</td>
						<td class="col-xs-1">{{ $dato->numero_muestra }}</td>
						<td class="col-xs-1">{{ $dato->estado }}</td>
						<td class="col-xs-2">
							@if($dato->numero_muestra == null)
								<?php
								// <button type="button" class="btn btn-default btn-sm" 
								// 		onclick="registraMuestra( {{$dato->id}}, {{$dato->numero}} )" title="Registrar Muestreo">
								// 	<i class="fa fa-eyedropper"></i>
								// </button>
								?>
								<button class="btn btn-warning btn-sm" data-id="{{ $dato->id }}" data-numero="{{ $dato->numero }}" data-toggle="modal" data-target="#registraMuestreo" title="Registrar Muestreo">
									<i class="fa fa-eyedropper"></i>
								</button>
								<button type="button" class="btn btn-default btn-sm" disabled="disabled">_</button>
							@else
								<button type="button" class="btn btn-default btn-sm" disabled="disabled">_</button>
								<a href="{{ route('resultado_lab.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Entregar">
									<i class="glyphicon glyphicon-pencil"></i>
								</a>
							@endif
							<button type="button" class="btn btn-default btn-sm" onclick="reporteResultado( {{$dato->id}} )" title="Reporte Resultado">
								<i class="glyphicon glyphicon-print"></i>
							</button>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      	{!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>

<div class="box box-success">
	@foreach($modelo as $dato) 
		<a class="btn btn-app">
	    	<i class="fa fa-heartbeat"> {{ $dato->total }}</i> {{ $dato->nombre }}
	  	</a>
	@endforeach	     
</div>
@stop




<!-- MODAL PARA REGISTRAR MUESTREO -->
<div class="modal fade" id="registraMuestreo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
         	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            <h4 class="modal-title" id="tituloModalMuestreoLab"> MUESTREO LABORATORIO Nº </h4>
        </div>
        <form id="formMuestreo" action="{{route('registraMuestreo')}}" method="post">
        	<?php
        	// {{method_field('delete')}}
        	?>

            {{csrf_field()}}
            <div class="modal-body">
              <input type="hidden" id="idtabla" name="idtabla">
              <input type="hidden" id="txtDatosExtras" name="txtDatosExtras">

              <div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h4 style="color: red; font-weight: bold;" id="txtNroFolioAnterior">Nº FOLIO ANTERIOR: XXX</h4>
					</div>
				</div>

	        	<div class="row">
	        		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	                    <label>Nº Folio</label>
	                    <input type='number' class="form-control input-lg" id="txtNumero_muestra" name="txtNumero_muestra">
	                </div>
	                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	                    <label>Estado</label>
	                    <select name="txtEstado_muestra" class="form-control">
	                    	<option value="0"></option>
	                    	<option value="1"> BUENA </option>
	                    	<option value="2"> MALA </option>
	                    	<option value="3"> REGULAR </option>
	                    </select>
	              	</div>
	        	</div>
	        	
	        	<div class="row">
	        		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	                    <label>Nº Sala</label>
	                    <input type='number' class="form-control" name="txtNumero_sala">
	                </div>
	                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	                    <label>Nº Cama</label>
	                    <input type='number' class="form-control" name="txtNumero_cama">
	                </div>
	        	</div>
	        	
	        	<div class="row">
	        		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
		                <label>Fecha</label>
		                <div class='input-group date' id='datepickerFecha'>
		                  <input type='text' class="form-control" name="txtFecha_muestra" value="{{ $model->fechaActual }}">
		                    <span class="input-group-addon">
		                        <span class="glyphicon glyphicon-calendar"></span>
		                    </span>
		                </div>
	              	</div>
	        		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
	                    <label>Hora</label>
	                    @include('layouts.hora')
	                </div>	
	        	</div>

        		<label>Observación</label>
	            <textarea class="form-control" name="taDescripcion_muestra" rows="3" placeholder="Escriba..."></textarea>
        	</div>
            </div>
            <div class="modal-footer">
	            <button type="button" class="btn btn-md btn-danger" data-dismiss="modal">
	                <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
	                Salir
	            </button>
	            <button type="button" class="btn btn-md btn-success" id="btnGuardarMuestreo">
	                <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> 
	                Guardar
	            </button>
          </div>
        </form>
    </div>
  </div>
</div>

<?php
// <div class="modal" id="vResultadoProgramacion_Muestra">
//   <div class="modal-dialog modal-md">
//     <div class="modal-content">
//         <div class="modal-header">
//             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
//             <h4 class="modal-title" id="tituloModalMuestreoLab"> MUESTREO LABORATORIO Nº </h4>
//         </div>
//         <div class="modal-body">
//         	<div class="container-fluid">
// 				<div class="row">
// 					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
// 						<h4 style="color: red; font-weight: bold;" id="txtNroFolioAnterior">Nº FOLIO ANTERIOR: XXX</h4>
// 					</div>
// 				</div>

// 	        	<div class="row">
// 	        		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
// 	                    <label>Nº Folio</label>
// 	                    <input type='number' class="form-control input-lg" id="txtNumero_muestra">
// 	                </div>
// 	                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
// 	                    <label>Estado</label>
// 	                    <select id="txtEstado_muestra" class="form-control">
// 	                    	<option value="0"></option>
// 	                    	<option value="1"> BUENA </option>
// 	                    	<option value="2"> MALA </option>
// 	                    	<option value="3"> REGULAR </option>
// 	                    </select>
// 	              	</div>
// 	        	</div>
	        	
// 	        	<div class="row">
// 	        		<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
// 	                    <label>Nº Sala</label>
// 	                    <input type='number' class="form-control" id="txtNumero_sala">
// 	                </div>
// 	                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
// 	                    <label>Nº Cama</label>
// 	                    <input type='number' class="form-control" id="txtNumero_cama">
// 	                </div>
// 	        	</div>
	        	
// 	        	<div class="row">
// 	        		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
// 		                <label>Fecha</label>
// 		                <div class='input-group date' id='datepickerFecha'>
// 		                  <input type='text' class="form-control" id="txtFecha_muestra" value="{{ $model->fechaActual }}">
// 		                    <span class="input-group-addon">
// 		                        <span class="glyphicon glyphicon-calendar"></span>
// 		                    </span>
// 		                </div>
// 	              	</div>
// 	        		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
// 	                    <label>Hora</label>
// 	                    @include('layouts.hora')
// 	                </div>	
// 	        	</div>

//         		<label>Descripción</label>
// 	            <textarea class="form-control" id="taDescripcion_muestra" rows="3" placeholder="Escriba..."></textarea>
//         	</div>
//         </div>
//         <div class="modal-footer">
//           <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
//             <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
//               Salir
//           </button>
//           <button type="button" id="btnGuardarMuestra" class="btn btn-sm btn-success">
//             <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
//               Guardar
//           </button>
//         </div>
//     </div>
//   </div>
// </div>
?>


<!-- MODAL PARA VISUALIZAR EL REPORTE -->
<div class="modal" id="vImprimeResultado">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> REPORTE RESULTADO </h4>
        </div>
        <div class="modal-body">
         	<div id="secccionImprimeResultado" style="height: 410px;"></div>
         	
	 		<div class="progress">
			  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			  </div>
			</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div>