<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" id="txtListagrupos" value="{{ $model->listagrupos }}">
    <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
    <!-- [2] -->
    <input type="hidden" id="txtListaVariables" name="txtListaVariables" value="{{$model->listavariable}}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>
    
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
             

            <div class="row">
              <label for="especialidad">Especialidad</label>
              <select id="especialidad" name="especialidad" class="form-control">
                <option value="0"> :: Seleccionar</option>  
                @foreach($modelEspecialidades as $especialidad)
                  @if($model != null)
                    <option value="{{$especialidad->id}}"  {{ $model->idlab_especialidad == $especialidad->id ? 'selected="selected"' : '' }} >{{$especialidad->nombre}}</option>    
                  @else
                     <option value="{{$especialidad->id}}">{{$especialidad->nombre}}</option> 
                  @endif
                @endforeach 
              </select> 
            </div>

            <div class="row">
              <label for="muestra">Tipo Muestra</label>
              <select id="muestra" name="muestra" class="form-control">
                <option value="0"> :: Seleccionar</option>  
                @foreach($modelMuestras as $muestra)
                  @if($model != null)
                    <option value="{{$muestra->id}}"  {{ $model->idlab_tipo_muestra == $muestra->id ? 'selected="selected"' : '' }} >{{$muestra->nombre}}</option>    
                  @else
                     <option value="{{$muestra->id}}">{{$muestra->nombre}}</option> 
                  @endif                      
                @endforeach 
              </select> 
            </div>

            <div class="row">
              {!! Form::label('codigo', 'Codigo', ['class' => 'label-control']) !!}
              {!! Form::text('codigo', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
            </div>

            <div class="row">
              {!! Form::label('nombre', 'Nombre:') !!}
              {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
            </div>

            <div class="row">
              {!! Form::label('descripcion', 'Descripción:', ['class' => 'label-control']) !!}
              {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'rows' => 3]) !!}
            </div>

          </div>
        </div>

        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
          
          <div class="row">
            <div class="panel panel-primary">
              <div class="panel-body"> 
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button class="btn btn-warning" type="button" id="btnBuscarVariable">
                          <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                          Buscar Variable
                        </button>
                      </span>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8" style="text-align: right;">
                    <h4>Presione f9 Para Buscar Variables</h4>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-12 col-sm-10 col-md-10 col-lg-12">
                    <div class="table-responsive" style="height: 350px; overflow-y: auto;">
                      <table id="tablaContenedorDatos" class="table table-bordered table-hover table-condensed">
                          <thead style="{{ config('app.styleTableHead') }}">
                              <tr>
                                 <th id="thCantidadVariables">Tipo Variable (0)</th>
                                 <th class="col-xs-4">Grupo</th>
                                 <th class="col-xs-1">Orden</th>
                                 <th class="col-xs-1"></th>
                                 <th class="col-xs-1"></th>
                             </tr>
                          </thead>
                          <tbody class="table-success" style="{{ config('app.styleTableRow') }}">
                            <tr id="idListaVacia">
                              <td>Lista Vacia...</td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarExamen" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif