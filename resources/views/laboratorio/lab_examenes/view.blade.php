@extends('adminlte::layouts.app')
@section('contentheader_title', 'VER EXAMEN')
@section('htmlheader_title', 'VER EXAMEN')
 
@section('main-content')
<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="panel panel-primary">
      <div class="panel-body">
        {!! Form::model($model, ['route' => ['examen.update', $model->id], 'method' => 'PUT', 'id' => 'form']) !!}
          @include($model->rutaview.'form')
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@stop