@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE EXÁMENES')
@section('htmlheader_title', 'LISTA DE EXÁMENES')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
 		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
 		<!-- "BUSCADOR MULTIPLE" -->
 		<div class="row">
	 		<form id="formBuscador" class="navbar-form navbar-right" action="/examen" autocomplete="off">
	 			<textarea class="form-control" id="datosBuscador" name="datosBuscador" style="display: none;"> {{ $model->datosBuscador }} </textarea>
	 		</form>
 		</div>
 		<!-- FIN "BUSCADOR MULTIPLE" -->
 		
 		
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('examen.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr>
	 					<th>Código</th>
	 					<th>Nombre</th>
	 					<th>Especialidad</th>
	 					<th>Tipo Muestra</th>
	 					<th></th>
					</tr>
					<tr id="indexSearch" style="background: #ffffff;">
	 					<th>
	 						<input type="text" id="txtSearch_codigo" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<select id="selectSearch_especialidad" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
				            	<option value="0"> </option>
				                @foreach($modelLab_especialidad as $dato)
				                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
				                @endforeach
				            </select>
	 					</th>
	 					<th>
	 						<select id="selectSearch_tipomuestra" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
				            	<option value="0"> </option>
				                @foreach($modelLab_tipo_muestra as $dato)
				                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
				                @endforeach
				            </select>
	 					</th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-1">{{ $dato->codigo }}</td>
						<td>{{ $dato->nombre }}</td>
						<td class="col-xs-4">{{ $dato->especialidad }}</td>
						<td class="col-xs-1">{{ $dato->tipomuestra }}</td>
						<td class="col-xs-1">
							<a href="{{ route('examen.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a>
							<a href="{{ route('examen.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@stop