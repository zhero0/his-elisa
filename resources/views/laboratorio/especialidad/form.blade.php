<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('codigo', 'Codigo', ['class' => 'label-control']) !!}
        {!! Form::text('codigo', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'autofocus' => 'autofocus']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-11 col-lg-10">
        {!! Form::label('descripcion', 'Descripción:', ['class' => 'label-control']) !!}
        {!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif