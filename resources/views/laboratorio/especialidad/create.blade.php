@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVA ESPECIALIDAD')
@section('htmlheader_title', 'NUEVA ESPECIALIDAD')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6">
		<div class="panel panel-primary">
      		<div class="panel-body">      			
      			{!! Form::Open(['route' => 'especialidad.store', 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}			
			</div>
		</div>
	</div>
</div>
@stop