<!-- VENTANA MODAL PARA BUSCAR PACIENTE -->
<div class="modal fade" id="vModalBuscadorPaciente">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="favoritesModalLabel">DATOS PACIENTE</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscadorPaciente" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscarPaciente">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>

		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedorDatosPaciente" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
                        		<th class="col-xs-2">Nro Historia</th>
		                        <th class="col-xs-4">Nombres</th>
	                            <th>Apellidos</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbodyPaciente" style="{{ config('app.styleTableRow') }}">
	                      <tr id="idListaVaciaPaciente">
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>


<!-- VENTANA MODAL PARA BUSCAR ESPECIALIDAD -->
<div class="modal fade" id="vModalBuscadorEspecialidad">
	<div class="modal-dialog modal-xs">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">ESPECIALIDADES</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Especialidad" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Especialidad">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Especialidad" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-10">Nombre</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Especialidad" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>


<!-- VENTANA MODAL PARA BUSCAR MÉDICOS -->
<div class="modal fade" id="vModalBuscadorMedicos">
	<div class="modal-dialog modal-xs">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title">MÉDICOS</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="input-group">
                  <input type="text" id="txtBuscador_Medico" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscar_Medico">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
                
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedor_Medico" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-5">Nombres</th>
		                        <th class="col-xs-6">Apellidos</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Medico" style="{{ config('app.styleTableRow') }}">
	                      <tr>
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>