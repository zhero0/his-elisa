@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR EXAMEN')
@section('htmlheader_title', 'EDITAR EXAMEN')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-11 col-lg-12">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['programacion.update', $model->id], 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop
@include($model->rutaview.'modal')


<!-- VENTA MODAL PARA BUSCADOR -->
<div class="modal fade" id="vModalBuscador">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="favoritesModalLabel">VARIABLES</h4>
		    </div>
		    <div class="modal-body">
		    	<input type="hidden" id="txtHiddenListaVariables">
		    	
		    	<div class="input-group">
                  <input type="text" id="txtBuscador" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnBuscador">
                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                      Buscar
                    </button>
                  </span>
                </div>
	        	
				<div id="divCheckAll" style="background: #374850; color: #f3eaf0;">
					<label class="checkbox-inline" style="font-size: 16px;">
						<input type="checkbox" id="checkVariable" onClick="selecciona_todas_las_variables(this.checked);">
						VARIABLES
					</label>
				</div>
				
				<div id="divCheckBuscadorVariables" style="height: 300px; overflow-y: scroll;">
				</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		        <span class="pull-right">
		          <button type="button" id="btnSeleccionarVariables" class="btn btn-primary">
		          	<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
		            Seleccionar
		          </button>
		        </span>
		    </div>
	    </div>
	</div>
</div>




<div class="modal fade" id="vModalTipoVariables">
	<div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">TIPO VARIABLES</h4>
		    </div>
		    <div class="modal-body">
		    	
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
                  	<table id="tablaContenedorDatosTipoVariable" class="table table-bordered table-hover table-condensed">
	                    <thead style="background: #374850; color: #f3eaf0;">
	                          <tr>
	                             <th id="thCantidadTipoVariable">Tipo Variables (0)</th>
	                         </tr>
	                    </thead>
	                    <tbody class="table-success" id="cuerpoTipoVariable">
	                        <tr id="idListaVaciaTipoVariable">
	                          <td>Lista Vacia...</td>
	                        </tr>
	                    </tbody>
                  	</table>
                </div>

	      	</div>
	      	<div class="modal-footer">
		        <button type="button" 
		           class="btn btn-default" 
		           data-dismiss="modal">
		           <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
		           Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>