<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" id="lab_examenesEspecialidad" value="{{ $model->lab_examenesEspecialidad }}">
    <input type="hidden" id="especialidadProgramados" value="{{ $model->especialidadProgramados }}">
    <input type="hidden" id="txtJsonExamenes" name="txtJsonExamenes">
    <input type="hidden" id="txtJsonHcl_poblacion" name="txtJsonHcl_poblacion" value="{{ $model->txtJsonHcl_poblacion }}">
    <input type="hidden" id="examenesProgramados" value="{{ $model->examenesProgramados }}">
    <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
    <input type="hidden" id="idhcl_cuaderno" name="idhcl_cuaderno" value="{{ $model->idhcl_cuaderno }}">
    <input type="hidden" id="iddatosgenericos" name="iddatosgenericos" value="{{ $model->iddatosgenericos }}">
    <input type="hidden" id="grupos" value="{{ $model->grupos }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-9">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>
    
    <div class="row">
      <ul class="timeline">
        <!-- timeline PACIENTE -->
        <li>
          <i class="fa fa-user bg-blue"></i>
          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
                      <h4 class="list-group-item-heading text-primary">
                        <i class="fa fa-user"></i>
                        PACIENTE
                      </h4>
                    </div>
                    
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-5">
                      <div class="input-group input-group-sm">
                        <input type="text" id="txtBuscarPaciente" class="form-control" placeholder="Escriba..." autofocus="autofocus" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" id="btnModalPaciente">
                            <span class="fa fa-search" aria-hidden="true"></span>
                            Buscar
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                      <input type="hidden" id="txtidhcl_poblacion" name="txtidhcl_poblacion">
                      <dl class="dl-horizontal">
                        <dt>Nº PACIENTE:</dt>
                        <dd id="datoNumeroPaciente"></dd>

                        <dt>NOMBRES:</dt>
                        <dd id="datoNombrePaciente"></dd>

                        <dt>APELLIDOS:</dt>
                        <dd id="datoApellidosPaciente"></dd>
                      </dl>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="timeline-footer">
              <div class="alert alert-danger" id="divErrorPaciente" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Seleccione algún PACIENTE
              </div>
            </div>
          </div>
        </li>
        <!-- END timeline PACIENTE -->

        <!-- timeline DATOS -->
        <li>
          <i class="fa fa-file-text-o bg-yellow"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-warning">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <h4 class="list-group-item-heading text-primary">
                        <i class="fa fa-file-o"></i>
                        DATOS
                      </h4>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="row">
                      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-check">
                          <input type="checkbox" class="form-check-input" id="checkHospitalizado" name="checkHospitalizado"> 
                          <label class="form-check-label" for="checkHospitalizado" style="cursor: pointer;">
                            <span class="glyphicon glyphicon-hand-left" aria-hidden="true"></span> 
                            HOSPITALIZADO
                          </label>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9">
                        {!! Form::label('diagnostico', 'Diagnostico:', ['class' => 'label-control']) !!}
                        {!! Form::textarea('diagnostico', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'rows' => 2]) !!}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        {!! Form::label('tipo', 'Tipo', ['class' => 'label-control']) !!}
                        {!! Form::select('idhcl_especialidad', $modelHcl_especialidad, null, ['class' => 'form-control']) !!}
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label>Especialidad</label>
                        <div class="input-group">
                          <input type="text" id="txtEspecialidad" name="txtEspecialidad" class="form-control" value="{{$model->especialidad}}" disabled="">
                          <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="btnModal_Especialidad" title="Click para Buscar">
                              <span class="fa fa-search" aria-hidden="true"></span>.
                            </button>
                          </span>
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <label>Médico Solicitante</label>
                        <div class="input-group">
                          <input type="text" id="txtMedico" name="txtMedico" value="{{$model->medico}}" class="form-control" disabled="">
                          <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="btnModal_Medico" title="Click para Buscar">
                              <span class="fa fa-search" aria-hidden="true"></span>.
                            </button>
                          </span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2">
                        <label>Fecha</label>
                        <div class='input-group date' id='datepickerFecha'>
                          <input type='text' class="form-control" id="fecha" name="fechaprogramacion" value="{{  $model->fechaprogramacion }}">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <input type="hidden" id="txtHiddenHora" name="txtHiddenHora" value="{{ $model->horaprogramacion }}">
                        <label>Hora</label>
                        @include('layouts.hora')
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {!! Form::label('observacion', 'Observación:', ['class' => 'label-control']) !!}
                        {!! Form::textarea('observacion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'rows' => 2]) !!}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="timeline-footer">
            </div>
          </div>
        </li>
        <!-- END timeline DATOS -->

        <!-- timeline item -->
        <li>
          <i class="glyphicon glyphicon-tasks bg-red"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="box box-danger">
                <div class="box-header with-border">
                  <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                      <h4 class="list-group-item-heading text-primary">
                        <i class="fa fa-file-text-o"></i>
                        EXÁMENES
                      </h4>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="input-group">
                        <input type="text" id="txtBuscarExamen" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
                        <span class="input-group-btn">
                          <button class="btn btn-warning" type="button" id="btnBuscarExamen">
                            <span class="fa fa-search" aria-hidden="true"></span>
                            Buscar
                          </button>
                        </span>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                      <div id="divContenedor_GruposLaboratorio" style="max-height: 325px; overflow-y: auto;">
                        <div class="list-group" id="listaGrupo">
                          <!-- <button type="button" class="list-group-item">BASICO <span class="badge">3</span></button>
                          <button type="button" class="list-group-item">LAB. HEMATOLOGIA <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Morbi leo risus <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Porta ac consectetur ac <span class="badge">14</span></button>
                   
                          <button type="button" class="list-group-item">Vestibulum at eros <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">BASICO <span class="badge">3</span></button>
                          <button type="button" class="list-group-item">LAB. HEMATOLOGIA <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Morbi leo risus <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Porta ac consectetur ac <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Vestibulum at eros <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">BASICO <span class="badge">3</span></button>
                          <button type="button" class="list-group-item">LAB. HEMATOLOGIA <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Morbi leo risus <span class="badge">14</span></button>
                          <button type="button" class="list-group-item">Porta ac consectetur ac <span class="badge">14</span></button> -->
                        </div>
                      </div>
                    </div>

                    <div id="divContenedor_Informacion_Lab_Ima" class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="table-responsive" style="height: 400px; overflow-y: auto; border: 1px solid #374850;">
                        <table id="tablaContenedorExamenes" class="table table-bordered table-hover table-condensed">
                            <thead style="{{ config('app.styleTableHead') }}">
                                <tr>
                                  <th class="col-xs-2">Código</th>
                                  <th>Nombre</th>
                                  <th class="col-xs-5">Especialidad</th>
                                  <th class="col-xs-1"></th>
                               </tr>
                            </thead>
                            <tbody class="table-success" id="tbodyExamenes" style="{{ config('app.styleTableRow') }}">
                              <tr>
                                <td>Vacío...</td>
                              </tr>
                            </tbody>
                        </table>
                      </div>

                      <button class="btn btn-default" id="btnVisualizaExamenesSeleccionados" type="button"  title="Click para visualizar los Exámenes Seleccionados">
                      Exámenes Seleccionados <span class="badge" id="spanTotalExamenSeleccion">0</span>
                      </button>
                      <button type="button" id="btnBorrarExamenesSeleccionados" class="btn btn-danger" title="Borrar Exámenes Seleccionados">
                          <span class="fa fa-trash-o" aria-hidden="true"></span>
                          Borrar Exámenes Seleccionados
                      </button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="timeline-footer">
              <div class="alert alert-danger" id="divErrorExamen" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Seleccione algún EXÁMEN
              </div>
            </div>
          </div>
        </li>
        <!-- END timeline item -->

        <!-- timeline ESPECIALIDADES -->
        <li>
          <i class="glyphicon glyphicon-modal-window bg-green"></i>

          <div class="timeline-item">
            <div class="timeline-body">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <h4 class="list-group-item-heading text-primary">
                          <i class="fa fa-gears"></i>
                          ESPECIALIDADES
                        </h4>
                        <button type="button" id="btnActualizaEspecialidad" class="btn btn-default btn-md" title="Click para Actualizar ESPECIALIDADES! ">
                          <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div style="max-height: 300px; overflow-y: scroll;">
                      <div id="divInformacionProgramados"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="timeline-footer">
            </div>
          </div>
        </li>
        <!-- END timeline ESPECIALIDADES -->

        <li>
          <i class="glyphicon glyphicon-option-horizontal bg-gray"></i>
        </li>
      </ul>  
    </div>
</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarExamen" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif