@extends('layouts.admin')
@section('contentheader_title', 'PROGRAMACIONES LABORATORIO')
@section('htmlheader_title', 'PROGRAMACIONES LABORATORIO')

<?php
use Illuminate\Support\Facades\Input; 
use \App\Models\Laboratorio\Lab_estado;
use \App\Models\Afiliacion\Hcl_especialidad;
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
 		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
 		<!-- "BUSCADOR MULTIPLE" -->
 		<div class="row">
	 		<form id="formBuscador" class="navbar-form navbar-right" action="/examen" autocomplete="off">
	 			<textarea class="form-control" id="datosBuscador" name="datosBuscador" style="display: none;"> {{ $model->datosBuscador }} </textarea>
	 		</form>
 		</div>
 		<!-- FIN "BUSCADOR MULTIPLE" -->


		<div class="container-fluid">
			<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('programacion.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		    <div class="col-xs-11 col-sm-11 col-md-10 col-lg-10 text-right">
		        <form class="navbar-form navbar-right" roles="searchNuevo" action="/programacion" autocomplete="off">
		        	@if($model->buscarFecha == 1)
		              <label class="checkbox-inline"><input type="checkbox" onchange="this.form.submit()" name="buscarFecha" value="{{ $model->buscarFecha = 1? 1 : 0 }}" checked>Todos</label> 
		            @else
		               <label class="checkbox-inline"><input type="checkbox" onchange="this.form.submit()" name="buscarFecha" value="1"  >Todos</label> 
		            @endif                            	   
		        	{{ Form::input('date', 'fechaRegistro', $model->fechaRegistro, ['class'=>'datepicker']) }}
		        	
		        	<select name="estado" class="form-control" onchange="this.form.submit()">
		              <option value="0"> Seleccione Estado </option>
		                @foreach($modelLab_estado as $dato)
		                	@if($dato->id == $model->estado)
		                		<option value="{{ $dato->id }}" selected="selected">{{ $dato->nombre }}</option>
		                    @else
		                    	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
		                    @endif
		                @endforeach
		            </select>
		        	
		          	{!! Form::text('searchNuevo', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
		          <button type="submit" class="btn btn-primary"> Buscar </button>
		        </form>         
	        </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr>
	 					<th>Nº</th>
	 					<th>Nº Historia</th>
	 					<th>Asegurado</th>
	 					<th>Especialidad</th>
	 					<th>Médico</th>
	 					<th></th>
	 					<th>Fecha Prog.</th>
	 					<th>Estado</th>
	 					<th></th>
					</tr>
					<?php
					// <tr id="indexSearch" style="background: #ffffff;">
	 			// 		<th>
	 			// 			<input type="text" id="txtSearch_codigo" class="form-control input-sm" style="text-transform: uppercase;">
	 			// 		</th>
	 			// 		<th>
	 			// 			<input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;">
	 			// 		</th>
	 			// 		<th>
	 			// 			<select id="selectSearch_especialidad" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
				 //            	<option value="0"> </option>
				 //                @foreach($modelLab_especialidad as $dato)
				 //                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
				 //                @endforeach
				 //            </select>
	 			// 		</th>
	 			// 		<th>
	 			// 			<select id="selectSearch_tipomuestra" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
				 //            	<option value="0"> </option>
				 //                @foreach($modelLab_tipo_muestra as $dato)
				 //                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
				 //                @endforeach
				 //            </select>
	 			// 		</th>
					// </tr>
					?>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<?php
						if($dato->idhcl_especialidad == Hcl_especialidad::Embarazo && $dato->idlab_estado == Lab_estado::PROGRAMACION)
			 				$table_tr_class = 'info';
						elseif($dato->idhcl_especialidad == Hcl_especialidad::Emergencias && $dato->idlab_estado == Lab_estado::PROGRAMACION)
			 				$table_tr_class = 'danger';
						elseif($dato->idima_estado == Lab_estado::PROGRAMACION)
			 				$table_tr_class = 'active';
			 			elseif($dato->idima_estado == Lab_estado::ENTREGADO)
			 				$table_tr_class = 'success';
			 			else
			 				$table_tr_class = 'default';
			 		?>
					<tr role="row" class="{{$table_tr_class}}" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-1">{{ $dato->numero }}</td>
						<td class="col-xs-1">{{ $dato->hcl_poblacion_matricula }}</td>
						<td>{{ $dato->hcl_poblacion_nombrecompleto }}</td>
						<td class="col-xs-2">{{ $dato->cuaderno }}</td>
						<td class="col-xs-2">{{ $dato->nombres.' '.$dato->apellidos }}</td>
						<td class="col-xs-0">
							@if($dato->idhcl_cabecera_registro > 0)
								<?php
								$fecha = date('d-m-Y', strtotime($dato->fechaprogramacion_sistema));
								$hora = $dato->horaprogramacion_sistema;
								$glosa = 'Fecha: '.$fecha.', hora: '.$hora;
								?>
								<i class="fa fa-stethoscope" title="Programado desde SALUD. {{ $glosa }}" style="cursor: help;"></i>
							@endif
						</td>
						<td class="col-xs-1">
							{{ $dato->fechaprogramacion != null? date('d-m-Y', strtotime($dato->fechaprogramacion)) : '' }}
						</td>
						<td class="col-xs-1">{{ $dato->estado }}</td>	
						<td class="col-xs-1">
							<a href="{{ route('programacion.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
							<button type="button" id="{{$dato->id}}" class="btn btn-default btn-sm" title="Reporte Programación">
								<i class="glyphicon glyphicon-print"></i>
							</button>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      	{!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>

<div class="box box-success">
	@foreach($modelo as $datos) 
		<a class="btn btn-app">
	    	<i class="fa fa-heartbeat"> {{ $datos->total }}</i> {{ $datos->nombre }}
	  	</a>
	@endforeach	     
</div>
@stop




<!-- MODAL PARA VISUALIZAR EL REPORTE -->
<div class="modal" id="vImprimeProgramacion">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title"> REPORTE PROGRAMACIÓN </h4>
        </div>
        <div class="modal-body">
         	<div id="secccionImprimeProgramacion" style="height: 410px;"></div>

	 		<div class="progress">
			  <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			  </div>
			</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">
            <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span> 
              Salir
          </button>
        </div>
    </div>
  </div>
</div>