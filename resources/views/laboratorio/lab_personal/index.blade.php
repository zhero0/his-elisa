@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE PERSONAL LABORATORIO')
@section('htmlheader_title', 'LISTA DE PERSONAL LABORATORIO')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('personal_lab.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr> 
	 					<th>Cuaderno</th>
	 					<th>Medico</th> 
	 					<th>Usuario</th>
	 					<th></th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-5">{{ $dato->nombre }}</td>
						<td class="col-xs-6">{{ $dato->apellidos.' '.$dato->nombres }}</td>  
						<td class="col-xs-1">{{ $dato->usuario }}</td> 
						<td>
							<a href="{{ route('personal_lab.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="REMOVER PERSONAL {{ $dato->id }}">
								<span class="glyphicon glyphicon-trash"></span>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@stop