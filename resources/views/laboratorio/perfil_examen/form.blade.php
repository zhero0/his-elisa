<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" id="txtListaTipoVariable" name="txtListaTipoVariable">
    <input type="hidden" id="txtListagrupos" value="{{ $model->listagrupos }}">
    <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}"> 
    <!-- [2] -->
    <input type="hidden" id="txtListaVariables" name="txtListaVariables" value="{{$model->listavariable}}">

    
    <div class="row"> 
     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <button class="btn btn-warning" type="button" id="btnBuscarExamen">
                          <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                          Buscar Examen
                        </button>
                      </span>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8" style="text-align: right;">
                    <h4>Presione f9 Para Buscar</h4>
                  </div>
                </div>

                <div class="row">
                  <div class="col-xs-12 col-sm-10 col-md-10 col-lg-12">
                    <div class="table-responsive" style="height: 350px; overflow-y: auto;">
                      <table id="tablaContenedorDatos" class="table table-bordered table-hover table-condensed">
                          <thead style="{{ config('app.styleTableHead') }}">
                              <tr>
                                 <th id="thCantidadVariables">Examen (0)</th>
                                 <th class="col-xs-5">Grupo</th>
                                 <th class="col-xs-1">Orden</th> 
                                 <th class="col-xs-1"></th>
                             </tr>
                          </thead>
                          <tbody class="table-success" style="{{ config('app.styleTableRow') }}">
                            <tr id="idListaVacia">
                              <td>Lista Vacia...</td>
                            </tr>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>  
        </div>

</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarExamen" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif 