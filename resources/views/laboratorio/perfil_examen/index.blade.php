@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE PERFILES')
@section('htmlheader_title', 'LISTA DE PERFILES')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('perfil_examen.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr> 
	 					<th>Perfil</th>
	 					<th>Código</th>
	 					<th>Exámen</th>
	 					<th>Usuario</th>
	 					<th></th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-3">{{ $dato->nombre }}</td>
						<td class="col-xs-1">{{ $dato->codigo }}</td>
						<td>{{ $dato->nombre_examen }}</td>
						<td class="col-xs-1">{{ $dato->usuario }}</td> 
						<td class="col-xs-1">
							<a href="{{ route('perfil_examen.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a>
							<a href="{{ route('perfil_examen.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@stop