@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVO PERFIL')
@section('htmlheader_title', 'NUEVO PERFIL')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="panel panel-primary">
      		<div class="panel-body">      			
      			{!! Form::Open(['route' => 'perfil_examen.store', 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}			
			</div>
		</div>
	</div>
</div>
@stop
@include($model->rutaview.'modal')