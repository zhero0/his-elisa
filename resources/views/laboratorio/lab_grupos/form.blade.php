<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    <input type="hidden" id="txtJsonExamenes" name="txtJsonExamenes">
    <input type="hidden" id="lab_examenesEspecialidad" value="{{ $model->lab_examenesEspecialidad }}">
    
    <div class="row"> 
      <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'autofocus' => 'autofocus']) !!}
        
        {!! Form::label('descripcion', 'Descripción:', ['class' => 'label-control']) !!}
        {!! Form::textarea('descripcion', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'rows' => 4]) !!}
      </div>

      <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <div class="input-group">
              <input type="text" id="txtBuscarExamen" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
              <span class="input-group-btn">
                <button class="btn btn-warning" type="button" id="btnBuscarExamen">
                  <span class="fa fa-search" aria-hidden="true"></span>
                  Buscar
                </button>
              </span>
            </div>
          </div>
        </div>
        
        <div class="row">
          <div id="divContenedor_Informacion_Lab_Ima" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive" style="height: 400px; overflow-y: auto; border: 1px solid #374850;">
              <table id="tablaContenedorExamenes" class="table table-bordered table-hover table-condensed">
                  <thead style="{{ config('app.styleTableHead') }}">
                      <tr>
                        <th class="col-xs-2">Código</th>
                        <th>Nombre</th>
                        <th class="col-xs-5">Especialidad</th>
                        <th class="col-xs-1"></th>
                     </tr>
                  </thead>
                  <tbody class="table-success" id="tbodyExamenes" style="{{ config('app.styleTableRow') }}">
                    <tr>
                      <td>Vacío...</td>
                    </tr>
                  </tbody>
              </table>
            </div>

            <button class="btn btn-default" id="btnVisualizaExamenesSeleccionados" type="button"  title="Click para visualizar los Exámenes Seleccionados">
            Exámenes Seleccionados <span class="badge" id="spanTotalExamenSeleccion">0</span>
            </button>
            <button type="button" id="btnBorrarExamenesSeleccionados" class="btn btn-danger" title="Borrar Exámenes Seleccionados">
                <span class="fa fa-trash-o" aria-hidden="true"></span>
                Borrar Exámenes Seleccionados
            </button>
            <div class="timeline-footer">
              <div class="alert alert-danger" id="divErrorExamen" role="alert">
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                Seleccione algún EXÁMEN
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardar" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif