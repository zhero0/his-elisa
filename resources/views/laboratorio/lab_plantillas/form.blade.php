<div class="form-group">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <input type="hidden" name="idalmacen" value="{{ $model->idalmacen }}">
    
    <div class="row">
      @if(count($errors) > 0)
        <div class="col-xs-12">
          <div class="panel panel-danger">
            <div class="panel-heading">VALIDACIONES</div>
              <div class="panel-body">
                <ul class="list-group">
              @foreach($errors->all() as $error)
                <h4 style="color: #ad0d0c;">
                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                  {{ $error }}
                </h4>
              @endforeach
              </ul>
            </div>
          </div>
        </div>
      @endif
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="especialidad">Especialidad</label>
        <select id="especialidad" name="especialidad" class="form-control">
          <option value="0"> :: Seleccionar</option>  
          @foreach($modelEspecialidad as $especialidad)
            @if($model != null)
              <option value="{{$especialidad->id}}"  {{ $model->idima_especialidad == $especialidad->id ? 'selected="selected"' : '' }} >{{$especialidad->nombre}}</option>    
            @else
               <option value="{{$especialidad->id}}">{{$especialidad->nombre}}</option> 
            @endif                      
          @endforeach 
        </select>
      </div> 
    </div> 

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <label for="dato">Personal</label>
        <select id="dato" name="dato" class="form-control">
          <option value="0"> :: Seleccionar</option>  
          @foreach($modelDatosgenericos as $dato)
            @if($model != null)
              <option value="{{$dato->id}}"  {{ $model->iddatosgenericos == $dato->id ? 'selected="selected"' : '' }} >{{ $dato->nombres}}</option>    
            @else
               <option value="{{$dato->id}}">{{ $dato->nombres}}</option> 
            @endif                      
          @endforeach 
        </select> 
      </div>
    </div>  
    
    <div class="row"> 
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <br>
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
        <label>Plantilla</label>
        <textarea class="form-control" id="summary-ckeditor" name="plantilla" value="Escriba...">{{ $model->plantilla != '' ? $model->plantilla : '' }}</textarea> 
        
      </div>
    </div> 
</div>

<a href="/plantilla_lab" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="submit" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif