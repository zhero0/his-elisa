@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE PLANTILLAS')
@section('htmlheader_title', 'LISTA DE PLANTILLAS')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('plantilla_lab.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr> 
	 					<th>Codigo</th>
	 					<th>Nombre</th> 
	 					<th>Especialidad</th>
	 					<th>Medico</th>
	 					<th>Usuario</th>
	 					<th></th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-1">{{ $dato->id }}</td>
						<td class="col-xs-3">{{ $dato->nombre_plantilla }}</td> 						
						<td class="col-xs-3">{{ $dato->nombre_especialidad }}</td>
						<td class="col-xs-3">{{ $dato->nombres }}</td>
						<td>{{ $dato->usuario }}</td> 
						<td>
							<a href="{{ route('plantilla_lab.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a>
							<a href="{{ route('plantilla_lab.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>

		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@stop