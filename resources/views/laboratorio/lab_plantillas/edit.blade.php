@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR PLANTILLA')
@section('htmlheader_title', 'EDITAR PLANTILLA')
 
@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
		<div class="panel panel-primary">
      		<div class="panel-body">
				{!! Form::model($model, ['route' => ['plantilla_lab.update', $model->id], 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop