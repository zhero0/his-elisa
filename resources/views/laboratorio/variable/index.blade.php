@extends('layouts.admin')
@section('contentheader_title', 'LISTA DE VARIABLES')
@section('htmlheader_title', 'LISTA DE VARIABLES')

<?php
use Illuminate\Support\Facades\Input; 
?>

@section('main-content')
<div class="box box-primary">
 	<div class="row">
 		<input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
 		<!-- "BUSCADOR MULTIPLE" -->
 		<div class="row">
	 		<form id="formBuscador" class="navbar-form navbar-right" action="/variable" autocomplete="off">
	 			<textarea class="form-control" id="datosBuscador" name="datosBuscador" style="display: none;"> {{ $model->datosBuscador }} </textarea>
	 		</form>
 		</div>
 		<!-- FIN "BUSCADOR MULTIPLE" -->


		<div class="container-fluid">
			<div class="col-xs-3 col-sm-3 col-md-2 col-lg-2" style="text-align: left;">
				<div class="row" style="height: 6px; color: transparent;">.</div>
		        <a type="button" class="btn btn-primary" href="{{ route('variable.create') }}">
		            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Nuevo
		        </a>
		    </div>
		</div>
	</div>

 	<div class="box-body">
	 	<div class="table-responsive" style="{{ config('app.styleIndex') }}">
			<table class="table table-bordered table-hover table-condensed">
				<thead style="{{ config('app.styleTableHead') }}">
					<tr>
	 					<th>Sigla</th>
	 					<th>Nombre</th>
	 					<th>Valor Inferior</th>
	 					<th>Valor Superior</th>
	 					<th>Unidad</th>
	 					<th>Tipo</th>
	 					<th></th>
					</tr>
					<tr id="indexSearch" style="background: #ffffff;">
	 					<th>
	 						<input type="text" id="txtSearch_sigla" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<input type="text" id="txtSearch_nombre" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<input type="text" id="txtSearch_valorInferior" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<input type="text" id="txtSearch_valorSuperior" class="form-control input-sm" style="text-transform: uppercase;">
	 					</th>
	 					<th>
	 						<input type="text" id="txtSearch_unidad" class="form-control input-sm">
	 					</th>
	 					<th>
	 						<select id="selectSearch_tipo_variables" onchange="Busqueda_index( $(this).attr('id') )" class="form-control input-sm">
				            	<option value="0"> </option>
				                @foreach($modelLab_tipo_variables as $dato)
				                	<option value="{{ $dato->id }}">{{ $dato->nombre }}</option>
				                @endforeach
				            </select>
	 					</th>
					</tr>
				</thead>
				@foreach($model as $dato)
				<tbody>
					<tr role="row" style="{{ config('app.styleTableRow') }}">
						<td class="col-xs-1">{{ $dato->sigla }}</td>
						<td title="Edad entre {{ $dato->edad_inicial.' y '.$dato->edad_final }}">{{ $dato->nombre }}</td>
						<td class="col-xs-1">{{ $dato->valor_inf }}</td>
						<td class="col-xs-1">{{ $dato->valor_sup }}</td>
						<td class="col-xs-1">{{ $dato->unidad }}</td>
						<td class="col-xs-2">{{ $dato->tipo_variables }}</td>
						<td class="col-xs-1">
							<a href="{{ route('variable.show', $dato->id) }}" type="button" class="btn btn-default btn-sm" title="VER">
								<span class="glyphicon glyphicon-eye-open"></span>
							</a>
							<a href="{{ route('variable.edit', $dato->id) }}" class="btn btn-default btn-sm" title="Editar">
								<i class="glyphicon glyphicon-pencil"></i>
							</a>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>
		
		<div class="text-center" style="{{ config('app.stylePaginacion') }}">
	      {!! $model->appends(Input::except('page'))->render() !!}
	    </div>
 	</div>
</div>
@stop