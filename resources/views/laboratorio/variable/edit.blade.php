@extends('adminlte::layouts.app')
@section('contentheader_title', 'EDITAR VARIABLE')
@section('htmlheader_title', 'EDITAR VARIABLE')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
    	<div class="panel panel-primary">
      		<div class="panel-body"> 
      			{!! Form::model($model, ['route' => ['variable.update', $model->id], 'autocomplete' => 'off', 'method' => 'PUT', 'id' => 'form']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop
@include($model->rutaview.'modal')