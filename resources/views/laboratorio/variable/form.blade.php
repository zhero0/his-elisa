<div class="form-group">
    <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="txtAccion" value="{{ $model->accion }}">
    <input type="hidden" id="txtScenario" value="{{ $model->scenario }}">
    <!-- <input type="hidden" id="txtListaTipoVariable" name="txtListaTipoVariable"> -->
    <input type="hidden" id="txtParametro" name="txtParametro">
    <input type="hidden" id="idalmacen" name="idalmacen" value="{{ $model->idalmacen }}">
    <input type="hidden" id="txtHiddenJSONdatos" name="txtHiddenJSONdatos" value="{{ $model->txtHiddenJSONdatos }}">
    <!-- [2] -->
    <input type="hidden" id='txtListavariable' value="{{$model->listavariable}}">

    <input type="hidden" id="txtLab_formula" value="{{ $model->lab_formula }}">


    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        {!! Form::label('sigla', 'Sigla', ['class' => 'label-control']) !!}
        {!! Form::text('sigla', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'autofocus' => 'autofocus']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control','placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        {!! Form::label('valor_inf', 'Valor Inferior', ['class' => 'label-control']) !!}
        {!! Form::text('valor_inf', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        {!! Form::label('valor_sup', 'Valor Superior:') !!}
        {!! Form::text('valor_sup', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        {!! Form::label('unidad', 'Unidad:', ['class' => 'label-control']) !!}
        {!! Form::text('unidad', null, ['class' => 'form-control', 'placeholder' => 'Escriba...']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        {!! Form::label('metodo', 'Metodo:', ['class' => 'label-control']) !!}
        {!! Form::text('metodo', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-11 col-md-4 col-lg-4">
        <label for="variable">Tipo</label>
        <select id="variable" name="variable" class="form-control">
          <option value="0"> :: Seleccionar</option>
            @foreach($modelTipoVariables as $variable)
              @if($model != null)
                <option value="{{ $variable->id.'-'.$variable->permite_llenado.'-'.$variable->selecciona_formula }}"
                  {{ $model->idlab_tipo_variables == $variable->id ? 'selected="selected"' : ''  }} > {{$variable->nombre}}
                </option>  
              @else
                 <option value="{{ $variable->id.'-'.$variable->permite_llenado.'-'.$variable->selecciona_formula }}">{{$variable->nombre}}</option> 
              @endif                        
            @endforeach
        </select> 
      </div>  
    </div>

    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        {!! Form::label('sexo', 'Sexo', ['class' => 'label-control']) !!}
        {!! Form::text('sexo', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;', 'autofocus' => 'autofocus']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        {!! Form::label('edad_inicial', 'Edad Desde:', ['class' => 'label-control']) !!}
        {!! Form::text('edad_inicial', null, ['class' => 'form-control','placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div>
      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
        {!! Form::label('edad_final', 'Edad Hasta:', ['class' => 'label-control']) !!}
        {!! Form::text('edad_final', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
      </div> 
    </div>


    <div class="row">
      <div class="col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <div id="valorNum" style="display: none;">
          {!! Form::label('valorNumero', 'Valor numero', ['class' => 'label-control']) !!}
          {!! Form::number('valorNumero', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;','step' =>'any']) !!}
        </div>
        <div id="valorText" style="display: none;">
          {!! Form::label('valorTexto', 'Valor Texto', ['class' => 'label-control']) !!}
          {!! Form::text('valorTexto', null, ['class' => 'form-control', 'placeholder' => 'Escriba...', 'style' => 'text-transform: uppercase;']) !!}
        </div>
      </div>
    </div>


    <div class="row" id="divListaVariable" style="display: none;">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
        <div class="panel panel-primary">
          <div class="panel-body"> 

            <div class="row">
              <div class="col-xs-12 col-sm-10 col-md-8 col-lg-8">
                <div class="input-group">
                  <input type="text" id="txtNombreTipo" class="form-control" placeholder="ESCRIBA" style="text-transform: uppercase;">
                  <span class="input-group-btn">
                    <button class="btn btn-warning" type="button" id="btnAgregarTipo">
                      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                      Agregar
                    </button>
                  </span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                <div class="table-responsive" style="max-height: 230px; overflow-y: auto;">
                  <table id="tablaContenedorDatos" class="table table-bordered table-hover table-condensed">
                      <thead style="background: #374850; color: #f3eaf0;">
                          <tr>
                             <th id="thCantidadTipoVariable">Tipo Variable (0)</th>
                             <th></th>
                         </tr>
                      </thead>
                      <tbody class="table-success">
                        <tr id="idListaVacia">
                          <td>Lista Vacia...</td>
                        </tr>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>      
    </div>


    <div class="row" id="divFormula_parametros" style="display: none;">
      <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
        <div class="panel panel-primary">
          <div class="panel-body">

            <div class="row">
              <div class="col-xs-12 col-sm-4 col-md-2 col-lg-2">
                <button type="button" id="btnMostrarFormulas" class="btn btn-warning" title="Pulse Click para Seleccionar Fórmula! ">
                  <span class="fa fa-medkit" aria-hidden="true"></span> Fórmulas
                </button>
              </div>
              <div class="col-xs-12 col-sm-8 col-md-10 col-lg-10">
                <div class="well well-sm" id="divFormulaSeleccionada">
                  ....
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="table-responsive" style="max-height: 200px; overflow-y: auto;">
                  <table id="tablaContenedorDatos_FormulaExamen" class="table table-bordered table-hover table-condensed">
                      <thead style="{{ config('app.styleTableHead') }}">
                          <tr>
                            <th>Exámen</th>
                            <th class="col-xs-2">Parámetro</th>
                            <th class="col-xs-1"></th>
                         </tr>
                      </thead>
                      <tbody class="table-success" id="tbody_FormulaExamen" style="{{ config('app.styleTableRow') }}">
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

</div>

<a href="{{ URL::previous() }}" class="btn btn-primary">
  <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Volver
</a>
@if($model->scenario != 'view')
  <button type="button" id="btnGuardarVariable" class="btn btn-success">
    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Guardar
  </button>
@endif 