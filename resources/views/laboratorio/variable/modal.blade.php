<!-- VENTANA MODAL PARA BUSCAR FÓRMULAS -->
<div class="modal fade" id="vModalBuscadorFormula">
	<div class="modal-dialog modal-md">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="favoritesModalLabel">FÓRMULAS</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="table-responsive" style="height: 350px; overflow-y: auto;">
	                <table id="tablaContenedorDatos_Formula" class="table table-bordered table-hover table-condensed">
	                    <thead style="{{ config('app.styleTableHead') }}">
	                        <tr>
		                        <th class="col-xs-10">Fórmula</th>
	                            <th></th>
	                       </tr>
	                    </thead>
	                    <tbody class="table-success" id="tbody_Formula" style="{{ config('app.styleTableRow') }}">
	                      <tr id="idListaVaciaFormula">
	                        <td>Lista Vacía...</td>
	                      </tr>
	                    </tbody>
	                </table>
	            </div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>



<!-- VENTANA MODAL PARA BUSCAR EXÁMENES -->
<div class="modal fade" id="vModalBuscadorExamen">
	<div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <button type="button" class="close" 
		          data-dismiss="modal" 
		          aria-label="Close">
		          <span aria-hidden="true">&times;</span></button>
		        <h4 class="modal-title" id="tituloModal">EXÁMENES DE LABORATORIO</h4>
		    </div>
		    <div class="modal-body">
		    	<div class="row">
			    	<!-- <div id="divContenedor_GruposLaboratorio" class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
						<div class="list-group" id="listaGrupo">
							<button type="button" class="list-group-item">BASICO <span class="badge">3</span></button>
							<button type="button" class="list-group-item">LAB. HEMATOLOGIA <span class="badge">14</span></button>
							<button type="button" class="list-group-item">Morbi leo risus <span class="badge">14</span></button>
							<button type="button" class="list-group-item">Porta ac consectetur ac <span class="badge">14</span></button>
							<button type="button" class="list-group-item">Vestibulum at eros <span class="badge">14</span></button>
						</div>
					</div> -->
					
					<div class="col-xs-12">
				    	<div class="input-group">
		                  <input type="text" id="txtBuscarExamen" class="form-control" placeholder="Buscar..." style="text-transform: uppercase;">
		                  <span class="input-group-btn">
		                    <button class="btn btn-warning" type="button" id="btnBuscarExamen">
		                      <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
		                      Buscar
		                    </button>
		                  </span>
		                </div>
		                
				    	<div class="table-responsive" style="height: 350px; overflow-y: auto; border: 1px solid #374850;">
		                    <table id="tablaContenedorExamenes" class="table table-bordered table-hover table-condensed">
		                        <thead style="{{ config('app.styleTableHead') }}">
		                            <tr>
		                              <th class="col-xs-2">Código</th>
		                              <th>Nombre</th>
		                              <th class="col-xs-5">Especialidad</th>
		                              <th class="col-xs-1"></th>
		                           </tr>
		                        </thead>
		                        <tbody class="table-success" id="tbodyExamenes" style="{{ config('app.styleTableRow') }}">
		                          <tr>
		                            <td>Vacío...</td>
		                          </tr>
		                        </tbody>
		                    </table>
		                </div>

		                <!-- <div class="row">
		                	<div class="col-xs-12 col-sm-4 col-md-3 col-lg-4">
				                <button class="btn btn-default" id="btnVisualizaExamenesSeleccionados" type="button"  title="Click para visualizar los Exámenes Seleccionados">
				                    Total Seleccionados <span class="badge" id="spanTotalExamenSeleccion">0</span>
				                </button>
			                </div>
			                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-8">
			                	<label>Observación</label>
			           			<textarea class="form-control" id="taObservaciones_lab_ima" rows="2" placeholder="Escriba..."></textarea>
			                </div>
		                </div> -->
	            	</div>
            	</div>
	      	</div>
	      	<div class="modal-footer">
		        <button type="button" id="btnCerrar_lab_ima" class="btn btn-danger" data-dismiss="modal">
			        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
			       	Cerrar
		       	</button>
		    </div>
	    </div>
	</div>
</div>
