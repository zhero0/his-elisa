@extends('adminlte::layouts.app')
@section('contentheader_title', 'NUEVA VARIABLE')
@section('htmlheader_title', 'NUEVA VARIABLE')

@section('main-content')
<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
    	<div class="panel panel-primary">
      		<div class="panel-body"> 
				{!! Form::Open(['route' => 'variable.store', 'method' => 'POST', 'id' => 'form', 'autocomplete' => 'off']) !!}
					@include($model->rutaview.'form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop
@include($model->rutaview.'modal')