<?php
namespace App\Http\Controllers\Internacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Internacion\Int_recepcion;
use \App\Models\Internacion\Int_servicio;
use \App\Models\Internacion\Int_sala;
use \App\Models\Internacion\Int_camas;
use \App\Models\Afiliacion\Hcl_internacion;
use \App\Models\Afiliacion\Hcl_cabecera_registro;
use \App\Models\Internacion\Int_estado_recepcion;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission; 
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

class Int_recepcionController extends Controller
{
    private $rutaVista = 'internacion.int_recepcion.';
    private $controlador = 'int_recepcion';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema(); 

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $modelEstado = Int_estado_recepcion::orderBy('nombre', 'ASC')->get();
        $modelServicio = Int_servicio::orderBy('nombre', 'ASC')->get();
        $modelSala = Int_sala::orderBy('nombre', 'ASC')->get();
        $modelCamas = Int_camas::orderBy('numero', 'ASC')->get();
        $matricula = ''; 
        $documento = ''; 
        $nombres = '';
        $servicio = '';

        $estado = 0; 
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $matricula = $datosBuscador->matricula;
            $documento = $datosBuscador->documento; 
            $nombres = $datosBuscador->nombres; 
            $servicio = $datosBuscador->servicio > 0? $datosBuscador->servicio : 0;
        }
        $model = Int_recepcion::where([
                    // ['idint_estado_recepcion_ingreso', '=', $estado],
                    ['matricula', 'ilike', '%'.$matricula.'%'],
                    ['documento', 'ilike', '%'.$documento.'%'],
                    ['nombre_completo', 'ilike', '%'.$nombres.'%'],  
                    ['eliminado', '=', 0],
                    // ['idalmacen', '=', $arrayDatos['idalmacen']]
                ]) 
                ->orderBy('matricula', 'ASC')
                ->paginate(config('app.pagination'));

        $model->datosBuscador = $request->datosBuscador;

        $model->scenario = 'index';
        $model->accion = $this->controlador;
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelEstado', $modelEstado)
                ->with('modelServicio', $modelServicio)
                ->with('modelSala', $modelSala)
                ->with('modelCamas', $modelCamas)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Int_recepcion; 
        $model->especialidad = '';
        $model->diagnostico = '';
        $model->medico = '';
        $modelServicio = Int_servicio::orderBy('nombre', 'ASC')->get();
        $modelSala = Int_sala::orderBy('nombre', 'ASC')->get();
        $modelCamas = Int_camas::orderBy('numero', 'ASC')->get();
        $modelEstado = Int_estado_recepcion::orderBy('nombre', 'ASC')->get();

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelEstado', $modelEstado)
                ->with('modelServicio', $modelServicio)
                ->with('modelSala', $modelSala)
                ->with('modelCamas', $modelCamas)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    { 

        // $rules = [            
        //     'codigo' => 'required', 
        //     'nombre' => 'required', 
        // ];
        // $messages = [
        //     'codigo.required' => 'Se requiere registrar Codigo', 
        //     'nombre.required' => 'Se requiere registrar Nombre',             
        // ];
        // $this->validate($request, $rules, $messages);
 

        $modelRegistro = Hcl_cabecera_registro::find($request->txtidcabecera);
        $date = date_create($request->fechaprogramacion);
        $model = new Int_recepcion; 

        $model->idhcl_poblacion = strtoupper($modelRegistro->idhcl_poblacion);
        $model->matricula = strtoupper($modelRegistro->hcl_poblacion_matricula); 
        $model->documento = 0;
        $model->nombre_completo = strtoupper($modelRegistro->hcl_poblacion_nombrecompleto);
        $model->fecha_nacimiento = $modelRegistro->hcl_poblacion_fechanacimiento;
        $model->edad = $modelRegistro->hcl_poblacion_edad;
        $model->idhcl_cuaderno = $modelRegistro->idhcl_cuaderno;
        $model->iddatosgenericos = $modelRegistro->iddatosgenericos;
        $model->idhcl_cabecera_registro = $modelRegistro->id;
   
        $model->fecha_ingreso = date_format($date, 'Y-m-d');
        $model->hora_ingreso = $request->txtHiddenHora;


        $model->idint_estado_recepcion_ingreso = $request->estado;        

        $model->idint_servicio = $request->idServicio;
        $model->idint_sala = $request->idSala;
        $model->idint_camas = $request->idCamas; 

        $model->descripcion_ingreso = $request->descripcion_ingreso;

        $model->idalmacen = $request->idalmacen;; 
        $model->usuario = Auth::user()['name'];
        
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("int_recepcion")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_examenes::find($codigo);
        $modelEspecialidad = Ima_especialidad::all();
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Int_recepcion::find($id);   
        $modelCuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno); 
        $modelInternacion = Hcl_internacion::where('idhcl_cabecera_registro', '=',$model->idhcl_cabecera_registro)->first(); 
        $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos); 
        $model->medico = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;
        $model->diagnostico = $modelInternacion->diagnostico;
        $model->especialidad = $modelCuaderno->nombre;
        $model->hora_programacion = $model->hora_ingreso;
        $modelServicio = Int_servicio::orderBy('nombre', 'ASC')->get();
        $modelSala = Int_sala::orderBy('nombre', 'ASC')->get();
        $modelCamas = Int_camas::orderBy('numero', 'ASC')->get();
        $modelEstado = Int_estado_recepcion::orderBy('nombre', 'ASC')->get();
  
        $model->idalmacen = $arrayDatos['idalmacen'];
 
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelEstado', $modelEstado)
                ->with('modelServicio', $modelServicio)
                ->with('modelSala', $modelSala)
                ->with('modelCamas', $modelCamas)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Int_recepcion::find($id);  
        // print_r($request->fechaprogramacion);
        // return;

        $modelRegistro = Hcl_cabecera_registro::find($model->idhcl_cabecera_registro);
        $date = date_create($request->fechaprogramacion); 

        $model->idhcl_poblacion = strtoupper($modelRegistro->idhcl_poblacion);
        $model->matricula = strtoupper($modelRegistro->hcl_poblacion_matricula); 
        $model->documento = 0;
        $model->nombre_completo = strtoupper($modelRegistro->hcl_poblacion_nombrecompleto);
        $model->fecha_nacimiento = $modelRegistro->hcl_poblacion_fechanacimiento;
        $model->edad = $modelRegistro->hcl_poblacion_edad;
        $model->idhcl_cuaderno = $modelRegistro->idhcl_cuaderno;
        $model->iddatosgenericos = $modelRegistro->iddatosgenericos;
        $model->idhcl_cabecera_registro = $modelRegistro->id;
   
        $model->fecha_ingreso = date_format($date, 'Y-m-d'); 

        $model->hora_ingreso = $request->txtHiddenHora;


        $model->idint_estado_recepcion_ingreso = $request->estado;        

        $model->idint_servicio = $request->idServicio;
        $model->idint_sala = $request->idSala;
        $model->idint_camas = $request->idCamas; 

        $model->descripcion_ingreso = $request->descripcion_ingreso;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("int_recepcion")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

    public function buscaInternado()
    { 
        if(isset($_POST['datoPersona'])||isset($_POST['datoMatricula']))
        {
            $persona = $_POST['datoPersona'];
            $matricula = $_POST['datoMatricula']; 

            $modelo = DB::table(DB::raw("consulta_internados('".$matricula."','".$persona."')"))
                ->take(config('app.muestraTotalBusqueda'))
                ->get();

            echo json_encode($modelo);
        }
    }

    public function muestraSala()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];  
            $modelo =   Int_sala::where([
                            ['eliminado', '=', 0],                            
                            ['idint_servicio', '=', $dato]
                        ])                        
                        ->orderBy('nombre', 'ASC')
                        ->limit(100)
                        ->get()->toJson();
            return $modelo;
        }
    }

    public function muestraCama()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];  
            $modelo =   Int_camas::where([
                            ['eliminado', '=', 0],                            
                            ['idint_sala', '=', $dato]
                        ])  
                        ->whereNotIn('id', DB::table("int_recepcion")->select('idint_camas'))                      
                        ->orderBy('numero', 'ASC')
                        ->limit(100)
                        ->get()->toJson();
            return $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeRecepcion($id)
    {
        Int_recepcion::imprimeRecepcion($id);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
}
