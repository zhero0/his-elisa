<?php

namespace App\Http\Controllers;

use App\Http\Requests; 
use \App\User;
use Illuminate\Http\Request; 
use Auth;
use Image;
use \App\Models\Almacen\Home; 
use Hash;
use Validator;

class UserController extends Controller
{
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index()
    {
        $arrayDatos = Home::parametrosSistema();      
        return view('perfil.index',array('user'=>Auth::user()))
                    ->with('arrayDatos', $arrayDatos);
    }
 
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data=Request::all();  
        // return view('almacen.create');     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo="")
    { 
        print_r(555555555);
        //return view("almacen.index");
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_avatar(Request $request)
    {
        $arrayDatos = Home::parametrosSistema(); 
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . '.' .  $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/' . $filename ) );
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        $rules = ['mypassword' => 'required',
            'password' => 'required|confirmed|min:6|max:18',

        ];

        $messages = ['mypassword.required' => 'El campo es requerido',
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los password no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',

        ];

        $validator = Validator::make($request->all(),$rules,$messages);
        if ($validator->fails()) {
            return view('perfil.index',array('user'=>Auth::user()))
                ->with('arrayDatos', $arrayDatos)
                ->withErrors($validator);
        }else{
            if (Hash::check($request->mypassword, Auth::user()->password)) {
                $user = new user;
                $user->where ('email','=',Auth::user()->email)
                        ->update(['password'=> bcrypt($request->password)]);
                return view('perfil.index',array('user'=>Auth::user()))
                ->with('arrayDatos', $arrayDatos)
                ->with('status', 'Password cambiado con exito');          
            }else{
                return view('perfil.index',array('user'=>Auth::user()))
                ->with('arrayDatos', $arrayDatos)
                ->with('message', 'Credenciales incorrectas');  
            }
            
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   
}
