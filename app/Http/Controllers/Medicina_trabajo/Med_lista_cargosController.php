<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Medicina_trabajo\Med_lista_cargos;
use \App\Models\Medicina_trabajo\med_administracion;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Med_lista_cargosController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.med_lista_cargos.';
    private $controlador = 'med_lista_cargos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $nombre = '';
        $apellidos = '';
        $matricula = '';         
        $cargo = ''; 
        // $datosBuscador = json_decode($request->datosBuscador);
        // if($datosBuscador != '')
        // {
        //     $nombre = $datosBuscador->nombre;
        //     $descripcion = $datosBuscador->descripcion;               
        // }
        $model = Med_administracion::where([ 
                    // ['idalmacen', '=', $arrayDatos['idalmacen']],                    
                    // ['observacion', 'ilike', '%'.$observacion.'%'],                  
                ])      
                ->orderBy('id', 'ASC')          
                ->paginate(config('app.pagination'));   
        $modelDatos = DB::table(DB::raw("med_consulta_listacargos(".$arrayDatos['idalmacen'].")"))->get();   
        if ( count($modelDatos)==0) {
                $modelDatos = null;
            }

        $model->datosBuscador = $request->datosBuscador;
        

        $model->scenario = 'index';        
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelDatos', $modelDatos)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $model = new Med_clasificacion;   
        $model->scenario = 'create';        
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
        $model->idalmacen = $arrayDatos['idalmacen'];

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Med_clasificacion;
        $model->nombre = $request->nombre; 
        $model->descripcion = strtoupper($request->descripcion);
        $model->costo_tramite = $request->costo_tramite;
        $model->costo_formulario = $request->costo_formulario;
        $model->idalmacen = $request->idalmacen;        
        $model->usuario = Auth::user()->name;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $arrayDatos = Home::parametrosSistema();
        
        $modelo = Med_lista_cargos::where('idmed_administracion', '=', $id)
                        ->update([
                            'eliminado' => 1,
                        ]);  

        $model = Med_administracion::where([ 
                    // ['idalmacen', '=', $arrayDatos['idalmacen']],                    
                    // ['observacion', 'ilike', '%'.$observacion.'%'],                  
                ])      
                ->orderBy('id', 'ASC')          
                ->paginate(config('app.pagination'));

        $modelDatos = DB::table(DB::raw("med_consulta_listacargos(".$arrayDatos['idalmacen'].")"))->get();   
        if ( count($modelDatos)==0) {
                $modelDatos = null;
            }

        $model->scenario = 'index';        
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelDatos', $modelDatos)
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Med_clasificacion::find($id);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        $model->costo_tramite = $request->costo_tramite;
        $model->costo_formulario = $request->costo_formulario;        
        $model->usuario = Auth::user()->name;
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function buscaMed_clasificacion() {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $modelo = Med_clasificacion::where([
                                ['eliminado', '=', 0],                                
                                ['id', '=', $dato],                                
                            ])->get(); 
            echo json_encode($modelo);
        } 
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscar_Persona()
    { 
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Datosgenericos::where('eliminado', '=', 0)
                    ->where(function($query) use ($dato) {
                            $query->orwhere('nombres', 'ilike', '%'.$dato.'%');
                            $query->orwhere('apellidos', 'ilike', '%'.$dato.'%');
                            $query->orwhere(DB::raw("CONCAT(nombres, ' ', apellidos)"), 'ilike', '%'.$dato.'%');
                        })
                    ->orderBy('nombres', 'ASC')
                    ->get()->toJson();
            echo $modelo;
        } 
    }
    public function asigna_Cargo()
    {
        // print_r('expression');
        // return;

        if(isset($_POST['iddatosgenericos']) && isset($_POST['idCargo']))
        {
            $arrayDatos = Home::parametrosSistema();
            $pidmed_administracion = $_POST['idCargo'];
            $piddatosgenericos = $_POST['iddatosgenericos'];            

            $modelDato = DB::table(DB::raw("med_registra_cargo('".$pidmed_administracion."','".$piddatosgenericos."','".$arrayDatos['idalmacen']."','".Auth::user()->name."')"))
                                ->get();

            $model = Med_administracion::where([ 
                    // ['idalmacen', '=', $arrayDatos['idalmacen']],                    
                    // ['observacion', 'ilike', '%'.$observacion.'%'],                  
                ])      
                ->orderBy('id', 'ASC')          
                ->paginate(config('app.pagination'));   

            $modelDatos = DB::table(DB::raw("med_consulta_listacargos(".$arrayDatos['idalmacen'].")"))->get();  

            if ( count($modelDatos)==0) {
                $modelDatos = null;
            }

            $view1 = view('medicina_trabajo.med_lista_cargos.indexContent')
                    ->with('model', $model)
                    ->with('modelDatos', $modelDatos)
                    ->render();
            return response()->json(['view1' => $view1]);
            
            
        }  
 
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
}