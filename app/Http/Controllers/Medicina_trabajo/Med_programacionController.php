<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Medicina_trabajo\Med_registro;
use \App\Models\Medicina_trabajo\Med_programacion;
use \App\Models\Medicina_trabajo\Med_clasificacion;
use \App\Models\Medicina_trabajo\Med_datos;
use \App\Models\Medicina_trabajo\Med_estado;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

use Session;
use Illuminate\Support\Facades\URL;

class Med_programacionController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.med_programacion.';
    private $controlador = 'med_programacion';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    { 
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;

        $view = $this->rutaVista . 'index'; 

        $nro_documento = '';
        $paterno = '';
        $materno = '';
        $nombre = '';        
        $estado = 1;  
        if($request->all()) {
            $params = $request->all();
            $param = json_decode($params['param']);

            if(isset($param->dato)) {
                foreach ($param->dato as $value) {
                    if($value->name !== '') eval("$".$value->name." = '".$value->valor."';");
                }
            }
            if (strpos($param->pathname, $this->controlador) !== false) $view = $this->rutaVista.'indexContent';
        }   

        $modelEstados = Med_estado::get();

        
        $model = DB::table(DB::raw("mt_consulta_listaprogramacion(".
                                "'".$nro_documento."', ".
                                "'".$paterno."', ".
                                "'".$materno."', ".
                                "'".$nombre."', ".  
                                $estado.",".
                                $arrayDatos['idalmacen'].")"
                            ))->get();  

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;

        return view($view)
                ->with('model', $model) 
                ->with('modelEstados', $modelEstados) 
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $modelEstados = Med_estado::get();

        $model = new Med_programacion;
        $modelRegistro = new Med_registro;   
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];        

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelEstados', $modelEstados)
                ->with('modelRegistro', $modelRegistro)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Med_programacion;
        $model->nro_movil = $request->nro_movil;
        $model->fecha_registro = $request->fecha_registro;
        $model->hora_registro = $request->hora_registro;
        $model->idmed_registro = $request->idmed_registro;
        $model->idmed_estado = $request->idmed_estado;
        $model->idalmacen = $request->idalmacen;  

        $model->usuario = Auth::user()->name;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();  

        $model = Med_programacion::find($id);
        $modelRegistro = Med_registro::find($model->idmed_registro);
        $modelEstados = Med_estado::get(); 

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelEstados', $modelEstados)
                ->with('modelRegistro', $modelRegistro)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Med_programacion::find($id);
        $model->nro_movil = $request->nro_movil;
        $model->fecha_registro = $request->fecha_registro;
        $model->hora_registro = $request->hora_registro;
        $model->idmed_registro = $request->idmed_registro;
        $model->idmed_estado = $request->idmed_estado;
                
        $model->usuario = Auth::user()->name;
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function buscaPoblacionMed() {  
        if(isset($_POST['dato']))
        {    
            $arrayDatos = Home::parametrosSistema();  
            $dato = $_POST['dato'];   
            
            $modelo = Med_registro::where([
                    ['eliminado', '=', 0],
                    //['idalmacen', '=', $arrayDatos['idalmacen']],                         
                    //['idmed_clasificacion', '=', $formulario],                    
                ])->where(function ($query) use ($dato) {
                    $query->orwhere('hcl_poblacion_primer_apellido', 'ilike', '%' . $dato . '%');
                    $query->orwhere('hcl_poblacion_segundo_apellido', 'ilike', '%' . $dato . '%');
                    $query->orwhere('hcl_poblacion_nombres', 'ilike', '%' . $dato . '%');
                    $query->orwhere('hcl_poblacion_documento', 'ilike', '%' . $dato . '%');
                })->get();  
            $view1 = view($this->rutaVista.'._poblacion')
                    ->with('modelo', $modelo)
                    ->render(); 
            return response()->json(['view1' => $view1]);
        }
    }

}