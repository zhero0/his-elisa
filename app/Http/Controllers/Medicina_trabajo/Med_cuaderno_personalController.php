<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;   
use Illuminate\Support\Facades\DB; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Permission; 
 
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Medicina_trabajo\Med_cuaderno_personal;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Administracion\Datosgenericos; 
use \App\Models\Almacen\Home;

use Session;

class Med_cuaderno_personalController extends Controller
{
    private $rutaVista = 'medicina_trabajo.med_cuaderno_personal.';
    private $controlador = 'med_cuaderno_personal';
    private $idalmacen;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
 
        $model = DB::table(DB::raw("med_cuaderno_personal_lista(".$arrayDatos['idalmacen'].")"))->get();  
  

        $model->scenario = 'index';        
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
         
        // $modelHcl_cuaderno = ['' => 'Seleccione! '] + 
        //                             Hcl_cuaderno::where('eliminado', '=', 0)
        //                             ->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        // $modelDatosgenericos = Datosgenericos::where('id_generico', '=', Generico::MEDICO)
        //                             ->orderBy('apellidos', 'ASC')->get();
        
        $model = new Med_cuaderno_personal;

        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista; 

        return view($model->rutaview.'create')
                ->with('model', $model)  
                // ->with('modelHcl_cuaderno', $modelHcl_cuaderno)
                // ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {   
        $arrayDatos = Home::parametrosSistema();
        $model = new Med_cuaderno_personal;
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos; 
        $model->usuario = Auth::user()['name']; 

        if($model->save())
        {           
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    { 
        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // print_r($arrayDatos['idalmacen']);
        // return; 

        $model = Med_cuaderno_personal::find($codigo); 
        $model->eliminado = 1;
        $mensaje = config('app.mensajeGuardado');
 
        if($model->save()){
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado'); 

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        
        return view($model->rutaview.'edit')
                ->with('model', $model) 
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
   

        return redirect("programacion_ima")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }
 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "====".$id;
        return;
        $model = Hcl_cuaderno_personal::find($id); 

        if($model->delete())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("cuaderno_personal")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
