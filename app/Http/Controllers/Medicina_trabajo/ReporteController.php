<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use \App\User;
use \App\Models\Almacen\Home;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use \App\Permission;
use Charts;

use \App\Models\Afiliacion\Hcl_especialidad; 
use \App\Models\Almacen\Almacen; 

use Elibyy\TCPDF\Facades\TCPDF;



use \App\Models\Medicina_trabajo\Med_reporte;


class ReporteController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.med_reporte.';
    private $controlador = 'med_reporte';
    
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
    {
        $arrayDatos = Home::parametrosSistema();

       // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
       //      return view('errors.403')
       //                  ->with('arrayDatos', $arrayDatos)
       //                  ->with('url', '');

        $model = new Home;
        $model->idalmacen = $arrayDatos['idalmacen'];

        $this->modelosParametro($model);
        
        $dato =  "";
        // $modelEspecialidad = Ima_especialidad::where([
        //                     ['ima_especialidad.eliminado', '=', 0],
        //                     ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
        //                 ])->get();

        $data = DB::table( 'ima_detalleplacasutilizadas as dpu' )
                ->join('ima_resultados_programacion as result', 'result.id', '=', 'dpu.idima_resultados_programacion')
                ->join('ima_tipo_placas as itip', 'itip.id', '=', 'dpu.idima_tipo_placas')
                ->select('itip.id', 'itip.nombre')
                ->where([
                        ['result.numero_placa', '!=', null],
                        ['result.idalmacen', '=', $arrayDatos['idalmacen']]
                    ])
                ->get();
        
        $model_consultas_realizadas = DB::table('ima_registroprogramacion as rp')
                    ->leftjoin('ima_resultados_programacion as result', 'rp.id', '=', 'result.idima_registroprogramacion')
                    ->join('hcl_poblacion', 'hcl_poblacion.id', '=', 'rp.idhcl_poblacion')
                    ->join('hcl_especialidad', 'hcl_especialidad.id', '=', 'rp.idhcl_especialidad')
                    ->join('hcl_cuaderno', 'hcl_cuaderno.id', '=', 'rp.idhcl_cuaderno')
                    ->join('datosgenericos', 'datosgenericos.id', '=', 'rp.iddatosgenericos')
                    ->join('ima_especialidad as e', 'result.idima_especialidad', '=', 'e.id')
                    ->leftjoin('ima_estado as est', 'result.idima_estado', '=', 'est.id')
                    ->leftjoin('ima_examenes_registroprogramacion as ir', 'rp.id', '=', 'ir.idima_registroprogramacion')
                    ->join('ima_examenes as ie', 'ie.id', '=', 'ir.idima_examenes')
                    ->select('hcl_cuaderno.nombre as cuaderno','ie.nombre as nombre_examen')
                    ->where([
                        ['rp.eliminado', '=', 0], 
                        ['ir.eliminado', '=', 0], 
                        ['result.eliminado', '=', 0], 
                        ['rp.idalmacen', '=', $arrayDatos['idalmacen']],
                    ])
                    ->orderBy('rp.numero', 'DESC')
                    ->get();

        // $modelIma_cuaderno_datosgenericos = DB::table('ima_cuaderno_datosgenericos as cdg')
        //                 ->join('datosgenericos as dg', 'cdg.iddatosgenericos', '=', 'dg.id')
        //                 ->select('dg.id', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"))
        //                 ->where([
        //                     ['cdg.eliminado', '=', 0],
        //                     ['cdg.idalmacen', '=', $arrayDatos['idalmacen']]
        //                 ])
        //                 ->orderBy('dg.nombres', 'ASC')
        //                 ->orderBy('dg.apellidos', 'ASC')
        //                 ->get();  
         
        $chart = Charts::database($data, 'donut', 'fusioncharts')
                ->title('CANTIDAD DE PLACAS UTILIZADAS')
                ->elementLabel("Total Placas") 
                // ->values($data->values())
                ->dimensions(700, 500)
                ->responsive(false)
                ->groupBy('nombre');
        $chart2 = Charts::database($data, 'bar', 'fusioncharts')
                ->title('CANTIDAD DE PLACAS UTILIZADAS')
                ->elementLabel("Total Placas") 
                // ->values($data->values())
                ->dimensions(700, 500)
                ->responsive(false)
                ->groupBy('nombre');

        $chart3 = Charts::database($model_consultas_realizadas, 'line', 'fusioncharts')
                ->title('CANTIDAD DE SOLICITUDES')
                ->elementLabel("Total Solicitudes") 
                // ->values($data->values())
                ->dimensions(900, 700)
                ->responsive(false)
                ->groupBy('cuaderno');

        $chart4 = Charts::database($model_consultas_realizadas, 'line', 'fusioncharts')
                ->title('CANTIDAD DE EXAMENES SOLICITADOS')
                ->elementLabel("Total Examenes") 
                // ->values($data->values())
                ->dimensions(1000, 700)
                ->responsive(false)
                ->groupBy('nombre_examen'); 

        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('chart', $chart)
                ->with('chart2', $chart2)
                ->with('chart3', $chart3)
                ->with('chart4', $chart4) 
                //->with('modelIma_cuaderno_datosgenericos', $modelIma_cuaderno_datosgenericos)
                // ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    private function modelosParametro($model) {
        $month = date('m');
        $year = date('Y');
        $fechaInicio_sistema = date('Y-m-d', mktime(0,0,0, $month, 1, $year));
        
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
        $fechaFin_sistema = date('Y-m-d', mktime(0,0,0, $month, $day, $year));
        
        $model->fechaInicio = date('d-m-Y', strtotime($fechaInicio_sistema));
        $model->fechaFin = date('d-m-Y', strtotime($fechaFin_sistema));
        // $model->Ima_tipo_placas = Ima_tipo_placas::where('idalmacen', '=', $model->idalmacen)->orderBy('nombre', 'ASC')->get();

        
        // REPORTES
        $model->Ima_especialidad = Ima_especialidad::where([
                                        ['eliminado', '=', 0],
                                        ['idalmacen', '=', $model->idalmacen]
                                    ])->orderBy('nombre', 'ASC')->get();
        $model->medicos = DB::table('hcl_medicos_view')
                        ->where('idalmacen', '=', $model->idalmacen)
                        ->get();
        $model->Hcl_especialidad = Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->get();
        $model->Ima_tipo_placas = Ima_tipo_placas::where('idalmacen', '=', $model->idalmacen)->orderBy('nombre', 'ASC')->get();
    }

    
    // ------------------------------------------- REPORTES -------------------------------------------
    public function reporteDocumento($valores)
    {       
        $parametro = json_decode($valores);          

        // if($parametro->opcion == Ima_reporte::opc_placasUtilizadas)
        //     // Ima_reporte::imprime_placasUtilizadas_vertical($parametro);
        //     Ima_reporte::imprime_placasUtilizadas_horizontal($parametro);
        if($parametro->opcion == Med_reporte::opc_reporteSolicitud)
            Med_reporte::imprime_Solicitud($parametro);
        else if($parametro->opcion == Med_reporte::opc_reporteProgramacion) 
            Med_reporte::imprime_Programacion($parametro);
    }
    // ----------------------------------------- FIN REPORTES -----------------------------------------
    // ------------------------------------------- REPORTE TICKET -------------------------------------------
    public function reporteTicket($valores)
    {
        $parametro = json_decode($valores);         
        Ima_reporte::imprime_Ticket($parametro);
    }
    // ----------------------------------------- FIN REPORTES -----------------------------------------
}