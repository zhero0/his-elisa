<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Medicina_trabajo\Hsi_causa;
use \App\Models\Medicina_trabajo\Hsi_area;
use \App\Models\Medicina_trabajo\Hsi_registro_datos;
use \App\Models\Medicina_trabajo\Med_administracion;
use \App\Models\Medicina_trabajo\Hsi_actividad_economica_division;
use \App\Models\Medicina_trabajo\Hsi_tipo_registro;
use \App\Models\Medicina_trabajo\Hsi_tipo;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Afiliacion\Hcl_poblacion;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Hsi_registro_datosController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.hsi_registro_datos.';
    private $controlador = 'hsi_registro_datos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;

         

        $matricula = $request->matricula; 
        $primer_apellido = $request->primer_apellido;
        $segundo_apellido = $request->segundo_apellido;
        $nombres = $request->nombres; 

        $model = DB::table(DB::raw("hsi_registro_datos_lista('".$matricula."','".$nombres."','".$primer_apellido."','".$segundo_apellido."',".$arrayDatos['idalmacen'].")"))->get();   
        
        $view = $this->rutaVista . 'index';        
        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;

        return view($view)
                ->with('model', $model)  
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        
        $model = new Hsi_registro_datos;
        $modelActividad = Hsi_actividad_economica_division::orderBy('id','ASC')->get();
        $modelHsi_tipo_registro = Hsi_tipo_registro::orderBy('id','ASC')->get();
        $modelHsi_tipo = Hsi_tipo::orderBy('id','ASC')->get();
        $modelCausa = Hsi_causa::get();
        $modelArea = Hsi_area::get();
        $modelEmpleador = new Hcl_empleador;
        $modelPoblacion = new Hcl_poblacion;
        if($model != null)
        {
            $txtcontenidoTablaC = Hsi_tipo::where([
                        ['eliminado', '=', 0],
                        // ['idhcl_cabecera_registro', '=', $model->id]
                    ])->get()->toJson();
            $model->documentodetalleC = $txtcontenidoTablaC;

            // $txtcontenidoEstablecimiento = Almacen::where('eliminado', '=', 0)->get()->toJson();
            // $txtcontenidoCuaderno = Hcl_cuaderno::where('eliminado', '=', 0)->get()->toJson();
            
            // $model->documentodetalleEstablecimiento = $txtcontenidoEstablecimiento;
            // $model->documentodetalleCuaderno = $txtcontenidoCuaderno;
        }
        $modelPoblacion->edad = '';

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista; 

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('modelEmpleador', $modelEmpleador) 
                ->with('modelPoblacion', $modelPoblacion) 
                ->with('modelActividad', $modelActividad)
                ->with('modelHsi_tipo_registro', $modelHsi_tipo_registro)
                ->with('modelHsi_tipo', $modelHsi_tipo)
                ->with('modelCausa', $modelCausa)
                ->with('modelArea', $modelArea)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Hsi_tipo;
        $model->idhsi_tipo_registro = $request->idhsi_tipo_registro;        
        $model->codigo = $request->codigo;        
        $model->nombre = $request->nombre;           
        $model->usuario = Auth::user()->name;

        if($model->save()){ 
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $model = Hsi_tipo::find($id);
        $modelRegistro = Hsi_tipo_registro::get();  

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelRegistro', $modelRegistro)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {         
        $model = Hsi_tipo::find($id); 
        $model->idhsi_tipo_registro = $request->idhsi_tipo_registro;        
        $model->codigo = $request->codigo;        
        $model->nombre = $request->nombre;            
        $model->usuario = Auth::user()->name;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaPaciente_hsi()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            // $modelo = Hcl_poblacion::where(function($query) use($dato) {
            //             $query->orwhere('numero_historia', 'ilike', '%'.$dato.'%');
            //             $query->orwhere('nombre', 'ilike', '%'.$dato.'%');
            //             $query->orwhere('primer_apellido', 'ilike', '%'.$dato.'%');
            //             $query->orwhere('segundo_apellido', 'ilike', '%'.$dato.'%');
            //             $query->orwhere(DB::raw("CONCAT(nombre, ' ',primer_apellido, ' ', segundo_apellido)"), 'ilike', '%'.$dato.'%');
            //         })
            //         ->select('hcl_poblacion.*', 
            //                 DB::raw("(SELECT date_part('year', age(fecha_nacimiento)))::integer AS edad"))
            //         ->take(config('app.muestraTotalBusqueda'))
            //         ->get();
            
            $modelo = DB::table(DB::raw("hsi_busca_paciente('".$dato."','".$dato."','".$dato."','".$dato."')"))->take(config('app.muestraTotalBusqueda'))->get();   

            echo json_encode($modelo);
        }
    }
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------

}