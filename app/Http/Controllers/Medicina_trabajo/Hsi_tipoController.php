<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Medicina_trabajo\Hsi_tipo_registro;
use \App\Models\Medicina_trabajo\Hsi_tipo;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Hsi_tipoController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.hsi_tipo.';
    private $controlador = 'hsi_tipo';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $modelRegistro = Hsi_tipo_registro::get();  
        $model = new Hsi_tipo;   
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista; 

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelRegistro', $modelRegistro)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Hsi_tipo;
        $model->idhsi_tipo_registro = $request->idhsi_tipo_registro;        
        $model->codigo = $request->codigo;        
        $model->nombre = $request->nombre;           
        $model->usuario = Auth::user()->name;

        if($model->save()){ 
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $model = Hsi_tipo::find($id);
        $modelRegistro = Hsi_tipo_registro::get();  

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelRegistro', $modelRegistro)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {         
        $model = Hsi_tipo::find($id); 
        $model->idhsi_tipo_registro = $request->idhsi_tipo_registro;        
        $model->codigo = $request->codigo;        
        $model->nombre = $request->nombre;            
        $model->usuario = Auth::user()->name;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function muestraDatosHsi()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $buscarOpcion = $_POST['buscarOpcion'];
            $buscarTodo = $_POST['buscarTodo'];
            $idsDatos = '';
            if(isset($_POST['datos']))
                $idsDatos = $this->retornaIdDatos(json_decode($_POST['datos']));
            
            $modelo = Hsi_tipo::where([
                            ['eliminado', '=', 0],
                            // ['idalmacen', '=', $idalmacen],
                            ['nombre', 'ilike', '%'.$dato.'%'],
                            ['idhsi_tipo_registro', '=', $buscarOpcion]
                        ])
                        ->whereIn($buscarTodo == 0 && $idsDatos != ''? 'id' : 'eliminado', 
                        array($buscarTodo == 0 && $idsDatos != ''? DB::raw("select id from hsi_tipo where id in(".$idsDatos.")") : 0) )
                        ->orderBy('codigo', 'ASC')
                        ->limit(100)
                        ->get()->toJson();
            return $modelo;
        }
    }
    private function retornaIdDatos($datosTabla) {
        $idsDatos = '';
        for($j = 0; $j < count($datosTabla); $j++)
        {
            if($j == 0)
                $idsDatos .= $datosTabla[$j];
            else
                $idsDatos .= ','. $datosTabla[$j];
        }
        return $idsDatos;
    }

}