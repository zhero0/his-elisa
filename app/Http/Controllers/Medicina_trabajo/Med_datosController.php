<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Medicina_trabajo\Med_datos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Med_datosController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.med_datos.';
    private $controlador = 'med_datos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $observacion = '';        
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $observacion = $datosBuscador->observacion;               
        }


        $model = Med_datos::where([ 
                    ['idalmacen', '=', $arrayDatos['idalmacen']],                    
                    // ['observacion', 'ilike', '%'.$observacion.'%'],                  
                ])
                ->orderBy('observacion', 'ASC')
                ->paginate(config('app.pagination'));  

        $model->datosBuscador = $request->datosBuscador;
        $model->almacen = $request->almacen; 

        $model->scenario = 'index';        
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $model = new Med_datos;   
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Med_datos;
        $model->observacion = $request->observacion;        
        $model->cargo_1 = $request->cargo_1;        
        $model->nombre_1 = $request->nombre_1;        
        $model->cargo_2 = $request->cargo_2;        
        $model->nombre_2 = $request->nombre_2;        
        $model->cargo_3 = $request->cargo_3;        
        $model->nombre_3 = $request->nombre_3;        
        $model->cargo_4 = $request->cargo_4;        
        $model->nombre_4 = $request->nombre_4;        
        $model->cargo_5 = $request->cargo_5;        
        $model->nombre_5 = $request->nombre_5;        
        $model->cargo_6 = $request->cargo_6;        
        $model->nombre_6 = $request->nombre_6;         
        $model->idalmacen = $request->idalmacen;        
        $model->usuario = Auth::user()->name;

        if($model->save()){
            Med_datos::where([
                    ['eliminado', '=', 0],
                    ['id', '!=', $model->id]
                ])->update(['eliminado' => 1]);
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();
        
        $model = Med_datos::find($id);

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Med_datos::find($id); 
        $model->observacion = $request->observacion;        
        $model->cargo_1 = $request->cargo_1;        
        $model->nombre_1 = $request->nombre_1;        
        $model->cargo_2 = $request->cargo_2;        
        $model->nombre_2 = $request->nombre_2;        
        $model->cargo_3 = $request->cargo_3;        
        $model->nombre_3 = $request->nombre_3;        
        $model->cargo_4 = $request->cargo_4;        
        $model->nombre_4 = $request->nombre_4;        
        $model->cargo_5 = $request->cargo_5;        
        $model->nombre_5 = $request->nombre_5;        
        $model->cargo_6 = $request->cargo_6;        
        $model->nombre_6 = $request->nombre_6;                
        $model->usuario = Auth::user()->name;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

}