<?php
namespace App\Http\Controllers\Medicina_trabajo;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Medicina_trabajo\Hsi_registro_accidente_trabajo;
use \App\Models\Medicina_trabajo\Med_clasificacion;
use \App\Models\Medicina_trabajo\Med_tipo_cancelacion;
use \App\Models\Medicina_trabajo\Med_tipo_pago;
use \App\Models\Medicina_trabajo\Med_datos;
use \App\Models\Medicina_trabajo\Med_consolidado;
use \App\Models\Medicina_trabajo\Med_lista_cargos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Hsi_registro_accidente_trabajoController extends Controller
{
    private $rutaVista = 'Medicina_trabajo.hsi_registro_accidente_trabajo.';
    private $controlador = 'hsi_registro_accidente_trabajo';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    { 
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;

        $view = $this->rutaVista . 'index'; 

        $folio = ''; 
        $nro_documento = '';
        $paterno = '';
        $materno = '';
        $nombre = '';        
        $formulario = 0;  
        if($request->all()) {
            $params = $request->all();
            $param = json_decode($params['param']);

            if(isset($param->dato)) {
                foreach ($param->dato as $value) {
                    if($value->name !== '') eval("$".$value->name." = '".$value->valor."';");
                }
            }
            if (strpos($param->pathname, $this->controlador) !== false) $view = $this->rutaVista.'indexContent';
        } 
      
        // $model = Med_registro::where([
        //             // ['eliminado', '=', 0],
        //             // //['idalmacen', '=', $arrayDatos['idalmacen']],                    
        //             // ['nro_folio', 'ilike', '%'.$folio.'%'],
        //             // ['hcl_poblacion_primer_apellido', 'ilike', '%'.$paterno.'%'],
        //             // ['hcl_poblacion_segundo_apellido', 'ilike', '%'.$materno.'%'],
        //             // ['hcl_poblacion_nombres', 'ilike', '%'.$nombre.'%'],
        //             // ['hcl_poblacion_documento', 'ilike', '%'.$nro_documento.'%'],                       
        //             // ['idmed_clasificacion', '=', $formulario],                    
        //         ])
        //         ->orderBy('id', 'DESC')
        //         ->paginate(config('app.pagination'));   

        $model = DB::table(DB::raw("med_lista_registros(".$arrayDatos['idalmacen'].")"))->get();   

        // print_r($model);
        // return;

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;

        return view($view)
                ->with('model', $model)  
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $modelClasificacion = Med_clasificacion::where([
                            ['eliminado', '=', 0],
                            // ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();
        $modelCancelacion = Med_tipo_cancelacion::where([
                            // ['eliminado', '=', 0],
                            // ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get(); 
        $modelDeposito = new Med_tipo_pago;

        $model = new Med_registro;   
        $model->costo_tramite = 0;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->iddatosgenericos = $arrayDatos['iddatosgenericos'];

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelClasificacion', $modelClasificacion)
                ->with('modelCancelacion', $modelCancelacion)
                ->with('modelDeposito', $modelDeposito)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {   
        $model = new Med_registro;
        $model->nro_folio = $request->nro_folio != null? $request->nro_folio : 0;
        $model->total_cancelado = floatval($request->costo_formulario) + floatval($request->costo_tramite);
        $model->fecha_registro = $request->fecha_registro;
        $model->idmed_tipo_cancelacion = $request->idmed_tipo_cancelacion;
        $model->idmed_clasificacion = $request->idmed_clasificacion;
        $model->idhcl_poblacion = $request->idhcl_poblacion; 
        $model->idhcl_empleador = $request->idhcl_empleador;  
        $model->iddatosgenericos = $request->iddatosgenericos; 
        $model->idalmacen = $request->idalmacen;        
        $model->usuario = Auth::user()->name; 
        if($model->save()){
            if ($request->idmed_tipo_cancelacion != Med_tipo_cancelacion::EFECTIVO) {            
                $modelCancelacion = new Med_tipo_pago;
                $modelCancelacion->nro_deposito = $request->nro_deposito;
                $modelCancelacion->banco_deposito = $request->banco_deposito;
                $modelCancelacion->fecha_deposito = $request->fecha_deposito; 
                $modelCancelacion->identificacion = $request->identificacion; 
                $modelCancelacion->idmed_registro = $model->id; 
                $modelCancelacion->usuario = Auth::user()->name;
                $modelCancelacion->save();
            }
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();  

        $model = Med_registro::where([
                            ['eliminado', '=', 0],
                            ['id', '=', $id]
                        ])->first();  

        $modelHcl_poblacion = Hcl_poblacion::find($model->idhcl_poblacion);
        $model->paciente = $modelHcl_poblacion->primer_apellido.' '.$modelHcl_poblacion->segundo_apellido.' '.$modelHcl_poblacion->nombre;
        $modelHcl_empleador = Hcl_empleador::find($model->idhcl_empleador);
        $model->hcl_empleador_nombre =  $modelHcl_empleador->nro_empleador.' '.$modelHcl_empleador->nombre;
        $modelClasificacion = Med_clasificacion::where([
                            ['eliminado', '=', 0],
                            // ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get(); 
        $modelCla = Med_clasificacion::find($model->idmed_clasificacion);

        $modelCancelacion = Med_tipo_cancelacion::where([
                            // ['eliminado', '=', 0],
                            // ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();   
        $modelDeposito = Med_tipo_pago::where([
                            ['eliminado', '=', 0],
                            ['idmed_registro', '=', $model->id]
                        ])->first(); 

        $model->costo_formulario = $modelCla->costo_formulario;
        $model->costo_tramite = $modelCla->costo_tramite;
        // $model->fecha_registro = date('d-m-Y', strtotime($model->fecha_registro));
        // print_r($model->fecha_registro);
        // return;

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelCancelacion', $modelCancelacion)
                ->with('modelClasificacion', $modelClasificacion)
                ->with('modelDeposito', $modelDeposito)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $model = Med_registro::find($id);
        $model->nro_folio = $request->nro_folio != null? $request->nro_folio : 0;
        $model->total_cancelado = floatval($request->costo_formulario) + floatval($request->costo_tramite);
        $model->fecha_registro = $request->fecha_registro;
        $model->idmed_tipo_cancelacion = $request->idmed_tipo_cancelacion;
        $model->idmed_clasificacion = $request->idmed_clasificacion;
        $model->idhcl_poblacion = $request->idhcl_poblacion; 
        $model->idhcl_empleador = $request->idhcl_empleador;  
        $model->iddatosgenericos = $request->iddatosgenericos; 
        $model->idalmacen = $request->idalmacen;        
        $model->usuario = Auth::user()->name; 
        if($model->save()){
            if ($request->idmed_tipo_cancelacion != Med_tipo_cancelacion::EFECTIVO) {            
                $modelCancelacion = new Med_tipo_pago;
                $modelCancelacion->nro_deposito = $request->nro_deposito;
                $modelCancelacion->banco_deposito = $request->banco_deposito;
                $modelCancelacion->fecha_deposito = $request->fecha_deposito; 
                $modelCancelacion->identificacion = $request->identificacion; 
                $modelCancelacion->idmed_registro = $model->id; 
                $modelCancelacion->usuario = Auth::user()->name;
                $modelCancelacion->save();
            }
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado'); 
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
 
    public function asigna_Cargo()
    {
        // print_r('expression');
        // return;

        if(isset($_POST['iddatosgenericos']) && isset($_POST['idCargo']))
        {
            $arrayDatos = Home::parametrosSistema();
            $pidmed_administracion = $_POST['idCargo'];
            $piddatosgenericos = $_POST['iddatosgenericos'];            

            $modelDato = DB::table(DB::raw("med_registra_cargo('".$pidmed_administracion."','".$piddatosgenericos."','".$arrayDatos['idalmacen']."','".Auth::user()->name."')"))
                                ->get();  
        }   
    }
    
    public function validarRegistro(Request $request, $id)
    {  
        if(isset($_POST['nro_folio']))
        {
            $nro_folio_recibido = $_POST['nro_folio'];
            $arrayDatos = Home::parametrosSistema();  
            $modelRegistro = Med_registro::find($id);
            $modelRegistro->nro_folio = $nro_folio_recibido;        
            $modelRegistro->validar = true;        
            $modelRegistro->usuario = Auth::user()->name; 
            $modelRegistro->save();   

            $modelAdministradores = Med_lista_cargos::where([
                                ['eliminado', '=', 0],
                                ['idalmacen', '=', $arrayDatos['idalmacen']]
                            ])->get();  
            if (count($modelAdministradores)>0) 
            {                
                foreach ($modelAdministradores as $dato) {
                    $model = new Med_consolidado;
                    $model->idmed_registro = $modelRegistro->id;        
                    $model->idmed_lista_cargos = $dato->id;    
                    $model->save();
                }
            } 
        }    
    }

    public function eliminarRegistro(Request $request, $id)
    {     
        $modelRegistro = Med_registro::find($id);        
        $modelRegistro->eliminado = 1;        
        $modelRegistro->usuario = Auth::user()->name; 
        $modelRegistro->save();    
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
}