<?php
namespace App\Http\Controllers\Fichaje;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Fichaje\Fic_fichas;
use \App\Models\Laboratorio\Lab_especialidad;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Fic_fichas_programadasController extends Controller
{
    private $rutaVista = 'fichaje.fic_fichas_programadas.';
    private $controlador = 'fic_fichas_programadas';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        // print_r($arrayDatos['modelUser_menuopcion']);
        // return;

        $dato = $request->search;
        $model = Lab_especialidad::where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']]
                ])
                ->where(function($query) use($dato) {
                    $query->orwhere('codigo', 'like', '%'.$dato.'%');
                    $query->orwhere('nombre', 'like', '%'.$dato.'%'); 
                    $query->orwhere('descripcion', 'like', '%'.$dato.'%');
                    $query->orwhere('usuario', 'like', '%'.$dato.'%');
                })
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
        // $model->modelMenu = $arrayDatos['modelMenu'];
        // $model->modelMenuopcion = $arrayDatos['modelMenuopcion'];
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Lab_especialidad;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Lab_especialidad;
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        $model->idalmacen = $request->idalmacen;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("especialidad"); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_especialidad::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_especialidad::find($id);
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Lab_especialidad::find($id); 
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("especialidad");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
 
    public function busca_Programaciones()
    {
        $arrayDatos = Home::parametrosSistema();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = DB::table('fic_fichas_programadas as fi')                        
                        ->select('fi.*')
                        ->where([['fi.eliminado', '=', 0],
                            ['fi.idalmacen', '=', $arrayDatos['idalmacen']]
                        ]) 
                        ->orderBy('fi.idhcl_cuaderno', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }

    public function buscaFicha_Programada()
    {
        if(isset($_POST['dato']))
        {
            $id = $_POST['dato'];
            $modelo = Fic_fichas_programadas::where('id', '=', $id)->get()->toJson();
            echo $modelo;
        }
    }

}
