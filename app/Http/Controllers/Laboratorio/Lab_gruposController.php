<?php
namespace App\Http\Controllers\Laboratorio;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;

use \App\Models\Laboratorio\Lab_grupos;
use \App\Models\Laboratorio\Lab_examenes;
use \App\Models\Laboratorio\lab_perfil_examenes;

use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

use Session;
use Illuminate\Support\Facades\URL;

class Lab_gruposController extends Controller
{
    private $rutaVista = 'laboratorio.lab_grupos.';
    private $controlador = 'lab_grupos';
    private $nombre_session = 'urlAnterior_lab_grupos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $model = DB::table('lab_grupos as g')
                ->select('g.id', 'g.nombre', 'g.descripcion', 'g.usuario', 
                    DB::raw('(select count(*) from lab_perfil_examenes where idlab_grupos = g.id) as totalexamenes') )
                ->where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']]
                ])
                ->where(function($query) use($dato) { 
                    $query->orwhere('nombre', 'like', '%'.$dato.'%');  
                    $query->orwhere('usuario', 'like', '%'.$dato.'%');
                })
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Lab_grupos;
        $model->idalmacen = $arrayDatos['idalmacen'];

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        if(!Session::has($this->nombre_session))
            Session::put($this->nombre_session, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Lab_grupos;

        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);
        $model->idalmacen = $request->idalmacen;
        $model->usuario = Auth::user()['name'];

        if($model->save())
        {
            Lab_examenes::registraExamenes($request, $model->id);
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has($this->nombre_session))
        {
            $valor_sesion = Session::get($this->nombre_session);
            Session::forget([$this->nombre_session]);
            return redirect()->to($valor_sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("grupo_lab");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_grupos::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_grupos::find($id);

        $model->lab_examenesEspecialidad = DB::table('lab_perfil_examenes as pe')
                    ->join('lab_examenes as le', 'pe.idlab_examenes', '=', 'le.id')
                    ->join('lab_especialidad as e', 'le.idlab_especialidad', '=', 'e.id')
                    ->select(
                        'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                        'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                    )
                    ->where([
                        ['le.eliminado', '=', 0],
                        ['pe.idlab_grupos', '=', $id]
                    ])
                    ->orderBy('le.nombre', 'ASC')
                    ->get()->toJson();

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Lab_grupos::find($id);

        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);

        if($model->save())
        {
            lab_perfil_examenes::where('idlab_grupos', '=', $id)->delete();
            Lab_examenes::registraExamenes($request, $model->id);
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("grupo_lab");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function muestraLab_grupos()
    {
        if(isset($_POST['idalmacen']))
        {
            $idalmacen = $_POST['idalmacen'];
            $modelo = Lab_grupos::muestraGrupos($idalmacen);
            
            // $arrayValor = array(
            //     'modelo' => $modelo,
            //     ''
            // );
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}