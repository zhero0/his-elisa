<?php
namespace App\Http\Controllers\Laboratorio;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Almacen\Generico;
use \App\Models\Almacen\Almacen;
use \App\Models\Almacen\Home;

use \App\Models\Laboratorio\Lab_variables;
use \App\Models\Laboratorio\Lab_examenes;
use \App\Models\Laboratorio\Lab_especialidad;
use \App\Models\Laboratorio\Lab_lista;
use \App\Models\Laboratorio\Lab_tipo_muestra;
use \App\Models\Laboratorio\Lab_tipo_variables;
use \App\Models\Laboratorio\Lab_registroprogramacion;
use \App\Models\Laboratorio\Lab_estado;
use \App\Models\Laboratorio\Lab_examenes_registroprogramacion;
use \App\Models\Laboratorio\Lab_resultados_programacion;
use \App\Models\Laboratorio\Lab_programacion_view;
use \App\Models\Laboratorio\Lab_resultados_examen;
use \App\Models\Laboratorio\Lab_resultados_variables;
use \App\Models\Laboratorio\Lab_resultados_lista;
use \App\Models\Laboratorio\Lab_cuaderno_datosgenericos;
use \App\Models\Laboratorio\Lab_resultados_documento;
use \App\Models\Laboratorio\Lab_formularespuesta;
use \App\Models\Laboratorio\Lab_formula;

use \App\Models\Administracion\Datosgenericos;

use Session;
use Illuminate\Support\Facades\URL;

class Lab_resultados_programacionController extends Controller
{
    private $rutaVista = 'laboratorio.lab_resultados_programacion.';
    private $controlador = 'lab_resultados_programacion';
    private $idalmacen;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $datoUrl = $request->search;
        $dato = $request->searchNuevo;
        $idlab_especialidad = $request->especialidad;
        $buscarFecha = $request->buscarFecha;
        $date = date_create($request->fechaRegistro);
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado;
        $cantidad = 0;

        $modelEspecialidad = DB::table('lab_especialidad as p')
                    ->join('lab_cuaderno_datosgenericos as e', 'p.id', '=', 'e.idlab_especialidad')                    
                    ->select('p.id', 'p.nombre')
                    ->where([
                        ['p.eliminado', '=', 0],                        
                        ['p.idalmacen', '=', $arrayDatos['idalmacen']],
                        ['e.iddatosgenericos','=',$arrayDatos['iddatosgenericos']]])      
                    ->orderBy('p.nombre', 'ASC')
                    ->get(); 
        
        $model = DB::table('lab_resultado_view as r')
                    ->where([                                                
                        ['r.idalmacen', '=', $arrayDatos['idalmacen']],
                        // ['esp','=',$idlab_especialidad == 0? 0 : $idlab_especialidad ],
                    ])
                    /*->where(function($query) use($fechaRegistro, $buscarFecha) {
                        $query->where([
                            ['fechaprogramacion', $buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro]
                        ]);
                    })
                    */
                    ->where(function($query) use($datoUrl) {
                        $query->orwhere('r.hcl_poblacion_nombrecompleto', 'like', '%'.$datoUrl.'%');
                        $query->orwhere('r.hcl_poblacion_matricula', 'like', '%'.$datoUrl.'%');
                    })
                    ->orderBy('r.numero', 'DESC')
                    ->paginate(config('app.pagination'));

        $modelo = DB::table('lab_examenes_registroprogramacion as p')
                    ->join('lab_examenes as e', 'p.idlab_examenes', '=', 'e.id')
                    ->join('lab_registroprogramacion as a', 'p.idlab_registroprogramacion', '=', 'a.id')
                    ->join('lab_especialidad as i', 'e.idlab_especialidad', '=', 'i.id')
                    ->select('i.codigo', 'i.nombre', DB::raw('count(a.id) as total'))                         
                    ->where([
                        ['a.eliminado', '=', 0],
                        ['p.eliminado', '=', 0],
                        ['a.idalmacen', '=', $arrayDatos['idalmacen']]
                        ]) 
                    ->where(function($query) use($fechaRegistro, $buscarFecha) {
                    $query->where([
                        ['a.fechaprogramacion', $buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro]]);
                    })
                    ->groupBy('i.codigo', 'i.nombre')
                    ->orderBy('i.nombre', 'ASC')
                    ->get();
        
        $model->scenario = 'index';
        $model->fechaRegistro = $fechaRegistro;
        $model->searchNuevo = $dato;
        $model->idlab_especialidad = $idlab_especialidad;
        $model->buscarFecha = $buscarFecha;
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
        $model->hora_actual = date('h:i A');
        $model->fechaActual = date('d-m-Y');
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelo', $modelo)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_resultados_programacion::find($id);
        $modelIma_registroprogramacion = Lab_registroprogramacion::find($model->idlab_registroprogramacion);
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        $date2 = date('Y-m-d');
        $diff = abs(strtotime($date2) - strtotime($model->hcl_poblacion_fechanacimiento));
        $edad = floor($diff / (365*60*60*24));
        if($model->hcl_poblacion_sexo == Hcl_poblacion::SEXO_MASCULINO)
            $idsexo = Hcl_poblacion::ID_SEXO_MASCULINO;
        else
            $idsexo = Hcl_poblacion::ID_SEXO_FEMENINO;
        
        // ======================================= [ DATOS GUARDADOS ] =======================================
        $examenesProgramados = DB::table('lab_resultados_examen as re')
                                ->select('re.idlab_examenes', 
                                         're.codigo_examen as codigo', 're.nombre_examen as nombre', 're.respuesta')
                                ->where([
                                    ['re.eliminado', '=', 0],
                                    ['re.is_resultado', '=', 0],
                                    ['re.idlab_resultados_programacion', '=', $model->id]
                                ])->orderBy('re.nombre_examen', 'ASC')->get();
        $x = 1;
        if(count($examenesProgramados) > 0)
        // if($x == 0)
        {
            $arrayPerfil = DB::select(
                DB::raw("select t.idlab_grupos as idgrupo, t.grupo
                        from lab_resultados_examen t
                        where t.eliminado = 0 and t.idlab_resultados_programacion = ".$id."
                        group by t.idlab_grupos, t.grupo
                        order by t.idlab_grupos asc ") );
            $arrayExamen = DB::select(
                DB::raw("
                select  t.idlab_grupos as idgrupo, t.idlab_examenes as idexamen, t.codigo_examen as codigo, 
                        t.nombre_examen as examen, t.respuesta, t.orden as orden, 
                        case when t.is_resultado is true then t.idlab_examenes else null end as idlab_examenes_resultado
                from lab_resultados_examen t
                where t.eliminado = 0 and t.idlab_resultados_programacion = ".$id."
                group by t.idlab_grupos, t.idlab_examenes, t.codigo_examen, t.nombre_examen, t.respuesta, t.orden, t.is_resultado
                order by t.orden asc ") );
            $arrayVariable = DB::select(
                DB::raw("
                select z.idlab_examenes, case when z.is_resultado is true then z.idlab_examenes else null end as idlab_examenes_resultado,
                    z.idlab_grupos as idgrupo,
                    z.grupo,
                     z.idlab_examenes as idexamen, z.codigo_examen as codigo, z.nombre_examen as examen, z.orden as orden,
                     ev.idlab_variables, ev.nombre as variable, ev.valor_inf,
                     ev.valor_sup, ev.unidad, ev.metodo, ev.edad_inicial, ev.edad_final, ev.sexo, ev.tipodato
                from lab_resultados_examen z left join lab_resultados_variables ev on z.id = ev.idlab_resultados_examen
                where z.eliminado = 0 and idlab_resultados_programacion = ".$id."
                order by z.grupo asc, z.orden asc;") );
            
            $arrayListaVariable = array();
        }
        else
        {
            // formula_examenes_programados_perfil
            // formula_examenes_programados_examen
            // formula_examenes_programados
            $arrayPerfil = DB::select(
                DB::raw("select t.idgrupo, t.grupo
                         from mostrar_examenes_variables(".$model->idlab_registroprogramacion.", ".$edad.", ".$idsexo.") t
                         group by t.idgrupo, t.grupo
                         order by t.grupo asc ") );
            $arrayExamen = DB::select(
                DB::raw("select t.idgrupo, t.idexamen, t.codigo, t.examen, t.respuesta, t.orden, t.idlab_examenes_resultado
                         from mostrar_examenes_variables(".$model->idlab_registroprogramacion.", ".$edad.", ".$idsexo.") t
                         group by t.idgrupo, t.idexamen, t.codigo, t.examen, t.respuesta, t.orden,t.idlab_examenes_resultado
                         order by t.orden asc ") );
            $arrayVariable = DB::select(
                DB::raw("select * 
                         from mostrar_examenes_variables(".$model->idlab_registroprogramacion.", ".$edad.", ".$idsexo.") 
                         ") );
            // print_r($arrayPerfil);
            // print_r($arrayExamen[0]);
            // return;
            
            /*
            opcion_simple   => 2
            opcion_multiple => 3
            En esta sección extraigo.. la lista de las variables de opciones "simples" ó "múltiples"
            */
            $idsLab_variables = '';
            for($i = 0; $i < count($arrayVariable); $i++)
            {
                if($arrayVariable[$i]->tipodato == Lab_tipo_variables::opcion_simple || $arrayVariable[$i]->tipodato == Lab_tipo_variables::opcion_multiple)
                {
                    $idsLab_variables .= ','.$arrayVariable[$i]->idlab_variables;
                }
            }

            $idsLab_variables = substr($idsLab_variables, 1, strlen($idsLab_variables));
            if($idsLab_variables == '')
                $arrayListaVariable = array();
            else
                $arrayListaVariable = DB::table('lab_lista as li')
                                    ->where([
                                        ['li.eliminado', '=', 0],
                                        ['li.lis_variable', '!=', "''"]
                                    ])
                                    ->whereIn('li.idlab_variables', array(DB::raw($idsLab_variables)) )
                                    ->orderBy('li.idlab_variables', 'ASC')
                                    ->orderBy('li.lis_variable', 'ASC')
                                    ->get();
        }

        $datosPaciente = array(
            'id' => $model->idhcl_poblacion,
            'hcl_poblacion_nombrecompleto' => $model->hcl_poblacion_nombrecompleto,
            'hcl_poblacion_matricula' => $model->hcl_poblacion_matricula,
            'hcl_poblacion_cod' => $model->hcl_poblacion_cod,
            'hcl_poblacion_sexo' => $model->hcl_poblacion_sexo,
            'edad' => $edad,
            'hcl_empleador_empresa' => $model->hcl_empleador_empresa,
            'hcl_empleador_patronal' => $model->hcl_empleador_patronal
        );
        $arrayValores = array(
            'arrayPerfil' => json_encode($arrayPerfil),
            'arrayExamen' => json_encode($arrayExamen),
            'arrayVariable' => json_encode($arrayVariable),
            'arrayListaVariable' => json_encode($arrayListaVariable),
            'datosPaciente' => json_encode($datosPaciente),
        );
        $model->JsonValores = json_encode($arrayValores);

        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->iddatosgenericos = $arrayDatos['iddatosgenericos'];
        
        // DATOS PROGRAMACIÓN
        $model->hospitalizado = $modelIma_registroprogramacion->hospitalizado;
        $modelHcl_especialidad = Hcl_especialidad::find($modelIma_registroprogramacion->idhcl_especialidad);
        $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
        $modelDatosgenericos = Datosgenericos::find($modelIma_registroprogramacion->iddatosgenericos);
        $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
        $modelHcl_cuaderno = Hcl_cuaderno::find($modelIma_registroprogramacion->idhcl_cuaderno);
        $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';
        $date = date_create($modelIma_registroprogramacion->fechaprogramacion);
        $model->fechaprogramacion = date_format($date, 'd-m-Y');
        $model->horaprogramacion = $modelIma_registroprogramacion->horaprogramacion;
        $model->diagnostico = $modelIma_registroprogramacion->diagnostico;
        $model->observacion = $modelIma_registroprogramacion->observacion;
        
        $modelLab_resultados_documento = Lab_resultados_documento::where([
                                            ['eliminado', '=', 0],
                                            ['idlab_resultados_programacion', '=', $id]
                                        ])->get();

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelLab_resultados_documento', $modelLab_resultados_documento)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $model = Lab_resultados_programacion::find($id);
        
        $model->respuesta = $request->taRespuesta;
        $model->idalmacen = $request->idalmacen;
        $model->iddatosgenericos = $request->iddatosgenericos;
        $model->fecha_resultado = date('Y-m-d');
        $model->hora_resultado = date('h:i A');
        $model->idlab_estado = Lab_estado::ENTREGADO;

        if($model->save())
        {
            Lab_resultados_examen::where([
                ['eliminado', '=', 0],
                ['idlab_resultados_programacion', '=', $id]
            ])->update(['eliminado' => true]);

            $jsonDatosForm = json_decode($request->txtJsonDatosGuardar);
            for($g = 0; $g < count($jsonDatosForm); $g++)
            {
                $lab_grupos = json_decode($jsonDatosForm[$g]->lab_grupos);
                
                // [ EXAMENES ]
                $resultado_examen = $jsonDatosForm[$g]->resultado_examen;
                for($e = 0; $e < count($resultado_examen); $e++)
                {
                    $lab_examen = json_decode($resultado_examen[$e]->lab_examen);

                    $modelo = new Lab_resultados_examen;
                    $modelo->idlab_resultados_programacion = $model->id;
                    // idlab_formulaparametro
                    // lab_parametro_variable
                    $modelo->idlab_examenes = $lab_examen->idlab_examenes;
                    $modelo->orden = $lab_examen->orden;
                    $modelo->codigo_examen = $lab_examen->codigo;
                    $modelo->nombre_examen = $lab_examen->examen;
                    $modelo->respuesta = str_replace(',', '', $resultado_examen[$e]->valorResultadoExamen);
                    $modelo->is_resultado = $lab_examen->idlab_examenes_resultado > 0? true : false;
                    $modelo->idlab_grupos = $lab_grupos->idgrupo;
                    $modelo->grupo = $lab_grupos->grupo;
                    $modelo->usuario = Auth::user()['name'];
                    $idlab_resultados_examen = 0;
                    
                    if($modelo->save())
                        $idlab_resultados_examen = $modelo->id;
                    
                    // [ VARIABLES ]
                    $resultado_variable = $resultado_examen[$e]->resultado_variable;
                    for($v = 0; $v < count($resultado_variable); $v++)
                    {
                        $variable = json_decode($resultado_variable[$v]->variable);

                        $modelo = new Lab_resultados_variables;
                        $modelo->idlab_resultados_examen = $idlab_resultados_examen;
                        $modelo->idlab_variables = $variable->idlab_variables;
                        $modelo->nombre = $variable->variable;
                        $modelo->valor_sup = $variable->valor_sup;
                        $modelo->valor_inf = $variable->valor_inf;
                        $modelo->unidad = $variable->unidad;
                        $modelo->metodo = $variable->metodo;
                        $modelo->sexo = $variable->sexo;
                        $modelo->edad_inicial = $variable->edad_inicial;
                        $modelo->edad_final = $variable->edad_final;
                        $idlab_resultados_variables = 0;

                        if($modelo->save())
                            $idlab_resultados_variables = $modelo->id;

                        // [ LISTA ]
                        $lista = $resultado_variable[$v]->resultado_lista;
                        for($li = 0; $li < count($lista); $li++)
                        {
                            $modelo = new Lab_resultados_lista;
                            $modelo->idlab_resultados_variables = $idlab_resultados_variables;
                            $modelo->lis_variable = $lista[$li]->lis_variable;
                            $modelo->respuesta = $lista[$li]->respuesta;
                            $modelo->save();
                        }
                    }
                }
            }

            

            // Actualiza el estado de PROGRAMACION siempre y cuando todos...los resultados se hayan ENTREGADO por COMPLETO...
            $modelo = lab_resultados_programacion::where([
                            ['idlab_registroprogramacion', '=', $model->idlab_registroprogramacion],
                            ['idlab_estado', '=', Lab_estado::PROGRAMACION]
                        ])->get();
            if(count($modelo) == 0)
            {
                Lab_registroprogramacion::where([
                    ['id', '=', $model->idlab_registroprogramacion]
                ])->update(['idlab_estado' => Lab_estado::ENTREGADO]);
            }


            // Copia los archivos al sistema y almacena el detalle en una tabla de la bd
            $files = $request->file('archivo');
            if($request->hasFile('archivo'))
            {
                $arrayParametro = array(
                    'files' => $files,
                    'id' => $id,
                );
                Lab_resultados_documento::registraArchivos($arrayParametro);
            }
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("resultado_lab");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    /*
        Registra muestreo de laboratorio desde la ventana MODAL del index
    */
    public function registraMuestraLab(Request $request)
    {
        $id = $request->idtabla;
        $datoExtra = json_decode($request->txtDatosExtras);
        
        $fecha = date('Y-m-d', strtotime($request->txtFecha_muestra));
        $modelo = Lab_resultados_programacion::where('id', '=', $id)->first();
        $modelo->numero_muestra = $request->txtNumero_muestra;
        $modelo->hora_muestra = $datoExtra->hora_muestra;
        $modelo->fecha_muestra = $fecha;
        $modelo->estado_muestra = $request->txtEstado_muestra;
        $modelo->descripcion_muestra = $request->taDescripcion_muestra;
        $modelo->numero_sala = $request->txtNumero_sala;
        $modelo->numero_cama = $request->txtNumero_cama;
        
        if($modelo->save())
            return back()->withInput();
        else
        {
            echo "Ha ocurrido un error al guardar el registro! ";
            return;
        }
    }
    
    
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADOR] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function resultadoFormulaExamenes() {
        if(isset($_POST['dato']))
        {
            $dato = json_decode($_POST['dato']);
            $examen_resultado = array_column($dato, 'idlab_examenes_resultado', 'idlab_examenes_resultado');
            $arrayExamen_Resultado = array();

            foreach ($examen_resultado as $key => $value)
            {
                if($value > 0)
                {
                    $arrayFormula_Examenes = array();

                    $formulaRespuesta = Lab_formularespuesta::where([
                            ['eliminado', '=', 0],
                            ['idlab_examenes', '=', $value]
                        ])->first();
                    if($formulaRespuesta != null)
                    {
                        $formulaRespuesta = Lab_formularespuesta::where([
                                ['eliminado', '=', 0],
                                ['idlab_variables', '=', $formulaRespuesta->idlab_variables]
                            ])->orderBy('id', 'ASC')->get();

                        $formula = '';
                        $class_formula = '';
                        for($i = 0; $i < count($formulaRespuesta); $i++)
                        {
                            if($i == 0)
                            {
                                $idlab_formula = DB::table('lab_formulaparametro')
                                            ->where('id', '=', $formulaRespuesta[$i]->idlab_formulaparametro)->first()->idlab_formula;
                                
                                $modelLab_formula = DB::table('lab_formula')->where('id', '=', $idlab_formula)->first();
                                $formula = $modelLab_formula->formula;
                                $class_formula = $modelLab_formula->class_formula;
                            }

                            for($j = 0; $j < count($dato); $j++) // $dato => viene desde el formulario todos...los examenes
                            {
                                if($dato[$j]->idlab_examenes == $formulaRespuesta[$i]->idlab_examenes)
                                {
                                    if($dato[$j]->idlab_examenes == $value)
                                        $variable = $formula;
                                    else
                                        $variable = $formulaRespuesta[$i]->lab_parametro_variable;
                                    
                                    $datosArray = array(
                                        'idlab_examenes' => $dato[$j]->idlab_examenes,
                                        'nombreExamen' => $dato[$j]->nombreExamen,
                                        'lab_parametro_variable' => $variable,
                                        'valor' => $dato[$j]->respuesta,
                                        'idlab_examenes_resultado' => $value,
                                        'class_formula' => $class_formula,
                                    );
                                    array_push($arrayFormula_Examenes, $datosArray);
                                }
                            }
                        }
                        $arrayValores = array(
                                    'idlab_examenes_resultado' => $value,
                                    'class_formula' => $class_formula,
                                    'arrayFormula_Examenes' => json_encode($arrayFormula_Examenes)
                                );
                        array_push($arrayExamen_Resultado, $arrayValores);
                    }
                }
            }
            $resultado = Lab_formula::escogeFuncionFormula($arrayExamen_Resultado);
            echo $resultado;
        }
    }
    public function obtenerNroFolio() {
        if(isset($_POST['idlab_resultados_programacion']))
        {
            $idlab_resultados_programacion = $_POST['idlab_resultados_programacion'];

            $model = Lab_resultados_programacion::where('id', '=', $idlab_resultados_programacion)->first();
            $numero = Lab_resultados_programacion::where([
                            ['eliminado', '=', 0],
                            ['idlab_especialidad', '=', $model->idlab_especialidad],
                            ['idlab_estado', '!=', Lab_estado::ANULADO]
                        ])
                        ->orderBy('id', 'DESC')->max('numero_muestra');
            $arrayValor = array(
                'numeroFolioAnterior' => $numero > 0? $numero : 0,
                'numeroFolioPosterior' => $numero+1
            );
            echo json_encode($arrayValor);
        }
    }
    public function agregaExamenGrupo() {
        if(isset($_POST['idsLabExamenArray']))
        {
            $idsLabExamenArray = $_POST['idsLabExamenArray'];
            // print_r($idsLabExamenArray);
            $idsLab = '';
            for($j = 0; $j < count($idsLabExamenArray); $j++)
            {
                if($j == 0)
                    $idsLab .= $idsLabExamenArray[$j];
                else
                    $idsLab .= ','. $idsLabExamenArray[$j];
            }
            // print_r($idsLab);
// ( [idgrupo] => 8 [idexamen] => 39 [codigo] => H02 [examen] => HEMATOCRITO [respuesta] => 0 [orden] => 1 [idlab_examenes_resultado] => )
            
            
            // $model = Lab_resultados_programacion::where('id', '=', $idlab_resultados_programacion)->first();
            // $numero = Lab_resultados_programacion::where([
            //                 ['eliminado', '=', 0],
            //                 ['idlab_especialidad', '=', $model->idlab_especialidad],
            //                 ['idlab_estado', '!=', Lab_estado::ANULADO]
            //             ])
            //             ->orderBy('id', 'DESC')->max('numero_muestra');
            // $arrayValor = array(
            //     'numeroFolioAnterior' => $numero > 0? $numero : 0,
            //     'numeroFolioPosterior' => $numero+1
            // );
            // echo json_encode($arrayValor);
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeProgramacionResultado($id)
    {
        Lab_resultados_programacion::imprimeProgramacionResultado($id);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}