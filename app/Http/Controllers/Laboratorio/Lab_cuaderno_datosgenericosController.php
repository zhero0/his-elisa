<?php
namespace App\Http\Controllers\Laboratorio;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Laboratorio\Lab_cuaderno_datosgenericos;
use \App\Models\Laboratorio\Lab_especialidad;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Lab_cuaderno_datosgenericosController extends Controller
{
    private $rutaVista = 'laboratorio.lab_personal.';
    private $controlador = 'lab_cuaderno_datosgenericos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $model = Lab_cuaderno_datosgenericos::join('datosgenericos', 'datosgenericos.id', '=', 'lab_cuaderno_datosgenericos.iddatosgenericos')
                ->join('lab_especialidad', 'lab_cuaderno_datosgenericos.idlab_especialidad', '=', 'lab_especialidad.id') 
                ->select('lab_cuaderno_datosgenericos.*', 
                            'datosgenericos.nombres', 'datosgenericos.apellidos','lab_especialidad.nombre')
                ->where([
                    ['datosgenericos.eliminado', '=', 0]
                ])
                ->where(function($query) use($dato) { 
                    $query->orwhere('datosgenericos.nombres', 'like', '%'.$dato.'%'); 
                    $query->orwhere('datosgenericos.apellidos', 'like', '%'.$dato.'%');
                })
                ->orderBy('datosgenericos.nombres', 'ASC')
                ->paginate(config('app.pagination'));
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Lab_cuaderno_datosgenericos;
        $modelHcl_cuaderno = ['' => 'Seleccione! '] + 
                                    Lab_especialidad::where([
                                        ['eliminado', '=', 0],
                                        ['idalmacen', '=', $arrayDatos['idalmacen']]
                                    ])                                    
                                    ->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $modelDatosgenericos = Datosgenericos::where('eliminado', '=', 0)                       
                            ->orderBy('nombres','ASC')->get();
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelHcl_cuaderno', $modelHcl_cuaderno)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        // print_r($request->iddatosgenericos);
        // return;
        $rules = [            
            'iddatosgenericos' => 'required',  
            'idhcl_cuaderno' => 'required',  
        ];
        $messages = [
            'iddatosgenericos.required' => 'Se requiere registrar Personal',  
            'idhcl_cuaderno.required' => 'Se requiere registrar Area',  
        ];
        $this->validate($request, $rules, $messages);

        $model = new Lab_cuaderno_datosgenericos;
        $model->idlab_especialidad = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos; 
        $model->idalmacen = $arrayDatos['idalmacen']; 
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("personal_lab");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // print_r($arrayDatos['idalmacen']);
        // return; 

        $model = Lab_cuaderno_datosgenericos::find($codigo);  

        if ($model!= null) {
             if($model->delete())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        }

        return redirect("personal_lab")
                ->with('message', 'update')
                ->with('mensaje', $mensaje); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_cuaderno_datosgenericos::find($id);
        $modelDatosgenericos = Datosgenericos::where('eliminado', '=', 0)              
                            ->orderBy('nombres','ASC')->get();
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Ima_cuaderno_datosgenericos::find($id); 
        $model->iddatosgenericos = $request->dato; 
        $model->idalmacen = $request->idalmacen; 
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("personal_ima");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
