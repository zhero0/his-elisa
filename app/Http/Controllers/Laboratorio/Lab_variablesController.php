<?php
namespace App\Http\Controllers\Laboratorio;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;

use \App\Models\Laboratorio\Lab_variables;  
use \App\Models\Laboratorio\Lab_lista;
use \App\Models\Laboratorio\Lab_tipo_muestra;
use \App\Models\Laboratorio\Lab_tipo_variables;
use \App\Models\Laboratorio\Lab_formularespuesta;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Lab_variablesController extends Controller
{
    private $rutaVista = 'laboratorio.variable.';
    private $controlador = 'lab_variables';
    private $idalmacen;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 
    
    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $dato = $request->search;
        $modelLab_tipo_variables = Lab_tipo_variables::orderBy('nombre', 'ASC')->get();
        

        $sigla = '';
        $nombre = '';
        $valorInferior = '';
        $valorSuperior = '';
        $unidad = '';
        $tipo_variables = null;
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $sigla = $datosBuscador->sigla;
            $nombre = $datosBuscador->nombre;
            $valorInferior = $datosBuscador->valorInferior;
            $valorSuperior = $datosBuscador->valorSuperior;
            $unidad = $datosBuscador->unidad;
            $tipo_variables = $datosBuscador->tipo_variables > 0? $datosBuscador->tipo_variables : null;
        }
        $model = DB::table('lab_variables as v')
                ->join('lab_tipo_variables as tv', 'tv.id', '=', 'v.idlab_tipo_variables')
                ->select('v.*', 'tv.nombre as tipo_variables')
                ->where([
                    ['v.eliminado', '=', 0],
                    ['v.idalmacen', '=', $arrayDatos['idalmacen']],
                    ['v.sigla', 'ilike', '%'.$sigla.'%'],
                    ['v.nombre', 'ilike', '%'.$nombre.'%'],
                    [DB::raw('v.valor_inf::text'), 'ilike', '%'.$valorInferior.'%'],
                    [DB::raw('v.valor_sup::text'), 'ilike', '%'.$valorSuperior.'%'],
                    ['v.unidad', 'ilike', '%'.$unidad.'%'],
                    ['v.idlab_tipo_variables', $tipo_variables > 0? '=' : '!=', $tipo_variables],
                ])
                ->orderBy('v.nombre', 'ASC')
                ->paginate(config('app.pagination'));
        $model->datosBuscador = $request->datosBuscador;
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelLab_tipo_variables', $modelLab_tipo_variables)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $modelTipoVariables = Lab_tipo_variables::orderBy('nombre', 'ASC')->get();
        $model = new Lab_variables;
        
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        $model->lab_formula = DB::table('lab_formula')->orderBy('formula', 'ASC')->get()->toJson();
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('modelTipoVariables', $modelTipoVariables)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $idalmacen = $request->idalmacen;
        $model = new Lab_variables;

        $numero = Lab_variables::generarNumero($idalmacen);
        $variable = explode('-', $request->variable);
        
        $model->numero = $numero;
        $model->nombre = strtoupper($request->nombre);
        $model->valor_sup = $request->valor_sup;
        $model->valor_inf = $request->valor_inf;
        $model->unidad = $request->unidad;
        $model->metodo = strtoupper($request->metodo);
        $model->comentarios = strtoupper($request->comentarios);
        $model->idlab_tipo_variables = $variable[0];
        $model->mascara = strtoupper($request->mascara);
        $model->sigla = strtoupper($request->sigla);
        $model->idcategoria = $request->idcategoria;
        $model->sexo = $request->sexo;
        $model->edad_inicial = $request->edad_inicial;
        $model->edad_final = $request->edad_final;
        $model->idalmacen = $idalmacen;
        $model->usuario = Auth::user()['name'];

        $jsonDatos = json_decode($request->txtHiddenJSONdatos);
        $jsonLab_formularespuesta = json_decode($jsonDatos->jsonLab_formularespuesta);
        $jsonListaTipoVariable = json_decode($jsonDatos->jsonListaTipoVariable);

        if($model->save())
        {
            // ------------------- REGISTRA TIPO DE VARIABLE -------------------
            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                // 'listaTipoVariable' => json_decode($request->txtListaTipoVariable)
                'listaTipoVariable' => $jsonListaTipoVariable,
                'jsonLab_formularespuesta' => $jsonLab_formularespuesta,
            );
            Lab_lista::registraDetalle($arrayDatos);
            Lab_formularespuesta::registraFormulaRespuesta($arrayDatos);

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("variable");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $modelTipoVariables = Lab_tipo_variables::all();
        $model = Lab_variables::find($codigo);

        $modelLab_lista = Lab_lista::where('idlab_variables', '=', $model->id)->get();
        foreach ($modelLab_lista as $value) {
            print_r($value['lis_variable']);    
        }
        
        return;
        $model->listavariable =  json_encode($modelLab_lista);
        
       
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelTipoVariables', $modelTipoVariables) 
                ->with('arrayDatos', $arrayDatos);  
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $modelTipoVariables = Lab_tipo_variables::orderBy('nombre', 'ASC')->get();
        $model = Lab_variables::find($id);

        // [1]
        $modelLab_lista = Lab_lista::where([
                                ['eliminado', '=', 0],
                                ['idlab_variables', '=', $model->id]
                            ])->get();
        $model->listavariable =  json_encode($modelLab_lista);

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        $lab_formula = DB::table('lab_formularespuesta as fr')
                            ->join('lab_formulaparametro as fp', 'fp.id', '=', 'fr.idlab_formulaparametro')
                            ->join('lab_formula as f', 'f.id', '=', 'fp.idlab_formula')
                            ->select('f.formula')
                            ->where([
                                ['fr.eliminado', '=', 0],
                                ['fr.idlab_variables', '=', $model->id]
                            ])
                            ->groupBy('f.formula')
                            ->get();
        $formula = '....';
        if(count($lab_formula) > 0)
            $formula = $lab_formula[0]->formula;
        else
           $model->lab_formula = DB::table('lab_formula')->orderBy('formula', 'ASC')->get()->toJson();
        
        $lab_formularespuesta = DB::table('lab_formularespuesta as fr')
                            ->select('fr.idlab_formulaparametro', 'fr.lab_parametro_variable', 'fr.idlab_examenes', 
                                     'fr.lab_examenes')
                            ->where([
                                ['fr.eliminado', '=', 0],
                                ['fr.idlab_variables', '=', $model->id],
                                ['fr.idlab_formulaparametro', '>', 0]
                            ])
                            ->get()->toJson();
        $datosParametro = array(
                            'formula' => $formula,
                            'lab_formularespuesta' => $lab_formularespuesta,
                        );
        $model->txtHiddenJSONdatos = json_encode($datosParametro);

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelTipoVariables', $modelTipoVariables)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Lab_variables::find($id); 
        $variable = explode('-', $request->variable);
        
        $model->nombre = strtoupper($request->nombre);
        $model->valor_sup = $request->valor_sup;
        $model->valor_inf = $request->valor_inf;
        $model->unidad = $request->unidad;
        $model->metodo = strtoupper($request->metodo);
        $model->comentarios = strtoupper($request->comentarios);
        $model->idlab_tipo_variables = $variable[0];
        $model->mascara = strtoupper($request->mascara);
        $model->sigla = strtoupper($request->sigla);
        $model->idcategoria = $request->idcategoria;
        $model->sexo = $request->sexo;
        $model->edad_inicial = $request->edad_inicial;
        $model->edad_final = $request->edad_final;
        
        $jsonDatos = json_decode($request->txtHiddenJSONdatos);
        $jsonLab_formularespuesta = json_decode($jsonDatos->jsonLab_formularespuesta);
        $jsonListaTipoVariable = json_decode($jsonDatos->jsonListaTipoVariable);

        if($model->save())
        {
            Lab_lista::where('idlab_variables', '=', $id)->update(['eliminado' => true]);
            Lab_formularespuesta::where('idlab_variables', '=', $id)->update(['eliminado' => true]);
            
            // ------------------- REGISTRA TIPO DE VARIABLE -------------------
            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                // 'listaTipoVariable' => json_decode($request->txtListaTipoVariable)
                'listaTipoVariable' => $jsonListaTipoVariable,
                'jsonLab_formularespuesta' => $jsonLab_formularespuesta,
            );
            Lab_lista::registraDetalle($arrayDatos);
            Lab_formularespuesta::registraFormulaRespuesta($arrayDatos);
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("variable");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaParametroFormula()
    {
        if(isset($_POST['idlab_formula']))
        {
            $idlab_formula = $_POST['idlab_formula'];

            $modelo = DB::table('lab_formulaparametro as fp')
                        ->join('lab_parametro as p', 'fp.idlab_parametro', '=', 'p.id')
                        ->select('fp.id', 'fp.idlab_parametro', 'fp.idlab_formula', 'p.variable')
                        ->where('fp.idlab_formula', '=', $idlab_formula)
                        ->get()->toJson();
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    
}