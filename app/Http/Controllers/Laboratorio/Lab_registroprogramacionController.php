<?php
namespace App\Http\Controllers\Laboratorio;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Almacen\Generico;
use \App\Models\Almacen\Almacen;
use \App\Models\Almacen\Home;

use \App\Models\Laboratorio\Lab_variables;
use \App\Models\Laboratorio\Lab_examenes;
use \App\Models\Laboratorio\Lab_especialidad;
use \App\Models\Laboratorio\Lab_lista;
use \App\Models\Laboratorio\Lab_tipo_muestra;
use \App\Models\Laboratorio\Lab_tipo_variables;
use \App\Models\Laboratorio\Lab_registroprogramacion;
use \App\Models\Laboratorio\Lab_estado;
use \App\Models\Laboratorio\Lab_examenes_registroprogramacion;
use \App\Models\Laboratorio\Lab_resultados_programacion;
use \App\Models\Laboratorio\Lab_programacion_view;
use \App\Models\Laboratorio\Lab_grupos;

use \App\Models\Administracion\Datosgenericos;

use Session;
use Illuminate\Support\Facades\URL;

class Lab_registroprogramacionController extends Controller
{
    private $rutaVista = 'laboratorio.lab_registroprogramacion.';
    private $controlador = 'lab_registroprogramacion';
    private $idalmacen;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $dato = $request->searchNuevo;
        $opcSistema = $request->opcSistema;
        $buscarFecha = $request->buscarFecha;
        $date = date_create($request->fechaRegistro);
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado; 
        $modelEspecialidad = Lab_especialidad::where([
                            ['lab_especialidad.eliminado', '=', 0],
                            ['lab_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();
        $cantidad = 0;
        // $dato = $request->search;

        if ($buscarFecha == 1) {
            $model = Lab_programacion_view::where([
                        ['eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
                        ['idlab_estado', '=', $request->estado == Lab_estado::ANULADO? '' : $request->estado],
                        ['idalmacen', '=', $arrayDatos['idalmacen']]
                    ])
                    ->where(function($query) use($dato) {
                        $query->orwhere('hcl_poblacion_nombrecompleto', 'like', '%'.$dato.'%');
                    })
                    ->orderBy('numero', 'DESC')
                    ->paginate(config('app.pagination'));

            $modelo = DB::table('lab_examenes_registroprogramacion as p')
                    ->join('lab_examenes as e', 'p.idlab_examenes', '=', 'e.id')
                    ->join('lab_registroprogramacion as a', 'p.idlab_registroprogramacion', '=', 'a.id')
                    ->join('lab_especialidad as i', 'e.idlab_especialidad', '=', 'i.id')
                    ->select('i.codigo', 'i.nombre', DB::raw('count(a.id) as total'))                         
                    ->where([
                        ['a.eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
                        ['p.eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
                        ['a.idalmacen', '=', $arrayDatos['idalmacen']]
                        ]) 
                    ->groupBy('i.codigo', 'i.nombre')
                    ->orderBy('i.nombre', 'ASC')
                    ->get();

        }else {
            // $model = Lab_programacion_view::where([
            //             ['eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
            //             ['idlab_estado', '=', $request->estado == Lab_estado::ANULADO? '' : $request->estado],
            //             ['idalmacen', '=', $arrayDatos['idalmacen']]
            //         ])
            //         ->where(function($query) use($dato) {
            //             $query->orwhere('hcl_poblacion_nombrecompleto', 'like', '%'.$dato.'%');
            //         })
            //         ->where(function($query) use($fechaRegistro, $opcSistema) { 
            //         $query->where([ 
            //             ['fechaprogramacion', $opcSistema == 1? null : '=', $fechaRegistro == 2? '!=' : $fechaRegistro ], ]); 
            //         })
            //         ->orderBy('numero', 'DESC')
            //         ->paginate(config('app.pagination'));

            // ****************************** PROBANDOOOO.... ******************************
            $datosBuscador = json_decode($request->datosBuscador);
            if($datosBuscador == '')
            {
                $asegurado = '';
                $medico = '';
            }
            else
            {
                $asegurado = $datosBuscador->asegurado;
                $medico = $datosBuscador->medico;
            }
            // print_r($datosBuscador);
            // return;
            $model = Lab_programacion_view::where([
                        ['eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
                        // ['idlab_estado', '=', $request->estado == Lab_estado::ANULADO? '' : $request->estado],
                        // ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ['hcl_poblacion_nombrecompleto', 'like', '%'.$asegurado.'%'],
                        ['nombres', 'like', '%'.$medico.'%']
                    ])
                    // ->where(function($query) use($dato) {
                    //     $query->orwhere('hcl_poblacion_nombrecompleto', 'like', '%'.$dato.'%');
                    //     $query->orwhere('nombres', 'like', '%'.$dato.'%');
                    // })
                    // ->where(function($query) use($fechaRegistro, $opcSistema) { 
                    // $query->where([ 
                    //     ['fechaprogramacion', $opcSistema == 1? null : '=', $fechaRegistro == 2? '!=' : $fechaRegistro ], ]); 
                    // })
                    ->orderBy('numero', 'DESC')
                    ->paginate(config('app.pagination'));

            // $dato->hcl_poblacion_nombrecompleto
            // $dato->nombres.' '.$dato->apellidos
            $model->datosBuscador = $request->datosBuscador;
            // ****************************************************************************

            $modelo = DB::table('lab_examenes_registroprogramacion as p')
                    ->join('lab_examenes as e', 'p.idlab_examenes', '=', 'e.id')
                    ->join('lab_registroprogramacion as a', 'p.idlab_registroprogramacion', '=', 'a.id')
                    ->join('lab_especialidad as i', 'e.idlab_especialidad', '=', 'i.id')
                    ->select('i.codigo', 'i.nombre', DB::raw('count(a.id) as total'))
                    ->where([
                        ['a.eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
                        ['p.eliminado', '=', $request->estado == Lab_estado::ANULADO? 1 : 0],
                        ['a.idalmacen', '=', $arrayDatos['idalmacen']]
                        ]) 
                    ->where(function($query) use($fechaRegistro) { 
                        $query->where([ 
                            ['a.fechaprogramacion', '=', $fechaRegistro ], ]); 
                        })
                    ->groupBy('i.codigo', 'i.nombre')
                    ->orderBy('i.nombre', 'ASC')
                    ->get();             
        } 

        $modelLab_estado = Lab_estado::orderBy('nombre', 'ASC')->get();
        
        $model->scenario = 'index';
        $model->fechaRegistro = $fechaRegistro;
        $model->searchNuevo = $dato;
        $model->buscarFecha = $buscarFecha;
        $model->estado = $request->estado;
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelo', $modelo)
                ->with('modelLab_estado', $modelLab_estado)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $modelHcl_especialidad = ['' => 'Seleccione! '] + 
                                    Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->pluck('descripcion', 'id')->all();
        $modelHcl_cuaderno = ['' => 'Seleccione! '] + 
                                    Hcl_cuaderno::where('eliminado', '=', 0)
                                    ->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $modelDatosgenericos = Datosgenericos::where('id_generico', '=', Generico::MEDICO)
                                    ->orderBy('nombres', 'ASC')->get();
        
        $model = new Lab_registroprogramacion;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];
        
        $this->seteaDatosComunes($model);
        $model->fechaprogramacion = date('d-m-Y');
        $model->hora_actual = date("g:i A");

        // print_r($model->grupos);
        // return;
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('modelHcl_cuaderno', $modelHcl_cuaderno)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Lab_registroprogramacion;
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();

        if($modelAlmacenusuario == null)
        {
            echo 'Por favor Asigne una UNIDAD MEDICA al usuario! ';
            return;
        } 
        $numero = Lab_registroprogramacion::generarNumero($modelAlmacenusuario->idalmacen);
        $date = date_create($request->fechaprogramacion);
        
        $model->numero = $numero;
        $model->idhcl_poblacion = $request->txtidhcl_poblacion;
        $model->idhcl_especialidad = $request->idhcl_especialidad;
        $model->idalmacen = $modelAlmacenusuario->idalmacen;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos;
        $model->hospitalizado = $request->checkHospitalizado == true? 1 : 0;
        $model->fechaprogramacion =  date_format($date, 'Y-m-d');
        $model->horaprogramacion = $request->txtHiddenHora;
        $model->diagnostico = strtoupper($request->diagnostico);
        $model->observacion = strtoupper($request->observacion);
        $model->idlab_estado = Lab_estado::PROGRAMACION;
        $model->usuario = Auth::user()['name'];

        // $arrayExamenes = json_decode($request->txtJsonExamenes);
        // print_r($arrayExamenes);
        // return;


        if($model->save())
        {
            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                'arrayExamenes' => json_decode($request->txtJsonExamenes)
            );
            Lab_resultados_programacion::registraResultado($arrayDatos);

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("programacion");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // print_r($arrayDatos['idalmacen']);
        // return; 

        $modelTipoVariables = Lab_tipo_variables::all();
        $model = Lab_variables::find($codigo);

        $modelLab_lista = Lab_lista::where('idlab_variables', '=', $model->id)->get();
        // foreach ($modelLab_lista as $value) {
        //     print_r($value['lis_variable']);    
        // }
        
        return;
        $model->listavariable =  json_encode($modelLab_lista);
        
       
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelTipoVariables', $modelTipoVariables) 
                ->with('arrayDatos', $arrayDatos);  
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelHcl_especialidad = ['' => 'Seleccione! '] + 
                                    Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->pluck('descripcion', 'id')->all();
        $model = Lab_registroprogramacion::find($id);

        $model->txtJsonHcl_poblacion = hcl_poblacion::find($model->idhcl_poblacion)->toJson();
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        $model->examenesProgramados = Lab_examenes_registroprogramacion::where([
                                ['eliminado', '=', 0],
                                ['idlab_registroprogramacion', '=', $id]
                            ])->get()->toJson();

        $this->seteaDatosComunes($model);
        $model->lab_examenesEspecialidad = DB::table('lab_examenes_registroprogramacion as exesp')
                    ->join('lab_examenes as le', 'exesp.idlab_examenes', '=', 'le.id')
                    ->join('lab_especialidad as e', 'le.idlab_especialidad', '=', 'e.id')
                    ->select(
                        'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                        'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                    )
                    ->where([
                        ['le.eliminado', '=', 0],
                        // ['e.idalmacen', '=', $model->idalmacen]
                        ['exesp.idlab_registroprogramacion', '=', $id]
                    ])
                    ->orderBy('le.nombre', 'ASC')
                    ->get()->toJson();
        
        $date = date_create($model->fechaprogramacion);
        $model->fechaprogramacion = date_format($date, 'd-m-Y');

        $modelHcl_cuaderno = Hcl_cuaderno::where('id', '=', $model->idhcl_cuaderno)->first();
        if($modelHcl_cuaderno != null)
            $model->especialidad = $modelHcl_cuaderno->nombre;

        $modelDatosgenericos = Datosgenericos::where('id', '=', $model->iddatosgenericos)->first();
        if($modelDatosgenericos != null)
            $model->medico = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model) 
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $date = date_create($request->fechaprogramacion);
        $fechaprogramacion = date_format($date, 'Y-m-d');

        $model = Lab_registroprogramacion::find($id); 

        $model->idhcl_poblacion = $request->txtidhcl_poblacion;
        $model->idhcl_especialidad = $request->idhcl_especialidad;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos;

        $model->hospitalizado = $request->checkHospitalizado == true? 1 : 0;
        $model->fechaprogramacion = $fechaprogramacion;
        $model->horaprogramacion = $request->txtHiddenHora;
        $model->diagnostico = strtoupper($request->diagnostico);
        $model->observacion = strtoupper($request->observacion);
        $model->idlab_estado = Lab_estado::PROGRAMACION;

        if($model->save())
        {
            Lab_examenes_registroprogramacion::where([
                ['eliminado', '=', 0],
                ['idlab_registroprogramacion', '=', $id]
            ])->update(['eliminado' => true]);

            Lab_resultados_programacion::where([
                ['eliminado', '=', 0],
                ['idlab_registroprogramacion', '=', $id]
            ])->update(['eliminado' => true]);

            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                'arrayExamenes' => json_decode($request->txtJsonExamenes)
            );
            Lab_resultados_programacion::registraResultado($arrayDatos);

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("programacion");
    }

    private function seteaDatosComunes($model)
    {
        $model->lab_examenesEspecialidad = DB::table('lab_examenes as le')
                        ->join('lab_especialidad as e', 'le.idlab_especialidad', '=', 'e.id')
                        ->select(
                            'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                            'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                        )
                        ->where([
                            ['le.eliminado', '=', 0],
                            ['e.idalmacen', '=', $model->idalmacen]
                        ])
                        ->orderBy('le.nombre', 'ASC')
                        ->get()->toJson();
        
        $model->especialidadProgramados = DB::table('lab_examenes_registroprogramacion as p')
                        ->join('lab_examenes as e', 'p.idlab_examenes', '=', 'e.id')
                        ->join('lab_especialidad as esp', 'e.idlab_especialidad', '=', 'esp.id')
                        ->select('esp.codigo', 'esp.nombre', DB::raw('count(p.idlab_examenes) as total'))
                        ->where([
                            ['p.eliminado', '=', 0],
                            ['e.idalmacen', '=', $model->idalmacen]
                        ])
                        ->groupBy('esp.codigo', 'esp.nombre')
                        ->orderBy('esp.nombre', 'ASC')
                        ->get()->toJson();

        $model->grupos = Lab_grupos::muestraGrupos($model->idalmacen);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaExamenes()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $buscarTodo = $_POST['buscarTodo'];
            $idsLab = '';
            
            if(isset($_POST['datosLab_Ima']))
                $idsLab = $this->retornaIdExamenes(json_decode($_POST['datosLab_Ima']));
            
            $modelLab_variables = DB::table('lab_examenes as le')
                ->join('lab_especialidad as e', 'le.idlab_especialidad', '=', 'e.id')
                ->select(
                    'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                    'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                )
                ->where([
                    ['le.eliminado', '=', 0]
                ])
                ->whereIn($buscarTodo == 0 && $idsLab != ''? 'le.id' : 'le.eliminado', 
                        array($buscarTodo == 0 && $idsLab != ''? DB::raw("select id from lab_examenes where id in(".$idsLab.")") : 0) )
                ->where(function($query) use($dato) {
                    $query->orwhere('le.codigo', 'ilike', '%'.$dato.'%');
                    $query->orwhere('le.nombre', 'ilike', '%'.$dato.'%');
                    $query->orwhere('e.codigo', 'ilike', '%'.$dato.'%');
                    $query->orwhere('e.nombre', 'ilike', '%'.$dato.'%');
                })
                ->take(config('app.muestraTotalBusqueda'))
                ->get();
            echo json_encode($modelLab_variables);
        }
    }
    private function retornaIdExamenes($datosTablaLab)
    {
        $idsLab = '';
        for($j = 0; $j < count($datosTablaLab); $j++)
        {
            if($j == 0)
                $idsLab .= $datosTablaLab[$j];
            else
                $idsLab .= ','. $datosTablaLab[$j];
        }
        return $idsLab;
    }
    public function buscaExamenesLab_perfil_examenes()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $idlab_grupos = isset($_POST['idlab_grupos'])? $_POST['idlab_grupos'] : 0;
            
            $idsLab = '0';
            if(isset($_POST['idlab_examenesArray']))
                $idsLab = $this->retornaIdExamenes($_POST['idlab_examenesArray']);
            
            $modelLab_variables = DB::table('lab_examenes as le')
                    ->join('lab_especialidad as e', 'le.idlab_especialidad', '=', 'e.id')
                    ->select(
                        'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                        'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                    )
                    ->where([
                        ['le.eliminado', '=', 0],
                    ])
                    ->whereIn('le.id', function($query) use($idlab_grupos){
                        $query->select('idlab_examenes')
                            ->from('lab_perfil_examenes')
                            ->where('idlab_grupos', '=', $idlab_grupos);
                    })
                    ->whereNotIn('le.id', [DB::raw($idsLab)])
                    ->where(function($query) use($dato) {
                        $query->orwhere('le.codigo', 'ilike', '%'.$dato.'%');
                        $query->orwhere('le.nombre', 'ilike', '%'.$dato.'%');
                        $query->orwhere('e.codigo', 'ilike', '%'.$dato.'%');
                        $query->orwhere('e.nombre', 'ilike', '%'.$dato.'%');
                    })
                    ->take(config('app.muestraTotalBusqueda'))
                    ->get();
            echo json_encode($modelLab_variables);
        }
    }
    public function buscaProgramados()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = DB::table('lab_examenes_registroprogramacion as p')
                        ->join('lab_examenes as e', 'p.idlab_examenes', '=', 'e.id')
                        ->select('e.codigo', 'e.nombre', DB::raw('count(p.idlab_examenes) as total'))
                        ->where([
                            ['p.eliminado', '=', 0]
                        ])
                        ->groupBy('e.codigo', 'e.nombre')
                        ->orderBy('e.nombre', 'ASC')
                        ->get();
            
            echo json_encode($modelo);
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------------------
    // ----------------------------------------------- [REPORTE] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public static function imprimeProgramacion($id)
    {
        Lab_registroprogramacion::imprimeProgramacion($id);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}