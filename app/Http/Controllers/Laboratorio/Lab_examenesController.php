<?php
namespace App\Http\Controllers\Laboratorio;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;

use \App\Models\Laboratorio\Lab_examenes;
use \App\Models\Laboratorio\Lab_especialidad;
use \App\Models\Laboratorio\Unidad_medica;
use \App\Models\Laboratorio\Lab_tipo_muestra;
use \App\Models\Laboratorio\Lab_variables;
use \App\Models\Laboratorio\Lab_lista;
use \App\Models\Laboratorio\Lab_examenes_variables;
use \App\Models\Laboratorio\Lab_grupos;
use \App\Models\Laboratorio\Lab_formularespuesta;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Lab_examenesController extends Controller
{
    private $rutaVista = 'laboratorio.lab_examenes.';
    private $controlador = 'lab_examenes';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;

        $modelLab_especialidad = Lab_especialidad::where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']],
                ])->orderBy('nombre', 'ASC')->get();
        $modelLab_tipo_muestra = Lab_tipo_muestra::orderBy('nombre', 'ASC')->get();


        $codigo = '';
        $nombre = '';
        $especialidad = null;
        $tipomuestra = null;
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $codigo = $datosBuscador->codigo;
            $nombre = $datosBuscador->nombre;
            $especialidad = $datosBuscador->especialidad > 0? $datosBuscador->especialidad : null;
            $tipomuestra = $datosBuscador->tipomuestra > 0? $datosBuscador->tipomuestra : null;
        }
        $model = DB::table('lab_examenes as le')
                    ->join('lab_especialidad as esp', 'esp.id', '=', 'le.idlab_especialidad')
                    ->join('lab_tipo_muestra as ltm', 'ltm.id', '=', 'le.idlab_tipo_muestra')
                    ->select('le.*', 'esp.nombre as especialidad', 'ltm.nombre as tipomuestra')
                    ->where([
                        ['le.eliminado', '=', 0],
                        ['le.idalmacen', '=', $arrayDatos['idalmacen']],
                        ['le.codigo', 'ilike', '%'.$codigo.'%'],
                        ['le.nombre', 'ilike', '%'.$nombre.'%'],
                        ['le.idlab_especialidad', $especialidad > 0? '=' : '!=', $especialidad],
                        ['le.idlab_tipo_muestra', $tipomuestra > 0? '=' : '!=', $tipomuestra],
                    ])
                    ->orderBy('le.nombre', 'ASC')
                    ->paginate(config('app.pagination'));
        $model->datosBuscador = $request->datosBuscador;
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelLab_especialidad', $modelLab_especialidad)
                ->with('modelLab_tipo_muestra', $modelLab_tipo_muestra)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $idalmacen = $arrayDatos['idalmacen'];
        $modelEspecialidades = Lab_especialidad::where('idalmacen', '=', $idalmacen)->orderBy('nombre', 'ASC')->get();
        $modelMuestras = Lab_tipo_muestra::orderBy('nombre', 'ASC')->get();
        
        $model = new Lab_examenes;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $idalmacen;
        $model->listagrupos = Lab_grupos::where([
                        ['eliminado', '=', 0],
                        ['idalmacen', '=', $model->idalmacen]
                    ])->get()->toJson();

        Session::put('urlAnterior', URL::previous());
        
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelEspecialidades', $modelEspecialidades)
                ->with('modelMuestras', $modelMuestras)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        if($request->txtListaVariables == null)
        {
            echo "NO se han recibido correctamente la lista de Variables! ";
            return;
        }
        $idalmacen = $request->idalmacen;

        $model = new Lab_examenes;

        // $rules = [
        //     'codigo'=>'required|unique:lab_examenes|max:50', 
        //     'nombre'=>'required', 
        // ];
        // $messages = [
        //     'codigo.required' => 'Introducir Código.',
        //     'codigo.unique' => 'El Código ya ha sido registrado.',
        //     'codigo.max' => 'El Código no puede tener más de 3 caracteres.',
        //     'nombre.required' => 'Introducir Nombre.',
        // ];
        // $this->validate($request, $rules, $messages);

        $numero = Lab_examenes::generarNumero($idalmacen);
        $model->numero = $numero;
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);
        $model->idlab_especialidad = $request->especialidad;
        $model->idlab_tipo_muestra = $request->muestra;
        $model->idalmacen = $idalmacen;
        $model->usuario = Auth::user()['name'];
        
        if($model->save())
        {
            //  Registra las variables
            $arrayDatos = array(
                'request' => $request,
                'model' => $model
            );
            Lab_examenes_variables::registraExamenVariables($arrayDatos);
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("examen");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelUnidadesMedicas = Unidad_medica::all();
        $modelEspecialidades = Lab_especialidad::all();
        $modelMuestras = Lab_tipo_muestra::all();

        $model = Lab_examenes::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelUnidadesMedicas', $modelUnidadesMedicas)
                ->with('modelEspecialidades', $modelEspecialidades)
                ->with('modelMuestras', $modelMuestras)
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_examenes::find($id);
        $modelEspecialidades = Lab_especialidad::all();
        $modelMuestras = Lab_tipo_muestra::all();
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        $model->listagrupos = Lab_grupos::where('eliminado', '=', 0)->get()->toJson();

        // [1]
        $model->listavariable = DB::table('lab_examenes_variables as ev')
                                ->join('lab_variables as v', 'ev.idlab_variables', '=', 'v.id')
                                ->join('lab_grupos as g', 'ev.idlab_grupos', '=', 'g.id')
                                ->join('lab_tipo_variables as tv', 'v.idlab_tipo_variables', '=', 'tv.id')
                                ->select('ev.idlab_variables', 'ev.orden', 'v.nombre', 'ev.idlab_grupos', 
                                         'tv.permite_llenado')
                                ->where([
                                    ['ev.idlab_examenes', '=', $id],
                                    ['ev.eliminado', '=', 0]
                                ])
                                ->orderBy('ev.orden', 'ASC')
                                ->get()->toJson();

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelEspecialidades', $modelEspecialidades)
                ->with('modelMuestras', $modelMuestras)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Lab_examenes::find($id);

        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);
        $model->idlab_especialidad = $request->especialidad;
        $model->idlab_tipo_muestra = $request->muestra;
        $model->usuario = Auth::user()['name'];

        if($model->save())
        {
            // ELIMINA LOS ANTERIORES REGISTROS
            Lab_examenes_variables::where('idlab_examenes', '=', $id)->update(['eliminado' => true]);
            Lab_formularespuesta::where('idlab_examenes', $id)->delete();
            
            //  Registra las variables
            $arrayDatos = array(
                'request' => $request,
                'model' => $model
            );
            Lab_examenes_variables::registraExamenVariables($arrayDatos);
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("examen");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaVariables()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $idalmacen = $_POST['idalmacen'];

            $modelo = DB::table('lab_variables as v')
                    ->join('lab_tipo_variables as t', 'v.idlab_tipo_variables', '=', 't.id')
                    ->select('v.id', 'v.nombre', 't.permite_llenado','v.sigla')
                    ->where([
                        ['v.idalmacen', '=', $idalmacen],
                    ])
                    ->where(function($query) use($dato) { 
                        $query->orwhere('v.nombre', 'ilike', '%'.$dato.'%');  
                        $query->orwhere('v.sigla', 'ilike', '%'.$dato.'%');
                    })
                    ->orderBy('v.nombre', 'ASC')
                    ->get();
            
            echo json_encode($modelo);
        }
    }

    public function buscaTipoVariables()
    {
        $idtabla = $_POST['idtabla'];
        $modelLab_lista = Lab_lista::where([
            ['idlab_variables', '=', $idtabla],
            ['eliminado', '=', 0]
        ])->get();
        echo json_encode($modelLab_lista);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
