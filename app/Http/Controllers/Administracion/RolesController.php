<?php
namespace App\Http\Controllers\Administracion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen; 
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use \App\Models\Administracion\Roles;
use \App\Models\Administracion\Menu;
use \App\Models\Administracion\Menuopcion;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    private $rutaVista = 'Administracion.roles.';
    private $controlador = 'roles';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1; 
        $model = Roles::get();   
        
        $view = $this->rutaVista . 'index';        
        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;

        return view($view)
                ->with('model', $model)  
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        
        $model = new Roles;    
        $modelMenu = Menu::all();   
        $modelOpcion = Menuopcion::all();   


        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista; 

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('modelMenu', $modelMenu) 
                ->with('modelOpcion', $modelOpcion) 
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Hsi_tipo;
        $model->idhsi_tipo_registro = $request->idhsi_tipo_registro;        
        $model->codigo = $request->codigo;        
        $model->nombre = $request->nombre;           
        $model->usuario = Auth::user()->name;

        if($model->save()){ 
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $model = Hsi_tipo::find($id);
        $modelRegistro = Hsi_tipo_registro::get();  

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelRegistro', $modelRegistro)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {         
        $model = Hsi_tipo::find($id); 
        $model->idhsi_tipo_registro = $request->idhsi_tipo_registro;        
        $model->codigo = $request->codigo;        
        $model->nombre = $request->nombre;            
        $model->usuario = Auth::user()->name;

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaPaciente_hsi()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            // $modelo = Hcl_poblacion::where(function($query) use($dato) {
            //             $query->orwhere('numero_historia', 'ilike', '%'.$dato.'%');
            //             $query->orwhere('nombre', 'ilike', '%'.$dato.'%');
            //             $query->orwhere('primer_apellido', 'ilike', '%'.$dato.'%');
            //             $query->orwhere('segundo_apellido', 'ilike', '%'.$dato.'%');
            //             $query->orwhere(DB::raw("CONCAT(nombre, ' ',primer_apellido, ' ', segundo_apellido)"), 'ilike', '%'.$dato.'%');
            //         })
            //         ->select('hcl_poblacion.*', 
            //                 DB::raw("(SELECT date_part('year', age(fecha_nacimiento)))::integer AS edad"))
            //         ->take(config('app.muestraTotalBusqueda'))
            //         ->get();
            
            $modelo = DB::table(DB::raw("hsi_busca_paciente('".$dato."','".$dato."','".$dato."','".$dato."')"))->take(config('app.muestraTotalBusqueda'))->get();   

            echo json_encode($modelo);
        }
    }
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------

}