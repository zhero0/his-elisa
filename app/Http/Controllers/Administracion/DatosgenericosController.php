<?php
namespace App\Http\Controllers\Administracion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Auth;

use \App\Models\Almacen\Home;
use \App\Models\Almacen\Generico;
use \App\Models\Almacen\Almacen;

use \App\Permission;
use \App\User;
use Hash;

use \App\Models\Usuario\Almacenusuario;

use \App\Models\Administracion\Datosgenericos;

use Session;
use Illuminate\Support\Facades\URL;

class DatosgenericosController extends Controller
{
    private $rutaVista = 'Administracion.datosgenericos.';
    private $controlador = 'datosgenericos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $dato = $request->search; 
        $model = Datosgenericos::where('eliminado', '=', 0)
                            ->where(function($query) use ($dato) {
                                $query->orwhere('nombres', 'like', '%'.$dato.'%');
                                $query->orwhere('apellidos', 'like', '%'.$dato.'%');
                                $query->orwhere('ci', 'like', '%'.$dato.'%');
                                $query->orwhere('direccion', 'like', '%'.$dato.'%');
                                $query->orwhere('telefono', 'like', '%'.$dato.'%');
                                $query->orwhere('descripcion', 'like', '%'.$dato.'%');
                            })
                            ->get();

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', 'personal');
 
        $modelGenerico = Generico::where([
                                ['eliminado', '=', 0],
                                ['idtipo', '=', Generico::PERSONAL]
                            ])->orderBy('nombre', 'ASC')->get(); 

        

        $modelAlmacen = Almacen::where([
                                ['eliminado', '=', 0], 
                            ])->orderBy('nombre', 'ASC')->get(); 

        $model = new Datosgenericos;

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelGenerico', $modelGenerico)
                ->with('modelAlmacen', $modelAlmacen)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombres' => 'required',
            'apellidos' => 'required', 
            'id_generico' => 'required',
            'idalmacen' => 'required',
        ];
        $messages = [
            'nombres.required' => 'Se requiere registrar Nombres',
            'apellidos.required' => 'Se requiere registrar Apeliidos', 
            'id_generico.required' => 'Tipo Personal es obligatorio',
            'idalmacen.required' => 'Unidad Médica es obligatorio',
        ];
        $this->validate($request, $rules, $messages);

        $nombre = explode(' ', strtoupper($request['nombres']));
        $apellido = explode(' ', strtoupper($request['apellidos']));
        $usuario = strtolower($nombre[0].'.'.$apellido[0]);
        // $password = $request['ci'] != ''? $usuario.'.'.$request['ci'] : $usuario;
        $password = $request['ci'];

        $model = new Datosgenericos();

        $model->nombres = strtoupper($request['nombres']);
        $model->apellidos = strtoupper($request['apellidos']);
        $model->direccion = strtoupper($request['direccion']);
        $model->ci = $request['ci'];
        $model->telefono = $request['telefono'];
        $model->descripcion = $request['descripcion'];
        $model->matricula = $request['matricula'];
        $model->titulo = $request['titulo'];
        $model->id_generico = $request['id_generico'];
        $model->idalmacen = $request['idalmacen'];
        $model->usuario = Auth::user()['name'];
                
        if($model->save())
        {
            $modelUser = new User;
            $modelUser->name = $usuario;
            $modelUser->email = $model->id.$usuario.'@gmail.com';
            $modelUser->avatar = 'default.jpg';
            $modelUser->password = Hash::make($password);;
            $modelUser->forceSave();
            
            $modelo = new Almacenusuario;
            $modelo->idalmacen = $request['idalmacen'];
            $modelo->idusuario = $modelUser->id;
            $modelo->iddatosgenericos = $model->id;
            $modelo->save();
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
                             
        }
        else
            return redirect("personal");



        // return redirect("personal")
        //         ->with('message', 'store')
        //         ->with('mensaje', config('app.mensajeGuardado'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrayDatos = Home::parametrosSistema();

        if(Permission::verificarAcceso($this->controlador.'.show') == 0)
            return view('errors.403')
                        ->with('arrayDatos', $arrayDatos)
                        ->with('url', 'personal');

        $modelGenerico = ['' => 'Seleccione! '] + 
                            Generico::where([
                                ['eliminado', '=', 0],
                                ['idtipo', '=', Generico::PERSONAL]
                            ])->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $modelAlmacen = ['' => 'Seleccione! '] + 
                            Almacen::where('eliminado', '=', 0)->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();

        $model = Datosgenericos::find($id); 
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'view',compact('model')) 
                ->with('modelGenerico', $modelGenerico)
                ->with('modelAlmacen', $modelAlmacen)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', 'personal');

        $modelGenerico = Generico::where([
                                ['eliminado', '=', 0],
                                ['idtipo', '=', Generico::PERSONAL]
                            ])->orderBy('nombre', 'ASC')->get(); 

        

        $modelAlmacen = Almacen::where([
                                ['eliminado', '=', 0], 
                            ])->orderBy('nombre', 'ASC')->get(); 

        $model = Datosgenericos::find($id);
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        // $modelAlmacenusuario = Almacenusuario::where('iddatosgenericos', '=', $id)->first();
        // if($modelAlmacenusuario != null)
        //     $model->idalmacen = $modelAlmacenusuario->idalmacen;

        Session::put('urlAnterior', URL::previous());
        
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelGenerico', $modelGenerico)
                ->with('modelAlmacen', $modelAlmacen)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nombres' => 'required',
            'apellidos' => 'required', 
            'id_generico' => 'required',
            'idalmacen' => 'required',
        ];
        $messages = [
            'nombres.required' => 'Se requiere registrar Nombres',
            'apellidos.required' => 'Se requiere registrar Apeliidos', 
            'id_generico.required' => 'Tipo Personal es obligatorio',
            'idalmacen.required' => 'Unidad Médica es obligatorio',
        ];
        $this->validate($request, $rules, $messages);

        $model = Datosgenericos::find($id);
        
        $model->nombres = strtoupper($request->nombres);
        $model->apellidos = strtoupper($request->apellidos); 
        $model->direccion = strtoupper($request->direccion);
        $model->ci = $request->ci;
        $model->telefono = $request->telefono;
        $model->descripcion = $request->descripcion;
        $model->matricula = $request->matricula;
        $model->titulo = $request->titulo;
        $model->id_generico = $request['id_generico'];
        $model->idalmacen = $request['idalmacen'];

        if($model->save())
        {
            $modelo = Almacenusuario::where('iddatosgenericos', '=', $id)->first();
            if($modelo->idalmacen != $request['idalmacen'])
            {
                $modelo->idalmacen = $request['idalmacen'];
                $modelo->save();
            }

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
                             
        }
        else
            return redirect("personal");


        // return redirect("personal")
        //         ->with('message', 'update')
        //         ->with('mensaje', config('app.mensajeGuardado'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
    Autocomplete de PERSONAL
    */
    public function autocomplete(Request $request)
    {
        $dato = $request->term;
        $data = Datosgenericos::where('id_generico', '=', '2')
                ->where(function($query) use ($dato) {
                        $query->orwhere('nombres', 'like', '%'.$dato.'%');
                        $query->orwhere('apellidos', 'like', '%'.$dato.'%');
                        $query->orwhere('ci', 'like', '%'.$dato.'%');
                    })
            ->orderBy('nombres','ASC')
            ->take(config('app.mostrarTotalDatos'))
            ->get();
        
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[] = [
                'id' => $value->id,
                'value' => $value->nombres.' '.$value->apellidos.' ('.$value->ci.')',
                'label' => $value->nombres.' '.$value->apellidos.' ('.$value->ci.')'
            ];
        }
        return response()->json($result);
    }
    
    
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function busca_Medico()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Datosgenericos::where('eliminado', '=', 0)
                    ->where(function($query) use ($dato) {
                            $query->orwhere('nombres', 'ilike', '%'.$dato.'%');
                            $query->orwhere('apellidos', 'ilike', '%'.$dato.'%');
                            $query->orwhere(DB::raw("CONCAT(nombres, ' ', apellidos)"), 'ilike', '%'.$dato.'%');
                        })
                    ->orderBy('nombres', 'ASC')
                     ->get();
            echo json_encode($modelo);
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    
}