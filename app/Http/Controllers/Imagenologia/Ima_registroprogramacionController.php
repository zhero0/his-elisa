<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Administracion\Impresion;
use \App\Models\Administracion\Datosgenericos;
use \App\Models\Almacen\Generico;
use \App\Models\Almacen\Almacen;
use \App\Models\Almacen\Home;
 
use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Imagenologia\Ima_examenes;
use \App\Models\Imagenologia\Ima_registroprogramacion;
use \App\Models\Imagenologia\Ima_estado;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;
use \App\Models\Imagenologia\Ima_resultados_programacion;

use Elibyy\TCPDF\Facades\TCPDF;
use Session;
use Illuminate\Support\Facades\URL;

class Ima_registroprogramacionController extends Controller
{
    private $rutaVista = 'imagenologia.ima_registroprogramacion.';
    private $controlador = 'ima_registroprogramacion';
    private $idalmacen;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $code = $request->searchCode != 0? $request->searchCode : 0;         
        $dato = $request->searchNuevo;        
        $buscarFecha = $request->buscarFecha;
        $date = date_create($request->fechaRegistro);
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado; 

        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();

        $cantidad = 0;
        if ($buscarFecha == 1) {
            $modelo = DB::table('ima_examenes_registroprogramacion as p')
                        ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                        ->join('ima_registroprogramacion as a', 'p.idima_registroprogramacion', '=', 'a.id')
                        ->join('ima_especialidad as i', 'e.idima_especialidad', '=', 'i.id')
                        ->select('i.codigo', 'i.nombre', DB::raw('count(a.id) as total'))                         
                        ->where([
                            ['a.eliminado', '=', $request->estado == Ima_estado::ANULADO? 1 : 0],
                            ['a.idalmacen', '=', $arrayDatos['idalmacen']]
                            ]) 
                        ->groupBy('i.codigo', 'i.nombre')
                        ->orderBy('i.nombre', 'ASC')
                        ->get();
        }
        else {
            $modelo = DB::table('ima_examenes_registroprogramacion as p')
                    ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                    ->join('ima_registroprogramacion as a', 'p.idima_registroprogramacion', '=', 'a.id')
                    ->join('ima_especialidad as i', 'e.idima_especialidad', '=', 'i.id')
                    ->select('i.codigo', 'i.nombre', DB::raw('count(a.id) as total'))                         
                    ->where([
                        ['a.eliminado', '=', $request->estado == Ima_estado::ANULADO? 1 : 0],
                        ['a.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])
                    ->where(function($query) use($fechaRegistro) { 
                    $query->where([ 
                        ['a.fechaprogramacion', '=', $fechaRegistro ], ]); 
                    })
                    ->groupBy('i.codigo', 'i.nombre')
                    ->orderBy('i.nombre', 'ASC')
                    ->get(); 
        }
        $modelIma_estado = Ima_estado::orderBy('nombre', 'ASC')->get();
        
        $paginacion = config('app.pagination');
        $numero = '';
        $numeroHistoria = '';
        $asegurado = '';
        $especialidad = '';
        $medico = '';
        $fecha = null;
        $estado = null;
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $paginacion = $datosBuscador->paginacion;
            $numero = $datosBuscador->numero > 0? $datosBuscador->numero : '';
            $numeroHistoria = $datosBuscador->numeroHistoria;
            $asegurado = $datosBuscador->asegurado;
            $especialidad = $datosBuscador->especialidad;
            $medico = $datosBuscador->medico;
            $fecha = $datosBuscador->fecha == ''? null : $datosBuscador->fecha;
            $estado = $datosBuscador->estado > 0? $datosBuscador->estado : null;
        }
        $model = DB::table('ima_programacion_view')
                    ->where([
                        // ['eliminado', '=', $request->estado == Ima_estado::ANULADO? 1 : 0],
                        ['idalmacen', '=', $arrayDatos['idalmacen']],
                        [DB::raw('numero::text'), 'ilike', '%'.$numero.'%'],
                        ['hcl_poblacion_matricula', 'ilike', '%'.$numeroHistoria.'%'],
                        ['hcl_poblacion_nombrecompleto', 'ilike', '%'.$asegurado.'%'],
                        ['cuaderno', 'ilike', '%'.$especialidad.'%'],
                        [DB::raw("CONCAT(nombres, ' ', apellidos)"), 'ilike', '%'.$medico.'%'],
                        // [DB::raw('fechaprogramacion::date'), $fecha == ''? '!=' : '=', $fecha],
                        // ['idima_estado', $estado > 0? '=' : '!=', $estado],
                    ])
                    ->orderBy('numero', 'DESC')
                    ->paginate($paginacion);
        $model->datosBuscador = $request->datosBuscador;
        

        $model->scenario = 'index';
        // $model->fechaRegistro = $fechaRegistro;
        
        // $model->searchNuevo = $dato;
        // $model->buscarFecha = $buscarFecha;
        // $model->estado = $request->estado;
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelo', $modelo)
                ->with('modelIma_estado', $modelIma_estado)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $modelHcl_especialidad = ['' => 'Seleccione! '] + 
                                    Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->pluck('descripcion', 'id')->all();
        $modelHcl_cuaderno = ['' => 'Seleccione! '] + 
                                    Hcl_cuaderno::where('eliminado', '=', 0)
                                    ->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $modelDatosgenericos = Datosgenericos::where('id_generico', '=', Generico::MEDICO)
                                    ->orderBy('apellidos', 'ASC')->get();

        $model = new Ima_registroprogramacion;
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        $this->seteaDatosComunes($model);
        $model->carpeta = 'AAA';
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('modelHcl_cuaderno', $modelHcl_cuaderno)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();

        if($modelAlmacenusuario == null)
        {
            echo 'Por favor Asigne una UNIDAD MEDICA al usuario! ';
            return;
        }
        
        $model = new Ima_registroprogramacion;
        $numero = Ima_registroprogramacion::generarNumero($modelAlmacenusuario->idalmacen);
        $date = date_create($request->fechaprogramacion);
        
        $model->numero = $numero;
        $model->idhcl_poblacion = $request->txtidhcl_poblacion;
        $model->idhcl_especialidad = $request->idhcl_especialidad;
        $model->idalmacen = $modelAlmacenusuario->idalmacen;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos;
        $model->hospitalizado = $request->checkHospitalizado == true? 1 : 0;
        $model->fechaprogramacion = date_format($date, 'Y-m-d');
        $model->horaprogramacion = $request->txtHiddenHora;
        $model->diagnostico = $request->diagnostico;
        $model->observacion = $request->observacion;
        $model->idima_estado = Ima_estado::PROGRAMACION;
        $model->usuario = Auth::user()['name'];

        // [ DATOS DEL PACIENTE ]
        $Hcl_poblacion = Hcl_poblacion::muestraDatosPoblacion($request);
        $model->hcl_poblacion_nombrecompleto = $Hcl_poblacion['hcl_poblacion_nombrecompleto'];
        $model->hcl_poblacion_matricula = $Hcl_poblacion['hcl_poblacion_matricula'];
        $model->hcl_poblacion_cod = $Hcl_poblacion['hcl_poblacion_cod'];
        $model->hcl_poblacion_sexo = $Hcl_poblacion['hcl_poblacion_sexo'];
        $model->hcl_poblacion_fechanacimiento = $Hcl_poblacion['hcl_poblacion_fechanacimiento'];
        $model->hcl_empleador_empresa = $Hcl_poblacion['hcl_empleador_empresa'];
        $model->hcl_empleador_patronal = $Hcl_poblacion['hcl_empleador_patronal'];

        if($model->save())
        {
            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                'arrayExamenes' => json_decode($request->txtJsonExamenes)
            );
            Ima_resultados_programacion::registra($arrayDatos);

            Ima_resultados_programacion::seteaNombreExamen($model->id);
                        
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("programacion_ima");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model')) 
                ->with('arrayDatos', $arrayDatos);  
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelHcl_especialidad = ['' => 'Seleccione! '] + 
                                    Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->pluck('descripcion', 'id')->all();
        $model = Ima_registroprogramacion::find($id);

        $model->txtJsonHcl_poblacion = hcl_poblacion::find($model->idhcl_poblacion)->toJson();
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        $model->examenesProgramados = Ima_examenes_registroprogramacion::where([
                                ['eliminado', '=', 0],
                                ['idima_registroprogramacion', '=', $id]
                            ])->get()->toJson();
        
        $this->seteaDatosComunes($model);
        
        $date = date_create($model->fechaprogramacion);
        $model->fechaprogramacion = date_format($date, 'd-m-Y');

        $modelHcl_cuaderno = Hcl_cuaderno::where('id', '=', $model->idhcl_cuaderno)->first();
        if($modelHcl_cuaderno != null)
            $model->especialidad = $modelHcl_cuaderno->nombre;

        $modelDatosgenericos = Datosgenericos::where('id', '=', $model->iddatosgenericos)->first();
        if($modelDatosgenericos != null)
            $model->medico = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model) 
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $date = date_create($request->fechaprogramacion);
        $fechaprogramacion = date_format($date, 'Y-m-d');

        $model = Ima_registroprogramacion::find($id);

        $model->idhcl_poblacion = $request->txtidhcl_poblacion;
        $model->idhcl_especialidad = $request->idhcl_especialidad;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos;
        $model->hospitalizado = $request->checkHospitalizado == true? 1 : 0;
        $model->fechaprogramacion = $fechaprogramacion;
        $model->horaprogramacion = $request->txtHiddenHora;
        $model->diagnostico = $request->diagnostico;
        $model->observacion = $request->observacion;
        
        // [ DATOS DEL PACIENTE ]
        $Hcl_poblacion = Hcl_poblacion::muestraDatosPoblacion($request);
        $model->hcl_poblacion_nombrecompleto = $Hcl_poblacion['hcl_poblacion_nombrecompleto'];
        $model->hcl_poblacion_matricula = $Hcl_poblacion['hcl_poblacion_matricula'];
        $model->hcl_poblacion_cod = $Hcl_poblacion['hcl_poblacion_cod'];
        $model->hcl_poblacion_sexo = $Hcl_poblacion['hcl_poblacion_sexo'];
        $model->hcl_poblacion_fechanacimiento = $Hcl_poblacion['hcl_poblacion_fechanacimiento'];
        $model->hcl_empleador_empresa = $Hcl_poblacion['hcl_empleador_empresa'];
        $model->hcl_empleador_patronal = $Hcl_poblacion['hcl_empleador_patronal'];
        
        if($model->save())
        {
            Ima_examenes_registroprogramacion::where([
                ['eliminado', '=', 0],
                ['idima_registroprogramacion', '=', $id]
            ])->update(['eliminado' => true]);

            Ima_resultados_programacion::where([
                ['eliminado', '=', 0],
                ['idima_registroprogramacion', '=', $id]
            ])->update(['eliminado' => true]);
            
            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                'idalmacen' => $request->idalmacen,
                'arrayExamenes' => json_decode($request->txtJsonExamenes)
            );
            Ima_resultados_programacion::registra($arrayDatos);

            Ima_resultados_programacion::seteaNombreExamen($model->id);

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("programacion_ima");
    }

    private function seteaDatosComunes($model)
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        $model->ima_examenesEspecialidad = DB::table('ima_examenes as le')
                        ->join('ima_especialidad as e', 'le.idima_especialidad', '=', 'e.id')
                        ->select(
                            'le.id as idima_examenes', 'le.codigo', 'le.nombre',
                            'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                        )
                        ->where([
                            ['le.eliminado', '=', 0],
                            ['le.idalmacen', '=', $modelAlmacenusuario->idalmacen]
                            ])
                        ->orderBy('le.nombre', 'ASC')
                        ->get()->toJson();
        
        $model->especialidadProgramados = DB::table('ima_examenes_registroprogramacion as p')
                        ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                        ->join('ima_especialidad as esp', 'e.idima_especialidad', '=', 'esp.id')
                        ->select('esp.codigo', 'esp.nombre', DB::raw('count(p.idima_examenes) as total'))
                        ->where([
                            ['p.eliminado', '=', 0],  
                            ['e.idalmacen', '=', $modelAlmacenusuario->idalmacen]                           
                            ])
                        ->groupBy('esp.codigo', 'esp.nombre')
                        ->orderBy('esp.nombre', 'ASC')
                        ->get()->toJson();
        $model->hora_actual = date('h:i A');
        $model->horaprogramacion = $model->horaprogramacion == null? $model->hora_actual : $model->horaprogramacion;
        $model->fechaprogramacion = $model->fechaprogramacion == null? date('d-m-Y') : $model->fechaprogramacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaExamenes()
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $buscarTodo = $_POST['buscarTodo'];
            $datosTablaIma = json_decode($_POST['datosLab_Ima']);
            $idsLab = '';
            for($j = 0; $j < count($datosTablaIma); $j++)
            {
                if($j == 0)
                    $idsLab .= $datosTablaIma[$j];
                else
                    $idsLab .= ','. $datosTablaIma[$j];
            }

            $modelIma_variables = DB::table('ima_examenes as le')
                            ->join('ima_especialidad as e', 'le.idima_especialidad', '=', 'e.id')
                            ->select(
                                'le.id as idima_examenes', 'le.codigo', 'le.nombre',
                                'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                            )
                            ->where([
                                ['le.eliminado', '=', 0],
                                ['le.idalmacen', '=', $modelAlmacenusuario->idalmacen]
                            ])
                            ->whereIn($buscarTodo == 0 && $idsLab != ''? 'le.id' : 'le.eliminado', 
                                    array($buscarTodo == 0 && $idsLab != ''? DB::raw("select id from ima_examenes where id in(".$idsLab.")") : 0) )
                            ->where(function($query) use($dato) {
                                $query->orwhere('le.codigo', 'ilike', '%'.$dato.'%');
                                $query->orwhere('le.nombre', 'ilike', '%'.$dato.'%');
                                $query->orwhere('e.codigo', 'ilike', '%'.$dato.'%');
                                $query->orwhere('e.nombre', 'ilike', '%'.$dato.'%');
                            })
                            ->take(config('app.muestraTotalBusqueda'))
                            ->get();
            echo json_encode($modelIma_variables);
        }
    }
    public function buscaProgramados()
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = DB::table('ima_examenes_registroprogramacion as p')
                        ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                        ->select('e.codigo', 'e.nombre', DB::raw('count(p.idima_examenes) as total'))                         
                        ->where([
                            ['p.eliminado', '=', 0],
                             
                            ])
                        ->groupBy('e.codigo', 'e.nombre')
                        ->orderBy('e.nombre', 'ASC')
                        ->get();
            
            echo json_encode($modelo);
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    
    
    
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeProgramacion($id)
    {
        Ima_registroprogramacion::imprimeProgramacion($id);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    
}