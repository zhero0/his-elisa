<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Imagenologia\Ima_cuaderno_datosgenericos;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

class Ima_cuaderno_datosgenericosController extends Controller
{
    private $rutaVista = 'imagenologia.ima_personal.';
    private $controlador = 'ima_cuaderno_datosgenericos';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;

        $nombres = '';
        $apellidos = '';
        $matricula = '';
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $nombres = $datosBuscador->nombres;
            $apellidos = $datosBuscador->apellidos;
            $matricula = $datosBuscador->matricula;
        }
        $model = DB::table('ima_cuaderno_datosgenericos as cd')
                ->join('datosgenericos as d', 'd.id', '=', 'cd.iddatosgenericos')
                ->select('cd.*', 
                            'd.nombres', 'd.apellidos', 'd.ci', 'd.matricula')
                ->where([
                    ['cd.eliminado', '=', 0],
                    ['d.nombres', 'ilike', '%'.$nombres.'%'],
                    ['d.apellidos', 'ilike', '%'.$apellidos.'%'],
                    // ['d.matricula', 'ilike', '%'.$matricula.'%'],
                    // ['cd.idalmacen', '=', $arrayDatos['idalmacen']],
                ])
                ->orderBy('d.nombres', 'ASC')
                ->paginate(config('app.pagination'));
        $model->datosBuscador = $request->datosBuscador;
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Ima_cuaderno_datosgenericos;
        $modelDatosgenericos = Datosgenericos::where('eliminado', '=', 0)                       
                            ->orderBy('nombres','ASC')->get();
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $rules = [            
            'dato' => 'required',  
        ];
        $messages = [
            'dato.required' => 'Se requiere registrar Personal',  
        ];
        $this->validate($request, $rules, $messages);

        $model = new Ima_cuaderno_datosgenericos;
        $model->iddatosgenericos = $request->dato; 
        $model->idalmacen = $request->idalmacen; 
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("personal_ima")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_cuaderno_datosgenericos::find($codigo);  

        if ($model!= null) {
             if($model->delete())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        }

        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return redirect("personal_ima")
                ->with('message', 'update')
                ->with('mensaje', $mensaje); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_cuaderno_datosgenericos::find($id);
        $modelDatosgenericos = Datosgenericos::where('eliminado', '=', 0)              
                            ->orderBy('nombres','ASC')->get();
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Ima_cuaderno_datosgenericos::find($id); 
        $model->iddatosgenericos = $request->dato; 
        $model->idalmacen = $request->idalmacen; 
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("personal_ima")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
