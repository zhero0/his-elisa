<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;

use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Imagenologia\Ima_plantillas;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

class Ima_plantillasController extends Controller
{
    private $rutaVista = 'imagenologia.plantilla.';
    private $controlador = 'ima_plantillas';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;

        $model = Ima_plantillas::join('ima_especialidad', 'ima_especialidad.id', '=', 'ima_plantillas.idima_especialidad')
                    ->join('datosgenericos as dg', 'dg.id', '=', 'ima_plantillas.iddatosgenericos')
                    ->select('ima_plantillas.id','ima_plantillas.nombre as nombre_plantilla', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"),
                            'ima_especialidad.nombre as nombre_especialidad')
                    ->where([
                    ['ima_plantillas.eliminado', '=', 0],
                    ['ima_plantillas.idalmacen', '=', $arrayDatos['idalmacen']],
                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
                ])
                ->where(function($query) use($dato) {                    
                    $query->orwhere('ima_plantillas.nombre', 'ilike', '%'.$dato.'%');                   
                    $query->orwhere('ima_plantillas.usuario', 'ilike', '%'.$dato.'%');
                })
                ->orderBy('ima_plantillas.nombre', 'ASC')
                ->paginate(config('app.pagination'));
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Ima_plantillas;        
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();

        $modelDatosgenericos = DB::table('ima_cuaderno_datosgenericos as cdg')
                        ->join('datosgenericos as dg', 'cdg.iddatosgenericos', '=', 'dg.id')
                        ->select('dg.id', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"))
                        ->where([
                            ['cdg.eliminado', '=', 0],
                            ['cdg.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])
                        ->orderBy('dg.nombres', 'ASC')
                        ->orderBy('dg.apellidos', 'ASC')
                        ->get();

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $rules = [                        
            'nombre' => 'required',
        ];
        $messages = [
            'nombre.required' => 'Se requiere registrar Nombre',            
        ];
        $this->validate($request, $rules, $messages);

        $model = new Ima_plantillas;        
        $model->nombre = strtoupper($request->nombre); 
        $model->idalmacen = $request->idalmacen;
        $model->plantilla = $request->plantilla;
        $model->iddatosgenericos = $request->dato;
        $model->idima_especialidad = $request->especialidad;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("plantilla_i")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_plantillas::find($codigo);
        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();

        $modelDatosgenericos = DB::table('ima_cuaderno_datosgenericos as cdg')
                        ->join('datosgenericos as dg', 'cdg.iddatosgenericos', '=', 'dg.id')
                        ->select('dg.id', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"))
                        ->where([
                            ['cdg.eliminado', '=', 0],
                            ['cdg.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])
                        ->orderBy('dg.nombres', 'ASC')
                        ->orderBy('dg.apellidos', 'ASC')
                        ->get();
        
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_plantillas::find($id); 

        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();

        $modelDatosgenericos = DB::table('ima_cuaderno_datosgenericos as cdg')
                        ->join('datosgenericos as dg', 'cdg.iddatosgenericos', '=', 'dg.id')
                        ->select('dg.id', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"))
                        ->where([
                            ['cdg.eliminado', '=', 0],
                            ['cdg.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])
                        ->orderBy('dg.nombres', 'ASC')
                        ->orderBy('dg.apellidos', 'ASC')
                        ->get();
        $model->accion = $this->controlador;
        $model->rutaview = $this->rutaVista;                


        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Ima_plantillas::find($id); 
        $model->nombre = strtoupper($request->nombre);         
        $model->plantilla = $request->plantilla;
        $model->iddatosgenericos = $request->dato;
        $model->idima_especialidad = $request->especialidad;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("plantilla_i")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
 

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscador_plantillas_imagenologia()
    {
        $arrayDatos = Home::parametrosSistema();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = DB::table('ima_plantillas as pla')
                        ->join('ima_especialidad as esp', 'pla.idima_especialidad', '=', 'esp.id')
                        ->select('pla.id', 'pla.nombre', 'pla.plantilla', 'esp.nombre as especialidad')
                        ->where([
                            ['pla.eliminado', '=', 0],
                            ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
                        ])
                        ->where(function($query) use ($dato) {
                                $query->orwhere('pla.nombre', 'ilike', '%'.$dato.'%');
                            })
                        ->orderBy('pla.nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------


}