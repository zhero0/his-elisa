<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

// use Illuminate\Support\Facades\DB;
use App\Http\Requests;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Imagenologia\Ima_examenes;
use \App\Models\Imagenologia\Ima_pacs;
use \App\Models\Imagenologia\Ima_especialidad;

use \App\Models\Administracion\Datosgenericos; 
use \App\Models\Imagenologia\Ima_tipo_placas;
use \App\Models\Imagenologia\Ima_tamano_placas;
use \App\Models\Imagenologia\Ima_registroprogramacion;
use \App\Models\Imagenologia\Ima_resultados_programacion;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;
use \App\Models\Imagenologia\Ima_estado; 


use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission; 
use \App\Models\Usuario\Almacenusuario; 

use PDF; // at the top of the file
use Elibyy\TCPDF\Facades\TCPDF;
use DB;

class Ima_pacsController extends Controller
{
    private $rutaVista = 'imagenologia.pacs_dicom.';
    private $controlador = 'ima_pacs';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();    

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $dato = $request->search;
        $datosBuscador = $request->datosBuscador;
        
        $modelResultados = Ima_resultados_programacion::where('eliminado', '=', 0)->get();
        $model = Ima_pacs::null;
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelResultados', $modelResultados)
                ->with('arrayDatos', $arrayDatos);
    }
    
    public function escByteA($binData) { 
      /** 
       * \134 = 92 = backslash, \000 = 00 = NULL, \047 = 39 = Single Quote 
       * 
       * str_replace() replaces the searches array in order. Therefore, we must 
       * process the 'backslash' character first. If we process it last, it'll 
       * replace all the escaped backslashes from the other searches that came 
       * before. 
       */ 

      $ddd = pg_unescape_bytea($binData);

      $search = array(chr(92), chr(0), chr(39)); 

      $replace = array('\\\134', '\\\000', '\\\047');
      print_r($search); 
      print_r('1 ------   <br>');
      print_r($replace); 
      print_r('2 ------   <br>');
      print_r($binData); 
      print_r('3 AAAAAAAAAAQUI ------   <br>');

      $binData = str_replace($search, $replace, $binData); 

      print_r($binData); 
      return $ddd; 
      //echo "<pre>$binData</pre>"; 
      //exit; 
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Ima_examenes;
        $modelEspecialidad = Ima_especialidad::all();
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Ima_examenes;
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        $model->idima_especialidad = $request->especialidad;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("examen_i")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_examenes::find($codigo);
        $modelEspecialidad = Ima_especialidad::all();
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_examenes::find($id);
        $modelEspecialidad = Ima_especialidad::all();
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Ima_examenes::find($id); 
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        $model->idima_especialidad = $request->especialidad;
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("examen_i")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function muestraDocumento($id)
    {
        $modelIma_resultados_programacion = Ima_resultados_programacion::where([
                        ['eliminado', '=', 0],
                        ['idpacs', '=', $id]
                    ])->first();
        if ($modelIma_resultados_programacion!=null) {
            $pac = $modelIma_resultados_programacion->idpacs;    
        }else{
            $pac = 'No tiene un informe.. Gracias!!';    
        }
        
        if ($pac>0) 
        {
            $model = Ima_registroprogramacion::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $modelIma_resultados_programacion->idima_registroprogramacion]
                            ])->first(); 
                    
            $numeroImprimir  = $model->numero;
            ob_end_clean();
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            
            $pdf::SetTitle("Imagenologia");
            $pdf::SetSubject("TCPDF");

            $pdf::SetCreator('PDF_CREATOR');
            $pdf::SetAuthor('usuario');
            $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
            
            //cambiar margenes
            $pdf::SetMargins(10, 15, 10, 15);
            $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

            //set auto page breaks
            $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            //$pdf::setAutoPageBreak(true);

            $pdf::startPageGroup();
            
            //set image scale factor
            $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf::setJPEGQuality(100);

            $pdf::setHeaderCallback(function($pdf)use($numeroImprimir) {
                $y = $pdf->GetY();
                $pdf->SetFillColor(230, 230, 230);
                // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

                $pdf->SetY($y + 1);
                $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
                $pdf->SetFont('courier', 'B', 20);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetY($pdf->GetY());
                $x = $pdf->getPageWidth();
                $y = $pdf->getPageHeight();
                // $pdf->MultiCell(140, 12, $x.'======'.$y.' C.N.S.', 0, 'R', '', 0, '', '', 1, '', '', '', 12, 'M');
                $pdf->MultiCell(130, 10, 'IMAGENOLOGIA C.N.S.', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
                $pdf->MultiCell(40, 10, '', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
                $pdf->MultiCell(20, 10, 'N°:'.$numeroImprimir , 0, 'L', '', 1, '', '', 1, '', '', '', 10, 'M');

                // $pdf->MultiCell(140, 12, 'C.N.S.', 0, 'R', '', 1, '', '', 1, '', '', '', 12, 'M');

                $pdf->Line(31, $pdf->getY()+6, $pdf->getPageWidth()-12, $pdf->getY()+6);
            });
            
            // setPage
            $usuario = Auth::user()['name'];
            $fecha = date('d-m-Y');
            
            $pdf::AddPage();

            $pdf::setPage(1, true);
            $pdf::SetFillColor(255, 255, 255);

            $y_Inicio = 30;
            $height = 5;
            $width = 190;
            $widthLabel = 21;
            $widthDosPuntos = 4;
            $widthInformacion = 40;
            $espacio = 3;
            $tipoLetra = 'helvetica';
            $tamanio = 9;
            $widthTitulo = 100;

            $pdf::SetY($y_Inicio);
            // ------------------------------------------ [PACIENTE] ------------------------------------------
            $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                    ->select('hcl_poblacion.*', 
                                        DB::raw("date_part('year',age(fecha_nacimiento)) as edad"))
                                    ->first();
            
            $pdf::SetFont($tipoLetra, 'B', $tamanio);
            $pdf::MultiCell($widthInformacion, $height, 'PACIENTE', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::SetFont($tipoLetra, '', $tamanio);

            $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(25, $height, $modelhcl_poblacion->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell(19, $height, 'NOMBRES: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, $modelhcl_poblacion->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell(21, $height, 'APELLIDOS: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, $modelhcl_poblacion->primer_apellido.' '.$modelhcl_poblacion->segundo_apellido, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell(12, $height, 'EDAD: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, $modelhcl_poblacion->edad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

            // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
            // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
            $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);
            $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
            $modelHcl_especialidad = Hcl_especialidad::find($model->idhcl_especialidad);
            $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
            $modelHcl_cuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
            $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';

            $pdf::MultiCell($widthTitulo, 7, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 7, 10);
            $pdf::SetFont($tipoLetra, 'B', $tamanio);
            $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::SetFont($tipoLetra, '', $tamanio);
            // $anchoHoja = $pdf::getPageWidth();
            // GetStringWidth( $s, $fontname = '', $fontstyle = '', $fontsize = 0, $getarray = false )

            $label_Hospitalizado = 'HOSPITALIZADO';
            $label_Tipo = 'TIPO';
            $label_Medico = 'MÉDICO';
            $label_Observacion = 'OBSERVACIÓN';
            $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
            $y_Tipo = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
            $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
            $y_Observacion = ceil($pdf::GetStringWidth($label_Observacion, $tipoLetra, '', $tamanio, false));
            $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico,  $y_Observacion);

            $label_Fecha = 'FECHA';
            $label_Especialidad = 'ESPECIALIDAD';
            $label_Diagnostico = 'DIAGNÓSTICO';
            $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
            $y_Especialidad = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
            $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
            $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);

            $hospitalizado = $model->hospitalizado == 1? "SI" : "NO";
            $fechaprogramacion = date('d-m-Y', strtotime($model->fechaprogramacion));
            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(48, $height, $hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(45, $height, $fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(12, $height, 'HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $model->horaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            
            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Tipo, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(48, $height, $model->especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, $model->cuaderno, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            
            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(48, $height, $model->personal, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Diagnostico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(45, $height, $model->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Observacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(120, $height, $model->observacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
            // ------------------------------------------ [RESULTADO] ------------------------------------------
            $modelDatosgenericos_Respuesta = Datosgenericos::find($modelIma_resultados_programacion->iddatosgenericos);
            $personal_Medico = $modelDatosgenericos_Respuesta != ''? $modelDatosgenericos_Respuesta->nombres.' '.$modelDatosgenericos_Respuesta->apellidos : '';
            $pdf::MultiCell(120, $height, 'RESULTADO:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 260, 20);
                 $inferior = 267;
            $pdf::setY($pdf::getY()+13);

            $pdf::SetY(268);
            $pdf::SetFont('courier', 'B', 7);
            $pdf::MultiCell('', '','Usuario: '.$usuario, 'T', 'L', 0);
            $pdf::SetY(268);
            $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf::SetY(268);
            $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
                    
            // set javascript
            // $pdf->IncludeJS('print(true);');

            $pdf::Output("resultadoProgramacion.pdf", "I");
            mb_internal_encoding('utf-8');
        }
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaDicom()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Ima_pacs::archivosPacsMedico($dato, 0);
            echo $modelo->toJson();
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
