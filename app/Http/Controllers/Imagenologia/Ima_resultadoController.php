<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Almacen\Generico;
use \App\Models\Almacen\Almacen;
use \App\Models\Almacen\Home;

use \App\Models\Laboratorio\Lab_variables;
use \App\Models\Laboratorio\Lab_examenes;
use \App\Models\Laboratorio\Lab_lista;
use \App\Models\Laboratorio\Lab_tipo_muestra;
use \App\Models\Laboratorio\Lab_tipo_variables;
use \App\Models\Laboratorio\Lab_registroprogramacion;
use \App\Models\Laboratorio\Lab_estado;
use \App\Models\Laboratorio\Lab_examenes_registroprogramacion;

use \App\Models\Administracion\Datosgenericos;
use \App\Models\Administracion\Impresion;
use \App\Models\Imagenologia\Ima_pacs;
use \App\Models\Imagenologia\Ima_tipo_placas;
use \App\Models\Imagenologia\Ima_tamano_placas;
use \App\Models\Imagenologia\Ima_registroprogramacion;
use \App\Models\Imagenologia\Ima_resultados_programacion;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;
use \App\Models\Imagenologia\Ima_estado;
use \App\Models\Imagenologia\Ima_examenes;
use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Imagenologia\Ima_cuaderno_datosgenericos;
use \App\Models\Imagenologia\Ima_detalleplacasutilizadas;
use \App\Models\Imagenologia\Ima_resultados_documento;

use PDF; // at the top of the file
use Elibyy\TCPDF\Facades\TCPDF;
use Session;
use Illuminate\Support\Facades\URL;

class Ima_resultadoController extends Controller
{
    // private $rutaVista = 'imagenologia.ima_resultado.';
    private $rutaVista = 'Imagenologia.ima_resultado.'; // ????????
    private $controlador = 'ima_resultado';
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();


        // *****************************************************************************
        // $modelo = Ima_resultados_programacion::where([
        //                 ['eliminado', '=', 0],
        //                 ['examenes_programados', '=', null]
        //             ])->orderBy('idima_registroprogramacion', 'ASC')->limit(7000)->get();
        // ini_set('max_execution_time', 30000);
        // if(count($modelo) > 0)
        // {
        //     for($i = 0; $i < count($modelo); $i++)
        //     {
        //         Ima_resultados_programacion::seteaNombreExamen($modelo[$i]->idima_registroprogramacion);
        //     }
        //     echo "ok...";
        // }
        // else
        //     echo "finalizado...";
        // return;
        // *****************************************************************************

        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");
        $buscarFecha = $request->buscarFecha;
        $date = date_create($request->fechaRegistro);
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado;
        $modelo = DB::table('ima_examenes_registroprogramacion as p')
                        ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                        ->join('ima_registroprogramacion as a', 'p.idima_registroprogramacion', '=', 'a.id')
                        ->join('ima_especialidad as i', 'e.idima_especialidad', '=', 'i.id')
                        ->select('i.codigo', 'i.nombre', DB::raw('count(a.id) as total'))                         
                        ->where([
                            ['a.eliminado', '=', 0],
                            ['p.eliminado', '=', 0],
                            ['a.idalmacen', '=', $arrayDatos['idalmacen'],]
                            ])
                        ->where(function($query) use($fechaRegistro, $buscarFecha) { 
                        $query->where([ 
                            ['a.fechaprogramacion', $buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro ], ]); 
                        })
                        ->groupBy('i.codigo', 'i.nombre')
                        ->orderBy('i.nombre', 'ASC')
                        ->get();

        $dato = $request->searchNuevo;
        $idima_especialidad = $request->especialidad;

        $paginacion = config('app.pagination');
        $numero = '';
        $numeroHistoria = '';
        $asegurado = '';
        $idimaEspecialidad = null;
        $examen = '';
        $especialidad = '';
        $fecha = null;
        $estado = null;
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $paginacion = $datosBuscador->paginacion;
            $numero = $datosBuscador->numero > 0? $datosBuscador->numero : '';
            $numeroHistoria = $datosBuscador->numeroHistoria;
            $asegurado = $datosBuscador->asegurado;
            $idimaEspecialidad = $datosBuscador->idimaEspecialidad > 0? $datosBuscador->idimaEspecialidad : null;
            $examen = $datosBuscador->examen;
            $especialidad = $datosBuscador->especialidad;
            $fecha = $datosBuscador->fecha == ''? null : $datosBuscador->fecha;
            $estado = $datosBuscador->estado > 0? $datosBuscador->estado : null;
        }
        $model = DB::table('ima_resultados_view')
                    ->where([
                        ['rp_idalmacen', '=', $arrayDatos['idalmacen']],
                        [DB::raw('numero::text'), 'ilike', '%'.$numero.'%'],
                        ['numero_historia', 'ilike', '%'.$numeroHistoria.'%'],
                        ['hcl_poblacion_nombrecompleto', 'ilike', '%'.$asegurado.'%'],
                        ['result_especialidad', $idimaEspecialidad > 0? '=' : '!=', $idimaEspecialidad],
                        ['nombre_examen', 'ilike', '%'.$examen.'%'],
                        ['cuaderno', 'ilike', '%'.$especialidad.'%'],
                        [DB::raw('fechaprogramacion::date'), $fecha == ''? '!=' : '=', $fecha],
                        ['idima_estado', $estado > 0? '=' : '!=', $estado],
                    ])
                    ->orderBy('numero', 'DESC')
                    ->paginate($paginacion);
        $model->datosBuscador = $request->datosBuscador;
        
        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();
        
        $model->scenario = 'index';
        $model->fechaRegistro = $fechaRegistro;
        $model->searchNuevo = $dato;
        $model->idima_especialidad = $idima_especialidad;
        $model->buscarFecha = $buscarFecha;
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        $modelIma_estado = Ima_estado::orderBy('nombre', 'ASC')->get();

        // imprimir resultado
        $model->datos_imprimir = '';
        if (Session::has('datos_imprimir'))
            $model->datos_imprimir = Session::get('datos_imprimir'); 

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelo', $modelo)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('modelIma_estado', $modelIma_estado)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelTipoVariables = Lab_tipo_variables::all();
        $model = Lab_variables::find($codigo);

        $modelLab_lista = Lab_lista::where('id_lab_variables', '=', $model->id)->get();
        // foreach ($modelLab_lista as $value) {
        //     print_r($value['lis_variable']);    
        // }
        
        return;
        $model->listavariable =  json_encode($modelLab_lista);
        
       
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelTipoVariables', $modelTipoVariables) 
                ->with('arrayDatos', $arrayDatos);  
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $idalmacen = $arrayDatos['idalmacen'];
        $iddatosgenericos = $arrayDatos['iddatosgenericos'];

        $modelIma_cuaderno_datosgenericos = DB::table('ima_cuaderno_datosgenericos as cdg')
                        ->join('datosgenericos as dg', 'cdg.iddatosgenericos', '=', 'dg.id')
                        ->select('dg.id', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"))
                        ->where([
                            ['cdg.eliminado', '=', 0],
                            ['cdg.idalmacen', '=', $idalmacen]
                        ])
                        ->orderBy('dg.nombres', 'ASC')
                        ->orderBy('dg.apellidos', 'ASC')
                        ->get();
        
        $model = Ima_resultados_programacion::find($id);
        $modelIma_registroprogramacion = Ima_registroprogramacion::find($model->idima_registroprogramacion);
        
        $model->txtJsonHcl_poblacion = Hcl_poblacion::select('id','numero_historia','nombre', 'primer_apellido','segundo_apellido', 
            DB::raw("(SELECT date_part('year'::text, age(fecha_nacimiento::date)))::integer as edad"))
                        ->where([
                            ['eliminado', '=', 0],
                            ['id', '=', $modelIma_registroprogramacion->idhcl_poblacion]
                        ])->first()->toJson();
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        $this->seteaDatosComunes($model, $modelIma_registroprogramacion);
        
        $modelIma_tipo_placas = Ima_tipo_placas::where('idalmacen', '=', $idalmacen)->orderBy('nombre', 'ASC')->get();
        $model->idalmacen = $idalmacen;
        
        $modelHcl_especialidad = Hcl_especialidad::find($modelIma_registroprogramacion->idhcl_especialidad);
        $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
        $modelDatosgenericos = Datosgenericos::find($modelIma_registroprogramacion->iddatosgenericos);
        $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
        $modelHcl_cuaderno = Hcl_cuaderno::find($modelIma_registroprogramacion->idhcl_cuaderno);
        $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';
        $date = date_create($modelIma_registroprogramacion->fechaprogramacion);
        $model->fechaprogramacion = date_format($date, 'd-m-Y');
        
        if($model->fecha_resultado != null) {
            $date = date_create($model->fecha_resultado);
            $model->fecha_resultado = date_format($date, 'd-m-Y');
        }
        
        $model->horaprogramacion = $modelIma_registroprogramacion->horaprogramacion;
        $model->diagnostico = $modelIma_registroprogramacion->diagnostico;
        $model->observacion = $modelIma_registroprogramacion->observacion;
        
        $idpacs = $model->idpacs > 0? $model->idpacs : 0;
        $model->txtJsonIma_pacs = Ima_pacs::archivosPacs('', $idpacs)->toJson();
        
        $modelIma_resultados_documento = Ima_resultados_documento::where([
                                            ['eliminado', '=', 0],
                                            ['idima_resultados_programacion', '=', $id]
                                        ])->get();
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelIma_tipo_placas', $modelIma_tipo_placas)
                ->with('modelIma_cuaderno_datosgenericos', $modelIma_cuaderno_datosgenericos)
                ->with('modelIma_resultados_documento', $modelIma_resultados_documento)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idalmacen = $request->idalmacen;
        $iddatosgenericos = $request->iddatosgenericos;
        $date = date_create($request->fecha_resultado);
        $fecha_resultado = date_format($date, 'Y-m-d');
        $fecha_resultado_sistema = date("Y").'-'.date("m").'-'.date("d");

        $model = Ima_resultados_programacion::find($id);
        $model->idalmacen = $idalmacen;
        $iddatosgenericos = $request->iddatosgenericos_responsable > 0? 
                                        $request->iddatosgenericos_responsable : $iddatosgenericos;
        $model->iddatosgenericos = $iddatosgenericos;
        $model->numero_placa = $request->numero_placa; // '1-77' nroCorrelativo-NroPlaca
        /*
        $model->idima_tipo_placas = $request->idima_tipo_placas > 0? $request->idima_tipo_placas : null;
        $model->idima_tamano_placas = $request->idima_tamano_placas > 0? $request->idima_tamano_placas : null;
        */
        $model->respuesta = $request->respuesta;
        $model->idpacs = $request->txtHiddenIdpacs;
        $model->fecha_resultado = $fecha_resultado = ''? $fecha_resultado_sistema : $fecha_resultado;
        $model->idima_estado = Ima_estado::ENTREGADO;
        $model->patologico = $request->patologico;
        $model->usuario = Auth::user()['name'];
        $datos_imprimir = $model->hcl_poblacion_nombrecompleto.'*|*'.$id;
        
        if($model->save())
        {
            $modelo = Ima_registroprogramacion::find($model->idima_registroprogramacion);
            $modelo->idima_estado = Ima_estado::ENTREGADO;
            $modelo->save();

            Ima_detalleplacasutilizadas::where('idima_resultados_programacion', '=', $model->id)->delete();
            $jsonDetalleplacasutilizadas = json_decode($request->txtHidden_detalleplacasutilizadas);
            if($jsonDetalleplacasutilizadas != '')
            {
                for($i = 0; $i < count($jsonDetalleplacasutilizadas); $i++)
                {
                    $modelo = new Ima_detalleplacasutilizadas;
                    $modelo->idima_resultados_programacion = $model->id;
                    $modelo->idima_tipo_placas = $jsonDetalleplacasutilizadas[$i]->idima_tipo_placas;
                    $modelo->ima_tipo_placas = $jsonDetalleplacasutilizadas[$i]->ima_tipo_placas;
                    $modelo->idima_tamano_placas = $jsonDetalleplacasutilizadas[$i]->idima_tamano_placas;
                    $modelo->ima_tamano_placas = $jsonDetalleplacasutilizadas[$i]->ima_tamano_placas;
                    $modelo->cantidad = $jsonDetalleplacasutilizadas[$i]->cantidad;
                    $modelo->usuario = Auth::user()['name'];
                    $modelo->save();
                }
            }

            // Copia los archivos al sistema y almacena el detalle en una tabla de la bd
            $files = $request->file('archivo');
            if($request->hasFile('archivo'))
            {
                $arrayParametro = array(
                    'files' => $files,
                    'id' => $id,
                );
                Ima_resultados_documento::registraArchivos($arrayParametro);
            }
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje)
                            ->with('datos_imprimir', $datos_imprimir);
        }
        else
            return redirect("resultado");
    }
    
    private function seteaDatosComunes($model, $modelIma_registroprogramacion)
    {
        $model->lab_examenesEspecialidad = DB::table('ima_examenes_registroprogramacion as erp')
                    ->join('ima_examenes as le', 'le.id', '=', 'erp.idima_examenes')
                    ->join('ima_especialidad as e', 'le.idima_especialidad', '=', 'e.id')
                    ->select(
                        'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                        'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                    )
                    ->where([
                        ['le.eliminado', '=', 0],
                        ['erp.eliminado', '=', 0],
                        ['erp.idima_registroprogramacion', '=', $modelIma_registroprogramacion->id],
                        ['e.id', '=', $model->idima_especialidad]
                    ])
                    ->orderBy('le.nombre', 'ASC')
                    ->get()->toJson();
        $model->Ima_detalleplacasutilizadas = Ima_detalleplacasutilizadas::
                                where('idima_resultados_programacion', '=', $model->id)
                                ->orderBy('ima_tipo_placas', 'ASC')
                                ->get()->toJson();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaIma_tamano_placas()
    {
        if(isset($_POST['dato']))
        {
            $idima_tipo_placa = $_POST['dato'];
            $modelo = Ima_tamano_placas::where('idima_tipo_placa', '=', $idima_tipo_placa)->get()->toJson();
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeProgramacionResultado($id)
    {
        Ima_resultados_programacion::imprimeProgramacionResultado($id);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}