<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Imagenologia\Ima_tipo_placas;
use \App\Models\Imagenologia\Ima_tamano_placas;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Ima_tamano_placasController extends Controller
{
    private $rutaVista = 'imagenologia.tamano_placa.';
    private $controlador = 'ima_tamano_placas';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $model = DB::table('ima_tamano_placas as tp')
                    ->join('ima_tipo_placas as tpla', 'tpla.id', '=', 'tp.idima_tipo_placa') 
                ->select('tp.*',
                        'tpla.nombre')
                ->where([
                    ['tp.eliminado', '=', 0],
                    ['tp.idalmacen', '=', $arrayDatos['idalmacen']]
                ])
                ->where(function($query) use($dato) {
                    $query->orwhere('tp.codigo', 'like', '%'.$dato.'%');
                    $query->orwhere('tp.tamanio_placa', 'like', '%'.$dato.'%');  
                })
                ->orderBy('tpla.nombre', 'ASC')
                ->paginate(config('app.pagination'));

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Ima_tamano_placas;
        $modelTipo_placa = Ima_tipo_placas::where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']]
                ])->get();
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelTipo_placa', $modelTipo_placa)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $rules = [            
            'codigo' => 'required', 
            'tamanio_placa' => 'required',
            'tipo' => 'required',
        ];
        $messages = [
            'codigo.required' => 'Se requiere registrar Codigo', 
            'tamanio_placa.required' => 'Se requiere registrar Tamaño de placa',            
            'tipo.required' => 'Se requiere registrar Tipo de placa',
        ];
        $this->validate($request, $rules, $messages);
        
        $model = new Ima_tamano_placas;
        $model->codigo = strtoupper($request->codigo);
        $model->tamanio_placa = strtoupper($request->tamanio_placa); 
        $model->idalmacen = $request->idalmacen;        
        $model->idima_tipo_placa = $request->tipo;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("tamano_placa");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_tamano_placas::find($codigo);
        $modelTipo_placa = Ima_tipo_placas::where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']]
                ])->get();
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelTipo_placa', $modelTipo_placa)
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_tamano_placas::find($id);
        $modelTipo_placa = Ima_tipo_placas::where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']]
                ])->get();
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelTipo_placa', $modelTipo_placa)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Ima_tamano_placas::find($id); 
        $model->codigo = strtoupper($request->codigo);
        $model->tamanio_placa = strtoupper($request->tamanio_placa);  
        $model->idima_tipo_placa = $request->tipo;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("tamano_placa"); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
