<?php
namespace App\Http\Controllers\Imagenologia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Imagenologia\Ima_examenes;
use \App\Models\Imagenologia\Ima_especialidad;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission; 
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

class Ima_examenesController extends Controller
{
    private $rutaVista = 'imagenologia.examen.';
    private $controlador = 'ima_examenes';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema(); 

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $modeloIma_especialidad = Ima_especialidad::where([
                            ['eliminado', '=', 0],
                            ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->orderBy('nombre', 'ASC')->get();

        $codigo = '';
        $nombre = '';
        $descripcion = '';
        $especialidad = null;
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $codigo = $datosBuscador->codigo;
            $nombre = $datosBuscador->nombre;
            $descripcion = $datosBuscador->descripcion;
            $especialidad = $datosBuscador->especialidad > 0? $datosBuscador->especialidad : null;
        }
        $model = DB::table('ima_examenes as imaex')
                    ->join('ima_especialidad as imaesp', 'imaex.idima_especialidad', '=', 'imaesp.id')
                    ->select('imaex.*', 'imaesp.nombre as especialidad')
                    ->where([
                            ['imaex.eliminado', '=', 0],
                            ['imaex.idalmacen', '=', $arrayDatos['idalmacen']],
                            ['imaex.codigo', 'ilike', '%'.$codigo.'%'],
                            ['imaex.nombre', 'ilike', '%'.$nombre.'%'],
                            ['imaex.descripcion', 'ilike', '%'.$descripcion.'%'],
                            ['imaex.idima_especialidad', $especialidad > 0? '=' : '!=', $especialidad],
                        ])
                    ->orderBy('imaex.nombre', 'ASC')
                    ->paginate(config('app.pagination'));
        $model->datosBuscador = $request->datosBuscador;

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modeloIma_especialidad', $modeloIma_especialidad)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Ima_examenes;
        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    { 
        $rules = [            
            'codigo' => 'required', 
            'nombre' => 'required', 
        ];
        $messages = [
            'codigo.required' => 'Se requiere registrar Codigo', 
            'nombre.required' => 'Se requiere registrar Nombre',             
        ];
        $this->validate($request, $rules, $messages);
 
        $model = new Ima_examenes;
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        $model->idima_especialidad = $request->especialidad;
        $model->idalmacen = $request->idalmacen;; 
        $model->usuario = Auth::user()['name'];
        
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("examen_i")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_examenes::find($codigo);
        $modelEspecialidad = Ima_especialidad::all();
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_examenes::find($id);
        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Ima_examenes::find($id); 
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        $model->idima_especialidad = $request->especialidad;
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("examen_i")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
