<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen; 
use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno; 
use \App\Models\Afiliacion\Hcl_fichas_registro; 
use \App\Models\Afiliacion\Hcl_ficha_estado; 
use \App\Models\Afiliacion\Hcl_fichas_registradas; 
use \App\Models\Afiliacion\Hcl_fichas_programadas; 
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

class Hcl_fichas_registradasController extends Controller
{
    private $rutaVista = 'Salud.hcl_fichas_registradas.';
    private $controlador = 'hcl_fichas_registradas';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $dato = $request->search;
  
        $model = Hcl_fichas_registro::join('hcl_cuaderno as b','hcl_fichas_registro.idhcl_cuaderno','=','b.id')
                ->join('datosgenericos as c','hcl_fichas_registro.iddatosgenericos','=','c.id')
                ->select('hcl_fichas_registro.*', 'b.sigla', 'b.nombre', 'c.nombres', 'c.apellidos') 
                ->where([
                    ['hcl_fichas_registro.eliminado', '=', 0]
                ]) ->get();   
        
        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador); 
        $model = new Hcl_fichas_registro;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                //->with('modelSucursales', $modelSucursales)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);

        $model = new hcl_cuaderno;
        $model->sigla = strtoupper($request->sigla);
        $model->id_especialidad = $request->id_especialidad; 
        $model->idfar_sucursales = $request->idfar_sucursales; 
        $model->nombre = strtoupper($request->nombre);
        $model->escala_tiempo =  '00:'.$request->escala_tiempo; 
        $model->fichas_defecto = 'SI';
        $model->transferencia = 'SI';
        $model->hora_inicio = $request->hora_inicio;
        $model->hora_fin = $request->hora_fin;
        $model->nivel_seguridad =  $txtHiddenJSONdatos->seguridad;
        $model->formato_historia = $request->formato_historia; 
        $model->idalmacen = $request->idalmacen;
        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("cuaderno")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Hcl_fichas_registradas::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador); 

        $model = Hcl_fichas_registro::find($id);
        $modelDatos = Datosgenericos::find($model->iddatosgenericos);
        $model->medico_titulo =  $modelDatos->titulo.' '.$modelDatos->apellidos.' '.$modelDatos->nombres;
        $modelCuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
        $model->especialidad_titulo =  $modelCuaderno->nombre;

        $model->dateHoy = date('Y-m-d'); 
        $model->fecha_actual = date('d-m-Y');  
        // $model->fecha_actual = '30-08-2022';  

        $modelFichas = DB::table(DB::raw("hcl_lista_programada(".$model->id.",'".$model->fecha_actual."')"))->get();  
        $model->fichas_disponibles = count($modelFichas);
         
        $model->scenario = 'edit';
        $model->accion = $this->controlador;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)  
                ->with('modelFichas', $modelFichas)  
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);

        $model = Hcl_cuaderno::find($id); 
        $model->sigla = strtoupper($request->sigla);
        $model->idfar_sucursales = $request->idfar_sucursales; 
        $model->id_especialidad = $request->id_especialidad; 
        $model->nombre = strtoupper($request->nombre);
        $model->escala_tiempo =  $request->escala_tiempo; 
        $model->fichas_defecto = 'SI';
        $model->transferencia = 'SI'; 
        $model->hora_inicio = $request->hora_inicio;
        $model->hora_fin = $request->hora_fin;
        $model->nivel_seguridad =  $txtHiddenJSONdatos->seguridad;
        $model->formato_historia = $request->formato_historia; 

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("cuaderno")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function busca_Especialidad()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Hcl_cuaderno::where([
                            ['eliminado', '=', 0],
                            ['nombre', 'ilike', '%'.$dato.'%']
                        ])
                        ->orderBy('nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }
    public function muestraEspecialidad()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];  
            $modelo = Hcl_cuaderno::where([
                            ['eliminado', '=', 0], 
                            ['idalmacen', '=', $dato]
                        ])                        
                        ->orderBy('nombre', 'ASC') 
                        ->get()->toJson();
            echo $modelo;
        }
    }

    public function registraAtencion()
    {
        print_r($_POST['idhcl_fichas_programadas']);
        return;
        if(isset($_POST['idhcl_fichas_programadas']) && isset($_POST['idhcl_cuaderno_fic']) && isset($_POST['iddatosgenericos_fic']) && isset($_POST['idhcl_poblacion_fic']))
        {   
            $arrayDatos = Home::parametrosSistema();
            $idhcl_fichas_programadas = $_POST['idhcl_fichas_programadas'];  
            $idhcl_cuaderno = $_POST['idhcl_cuaderno_fic'];  
            $iddatosgenericos = $_POST['iddatosgenericos_fic'];  
            $idhcl_poblacion = $_POST['idhcl_poblacion_fic']; 
 
            $modelProgramado = Hcl_fichas_programadas::find($idhcl_fichas_programadas); 
            $modelProgramado->idhcl_ficha_estado = Hcl_ficha_estado::RESERVADO;
            $modelProgramado->save();

            $model = new Hcl_fichas_registradas;
            $model->idhcl_fichas_programadas = $modelProgramado->id;
            $model->numero = $modelProgramado->numero;
            $model->fecha_ficha = $modelProgramado->fecha_registro;
            $model->hora = $modelProgramado->hora_registro;
            $model->idhcl_poblacion = $idhcl_poblacion;
            $model->idhcl_cuaderno = $idhcl_cuaderno;
            $model->iddatosgenericos = $iddatosgenericos;
            $model->iddatosgenericos_registro = $arrayDatos['iddatosgenericos'];
            $model->idhcl_ficha_estado = Hcl_ficha_estado::RESERVADO;
            $model->usuario = Auth::user()['name'];
            $model->save(); 
        }
    }

    public function eliminarAtencion()
    {
        if(isset($_POST['idhcl_fichas_programadas']))
        {
            $idhcl_fichas_programadas = $_POST['idhcl_fichas_programadas']; 


            $model = Hcl_fichas_registradas::where([
                                        ['eliminado', '=', 0],
                                        ['idhcl_fichas_programadas', '=', $idhcl_fichas_programadas]
                                    ])->first(); 
            $model->delete();

            $modelProgramado = Hcl_fichas_programadas::find($idhcl_fichas_programadas); 
            $modelProgramado->idhcl_ficha_estado = Hcl_ficha_estado::LIBRE;
            $modelProgramado->save();
        }
    }

    public function reporteFicha($valores)
    {       
        $parametro = json_decode($valores);          
        Hcl_fichas_programadas::imprime_Ticket_mdt($parametro); 
    }

    public function reporteMedicina($valores)
    {       
        $parametro = json_decode($valores);          
        Hcl_fichas_programadas::imprime_Estudios($parametro); 
    }

    public function reporteLista($valores)
    {       
        $parametro = json_decode($valores);   
        
        Hcl_fichas_programadas::imprimirLista_atencion($parametro);
        // Med_reporte::imprime_Solicitud($parametro); 
    }

    public function buscaPaciente_ficha($id)
    { 
        if(isset($_POST['documento'])||isset($_POST['paterno'])||isset($_POST['materno'])||isset($_POST['nombres']) || isset($_POST['fecha']) && isset($_POST['opcionBusqueda']))
        {
            $documento = $_POST['opcionBusqueda'] == 2? $_POST['documento'] : ''; 
            $paterno = $_POST['opcionBusqueda'] == 2? $_POST['paterno'] : ''; 
            $materno = $_POST['opcionBusqueda'] == 2? $_POST['materno'] : ''; 
            $nombres = $_POST['opcionBusqueda'] == 2? $_POST['nombres'] : ''; 
            $fecha = $_POST['opcionBusqueda'] == 1? $_POST['fecha'] : ''; 
            $iddatosgenericos = $_POST['iddatosgenericos']; 
            $idhcl_cuaderno = $_POST['idhcl_cuaderno']; 
            

            $dato = ''; 

            if ($documento != '' ) { 

                $dato = "c.documento::text ilike '%".$documento."%'"; 
            } 
            if ($paterno != '') {  
                if ($dato != '') {
                    $dato  = $dato." and c.primer_apellido::text ilike '%".$paterno."%'";  
                }else
                    $dato  = "c.primer_apellido::text ilike '%".$paterno."%'";                  
            }  
            if ($materno != '') {
                if ($dato != '') {
                    $dato  = $dato." and c.segundo_apellido::text ilike '%".$materno."%'";  
                }else  
                    $dato  = "c.segundo_apellido::text ilike '%".$materno."%'";    
            }  
            if ($nombres != '') {
                if ($dato != '') {
                    $dato  = $dato." and c.nombre::text ilike '%".$nombres."%'";   
                }else
                    $dato  = "c.nombre::text ilike '%".$nombres."%'";   
            } 

            if ( $_POST['opcionBusqueda'] == 2) {                
                $modelo = Hcl_fichas_registradas::buscarDato($dato, $idhcl_cuaderno, $iddatosgenericos); 
            }
            if ( $_POST['opcionBusqueda'] == 1) {                
                $modelo = DB::table(DB::raw("hcl_lista_programada(".$id.",'".$fecha."')"))->get();                  
            } 
            echo json_encode($modelo);
        }
    }

    public function buscaPaciente_post()
    { 
        $paterno = $_POST['paterno']; 
        $materno = $_POST['materno']; 
        $nombres = $_POST['nombres']; 
        
        $dato = ''; 
 
        if ($paterno != '') {   
                $dato  = "a.primer_apellido::text ilike '%".$paterno."%'";  
                            
        }  
        if ($materno != '') {
            if ($dato != '') {
                $dato  = $dato." and a.segundo_apellido::text ilike '%".$materno."%'";  
            }else  
                $dato  = "a.segundo_apellido::text ilike '%".$materno."%'";    
        }  
        if ($nombres != '') {
            if ($dato != '') {
                $dato  = $dato." and a.nombre::text ilike '%".$nombres."%'";   
            }else
                $dato  = "a.nombre::text ilike '%".$nombres."%'";   
        } 

        $modelo = DB::select(DB::raw('select a.id as res_idhcl_poblacion, a.numero_historia as res_numero_historia, a.nombre as res_nombre, a.primer_apellido as res_primer_apellido, a.segundo_apellido as res_segundo_apellido, a.sexo as res_sexo, a.fecha_nacimiento as res_fecha_nacimiento, age(a.fecha_nacimiento) as res_edad, c.id as es_idhcl_empleador, c.nro_empleador as res_nro_empleador, c.nombre as res_nombre_empleador,a.documento as res_documento from hcl_poblacion a inner join hcl_poblacion_afiliacion b on a.id = b.idpoblacion inner join hcl_empleador c on c.id = b.idempleador where '.$dato.' order by a.primer_apellido desc')); 

        echo json_encode($modelo); 
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}