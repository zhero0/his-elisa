<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Afiliacion\Hcl_afiliacion_parentesco;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion_afiliacion;
use \App\Models\Afiliacion\Hcl_cobertura;
use \App\Models\Afiliacion\Hcl_cie;

use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use Illuminate\Support\Facades\DB;
use \App\Permission;

class Hcl_cieController extends Controller
{
    private $rutaVista = 'afiliacion.hcl_cie.';
    private $controlador = 'hcl_cie';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
 
         
        $dato = $request->search;
        $model = Hcl_poblacion::join('hcl_poblacion_afiliacion as b','b.idpoblacion','=','hcl_poblacion.id')
        ->join('hcl_empleador as c','c.id','=','b.idempleador')
        ->join('hcl_afiliacion_parentesco as d','d.id','=','b.idparentesco')
        ->select('hcl_poblacion.*','c.nombre  as nombre_empresa','c.nro_empleador','d.descripcion') 
        ->where([
            ['hcl_poblacion.eliminado', '=', 0]
        ])
        ->where(function($query) use($dato) {
            $query->orwhere('hcl_poblacion.numero_historia', 'like', '%'.$dato.'%');
            $query->orwhere('hcl_poblacion.codigo', 'like', '%'.$dato.'%'); 
            $query->orwhere('hcl_poblacion.nombre', 'like', '%'.$dato.'%');
            $query->orwhere('hcl_poblacion.primer_apellido', 'like', '%'.$dato.'%');
            $query->orwhere('hcl_poblacion.segundo_apellido', 'like', '%'.$dato.'%') ; 
        })
        
        ->orderBy('hcl_poblacion.primer_apellido', 'ASC')
        ->paginate(config('app.pagination'));

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Hcl_poblacion;
        $modelParentesco = Hcl_afiliacion_parentesco::all();
        $modelAfiliacion = null;
        $modelCuaderno = Hcl_cuaderno::all();
        $modelCobertura = Hcl_cobertura::all();
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelParentesco', $modelParentesco)
                ->with('departamentos', $departamentos)
                ->with('modelCuaderno', $modelCuaderno)
                ->with('modelAfiliacion', $modelAfiliacion)
                ->with('modelCobertura', $modelCobertura)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        //$x = json_decode($request->txtListaPoblacion);
       // $x = $request->txtId_empleador;
        // print_r($x );
        // return;

        $ultimo_registro =  DB::table('hcl_poblacion_afiliacion') 
        ->selectraw('max(hcl_poblacion_afiliacion.idasegurado) as idasegurado')           
        ->get();
        $codigo_parentesco =  DB::table('hcl_afiliacion_parentesco') 
        ->selectraw('hcl_afiliacion_parentesco.codificacion as paren')
        ->where('hcl_afiliacion_parentesco.id','=',$request->parentesco)           
        ->get();
        // $ultimo_registro = $ultimo_registro;
        // print_r($ultimo_registro[0]->idasegurado+1);
        // return;

        $model = new Hcl_poblacion;
        $model->numero_historia = strtoupper($request->numero_historia);
        $model->codigo = strtoupper($request->numero_historia.'-'.$codigo_parentesco[0]->paren);
        $model->primer_apellido = strtoupper($request->primer_apellido); 
        $model->segundo_apellido = strtoupper($request->segundo_apellido);
        $model->nombre = strtoupper($request->nombre); 
        $model->documento = strtoupper($request->documento);
        $model->sexo = $request->optradio;
        $model->fecha_nacimiento = $request->fecha_nacimiento;  
        $model->id_departamento = 0;
        $model->id_provincia = 0;
        $model->id_municipio = 0;
        $model->id_localidad = $request->departamento;
        $model->observaciones = strtoupper($request->comentarios);
        $model->usuario = Auth::user()['name']; 
        if($model->save()){
            $model_afiliacion = new Hcl_poblacion_afiliacion;
            $model_afiliacion->idpoblacion = $model->id; 
            if ($request->parentesco == 29) {
                $model_afiliacion->idtipopaciente = 1;
            }else{
                $model_afiliacion->idtipopaciente = 2;
            }
            $model_afiliacion->idcobertura = $request->cobertura;
            $model_afiliacion->idAsegurado = $ultimo_registro[0]->idasegurado+1;
            $model_afiliacion->idparentesco = $request->parentesco;
            $model_afiliacion->idestadoafiliacion = 1;
            $model_afiliacion->idcuaderno = $request->cuaderno;
            $model_afiliacion->fin_cobertura = '2018-01-01';
            $model_afiliacion->fecha_afiliacion = '2018-01-01';
            $model_afiliacion->fecha_Registro = '2018-01-01';
            $model_afiliacion->idempleador = $request->txtId_empleador;
            $model_afiliacion->usuario = Auth::user()['name'];
            $model_afiliacion->save();
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("poblacion")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);


        $model = Hcl_poblacion::find($codigo);
        $model->fecha_nacimiento = date("Y-m-d", strtotime($model->fecha_nacimiento));
        //print_r($model->fecha_nacimiento);
        //return;
        $modelParentesco = Hcl_afiliacion_parentesco::all();
        $modelAfiliacion = Hcl_poblacion_afiliacion::where('idpoblacion', '=', $codigo)->first(); 
        $modelCuaderno = Hcl_cuaderno::all();
        $modelCobertura = Hcl_cobertura::all();
        $departamentos = DB::table('hcl_departamento') 
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
        $modelEmpresas = Hcl_empleador::where('id', '=', $modelAfiliacion->idempleador)->first()->toJson();
        $model->fecha_nacimiento = date("Y-m-d", strtotime($model->fecha_nacimiento));

        $model->txtListaPoblacion = $modelEmpresas;// json_encode($modelLab_empresas);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('modelParentesco', $modelParentesco)
                ->with('departamentos', $departamentos)
                ->with('modelCuaderno', $modelCuaderno)
                ->with('modelAfiliacion', $modelAfiliacion)
                ->with('modelCobertura', $modelCobertura)
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Hcl_poblacion::find($id);
        $modelParentesco = Hcl_afiliacion_parentesco::all();
        $modelAfiliacion = Hcl_poblacion_afiliacion::where('idpoblacion', '=', $id)->first(); 
        $modelCuaderno = Hcl_cuaderno::all();
        $modelCobertura = Hcl_cobertura::all();
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
        $modelEmpresas = Hcl_empleador::where('id', '=', $modelAfiliacion->idempleador)->first()->toJson();
        $model->fecha_nacimiento = date("Y-m-d", strtotime($model->fecha_nacimiento));
        $model->txtListaPoblacion = $modelEmpresas;
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelParentesco', $modelParentesco)
                ->with('departamentos', $departamentos)
                ->with('modelCuaderno', $modelCuaderno)
                ->with('modelAfiliacion', $modelAfiliacion)
                ->with('modelCobertura', $modelCobertura)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Hcl_poblacion::find($id); 
        $model->codigo = strtoupper($request->codigo);
        $model->nombre = strtoupper($request->nombre); 
        $model->descripcion = strtoupper($request->descripcion);
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("especialidad")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    // public function autocompleteHcl_cie(Request $request)
    // {
    //     $dato = $request->dato;
    //     $idalmacen = $request->idalmacen;

    //     $data = Hcl_cie::where('descripcion','LIKE','%'.$dato.'%')
    //                 ->take(10)->get();

    //     $result=array();
    //     foreach ($data as $key => $value)
    //     {
    //         $result[]=[
    //             'id' => $value->id,
    //             'value' => $value->descripcion.' ('.$value->codigo.')',
    //             'label' => $value->descripcion.' ('.$value->codigo.')',
    //         ];
    //     }
    //     return response()->json($result);
    // }

    public function muestraHcl_cie()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $idalmacen = $_POST['idalmacen'];
            $buscarTodo = $_POST['buscarTodo'];
            $idsLab = '';
            if(isset($_POST['datosDiagnostico']))
                $idsLab = $this->retornaIdExamenes(json_decode($_POST['datosDiagnostico']));
            
            $modelo = Hcl_cie::where([
                            ['eliminado', '=', 0],
                            // ['idalmacen', '=', $idalmacen],
                            ['descripcion', 'ILIKE', '%'.$dato.'%'],
                            // ['codigo', 'ILIKE', '%'.$dato.'%']
                        ])
                        ->whereIn($buscarTodo == 0 && $idsLab != ''? 'id' : 'eliminado', 
                        array($buscarTodo == 0 && $idsLab != ''? DB::raw("select id from hcl_cie where id in(".$idsLab.")") : 0) )
                        ->orderBy('descripcion', 'ASC')
                        ->limit(100)
                        ->get()->toJson();
            return $modelo;
        }
    }
    private function retornaIdExamenes($datosTablaLab) {
        $idsLab = '';
        for($j = 0; $j < count($datosTablaLab); $j++)
        {
            if($j == 0)
                $idsLab .= $datosTablaLab[$j];
            else
                $idsLab .= ','. $datosTablaLab[$j];
        }
        return $idsLab;
    }
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------
}