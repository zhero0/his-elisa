<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen; 
use \App\Models\Afiliacion\Hcl_plantilla;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

class Hcl_plantillaController extends Controller
{
    private $rutaVista = 'Salud.hcl_plantilla.';
    private $controlador = 'hcl_plantilla';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        // print_r($arrayDatos['modelUser_menuopcion']);
        // return;

        $dato = $request->search;
        $model = Hcl_plantilla::where([
                    ['eliminado', '=', 0],
                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
                ])
                ->where(function($query) use($dato) { 
                    $query->orwhere('nombre', 'like', '%'.$dato.'%');  
                })
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        // $model->modelMenu = $arrayDatos['modelMenu'];
        // $model->modelMenuopcion = $arrayDatos['modelMenuopcion'];
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

     
        $model = new hcl_plantilla;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $model = new hcl_plantilla;  
        $model->nombre = strtoupper($request->nombre); 
        $model->iddatosgenericos = $arrayDatos['iddatosgenericos'];
        // print_r($request->plantilla);
        // return;
        $model->plantilla = $request->plantilla;  
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("plantilla_personal")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_especialidad::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = hcl_plantilla::find($id); 
        // $model->inicio = date("g:i a",strtotime($model->hora_inicio));
        // $model->fin = date("g:i a",strtotime($model->hora_fin));
        // print_r($model->inicio);
        // return;

        $model->accion = $this->controlador;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model) 
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arrayDatos = Home::parametrosSistema();
        $model = Hcl_plantilla::find($id);  
        $model->nombre = strtoupper($request->nombre); 
        $model->iddatosgenericos = $arrayDatos['iddatosgenericos'];
        $model->plantilla = $request->plantilla;
        $model->usuario = Auth::user()['name'];
        
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("plantilla_personal")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function registrar_plantilla_salud()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $descripcion = $_POST['descripcion'];
            
            $arrayDatos = Home::parametrosSistema();
            $model = new Hcl_plantilla;  
            $model->nombre = strtoupper($dato); 
            $model->plantilla = $descripcion; 
            $model->iddatosgenericos = $arrayDatos['iddatosgenericos'];
            $model->save(); 
        }
    }
    public function busca_Plantilla()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Hcl_plantilla::where([
                            ['eliminado', '=', 0],
                            ['iddatosgenericos', 'like', '%'.$dato.'%']
                        ])
                        ->orderBy('nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }
    public function buscador_plantillas_salud()
    {
        $arrayDatos = Home::parametrosSistema();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Hcl_plantilla::where([
                            ['eliminado', '=', 0],
                             ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
                        ])
                        ->where(function($query) use ($dato) {
                                $query->orwhere('nombre', 'like', '%'.$dato.'%');
                            })
                        ->orderBy('nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}