<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Afiliacion\Hcl_afiliacion_parentesco;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion_afiliacion;
use \App\Models\Afiliacion\Hcl_cobertura;
use \App\Models\Afiliacion\Hcl_sexo;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use Illuminate\Support\Facades\DB;
use \App\Permission;
use GuzzleHttp\Client;

use Session;
use Illuminate\Support\Facades\URL;

class Hcl_poblacionController extends Controller
{
    private $rutaVista = 'afiliacion.hcl_poblacion.';
    private $controlador = 'hcl_poblacion';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
         
       
        $matricula = $request->matricula; 
        $primer_apellido = $request->primer_apellido;
        $segundo_apellido = $request->segundo_apellido;
        $nombres = $request->nombres;   
        $model = Hcl_poblacion::join('hcl_poblacion_afiliacion as b','b.idpoblacion','=','hcl_poblacion.id')
                ->join('hcl_empleador as c','c.id','=','b.idempleador')
                ->join('hcl_afiliacion_parentesco as d','d.id','=','b.idparentesco')
                ->select('hcl_poblacion.*','c.nombre  as nombre_empresa','c.nro_empleador','d.descripcion','b.idparentesco') 
                ->where([
                    ['hcl_poblacion.eliminado', '=', 0]
                ])
                ->where([
                    ['hcl_poblacion.numero_historia', $matricula == ''? '!=' : 'ilike', $matricula == ''? $matricula : '%'.$matricula.'%']
                ])
                ->where([
                    ['hcl_poblacion.nombre',$nombres == ''? '!=' : 'ilike', $nombres == ''? $nombres : '%'.$nombres.'%']
                ])
                ->where([
                    ['hcl_poblacion.primer_apellido', $primer_apellido == ''? '!=' : 'ilike', $primer_apellido == ''? $primer_apellido : '%'.$primer_apellido.'%']
                ])
                ->where([
                    ['hcl_poblacion.segundo_apellido', $segundo_apellido == ''? '!=' : 'ilike', $segundo_apellido == ''? $segundo_apellido : '%'.$segundo_apellido.'%']
                ]) 
                ->orderBy('hcl_poblacion.primer_apellido', 'ASC')
                ->paginate(config('app.pagination'));

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Hcl_poblacion;
        $modelAfiliacion = null;
        $modelCuaderno = Hcl_cuaderno::all();
        $modelDato = Hcl_sexo::all();
        $modelCobertura = Hcl_cobertura::all();
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
 
        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('departamentos', $departamentos)
                ->with('modelCuaderno', $modelCuaderno)
                ->with('modelDato', $modelDato)
                ->with('modelAfiliacion', $modelAfiliacion)
                ->with('modelCobertura', $modelCobertura)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        // $ultimo_registro =  DB::table('hcl_poblacion_afiliacion') 
        // ->selectraw('max(hcl_poblacion_afiliacion.idasegurado) as idasegurado')           
        // ->get();
        // $codigo_parentesco =  DB::table('hcl_afiliacion_parentesco') 
        // ->selectraw('hcl_afiliacion_parentesco.codificacion as paren')
        // ->where('hcl_afiliacion_parentesco.id','=',$request->parentesco)           
        // ->get();

        $model = new Hcl_poblacion;        
        
        $model->primer_apellido = strtoupper($request->primer_apellido); 
        
        $model->segundo_apellido = strtoupper($request->segundo_apellido);
        $model->nombre = strtoupper($request->nombre); 
        $model->documento = strtoupper($request->documento);
        $model->sexo = $request->optradio;
        $model->fecha_nacimiento = $request->fecha_nacimiento;  

        $anio = date("Y", strtotime($model->fecha_nacimiento));
        $anio = substr($anio,-2);

        $mes = date("m", strtotime($model->fecha_nacimiento));
 
        $model->sexo = $request->idSexo;
        $dia = date("d", strtotime($model->fecha_nacimiento)); 
        $primera_inicial = substr($model->primer_apellido,0,1);
        $segundo_inicial = substr($model->segundo_apellido,0,1);
        $nombre_inicial = substr($model->nombre,0,1);
        $historia = $anio.$mes.$dia.$primera_inicial.$segundo_inicial.$nombre_inicial;  
        $model->numero_historia = $historia;
        // $model->codigo = strtoupper($historia.'-'.$codigo_parentesco[0]->paren);

        $model->codigo = strtoupper($historia.'-ID');

        $model->telefono = $request->telefono;  
        $model->domicilio = $request->domicilio;  
        $model->id_departamento = 0;
        $model->id_provincia = 0;
        $model->id_municipio = 0;
        $model->id_localidad = 1;
        $model->observaciones = strtoupper($request->comentarios);
        $model->usuario = Auth::user()['name']; 
        // print_r('expression');
        // return;
        if($model->save()){
            $model_afiliacion = new Hcl_poblacion_afiliacion;
            $model_afiliacion->idpoblacion = $model->id; 
            // if ($request->parentesco == 29) {
            //     $model_afiliacion->idtipopaciente = 1;
            // }else{
            //     $model_afiliacion->idtipopaciente = 2;
            // }
            $model_afiliacion->idtipopaciente = 1;
            $model_afiliacion->idcobertura = 1;
            $model_afiliacion->idasegurado = $model->id;  //$ultimo_registro[0]->idasegurado+1;
            $model_afiliacion->idparentesco = 29;
            $model_afiliacion->idestadoafiliacion = 1;
            $model_afiliacion->idcuaderno = 3001;
            $model_afiliacion->fin_cobertura = '2018-01-01';
            $model_afiliacion->fecha_afiliacion = '2018-01-01';
            $model_afiliacion->fecha_registro = '2018-01-01';
            $model_afiliacion->idempleador = $request->txtId_empleador;
            $model_afiliacion->usuario = Auth::user()['name'];
            $model_afiliacion->save();
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
                             
        }
        else
            return redirect("poblacion");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {    
        $param = json_decode($request->param, true);
       
        try {
            $client = new Client();
            $response = $client->post(
                'https://auth-desarrollo.cns.gob.bo/connect/token',
                [
                    'form_params' => [
                        "grant_type" => "password",
                        "username" => "regional-lpz-salud",
                        "password" => "Regional-lpz-2021",
                        "scope" => "afiliaciones",
                        "client_id" => "regional-lpz",
                        "client_secret" => "regional-lpz testing"
                    ]
                ]
            );
            print_r($response);
            return;

            if($response->getStatusCode() == "200"){
                $data = json_decode($response->getBody(), true);
                $token = $data['access_token'];

                $url = 'https://api-desarrollo.cns.gob.bo/erp/v1/Afiliaciones/Asegurados?DocumentoIdentidad='.$param['documento'].'&FechaNacimiento='.$param['fecha_nacimiento'];
                $response = $client->request('GET', $url, [
                    'headers' => [
                        'Authorization' => 'Bearer '.$token,        
                        'Accept'        => 'application/json',
                    ]
                ]);
                return json_encode([
                    'status' => 'success',
                    'response' => $response->getBody()->getContents()
                ]);
            }else{
                return json_encode([
                    'status' => 'error',
                    'response' => 'No se pudo encontar la página! '
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                'status' => 'error',
                'response' => $e->getMessage()
            ]);
        }  
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Hcl_poblacion::find($id);
        $modelAfiliacion = Hcl_poblacion_afiliacion::where('idpoblacion', '=', $id)->first(); 
 
        $dato = $modelAfiliacion->idparentesco;
        // $sexo = $model->sexo;
        $modelCuaderno = Hcl_cuaderno::all();
        $modelDato = Hcl_sexo::all();
        $modelCobertura = Hcl_cobertura::all();
        $departamentos = DB::table('hcl_departamento')
                ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
                ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
                ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
                ->get();
        $modelEmpresas = Hcl_empleador::where('id', '=', $modelAfiliacion->idempleador)->first()->toJson();
        $model->txtListaPoblacion = $modelEmpresas;
        $model->fecha_nacimiento = date("Y-m-d", strtotime($model->fecha_nacimiento));

        $model->telefono = $model->telefono;  
        $model->domicilio = $model->domicilio;  
        $model->comentarios = $model->observaciones;
        $model->idempleador =$modelAfiliacion->idempleador;
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        // $model->Hcl_afiliacion_parentesco = Hcl_afiliacion_parentesco::where([
        //             ['eliminado', '=', 0],
        //             ['sexo', '=', $model->sexo]
        //         ])->get()->toJson();
        $model->Hcl_afiliacion_parentesco =  Hcl_afiliacion_parentesco::where('eliminado', '=', 0)
                ->where(function($query) use($dato) {                    
                    $query->orwhere('id', '=', $dato); 
                })->get()->toJson(); 

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('departamentos', $departamentos)
                ->with('modelCuaderno', $modelCuaderno)
                ->with('modelDato', $modelDato)
                ->with('modelAfiliacion', $modelAfiliacion)
                ->with('modelCobertura', $modelCobertura)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $codigo_parentesco =  DB::table('hcl_afiliacion_parentesco') 
        // ->selectraw('hcl_afiliacion_parentesco.codificacion as paren')
        // ->where('hcl_afiliacion_parentesco.id','=',$request->parentesco)           
        // ->get();

        $model = Hcl_poblacion::find($id); 
        $model->numero_historia = strtoupper($request->numero_historia);
       
        $model->primer_apellido = strtoupper($request->primer_apellido);         
        $model->segundo_apellido = strtoupper($request->segundo_apellido);
        $model->nombre = strtoupper($request->nombre); 
        $model->documento = strtoupper($request->documento);
        $model->sexo = $request->idSexo;
        $model->fecha_nacimiento = $request->fecha_nacimiento;  
        
        // $model->codigo = strtoupper($historia.'-ID');
        $model->codigo = strtoupper($request->numero_historia.'-ID');
        $model->telefono = $request->telefono;  
        $model->domicilio = $request->domicilio;  
        $model->id_departamento = 0;
        $model->id_provincia = 0;
        $model->id_municipio = 0;
        $model->id_localidad = 1;
        $model->observaciones = strtoupper($request->comentarios);
        $model->usuario = Auth::user()['name']; 
  
        if($model->save()){
            $model_afiliacion = Hcl_poblacion_afiliacion::where([
                    ['idpoblacion', '=', $model->id]
                ])->first();
 
            // $model_afiliacion->idpoblacion = $model->id; 
            // $model_afiliacion->idpoblacion = $model->id; 
            // if ($request->parentesco == 29) {
            //     $model_afiliacion->idtipopaciente = 1;
            // }else{
            //     $model_afiliacion->idtipopaciente = 2;
            // }
            $model_afiliacion->idtipopaciente = 1;
            $model_afiliacion->idcobertura = 1;
            $model_afiliacion->idasegurado = $model->id;  //$ultimo_registro[0]->idasegurado+1;
            $model_afiliacion->idparentesco = 29;
            $model_afiliacion->idestadoafiliacion = 1;
            $model_afiliacion->idcuaderno = 3001;
            $model_afiliacion->fin_cobertura = '2018-01-01';
            $model_afiliacion->fecha_afiliacion = '2018-01-01';
            $model_afiliacion->fecha_registro = '2018-01-01';
            $model_afiliacion->idempleador = $request->idempleador;
            $model_afiliacion->usuario = Auth::user()['name'];
            $model_afiliacion->save();
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

         
        return redirect("hcl_poblacion")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createBeneficiario($id) //Request $request)
    {
        
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Hcl_poblacion;
        $modelAfiliacion = null;
        $modelCuaderno = Hcl_cuaderno::all();
        $modelCobertura = Hcl_cobertura::all();
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
      
        $model->accion = $this->controlador;
        $model->scenario = 'createBeneficiario';
        $model->rutaview = $this->rutaVista;
        $model->idpoblacion = $id;
        Session::put('urlAnterior', URL::previous());

        //print_r($model->idpoblacion);
        //return;

        return view($model->rutaview.'createBeneficiario')
                ->with('model', $model)
                ->with('departamentos', $departamentos)
                ->with('modelCuaderno', $modelCuaderno)
                ->with('modelAfiliacion', $modelAfiliacion)
                ->with('modelCobertura', $modelCobertura)
                ->with('arrayDatos', $arrayDatos);
    }
    
    public function storeBeneficiario(Request $request,$id)
    {
        // $ultimo_registro =  DB::table('hcl_poblacion_afiliacion') 
        // ->selectraw('max(hcl_poblacion_afiliacion.idasegurado) as idasegurado')           
        // ->get();
        print_r('==>> '.$request->id);
        return;

        $codigo_parentesco =  DB::table('hcl_afiliacion_parentesco') 
        ->selectraw('hcl_afiliacion_parentesco.codificacion as paren')
        ->where('hcl_afiliacion_parentesco.id','=',$request->parentesco)           
        ->get();

        $modelTitular = Hcl_poblacion::join('hcl_poblacion_afiliacion as b','b.idpoblacion','=','hcl_poblacion.id')
                ->select('hcl_poblacion.id','hcl_poblacion.numero_historia','hcl_poblacion.id_departamento','hcl_poblacion.id_provincia','hcl_poblacion.id_municipio','hcl_poblacion.id_localidad','b.idcobertura','b.idtipopaciente','b.idasegurado','b.idcuaderno','b.idempleador') 
                ->where([
                    ['hcl_poblacion.id', '=', $id]
                ])->first();

        $model = new Hcl_poblacion;        
        
        $model->primer_apellido = strtoupper($request->primer_apellido); 
        
        $model->segundo_apellido = strtoupper($request->segundo_apellido);
        $model->nombre = strtoupper($request->nombre); 
        $model->documento = strtoupper($request->documento);
        $model->sexo = $request->optradio;
        $model->fecha_nacimiento = $request->fecha_nacimiento;  

        
        $historia = $modelTitular->numero_historia;  
        $model->numero_historia = $historia;
        $model->codigo = strtoupper($request->numero_historia.'-ID');

        $model->telefono = $request->telefono;  
        $model->domicilio = $request->domicilio;  
        $model->id_departamento = 0;
        $model->id_provincia = 0;
        $model->id_municipio = 0;
        $model->id_localidad = $request->departamento;
        $model->observaciones = strtoupper($request->comentarios);
        $model->usuario = Auth::user()['name']; 
        if($model->save()){
            $model_afiliacion = new Hcl_poblacion_afiliacion;
            $model_afiliacion->idpoblacion = $model->id; 
            // if ($request->parentesco == 29) {
            //     $model_afiliacion->idtipopaciente = 1;
            // }else{
            //     $model_afiliacion->idtipopaciente = 2;
            // }
            $model_afiliacion->idtipopaciente = 1;
            $model_afiliacion->idcobertura = 1;
            $model_afiliacion->idasegurado = $model->id;  //$ultimo_registro[0]->idasegurado+1;
            $model_afiliacion->idparentesco = 29;
            $model_afiliacion->idestadoafiliacion = 1;
            $model_afiliacion->idcuaderno = 3001;
            $model_afiliacion->fin_cobertura = '2018-01-01';
            $model_afiliacion->fecha_afiliacion = '2018-01-01';
            $model_afiliacion->fecha_registro = '2018-01-01';
            $model_afiliacion->idempleador = $request->txtId_empleador;
            $model_afiliacion->usuario = Auth::user()['name'];
            $model_afiliacion->save();
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

       return redirect("hcl_poblacion")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);

    }



    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaCobertura()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            $modelCoberturas = Hcl_cobertura::where('eliminado', '=', 0)
                ->where(function($query) use($dato) {                    
                    $query->orwhere('descripcion', 'ilike', '%'.$dato.'%'); 
                })->get();

            echo json_encode($modelCoberturas);
        }
    }
    public function buscaEmpleador()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            $modelLab_empresas = Hcl_empleador::where('eliminado', '=', 0)
                ->where(function($query) use($dato) {                    
                    $query->orwhere('nombre', 'ilike', '%'.$dato.'%'); 
                    $query->orwhere('nro_empleador', 'ilike', '%'.$dato.'%');
                    
                })->get();

            echo json_encode($modelLab_empresas);
        }
    }
    public function buscaParentesco()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];   
            $modelBuscaParentesco = Hcl_afiliacion_parentesco::where('eliminado', '=', 0)
                ->where(function($query) use($dato) {                    
                    $query->orwhere('sexo', '=', $dato);
                    $query->orwhere('sexo', '=', '0'); 
                })->get();
            echo json_encode($modelBuscaParentesco);
        }
    }
    public function buscaPacientes()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            $modelo = Hcl_poblacion::where(function($query) use($dato) {
                        $query->orwhere('numero_historia', 'ilike', '%'.$dato.'%');
                        $query->orwhere('nombre', 'ilike', '%'.$dato.'%');
                        $query->orwhere('primer_apellido', 'ilike', '%'.$dato.'%');
                        $query->orwhere('segundo_apellido', 'ilike', '%'.$dato.'%');
                        $query->orwhere(DB::raw("CONCAT(nombre, ' ',primer_apellido, ' ', segundo_apellido)"), 'ilike', '%'.$dato.'%');
                    })
                    ->where('eliminado', '=', 0)
                    ->select('hcl_poblacion.*', 
                            DB::raw("(SELECT date_part('year', age(fecha_nacimiento)))::integer AS edad"))
                    ->take(config('app.muestraTotalBusqueda'))
                    ->get();
            echo json_encode($modelo);
        }
    }
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------
}
