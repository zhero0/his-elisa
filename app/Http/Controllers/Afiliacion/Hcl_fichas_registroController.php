<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen; 
use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;  
use \App\Models\Afiliacion\Hcl_fichas_registro;
use \App\Models\Afiliacion\Hcl_fichas_programadas; 
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

class Hcl_fichas_registroController extends Controller
{
    private $rutaVista = 'Salud.hcl_fichas_registro.';
    private $controlador = 'hcl_fichas_registro';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
 
        $cuaderno = $request->cuaderno; 
        $apellidos = $request->apellidos; 
        $nombres = $request->nombres;          
         
        $model = Hcl_fichas_registro::join('hcl_cuaderno as b','hcl_fichas_registro.idhcl_cuaderno','=','b.id')
                ->join('datosgenericos as c','hcl_fichas_registro.iddatosgenericos','=','c.id')
                ->select('hcl_fichas_registro.*', 'b.sigla', 'b.nombre', 'c.matricula', 'c.nombres', 'c.apellidos') 
                ->where([ 
                    ['b.nombre', 'ilike', '%'.$cuaderno.'%'], 
                    ['c.apellidos', 'ilike', '%'.$apellidos.'%'],
                    // ['c.matricula', 'ilike', '%'.$matricula.'%'],
                    ['c.nombres', 'ilike', '%'.$nombres.'%'.'%'],
                ])->get();     
        
        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador); 
        $model = new Hcl_fichas_registro;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        return view($model->rutaview.'create')
                ->with('model', $model) 
                //->with('modelSucursales', $modelSucursales)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {   
        $model = new Hcl_fichas_registro;
        $model->cantidad_fichas = $request->cantidad_fichas; 
        $model->fecha_desde = $request->fecha_desde; 
        $model->fecha_hasta = $request->fecha_hasta; 
        $model->hora_inicio = $request->hora_inicio; 
        $model->hora_intervalo = $request->hora_intervalo; 
        $model->idhcl_cuaderno = $request->idhcl_cuaderno; 
        $model->iddatosgenericos = $request->iddatosgenericos; 
        $model->idalmacen = $request->idalmacen;
        $model->usuario = Auth::user()['name'];

        if($model->save()){
            $modelDatos = DB::table(DB::raw("hcl_programar_fichas(".$model->id.")"))->get();  
            $mensaje = config('app.mensajeGuardado');
        }else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("hcl_fichas_registro")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Hcl_fichas_registradas::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador); 

        $model = Hcl_fichas_registro::find($id); 
        $modelDatos = Datosgenericos::find($model->iddatosgenericos);
        $modelCuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
        $model->medico = $modelDatos->matricula.' - '.$modelDatos->apellidos.' '.$modelDatos->nombres;
        $model->especialidad = $modelCuaderno->sigla.' - '.$modelCuaderno->nombre;
        
        $model->scenario = 'edit';
        $model->accion = $this->controlador;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)    
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);

        $model = Hcl_cuaderno::find($id); 
        $model->sigla = strtoupper($request->sigla);
        $model->idfar_sucursales = $request->idfar_sucursales; 
        $model->id_especialidad = $request->id_especialidad; 
        $model->nombre = strtoupper($request->nombre);
        $model->escala_tiempo =  $request->escala_tiempo; 
        $model->fichas_defecto = 'SI';
        $model->transferencia = 'SI'; 
        $model->hora_inicio = $request->hora_inicio;
        $model->hora_fin = $request->hora_fin;
        $model->nivel_seguridad =  $txtHiddenJSONdatos->seguridad;
        $model->formato_historia = $request->formato_historia; 

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("cuaderno")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function busca_Especialidad()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Hcl_cuaderno::where([
                            ['eliminado', '=', 0],
                            ['nombre', 'ilike', '%'.$dato.'%']
                        ])
                        ->orderBy('nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }
    public function muestraEspecialidad()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];  
            $modelo = Hcl_cuaderno::where([
                            ['eliminado', '=', 0], 
                            ['idalmacen', '=', $dato]
                        ])                        
                        ->orderBy('nombre', 'ASC') 
                        ->get()->toJson();
            echo $modelo;
        }
    }

    public function eliminarFichas(Request $request, $id)
    {     
        $model = Hcl_fichas_registro::find($id);        
        $model->eliminado = 1;        
        $model->usuario = Auth::user()->name; 
        $model->save();      

        Hcl_fichas_programadas::where([
                                        ['eliminado', '=', 0],
                                        ['idhcl_fichas_registro', '=', $model->id]
                                    ])->update(['eliminado' => 1]); 
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}