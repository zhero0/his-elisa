<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Laboratorio\Lab_especialidad;
use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Farmacia\Far_sucursales;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

class Hcl_cuadernoController extends Controller
{
    private $rutaVista = 'Salud.hcl_cuaderno.';
    private $controlador = 'hcl_cuaderno';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $dato = $request->search;
        $model = Hcl_cuaderno::where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $arrayDatos['idalmacen']],
                ])
                ->where(function($query) use($dato) { 
                    $query->orwhere('nombre', 'like', '%'.$dato.'%');  
                })
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));
        
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelHcl_especialidad = Hcl_especialidad::where([
                ['eliminado', '=', 0],
                ['cuaderno', '=', Hcl_especialidad::cuaderno]])->get(); 

        // $modelSucursales = Far_sucursales::where([ 
        //                     ['eliminado', '=', 0],  
        //                     ['idalmacen', '=', $arrayDatos['idalmacen']],                                                                                                           
        //                 ])->pluck('nombre', 'id')->all();


        $model = new hcl_cuaderno;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                //->with('modelSucursales', $modelSucursales)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);

        $model = new hcl_cuaderno;
        $model->sigla = strtoupper($request->sigla);
        $model->id_especialidad = $request->id_especialidad; 
        $model->idfar_sucursales = $request->idfar_sucursales; 
        $model->nombre = strtoupper($request->nombre);
        $model->escala_tiempo =  '00:'.$request->escala_tiempo; 
        $model->fichas_defecto = 'SI';
        $model->transferencia = 'SI';
        $model->hora_inicio = $request->hora_inicio;
        $model->hora_fin = $request->hora_fin;
        $model->nivel_seguridad =  $txtHiddenJSONdatos->seguridad;
        $model->formato_historia = $request->formato_historia; 
        $model->idalmacen = $request->idalmacen;
        $model->idfar_sucursales = 15;
        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("cuaderno")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Lab_especialidad::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelHcl_especialidad = Hcl_especialidad::where([
                ['eliminado', '=', 0],
                ['cuaderno', '=', Hcl_especialidad::cuaderno]])->get(); 

        // $modelSucursales = Far_sucursales::where([ 
        //                     ['eliminado', '=', 0],  
        //                     ['idalmacen', '=', $arrayDatos['idalmacen']],                                    
        //                 ])->pluck('nombre', 'id')->all();

        $model = hcl_cuaderno::find($id);
        //$model->escala_tiempo = substr($model->escala_tiempo, 6);

        $array_Informacion = array(
            'nivel_seguridad' => $model->nivel_seguridad,
        );
        $model->txtHiddenJSONdatos = json_encode($array_Informacion);
        
        $model->scenario = 'edit';
        $model->accion = $this->controlador;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                // ->with('modelSucursales', $modelSucursales)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);

        $model = Hcl_cuaderno::find($id); 
        $model->sigla = strtoupper($request->sigla);
        $model->idfar_sucursales = 15;
        $model->id_especialidad = $request->id_especialidad; 
        $model->nombre = strtoupper($request->nombre);
        $model->escala_tiempo =  $request->escala_tiempo; 
        $model->fichas_defecto = 'SI';
        $model->transferencia = 'SI'; 
        $model->hora_inicio = $request->hora_inicio;
        $model->hora_fin = $request->hora_fin;
        $model->nivel_seguridad =  $txtHiddenJSONdatos->seguridad;
        $model->formato_historia = $request->formato_historia; 

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect("cuaderno")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function busca_Especialidad()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = Hcl_cuaderno::where([
                            ['eliminado', '=', 0],
                            ['nombre', 'ilike', '%'.$dato.'%']
                        ])
                        ->orderBy('nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }
    public function muestraEspecialidad()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];  
            $modelo = Hcl_cuaderno::where([
                            ['eliminado', '=', 0], 
                            ['idalmacen', '=', $dato]
                        ])                        
                        ->orderBy('nombre', 'ASC') 
                        ->get()->toJson();
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}