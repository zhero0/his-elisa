<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Afiliacion\Hcl_empleador;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\CursorPaginator;

use Session;
use Illuminate\Support\Facades\URL;

class Hcl_empleadorController extends Controller
{
    private $rutaVista = 'afiliacion.empresa.';
    private $controlador = 'hcl_empleador';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $estados = array(            
            ['id' => '2','nombre'=>'Vigente'],
            ['id' => '3','nombre'=>'En mora'],
            ['id' => '4','nombre'=>'Baja Temporal'],); 
        $dato = $request->search;
        $model = Hcl_empleador::where('eliminado', '=', 0)
                ->where(function($query) use($dato) {
                    $query->orwhere('nro_empleador', 'like', '%'.$dato.'%'); 
                    $query->orwhere('nombre', 'like', '%'.$dato.'%'); 
                })
                ->orderBy('nombre', 'ASC')->get();
                // ->paginate();
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('estados', $estados)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        $estados = array(            
            ['id' => '2','nombre'=>'Vigente'],
            ['id' => '3','nombre'=>'En mora'],
            ['id' => '4','nombre'=>'Baja Temporal'],); 
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
        $model = new Hcl_empleador;
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('departamentos', $departamentos)
                ->with('estados', $estados)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.id','hcl_provincia.id','hcl_municipio.id','hcl_municipio.id_municipio')  
        ->where([
                    ['hcl_municipio.id_municipio', '=', $request->variable],                
                ])         
        ->get();

        $model = new Hcl_empleador;                  
        $model->nombre = strtoupper($request->nombre); 
        $model->nro_empleador = strtoupper($request->nro_empleador); 
        $model->zona = strtoupper($request->zona); 
        $model->calle = strtoupper($request->calle); 
        $model->nro = strtoupper($request->nro); 
        $model->telefono = strtoupper($request->telefono); 
        $model->representante_legal = strtoupper($request->representante_legal); 
        $model->nro_trabajadores = $request->nro_trabajadores; 
        $model->nro_padron = strtoupper($request->nro_padron); 
        $model->fecha_presentacion = $request->fecha_presentacion; 
        $model->estado = $request->estado; 
        $model->id_departamento = 1;
        $model->id_provincia = 1;
        $model->id_municipio = 1;
        $model->id_localidad = $departamentos[0]->id_municipio;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
                             
        }
        else
            return redirect("empresa"); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);


        $estados = array(            
            ['id' => '2','nombre'=>'Vigente'],
            ['id' => '3','nombre'=>'En mora'],
            ['id' => '4','nombre'=>'Baja Temporal'],); 
        // foreach ($estados as $value) {
        //     print_r($value['nombre']);
        //     print_r('<br>');
        // }
        // //print_r($estados['0']['id'] );
        // return;

        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();

        //foreach ($departamentos as $value) {
        //     print_r($value->dep_descripcion.' - '.$value->prov_descripcion.' - '.$value->mun_descripcion);
        //     print_r('<br>');
        // }
        // // print_r($asd[0]->dep_descripcion.' - '.$asd[0]->prov_descripcion.' - '.$asd[0]->mun_descripcion);
        // return;

        $model = Hcl_empleador::find($codigo); 
        // print_r($model->id_localidad);
        // return;
        $desde = date("Y-m-d", strtotime($model->fecha_presentacion));
        $model->fecha_presentacion = $desde;
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))                
                ->with('departamentos', $departamentos)
                ->with('estados', $estados)
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $estados = array(            
            ['id' => '2','nombre'=>'Vigente'],
            ['id' => '3','nombre'=>'En mora'],
            ['id' => '4','nombre'=>'Baja Temporal'],); 

        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.dep_descripcion','hcl_provincia.prov_descripcion','hcl_municipio.mun_descripcion','hcl_municipio.id_municipio')           
        ->get();
        $model = Hcl_empleador::find($id);
        $desde = date("Y-m-d", strtotime($model->fecha_presentacion));
        $model->fecha_presentacion = $desde;
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('departamentos', $departamentos)
                ->with('estados', $estados)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $departamentos = DB::table('hcl_departamento')
        ->join('hcl_provincia','hcl_provincia.id_departamento','=','hcl_departamento.id')
        ->join('hcl_municipio','hcl_municipio.id','=','hcl_provincia.id')
        ->select('hcl_departamento.id','hcl_provincia.id','hcl_municipio.id','hcl_municipio.id_municipio')  
        ->where([
                    ['hcl_municipio.id_municipio', '=', $request->variable],                
                ])         
        ->get();

        // print_r($departamentos[0]->id_municipio);
        // return;

        $model = Hcl_empleador::find($id);                 
        $model->nombre = strtoupper($request->nombre); 
        $model->nro_empleador = strtoupper($request->nro_empleador); 
        $model->zona = strtoupper($request->zona); 
        $model->calle = strtoupper($request->calle); 
        $model->nro = strtoupper($request->nro); 
        $model->telefono = strtoupper($request->telefono); 
        $model->representante_legal = strtoupper($request->representante_legal); 
        $model->nro_trabajadores = $request->nro_trabajadores; 
        $model->nro_padron = strtoupper($request->nro_padron); 
        $model->fecha_presentacion = $request->fecha_presentacion; 
        $model->estado = $request->estado; 
        $model->id_departamento = 1;
        $model->id_provincia = 1;
        $model->id_municipio = 1;
        $model->id_localidad = $departamentos[0]->id_municipio;;
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("empresa");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}


