<?php
namespace App\Http\Controllers\Afiliacion;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Input;
use Auth;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_cuaderno_personal;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Administracion\Datosgenericos;
use \App\Models\Almacen\Generico;
use \App\Models\Almacen\Almacen;
use \App\Models\Almacen\Home;
 
use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Imagenologia\Ima_examenes;
use \App\Models\Imagenologia\Ima_registroprogramacion;
use \App\Models\Imagenologia\Ima_estado;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;
use \App\Models\Imagenologia\Ima_resultados_programacion;

class Hcl_cuaderno_personalController extends Controller
{
    private $rutaVista = 'Salud.hcl_cuaderno_personal.';
    private $controlador = 'hcl_cuaderno_personal';
    private $idalmacen;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        // $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
 
        $model = Hcl_cuaderno_personal::join('hcl_cuaderno', 'hcl_cuaderno_personal.idhcl_cuaderno', '=', 'hcl_cuaderno.id')
                ->join('datosgenericos', 'hcl_cuaderno_personal.iddatosgenericos', '=', 'datosgenericos.id') 
                ->select('hcl_cuaderno_personal.id',
                        'datosgenericos.apellidos',
                        'datosgenericos.nombres', 
                        'hcl_cuaderno.nombre', 
                        'hcl_cuaderno_personal.usuario')
                ->where([
                            ['hcl_cuaderno_personal.eliminado', '=', 0],
                            ['hcl_cuaderno_personal.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])
                ->where(function($query) use($dato) {
                    $query->orwhere('datosgenericos.apellidos', 'like', '%'.$dato.'%');
                    $query->orwhere('datosgenericos.nombres', 'like', '%'.$dato.'%'); 
                    })
                ->orderBy('datosgenericos.apellidos', 'ASC')
                ->paginate(config('app.pagination'));
 

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
         
        $modelHcl_cuaderno = ['' => 'Seleccione! '] + 
                                    Hcl_cuaderno::where('eliminado', '=', 0)
                                    ->orderBy('nombre', 'ASC')->pluck('nombre', 'id')->all();
        $modelDatosgenericos = Datosgenericos::where('id_generico', '=', Generico::MEDICO)
                                    ->orderBy('apellidos', 'ASC')->get();
        
        $model = new Hcl_cuaderno_personal;

        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        $this->seteaDatosComunes($model);

        return view($model->rutaview.'create')
                ->with('model', $model)  
                ->with('modelHcl_cuaderno', $modelHcl_cuaderno)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Hcl_cuaderno_personal;
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();

        if($modelAlmacenusuario == null)
        {
            echo 'Por favor Asigne una UNIDAD MEDICA al usuario! ';
            return;
        }   
         
        $model->idalmacen = $modelAlmacenusuario->idalmacen;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos; 
        $model->usuario = Auth::user()['name'];
        // print_r($model);
        // return;
        
        if($model->save())
        {           
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("cuaderno_personal")
                ->with('message', 'store')
                ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // print_r($arrayDatos['idalmacen']);
        // return; 

        $model = Hcl_cuaderno_personal::find($codigo); 
        $mensaje = config('app.mensajeGuardado');

        if ($model!= null) {
             if($model->delete())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        }

        return redirect("cuaderno_personal")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);  
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        
        return view($model->rutaview.'edit')
                ->with('model', $model) 
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
   

        return redirect("programacion_ima")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }

    private function seteaDatosComunes($model)
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        $model->ima_examenesEspecialidad = DB::table('ima_examenes as le')
                        ->join('ima_especialidad as e', 'le.idima_especialidad', '=', 'e.id')
                        ->select(
                            'le.id as idima_examenes', 'le.codigo', 'le.nombre',
                            'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                        )
                        ->where([
                            ['le.eliminado', '=', 0],
                            ['le.idalmacen', '=', $modelAlmacenusuario->idalmacen]
                            ])
                        ->orderBy('le.nombre', 'ASC')
                        ->get()->toJson();
        
        $model->especialidadProgramados = DB::table('ima_examenes_registroprogramacion as p')
                        ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                        ->join('ima_especialidad as esp', 'e.idima_especialidad', '=', 'esp.id')
                        ->select('esp.codigo', 'esp.nombre', DB::raw('count(p.idima_examenes) as total'))
                        ->where([
                            ['p.eliminado', '=', 0],  
                            ['e.idalmacen', '=', $modelAlmacenusuario->idalmacen]                           
                            ])
                        ->groupBy('esp.codigo', 'esp.nombre')
                        ->orderBy('esp.nombre', 'ASC')
                        ->get()->toJson();
        $model->hora_actual = date('h:i A');
        $model->horaprogramacion = $model->horaprogramacion == null? $model->hora_actual : $model->horaprogramacion;
        $model->fechaprogramacion = $model->fechaprogramacion == null? date('d-m-Y') : $model->fechaprogramacion;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        echo "====".$id;
        return;
        $model = Hcl_cuaderno_personal::find($id); 

        if($model->delete())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        return redirect("cuaderno_personal")
                ->with('message', 'update')
                ->with('mensaje', $mensaje);
    }


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaExamenes()
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            $modelIma_variables = DB::table('ima_examenes as le')
                            ->join('ima_especialidad as e', 'le.idima_especialidad', '=', 'e.id')
                            ->select(
                                'le.id as idima_examenes', 'le.codigo', 'le.nombre',
                                'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                            ) 
                            ->where([
                            ['le.eliminado', '=', 0],
                            ['le.idalmacen', '=', $modelAlmacenusuario->idalmacen]
                            ])
                            ->where(function($query) use($dato) {
                                $query->orwhere('le.codigo', 'like', '%'.$dato.'%');
                                $query->orwhere('le.nombre', 'like', '%'.$dato.'%');
                                $query->orwhere('e.codigo', 'like', '%'.$dato.'%');
                                $query->orwhere('e.nombre', 'like', '%'.$dato.'%');
                            })
                            ->take(config('app.muestraTotalBusqueda'))
                            ->get();
            echo json_encode($modelIma_variables);
        }
    }
    public function buscaProgramados()
    {
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = DB::table('ima_examenes_registroprogramacion as p')
                        ->join('ima_examenes as e', 'p.idima_examenes', '=', 'e.id')
                        ->select('e.codigo', 'e.nombre', DB::raw('count(p.idima_examenes) as total'))                         
                        ->where([
                            ['p.eliminado', '=', 0],
                             
                            ])
                        ->groupBy('e.codigo', 'e.nombre')
                        ->orderBy('e.nombre', 'ASC')
                        ->get();
            
            echo json_encode($modelo);
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
