<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;

use Session;
use Illuminate\Support\Facades\URL;

class AlmacenController extends Controller
{ 
    private $rutaVista = 'almacen.';
    private $controlador = 'almacen';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $model = Almacen::where('eliminado', '=', 0)
                    // ->where(function($query) use($dato) {
                    //     $query->orwhere('codigo', 'like', '%'.$dato.'%');
                    //     $query->orwhere('nombre', 'like', '%'.$dato.'%');
                    //     $query->orwhere('direccion', 'like', '%'.$dato.'%');
                    //     $query->orwhere('descripcion', 'like', '%'.$dato.'%');
                    //     $query->orwhere('usuario', 'like', '%'.$dato.'%');
                    // })
                    ->orderBy('nombre', 'ASC')
                    ->paginate(config('app.pagination'));

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
                        
        $model = new Almacen; 

        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous()); 

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {   
        $rules = [
            'codigo'=>'required',
            'nombre'=>'required', 
        ];
        $messages = [
            'codigo.required'=>'Se requiere registrar Codigo de la Sucursal.',
            'nombre.required'=>'Se requiere registrar Nombre de la Sucursal.', 
        ];

        $this->validate($request, $rules, $messages); 

        $model = new Almacen();
        $model->codigo = $request->codigo;
        $model->nombre = strtoupper($request->nombre);
        $model->direccion = $request->direccion;
        $model->descripcion = $request->descripcion; 
        // $model->usuario = Auth::user()['name']; 

        if($model->save())
        {                        
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        
        if(Session::has($this->nombre_session))
        {
            $valor_sesion = Session::get($this->nombre_session);
            Session::forget([$this->nombre_session]);
            return redirect()->to($valor_sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("almacen"); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Almacen::find($codigo);
        $model->accion = 'grupo';
        $model->scenario = 'view';
        
        return view('almacen.view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Almacen::find($id); 
        
       $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { 
        $rules = [
            'codigo'=>'required',
            'nombre'=>'required', 
        ];
        $messages = [
            'codigo.required'=>'Se requiere registrar Codigo de la Sucursal.',
            'nombre.required'=>'Se requiere registrar Nombre de la Sucursal.', 
        ];

        $this->validate($request, $rules, $messages); 

        $model = Almacen::find($id); 
        $model->codigo = $request->codigo;
        $model->nombre = strtoupper($request->nombre);
        $model->direccion = $request->direccion;
        $model->descripcion = $request->descripcion;
        // $model->usuario =  Auth::user()['name']; 

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("almacen");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
