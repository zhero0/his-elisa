<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\UnidadRequest;
use \App\Models\Almacen\Unidad;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;

class UnidadController extends Controller
{
    private $controlador = 'unidad';

   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();

        if(Permission::verificarAcceso($this->controlador.'.index') == 0)
            return view('errors.403')
                        ->with('arrayDatos', $arrayDatos)
                        ->with('url', '');

        $dato = $request->search;
        $model = Unidad::where('eliminado', '=', 0)
                        // ->where(function($query) use($dato) {
                        //     $query->orwhere('nombre', 'like', '%'.$dato.'%');
                        //     $query->orwhere('simbolo', 'like', '%'.$dato.'%');
                        //     $query->orwhere('usuario', 'like', '%'.$dato.'%');
                        //     $query->orwhere('created_at', 'like', '%'.date('Y-m-d', strtotime($dato)).'%');
                        // })
                        ->orderBy('nombre', 'ASC')
                        ->paginate(config('app.pagination'));
        $model->scenario = 'index';
        $model->search = $request->search;

        return view("unidad.index",compact('model'))
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        if(Permission::verificarAcceso($this->controlador.'.create') == 0)
            return view('errors.403')
                        ->with('arrayDatos', $arrayDatos)
                        ->with('url', $this->controlador);
        $model = new Unidad;
        $model->accion = 'unidad';
        $model->scenario = 'create';

        return view('unidad.create')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = [  
            'nombre' => strtoupper($request['nombre']),
            'simbolo' => strtoupper($request['simbolo']),
            'usuario' => Auth::user()['name'],
        ];
        $rules = [ 
            'nombre'=>'required', 
            'simbolo'=>'required', 
        ];
        $messages = [ 
            'nombre.required'=>'Se requiere registrar Nombre de la Unidad.', 
            'simbolo.required'=>'Se requiere registrar Simbolo de Unidad.', 
        ];

        $this->validate($request, $rules, $messages); 
        Unidad::create($model); 
        return redirect("unidad")
                ->with('message', 'store')
                ->with('mensaje', config('app.mensajeGuardado'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrayDatos = Home::parametrosSistema();

        if(Permission::verificarAcceso($this->controlador.'.show') == 0)
            return view('errors.403')
                        ->with('arrayDatos', $arrayDatos)
                        ->with('url', $this->controlador);

        $model = Unidad::find($id);
        $model->accion = 'grupo';
        $model->scenario = 'view';

        return view('unidad.view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
            return view('errors.403')
                        ->with('arrayDatos', $arrayDatos)
                        ->with('url', $this->controlador);

        $model = Unidad::findOrFail($id);
        return view('unidad.edit',compact('model'))
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model=Unidad::find($id);
        $this -> validate($request, [ 
            'nombre' => 'required',
            'simbolo' => 'required',]);
        $model->nombre = strtoupper($request->nombre);
        $model->simbolo = strtoupper($request->simbolo);
        $model->usuario =  Auth::user()['name'];
        $model->save(); 
        return redirect("unidad")
                ->with('message', 'update')
                ->with('mensaje', config('app.mensajeGuardado'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
