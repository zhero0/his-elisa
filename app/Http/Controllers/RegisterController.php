<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Almacen\Home;
use \App\Models\Afp\Afp_datos;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $arrayDatos = Home::parametrosSistema();
        return view('home')
            ->with('arrayDatos', $arrayDatos);
    }

        // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function registraDatos()
    {
        if(isset($_POST['documento']))
        {
            $documento = $_POST['documento'];
            $complemento = $_POST['complemento'];
            $paterno = $_POST['paterno'];
            $materno = $_POST['materno'];
            $casada = $_POST['casada'];
            $nombres = $_POST['nombres'];

            $model = new Afp_datos; 
            $model->ap_paterno = strtoupper($paterno); 
            $model->ap_materno = strtoupper($materno); 
            $model->nombres = strtoupper($nombres); 
            $model->numero_ci = strtoupper($documento); 
            $model->complemento = strtoupper($complemento); 
            $model->save(); 
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    // ------------------------------------------- REPORTES -------------------------------------------     
    public function reporteDocumento($valores)
    {    
        $parametro = json_decode($valores);           
        if($parametro->opcion == Afp_datos::AFP)
            Afp_datos::imprime_Solicitud($parametro);
        else if($parametro->opcion == Afp_datos::OTHER) 
            Afp_datos::imprime_Solicitud($parametro);
    }
    // ----------------------------------------- FIN REPORTES -----------------------------------------
}
