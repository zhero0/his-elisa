<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_proveedores; 
use \App\Models\Afiliacion\Hcl_departamento;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Far_proveedoresController extends Controller
{
    private $rutaVista = 'Farmacia.far_proveedores.';
    private $controlador = 'far_proveedores';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
        
        $estados = array(                        
            ['id' => '0','nombre'=>'Vigente'],
            ['id' => '1','nombre'=>'No Vigente'],
            );  

        $estado = 2;
        $nombre = '';
        $descripcion = '';
        $dato = $request->search;
        $datosBuscador = json_decode($request->datosBuscador);
        // print_r($datosBuscador);
        // return;
        if($datosBuscador != '')
        { 
            $estado = $datosBuscador->estado = ''? 0: $datosBuscador->estado;  
            $nombre = $datosBuscador->nombre;
            $descripcion = $datosBuscador->descripcion;
        }
    

        $model = Far_proveedores::where([
                            ['eliminado', '=', $estado],                            
                            ['nombre', 'ilike', '%'.$nombre.'%'],
                            ['descripcion', 'ilike', '%'.$descripcion.'%'],
                        ])
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));

        $model->datosBuscador = $request->datosBuscador;

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;
        $model->search = $request->search;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('estados', $estados)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_proveedores;

        $modelDepartamentos =  Hcl_departamento::get();
        
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)  
                ->with('modelDepartamentos', $modelDepartamentos)                               
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        // print_r($request->iddatosgenericos);
        // return;
        $rules = [            
            'nombre' => 'required',              
            'descripcion' => 'required',              
        ];
        $messages = [
            'nombre.required' => 'Se requiere registrar nombre',              
            'descripcion.required' => 'Se requiere registrar descripcion',              
        ];
        $this->validate($request, $rules, $messages);

        $model = new Far_proveedores;   

        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);        
        $model->nit = $request->nit;        
        $model->numero_movil = $request->numero_movil;
        $model->idhcl_departamento = $request->departamento;
        $model->ubicacion = strtoupper($request->ubicacion);        

        
        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // print_r($arrayDatos['idalmacen']);
        // return; 
        $model = Far_proveedores::find($codigo);  
        $model->eliminado = $model->eliminado == 0? 1 : 0;
        if ($model!= null) {
             if($model->save())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        }

        return redirect("inv_proveedor")
                ->with('message', 'update')
                ->with('mensaje', $mensaje); 
        
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_proveedores::find($id); 
        $modelDepartamentos =  Hcl_departamento::get();               
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)        
                ->with('modelDepartamentos', $modelDepartamentos)        
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Far_proveedores::find($id);  
             
        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);        
        $model->nit = $request->nit;        
        $model->numero_movil = $request->numero_movil;
        $model->idhcl_departamento = $request->departamento;
        $model->ubicacion = strtoupper($request->ubicacion);   

        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
    Autocomplete de PERSONAL
    */
 
    
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaProveedor()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
             $modelo = DB::table('far_proveedores as p')
                        ->join('hcl_departamento as d', 'd.id', '=', 'p.idhcl_departamento')
                        ->select('p.*', 'd.dep_descripcion as departamento')
                        ->where([
                                ['p.eliminado', '=', 0],
                            ])
                        ->where(function($query) use($dato) {
                            $query->orwhere('p.nombre', 'like', '%'.$dato.'%');
                        })
                        ->orderBy('p.nombre', 'ASC')
                        ->get()->toJson();

            // $modelo = Proveedor::where('eliminado', '=', 0)
            //             ->where(function($query) use ($dato) {
            //                     $query->orwhere('nombre', 'like', '%'.$dato.'%');
            //                     // $query->orwhere('apellidos', 'like', '%'.$dato.'%');
            //                 })
            //             ->orderBy('nombre', 'ASC')
            //             ->get()->toJson();
            echo $modelo;
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
