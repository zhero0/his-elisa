<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_adm_estado;
use \App\Models\Farmacia\Far_adm_articulo;
use \App\Models\Farmacia\Far_adm_clasificacion; 
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class Far_adm_articuloController extends Controller
{
    private $rutaVista = 'Farmacia.far_adm_articulo.';
    private $controlador = 'far_adm_articulo';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $modelEstados = Far_adm_estado::get();

        $model = Far_adm_articulo::where([ 
                            // ['idfar_adm_estado', '=', $estado],
                            // ['idfar_adm_clasificacion', '=', $clasificacion],
                            // ['codificacion', 'ilike', '%'.$codigo.'%'],
                            // ['descripcion', 'ilike', '%'.$descripcion.'%'],
                            // ['unidad', 'ilike', '%'.$unidad.'%'],                                                        
                        ]) 
                ->orderBy('codificacion', 'ASC')
                ->paginate(config('app.pagination'));
        
        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['eliminado', '=', 0],                                                        
                        ])->get();

        $model->idalmacen = $arrayDatos['idalmacen'];                
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')                
                ->with('modelEstados', $modelEstados)
                ->with('model', $model)
                ->with('modelClasificacion', $modelClasificacion)  
                ->with('arrayDatos', $arrayDatos);
                

                  
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_adm_articulo;

        // $model = Far_adm_articulo::alm_lista_articulos_farmacia();

        $modelEstado = Far_adm_estado::get(); 
        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['eliminado', '=', 0],                                                        
                        ])->get(); 
 
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)                
                ->with('modelEstado', $modelEstado)   
                ->with('modelClasificacion', $modelClasificacion)                              
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    { 

        // $rules = [            
        //     'codigo_identificacion' => 'required',              
        //     'nombre' => 'required',              
        //     'descripcion' => 'required',              
        // ];
        // $messages = [
        //     'codigo_identificacion.required' => 'Se requiere registrar codigo',              
        //     'nombre.required' => 'Se requiere registrar nombre',              
        //     'descripcion.required' => 'Se requiere registrar descripcion',              
        // ];
        // $this->validate($request, $rules, $messages); 

        
        $modelArticulos_almacen = Far_adm_articulo::alm_lista_articulos_farmacia_excluida();

        $modelArticulos_farmacia = Far_adm_articulo::get();  
         
        foreach ($modelArticulos_almacen as $dato) {

            $modelArticulos_farmacia = Far_adm_articulo::where([['articulo_id', '=', $dato->res_articulo_id],])->first(); 
 
            if ($modelArticulos_farmacia==null) {
                $model = new Far_adm_articulo;
                $model->articulo_id = $dato->res_articulo_id;      
                $model->codificacion_nacional = $dato->res_codificacion_nacional;
                $model->codificacion = $dato->res_codificacion;
                $model->descripcion = $dato->res_descripcion;
                $model->unidad = $dato->res_unidad;
                $model->concentracion = $dato->res_concentracion;   
                $model->tipo_envase = $dato->res_tipo_envase;          
                $model->psicotropico = 0;      
                $model->estupefaciente = 0;      
                $model->idfar_adm_estado = $dato->res_estado;        
                $model->idfar_adm_clasificacion = 1;         
                $model->usuario = 'almacen';
                $model->save();
            }else{     
                $modelArticulos_farmacia->unidad = $dato->res_unidad;
                $modelArticulos_farmacia->concentracion = $dato->res_concentracion;   
                $modelArticulos_farmacia->tipo_envase = $dato->res_tipo_envase;           
                $modelArticulos_farmacia->idfar_adm_estado = $dato->res_estado;         
                $modelArticulos_farmacia->usuario = 'almacen';
                $modelArticulos_farmacia->save();
            } 
        }  
  
        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', 'Lista de articulos actualizados correctamente!'); 
        }
        else
            return redirect("articulo");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_adm_articulo::find($codigo);

        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_adm_articulo::find($id); 

        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['eliminado', '=', 0],                                                        
                        ])->get();  

        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)   
                ->with('modelClasificacion', $modelClasificacion)                               
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Far_adm_articulo::find($id); 
        $model->idfar_adm_clasificacion = $request->clasificacion;      
        $model->codificacion_nacional = strtoupper($request->codificacion_nacional);
        $model->codificacion = strtoupper($request->codificacion);
        $model->descripcion = strtoupper($request->descripcion);
        $model->codigo_barras = $request->codigo_barras; 
        $model->unidad = $request->unidad;
        $model->concentracion = $request->concentracion;   
        $model->tipo_envase = $request->tipo_envase;          
        $model->psicotropico = $request->checkPsicotropico == 0? 0 : 1;      
        $model->estupefaciente = $request->checkEstupefaciente == 0? 0 : 1;      
        $model->idfar_adm_estado = $request->estadoArticulo;        
        $model->usuario = Auth::user()['name'];  
        
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("sucursal");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------


    public function busca_Articulo()
    { 
        if(isset($_POST['clasificacion']) || isset($_POST['sucursal']))
        {

            $clasificacion = $_POST['clasificacion'];
            $sucursal = $_POST['sucursal'];
            $pagina=1;//($_POST['pagina']!=null)?$_POST['pagina']:1;            
            if(isset($_POST['pagina']))
                $pagina=$_POST['pagina'];
            $offset=(0+$pagina-1)*100;
           
            $modelo = DB::table(DB::raw("far_lista_articulos_sucursal(
                          ".$sucursal.",
                          ".Far_adm_estado::AUTORIZADO.",
                          ".$clasificacion.",100,".$offset."
                       )"
                      ))->get(); 

            $count_modelo_all= DB::select(DB::raw("select count(id) from far_adm_articulo where id not in (
                                select idfar_adm_articulo from far_articulo_sucursal where idfar_sucursales = ".$sucursal." )
                                and idfar_adm_estado = ".Far_adm_estado::AUTORIZADO." and idfar_adm_clasificacion = ".$clasificacion));
            $row=$count_modelo_all[0];
            $cantidad=$row->count;
            $nro_paginas=floor($cantidad/100);
            if(($cantidad%100)>0)
                $nro_paginas++;

            //-------------------------------------------
            if($modelo!=null)
            if($cantidad>0){
                $modelo->paginacion=true;
                $modelo->total=$cantidad;
                $modelo->pagina_actual=$pagina;
                $modelo->pagina_inicio=$offset+1;
                $modelo->paginas_ranged=($offset+100>$cantidad)?$cantidad:$offset+100;
                $modelo->nro_paginas=$nro_paginas;
                $modelo->clasificacion=$clasificacion;
                $modelo->sucursal=$sucursal;
            }else{
                $modelo->paginacion=false;
            }
            //--------------------------------------------
         
            $viewArticulo = view('Farmacia.far_articulo_sucursal._articulos')
                    ->with('modelo', $modelo)
                    ->render();
            return response()->json(['viewArticulo' => $viewArticulo]);
       }
     
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
