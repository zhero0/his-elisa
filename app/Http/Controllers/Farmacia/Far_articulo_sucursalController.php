<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_adm_estado;
use \App\Models\Farmacia\Far_estado;
use \App\Models\Farmacia\Far_adm_articulo; 
use \App\Models\Farmacia\Far_sucursales;
use \App\Models\Farmacia\Far_articulo_sucursal;
use \App\Models\Farmacia\Far_adm_clasificacion; 
use \App\Models\Afiliacion\Hcl_cuaderno;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Far_articulo_sucursalController extends Controller
{
    private $rutaVista = 'Farmacia.far_articulo_sucursal.';
    private $controlador = 'far_articulo_sucursal';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');
 
        $modelEstados = Far_estado::get(); 
        $dato = $request->search; 
        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['eliminado', '=', 0],                                                        
                        ])->get();
        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],                                                     
                        ])->get();

        $model = Far_articulo_sucursal::where([ 
                            //['idfar_estado', '=', 1],                                                        
                        ]) 
                ->orderBy('id', 'ASC')
                ->paginate(20); 
        
        $model->scenario = 'index';                                
        $model->rutaview = $this->rutaVista;
        $model->search = $request->search;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')                
                ->with('modelEstados', $modelEstados)
                ->with('model', $model)
                ->with('modelClasificacion', $modelClasificacion)  
                ->with('modelSucursales', $modelSucursales)  
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_adm_articulo;   

        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['eliminado', '=', 0],                                                        
                        ])->get();   
        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],  
                            ['idalmacen', '=', $arrayDatos['idalmacen']],                                                                                                           
                        ])->get();

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)                
                ->with('modelClasificacion', $modelClasificacion)            
                ->with('modelSucursales', $modelSucursales)            
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $datosTablaArt = json_decode($request->datosTablaArt); 
        for($i = 0; $i < count($datosTablaArt); $i++)
        { 
            Far_articulo_sucursal::agregarArticuloSucursal($datosTablaArt[$i],$request->sucursal);
        }  

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion);
                            //->with('message', 'store')
                            //->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_sucursales::find($codigo);  
        $model->eliminado = $model->eliminado == 0? 1 : 0;

        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_sucursales::find($id);        
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Far_sucursales::find($id); 
        $model->codigo_identificacion = strtoupper($request->codigo_identificacion);
        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);       
        $model->usuario = Auth::user()['name'];  
        
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("sucursal");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaProducto(Request $request)
    {          
        // if(isset($_POST['txtBuscador']) && isset($_POST['txtSucursal']) && isset($_POST['txtClasificacion']))
        // {
        //     $dato = $_POST['txtBuscador'];
        //     $sucursal = $_POST['txtSucursal'];
        //     $clasificacion = $_POST['txtClasificacion'];

        //     $modelo = Far_articulo_sucursal::where([
        //                 ['idfar_estado', '=', Far_estado::AUTORIZADO],
        //                 ['idfar_sucursales', '=', $sucursal], 
        //                 ['idfar_adm_clasificacion', '=', $clasificacion],
        //                 //['idinv_almacenes', Session::get('session_idAlmacenes') > 0? '=' : '!=', Session::get('session_idAlmacenes')],
        //                 ])
        //                 ->where(function($query) use($dato) {
        //                     $query->orwhere('codificacion', 'ilike', '%'.$dato.'%');
        //                     $query->orwhere('descripcion', 'ilike', '%'.$dato.'%');
        //                     // $query->orwhere('precio', '=', $dato);
        //                 })
        //                 ->orderBy('descripcion', 'ASC')
        //                 ->get();
                        
        //     if(count($modelo) > 0)
        //     {
        //         return $modelo->toJson();
        //     }
        //     else
        //         return 0;
        // } 

        if(isset($_POST['dato']))
        {
            // $arrayDatos = Home::parametrosSistema();
            $dato = $_POST['dato'];
            $datosBuscador = json_decode($dato);
            if(Session::has('session_idhcl_cuaderno'))
            {
                $idhcl_cuaderno = Session::get('session_idhcl_cuaderno');
                $modelHcl_cuaderno = Hcl_cuaderno::find($idhcl_cuaderno);                    
            }

            if($datosBuscador != null)
            { 
                $codigo = isset($datosBuscador->codigo)? $datosBuscador->codigo : null;
                $medicamento = isset($datosBuscador->medicamento)? $datosBuscador->medicamento : null;
            }
            
            $modelo = DB::table('far_articulo_sucursal as a')
                    ->join('far_adm_articulo as b','b.id','=','a.idfar_adm_articulo')                    
                    ->join('hcl_cuaderno as c','c.idfar_sucursales','=','a.idfar_sucursales')
                    ->select('a.id','b.codificacion','b.descripcion','b.unidad','b.concentracion','a.cantidad_max_salidad','a.saldo','a.indicacion','a.dispensar_cuaderno')
                        ->where([
                                 ['b.codificacion', 'ilike', '%'.$codigo.'%'], 
                                 ['a.idfar_estado', '=', Far_estado::AUTORIZADO],
                                 // ['ia.idalmacen', '=', $arrayDatos['idalmacen']],
                                 // ['hc.id', '=', $modelHcl_cuaderno->id],
                                 ['c.id', '=', 10136],
                              ])
                        ->groupBy('a.id','b.codificacion','b.descripcion','b.unidad','b.concentracion','a.cantidad_max_salidad','a.saldo','a.indicacion','a.dispensar_cuaderno')
                        ->where(function($query) use($medicamento) {                            
                            $query->orwhere('b.descripcion', 'ilike', '%'.$medicamento.'%');
                        })
                        ->orderBy('b.codificacion', 'ASC')
                        ->get();
            if(count($modelo) > 0)
            {
                return $modelo->toJson();
            }
            else
                return 0;
        }
    }

    public function buscaProductoEgreso(Request $request)
    {          
        if(isset($_POST['txtBuscador']) && isset($_POST['txtSucursal']) && isset($_POST['txtClasificacion']))
        {
            $dato = $_POST['txtBuscador'];
            $sucursal = $_POST['txtSucursal'];
            $clasificacion = $_POST['txtClasificacion'];

            $modelo = Far_articulo_sucursal::where([
                        ['idfar_estado', '=', Far_estado::AUTORIZADO],
                        ['idfar_sucursales', '=', $sucursal], 
                        ['idfar_adm_clasificacion', '=', $clasificacion],
                        ['saldo', '>', 0],
                        ])
                        ->where(function($query) use($dato) {
                            $query->orwhere('codificacion', 'ilike', '%'.$dato.'%');
                            $query->orwhere('descripcion', 'ilike', '%'.$dato.'%');
                            // $query->orwhere('precio', '=', $dato);
                        })
                        ->orderBy('descripcion', 'ASC')
                        ->get();
                        
            if(count($modelo) > 0)
            {
                return $modelo->toJson();
            }
            else
                return 0;
        }  
    }

    public function productoDispensacion(Request $request)
    {           
        // if(isset($_POST['txtBuscador']) && isset($_POST['txtSucursal']) && isset($_POST['txtClasificacion']))
        // {
        //     $dato = $_POST['txtBuscador'];
        //     $sucursal = $_POST['txtSucursal'];
        //     $clasificacion = $_POST['txtClasificacion'];

        //     $modelo = Far_articulo_sucursal::where([
        //                 ['idfar_estado', '=', Far_estado::AUTORIZADO],
        //                 ['idfar_sucursales', '=', $sucursal], 
        //                 ['idfar_adm_clasificacion', '=', $clasificacion],
        //                 //['idinv_almacenes', Session::get('session_idAlmacenes') > 0? '=' : '!=', Session::get('session_idAlmacenes')],
        //                 ])
        //                 ->where(function($query) use($dato) {
        //                     $query->orwhere('codificacion', 'ilike', '%'.$dato.'%');
        //                     $query->orwhere('descripcion', 'ilike', '%'.$dato.'%');
        //                     // $query->orwhere('precio', '=', $dato);
        //                 })
        //                 ->orderBy('descripcion', 'ASC')
        //                 ->get();
                        
        //     if(count($modelo) > 0)
        //     {
        //         return $modelo->toJson();
        //     }
        //     else
        //         return 0;
        // } 

        if(isset($_POST['txtBuscador']))
        {
            // $arrayDatos = Home::parametrosSistema();
            $dato = $_POST['txtBuscador'];
            $datosBuscador = json_decode($dato); 

            $modelo = DB::table('far_articulo_sucursal') 
                    ->select('id','codificacion','descripcion','unidad','concentracion','cantidad_max_salidad','saldo','indicacion')
                    ->where([
                            ['codificacion', 'ilike', '%'.$dato.'%'], 
                            // ['idfar_estado', '=', Far_estado::AUTORIZADO],
                            // ['idfar_sucursales', '=', $sucursal], 
                            // ['idfar_adm_clasificacion', '=', $clasificacion],
                          ])
                    // ->groupBy('id','codificacion','descripcion','unidad','concentracion','cantidad_max_salidad','saldo','indicacion')
                    // ->where(function($query) use($dato) {                            
                    //     $query->orwhere('descripcion', 'ilike', '%'.$dato.'%');
                    // })
                    ->orderBy('codificacion', 'ASC')
                    ->get();
            // print_r($modelo);

            if(count($modelo) > 0)
            {
                return $modelo->toJson();
            }
            else
                return 0;
        }
    }

    public function buscaProductoCuaderno(Request $request)
    {  
        // $arrayDatos = Home::parametrosSistema(); 
        if(Session::has('session_idhcl_cuaderno'))
        {
            $idhcl_cuaderno = Session::get('session_idhcl_cuaderno');
            $modelHcl_cuaderno = Hcl_cuaderno::find($idhcl_cuaderno);                    
        } 

        // print_r($modelHcl_cuaderno);
        // return;
        
        $modelo = DB::table('far_articulo_sucursal')                    
                    ->select('id','codificacion','descripcion','unidad','concentracion','cantidad_max_salidad','saldo','indicacion','dispensar_cuaderno')
                    ->where([
                        // ['idfar_estado', '=', 0],
                        // ['ia.idalmacen', '=', $arrayDatos['idalmacen']],
                        ['idfar_sucursales', '=', $modelHcl_cuaderno->idfar_sucursales],
                        ['saldo', '!=', null],
                    ]) 
                    ->groupBy('id','codificacion','descripcion','unidad','concentracion','cantidad_max_salidad'   ,'saldo','indicacion','dispensar_cuaderno')
                    ->orderBy('codificacion', 'ASC')
                    ->get();
        if(count($modelo) > 0)
        {
            return $modelo->toJson();
        }
        else
            return 0;          
    }
    // ------------------------------------------------------------------------------------------------------

}
