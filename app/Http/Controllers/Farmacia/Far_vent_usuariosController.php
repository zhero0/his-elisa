<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_vent_usuarios;
use \App\Models\Farmacia\Far_ventanillas;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class Far_vent_usuariosController extends Controller
{
    private $rutaVista = 'Farmacia.far_vent_usuarios.';
    private $controlador = 'far_vent_usuarios';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $model = DB::table('far_vent_usuarios as a')
                ->join('far_ventanillas as b', 'b.id', '=', 'a.idfar_ventanillas')
                ->join('far_sucursales as c', 'c.id', '=', 'b.idfar_sucursales')                
                ->join('datosgenericos as e', 'e.id', '=', 'a.iddatosgenericos')
                ->select('a.*','b.nombre as ventanilla', 'c.codigo_almacen as codigo_sucursal', 'c.nombre as sucursal', 'e.matricula','e.nombres','e.apellidos')
                ->where([
                    ['e.eliminado', '=', 0]
                ])
                ->where(function($query) use($dato) { 
                    $query->orwhere('e.nombres', 'like', '%'.$dato.'%'); 
                    $query->orwhere('e.apellidos', 'like', '%'.$dato.'%');
                })
                ->orderBy('e.nombres', 'ASC')
                ->paginate(config('app.pagination'));
  
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_vent_usuarios;
 
        $modelDatosgenericos = Datosgenericos::where('eliminado', '=', 0)                       
                            ->orderBy('nombres','ASC')->get();
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model) 
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        // print_r($request->iddatosgenericos);
        // return;
        // $rules = [            
        //     'iddatosgenericos' => 'required',  
        //     'idfar_ventanillas' => 'required',  
        // ];
        // $messages = [
        //     'iddatosgenericos.required' => 'Se requiere registrar Personal',  
        //     'idfar_ventanillas.required' => 'Se requiere registrar Area',  
        // ];
        // $this->validate($request, $rules, $messages);


        // print_r($request->iddatosgenericos);
        // return;


        $model = new Far_vent_usuarios;
        $model->idfar_ventanillas = $request->idfar_ventanillas;
        $model->iddatosgenericos = $request->iddatosgenericos;         
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("far_vent_usuarios");  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // print_r($arrayDatos['idalmacen']);
        // return; 

        $model = Far_vent_usuarios::find($codigo);  

        if ($model!= null) {
             if($model->delete())
        {
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        }

        return redirect("far_vent_usuarios")
                ->with('message', 'update')
                ->with('mensaje', $mensaje); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function busca_Ventanilla()
    {
        if(isset($_POST['dato']))
        {
            $arrayDatos = Home::parametrosSistema();
            $dato = $_POST['dato'];
            
            $modelo = Far_ventanillas::join('far_sucursales as b', 'b.id', '=', 'far_ventanillas.idfar_sucursales')
                        ->select('far_ventanillas.*','b.nombre as sucursal')
                        ->where([
                            ['far_ventanillas.eliminado', '=', 0],
                            ['b.idalmacen', '=', $arrayDatos['idalmacen']],
                            ['far_ventanillas.nombre', 'ilike', '%'.$dato.'%']
                        ])
                        ->orderBy('far_ventanillas.nombre', 'ASC')
                        ->get()->toJson();
            echo $modelo;
        }
    }

}
