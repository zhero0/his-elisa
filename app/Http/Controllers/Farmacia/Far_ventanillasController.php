<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_sucursales;
use \App\Models\Farmacia\far_ventanillas;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Far_ventanillasController extends Controller
{
    private $rutaVista = 'Farmacia.far_ventanillas.';
    private $controlador = 'far_ventanillas';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $modelSucursal = Far_sucursales::where([                            
                            ['idalmacen', '=', $arrayDatos['idalmacen']],
                            // ['eliminado', '=', 0],
                            // ['codigo_identificacion', 'ilike', '%'.$codigo.'%'],
                            // ['nombre', 'ilike', '%'.$nombre.'%'],
                            // ['descripcion', 'ilike', '%'.$descripcion.'%']
                        ]) 
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));

        $model = Far_ventanillas::where([                            
                            // ['idalmacen', '=', $arrayDatos['idalmacen']],
                            // ['eliminado', '=', 0],
                            // ['codigo_identificacion', 'ilike', '%'.$codigo.'%'],
                            // ['nombre', 'ilike', '%'.$nombre.'%'],
                            // ['descripcion', 'ilike', '%'.$descripcion.'%']
                        ]) 
                ->orderBy('nombre', 'ASC')
                ->paginate(config('app.pagination'));
        
        $model->idalmacen = $arrayDatos['idalmacen'];                
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelSucursal', $modelSucursal)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_ventanillas;

        $modelSucursales  = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],      
                        ])->get();
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelSucursales', $modelSucursales)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Far_ventanillas;        
        $model->descripcion = strtoupper($request->descripcion);
        $model->nombre = strtoupper($request->nombre);;        
        $model->idfar_sucursales = $request->sucursal;
        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("far_ventanillas");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
         
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

       
        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
  
        // Session::put('urlAnterior', URL::previous());

        $model = Far_ventanillas::find($id);
        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],      
                        ])->get(); 
  
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)    
                ->with('modelSucursales', $modelSucursales)            
                ->with('arrayDatos', $arrayDatos);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Far_ventanillas::find($id);       
        $model->descripcion = strtoupper($request->descripcion);
        $model->nombre = strtoupper($request->nombre);
        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("far_ventanillas");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
