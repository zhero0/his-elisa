<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_adm_tipotransaccion; 
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission; 
use \App\Models\Usuario\Almacenusuario;

use Session;
use Illuminate\Support\Facades\URL;

class Far_adm_tipotransaccionController extends Controller
{
    private $rutaVista = 'Farmacia.far_adm_tipotransaccion.';
    private $controlador = 'far_adm_tipotransaccion';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema(); 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search;
        $estado = $request->estado;
        $estados = array(            
            ['id' => '0','nombre'=>'Vigente'],
            ['id' => '1','nombre'=>'No Vigente'],);   

        $model = Far_adm_tipotransaccion::where([
                            ['eliminado', '=', 0],                            
                        ])
                ->where(function($query) use($dato) {
                    $query->orwhere('descripcion', 'ilike', '%'.$dato.'%');                    
                    $query->orwhere('codigo', 'ilike', '%'.$dato.'%');                    
                })
                ->orderBy('codigo', 'ASC')
                ->paginate(config('app.pagination'));

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->estado = $estado; 
        $model->almacen = $request->almacen;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('estados', $estados)                
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_adm_tipotransaccion;  

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->idalmacen = $arrayDatos['idalmacen'];

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)                
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    { 
        // $rules = [            
        //     'codificacion' => 'codificacion', 
        //     'descripcion' => 'descripcion', 
        // ];
        // $messages = [
        //     'codificacion.required' => 'Se requiere registrar codificacion', 
        //     'descripcion.required' => 'Se requiere registrar descripcion',             
        // ];
        // $this->validate($request, $rules, $messages);
 
        $model = new Far_adm_tipotransaccion;
        $model->tipo = strtoupper($request->tipo);
        $model->codigo = strtoupper($request->codigo);
        $model->descripcion = strtoupper($request->descripcion);
        $model->tipo_documento = $request->tipo_documento;

        // $model->idinv_almacenes = $request->almacen;
        $model->usuario = Auth::user()['name'];
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
                             
        }
        else
            return redirect("inv_tipo_transaccion"); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_adm_tipotransaccion::find($codigo);        
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))                
                ->with('arrayDatos', $arrayDatos); 
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Far_adm_tipotransaccion::find($id);
        // $modelAlmacenes= Inv_almacenes::join('Inv_cuaderno_datosgenericos', 'inv_almacenes.id', '=', 'Inv_cuaderno_datosgenericos.idinv_almacenes')                            
        //                     ->select('inv_almacenes.id','inv_almacenes.nombre')
        //                     ->where([
        //                     ['inv_almacenes.eliminado', '=', 0], 
        //                     // ['inv_almacenes.idlaboratorio', '=', Inv_almacenes::FARMACIA],
        //                     ['inv_almacenes.idalmacen', '=', $arrayDatos['idalmacen']],                           
        //                     ['inv_cuaderno_datosgenericos.iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
        //                 ])->get();
        
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'edit')
                ->with('model', $model)                   
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $rules = [            
        //     'codificacion' => 'codificacion', 
        //     'descripcion' => 'descripcion', 
        // ];
        // $messages = [
        //     'codificacion.required' => 'Se requiere registrar codificacion', 
        //     'descripcion.required' => 'Se requiere registrar descripcion',             
        // ];
        // $this->validate($request, $rules, $messages);

        
        $model = Far_adm_tipotransaccion::find($id); 
        $model->tipo = strtoupper($request->tipo);
        $model->codigo = strtoupper($request->codigo);
        $model->descripcion = strtoupper($request->descripcion);
        $model->tipo_documento = $request->tipo_documento;

        
        $model->usuario = Auth::user()['name'];

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');        
       
        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("inv_tipo_transaccion"); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function buscaTransaccion()
    {
        $arrayDatos = Home::parametrosSistema();

        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato']; 
            $modelo = Inv_tipo_transaccion::where([
                                ['eliminado', '=', 0],                                
                            ])
                            ->select('id', 'codigo','descripcion as descripcion','tipo_documento')
                            ->orderBy('descripcion', 'ASC')
                            ->get()->toJson(); 
            echo $modelo;
        }
    }

    public function buscaFormulario()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];

            $modelFormularios = Far_adm_tipotransaccion::where([
                                ['eliminado', '=', 0],                                
                                ['tipo', '=', Far_adm_tipotransaccion::DISPENSACION],                                
                            ])
                ->where(function($query) use($dato) {                    
                    $query->orwhere('descripcion', 'ilike', '%'.$dato.'%'); 
                })->get();

            echo json_encode($modelFormularios);
        }
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}
