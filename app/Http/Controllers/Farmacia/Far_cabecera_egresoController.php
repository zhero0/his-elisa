<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Auth;

use Illuminate\Http\Request; 
use \App\Models\Almacen\Almacen;
use \App\Models\Almacen\Datosgenericos;
use Illuminate\Support\Facades\DB;
use \App\Models\Almacen\Home;
use Redirect;
use \App\Permission;

use \App\Models\Inventario\Inv_tipo_transaccion;
use \App\Models\Inventario\Inv_almacenes;
use \App\Models\Inventario\Inv_proveedores;
use \App\Models\Inventario\Inv_cabecera_registro;
use \App\Models\Inventario\Inv_cab_registro_detalle;


use \App\Models\Farmacia\Far_sucursales;
use \App\Models\Farmacia\Far_estado;
use \App\Models\Farmacia\Far_proveedores;
use \App\Models\Farmacia\Far_adm_tipotransaccion;
use \App\Models\Farmacia\Far_cabecera_registro; 
use \App\Models\Farmacia\Far_adm_clasificacion;
use \App\Models\Farmacia\Far_cab_registro_detalle;

// use \App\Models\Administracion\Productonota;
use \App\Models\Inventario\Inv_articulo;

use \App\Models\Venta\Venta;

use Session;
use Illuminate\Support\Facades\URL;
use Elibyy\TCPDF\Facades\TCPDF;

class Far_cabecera_egresoController extends Controller
{
    private $rutaVista = 'Farmacia.far_cabecera_egreso.';
    private $controlador = 'far_cabecera_egreso';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $buscarFecha = $request->buscarFecha;
        $date = date_create($request->fechaRegistro);
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado; 

        $estado = $request->modelEstados; 

        $modelEstados = Far_estado::get();
        
        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],                                                     
                        ])->get();

        $modelTipo_transaccion = Far_adm_tipotransaccion::where([ 
                            ['eliminado', '=', 0],   
                            ['tipo', '=', Far_adm_tipotransaccion::EGRESO],   
                        ])->get();

        $modelProveedores = Far_proveedores::where([ 
                            ['eliminado', '=', 0],                                                       
                        ])->get();

        $model = Far_cabecera_registro::where([ 
                            ['tipo', '=', Far_adm_tipotransaccion::EGRESO],
                            // ['idfar_adm_clasificacion', '=', $clasificacion],
                            // ['codificacion', 'ilike', '%'.$codigo.'%'],
                            // ['descripcion', 'ilike', '%'.$descripcion.'%'],
                            // ['unidad', 'ilike', '%'.$unidad.'%'],                                                        
                        ]) 
                ->orderBy('numero_nota', 'ASC')
                ->paginate(config('app.pagination')); 

        
        // $model->estado = $estado;        

        $model->accion = $this->controlador;
        $model->scenario = 'index';        
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;

        return view($model->rutaview.'index')
                ->with('model', $model)                
                ->with('modelEstados', $modelEstados)
                ->with('modelTipo_transaccion', $modelTipo_transaccion)
                ->with('modelProveedores', $modelProveedores)
                ->with('modelSucursales', $modelSucursales)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // if($arrayDatos['idalmacen'] == 0)
        //     return view('errors.sinSucursal')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Inv_cabecera_registro; 

        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],                                                     
                        ])->get();

        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['eliminado', '=', 0],                                                       
                        ])->get();

        $modelIngresos  = Far_adm_tipotransaccion::where([
                                        ['tipo', '=', Far_adm_tipotransaccion::EGRESO],
                                        ['eliminado', '=', 0]
                                    ])
                                ->get();  

        // if(Session::has('session_idAlmacenes'))
        // {
        //    $model->idalmacenes = Session::get('session_idAlmacenes');   
        // }  

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelIngresos', $modelIngresos)
                ->with('modelClasificacion', $modelClasificacion)
                ->with('modelSucursales', $modelSucursales)                
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
        // print_r($request->txtHiddenIdSucursal);
        // return;
        $gridItem = json_decode($request->arrayDatos);
        
                
        $mensaje = Far_cabecera_registro::registrarNota_ingreso($request, $gridItem, Far_adm_tipotransaccion::EGRESO);
        
 
        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("notaIngreso");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $model = Inv_cabecera_registro::find($id);
        $model->proveedor = $model->id_datosgenericos != null? 
                                    Datosgenericos::find($model->id_datosgenericos)->nombres : '';
        
        // $model->productoNota = DB::table('inv_cabecera_registro as pn')
        //             ->join('inv_articulo as p', 'p.id', '=', 'pn.idinv_articulo')
        //             ->select('p.id as idproducto', 'p.descripcion as nombre', 'pn.ingreso as cantidad', 'pn.costo')
        //             ->where([
        //                     ['pn.idnota', '=', $model->id]
        //                 ])
        //             ->orderBy('p.descripcion', 'ASC')
        //             ->get()->toJson();

         $model->productoNota = DB::table('inv_cab_registro_detalle as pn')
                    ->join('inv_articulo as p', 'p.id', '=', 'pn.idinv_articulo')
                    ->select('p.id as idproducto', 'p.descripcion as nombre', 'pn.ingreso as cantidad', 'pn.costo')
                    ->where([
                            ['pn.id', '=', $model->id]
                        ])
                    ->orderBy('p.descripcion', 'ASC')
                    ->get()->toJson();
        
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'view')
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $model = Far_cabecera_registro::find($id);
 

        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['id', '=', $model->idfar_sucursales],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],                                                     
                        ])->get();

        $modelClasificacion = Far_adm_clasificacion::where([ 
                            ['id', '=', $model->idfar_adm_clasificacion], 
                            ['eliminado', '=', 0],                                                       
                        ])->get();

        // $model->proveedor = Far_proveedores::find($model->idfar_proveedores)->toJson(); 

        $modelIngresos  = Far_adm_tipotransaccion::where([
                                        ['tipo_documento', '=', $model->tipo_documento],
                                        ['eliminado', '=', 0]
                                    ])
                                ->get();  
        
        $model->productoNota = DB::table('far_cab_registro_detalle as pn')
                    ->join('far_articulo_sucursal as p', 'p.id', '=', 'pn.idfar_articulo_sucursal')
                    ->select('p.id as idproducto', 'p.descripcion as nombre', 'pn.salida as cantidad', 'pn.costo','pn.lote as lote', 'pn.fecha_vencimiento', 'pn.id as idfar_cab_registro_detalle', 'p.codificacion as codificacion','pn.saldo as total')
                    ->where([
                            ['pn.eliminado', '=', 0],  
                            ['pn.idfar_cabecera_registro', '=', $model->id]
                        ])
                    ->orderBy('p.descripcion', 'ASC')
                    ->get()->toJson();
 
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelSucursales', $modelSucursales)
                ->with('modelClasificacion', $modelClasificacion)
                ->with('modelIngresos', $modelIngresos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gridItem = json_decode($request->arrayDatos);
 
        $model = Far_cabecera_registro::find($id); 

        $modelTipo  = Far_adm_tipotransaccion::where([ 
                                        ['eliminado', '=', 0],
                                        ['id', '=', $request->txtHiddenIdIngreso]
                                    ])
                                ->first();
        
        $model->total = $request->txtHiddenTotal;
        $model->idfar_proveedores = $request->txtHiddenIdProveedor;
        $model->idfar_sucursales = $request->txtHiddenIdSucursal;
        $model->idfar_adm_tipotransaccion = $modelTipo->id;
        $model->tipo_documento = $modelTipo->tipo_documento; 

        if($model->save())
        {
            Far_cab_registro_detalle::registraProductoNota($model->id, $model->numero_nota, $gridItem, $modelTipo->tipo);
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("notaIngreso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //Productonota::where('idnota', '=', $id)->delete();
        //Nota::where('id', '=', $id)->delete();

        $id = $request->idnota;
        
        $ProductosNoEliminar = Inv_cab_registro_detalle::join('inv_articulo', 'inv_cab_registro_detalle.idinv_articulo', '=', 'inv_articulo.id')
                                ->where([
                                        ['inv_cab_registro_detalle.idinv_cabecera_registro', '=', $id],
                                        ['inv_cab_registro_detalle.ingreso', '>', DB::raw('inv_articulo.saldo')]
                                    ])
                                ->count();
        if($ProductosNoEliminar > 0)
            return redirect("notaIngreso")
                ->with('message', 'destroyError')
                ->with('mensaje', 'NO puede ser Eliminado, el saldo de algunos productos quedará en NEGATIVO! ');

        $modelProductoNota = Inv_cab_registro_detalle::where('idinv_cabecera_registro', '=', $id)->get();
        for($i = 0; $i < count($modelProductoNota); $i++)
        {
            $idproducto = $modelProductoNota[$i]['idinv_articulo'];
            $modelProducto = Inv_articulo::find($idproducto);

            $modelProducto->saldo = $modelProducto->saldo - $modelProductoNota[$i]['ingreso'];
            // $modelProducto->saldo_importe = $modelProducto->saldo_importe - $modelProductoNota[$i]['ingresoimporte'];
            $modelProducto->save();
        }
        
        // Productonota::where('idnota', '=', $id)->update(['eliminado' => true]);
        $eliminado = Inv_cabecera_registro::where('id', '=', $id)
                        ->update([
                            'eliminado' => true,
                            'usuarioanulacion' => Auth::user()['name'],
                            'fechaanulacion' => date('Y-m-d'),
                            'descripcionanulacion' => strtoupper($request->descripcionanulacion),
                        ]);
        $mensaje = $eliminado == 1? config('app.mensajeDestroy') : config('app.mensajeErrorDestroy');
        Inv_cabecera_registro::corregirKardexProductos(1);
        
        return redirect("notaIngreso")
                ->with('message', 'destroy')
                ->with('mensaje', $mensaje);
    }

    public function eliminarNotaEgreso()
    {
        if(isset($_POST['opcion']))
        {
            $opcion = $_POST['opcion'];
            
            Far_cab_registro_detalle::eliminarProductoNota($opcion);
            echo 'Actualizado correctamente....';
        }
    }

    public function corregirKardexProductos()
    {
        if(isset($_POST['opcion']))
        {
            $opcion = $_POST['opcion'];
            
            Nota::corregirKardexProductos($opcion);
            echo 'Actualizado correctamente....';
        }
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [ REPORTES ] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeInformeIngresos($id)
    {
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetTitle("ingreso");
        $pdf::SetSubject("TCPDF");

        // ==================================================================================================
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            $pdf->Image('imagenes/inventario2.jpg', 12, '', '', 18);
            $pdf->SetFont('courier', 'B', 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $pdf->MultiCell('', 12, 'DETALLE DE INGRESOS', 0, 'C', '', 1, '', '', 1, '', '', '', 12, 'M');
            $pdf->SetFont('courier', '', 10);
            $pdf->MultiCell('', 6, 'Dirección: Junin Nº 752', 0, 'C', '', 1, '', '', 1, '', '', '', 6, 'M');
        });
        
        // setPage
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha) {
            $pdf->SetY(-10);
            $pdf->SetFont('courier', 'B', 7);
            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-10);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-10);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });
        // ==================================================================================================
        
        // $pdf::AddPage('P', 'Legal');
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 30;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 100;
        $espacio = 2;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $modelNota = DB::table('nota as n')
                    ->join('tipodocumento as td', 'td.id', '=', 'n.idtipodocumento')
                    ->join('proveedor as p', 'p.id', '=', 'n.idproveedor')
                    ->select('n.*', 'p.nit', 'p.nombre as proveedor')
                    ->where([
                            ['n.eliminado', '=', 0],
                        ])
                    ->orderBy('n.numero', 'ASC')
                    ->get();

        for($j = 0; $j < count($modelNota); $j++)
        {
            $pdf::AddPage('P', 'Legal');
            $pdf::SetY($y_Inicio);

            $id = $modelNota[$j]->id;
            $model = Nota::find($id);
            
            $pdf::SetFont($tipoLetra, 'B', 12);
            $pdf::MultiCell($width, 7, 'INGRESO Nº '.$model->numero, 0, 'C', 0, 1, '', '', true, 0, false, true, 7, 10);
            $pdf::SetFont($tipoLetra, '', $tamanio);

            $pdf::MultiCell($widthLabel, $height, 'PROVEEDOR', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthDosPuntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, $modelNota[$j]->proveedor, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell($widthLabel, $height, 'FECHA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthDosPuntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, date('d-m-Y', strtotime($model->fecha)), 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell($widthLabel, $height, 'USUARIO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthDosPuntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, $model->usuario, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell(100, $height, 'PRODUCTO', 'TBLR', 'L', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell(25, $height, 'CANTIDAD', 'TBR', 'R', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell(25, $height, 'COSTO', 'TBR', 'R', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell(25, $height, 'SUB TOTAL', 'TBR', 'R', 1, 1, '', '', true, 0, false, true, $height);

            $modelDocumentoproducto = DB::table('productonota as pn')
                        ->join('producto as p', 'p.id', '=', 'pn.idproducto')
                        ->select('pn.*', 'p.nombre as producto')
                        ->orderBy('p.nombre', 'ASC')
                        ->get();
            $total = 0;

            for($i = 0; $i < count($modelDocumentoproducto); $i++)
            {                
                // -------------------------------------------------------------------------------------------------
                $y_producto = ceil($pdf::getStringHeight(100, trim($modelDocumentoproducto[$i]->producto), $reseth = true, $autopadding = true, $border = 0));
                $y = max(array($y_producto)) + $height;
                $valor1 = $pdf::getY() + $y + $height + $height;
                $valor2 = $pdf::getPageHeight() - $y;
                if ($valor1 > $valor2)
                {
                    $pdf::AddPage('P', 'Legal');
                    $pdf::SetY($y_Inicio);

                    $pdf::MultiCell(100, $height, 'PRODUCTO', 'LBT', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                    $pdf::MultiCell(25, $height, 'CANTIDAD', 'LBT', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
                    $pdf::MultiCell(25, $height, 'COSTO', 'LBT', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
                    $pdf::MultiCell(25, $height, 'SUB TOTAL', 'LBTR', 'R', 0, 1, '', '', true, 0, false, true, $height, 10);
                }
                // -------------------------------------------------------------------------------------------------

                $pdf::MultiCell(100, $y_producto, $modelDocumentoproducto[$i]->producto, 'LBR', 'L', 0, 0, '', '', true, 0, false, true, $y_producto, 10);
                $pdf::MultiCell(25, $y_producto, $modelDocumentoproducto[$i]->ingreso, 'BR', 'R', 0, 0, '', '', true, 0, false, true, $y_producto, 10);
                $pdf::MultiCell(25, $y_producto, $modelDocumentoproducto[$i]->costo, 'BR', 'R', 0, 0, '', '', true, 0, false, true, $y_producto, 10);
                $subTotal = $modelDocumentoproducto[$i]->ingreso * $modelDocumentoproducto[$i]->costo;
                $pdf::MultiCell(25, $y_producto, number_format($subTotal, 2, '.', ','), 'BR', 'R', 0, 1, '', '', true, 0, false, true, $y_producto, 10);

                $total += $subTotal;
            }
            //$pdf::Line(10, $pdf::getY(), $width, $pdf::getY());


            // -------------------------------------- [FINAL DE LA HOJA] --------------------------------------
            $pdf::SetFont($tipoLetra, 'B', 10);
            $textoTotalventa = 'TOTAL';

            // totalAncho = 175
            $width1 = 130;
            $width2 = 15;
            $width3 = 5;
            $width4 = 25;
            
            $y_textoTotalventa = ceil($pdf::getStringHeight(70, trim($textoTotalventa), $reseth = true, $autopadding = true, $border = 1));
            
            $y = max(array($y_textoTotalventa));
            $valor1 = $pdf::getY() + $y + 15;
            $valor2 = $pdf::getPageHeight() - $y - 15;
            if ($valor1 > $valor2)
            {
                $pdf::AddPage('P', 'Legal');
                $pdf::SetY($y_Inicio);
            }
            
            $pdf::MultiCell($width1, $height, '', 'T', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($width2, $height, $textoTotalventa, 'T', 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($width3, $height, ': ', 'T', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($width4, $height, number_format($total, 2, '.', ','), 'T', 'R', 0, 1, '', '', true, 0, false, true, $height);
            // ----------------------------------------------------------------------------------------------------------------
        }

        // set javascript
        $pdf::IncludeJS('print(true);');
            
        $pdf::Output("informeVentas.pdf", "I");

        // mb_internal_encoding('utf-8');
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}