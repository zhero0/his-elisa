<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Auth;

use Illuminate\Http\Request; 
use \App\Models\Almacen\Almacen;
use \App\Models\Administracion\Datosgenericos;
use Illuminate\Support\Facades\DB;
use \App\Models\Almacen\Home;
use \App\Models\Usuario\Almacenusuario;
use Redirect;
use \App\Permission;
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_poblacion_afiliacion;
use \App\Models\Afiliacion\Hcl_empleador;

use \App\Models\Inventario\Inv_tipo_transaccion;
use \App\Models\Inventario\Inv_almacenes;
use \App\Models\Inventario\Inv_proveedores;
use \App\Models\Inventario\Inv_cabecera_registro;
use \App\Models\Inventario\Inv_cabecera_receta;
use \App\Models\Inventario\Inv_cab_registro_detalle;
use \App\Models\Inventario\Inv_articulo;
use \App\Models\Inventario\Inv_estado;

use \App\Models\Farmacia\Far_cabecera_receta;
use \App\Models\Farmacia\Far_adm_estado;
use \App\Models\Farmacia\Far_vent_usuarios;
use \App\Models\Farmacia\Far_sucursales;
 
use \App\Models\Venta\Venta;

use Session;
use Illuminate\Support\Facades\URL;
use Elibyy\TCPDF\Facades\TCPDF;

class Far_cabecera_recetaController extends Controller
{
    private $rutaVista = 'Farmacia.far_cabecera_receta.';
    private $controlador = 'far_cabecera_receta';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema(); 
        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");

        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', ''); 

        // $buscarFecha = $request->buscarFecha;
        $date = date_create($request->fechaRegistro);
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado;

        $estados = Far_adm_estado::get();       
        $modelAlmacenes = DB::table(DB::raw("far_lista_sucursales_usuario_completo('".$arrayDatos['idalmacen']."','".$arrayDatos['iddatosgenericos']."')"))
                                ->orderBy('id', 'DESC')
                                ->get();  
 
        $estado = $request->estado == ''? 0 : $request->estado; 
        $almacen = $request->idfar_sucursales == ''? 0 : $request->idfar_sucursales; 

        $folio = 0;
        $hcl_poblacion_cod = '';
        $hcl_poblacion_nombrecompleto = 'luis';
        $idU = 3;
        $estadoPrueba = 1; 
        
        //$model = DB::table(DB::raw("far_lista_dispensados('".$folio."','".$hcl_poblacion_cod."','".$hcl_poblacion_nombrecompleto."','".$estado."','".$almacen."')"))  
        $model = DB::table(DB::raw("far_lista_dispensados('".$almacen."','".$arrayDatos['iddatosgenericos']."','".$estado."','".$almacen."')"))
                                ->orderBy('res_folio', 'DESC')
                                //->get();  //error de render aqui
                                ->paginate(config('app.pagination'));

        // print_r($estados);
        // return;

 
        $searchCode = $request->txtsearchCode;
        $model->searchCode = $searchCode; 
        $model->estado = $estado;
        $model->idfar_sucursales = $almacen; 

        if($model->idfar_sucursales > 0)
            session(['session_idfar_sucursales' => $model->idfar_sucursales]);
        else
        {
            if(isset($modelAlmacenes[0]))
                session(['session_idfar_sucursales' => $modelAlmacenes[0]->id]); //aqui era el error res_id
        }
  
        $model->accion = $this->controlador; 
        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
 

        return view($model->rutaview.'index') 
                ->with('model', $model)    
                ->with('estados', $estados)             
                ->with('modelAlmacenes', $modelAlmacenes)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        // if($arrayDatos['idalmacen'] == 0)
        //     return view('errors.sinSucursal')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

       
        $model = new Far_cabecera_receta;         
    
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        $model->numeroFolio = 0;
 
        $modelDatosgenericos = Datosgenericos::find($arrayDatos['iddatosgenericos']);        
        $model->iddatosgenericos = $modelDatosgenericos->id;
        $model->usuario = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;



        if(Session::has('session_idfar_sucursales'))
        {
            $idfar_sucursales = Session::get('session_idfar_sucursales');
            $ventanilla = DB::table(DB::raw("far_lista_sucursales_usuario('".$idfar_sucursales."','".$arrayDatos['iddatosgenericos']."')"))                                                    
                                                    ->first();
            // print_r($ventanilla);
            // return;

            $model->idfar_sucursales = $idfar_sucursales;
            $model->sucursal = $ventanilla->res_nombre_sucursal;
            $model->idfar_ventanillas = $ventanilla->idfar_ventanillas;
            $model->ventanilla = $ventanilla->res_nombre_ventanilla;

            // print_r($ventanilla);
            // return;
        }
        else
            $vista = 'cuadernoVacio';   

        $numeroFolio = Session::get('numeroFolio');
        if(isset($numeroFolio))
        {
            $model->numeroFolio = $numeroFolio;
        }

        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'create') 
                ->with('model', $model)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $arrayDatos = Home::parametrosSistema();
        // $gridItem = json_decode($request->arrayDatos);
        // $mensaje = Inv_cabecera_registro::registrarNota($request, $gridItem, Inv_tipo_transaccion::EGRESO);
        // $idalmacen = Session::get('session_idAlmacenes'); 

        // ESTA PARTE ES MANUAL
        // if ($request->txtEstado == Inv_estado::MANUAL)
        // {            
        //     $gridDocumentodetalle = json_decode($request->txtcontenidoTabla); 
        //     $modelPoblacion = Hcl_poblacion::join('hcl_poblacion_afiliacion as b','b.idpoblacion','=','hcl_poblacion.id')
        //         ->join('hcl_empleador as c','c.id','=','b.idempleador')
        //         ->select('hcl_poblacion.id','hcl_poblacion.nombre','hcl_poblacion.primer_apellido','hcl_poblacion.segundo_apellido','hcl_poblacion.numero_historia','hcl_poblacion.codigo','hcl_poblacion.sexo','hcl_poblacion.fecha_nacimiento','hcl_poblacion.telefono','hcl_poblacion.domicilio','hcl_poblacion.documento','c.nro_empleador','c.nombre as empresa') 
        //         ->where([
        //             ['hcl_poblacion.id', '=', $request->txtidhcl_poblacion]
        //         ])->first();

        //     $model = DB::table('inv_cabecera_receta as icr')                    
        //                 // ->select('icr.*')
        //                 ->selectraw('max(icr.folio) as folio')
        //                 ->where([                           
        //                         ['idinv_almacenes', Session::get('session_idAlmacenes') > 0? '=' : '!='  , Session::get('session_idAlmacenes')],
        //                         ['idinv_tipo_transaccion', '=', $request->txtinv_tipo_transaccion],
        //                         ])
        //                     ->first();

        //     $modelo = new Inv_cabecera_receta;         
        //     $modelo->numero = 0;
        //     $modelo->folio = $model->folio +1;
        //     $modelo->cod_barra = 0;
        //     // la gestion no estoy utilizando --- hay que hacer una mejora a la gestion
        //     $modelo->idinv_gestion = 1;
        //     $modelo->idinv_tipo_transaccion = $request->txtinv_tipo_transaccion;
        //     $modelo->idhcl_poblacion = $request->txtidhcl_poblacion;
        //     $modelo->idhcl_cobertura = $request->txtidcobertura;
        //     $modelo->idhcl_cuaderno = $request->idhcl_cuaderno;
        //     $modelo->idinv_estado = $request->txtEstado;
        //     $modelo->diagnostico = '';
        //     $modelo->iddatosgenericos = $request->iddatosgenericos;
        //     $modelo->fechadispensacion = date('d-m-Y');
        //     $modelo->horadispensacion = date('h:i A');
        //     //$modelo->hcl_poblacion_nombrecompleto = $modelPoblacion->nombre.' '.$modelPoblacion->primer_apellido.' '.$modelPoblacion->segundo_apellido;
        //     //$modelo->hcl_poblacion_matricula = $modelPoblacion->numero_historia;
        //     //$modelo->hcl_poblacion_cod = $modelPoblacion->codigo;
        //    // $modelo->hcl_poblacion_sexo = $modelPoblacion->sexo == 1? 'MAS':'FEM';
        //     //$modelo->hcl_poblacion_fechanacimiento = $modelPoblacion->fecha_nacimiento;
        //     //$modelo->hcl_poblacion_telefono = $request->txtidtelefono;
        //     //$modelo->hcl_poblacion_domicilio = $request->txtiddomicilio;
        //     //$modelo->hcl_poblacion_documento = $request->txtiddocumento;
        //     //$modelo->hcl_empleador_patronal = $modelPoblacion->nro_empleador;
        //     //$modelo->hcl_empleador_empresa = $modelPoblacion->empresa;
        //     $modelo->idinv_almacenes = Session::get('session_idAlmacenes');
        //     $modelo->usuario = Auth::user()['name'];

        //     // if ($modelPoblacion->telefono != '' || $modelPoblacion->domicilio != '' || $modelPoblacion->documento != '') {
        //     Hcl_poblacion::where([['eliminado', '=', 0],
        //                             ['hcl_poblacion.id', '=', $request->txtidhcl_poblacion]
        //                         ])->update(['telefono' => $request->txtidtelefono,
        //                         'domicilio' => $request->txtiddomicilio,
        //                         'documento' => $request->txtiddocumento]);    
                         
        //     if($modelo->save())
        //     {            
        //         Inv_cabecera_receta::registraDocumentoproducto($modelo, $gridDocumentodetalle);
        //         $mensaje = 'Se ha guardado Correctamente! ';
        //     }
        //     else
        //         $mensaje = 'El registro ya tiene Folio! '; 
        // }


        // // ESTO ES POR SISTEMA

        // if ($request->txtEstado == Inv_estado::SISTEMA) {
        //     $modelo = Inv_cabecera_receta::find($request->idinv_cabecera_receta);        
        //     if ($modelo->folio<0) {
        //         $model = DB::table('inv_cabecera_receta as icr')                    
        //                 // ->select('icr.*')
        //                 ->selectraw('max(icr.folio) as folio')
        //                 ->where([                           
        //                         ['idinv_almacenes', Session::get('session_idAlmacenes') > 0? '=' : '!='  , Session::get('session_idAlmacenes')],
        //                         // ['id', '=', $request->idinv_cabecera_receta],
        //                         ['idinv_tipo_transaccion', '=', $request->txtinv_tipo_transaccion],
        //                         ])
        //                     ->first();

        //         $folio = $model->folio +1;
        //         $modelo->folio = $folio;

        //         if($modelo->save())
        //         {
        //             $mensaje = 'Se ha guardado Correctamente! ';
        //         }
        //         else
        //             $mensaje = 'El registro ya tiene Folio! ';
        //     }else
        //         $mensaje = 'El registro ya tiene Folio! ';
        // }
      
        


        // return redirect()->back()->with('numeroFolio', $modelo->folio)
        //                 ->with('message', 'store')
        //                 ->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrayDatos = Home::parametrosSistema();
        
        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        // $model = Inv_cabecera_registro::find($id);
        // // $model->proveedor = $model->id_datosgenericos != null? 
        //                             // Datosgenericos::find($model->id_datosgenericos)->nombres : '';
        
        // // $model->productoNota = DB::table('inv_cabecera_registro as pn')
        // //             ->join('inv_articulo as p', 'p.id', '=', 'pn.idinv_articulo')
        // //             ->select('p.id as idproducto', 'p.descripcion as nombre', 'pn.ingreso as cantidad', 'pn.costo')
        // //             ->where([
        // //                     ['pn.idnota', '=', $model->id]
        // //                 ])
        // //             ->orderBy('p.descripcion', 'ASC')
        // //             ->get()->toJson();

        //  $model->productoNota = DB::table('inv_cab_registro_detalle as pn')
        //             ->join('inv_articulo as p', 'p.id', '=', 'pn.idinv_articulo')
        //             ->select('p.id as idproducto', 'p.descripcion as nombre', 'pn.ingreso as cantidad', 'pn.costo')
        //             ->where([
        //                     ['pn.id', '=', $model->id]
        //                 ])
        //             ->orderBy('p.descripcion', 'ASC')
        //             ->get()->toJson();
        
        // $model->accion = $this->controlador;
        // $model->scenario = 'view';
        // $model->rutaview = $this->rutaVista;
        
        // Session::put('urlAnterior', URL::previous());
        // return view($model->rutaview.'view')
        //         ->with('model', $model)
        //         ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();        
        
        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);
        
        $model = Inv_cabecera_registro::find($id);
        // $model->proveedor = $model->idinv_proveedores != null? 
        //                             Inv_proveedores::find($model->idinv_proveedores)->nombre : '';

         // print_r($model->proveedor);
         // return;  

        $modelIngresos  = Inv_tipo_transaccion::where([
                                        ['tipo_documento', '=', $model->idTipo_documento],
                                        ['eliminado', '=', 0]
                                    ])
                                ->get();  
        
        $model->productoNota = DB::table('inv_cab_registro_detalle as pn')
                    ->join('inv_articulo as p', 'p.id', '=', 'pn.idinv_articulo')
                    ->select('p.id as idproducto', 'p.descripcion as nombre', 'pn.salida as cantidad', 'pn.costo','pn.lote as lote', 'pn.fecha_vencimiento')
                    ->where([
                            ['pn.idinv_cabecera_registro', '=', $model->id]
                        ])
                    ->orderBy('p.descripcion', 'ASC')
                    ->get()->toJson(); 
        
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelIngresos', $modelIngresos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gridItem = json_decode($request->arrayDatos);
        
        $model = Nota::find($id);
        
        $model->total = $request->txtHiddenTotal;
        $model->idproveedor = $request->txtHiddenIdProveedor;
        $model->montopagado = $request->txtMontoPagado_bd > 0? $request->txtMontoPagado_bd : 0;
        $model->producto = Venta::concatenaProductos($gridItem);
        
        if($model->save())
        {
            Productonota::where('idnota', '=', $id)->delete();
            Productonota::registraProductoNota($model->id, $model->numero, $gridItem, Tipodocumento::EGRESO);
            Nota::corregirKardexProductos(1);

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect("notaIngreso");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //Productonota::where('idnota', '=', $id)->delete();
        //Nota::where('id', '=', $id)->delete();

        $id = $request->idnota;
        
        $ProductosNoEliminar = Inv_cab_registro_detalle::join('inv_articulo', 'inv_cab_registro_detalle.idinv_articulo', '=', 'inv_articulo.id')
                                ->where([
                                        ['inv_cab_registro_detalle.idinv_cabecera_registro', '=', $id],
                                        ['inv_cab_registro_detalle.ingreso', '>', DB::raw('inv_articulo.saldo')]
                                    ])
                                ->count();
        if($ProductosNoEliminar > 0)
            return redirect("notaIngreso")
                ->with('message', 'destroyError')
                ->with('mensaje', 'NO puede ser Eliminado, el saldo de algunos productos quedará en NEGATIVO! ');

        $modelProductoNota = Inv_cab_registro_detalle::where('idinv_cabecera_registro', '=', $id)->get();
        for($i = 0; $i < count($modelProductoNota); $i++)
        {
            $idproducto = $modelProductoNota[$i]['idinv_articulo'];
            $modelProducto = Inv_articulo::find($idproducto);

            $modelProducto->saldo = $modelProducto->saldo - $modelProductoNota[$i]['ingreso'];
            // $modelProducto->saldo_importe = $modelProducto->saldo_importe - $modelProductoNota[$i]['ingresoimporte'];
            $modelProducto->save();
        }
        
        // Productonota::where('idnota', '=', $id)->update(['eliminado' => true]);
        $eliminado = Inv_cabecera_registro::where('id', '=', $id)
                        ->update([
                            'eliminado' => true,
                            'usuarioanulacion' => Auth::user()['name'],
                            'fechaanulacion' => date('Y-m-d'),
                            'descripcionanulacion' => strtoupper($request->descripcionanulacion),
                        ]);
        $mensaje = $eliminado == 1? config('app.mensajeDestroy') : config('app.mensajeErrorDestroy');
        Inv_cabecera_registro::corregirKardexProductos(1);
        
        return redirect("notaIngreso")
                ->with('message', 'destroy')
                ->with('mensaje', $mensaje);
    }


    public function corregirKardexProductos()
    {
        if(isset($_POST['opcion']))
        {
            $opcion = $_POST['opcion'];
            
            Nota::corregirKardexProductos($opcion);
            echo 'Actualizado correctamente....';
        }
    }


    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function autocompleteLote(Request $request)
    {
        $dato = $request->dato;
        $lote = $request->lote;
        $idalmacen = $request->idalmacen; 

        $data= Inv_cab_registro_detalle::join('inv_articulo', 'inv_articulo.idinv_articulo', '=', 'inv_cab_registro_detalle.id')           
                        ->join('inv_cabecera_registro', 'inv_cabecera_registro.id', '=', 'Inv_cab_registro_detalle.idinv_cabecera_registro')                 
                            ->select('Inv_cab_registro_detalle.id','Inv_cab_registro_detalle.lote')
                            ->where([
                            ['inv_cabecera_registro.idinv_almacenes', '=', $idalmacen],  
                            ['inv_cabecera_registro.eliminado', '=', 0],                           
                            ['inv_cab_registro_detalle.lote', '=', $lote],                           
                            ['inv_articulo.id', '=', $dato]
                        ])->take(10)->get(); 

        $result=array();
        foreach ($data as $key => $value)
        {
            $result[]=[
                'id' => $value->id,
                'value' => $value->lote,
                'label' => $value->lote,
            ];
        }
        return response()->json($result);
    }
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------

    // ---------------------------------------------- [BUSCA RECETA] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
 
    public function buscaReceta()
    {
        //$modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            
            $modelo = DB::table('far_cabecera_receta as icr')
                        ->join('far_sucursales as ia', 'icr.idfar_sucursales_expedido', '=', 'ia.id')
                        ->join('datosgenericos as dg', 'icr.iddatosgenericos', '=', 'dg.id')
                        ->join('hcl_cuaderno as hc', 'icr.idhcl_cuaderno', '=', 'hc.id')
                        ->join('hcl_cobertura as hco', 'icr.idhcl_cobertura', '=', 'hco.id')
                        ->join('far_adm_tipotransaccion as itt', 'icr.idfar_adm_tipotransaccion', '=', 'itt.id')
                        ->select('icr.id', 'ia.nombre', 'ia.descripcion', 'icr.numero', 'icr.cod_barra', 'icr.idhcl_poblacion', 'icr.hcl_poblacion_nombrecompleto', 'icr.hcl_poblacion_matricula', 'icr.hcl_poblacion_cod', 'icr.hcl_poblacion_fechanacimiento', 'icr.hcl_poblacion_telefono', 'icr.hcl_poblacion_domicilio', 'icr.hcl_poblacion_domicilio', 'icr.diagnostico', 'icr.fechadispensacion', 'icr.horadispensacion', 'dg.matricula','dg.nombres','dg.apellidos', 'hc.nombre as especialidad', 'hco.descripcion as cobertura','itt.id as idFormulario', 'itt.descripcion as formulario')                         
                        ->where([                           
                            ['icr.cod_barra', '=', $dato],                             
                            ])
                        ->first();

            $modeloRecetaCabecera = DB::table(DB::raw("far_lista_articulos_dispensados('".$modelo->idhcl_poblacion."')"))
                        ->where([                           
                            ['res_id', '!=', $modelo->id],                             
                            ])
                        ->get();

            $modeloRecetaSistema = DB::table(DB::raw("far_lista_articulos_dispensado_sistema('".$modelo->id."')"))->get();

            $arrayDatos = array('modelo' => $modelo,
                                'modeloRecetaCabecera' => $modeloRecetaCabecera,
                                'modeloRecetaSistema' => $modeloRecetaSistema
            ); 
            
            echo json_encode($arrayDatos);
        }
    }


    public function buscaDispensado()
    {  
            
        if(isset($_POST['idhcl_poblacion']))
        { 
            $idhcl_poblacion = $_POST['idhcl_poblacion'];
            
            $modeloRecetaCabecera = DB::table(DB::raw("far_lista_articulos_dispensados('".$idhcl_poblacion."')"))->get();  

            $arrayDatos = array('modeloRecetaCabecera' => $modeloRecetaCabecera);  


            echo json_encode($arrayDatos);
        }
    }

    public function guardar_Sistema()
    {    
        if(isset($_POST['idfar_cabecera_receta']) && isset($_POST['idfar_sucursales_recepcionado']))
        { 
            $idfar_cabecera_receta = $_POST['idfar_cabecera_receta'];
            $idfar_adm_estado = Far_adm_estado::DISPENSADO;
            $idfar_sucursales_recepcionado = $_POST['idfar_sucursales_recepcionado'];

            $folio = Far_cabecera_receta::generarCodigo($idfar_sucursales_recepcionado);
            
            $modeloRecetaCabecera = DB::table(DB::raw("far_actualizar_receta_dispensado('".$idfar_cabecera_receta."','".$idfar_adm_estado."','".$idfar_sucursales_recepcionado."','".$folio."')"))->get();  

            // $arrayDatos = array('modeloRecetaCabecera' => $modeloRecetaCabecera);  

            $model = Far_cabecera_receta::find($idfar_cabecera_receta);

            echo '<h5>RECETA DISPENSADO FOLIO NRO:</h5><h3><p style="color:red;">'.$model->folio.'</p></h3>';
 
            // echo json_encode($arrayDatos);
        }
    }

    public function guardar_Manual()
    {    
        //print_r($_POST['idhcl_poblacion']);

        $modelo = new Far_cabecera_receta;         
        $modelo->numero = 0;
        $modelo->folio = 0;//Far_cabecera_receta::generarCodigo($_POST['idfar_sucursales_recepcionado']);
        $modelo->cod_barra = 0;
        // la gestion no estoy utilizando --- hay que hacer una mejora a la gestion
        // $modelo->idinv_gestion = 1;
        $modelo->idfar_adm_tipotransaccion = $_POST['idfar_adm_tipotransaccion'];
        $modelo->idhcl_poblacion = $_POST['idhcl_poblacion'];
        $modelo->idhcl_cobertura = $_POST['idhcl_cobertura'];
        $modelo->idhcl_cuaderno = $_POST['idhcl_cuaderno'];
        $modelo->idfar_adm_estado = Far_adm_estado::DISPENSADO;
        // $modelo->diagnostico = '';
        $modelo->iddatosgenericos = $_POST['iddatosgenericos'];
        $modelo->fechadispensacion = date('d-m-Y');
        $modelo->horadispensacion = date('h:i A');

        $modelPoblacion = Hcl_poblacion::find($_POST['idhcl_poblacion']);
        $modelPoblacion_afiliacion = Hcl_poblacion_afiliacion::where([                            
                            ['idpoblacion', '=', $modelPoblacion->id],                                        
                            ])->first();
        $modelEmpleador = Hcl_empleador::find($modelPoblacion_afiliacion->idempleador); 

        $modelo->hcl_poblacion_nombrecompleto = $modelPoblacion->nombre.' '.$modelPoblacion->primer_apellido.' '.$modelPoblacion->segundo_apellido;
        $modelo->hcl_poblacion_matricula = $modelPoblacion->numero_historia;
        $modelo->hcl_poblacion_cod = $modelPoblacion->codigo;
        $modelo->hcl_poblacion_sexo = $modelPoblacion->sexo == 1? 'MAS':'FEM';
        $modelo->hcl_poblacion_fechanacimiento = $modelPoblacion->fecha_nacimiento;
        $modelo->hcl_poblacion_telefono = $_POST['telf'];
        $modelo->hcl_poblacion_domicilio = $_POST['dom'];
        $modelo->hcl_poblacion_documento = $_POST['doc'];
        $modelo->hcl_empleador_patronal = $modelEmpleador->nro_empleador;
        $modelo->hcl_empleador_empresa = $modelEmpleador->nombre;

        $modelo->idfar_sucursales_expedido = $_POST['idfar_sucursales_recepcionado'];
        $modelo->idfar_sucursales_recepcionado = $_POST['idfar_sucursales_recepcionado'];
        $modelo->usuario = Auth::user()['name'];
  

        if ($modelo->save()) {
            $resultado = Far_cabecera_receta::registraDocumentoproducto($modelo, json_decode($_POST['gridDocumentodetalle']));
            $model = Far_cabecera_receta::find($modelo->id);
            if ($resultado == 0) {
                $model->folio = 0;
                $model->save();
            }else{
                $model->folio = Far_cabecera_receta::generarCodigo($_POST['idfar_sucursales_recepcionado']);
                $model->save();
            }
            echo '<h5>RECETA DISPENSADO FOLIO NRO:</h5><h3><p style="color:red;">' . $model->folio . '</p></h3>';
        } else
            $mensaje = 'El registro ya tiene Folio! ';

        // if(isset($_POST['idfar_cabecera_receta']) && isset($_POST['idfar_sucursales_recepcionado']))
        // { 
        //     $idfar_cabecera_receta = $_POST['idfar_cabecera_receta'];
        //     $idfar_adm_estado = Far_adm_estado::DISPENSADO;
        //     $idfar_sucursales_recepcionado = $_POST['idfar_sucursales_recepcionado'];

        //     $folio = Far_cabecera_receta::generarCodigo($idfar_sucursales_recepcionado);
            
        //     $modeloRecetaCabecera = DB::table(DB::raw("far_actualizar_receta_dispensado('".$idfar_cabecera_receta."','".$idfar_adm_estado."','".$idfar_sucursales_recepcionado."','".$folio."')"))->get();  

        //     // $arrayDatos = array('modeloRecetaCabecera' => $modeloRecetaCabecera);  

        //     $model = Far_cabecera_receta::find($idfar_cabecera_receta);

        //    echo '<h5>RECETA DISPENSADO FOLIO NRO:</h5><h3><p style="color:red;">'.$model->folio.'</p></h3>';
 
        //     // echo json_encode($arrayDatos);
        // }
    }

    public function buscaRecetaLista()
    {
        //$modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if(isset($_POST['idhcl_poblacion']) && isset($_POST['codificacion']))
        { 
            $idhcl_poblacion = $_POST['idhcl_poblacion'];
            $codificacion = $_POST['codificacion']; 
            $modeloRecetaCabecera = DB::table('inv_cab_receta_detalle')                        
                        ->join('inv_articulo as iart', 'inv_cab_receta_detalle.idinv_articulo', '=', 'iart.id')
                        ->select('inv_cab_receta_detalle.*','iart.saldo')
                        ->where([                            
                            ['inv_cab_receta_detalle.idhcl_poblacion', '=', $idhcl_poblacion],            
                            ['inv_cab_receta_detalle.codificacion_articulo', '=', $codificacion],            
                            ]) 
                        ->orderBy('inv_cab_receta_detalle.id', 'DESC')                       
                        ->get();

            $arrayDatos = array('modeloRecetaCabecera' => $modeloRecetaCabecera); 
            
            echo json_encode($arrayDatos);
        }
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------    

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [ REPORTES ] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeInformeIngresos($id)
    {
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetTitle("ingreso");
        $pdf::SetSubject("TCPDF");

        // ==================================================================================================
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            $pdf->Image('imagenes/inventario2.jpg', 12, '', '', 18);
            $pdf->SetFont('courier', 'B', 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $pdf->MultiCell('', 12, 'DETALLE DE EGRESOS', 0, 'C', '', 1, '', '', 1, '', '', '', 12, 'M');
            $pdf->SetFont('courier', '', 10);
            $pdf->MultiCell('', 6, 'Dirección: Junin Nº 752', 0, 'C', '', 1, '', '', 1, '', '', '', 6, 'M');
        });
        
        // setPage
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha) {
            $pdf->SetY(-10);
            $pdf->SetFont('courier', 'B', 7);
            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-10);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-10);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });
        // ==================================================================================================
        
        // $pdf::AddPage('P', 'Legal');
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 30;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 100;
        $espacio = 2;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $modelNota = DB::table('nota as n')
                    ->join('tipodocumento as td', 'td.id', '=', 'n.idtipodocumento')
                    ->join('proveedor as p', 'p.id', '=', 'n.idproveedor')
                    ->select('n.*', 'p.nit', 'p.nombre as proveedor')
                    ->where([
                            ['n.eliminado', '=', 0],
                        ])
                    ->orderBy('n.numero', 'ASC')
                    ->get();

        for($j = 0; $j < count($modelNota); $j++)
        {
            $pdf::AddPage('P', 'Legal');
            $pdf::SetY($y_Inicio);

            $id = $modelNota[$j]->id;
            $model = Nota::find($id);
            
            $pdf::SetFont($tipoLetra, 'B', 12);
            $pdf::MultiCell($width, 7, 'INGRESO Nº '.$model->numero, 0, 'C', 0, 1, '', '', true, 0, false, true, 7, 10);
            $pdf::SetFont($tipoLetra, '', $tamanio);

            $pdf::MultiCell($widthLabel, $height, 'PROVEEDOR', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthDosPuntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, $modelNota[$j]->proveedor, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell($widthLabel, $height, 'FECHA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthDosPuntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, date('d-m-Y', strtotime($model->fecha)), 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell($widthLabel, $height, 'USUARIO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthDosPuntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthInformacion, $height, $model->usuario, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

            $pdf::MultiCell(100, $height, 'PRODUCTO', 'TBLR', 'L', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell(25, $height, 'CANTIDAD', 'TBR', 'R', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell(25, $height, 'COSTO', 'TBR', 'R', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell(25, $height, 'SUB TOTAL', 'TBR', 'R', 1, 1, '', '', true, 0, false, true, $height);

            $modelDocumentoproducto = DB::table('productonota as pn')
                        ->join('producto as p', 'p.id', '=', 'pn.idproducto')
                        ->select('pn.*', 'p.nombre as producto')
                        ->orderBy('p.nombre', 'ASC')
                        ->get();
            $total = 0;

            for($i = 0; $i < count($modelDocumentoproducto); $i++)
            {                
                // -------------------------------------------------------------------------------------------------
                $y_producto = ceil($pdf::getStringHeight(100, trim($modelDocumentoproducto[$i]->producto), $reseth = true, $autopadding = true, $border = 0));
                $y = max(array($y_producto)) + $height;
                $valor1 = $pdf::getY() + $y + $height + $height;
                $valor2 = $pdf::getPageHeight() - $y;
                if ($valor1 > $valor2)
                {
                    $pdf::AddPage('P', 'Legal');
                    $pdf::SetY($y_Inicio);

                    $pdf::MultiCell(100, $height, 'PRODUCTO', 'LBT', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                    $pdf::MultiCell(25, $height, 'CANTIDAD', 'LBT', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
                    $pdf::MultiCell(25, $height, 'COSTO', 'LBT', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
                    $pdf::MultiCell(25, $height, 'SUB TOTAL', 'LBTR', 'R', 0, 1, '', '', true, 0, false, true, $height, 10);
                }
                // -------------------------------------------------------------------------------------------------

                $pdf::MultiCell(100, $y_producto, $modelDocumentoproducto[$i]->producto, 'LBR', 'L', 0, 0, '', '', true, 0, false, true, $y_producto, 10);
                $pdf::MultiCell(25, $y_producto, $modelDocumentoproducto[$i]->ingreso, 'BR', 'R', 0, 0, '', '', true, 0, false, true, $y_producto, 10);
                $pdf::MultiCell(25, $y_producto, $modelDocumentoproducto[$i]->costo, 'BR', 'R', 0, 0, '', '', true, 0, false, true, $y_producto, 10);
                $subTotal = $modelDocumentoproducto[$i]->ingreso * $modelDocumentoproducto[$i]->costo;
                $pdf::MultiCell(25, $y_producto, number_format($subTotal, 2, '.', ','), 'BR', 'R', 0, 1, '', '', true, 0, false, true, $y_producto, 10);

                $total += $subTotal;
            }
            //$pdf::Line(10, $pdf::getY(), $width, $pdf::getY());


            // -------------------------------------- [FINAL DE LA HOJA] --------------------------------------
            $pdf::SetFont($tipoLetra, 'B', 10);
            $textoTotalventa = 'TOTAL';

            // totalAncho = 175
            $width1 = 130;
            $width2 = 15;
            $width3 = 5;
            $width4 = 25;
            
            $y_textoTotalventa = ceil($pdf::getStringHeight(70, trim($textoTotalventa), $reseth = true, $autopadding = true, $border = 1));
            
            $y = max(array($y_textoTotalventa));
            $valor1 = $pdf::getY() + $y + 15;
            $valor2 = $pdf::getPageHeight() - $y - 15;
            if ($valor1 > $valor2)
            {
                $pdf::AddPage('P', 'Legal');
                $pdf::SetY($y_Inicio);
            }
            
            $pdf::MultiCell($width1, $height, '', 'T', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($width2, $height, $textoTotalventa, 'T', 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($width3, $height, ': ', 'T', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($width4, $height, number_format($total, 2, '.', ','), 'T', 'R', 0, 1, '', '', true, 0, false, true, $height);
            // ----------------------------------------------------------------------------------------------------------------
        }

        // set javascript
        $pdf::IncludeJS('print(true);');
            
        $pdf::Output("informeVentas.pdf", "I");

        // mb_internal_encoding('utf-8');
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}