<?php
namespace App\Http\Controllers\Farmacia;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use \App\Models\Farmacia\Far_caja;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

use Session;
use Illuminate\Support\Facades\URL;

class Far_cajaController extends Controller
{
    private $rutaVista = 'Farmacia.far_caja.';
    private $controlador = 'far_caja';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
 
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $dato = $request->search; 

        $model = DB::table(DB::raw("far_lista_apertura_caja('".$arrayDatos['iddatosgenericos']."')")) 
                                ->paginate(config('app.pagination'));
        $modelAlmacenes = DB::table(DB::raw("far_lista_sucursales_usuario_completo('".$arrayDatos['idalmacen']."','".$arrayDatos['iddatosgenericos']."')"))
                                ->orderBy('id', 'DESC')
                                ->get(); 

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('modelAlmacenes', $modelAlmacenes)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = new Far_caja;

        // $modelSucursales = Far_caja::where([ 
        //                     ['eliminado', '=', 0],   
        //                     ['idalmacen', '=', $arrayDatos['idalmacen']],      
        //                 ])->get();

         
 
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        Session::put('urlAnterior', URL::previous());

        return view($model->rutaview.'create')
                ->with('model', $model)                
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
         
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

       
        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelFar_sucursales = Far_sucursales::datoMigrado($id); 

        $modelSucursales = Far_sucursales::where([ 
                            ['eliminado', '=', 0],   
                            ['idalmacen', '=', $arrayDatos['idalmacen']],                                                     
                            ['almacen_id', '=', $id],  
                        ])->first();

        if ($modelSucursales == null) {
            $model = new Far_sucursales;        
            $model->codigo_almacen = $modelFar_sucursales->codigo_almacen;
            $model->organigrama_id = $modelFar_sucursales->organigrama_id;
            $model->almacen_id = $modelFar_sucursales->almacen_id;
            $model->nombre = $modelFar_sucursales->nombre_almacen;
            $model->descripcion = '';                
            $model->idalmacen = $arrayDatos['idalmacen'];                
            $model->usuario = Auth::user()['name'];  

            if($model->save())
                $mensaje = config('app.mensajeGuardado');
            else
                $mensaje = config('app.mensajeErrorGuardado');
        }  

        // Session::put('urlAnterior', URL::previous());

        return redirect("sucursal");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Far_sucursales::find($id); 
        $model->codigo_identificacion = strtoupper($request->codigo_identificacion);
        $model->nombre = strtoupper($request->nombre);
        $model->descripcion = strtoupper($request->descripcion);       
        $model->usuario = Auth::user()['name'];  
        
        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);                             
        }
        else
            return redirect("sucursal");  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
