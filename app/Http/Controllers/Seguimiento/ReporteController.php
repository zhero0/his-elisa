<?php
namespace App\Http\Controllers\Seguimiento;
use App\Http\Controllers\Controller;

use Auth;
use App\Http\Requests\PdfRequest;

use Illuminate\Http\Request; 
// use dompdf;
use Illuminate\Support\Facades\DB;
use \App\Models\Almacen\Home;
use \App\Permission;
use Charts;
// use App\Charts\SampleChart;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Imagenologia\Ima_tamano_placas;
use \App\Models\Imagenologia\Ima_tipo_placas;
use \App\Models\Almacen\Almacen;
use \App\Models\Imagenologia\Ima_detalleplacasutilizadas;
use \App\Models\Imagenologia\Ima_pacs;

use Elibyy\TCPDF\Facades\TCPDF;

class PdfController extends Controller
{
    private $rutaVista = 'Imagenologia.reporte.';
    private $controlador = 'ima_reportePDF';

    private $sinContenido = 'reporte.sinContenidoPrint';
    private $sinDatos = 'SIN DATOS';
    private $landscape = 'landscape';
    private $portrait = 'portrait';
    // private $controlador = 'pdf';
    // ----------------------------------------------------------------------------
    // ----------------------------------------------------------------------------
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $arrayDatos = Home::parametrosSistema();

       // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
       //      return view('errors.403')
       //                  ->with('arrayDatos', $arrayDatos)
       //                  ->with('url', '');
        
        $dato =  "";
        $model = new Home;
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        $modelEspecialidad = Ima_especialidad::where([
                            ['ima_especialidad.eliminado', '=', 0],
                            ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();

        $data = DB::table( 'ima_detalleplacasutilizadas as dpu' )
                ->join('ima_resultados_programacion as result', 'result.id', '=', 'dpu.idima_resultados_programacion')
                ->join('ima_tipo_placas as itip', 'itip.id', '=', 'dpu.idima_tipo_placas')
                // ->select('itip.id', 'itip.nombre', DB::raw("SUM(dpu.cantidad) as cantidad"))
                ->select('itip.id', 'itip.nombre')
                ->where([
                        // ['dpu.idima_tipo_placas', '=', $Ima_tipo_placas[$i]->id],
                        ['result.numero_placa', '!=', null],
                        // ['result.eliminado', '!=', 0],
                        ['result.idalmacen', '=', $arrayDatos['idalmacen']]
                    ])
                // ->whereBetween('result.fecha_resultado', [$fechaDesde, $fechaHasta])
                // ->groupBy('itip.nombre','itip.id')
                ->get();
        
        $model_consultas_realizadas = DB::table('ima_registroprogramacion as rp')
                    ->leftjoin('ima_resultados_programacion as result', 'rp.id', '=', 'result.idima_registroprogramacion')
                    ->join('hcl_poblacion', 'hcl_poblacion.id', '=', 'rp.idhcl_poblacion')
                    ->join('hcl_especialidad', 'hcl_especialidad.id', '=', 'rp.idhcl_especialidad')
                    ->join('hcl_cuaderno', 'hcl_cuaderno.id', '=', 'rp.idhcl_cuaderno')
                    ->join('datosgenericos', 'datosgenericos.id', '=', 'rp.iddatosgenericos')
                    // ->join('ima_estado', 'ima_estado.id', '=', 'rp.idima_estado')
                    ->join('ima_especialidad as e', 'result.idima_especialidad', '=', 'e.id')
                    ->leftjoin('ima_estado as est', 'result.idima_estado', '=', 'est.id')
                    ->leftjoin('ima_examenes_registroprogramacion as ir', 'rp.id', '=', 'ir.idima_registroprogramacion')
                    ->join('ima_examenes as ie', 'ie.id', '=', 'ir.idima_examenes')
                    ->select('hcl_cuaderno.nombre as cuaderno','ie.nombre as nombre_examen')
                    ->where([
                        ['rp.eliminado', '=', 0], 
                        ['ir.eliminado', '=', 0], 
                        ['result.eliminado', '=', 0], 
                        ['rp.idalmacen', '=', $arrayDatos['idalmacen']],
                        // ['rp.fechaprogramacion', '=', "'".$fechaRegistro."'"]
                        // ['result.idima_especialidad', '=', $idima_especialidad]
                    ])
                    // ->where(function($query) use($fechaRegistro, $buscarFecha) {
                    // $query->where([
                    //     ['rp.fechaprogramacion', $buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro]]);
                    // }) 
                    ->orderBy('rp.numero', 'DESC')
                    ->get();

        $modelIma_cuaderno_datosgenericos = DB::table('ima_cuaderno_datosgenericos as cdg')
                        ->join('datosgenericos as dg', 'cdg.iddatosgenericos', '=', 'dg.id')
                        ->select('dg.id', DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as nombres"))
                        ->where([
                            ['cdg.eliminado', '=', 0],
                            ['cdg.idalmacen', '=', $arrayDatos['idalmacen']]
                        ])
                        ->orderBy('dg.nombres', 'ASC')
                        ->orderBy('dg.apellidos', 'ASC')
                        ->get();  
         
        $chart = Charts::database($data, 'donut', 'fusioncharts')
                ->title('CANTIDAD DE PLACAS UTILIZADAS')
                ->elementLabel("Total Placas") 
                // ->values($data->values())
                ->dimensions(700, 500)
                ->responsive(false)
                ->groupBy('nombre');
        $chart2 = Charts::database($data, 'bar', 'fusioncharts')
                ->title('CANTIDAD DE PLACAS UTILIZADAS')
                ->elementLabel("Total Placas") 
                // ->values($data->values())
                ->dimensions(700, 500)
                ->responsive(false)
                ->groupBy('nombre');

        $chart3 = Charts::database($model_consultas_realizadas, 'line', 'fusioncharts')
                ->title('CANTIDAD DE SOLICITUDES')
                ->elementLabel("Total Solicitudes") 
                // ->values($data->values())
                ->dimensions(900, 700)
                ->responsive(false)
                ->groupBy('cuaderno');

        $chart4 = Charts::database($model_consultas_realizadas, 'line', 'fusioncharts')
                ->title('CANTIDAD DE EXAMENES SOLICITADOS')
                ->elementLabel("Total Examenes") 
                // ->values($data->values())
                ->dimensions(1000, 700)
                ->responsive(false)
                ->groupBy('nombre_examen'); 

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('chart', $chart)
                ->with('chart2', $chart2)
                ->with('chart3', $chart3)
                ->with('chart4', $chart4) 
                ->with('modelIma_cuaderno_datosgenericos', $modelIma_cuaderno_datosgenericos)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    public function imprimeDocumento($valores)//Request $request)
    {
        $parametro = json_decode($valores);      
        $this->imprimePlacasUtilizadas($parametro);
    }
    // ----------------------------------------------------------------------------
    // ----------------------------------------------------------------------------
    private function encabezadoPlacasUtilizadas($arrayValores)
    {

        $pdf = $arrayValores['pdf'];
        $height = $arrayValores['height'];
        
        $width_numero = $arrayValores['width_numero'];
        $width_nombreApellido = $arrayValores['width_nombreApellido'];
        $width_matricula = $arrayValores['width_matricula'];
        $width_cod = $arrayValores['width_cod'];
        $width_sexo = $arrayValores['width_sexo'];
        $width_edad = $arrayValores['width_edad'];
        $width_empresa = $arrayValores['width_empresa'];
        $width_patronal = $arrayValores['width_patronal'];
        $width_placaSolicitada = $arrayValores['width_placaSolicitada'];
        $width_tipoPlaca = $arrayValores['width_tipoPlaca'];
        $width_tamanio_placa = $arrayValores['width_tamanio_placa'];
        $width_cantidad_placas = $arrayValores['width_cantidad_placas']; // aumentado...
        $width_medicoSolicitante = $arrayValores['width_medicoSolicitante'];

        $tipoLetra = $arrayValores['tipoLetra'];
        $tamanio_subTitulo = $arrayValores['tamanio_subTitulo'];
        $tamanio = $arrayValores['tamanio'];
        $medico_responsable = $arrayValores['medico_responsable'];


        $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
        $pdf::MultiCell(40, $height, 'NOMBRE COMPLETO: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio_subTitulo);
        $pdf::MultiCell(100, $height, $medico_responsable, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        
        $pdf::MultiCell($width_numero, $height, 'Nº', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_nombreApellido, $height, 'Nombre y Apellido', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_matricula, $height, 'Matrícula', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_cod, $height, 'Cod.', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_sexo, $height, 'Sexo', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_edad, $height, 'Edad', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_empresa, $height, 'Empresa', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_patronal, $height, 'Patronal', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_placaSolicitada, $height, 'Placa Solicitada', 1, 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_tipoPlaca, $height, 'Tipo Placa', 1, 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_medicoSolicitante, $height, 'Médico Solic.', 'TB', 'L', 1, 1, '', '', true, 0, false, true, $height);
    }
    private function y_hoja($pdf, $y_dato, $height)
    {
        $valor1 = $pdf::getY() + $y_dato + $height + 31;   // para que no desborde la pagina
        $valor2 = $pdf::getPageHeight() - 6;
        return $valor1.'*'.$valor2;
    }
    private function imprimePlacasUtilizadas($parametro)
    {
        $arrayDatos = Home::parametrosSistema();
        $especialidad = $parametro->especialidad;
        $particular = $parametro->particular;
        $iddatosgenericos_responsable = $parametro->iddatosgenericos_responsable;
        
        $pFechaDesde = $parametro->fechaDesde != ''? $parametro->fechaDesde : date('d-m-Y');
        $fecha = date_create($pFechaDesde);
        $fechaDesde = date_format($fecha, 'Y-m-d');

        $pFechaHasta = $parametro->fechaHasta != ''? $parametro->fechaHasta : date('d-m-Y');
        $fecha = date_create($pFechaHasta);
        $fechaHasta = date_format($fecha, 'Y-m-d');

        $rangoFecha = 'Fecha Desde: '.$pFechaDesde.'     Fecha Hasta: '.$pFechaHasta;
        $idalmacen = $arrayDatos['idalmacen'];
        $nombreAlmacen = Almacen::select('nombre')->where('id', '=', $idalmacen)->first();
        
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            
        $pdf::SetTitle("Placas Utilizadas");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

        $pdf::setHeaderCallback(function($pdf) use($rangoFecha,$nombreAlmacen) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            $pdf->Image('img/pacs1.jpg', 8, '', '', 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            
            $height = 5;
            $width = 80;
            $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(20, $height, '', 0, 'R', '', 0, '', '', 1, '', '', '', $height, 'M');

            $texto = 'CAJA NACIONAL DE SALUD                                  '.$nombreAlmacen->nombre;
            $textoTitulo = 'DETALLE DE PLACAS UTILIZADAS';
            
            $pdf->SetFont('courier', 'B', 8);
            $y_texto = ceil($pdf->getStringHeight(70, trim($texto), $reseth = true, $autopadding = true, $border = 0));
            $y_textoTitulo = ceil($pdf->getStringHeight(175, trim($textoTitulo), $reseth = true, $autopadding = true, $border = 0));
            $y_dato = max($y_texto, $y_textoTitulo);

            $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell($width, $y_dato, $texto, 0, 'L', '', 0, '', '', 1, '', '', '', $y_dato, 'M');

            $pdf->SetFont('courier', 'B', 16);
            $pdf->MultiCell(175, $y_dato, $textoTitulo, 0, 'C', '', 1, '', '', 1, '', '', '', $y_dato, 'M');
            
            $pdf->SetFont('helvetica', 'B', 9);
            $pdf->MultiCell(20, $height, '', 0, 'R', '', 0, '', '', 1, '', '', '', $height, 'M');
            $pdf->MultiCell($width, $height, 'SERVICIO DE IMAGENOLOGIA', 0, 'L', '', 0, '', '', 1, '', '', '', $height, 'M');

            $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(175, $height, $rangoFecha, 0, 'C', '', 1, '', '', 1, '', '', '', $height, 'M');

            $pdf->Line(30, $pdf->getY()+6, $pdf->getPageWidth()-5, $pdf->getY()+6);
        });
        
        // setPage
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha) {
            $pdf->SetY(-5);
            $pdf->SetFont('courier', 'B', 7);
            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-5);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-5);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });
        
        $pdf::AddPage('L', 'A4');        
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 30;
        $height = 5;
        $height_Espacio = 8;
        $width = 190;
        $widthLabel = 21;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 8;//9;
        $tamanio_subTitulo = 10;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);
        $anchoHoja = $pdf::getPageWidth();

        // **********************************************************************************************************
        $modelo = DB::table('ima_registroprogramacion as rp')
            ->join('ima_resultados_programacion as result', 'rp.id', '=', 'result.idima_registroprogramacion')
            ->join('datosgenericos as dg', 'result.iddatosgenericos', '=', 'dg.id')
            ->select(
                'result.id as idima_resultados_programacion',
                'rp.id as idima_registroprogramacion',
                'result.numero', 'result.numero_placa',
                'result.hcl_poblacion_nombrecompleto',
                'result.hcl_poblacion_matricula as matricula',
                'result.hcl_poblacion_cod as cod',
                'result.hcl_poblacion_sexo as sexo',
                // DB::raw("COALESCE(TIMESTAMPDIFF(YEAR, result.hcl_poblacion_fechanacimiento, CURDATE()), 0) as edad"),
                DB::raw("(SELECT date_part('year'::text, age(result.hcl_poblacion_fechanacimiento::timestamp with time zone))) as edad"),
                'result.hcl_empleador_empresa as empresa',
                'result.hcl_empleador_patronal as patronal',
                DB::raw("(select CONCAT(nombres, ' ', apellidos) from datosgenericos where id = rp.iddatosgenericos) as medico_solitante"),
                DB::raw("CONCAT(dg.nombres, ' ', dg.apellidos) as medico_responsable")
            )
            ->where([ //$buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro
                    ['rp.eliminado', '=', 0],
                    ['rp.idhcl_especialidad', $particular == 4? '!=' : '=', Hcl_especialidad::PARTICULAR],
                    ['result.eliminado', '=', 0],
                    ['result.numero_placa', '!=', null],
                    ['result.idalmacen', '=', $idalmacen],
                    ['result.idima_especialidad', '=', $especialidad],
                    ['dg.id', '=', $iddatosgenericos_responsable],
                    // ['numero_placa', '=', 3428]
                ])
            ->whereBetween('rp.fechaprogramacion', [$fechaDesde, $fechaHasta])
            ->orderBy(DB::raw('result.numero_placa::integer'), 'ASC')
            ->get();

        $width_numero = 10;
        $width_nombreApellido = 50;
        $width_matricula = 20;
        $width_cod = 8;
        $width_sexo = 9;
        $width_edad = 9;
        $width_empresa = 45;
        $width_patronal = 20;
        $width_placaSolicitada = 30;//35;
        $width_tipoPlaca = 35;
        $width_tamanio_placa = 25;
        $width_cantidad_placas = 10;
        $width_medicoSolicitante = 50;
        $medicoResponsable = '';

        for($i = 0; $i < count($modelo); $i++)
        {
            $numero = $modelo[$i]->numero_placa; //numero;
            // $numero = $modelo[$i]->idima_registroprogramacion; //numero codigo de registro
            $hcl_poblacion_nombrecompleto = $modelo[$i]->hcl_poblacion_nombrecompleto;
            $matricula = $modelo[$i]->matricula;
            $cod = $modelo[$i]->cod;
            $sexo = $modelo[$i]->sexo;
            $edad = $modelo[$i]->edad;
            $empresa = $modelo[$i]->empresa;
            $patronal = $modelo[$i]->patronal;
            $medico_solitante = $modelo[$i]->medico_solitante;
            $medico_responsable = $modelo[$i]->medico_responsable;

            $arrayValores = array(
                'pdf' => $pdf,
                'height' => $height,
                
                'width_numero' => $width_numero,
                'width_nombreApellido' => $width_nombreApellido,
                'width_matricula' => $width_matricula,
                'width_cod' => $width_cod,
                'width_sexo' => $width_sexo,
                'width_edad' => $width_edad,
                'width_empresa' => $width_empresa,
                'width_patronal' => $width_patronal,
                'width_placaSolicitada' => $width_placaSolicitada,
                'width_tipoPlaca' => $width_tipoPlaca,
                'width_tamanio_placa' => $width_tamanio_placa,
                'width_cantidad_placas' => $width_cantidad_placas, // aumentado...
                'width_medicoSolicitante' => $width_medicoSolicitante,

                'tipoLetra' => $tipoLetra,
                'tamanio_subTitulo' => $tamanio_subTitulo,
                'tamanio' => $tamanio,
                'medico_responsable' => $medico_responsable,
            );
            
            if($medico_responsable != $medicoResponsable)
            {
                if($i > 0)
                {
                    $pdf::AddPage('L', 'A4');
                    $pdf::SetY($y_Inicio);

                    $medicoResponsable = $medico_responsable;
                    $this->encabezadoPlacasUtilizadas($arrayValores);
                }
                else
                {
                    $medicoResponsable = $medico_responsable;
                    $this->encabezadoPlacasUtilizadas($arrayValores);
                }
            }

            $y_numero = ceil($pdf::getStringHeight($width_numero, trim($numero), $reseth = true, $autopadding = true, $border = 0));
            $y_hcl_poblacion_nombrecompleto = ceil($pdf::getStringHeight($width_nombreApellido, trim($hcl_poblacion_nombrecompleto), $reseth = true, $autopadding = true, $border = 0));
            $y_matricula = ceil($pdf::getStringHeight($width_matricula, trim($matricula), $reseth = true, $autopadding = true, $border = 0));
            $y_cod = ceil($pdf::getStringHeight($width_cod, trim($cod), $reseth = true, $autopadding = true, $border = 0));
            $y_sexo = ceil($pdf::getStringHeight($width_sexo, trim($sexo), $reseth = true, $autopadding = true, $border = 0));
            $y_edad = ceil($pdf::getStringHeight($width_edad, trim($edad), $reseth = true, $autopadding = true, $border = 0));
            $y_empresa = ceil($pdf::getStringHeight($width_empresa, trim($empresa), $reseth = true, $autopadding = true, $border = 0));
            $y_patronal = ceil($pdf::getStringHeight($width_patronal, trim($patronal), $reseth = true, $autopadding = true, $border = 0));
            $y_medico_solitante = ceil($pdf::getStringHeight($width_medicoSolicitante, trim($medico_solitante), $reseth = true, $autopadding = true, $border = 0));
            
            
            $modeloDetallePlacaSolicitada = DB::table('ima_examenes_registroprogramacion as erp')
                        ->join('ima_examenes as e', 'erp.idima_examenes', '=', 'e.id')
                        ->select('e.nombre as placa_solicitada')
                        ->where([
                            ['erp.eliminado', '=', 0],
                            ['erp.idima_registroprogramacion', '=', $modelo[$i]->idima_registroprogramacion]
                        ])
                        ->get();
            $modeloDetallePlaca = Ima_detalleplacasutilizadas::where('idima_resultados_programacion', '=', $modelo[$i]->idima_resultados_programacion)->get();
            $placa_solicitada = 0;
            $tamanio_placa = 0;

            if(count($modeloDetallePlacaSolicitada) == 1 && count($modeloDetallePlaca) == 1)
            {
                $placa_solicitada = trim($modeloDetallePlacaSolicitada[0]->placa_solicitada);
                $tamanio_placa = trim($modeloDetallePlaca[0]->ima_tamano_placas);
            }
            $y_placa_solicitada = ceil($pdf::getStringHeight($width_placaSolicitada, $placa_solicitada, $reseth = true, $autopadding = true, $border = 0));
            $y_tamanio_placa = ceil($pdf::getStringHeight($width_tamanio_placa, $tamanio_placa, $reseth = true, $autopadding = true, $border = 0));

            $y_dato = max($y_numero, $y_hcl_poblacion_nombrecompleto, $y_matricula, $y_cod, $y_sexo, $y_edad, $y_empresa, 
                          $y_patronal, $y_placa_solicitada, $y_tamanio_placa, $y_medico_solitante);

            $pdf::MultiCell($width_numero, $y_dato, $numero, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_nombreApellido, $y_dato, $hcl_poblacion_nombrecompleto, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_matricula, $y_dato, $matricula, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_cod, $y_dato, $cod, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_sexo, $y_dato, $sexo, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_edad, $y_dato, $edad, 'TB', 'C', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_empresa, $y_dato, $empresa, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_patronal, $y_dato, $patronal, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $raya = 'TB';

            // ---------------------------------------------------------------------------------------------------------
            $valor = explode('*', $this->y_hoja($pdf, $y_dato, $height));
            $valor1 = $valor[0];
            $valor2 = $valor[1];
            $siguientePagina = 0;

            if($valor1 > $valor2)
            {
                $pdf::MultiCell($width_placaSolicitada, $y_dato, '', $raya, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::MultiCell($width_tipoPlaca, $y_dato, '', $raya, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::MultiCell($width_medicoSolicitante, $y_dato, '', $raya, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);

                $pdf::AddPage('L', 'A4');
                $pdf::SetY($y_Inicio);

                $siguientePagina = 1;
                $medicoResponsable = $medico_responsable;
                $this->encabezadoPlacasUtilizadas($arrayValores);
            }
            // ---------------------------------------------------------------------------------------------------------
            $ancho_anteriores = $width_numero + $width_nombreApellido + $width_matricula + $width_cod + $width_sexo + $width_edad + $width_empresa + $width_patronal + $width_placaSolicitada;

            if(count($modeloDetallePlacaSolicitada) == 1 && count($modeloDetallePlaca) == 1)
            {
                if($siguientePagina == 1)
                {
                    $ancho_anteriores = $width_numero + $width_nombreApellido + $width_matricula + $width_cod + $width_sexo + $width_edad + $width_empresa + $width_patronal;// + $width_placaSolicitada;

                    $pdf::MultiCell($ancho_anteriores, $y_dato, '', 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                }
                $pdf::MultiCell($width_placaSolicitada, $y_dato, $placa_solicitada, 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                
                $pdf::MultiCell($width_tipoPlaca, $y_dato, $modeloDetallePlaca[0]->ima_tipo_placas, 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::MultiCell($width_medicoSolicitante, $y_dato, $medico_solitante, $raya, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);

                if($siguientePagina == 1)
                {
                    $ancho_anteriores = $width_numero + $width_nombreApellido + $width_matricula + $width_cod + $width_sexo + $width_edad + $width_empresa + $width_patronal + $width_placaSolicitada;

                    $pdf::MultiCell($ancho_anteriores, $y_dato, '', 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                }
                else
                    $pdf::MultiCell($ancho_anteriores, $y_dato, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);

                $pdf::MultiCell($width_tamanio_placa, $y_dato, $tamanio_placa, 1, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::MultiCell($width_cantidad_placas, $y_dato, $modeloDetallePlaca[0]->cantidad, 1, 'R', 0, 1, '', '', true, 0, false, true, $y_dato, 10);
            }
            else
            {
                $ancho_anteriores = $width_numero + $width_nombreApellido + $width_matricula + $width_cod + $width_sexo + $width_edad + $width_empresa + $width_patronal;// + $width_placaSolicitada;
                for($j = 0; $j < count($modeloDetallePlacaSolicitada); $j++)
                {
                    $placa_solicitada = trim($modeloDetallePlacaSolicitada[$j]->placa_solicitada);
                    $saltoLinea = $j == 0? 0 : 1;

                    $alturaInformacionPaciente = $y_dato;
                    $y_placa_solicitada = ceil($pdf::getStringHeight($width_placaSolicitada, $placa_solicitada, $reseth = true, $autopadding = true, $border = 0));
                    $y_medico_solitante = ceil($pdf::getStringHeight($width_medicoSolicitante, trim($medico_solitante), $reseth = true, $autopadding = true, $border = 0));
                    
                    $y_dato = max($y_placa_solicitada, $y_medico_solitante);
                    // ---------------------------------------------------------------------------------------------------------
                    $valor = explode('*', $this->y_hoja($pdf, $y_dato, $height));
                    $valor1 = $valor[0];
                    $valor2 = $valor[1];
                    $siguientePagina_anterior = $siguientePagina;
                    $siguientePagina = 0;

                    if($valor1 > $valor2)
                    {
                        $pdf::AddPage('L', 'A4');
                        $pdf::SetY($y_Inicio);

                        $siguientePagina = 1;
                        $medicoResponsable = $medico_responsable;
                        $this->encabezadoPlacasUtilizadas($arrayValores);
                    }
                    // ---------------------------------------------------------------------------------------------------------

                    if($siguientePagina == 1 || $siguientePagina_anterior == 1)
                    {
                        $pdf::MultiCell($ancho_anteriores, $y_dato, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    }
                    else
                    {
                        if($j > 0)
                        {
                            $pdf::MultiCell($ancho_anteriores, $y_dato, '', 0, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                        }
                        else
                        {
                            // SALTO de linea cuando la altura de la "información del paciente" es diferente a la altura "Placa Solicitada"
                            if($alturaInformacionPaciente != $y_dato)
                            {
                                $ancho_informacionPacienteFinal = $width_placaSolicitada + $width_tipoPlaca + $width_medicoSolicitante;

                                // estas 2 lineas van juntos....
                                $pdf::MultiCell($ancho_informacionPacienteFinal, $alturaInformacionPaciente, '', 1, 'L', 0, 1, '', '', true, 0, false, true, $alturaInformacionPaciente, 10);
                                $pdf::MultiCell($ancho_anteriores, $y_dato, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                            }
                        }
                    }

                    $pdf::MultiCell($width_placaSolicitada, $y_dato, $placa_solicitada, 1, 'L', 0, $saltoLinea, '', '', true, 0, false, true, $y_dato, 10);
                    if($j == 0)
                    {
                        if($alturaInformacionPaciente != $y_dato)
                            $pdf::MultiCell($width_tipoPlaca, $y_dato, '', 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                        else
                            $pdf::MultiCell($width_tipoPlaca, $y_dato, '', 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);

                        $pdf::MultiCell($width_medicoSolicitante, $y_dato, $medico_solitante, $raya, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);
                    }
                }
                

                for($j = 0; $j < count($modeloDetallePlaca); $j++)
                {
                    $ima_tipo_placas = trim($modeloDetallePlaca[$j]->ima_tipo_placas);
                    $y_ima_tipo_placas = ceil($pdf::getStringHeight($width_tipoPlaca, $ima_tipo_placas, $reseth = true, $autopadding = true, $border = 0));
                    $y_dato = $y_ima_tipo_placas;

                    // ---------------------------------------------------------------------------------------------------------
                    $valor = explode('*', $this->y_hoja($pdf, $y_dato, $height));
                    $valor1 = $valor[0];
                    $valor2 = $valor[1];

                    if($valor1 > $valor2)
                    {
                        $pdf::AddPage('L', 'A4');
                        $pdf::SetY($y_Inicio);
                        
                        // $siguientePagina = 1;
                        $medicoResponsable = $medico_responsable;
                        $this->encabezadoPlacasUtilizadas($arrayValores);
                    }
                    // ---------------------------------------------------------------------------------------------------------
                    $raya = 0;
                    $ancho_hasta_placaSol = $ancho_anteriores + $width_placaSolicitada;

                    $pdf::MultiCell($ancho_hasta_placaSol, $y_dato, '', $raya, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);

                    $pdf::MultiCell($width_tipoPlaca, $y_dato, $ima_tipo_placas, 1, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);

                    $pdf::MultiCell($ancho_hasta_placaSol, $y_dato, '', $raya, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    $pdf::MultiCell($width_tamanio_placa, $y_dato, $modeloDetallePlaca[$j]->ima_tamano_placas, 1, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    $pdf::MultiCell($width_cantidad_placas, $y_dato, $modeloDetallePlaca[$j]->cantidad, 1, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    
                    $pdf::MultiCell($width_medicoSolicitante, $y_dato, '', $raya, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);
                }
            }
            // ************************************************************************************************
        }

        // ---------------------------------------------- [FINAL DE LA HOJA] ----------------------------------------------
        $pdf::SetY($pdf::getY() + 5);
        $Ima_tipo_placas = Ima_tipo_placas::where('idalmacen', '=', $idalmacen)->get();
        $muestraColumnaCantidad = 1;

        for($i = 0; $i < count($Ima_tipo_placas); $i++)
        {
            $tipo_placa = $Ima_tipo_placas[$i]->nombre;
            $ancho_1 = 35;
            $ancho_2 = 22;



            // $modelIma_tamano_placas = DB::table('ima_registroprogramacion as erp')
            //         ->join('ima_resultados_programacion as result', 'result.idima_registroprogramacion','=','erp.id' )
            //         ->join('ima_detalleplacasutilizadas as dpu', 'result.id', '=', 'dpu.idima_resultados_programacion')
            //         ->select(
            //             'dpu.ima_tamano_placas as tamanio_placa',
            //             DB::raw("SUM(dpu.cantidad) as cantidad")
            //         )
            //         ->where([
            //                 ['dpu.idima_tipo_placas', '=', $Ima_tipo_placas[$i]->id],
            //                 ['result.numero_placa', '!=', null],
            //                 ['result.eliminado', '=', 0],
            //                 ['result.idalmacen', '=', $idalmacen],
            //                 ['result.iddatosgenericos', '=', $iddatosgenericos_responsable],
            //             ])
            //         ->whereBetween('erp.fechaprogramacion', [$fechaDesde, $fechaHasta])
            //         ->groupBy('dpu.ima_tamano_placas')
            //         ->get();

            $modelIma_tamano_placas = DB::table('ima_registroprogramacion as rp')
            ->join('ima_resultados_programacion as result', 'rp.id', '=', 'result.idima_registroprogramacion')
            ->join('datosgenericos as dg', 'result.iddatosgenericos', '=', 'dg.id')
            ->join('ima_detalleplacasutilizadas as dpu', 'result.id', '=', 'dpu.idima_resultados_programacion')
            ->select('dpu.ima_tamano_placas as tamanio_placa',
                        DB::raw("SUM(dpu.cantidad) as cantidad"))
            ->where([ //$buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro
                    ['rp.eliminado', '=', 0],
                    ['dpu.idima_tipo_placas', '=', $Ima_tipo_placas[$i]->id],
                    ['rp.idhcl_especialidad', $particular == 4? '!=' : '=', Hcl_especialidad::PARTICULAR],
                    ['result.eliminado', '=', 0],
                    ['result.numero_placa', '!=', null],
                    ['result.idalmacen', '=', $idalmacen],
                    ['result.idima_especialidad', '=', $especialidad],
                    ['dg.id', '=', $iddatosgenericos_responsable],
                    // ['numero_placa', '=', 3428]
                ])
            ->whereBetween('rp.fechaprogramacion', [$fechaDesde, $fechaHasta])
            ->groupBy('dpu.ima_tamano_placas')
            ->get();


            if(count($modelIma_tamano_placas) > 0)
            {
                $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
                $y_tipo_placa = ceil($pdf::getStringHeight($ancho_1, trim($tipo_placa), $reseth = true, $autopadding = true, $border = 0));
                $y_dato = $y_tipo_placa;
                $valor1 = $pdf::getY() + $y_dato + $height + $height + $height;
                $valor2 = $pdf::getPageHeight() - $y_dato - $height;
                if($valor1 > $valor2)
                {
                    $pdf::AddPage('L', 'A4');
                    $pdf::SetY($y_Inicio);
                }

                if($i == 0)
                {
                    $pdf::MultiCell($ancho_1, $y_tipo_placa, $Ima_tipo_placas[$i]->nombre, 1/*'TB'*/, 'L', 1, 0, '', '', true, 0, false, true, $y_tipo_placa);
                }
                else
                {
                    $pdf::MultiCell($ancho_1, $y_tipo_placa, $Ima_tipo_placas[$i]->nombre, 1/*'TB'*/, 'L', 1, 0, '', '', true, 0, false, true, $y_tipo_placa);
                }
                if($muestraColumnaCantidad == 1)
                {
                    $pdf::MultiCell($ancho_2, $y_tipo_placa, 'CANTIDAD', 1, 'C', 1, 1, '', '', true, 0, false, true, $y_tipo_placa);
                    $muestraColumnaCantidad = 0;
                }
                else
                {
                    $pdf::MultiCell($ancho_2, $y_tipo_placa, '', 1, 'C', 1, 1, '', '', true, 0, false, true, $y_tipo_placa);
                }
                $pdf::SetFont($tipoLetra, '', $tamanio_subTitulo);

                for($j = 0; $j < count($modelIma_tamano_placas); $j++)
                {
                    $tamanio_placa = $modelIma_tamano_placas[$j]->tamanio_placa;
                    $cantidad = $modelIma_tamano_placas[$j]->cantidad;

                    $y_dato = ceil($pdf::getStringHeight($ancho_1, trim($tamanio_placa), $reseth = true, $autopadding = true, $border = 0));
                    $valor1 = $pdf::getY() + $y_dato + $height + $height + $height;
                    $valor2 = $pdf::getPageHeight() - $y_dato - $height;
                    if($valor1 > $valor2)
                    {
                        $pdf::AddPage('L', 'A4');
                        $pdf::SetY($y_Inicio);
                    }

                    $pdf::MultiCell($ancho_1, $height, $tamanio_placa, 'LTBR', 'L', 1, 0, '', '', true, 0, false, true, $height);
                    $pdf::MultiCell($ancho_2, $height, $cantidad, 'TBR', 'C', 1, 1, '', '', true, 0, false, true, $height);
                }
            }
        }

        for ($i=0; $i <2 ; $i++) { 
            $y_dato = ceil($pdf::getStringHeight($ancho_1, 0, $reseth = true, $autopadding = true, $border = 0));
            $valor1 = $pdf::getY() + $y_dato + $height + $height + $height;
            $valor2 = $pdf::getPageHeight() - $y_dato - $height;


             $modelIma_sexo = DB::table('ima_registroprogramacion as rp')
            ->join('ima_resultados_programacion as result', 'rp.id', '=', 'result.idima_registroprogramacion')
            ->join('datosgenericos as dg', 'result.iddatosgenericos', '=', 'dg.id')
            ->select(DB::raw("count(result.hcl_poblacion_sexo) as cantidad"))
            ->where([ //$buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro
                    ['rp.eliminado', '=', 0],
                    ['rp.idhcl_especialidad', $particular == 4? '!=' : '=', Hcl_especialidad::PARTICULAR],
                    ['result.eliminado', '=', 0],
                    ['result.numero_placa', '!=', null],
                    ['result.idalmacen', '=', $idalmacen],
                    ['result.idima_especialidad', '=', $especialidad],
                    ['dg.id', '=', $iddatosgenericos_responsable],
                    ['result.hcl_poblacion_sexo', '=', $i == 0? 'MAS' : 'FEM'],
                ]) 
            ->whereBetween('rp.fechaprogramacion', [$fechaDesde, $fechaHasta])            
            ->get();

            if($valor1 > $valor2)
            {
                $pdf::AddPage('L', 'A4');
                $pdf::SetY($y_Inicio);
            }

            $pdf::MultiCell($ancho_1, $height, $i == 0? 'HOMBRES': 'MUJERES', 'LTBR', 'L', 1, 0, '', '', true, 0, false, true, $height);
            $pdf::MultiCell($ancho_2, $height,  $modelIma_sexo[0]->cantidad, 'TBR', 'C', 1, 1, '', '', true, 0, false, true, $height);
        }
        // ----------------------------------------------------------------------------------------------------------------

        // set javascript
        // $pdf->IncludeJS('print(true);');

        $pdf::Output("placasUtilizadas Desde ".$pFechaDesde." hasta ".$pFechaHasta.".pdf", "I");

        mb_internal_encoding('utf-8');
    }


}
