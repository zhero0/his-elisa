<?php
namespace App\Http\Controllers\Seguimiento;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use App\Models\Administracion\Datosgenericos;
use \App\Models\Almacen\Almacen;

use \App\Models\Seguimiento\Seg_estado_cov;
use \App\Models\Seguimiento\Seg_establecimiento;
use \App\Models\Seguimiento\Seg_ficha_generada;
use \App\Models\Seguimiento\Seg_tipo_muestra;
use \App\Models\Seguimiento\Seg_tipo_paciente;
use \App\Models\Seguimiento\Seg_metodo_diagnostico;
use \App\Models\Seguimiento\Seg_registro_resultado;
use \App\Models\Seguimiento\Seg_metodo_valores; 
use \App\Models\Seguimiento\Seg_envio_laboratorio; 
use \App\Models\Seguimiento\Seg_resultado; 
use \App\Models\Seguimiento\Seg_recepcion_laboratorio; 

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Illuminate\Support\Facades\URL;
use Session;

class Seg_registro_resultadoController extends Controller
{
    private $rutaVista = 'Seguimiento.seg_registro_resultado.';
    private $controlador = 'seg_registro_resultado';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $modelEstadoLaboratorio = Seg_estado_cov::get();
        $modelMetodo_Laboratorio = Seg_metodo_diagnostico::get(); 

        $datosBuscador = $request->datosBuscador;
        $model = Seg_registro_resultado::resultados_covLaboratorio($datosBuscador, 0)->paginate(config('app.pagination'));
         
        
        $model->datosBuscador = $request->datosBuscador;

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;
        $model->search = $request->search;

        // imprimir resultado
        $model->datos_imprimir = '';
        if (Session::has('datos_imprimir'))
            $model->datos_imprimir = Session::get('datos_imprimir');

        return view($model->rutaview.'index')
                ->with('model', $model)                
                ->with('modelEstadoLaboratorio', $modelEstadoLaboratorio) 
                ->with('modelMetodo_Laboratorio', $modelMetodo_Laboratorio) 
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $modelDato = Seg_registro_resultado::where('idseg_ficha_generada', '=', $id)->first();  

        
        if ($modelDato == null) {
            $model = new Seg_registro_resultado;

            $model->id = 0;
            $modelResultado = new Seg_resultado;
        }else{ 
            $model = $modelDato;
            $modelResultado = Seg_resultado::where('idseg_registro_resultado', '=', $model->id)->first();            
            $envio = Seg_envio_laboratorio::where('idseg_registro_resultado', '=', $model->id)->first();
            $recepcion = Seg_recepcion_laboratorio::where('idseg_registro_resultado', '=', $model->id)->first();
            if ($envio !=null) {
                $model->idseg_establecimiento_envio = $envio->idseg_establecimiento_envio;
                $model->fecha_envio = $envio->fecha_envio;
            }
            if ($recepcion !=null) {
                $model->idseg_establecimiento_recepcion = $recepcion->idseg_establecimiento_recepcion;
                $model->fecha_recepcion = $recepcion->fecha_recepcion;
                $model->hora_recepcion = $recepcion->hora_recepcion;
            }
        } 
        $modelSeg_ficha_generada = Seg_ficha_generada::find($id);

        $model->idseg_ficha_generada = $modelSeg_ficha_generada->id;
        $modelTipo_muestra = Seg_tipo_muestra::get();
        $modelEstado_covid = Seg_estado_cov::get();
        $modelHospital = Seg_establecimiento::get();
        $modelMetodo = Seg_metodo_diagnostico::get();
        $modelMetodo_valores = Seg_metodo_valores::get(); 
        $modelEnvio = Seg_envio_laboratorio::get();
        $modelRecepcion = Seg_recepcion_laboratorio::get();
        $modelDatosgenericos = Datosgenericos::orderBy('nombres', 'ASC')->get();
        
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelMetodo', $modelMetodo)
                ->with('modelTipo_muestra', $modelTipo_muestra)
                ->with('modelHospital', $modelHospital) 
                ->with('modelResultado', $modelResultado)
                ->with('modelMetodo_valores', $modelMetodo_valores)
                ->with('modelEstado_covid', $modelEstado_covid)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('modelSeg_ficha_generada', $modelSeg_ficha_generada)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelDato = Seg_registro_resultado::where('idseg_ficha_generada', '=', $request->idseg_ficha_generada)->first();   
        if ($modelDato == null) {
            $model = new Seg_registro_resultado;
            $modelResultado = new Seg_resultado;
            $model->correlativo = Seg_registro_resultado::max('correlativo') + 1;
        }else{
            $model = $modelDato;
            $modelFind = Seg_resultado::where('idseg_registro_resultado', '=', $model->id)->first();
            $modelResultado = Seg_resultado::find($modelFind->id);
        }        
       
        $model->registro_laboratorio = $request->registro_laboratorio;
        $model->registro_sistema = $request->registro_sistema;
        // $model->muestreo = $request->muestreo;
        $model->envio = $request->envio == true? 1 : 0;
        $model->recepcion = $request->recepcion == true? 1 : 0;
        $model->idseg_tipo_muestra = $request->idseg_tipo_muestra;
        $model->fecha_toma = date('Y-m-d', strtotime($request->fecha_toma));
        $model->hora_toma = $request->hora_toma;
        $model->iddatosgenericos_responsable_toma = $request->iddatosgenericos_responsable_toma;
        // $model->bioquimico_responsable_toma = strtoupper($request->bioquimico_responsable_toma);

        $modelDatosgenericos = Datosgenericos::find($request->iddatosgenericos_responsable_toma);
        $model->bioquimico_responsable_toma = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;
        $model->idalmacen = $request->idalmacen;
        $model->idseg_ficha_generada = $request->idseg_ficha_generada;
        
        if($model->save()){
            if ($model->envio == 1) { 
                Seg_envio_laboratorio::registra_envioLaboratorio($model, $request);
            }
            if ($model->recepcion == 1) { 
                Seg_recepcion_laboratorio::registra_recepcionLaboratorio($model, $request);
            }
            
            $modelMetodo = Seg_metodo_diagnostico::find($request->idseg_metodo_diagnostico);
            $modelResultado->idseg_metodo_diagnostico = $modelMetodo->id;
            $modelResultado->descripcion_metodo = $modelMetodo->descripcion;
            $modelResultado->diagnostico = $request->diagnostico;
            $modelResultado->diagnostico_fecha = date('Y-m-d', strtotime($request->diagnostico_fecha));
            $modelResultado->seguimiento = $request->seguimiento;
            $modelResultado->seguimiento_fecha = date('Y-m-d', strtotime($request->seguimiento_fecha));
            $modelResultado->fecha_resultado = date('Y-m-d', strtotime($request->fecha_resultado));
            $modelResultado->hora_resultado = $request->hora_resultado;

            $modelResultado->iddatosgenericos_responsable_analisis = $request->iddatosgenericos_responsable_analisis;
            // $model->bioquimico_responsable_toma = strtoupper($request->bioquimico_responsable_toma);

            $modelDatosgenericos = Datosgenericos::find($request->iddatosgenericos_responsable_analisis);
            $modelResultado->medico_responsable_analisis = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;
            $modelResultado->res_igm = $request->res_igm;
            $modelResultado->res_igg = $request->res_igg;
            $modelResultado->idseg_metodo_valores = $request->idseg_metodo_valores;
            $modelEstado_covid = Seg_estado_cov::find( $request->idseg_estado_cov);
            if ($modelEstado_covid!=null) {
                $modelResultado->idseg_estado_cov = $modelEstado_covid->id;
                $modelResultado->nombre_estado = $modelEstado_covid->nombre;
            }
            
            $modelSeg_ficha_generada = Seg_ficha_generada::find($request->idseg_ficha_generada);
            $modelResultado->documento = $modelSeg_ficha_generada->documento;
            $modelResultado->fecha_nacimiento = $modelSeg_ficha_generada->fecha_nacimiento;
            $modelResultado->complemento = $modelSeg_ficha_generada->complemento;
            $modelResultado->nombre_completo = $modelSeg_ficha_generada->nombre.' '.$modelSeg_ficha_generada->primer_apellido.' '.$modelSeg_ficha_generada->segundo_apellido;
            $modelResultado->idseg_registro_resultado = $model->id;
            $modelResultado->idalmacen = $request->idalmacen;
            $modelResultado->save();
            $mensaje = config('app.mensajeGuardado');
        }    
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        return redirect($this->controlador);
    } 

    
    public function buscaMedico() {
        echo 'xxe';
    }
    public function imprimeResultado($id)
    {
        Seg_registro_resultado::imprimeResultado($id);
    }
    

    public function consultaCov()
    {
        //echo 'sdf';
        //return;
        if(isset($_POST['dato_a']) && isset($_POST['dato_b']))
        {
            $documento = $_POST['dato_a'];
            $fecha_nacimiento = $_POST['dato_b']; 

            $query = DB::select("select * from consulta_sarscov('".$documento."','".$fecha_nacimiento."')");     
            echo json_encode($query);  
        }
    } 
}