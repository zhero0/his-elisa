<?php
namespace App\Http\Controllers\Seguimiento;
use App\Http\Controllers\Controller;

use \App\Models\Seguimiento\Seg_estado_cov;
use \App\Models\Seguimiento\Seg_establecimiento;
use \App\Models\Seguimiento\Seg_ficha_generada;
use \App\Models\Seguimiento\Seg_tipo_muestra;
use \App\Models\Seguimiento\Seg_tipo_paciente;
use \App\Models\Seguimiento\Seg_metodo_diagnostico;


use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Afiliacion\Hcl_afiliacion_parentesco;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion_afiliacion;
use \App\Models\Afiliacion\Hcl_cobertura;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use Illuminate\Support\Facades\URL;
use Session;
use GuzzleHttp\Client;

class Seg_ficha_generadaController extends Controller
{
    private $rutaVista = 'Seguimiento.seg_ficha_generada.';
    private $controlador = 'seg_ficha_generada';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;
        
        $modelEstadoLaboratorio = Seg_estado_cov::get();
        $documento = '';
        $primer_apellido = '';
        $segundo_apellido = '';
        $nombre = '';
        $numero_historia = '';
        $covid = 0;
        $datosBuscador = json_decode($request->datosBuscador);
        if($datosBuscador != '')
        {
            $documento = $datosBuscador->documento;
            $primer_apellido = $datosBuscador->primer_apellido;
            $segundo_apellido = $datosBuscador->segundo_apellido;
            $nombre = $datosBuscador->nombre;
            $numero_historia = $datosBuscador->numero_historia;
            $covid = $datosBuscador->covid > 0? $datosBuscador->covid : null;
        }
        $model = Seg_ficha_generada::where([
                    // ['idseg_estado_cov', '=', $covid],
                    // ['idalmacen', '=', $arrayDatos['idalmacen']],
                    ['nombre', 'ilike', '%'.$nombre.'%'],
                    ['primer_apellido', 'ilike', '%'.$primer_apellido.'%'],
                    ['segundo_apellido', 'ilike', '%'.$segundo_apellido.'%'],
                    ['documento', 'ilike', '%'.$documento.'%'],
                    ['numero_historia', 'ilike', '%'.$numero_historia.'%'], 
                    ['eliminado', '=', 0],
                ])
                ->orderBy('primer_apellido', 'ASC')
                ->paginate(config('app.pagination'));

        $model->datosBuscador = $request->datosBuscador;

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;
        $model->search = $request->search;

        return view($model->rutaview.'index')
                ->with('model', $model)                
                ->with('modelEstadoLaboratorio', $modelEstadoLaboratorio) 
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $model = new Seg_ficha_generada();        
        $model->idalmacen = $arrayDatos['idalmacen'];
        
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        $modelTipo_paciente = Seg_tipo_paciente::get();
        $modelTipo_muestra = Seg_tipo_muestra::get();
        $modelEstado_covid = Seg_estado_cov::get();
        $modelHospital = Seg_establecimiento::get();
        $modelMetodo = Seg_metodo_diagnostico::get();

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelTipo_muestra', $modelTipo_muestra)
                ->with('modelTipo_paciente', $modelTipo_paciente)
                ->with('modelHospital', $modelHospital)
                ->with('modelMetodo', $modelMetodo)
                ->with('modelEstado_covid', $modelEstado_covid)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {

        $codigo_parentesco =  DB::table('hcl_afiliacion_parentesco') 
        ->selectraw('codificacion as paren, id')
        ->where('descripcion','ilike','%'.$request->parentesco.'%')           
        ->get(); 

        // print_r($codigo_parentesco);
        // return;

        $model = new Hcl_poblacion;        
        
        $model->primer_apellido = strtoupper($request->primer_apellido);         
        $model->segundo_apellido = strtoupper($request->segundo_apellido);
        $model->nombre = strtoupper($request->nombre); 
        $model->documento = strtoupper($request->documento);
        $model->sexo = $request->sexo == 'MASCULINO'? 1 : 2;
        $model->fecha_nacimiento = $request->fecha_nacimiento;  

        $model->numero_historia = strtoupper($request->numero_historia);
        $model->codigo = strtoupper($request->numero_historia.'-'.$codigo_parentesco[0]->paren);

        $model->telefono = $request->telefono;  
        $model->domicilio = strtoupper($request->calle);  
        $model->id_departamento = 0;
        $model->id_provincia = 0;
        $model->id_municipio = 0;
        $model->id_localidad = 0;
        $model->idmigracion = $request->iderp;
        $model->observaciones = 'Migrado ERP';
        $model->usuario = Auth::user()['name']; 
        if($model->save()){

            $modelEmpleador = Hcl_empleador::where('nro_empleador','ilike', '%'.$request->nro_empleador.'%' )->first();

            if ($modelEmpleador == null) {
                $modelEmpleador = new Hcl_empleador;                  
                $modelEmpleador->nombre = strtoupper($request->nombre); 
                $modelEmpleador->nro_empleador = strtoupper($request->nro_empleador); 
                $modelEmpleador->zona = strtoupper($request->zona); 
                $modelEmpleador->calle = strtoupper($request->calle); 
                $modelEmpleador->nro = strtoupper($request->nro); 
                $modelEmpleador->telefono = strtoupper($request->telefono); 
                $modelEmpleador->representante_legal = strtoupper($request->representante_legal); 
                $modelEmpleador->nro_trabajadores = $request->nro_trabajadores; 
                $modelEmpleador->nro_padron = strtoupper($request->nro_padron); 
                $modelEmpleador->fecha_presentacion = $request->fecha_presentacion; 
                $modelEmpleador->estado = $request->estado; 
                $modelEmpleador->id_departamento = 1;
                $modelEmpleador->id_provincia = 1;
                $modelEmpleador->id_municipio = 1;
                // $modelEmpleador->id_localidad = $departamentos[0]->id_municipio;
                $modelEmpleador->id_localidad = 1;
                $modelEmpleador->usuario = Auth::user()['name'];
                $modelEmpleador->save();
            }

            $model_afiliacion = new Hcl_poblacion_afiliacion;
            $model_afiliacion->idpoblacion = $model->id; 
            if ($codigo_parentesco[0]->id == 29) {
                $model_afiliacion->idtipopaciente = 1;
            }else{
                $model_afiliacion->idtipopaciente = 2;
            }
            $model_afiliacion->idcobertura = 1;
            $model_afiliacion->idasegurado = $model->id;  //$ultimo_registro[0]->idasegurado+1;
            $model_afiliacion->idparentesco = $codigo_parentesco[0]->id;
            $model_afiliacion->idestadoafiliacion = 1;
            $model_afiliacion->idcuaderno = $request->cuaderno;
            $model_afiliacion->fin_cobertura = '2018-01-01';
            $model_afiliacion->fecha_afiliacion = '2018-01-01';
            $model_afiliacion->fecha_registro = '2018-01-01';
            $model_afiliacion->idempleador = $modelEmpleador->id;
            $model_afiliacion->usuario = Auth::user()['name'];
            $model_afiliacion->save();
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('urlAnterior'))
        {
            // return redirect()->to(Session::get('urlAnterior'))
            return redirect()->to("poblacion")
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
                             
        }
        else
            return redirect("poblacion");
        
        // -------------------------------------- 
        // esta parte es del codigo original de seguimiento

         
        // $model = new Seg_ficha_generada();   

        // $model->numero_registro_sive = $request->numero_registro_sive;
        // $model->iderp = $request->iderp;
        // // poblacion
        // // $model->idhcl_poblacion = $request->idhcl_poblacion;
        // $model->fecha_registro = date('Y-m-d', strtotime($request->fecha_registro));
        // $model->documento = $request->documento;
        // $model->complemento = strtoupper($request->complemento);
        // $model->procedencia = strtoupper($request->procedencia);
        // $model->primer_apellido = strtoupper($request->primer_apellido); 
        // $model->segundo_apellido = strtoupper($request->segundo_apellido); 
        // $model->nombre = strtoupper($request->nombre);
        // $model->sexo = $request->sexo == 'MASCULINO'? 1 : 2;
        // $model->fecha_nacimiento = date('Y-m-d', strtotime($request->fecha_nacimiento));
        // $model->edad = $request->edad == ''? Seg_ficha_generada::getEdad($model->fecha_nacimiento) : $request->edad;
        // // datos recidencia
        // $model->ciudad = strtoupper($request->ciudad); 
        // $model->calle = strtoupper($request->calle); 
        // $model->zona = strtoupper($request->zona); 
        // $model->domicilio = strtoupper($request->domicilio); 
        // $model->telefono = strtoupper($request->telefono);
        // // datos empleador
        // $model->numero_historia = strtoupper($request->numero_historia); 
        // $model->parentesco = strtoupper($request->parentesco); 
        // $model->tipo_seguro = strtoupper($request->tipo_seguro); 
        // $model->nro_empleador = strtoupper($request->nro_empleador); 
        // $model->razon_empleador = strtoupper($request->razon_empleador);
        // // dato epidemiologico contacto
        // $model->contacto =  $request->contacto;
        // $model->dato_contacto =  $request->dato_contacto;
        // $model->fecha_contacto = date('Y-m-d', strtotime($request->fecha_contacto));
        // $model->idseg_tipo_paciente = $request->idseg_tipo_paciente;
        // // // medico responsable
        // // $model->iddatosgenericos = $request->iddatosgenericos;
        // // $model->medico_responsable = strtoupper($request->medico_responsable);
        // // $model->idseg_estado = $request->idseg_estado;

        // // $model->idalmacen = $request->idalmacen;        
        // // $model->usuario = Auth::user()->name;


        // if($model->save())
        // {
        //     // if ($model->envio==1) {
        //     //     Seg_envio_laboratorio::registra_envioLaboratorio($model, $request);
        //     // } 
        //     // if ($model->recepcion==1) {
        //     //     Seg_recepcion_laboratorio::registra_recepcionLaboratorio($model, $request);
        //     // } 
        //     // if ($model->muestreo==1) {
        //     //     Seg_resultado::registra_datosResultado($model, $request);
        //     // }
        //     // Seg_datos_epidemiologicos::registra_datosEpidemiologicos($model, $request);

        //     $mensaje = config('app.mensajeGuardado');
        // }
        // else
        //     $mensaje = config('app.mensajeErrorGuardado');

        // // if(Session::has('url_'.$this->controlador))
        // // {
        // //     $sesion = Session::get('url_'.$this->controlador);
        // //     Session::forget('url_'.$this->controlador);
        // //     return redirect()->to($sesion)
        // //                     ->with('message', 'store')
        // //                     ->with('mensaje', $mensaje);
        // // }
        // // else
        //     return redirect($this->controlador);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $param = json_decode($request->param, true);
        
        try {
            $client = new Client();
            $response = $client->post(
                'https://auth-desarrollo.cns.gob.bo/connect/token',
                array(
                    'form_params' => [
                        "grant_type" => "password",
                        "username" => "regional-lpz-salud",
                        "password" => "Regional-lpz-2021",
                        "scope" => "afiliaciones",
                        "client_id" => "regional-lpz",
                        "client_secret" => "regional-lpz testing"
                    ]
                )
            );

            if($response->getStatusCode() == "200"){
                $data = json_decode($response->getBody(), true);
                $token = $data['access_token'];

                $url = 'https://api-desarrollo.cns.gob.bo/erp/v1/Afiliaciones/Asegurados?DocumentoIdentidad='.$param['documento'].'&FechaNacimiento='.$param['fecha_nacimiento'];
                $response = $client->request('GET', $url, [
                    'headers' => [
                        'Authorization' => 'Bearer '.$token,        
                        'Accept'        => 'application/json',
                    ]
                ]);
                return json_encode([
                    'status' => 'success',
                    'response' => $response->getBody()->getContents()
                ]);
            }else{
                return json_encode([
                    'status' => 'error',
                    'response' => 'No se pudo encontar la página! '
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                'status' => 'error',
                'response' => $e->getMessage()
            ]);
        }
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $model = Seg_ficha_generada::find($id);  

        $modelTipo_paciente = Seg_tipo_paciente::get();
        $modelTipo_muestra = Seg_tipo_muestra::get();
        $modelEstado_covid = Seg_estado_cov::get();
        $modelHospital = Seg_establecimiento::get();
        $modelMetodo = Seg_metodo_diagnostico::get();

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelTipo_muestra', $modelTipo_muestra)
                ->with('modelTipo_paciente', $modelTipo_paciente)
                ->with('modelHospital', $modelHospital)
                ->with('modelMetodo', $modelMetodo)
                ->with('modelEstado_covid', $modelEstado_covid)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Seg_ficha_generada::find($id);

        $model->numero_registro_sive = $request->numero_registro_sive;
        $model->iderp = $request->iderp;
        // poblacion
        // $model->idhcl_poblacion = $request->idhcl_poblacion;
        $model->fecha_registro = date('Y-m-d', strtotime($request->fecha_registro));
        $model->documento = $request->documento;
        $model->complemento = strtoupper($request->complemento);
        $model->procedencia = strtoupper($request->procedencia);
        $model->primer_apellido = strtoupper($request->primer_apellido); 
        $model->segundo_apellido = strtoupper($request->segundo_apellido); 
        $model->nombre = strtoupper($request->nombre);
        $model->sexo = $request->sexo == 'MASCULINO'? 1 : 2;
        $model->fecha_nacimiento = date('Y-m-d', strtotime($request->fecha_nacimiento));
        $model->edad = $request->edad == ''? Seg_ficha_generada::getEdad($model->fecha_nacimiento) : $request->edad;
        // datos recidencia
        $model->ciudad = strtoupper($request->ciudad); 
        $model->calle = strtoupper($request->calle); 
        $model->zona = strtoupper($request->zona); 
        $model->domicilio = strtoupper($request->domicilio); 
        $model->telefono = strtoupper($request->telefono);
        // datos empleador
        $model->numero_historia = strtoupper($request->numero_historia); 
        $model->parentesco = strtoupper($request->parentesco); 
        $model->tipo_seguro = strtoupper($request->tipo_seguro); 
        $model->nro_empleador = strtoupper($request->nro_empleador); 
        $model->razon_empleador = strtoupper($request->razon_empleador);
        // dato epidemiologico contacto
        $model->contacto =  $request->contacto;
        $model->dato_contacto =  $request->dato_contacto;
        $model->fecha_contacto = date('Y-m-d', strtotime($request->fecha_contacto));
        $model->idseg_tipo_paciente = $request->idseg_tipo_paciente;
        // // medico responsable
        // $model->iddatosgenericos = $request->iddatosgenericos;
        // $model->medico_responsable = strtoupper($request->medico_responsable);
        // $model->idseg_estado = $request->idseg_estado;

        // $model->idalmacen = $request->idalmacen;        
        // $model->usuario = Auth::user()->name; 

        if($model->save())
            $mensaje = config('app.mensajeGuardado');
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }
    
 

}