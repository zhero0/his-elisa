<?php
namespace App\Http\Controllers\Seguimiento;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;

use \App\Models\Seguimiento\Seg_estado_cov;
use \App\Models\Seguimiento\Seg_establecimiento;
use \App\Models\Seguimiento\Seg_ficha_generada;
use \App\Models\Seguimiento\Seg_tipo_muestra;
use \App\Models\Seguimiento\Seg_tipo_paciente;
use \App\Models\Seguimiento\Seg_metodo_diagnostico;
use \App\Models\Seguimiento\Seg_registro_resultado;
use \App\Models\Seguimiento\Seg_metodo_valores; 
use \App\Models\Seguimiento\Seg_envio_laboratorio; 
use \App\Models\Seguimiento\Seg_resultado; 
use \App\Models\Seguimiento\Seg_recepcion_laboratorio; 
use \App\Models\Seguimiento\Seg_resultado_valido; 

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Illuminate\Support\Facades\URL;
use Session;

class Seg_resultado_validoController extends Controller
{
    private $rutaVista = 'Seguimiento.seg_resultado_valido.';
    private $controlador = 'seg_resultado_valido';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;
        
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $modelEstadoLaboratorio = Seg_estado_cov::get();
        $modelMetodo_Laboratorio = Seg_metodo_diagnostico::get(); 

        $datosBuscador = $request->datosBuscador;
        $model = Seg_registro_resultado::resultados_covLaboratorio($datosBuscador, 0)->paginate(config('app.pagination'));
         
        
        $model->datosBuscador = $request->datosBuscador;

        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;
        $model->search = $request->search;

        // imprimir resultado
        $model->datos_imprimir = '';
        if (Session::has('datos_imprimir'))
            $model->datos_imprimir = Session::get('datos_imprimir');

        return view($model->rutaview.'index')
                ->with('model', $model)                
                ->with('modelEstadoLaboratorio', $modelEstadoLaboratorio) 
                ->with('modelMetodo_Laboratorio', $modelMetodo_Laboratorio) 
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        $model = new Seg_registro_resultado;        
        $model->idalmacen = $arrayDatos['idalmacen'];

        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;
        
        $modelTipo_muestra = Seg_tipo_muestra::get();
        $modelEstado_covid = Seg_estado_cov::get();
        $modelHospital = Seg_establecimiento::get();
        $modelMetodo = Seg_metodo_diagnostico::get();
        $modelMetodo_valores = Seg_metodo_valores::get();

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'create')
                ->with('model', $model)
                ->with('modelTipo_muestra', $modelTipo_muestra)                
                ->with('modelHospital', $modelHospital)
                ->with('modelMetodo', $modelMetodo)
                ->with('modelMetodo_valores', $modelMetodo_valores)
                ->with('modelEstado_covid', $modelEstado_covid)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
        $model = new Seg_registro_resultado;



        $arrayOpciones = json_decode($request->txtHiddenDatos);
        // print_r($arrayOpciones[0]->idSeg_metodo_diagnostico); 
        print_r($request->idSeg_metodo_diagnostico);

        $model->correlativo = $request->correlativo;
        $model->registro_laboratorio = $request->registro_laboratorio;
        $model->registro_sistema = $request->registro_sistema;
        $model->muestreo = $request->muestreo;
        $model->envio = $request->envio;
        $model->recepcion = $request->recepcion;
        $model->idseg_tipo_muestra = $request->idseg_tipo_muestra;
        $model->fecha_toma = date('Y-m-d', strtotime($request->fecha_toma));
        $model->hora_toma = $request->hora_toma;

        $model->numero_historia = strtoupper($request->numero_historia);      
        $model->bioquimico_responsable_toma = strtoupper($request->bioquimico_responsable_toma);
        $model->idalmacen = $request->idalmacen;              

        return;
         
        if($model->save())
        {             
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $modelEspecialidad = Ima_especialidad::where([
                            ['eliminado', '=', 0],
                            ['idalmacen', '=', $arrayDatos['idalmacen']]
                        ])->get();

        $model = Ima_plantillas::find($id);
        
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view')
                ->with('model', $model)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $modelDato = Seg_registro_resultado::where('idseg_ficha_generada', '=', $id)->first();        

        if ($modelDato == null) {
            $model = new Seg_registro_resultado;
            $modelResultado = new Seg_resultado;
        }else{
            $model = $modelDato;
            $modelResultado = Seg_resultado::where('idseg_registro_resultado', '=', $model->id)->first();
        } 
 
        $modelTipo_muestra = Seg_tipo_muestra::get();
        $modelEstado_covid = Seg_estado_cov::get();
        $modelHospital = Seg_establecimiento::get();
        $modelMetodo = Seg_metodo_diagnostico::get();
        $modelMetodo_valores = Seg_metodo_valores::get(); 
        $modelEnvio = Seg_envio_laboratorio::get();
        $modelRecepcion = Seg_recepcion_laboratorio::get();

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelMetodo', $modelMetodo)
                ->with('modelTipo_muestra', $modelTipo_muestra)
                ->with('modelHospital', $modelHospital) 
                ->with('modelResultado', $modelResultado)
                ->with('modelMetodo_valores', $modelMetodo_valores)
                ->with('modelEstado_covid', $modelEstado_covid)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modelDato = Seg_registro_resultado::where('idseg_ficha_generada', '=', $id)->first();        

        if ($modelDato == null) {
            $model = new Seg_registro_resultado;
            $modelResultado = new Seg_resultado;
        }else{
            $model = $modelDato;
            $modelResultado = Seg_resultado::where('idseg_registro_resultado', '=', $model->id)->first(); 
        } 
        $model->registro_laboratorio = $request->registro_laboratorio;
        $model->envio = $request->envio;
        $model->recepcion = $request->recepcion;
        $model->idseg_tipo_muestra = $request->idseg_tipo_muestra;
        $model->fecha_toma = date('Y-m-d', strtotime($request->fecha_toma));
        $model->hora_toma = $request->hora_toma;
        $model->iddatosgenericos_responsable_toma = $request->iddatosgenericos_responsable_toma;
        $model->txtResponsable_muestra = $request->bioquimico_responsable_toma; 
        $model->idSeg_ficha_generada = $request->id;
        $model->idalmacen = $request->idalmacen;    

        if($model->save()){
            if ($model->envio == 1) { 
                Seg_envio_laboratorio::registra_envioLaboratorio($model, $request);
            }
            if ($model->recepcion == 1) { 
                Seg_recepcion_laboratorio::registra_recepcionLaboratorio($model, $request);
            }            
            $mensaje = config('app.mensajeGuardado');
        }    
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje);
        }
        else
            return redirect($this->controlador);
    } 

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimeResultado($id)
    {
        Seg_registro_resultado::imprimeResultado($id);
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------

}