<?php
namespace App\Http\Controllers\Seguimiento;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use App\Models\Administracion\Datosgenericos;
use \App\Models\Almacen\Almacen;

use \App\Models\Seguimiento\Seg_estado_cov;
use \App\Models\Seguimiento\Seg_establecimiento;
use \App\Models\Seguimiento\Seg_ficha_generada;
use \App\Models\Seguimiento\Seg_tipo_muestra;
use \App\Models\Seguimiento\Seg_tipo_paciente;
use \App\Models\Seguimiento\Seg_metodo_diagnostico;
use \App\Models\Seguimiento\Seg_registro_resultado;
use \App\Models\Seguimiento\Seg_metodo_valores; 
use \App\Models\Seguimiento\Seg_envio_laboratorio; 
use \App\Models\Seguimiento\Seg_resultado; 
use \App\Models\Seguimiento\Seg_resultado_valido; 


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;

use Illuminate\Support\Facades\URL;
use Session;

class Seg_registro_resultado_MedController extends Controller
{
    private $rutaVista = 'Seguimiento.seg_registro_resultado_med.';
    private $controlador = 'seg_registro_resultado';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        //$this->middleware('auth');
    } 

    public function index(Request $request)
    {
        Session::forget('url_'.$this->controlador);
        $arrayDatos = Home::parametrosSistema();
        $arrayDatos['mini'] = 1;
        
        $modelEstadoLaboratorio = Seg_estado_cov::get();
        $modelMetodo_Laboratorio = Seg_metodo_diagnostico::get(); 

        $datosBuscador = $request->datosBuscador;
        $model = Seg_registro_resultado::resultados_covLaboratorio($datosBuscador, 2)->paginate(config('app.pagination'));
         
        
        $model->datosBuscador = $request->datosBuscador;

        $model->medico = 1;
        $model->accion = $this->controlador;
        $model->scenario = 'index';
        $model->rutaview = $this->rutaVista;
        $model->search = $request->search;

        // imprimir resultado
        $model->datos_imprimir = '';
        if (Session::has('datos_imprimir'))
            $model->datos_imprimir = Session::get('datos_imprimir');

        return view($model->rutaview.'index')
                ->with('model', $model)                
                ->with('modelEstadoLaboratorio', $modelEstadoLaboratorio) 
                ->with('modelMetodo_Laboratorio', $modelMetodo_Laboratorio) 
                ->with('arrayDatos', $arrayDatos);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        $model = Seg_resultado_valido::find($id);

        $modelSeg_ficha_generada = Seg_ficha_generada::find($model->idseg_ficha_generada);
        $modelResultado = Seg_resultado::find($model->idseg_resultado);
        $modelRegistroResultado = Seg_registro_resultado::find($model->idseg_resultado);

        $modelDatosgenericos = Datosgenericos::orderBy('nombres', 'ASC')->get();

        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;

        // if(!Session::has('url_'.$this->controlador))
        //     Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.'edit')
                ->with('model', $model)
                ->with('modelSeg_ficha_generada', $modelSeg_ficha_generada)
                ->with('modelResultado', $modelResultado)
                ->with('modelRegistroResultado', $modelRegistroResultado)
                ->with('modelDatosgenericos', $modelDatosgenericos)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Seg_resultado_valido::find($id);

        $Dato = Datosgenericos::find($request->iddatosgenericos_responsable);
        if($Dato != null) {
            $model->iddatosgenericos_responsable = $Dato->id;
            $model->medico_responsable = $Dato->nombres.' '.$Dato->apellidos;
        }
        $model->fecha_validacion = date('Y-m-d');
        $model->medico_autorizado = $request->autorizado;
        $model->medico_valido = $request->valido;
        $model->descripcion = $request->descripcion;
        $model->save();
        
        return redirect('seg_registro_resultado_med');
    }

    
    public function seleccionar() {
        if(isset($_POST['id'])) {
            $model = new Seg_resultado_valido();
            $model->idseg_resultado = $_POST['idseg_resultado'];
            $model->idseg_ficha_generada = $_POST['id'];
            $model->save();
        }
    }
    
}