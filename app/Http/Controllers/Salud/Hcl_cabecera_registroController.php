<?php
namespace App\Http\Controllers\Salud;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use App\Http\Requests\AlmacenRequest;
use \App\Models\Almacen\Almacen;
use Illuminate\Http\Request; 
use Auth;
use \App\Models\Almacen\Home;
use \App\Permission;
use \App\Models\Usuario\Almacenusuario;
use Illuminate\Support\Facades\DB;

use \App\Models\Administracion\Datosgenericos;
// farmacia
use \App\Models\Farmacia\Far_cabecera_receta;


use \App\Models\Afiliacion\Hcl_variables_signos_vitales;
use \App\Models\Afiliacion\Hcl_variables_bajas;
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_cuaderno_personal;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_cabecera_registro;
use \App\Models\Afiliacion\Hcl_estado;
use \App\Models\Afiliacion\Hcl_registro_bajas_medicas;
use \App\Models\Afiliacion\Hcl_registro_referencias_medicas;
use \App\Models\Afiliacion\Hcl_cie;
use \App\Models\Afiliacion\Hcl_diagnosticosCIE;
use \App\Models\Afiliacion\Hcl_datos_signos_vitales;
use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cabecera_registro_documento;
use \App\Models\Afiliacion\Hcl_internacion;

use \App\Models\Imagenologia\Ima_pacs;
use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Imagenologia\Ima_registroprogramacion;
use \App\Models\Imagenologia\Ima_resultados_programacion;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;
use \App\Models\Imagenologia\Ima_estado;

use \App\Models\Laboratorio\Lab_registroprogramacion;
use \App\Models\Laboratorio\Lab_examenes_registroprogramacion;
use \App\Models\Laboratorio\Lab_resultados_programacion;
use \App\Models\Laboratorio\Lab_estado;

use \App\Models\Farmacia\Far_adm_estado;
use \App\Models\Farmacia\Far_cab_receta_detalle;
use \App\Models\Farmacia\Far_cab_detalle;

use \App\Models\Inventario\Inv_cabecera_receta;
use \App\Models\Inventario\Inv_estado;

use Elibyy\TCPDF\Facades\TCPDF;
use Session;
use Illuminate\Support\Facades\URL;

class Hcl_cabecera_registroController extends Controller
{
    private $rutaVista = 'Salud.hcl_cabecera_registro.';
    private $controlador = 'hcl_cabecera_registro';

    /*
        embarazo    (AZUL)
        emergencia  (ROJO)
        otros...    (VERDE)
        
    */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();
        $fecha_resultado = date("Y").'-'.date("m").'-'.date("d");
          
        // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', '');

        $date = date_create($request->fechaRegistro);
        $buscarFecha = $request->buscarFecha;
        $fechaRegistro = date_format($date, 'Y-m-d');
        $fechaRegistro = $fechaRegistro != $fecha_resultado? $fechaRegistro : $fecha_resultado;

        $dato = $request->searchNuevo;
        $model = DB::table('hcl_cabecera_registro_view')
                    ->where([
                        ['eliminado', '=', 0],
                        ['idalmacen', '=', $arrayDatos['idalmacen']],
                        ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']],
                        ['idhcl_cuaderno', '=', $request->hcl_cuaderno_personal],
                    ])
                    ->where(function($query) use($fechaRegistro, $buscarFecha) {
                    $query->where([
                        ['fecha_registro', $buscarFecha == 1? '!=' : '=', $buscarFecha == 1? null : $fechaRegistro]]);
                    })
                    ->where(function($query) use($dato) {
                        $query->orwhere('hcl_poblacion_nombrecompleto', 'like', '%'.$dato.'%');
                        $query->orwhere('hcl_poblacion_matricula', 'like', '%'.$dato.'%');                        
                    })
                    ->orderBy('numero', 'DESC')
                    ->get();

        $model->scenario = 'index';
        $model->search = $request->search;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;

        $model->hcl_cuaderno_personal = $request->hcl_cuaderno_personal;
     
        $cuaderno_personal = DB::table('hcl_cuaderno_personal as cp')
                        ->join('hcl_cuaderno as c', 'cp.idhcl_cuaderno', '=', 'c.id')
                        ->where([
                            ['cp.eliminado', '=', 0],
                            ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
                        ])
                        ->orderBy('c.nombre', 'ASC')
                        ->get();
        if($model->hcl_cuaderno_personal > 0)
        {
            session(['session_idhcl_cuaderno' => $model->hcl_cuaderno_personal]); 
        }
        else
        {
            if(isset($cuaderno_personal[0]))
                session(['session_idhcl_cuaderno' => $cuaderno_personal[0]->idhcl_cuaderno]);
        }

        // imprimir resultado
        $model->fechaRegistro = $fechaRegistro;
        $model->buscarFecha = $buscarFecha;
        // $model->fecha_actual = date('d-m-Y');
        // print_r($model->fecha_actual);
        // return;
        $model->searchNuevo = $dato;
        $model->datos_imprimir = '';
        if (Session::has('datos_imprimir'))
            $model->datos_imprimir = Session::get('datos_imprimir');

        return view($model->rutaview.'index')
                ->with('model', $model)
                ->with('cuaderno_personal', $cuaderno_personal)
                ->with('arrayDatos', $arrayDatos);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.create') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelHcl_variables_bajas = Hcl_variables_bajas::where('eliminado', '=', 0)->get(); 
        $modelEstablecimiento = Almacen::where('eliminado', '=', 0)->get();
        $modelEspecialidad = Hcl_cuaderno::where('eliminado', '=', 0)->get();

        $modelHcl_especialidad = ['' => 'Seleccione! '] + 
                                    Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->pluck('descripcion', 'id')->all();

        $model = new Hcl_cabecera_registro;
        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->accion = $this->controlador;
        $model->scenario = 'create';
        $model->rutaview = $this->rutaVista;

        $model->idalmacen = $arrayDatos['idalmacen'];
        $model->iddatosgenericos = $arrayDatos['iddatosgenericos'];

        $vista = 'create'; 
        if(Session::has('session_idhcl_cuaderno'))
        {             
            $idhcl_cuaderno = Session::get('session_idhcl_cuaderno');
            $modelHcl_cuaderno = Hcl_cuaderno::find($idhcl_cuaderno);

            $model->taDescripcion_formato_historia = $modelHcl_cuaderno->formato_historia;
            $model->hcl_cuaderno = $modelHcl_cuaderno->nombre;
            $model->idhcl_cuaderno = $modelHcl_cuaderno->id;
            $model->nivel_seguridad = $modelHcl_cuaderno->nivel_seguridad;
        }
        else
            $vista = 'cuadernoVacio';

        $modelHcl_cabecera_registro_documento = array();
        $model->fechaDesde = date('01-m-Y');
        $model->fechaHasta = date('d-m-Y');
        $model->hcl_clasificacionsalud = DB::table('hcl_clasificacionsalud')->get()->toJson();
        
        if(!Session::has('url_'.$this->controlador))
            Session::put('url_'.$this->controlador, URL::previous());
        return view($model->rutaview.''.$vista)
                ->with('model', $model)
                ->with('modelEstablecimiento', $modelEstablecimiento)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('modelHcl_especialidad', $modelHcl_especialidad) 
                ->with('modelHcl_variables_bajas', $modelHcl_variables_bajas) 
                ->with('modelHcl_cabecera_registro_documento', $modelHcl_cabecera_registro_documento)
                ->with('arrayDatos', $arrayDatos);  
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    protected function store(Request $request)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);
        $jsonDatoExtra = json_decode($txtHiddenJSONdatos->jsonDatoExtra);
        $gridDocumentodetalle = json_decode($request->txtcontenidoTabla); 
        
        $model = new Hcl_cabecera_registro;
        $model->numero = Hcl_cabecera_registro::generarNumero($request->idalmacen);
        $model->idhcl_poblacion = $request->txtidhcl_poblacion;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos;
        $model->fecha_registro = date('Y-m-d');
        $model->hora_registro = date('g:i');
        $model->idhcl_estado = Hcl_estado::VIGENTE;
        $model->tipo_consulta = $request->tipo_consulta; // 1 = NUEVA       2 = REPETIDA
        // $model->idhcl_cie = $request->txtIdhcl_cie; // Diagnóstico        
        $model->respuesta = $request->taDescripcion_formato_historia;
        $model->hospitalizado = $request->checkHospitalizado == true? 1 : 0;
        
        if($jsonDatoExtra->idsexo == 1) // MASCULINO
            $model->embarazada = 0;
        else // FEMENINO
            $model->embarazada = $jsonDatoExtra->embarazada;

        $model->emergencia = $jsonDatoExtra->emergencia;
        $model->nivel_seguridad = $jsonDatoExtra->seguridad; // 1 = VISIBLE     2 = OCULTO
        $model->observaciones_imagenologia = $jsonDatoExtra->observaciones_imagenologia;
        $model->observaciones_laboratorio = $jsonDatoExtra->observaciones_laboratorio;
        $model->descripcion_certificado_med = $jsonDatoExtra->Descripcion_CertificadoMedico;
        $model->idalmacen = $request->idalmacen;
        $model->usuario = Auth::user()['name'];
        // -- ================== [ DATOS PACIENTE ] ======================
        $Hcl_poblacion = Hcl_poblacion::muestraDatosPoblacion($request);
        $model->hcl_poblacion_nombrecompleto = $Hcl_poblacion['hcl_poblacion_nombrecompleto'];
        $model->hcl_poblacion_matricula = $Hcl_poblacion['hcl_poblacion_matricula'];
        $model->hcl_poblacion_cod = $Hcl_poblacion['hcl_poblacion_cod'];
        $model->hcl_poblacion_sexo = $Hcl_poblacion['hcl_poblacion_sexo'];
        $model->hcl_poblacion_fechanacimiento = $Hcl_poblacion['hcl_poblacion_fechanacimiento'];
        $model->hcl_empleador_empresa = $Hcl_poblacion['hcl_empleador_empresa'];
        $model->hcl_empleador_patronal = $Hcl_poblacion['hcl_empleador_patronal'];
        $model->diagnostico_seguro = $jsonDatoExtra->diagnostico_seguro;
        $model->respuesta_seguro = $jsonDatoExtra->respuesta_seguro; 
        // $jsonReferenciaMedica = json_decode($request->txtDatosJsonReferenciaMedica);  
        // print_r($jsonReferenciaMedica);
        // return;

        if($model->save())        
        {
            $datos_imprimir = $model->hcl_poblacion_nombrecompleto.'*|*'.$model->id;
            $this->registrarDetalle($model, $request);

            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado'); 

        if(Session::has('url_'.$this->controlador))
        {
            $sesion = Session::get('url_'.$this->controlador);
            Session::forget('url_'.$this->controlador);
            return redirect()->to($sesion)
                            ->with('message', 'store')
                            ->with('mensaje', $mensaje)
                            ->with('datos_imprimir', $datos_imprimir);
        }
        else
            return redirect($this->controlador);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($codigo)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.show') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $model = Ima_especialidad::find($codigo);
        $model->accion = $this->controlador;
        $model->scenario = 'view';
        $model->rutaview = $this->rutaVista;
        
        return view($model->rutaview.'view',compact('model'))
                ->with('arrayDatos', $arrayDatos); 
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arrayDatos = Home::parametrosSistema();

        // if(Permission::verificarAcceso($this->controlador.'.edit') == 0)
        //     return view('errors.403')
        //                 ->with('arrayDatos', $arrayDatos)
        //                 ->with('url', $this->controlador);

        $modelHcl_variables_bajas = Hcl_variables_bajas::where('eliminado', '=', 0)->get();
        $modelEstablecimiento = Almacen::where('eliminado', '=', 0)->get();
        $modelEspecialidad = Hcl_cuaderno::where('eliminado', '=', 0)->get();
        
        $modelHcl_especialidad = ['' => 'Seleccione! '] + 
                                    Hcl_especialidad::where('eliminado', '=', 0)
                                    ->orderBy('descripcion', 'ASC')->pluck('descripcion', 'id')->all();

        $model = Hcl_cabecera_registro::find($id);
        $model->taDescripcion_formato_historia = $model->respuesta;
        $model->accion = $this->controlador;
        $model->scenario = 'edit';
        $model->rutaview = $this->rutaVista;
        // $modelHcl_cie = Hcl_cie::find($model->idhcl_cie);
        // if($modelHcl_cie != null)
        // {
        //     $model->txtHcl_cie = $modelHcl_cie->descripcion.' ('.$modelHcl_cie->codigo.')';
        // }
        // [ CIE ]
        if($model != null)
        {
            $txtcontenidoTablaDiagnosticos = Hcl_diagnosticosCIE::where([
                        ['eliminado', '=', 0],
                        ['idhcl_cabecera_registro', '=', $model->id]
                    ])->get()->toJson();
            $model->documentodetalleDiagnostico = $txtcontenidoTablaDiagnosticos;

            // $txtcontenidoEstablecimiento = Almacen::where('eliminado', '=', 0)->get()->toJson();
            // $txtcontenidoCuaderno = Hcl_cuaderno::where('eliminado', '=', 0)->get()->toJson();
            
            // $model->documentodetalleEstablecimiento = $txtcontenidoEstablecimiento;
            // $model->documentodetalleCuaderno = $txtcontenidoCuaderno;
        }
        // [ HASTA AQUI EL CIE ]

        // print_r($model);
        // return;

        $Hcl_poblacion = Hcl_poblacion::
                    select('id', DB::raw("CONCAT(nombre, ' ', primer_apellido, ' ', segundo_apellido) as nombres"),
                        'numero_historia', 'documento', 
                        DB::raw("date_part('year',age(fecha_nacimiento)) as edad"),
                        DB::raw("CASE WHEN sexo = 1 THEN 'Masculino' ELSE 'Femenino' end as sexo"),
                        DB::raw("'FALTA...' as procedencia"), DB::raw("'FALTA...' as residencia"),
                        DB::raw("'NO HAY EL CAMPO' as ocupacion")
                    )
                    ->find($model->idhcl_poblacion)->toJson();

        // [ RECETAS ]
        $Far_recetas_dispensadas = ''; 
        $recetas_registradas = Far_cabecera_receta::select('id') 
                    ->where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])->first();
 
        if($recetas_registradas != null)
        {  
            $Far_recetas_dispensadas = DB::table('far_cab_detalle as a')
            ->select('a.id','a.idfar_articulo_sucursal','a.indicacion_articulo','a.cantidad','a.eliminado','c.codificacion','c.descripcion','c.unidad','c.concentracion') 
                ->join('far_articulo_sucursal as b', 'a.idfar_articulo_sucursal', '=', 'b.id')                
                ->join('far_adm_articulo as c', 'b.idfar_adm_articulo', '=', 'c.id') 
                ->where('a.eliminado','=',0)
                ->whereIn('a.idfar_cab_receta_detalle',[DB::raw('select id from far_cab_receta_detalle where idfar_cabecera_receta = '.$recetas_registradas->id)] )
                ->get()->toJson(); 
        } 
        
        // [ LABORATORIO ]
        $Lab_examenes_registroprogramacion = '';
        $Lab_registroprogramacion = Lab_registroprogramacion::where([
                    ['eliminado', '=', 0],
                    ['idlab_estado', '!=', Lab_estado::ANULADO],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])->first();
        if($Lab_registroprogramacion != null)
        {
            $Lab_examenes_registroprogramacion = Lab_examenes_registroprogramacion::where([
                        ['eliminado', '=', 0],
                        ['idlab_registroprogramacion', '=', $Lab_registroprogramacion->id]
                    ])->get()->toJson();
        }        


        // [ IMAGENOLOGIA ]
        $Ima_examenes_registroprogramacion = '';
        $Ima_registroprogramacion = Ima_registroprogramacion::where([
                        ['eliminado', '=', 0],
                        ['idima_estado', '!=', Ima_estado::ANULADO],
                        ['idhcl_cabecera_registro', '=', $model->id]
                    ])->first();

        if($Ima_registroprogramacion != null)
        {
            $Ima_examenes_registroprogramacion = Ima_examenes_registroprogramacion::where([
                        ['eliminado', '=', 0],
                        ['idima_registroprogramacion', '=', $Ima_registroprogramacion->id]
                    ])->get()->toJson(); 
        }

        // [ REFERENCIA MEDICA ]
        $Hcl_registro_referencias_medicas = Hcl_registro_referencias_medicas::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get()->toJson();

        // [ BAJA MEDICA ]
        $Hcl_registro_bajas_medicas = Hcl_registro_bajas_medicas::
                            select('id', DB::raw("fecha_inicio as fecha_inicio"), 
                                DB::raw("fecha_fin as fecha_fin"),
                                'cantidad_dias', 'idhcl_variables_bajas', 'descripcion'
                            )
                            ->where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get()->toJson();

        // [ SIGNOS VITALES ]
        $Hcl_datos_signos_vitales = Hcl_datos_signos_vitales::where([
                                    ['eliminado', '=', 0],
                                    ['idhcl_cabecera_registro', '=', $model->id]
                                ])->get();

        if(count($Hcl_datos_signos_vitales) == 0) // No existe registro
        {
            $edad = json_decode($Hcl_poblacion)->edad;
            $Hcl_datos_signos_vitales = Hcl_variables_signos_vitales::where([
                                // ['idalmacen', '=', $model->idalmacen],
                                ['edad_final', config('app.edad_minimo') <= $edad? '>=' : '!=', $edad]
                            ])->get();
        }
        $Hcl_datos_signos_vitales = $Hcl_datos_signos_vitales->toJson();
        
        
        $array_Informacion = array(
            'Hcl_poblacion' => $Hcl_poblacion,
            'Lab_examenes_registroprogramacion' => $Lab_examenes_registroprogramacion,
            'Far_recetas_dispensadas' => $Far_recetas_dispensadas, 
            'Ima_examenes_registroprogramacion' => $Ima_examenes_registroprogramacion,
            'Hcl_registro_bajas_medicas' => $Hcl_registro_bajas_medicas,
            'Hcl_registro_referencias_medicas' => $Hcl_registro_referencias_medicas,
            'Hcl_datos_signos_vitales' => $Hcl_datos_signos_vitales,
            // --------------------------- DATOS EXTRAS ---------------------------
            'tipo_consulta' => $model->tipo_consulta,
            'embarazada' => $model->embarazada,
            'emergencia' => $model->emergencia,
            'observaciones_imagenologia' => $model->observaciones_imagenologia,
            'observaciones_laboratorio' => $model->observaciones_laboratorio,
            'certificadoMedico' => $model->descripcion_certificado_med,
            'diagnostico_seguro' => $model->diagnostico_seguro,
            'respuesta_seguro' => $model->respuesta_seguro,
        );
        $model->txtHiddenJSONdatos = json_encode($array_Informacion);

        $modelHcl_cabecera_registro_documento = Hcl_cabecera_registro_documento::where([
                                            ['eliminado', '=', 0],
                                            ['idhcl_cabecera_registro', '=', $id]
                                        ])->get();
        $model->fechaDesde = date('01-m-Y');
        $model->fechaHasta = date('d-m-Y');
        $model->hcl_clasificacionsalud = DB::table('hcl_clasificacionsalud')->get()->toJson();

        if($model->idhcl_cuaderno > 0)
        {
            $modelHcl_cuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);

            $model->hcl_cuaderno = $modelHcl_cuaderno->nombre;
            $model->nivel_seguridad = $modelHcl_cuaderno->nivel_seguridad;
        }
        
        Session::put('urlAnterior', URL::previous());
        return view($model->rutaview.'edit')
        ->with('model', $model)
                ->with('modelEstablecimiento', $modelEstablecimiento)
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('modelHcl_especialidad', $modelHcl_especialidad)
                ->with('modelHcl_variables_bajas', $modelHcl_variables_bajas)
                ->with('modelHcl_cabecera_registro_documento', $modelHcl_cabecera_registro_documento)
                ->with('arrayDatos', $arrayDatos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);
        $jsonDatoExtra = json_decode($txtHiddenJSONdatos->jsonDatoExtra);
        $datosTablaLab_laboratorio = $txtHiddenJSONdatos->datosTablaLab;
        $datosTabla_imagenologia = $txtHiddenJSONdatos->datosTabla;

        // $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);
        // $jsonDatoExtra = json_decode($txtHiddenJSONdatos->jsonDatoExtra);

        $model = Hcl_cabecera_registro::find($id);
        $model->tipo_consulta = $request->tipo_consulta; // 1 = NUEVA       2 = REPETIDA
        $model->nivel_seguridad = 1; // 1 = VISIBLE     2 = OCULTO
        $model->idhcl_cie = $request->txtIdhcl_cie;
        $model->respuesta = $request->taDescripcion_formato_historia;
        $model->embarazada = $jsonDatoExtra->embarazada;
        $model->emergencia = $jsonDatoExtra->emergencia;
        $model->observaciones_imagenologia = $jsonDatoExtra->observaciones_imagenologia;
        $model->observaciones_laboratorio = $jsonDatoExtra->observaciones_laboratorio;
        $model->diagnostico_seguro = $jsonDatoExtra->diagnostico_seguro;
        $model->respuesta_seguro = $jsonDatoExtra->respuesta_seguro;
        $model->descripcion_certificado_med = $jsonDatoExtra->Descripcion_CertificadoMedico;

        // print_r(json_decode($request->txtcontenidoTablaPrescripcion));
        // return;
        // $this->registrarDetalle($model, $request);
        // return;
        if($model->save())
        {
            // ====================================================================== [ LABORATORIO (1) ]
            $Lab_registroprogramacion = Lab_registroprogramacion::where([
                    ['eliminado', '=', 0],
                    ['idlab_estado', '!=', Lab_estado::ANULADO],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])
                ->first();
            if($Lab_registroprogramacion != null)
            {
                // Valida si existen exámenes y elimina virtualmente de forma interna...
                if($datosTablaLab_laboratorio != null) {
                    Lab_examenes_registroprogramacion::where([
                                        ['eliminado', '=', 0],
                                        ['idlab_registroprogramacion', '=', $Lab_registroprogramacion->id]
                                    ])->update(['eliminado' => true]);
                    Lab_registroprogramacion::where([
                                        ['eliminado', '=', 0],
                                        ['id', '=', $Lab_registroprogramacion->id]
                                    ])->update(['eliminado' => true]);
                    Lab_resultados_programacion::where([
                                        ['eliminado', '=', 0],
                                        ['idlab_registroprogramacion', '=', $Lab_registroprogramacion->id]
                                    ])->update(['eliminado' => true]);
                }
                else // Si NO existen exámenes el anterior registro cambia el estado a ANULADO
                {
                    Lab_registroprogramacion::where([
                                        ['eliminado', '=', 0],
                                        ['id', '=', $Lab_registroprogramacion->id]
                                    ])->update(['idlab_estado' => Lab_estado::ANULADO]);
                }
            }
            // ====================================================================== [ IMAGENOLOGIA (1) ]
            $Ima_registroprogramacion = Ima_registroprogramacion::where([
                        ['eliminado', '=', 0],
                        // ['idima_estado', '!=', Ima_estado::ENTREGADO],
                        ['idhcl_cabecera_registro', '=', $model->id]
                    ])
                    ->get(); 

            if($Ima_registroprogramacion != null)
            {
                // Valida si existen exámenes y elimina virtualmente de forma interna...
                foreach ($Ima_registroprogramacion as $dato) {
                    if ($dato->idima_estado != Ima_estado::ENTREGADO) {
                        if($datosTabla_imagenologia != null) {
                            Ima_examenes_registroprogramacion::where([
                                                ['eliminado', '=', 0],
                                                ['idima_registroprogramacion', '=', $dato->id]
                                            ])->update([
                                                'eliminado' => true,
                                            ]);

                            Ima_registroprogramacion::where([
                                                ['eliminado', '=', 0],
                                                ['id', '=', $dato->id]
                                            ])->update([
                                                'eliminado' => true,
                                                'idima_estado' => Ima_estado::ANULADO
                                        ]);

                            Ima_resultados_programacion::where([
                                                ['eliminado', '=', 0],
                                                ['idima_registroprogramacion', '=', $dato->id]
                                            ])->update([
                                                'eliminado' => true,
                                                'idima_estado' => Ima_estado::ANULADO
                                            ]);

                        }
                        else // Si NO existen exámenes el anterior registro cambia el estado a ANULADO
                        {
                            Ima_registroprogramacion::where([
                                                ['eliminado', '=', 0],
                                                ['id', '=', $dato->id]
                                            ])->update([
                                                'eliminado' => true,
                                                'idima_estado' => Ima_estado::ANULADO
                                            ]);
                        }
                    }
                } 
            }

            // ====================================================================== [ DIAGNOSTICOS - CIE ]
            Hcl_diagnosticoscie::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])->update(['eliminado' => true]);
            // ====================================================================== [ REFERENCIA MEDICA ]
            Hcl_registro_referencias_medicas::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])->update(['eliminado' => true]);
            // ====================================================================== [ BAJA MEDICA ]
            Hcl_registro_bajas_medicas::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])->update(['eliminado' => true]);
            // ====================================================================== [ Signos Vitales ]
            Hcl_datos_signos_vitales::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $model->id]
                ])->update(['eliminado' => true]);


            // Vuelve..a insertar nuevamente...TODAS...las opciones de SALUD
            $this->registrarDetalle($model, $request);



            // ********************************* [ LABORATORIO (2) ] *********************************
            if($Lab_registroprogramacion != null)
            {
                $Lab_registroprogramacion_nuevo = Lab_registroprogramacion::where([
                        ['eliminado', '=', 0],
                        ['idhcl_cabecera_registro', '=', $model->id]
                    ])
                    ->orderBy('id', 'DESC')
                    ->first();
                if($Lab_registroprogramacion_nuevo != null)
                {
                    $Lab_registroprogramacion_nuevo->numero = $Lab_registroprogramacion->numero;
                    $Lab_registroprogramacion_nuevo->save();
                }
            }
            // ********************************* [ IMAGENOLOGIA (2) ] *********************************
            // if($Ima_registroprogramacion != null)
            // {
            //     $Ima_registroprogramacion_nuevo = Ima_registroprogramacion::where([
            //             ['eliminado', '=', 0],
            //             ['idhcl_cabecera_registro', '=', $model->id]
            //         ])
            //         ->orderBy('id', 'DESC')
            //         ->get();
                
            //     if($Ima_registroprogramacion_nuevo != null)
            //     {
            //         $Ima_registroprogramacion_nuevo->numero = $Ima_registroprogramacion->numero;
            //         $Ima_registroprogramacion_nuevo->save();
            //     }
            // }
            
            $mensaje = config('app.mensajeGuardado');
        }
        else
            $mensaje = config('app.mensajeErrorGuardado');
        
        if(Session::has('urlAnterior'))
        {
            return redirect()->to(Session::get('urlAnterior'))
                            ->with('message', 'update')
                            ->with('mensaje', $mensaje);
                            // ->with('datos_imprimir', $datos_imprimir);
        }
        else
            return redirect("consulta_externa");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function registrarDetalle($model, $request)
    {
        $txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);
        $datosTablaLab_laboratorio = $txtHiddenJSONdatos->datosTablaLab;
        $datosTabla_imagenologia = $txtHiddenJSONdatos->datosTabla;
        $jsonBajaMedica = json_decode($txtHiddenJSONdatos->jsonBajaMedica);
        $jsonReferenciaMedica = json_decode($request->txtDatosJsonReferenciaMedica);  
        $jsonInternacionMedica = json_decode($request->txtDatosJsonInternacionMedica);  
        $jsonVariableSignosVitales = json_decode($txtHiddenJSONdatos->jsonVariableSignosVitales);
        $jsonDatoExtra = json_decode($txtHiddenJSONdatos->jsonDatoExtra);
        $jsonDiagnosticos = $txtHiddenJSONdatos->datosDiagnostico; 
        $jsonPrescripciones = json_decode($request->txtcontenidoTablaPrescripcion); 


        // ==================== Guarda "DIAGNOSTICOS" ====================
        if($jsonDiagnosticos != null) {
            $arrayValor = array(                
                'model' => $model,                
                'arrayDiagnosticos' => $jsonDiagnosticos
            );
            Hcl_diagnosticosCIE::registraDiagnosticos($arrayValor);            
        }

        // ==================== Guarda "PRESCRIPCIONES" ====================
        if($jsonPrescripciones != null) {
            $arrayValor = array(
                'request' => $request,
                'model' => $model,
                'idalmacen' => $request->idalmacen,
                'arrayPrescripciones' => $jsonPrescripciones
            );            
            Far_cabecera_receta::registraProgramacion_Prescripcion($arrayValor);
        }

        // ==================== Guarda "LABORATORIO" ====================
        if($datosTablaLab_laboratorio != null) {
            $arrayValor = array(
                'request' => $request,
                'model' => $model,
                'idalmacen' => $request->idalmacen,
                'arrayExamenes' => $datosTablaLab_laboratorio
            );
            Lab_registroprogramacion::registraProgramacion_Laboratorio($arrayValor);
        }

        // ==================== Guarda "IMAGENOLOGIA" ====================
        if($datosTabla_imagenologia != null) {
            $arrayValor = array(
                'request' => $request,
                'model' => $model,
                'idalmacen' => $request->idalmacen,
                'arrayExamenes' => $datosTabla_imagenologia
            );
            Ima_registroprogramacion::registraProgramacion_Imagenologia($arrayValor);
        }
        
        // ==================== Guarda "BAJA MÉDICA" ====================
        if($jsonBajaMedica != '') {
            $modelo = new Hcl_registro_bajas_medicas;
            $modelo->fecha_inicio = date('Y-m-d', strtotime($jsonBajaMedica->fechaDesde));
            $modelo->fecha_fin = date('Y-m-d', strtotime($jsonBajaMedica->fechaHasta));
            $modelo->cantidad_dias = $jsonBajaMedica->txtCantidadBajaMedica;
            $modelo->idhcl_variables_bajas = $jsonBajaMedica->txtIdHcl_variables_bajas;
            $modelo->idhcl_cabecera_registro = $model->id;
            $modelo->descripcion = $jsonBajaMedica->descripcion;
            $modelo->usuario = Auth::user()['name'];
            $modelo->save();
        }
        
        // ==================== Guarda "SIGNOS VITALES" ====================
        if($jsonDatoExtra->editadoVariablesSignosVitales > 0) {

            for($i = 0; $i < count($jsonVariableSignosVitales); $i++)
            {
                if(isset($jsonVariableSignosVitales[$i]->contenido_variable))
                    $contenido_variable = $jsonVariableSignosVitales[$i]->contenido_variable;
                else
                    $contenido_variable = ''; 
 
                if($jsonVariableSignosVitales[$i]->idhcl_variables_signos_vitales != 'undefined')
                {    
                    $modelo = new Hcl_datos_signos_vitales;
                    $modelo->idhcl_variables_signos_vitales = $jsonVariableSignosVitales[$i]->idhcl_variables_signos_vitales;
                    $modelo->idhcl_cabecera_registro = $model->id;
                    $modelo->contenido_variable = $contenido_variable;
                    $modelo->variable = $jsonVariableSignosVitales[$i]->variable;
                    $modelo->unidad = $jsonVariableSignosVitales[$i]->unidad;
                    $modelo->usuario = Auth::user()['name'];
                    $modelo->save();
                }             
            }
        }

        // ==================== Guarda "REFERENCIA MEDICA" ====================
        if($jsonReferenciaMedica != null) {
            $arrayValor = array(
                'request' => $request,
                'model' => $model,
                'idalmacen' => $request->idalmacen,
                'arrayReferenciaMedica' => $jsonReferenciaMedica
            );
            
            Hcl_registro_referencias_medicas::registraReferencia_Medica($arrayValor);
        }

        // ==================== Guarda "REFERENCIA MEDICA" ====================
        if($jsonInternacionMedica != null) {
            $arrayValor = array(
                'request' => $request,
                'model' => $model,
                'idalmacen' => $request->idalmacen,
                'arrayInternacionMedica' => $jsonInternacionMedica
            );
            
            Hcl_internacion::registraInternacion_Medica($arrayValor);
        }

        // ========================== Sube "ARCHIVOS" ==========================
        // Copia los archivos al sistema y almacena el detalle en una tabla de la bd
        $files = $request->file('archivo');
        if($request->hasFile('archivo')) {
            $arrayParametro = array(
                'files' => $files,
                'id' => $model->id
            );
            Hcl_cabecera_registro_documento::registraArchivos($arrayParametro);
        }
    }

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [BUSCADORES] ---------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function muestraVariables_signos_vitales()
    {
        if(isset($_POST['dato']))
        {
            $dato = $_POST['dato'];
            $idalmacen = $dato['idalmacen'];
            $edad = $dato['edad']; 
            
            $modelHcl_variables_signos_vitales = Hcl_variables_signos_vitales::where([
                                // ['idalmacen', '=', $idalmacen],
                                ['edad_final', config('app.edad_minimo') <= $edad? '>=' : '!=', $edad]
                            ])->get()->toJson();
            echo $modelHcl_variables_signos_vitales;
        }
    }
    public function muestra_Prescripcion()
    {
        if(isset($_POST['dato']))
        {
            $desde = $_POST['fechaDesde'] == ''? date('01-01-Y') : $_POST['fechaDesde'];
            $hasta = $_POST['fechaHasta'] == ''? date('d-m-Y') : $_POST['fechaHasta'];
            $fechaDesde = date('Y-m-d', strtotime($desde));
            $fechaHasta = date('Y-m-d', strtotime($hasta));
            $dato = $_POST['dato'];
            $idhcl_poblacion = $_POST['idhcl_poblacion'];

            $modelo = DB::table('muestra_precripciones_view')
                        ->where([
                            ['idhcl_poblacion', '=', $idhcl_poblacion],
                            ['idfar_adm_estado', '=', Far_adm_estado::DISPENSADO],
                        ])
                        ->get()->toJson();
            echo $modelo;
        }
    }

    public function elminar_Receta()
    {
        if(isset($_POST['dato']))
        {             
            $dato = $_POST['dato'];   
            Far_cab_detalle::where('id', '=', $dato)->update(['eliminado' => 1]);      
        }
    }

    // ---------------------------------------------- [IMAGENOLOGIA] ---------------------------------------------
    public function muestra_Imagenologia()
    {
        if(isset($_POST['dato']))
        {
            $desde = $_POST['fechaDesde'] == ''? date('01-01-Y') : $_POST['fechaDesde'];
            $hasta = $_POST['fechaHasta'] == ''? date('d-m-Y') : $_POST['fechaHasta'];
            $fechaDesde = date('Y-m-d', strtotime($desde));
            $fechaHasta = date('Y-m-d', strtotime($hasta));
            $dato = $_POST['dato'];
            $idhcl_poblacion = $_POST['idhcl_poblacion'];

            $modelo = DB::table('muestra_imagenologia_view')
                        ->where([
                            ['idhcl_poblacion', '=', $idhcl_poblacion]
                        ])
                        ->get()->toJson();
            echo $modelo;
        }
    } 
    // ---------------------------------------------- [FIN IMAGENOLOGIA] ---------------------------------------------
    public function muestra_Laboratorio()
    {
        if(isset($_POST['dato']))
        {
            $desde = $_POST['fechaDesde'] == ''? date('01-01-Y') : $_POST['fechaDesde'];
            $hasta = $_POST['fechaHasta'] == ''? date('d-m-Y') : $_POST['fechaHasta'];
            $fechaDesde = date('Y-m-d', strtotime($desde));
            $fechaHasta = date('Y-m-d', strtotime($hasta));
            $dato = $_POST['dato'];
            $idhcl_poblacion = $_POST['idhcl_poblacion'];

            $modelo = DB::table('muestra_laboratorio_view')
                        ->where([
                            ['idlab_estado', '!=', Lab_estado::ANULADO],
                            ['idhcl_poblacion', '=', $idhcl_poblacion]
                        ])
                        ->get()->toJson();
            echo $modelo;
        }
    }

    public static function converToPlain($text)
    {
        $text = preg_replace('"{\*?\\\\.+(;})|\\s?\\\[A-Za-z0-9]+|\\s?{\\s?\\\[A-Za-z0-9‹]+\\s?|\\s?}\\s?"', '', $text);
        return $text;
    }

    public function muestra_HistoriaClinico()
    {
        if(isset($_POST['dato']))
        {
            $desde = $_POST['fechaDesde'] == ''? date('01-01-Y') : $_POST['fechaDesde'];
            $hasta = $_POST['fechaHasta'] == ''? date('d-m-Y') : $_POST['fechaHasta'];
            $fechaDesde = date('Y-m-d', strtotime($desde));
            $fechaHasta = date('Y-m-d', strtotime($hasta));
            $dato = $_POST['dato'];
            $idhcl_poblacion = $_POST['idhcl_poblacion'];
            
            

            $modelo = DB::table('muestra_historialclinico_view')
                    ->where([
                        ['idhcl_poblacion', '=', $idhcl_poblacion]
                    ])
                    ->orderBy('fecha_registro', 'DESC')
                    ->get()->toJson();
            

            $arrayValor = array(
                'salud' => $modelo,
                'imagenologia' => 1,
                'certificadoMedico' => 0
            );
            echo json_encode($arrayValor);
        }
    }
 
    public function verifica_opciones_reporte()
    {
        if(isset($_POST['dato']))
        {
            $idhcl_cabecera_registro = $_POST['dato'];

            $cabecera_receta = Far_cabecera_receta::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $idhcl_cabecera_registro]
                ])->first();  
            if ($cabecera_receta != '') {
                $receta = DB::table('far_cab_receta_detalle as a')     
                ->join('far_cab_detalle as b', 'b.idfar_cab_receta_detalle', '=', 'a.id')  
                ->where([
                    ['b.eliminado', '=', 0],
                    ['a.idfar_cabecera_receta', '=', $cabecera_receta->id] 
                ])->count();     
            }else
                $receta = 0;           
  
            $laboratorio = Lab_registroprogramacion::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $idhcl_cabecera_registro]
                ])->count();

            $imagenologia = Ima_registroprogramacion::where([
                    ['eliminado', '=', 0],
                    ['idhcl_cabecera_registro', '=', $idhcl_cabecera_registro]
                ])->count();
            
            $Hcl_cabecera_registro = Hcl_cabecera_registro::where([
                    ['eliminado', '=', 0],
                    ['id', '=', $idhcl_cabecera_registro]
                ])->first();

            $certificado_medico = $Hcl_cabecera_registro->descripcion_certificado_med != ''? 1 : 0;

            $baja_medica = Hcl_registro_bajas_medicas::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $idhcl_cabecera_registro]
                            ])->count();

            $referencia_medica = Hcl_registro_referencias_medicas::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $idhcl_cabecera_registro]
                            ])->count();

            $internacion_medica = Hcl_internacion::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $idhcl_cabecera_registro]
                            ])->first();

            //print_r($internacion_medica);
            
            $parametros = array(
                'receta' => $receta,
                'laboratorio' => $laboratorio,
                'imagenologia' => $imagenologia,
                'certificado_medico' => $certificado_medico,
                'baja_medica' => $baja_medica,
                'referencia_medica' => $referencia_medica,
                'internacion_medica' => $internacion_medica
            );
            echo json_encode($parametros);
        }
    }
    public function descargaDicom()
    {
        if(isset($_POST['idestudio']))
        {
            $idestudio = $_POST['idestudio'];
            $resultadoIma_pacs = Ima_pacs::buscarIdPacs($idestudio);
            echo $resultadoIma_pacs->idestudio;
        }
    }
    // ------------------------------------------------------------------------------------------------------
    // ------------------------------------------------------------------------------------------------------

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------- [REPORTES] -----------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprime_Salud($id)
    {
        $arrayDatos = Home::parametrosSistema();


        $modelAlmacen = Almacen::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $arrayDatos['idalmacen'] ]
                            ])->first();
        
        $imprimirDirecto = 1;
        $diagnostico = '';
        $band = false;
        $model = DB::table('hcl_cabecera_registro as cr')                    
                    ->select(
                        'cr.id','cr.respuesta', 'cr.tipo_consulta', 'cr.numero', 'cr.hcl_poblacion_nombrecompleto as paciente', 'cr.hcl_poblacion_matricula', 'cr.hcl_poblacion_cod', 'cr.hcl_poblacion_sexo', 
                        DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"),
                        'cr.hcl_empleador_empresa', 'cr.hcl_empleador_patronal', 'cr.idhcl_poblacion','cr.iddatosgenericos','cr.idhcl_cuaderno','cr.fecha_registro','cr.hora_registro'
                    )
                    ->where([
                        ['cr.eliminado', '=', 0],
                        ['cr.id', '=', $id]
                    ])
                    ->first();

        $modelImagenologia = DB::table('ima_registroprogramacion as ir')
                    ->join('ima_examenes_registroprogramacion as ier', 'ier.idima_registroprogramacion', '=', 'ir.id')                
                    ->join('ima_examenes as ie', 'ier.idima_examenes', '=', 'ie.id')
                    ->select('ir.id as id', 'ie.codigo as codigo', 'ie.nombre as nombre')
                    ->where([
                        ['ir.eliminado', '=', 0],                        
                        ['ir.idhcl_cabecera_registro', '=', $model->id]
                    ])->get();


        // print_r($model->id);
        // return;

        $modelMedico = Datosgenericos::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->iddatosgenericos]
                            ])->first();

        $modelCuaderno = Hcl_cuaderno::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->idhcl_cuaderno]
                            ])->first();

        $modelDiagnosticos = Hcl_diagnosticosCIE::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get(); 

        $modelSignos_vitales = Hcl_datos_signos_vitales::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get(); 

        $numeroImprimir = $model->numero;
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);


        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$model,$modelAlmacen) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
             
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');
            $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $x = $pdf->getPageWidth();
            $y = $pdf->getPageHeight();
            $pdf->SetFont('helvetica', '', 9); 
            $pdf->MultiCell(160, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->MultiCell(50, 5, 'Form. DM-146' , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(160, 5, $modelAlmacen->nombre , 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 5, 'N°: '.$numeroImprimir , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 10, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->SetFont('courier', 'B', 20); 
            $pdf->MultiCell(150, 10, 'EVOLUCION Y TRATAMIENTO', 0, 'L', 0, 1, '', '', true, 1, false, true, 10, 10);

            // $pdf->SetFont('helvetica', '', 9); 
            // $pdf->MultiCell(15, 10, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);            
            // $pdf->MultiCell(15, 10, $modelAlmacen->nombre , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);

        //     $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     );

        // $pdf->write2DBarcode($model->numero.' - '.$model->hcl_poblacion_matricula.' - '.$model->paciente, 'QRCODE,L', 15, 0, 25, 25, $style, 'N');
        // $pdf->Text(14, 25, 'CNS');

            $pdf->Line(10, 25, $pdf->getPageWidth()-12,25);
        });
// $pdf::MultiCell(50, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 5, 10);
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha,$model,$modelMedico,$modelCuaderno) {
            $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            ); 
            
            $pdf->write2DBarcode($model->id.' - '.$model->hcl_poblacion_matricula.'-'.$model->hcl_poblacion_cod.' - '.$model->paciente.' - '.$model->hcl_empleador_patronal.' - '.$model->hcl_empleador_empresa, 'QRCODE,L', 15, 250, 35, 35, $style, 'N');
           

            $pdf->SetY(-20);
             $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(70, 10,'', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 10, 'Firma y sello Enfermera', 'T', 'C', 0, 0);
            // $pdf->MultiCell(95, 10, $modelCuaderno->nombre.'-'. $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
           
            // $pdf->MultiCell(10, 10,  $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 'T', 'C', 0, 1);
            $pdf->MultiCell(10, '', '', 0, 'C', 0, 0);            
            $pdf->MultiCell(50, 10, 'Firma y sello Médico', 'T', 'C', 0, 1);

            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-10);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-10);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        // esto hace que baje la hoja
        $y_Inicio = 27;
        // hasta aqui

        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);

        // ------------------------------------------ [PACIENTE] ------------------------------------------

        // ===================================                ====================================   
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                        DB::raw("date_part('year',age(fecha_nacimiento))  as edad"))
                                ->first();
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $puntos = 3;

        $pdf::MultiCell($widthLabel, $height, 'MEDICO:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelCuaderno->nombre.', '. $modelMedico->matricula.'-'.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'FECHA/HORA:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->fecha_registro.' '.$model->hora_registro, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelhcl_poblacion->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'SEXO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->hcl_poblacion_sexo, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'PACIENTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(140, $height, $model->paciente, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        

        $pdf::MultiCell($widthLabel, $height, 'EDAD', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $model->edad.' años', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $tipo_atencion = $model->tipo_consulta == 1? 'NUEVA' : 'REPETIDA';
        $pdf::MultiCell($widthLabel, $height, 'TIPO ATENCIÓN', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $tipo_atencion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);


        $pdf::MultiCell($widthLabel, $height, 'DIAGNÓSTICO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        foreach ($modelDiagnosticos as $dato) 
        {
            if ($band) {
                $pdf::MultiCell($widthLabel, $height, ' ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($puntos, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
            }else
                $band =true;
            
            $diagnostico = '('.$dato->codigo.') '.$dato->diagnostico;
            $pdf::MultiCell(150, $height, $diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    
        }    
        
        

        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        $pdf::SetFont('helvetica', 'B', 9); 
        // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(190, $height, 'SIGNOS VITALES ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9); 
        
        $bandera_signos_vitales = true; 
        foreach ($modelSignos_vitales as $dato) 
        {
            if ($bandera_signos_vitales) { 
                $pdf::MultiCell($widthLabel, $height, $dato->variable, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);   
                $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
                $pdf::MultiCell(50, $height, $dato->contenido_variable, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $bandera_signos_vitales =false; 
            }else{    
                $pdf::MultiCell($widthLabel, $height, $dato->variable, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);   
                $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
                $pdf::MultiCell(150, $height, $dato->contenido_variable, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                $bandera_signos_vitales =true; 
            }            
        }
        if ($bandera_signos_vitales == false) {
            $pdf::MultiCell(150, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        }
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        

        // HASTA AQUI TERMINA LOS DATOS DEL ASEGURADO
        // ===================================                ====================================   


        
        $pdf::MultiCell(120, $height, 'DESCRIPCIÓN:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(10, $height, 'I.', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // foreach ($modelDiagnosticos as $dato) 
        // {
        //     if ($band) {
        //         $pdf::MultiCell(10, $height, ' ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell(10, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
        //     }else
        //         $band =true;
            
        //     $diagnostico = '('.$dato->codigo.') '.$dato->diagnostico;
        //     $pdf::MultiCell(150, $height, $diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    
        // }  
        $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        // $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, false, true, 80, 20);
        

        // $modelImagenologia
        $examenes_imagenologia = '';
        $examen = '';
        if (count($modelImagenologia)>0) {
            $pdf::SetY(240);
            $pdf::MultiCell($widthLabel, $height, 'IMAGENOLOGIA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);   
                $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
            foreach ($modelImagenologia as $dato) 
            { 
                $examen = '('.$dato->codigo.') '.$dato->nombre;
                if ($examenes_imagenologia != '') {
                    $examenes_imagenologia = $examenes_imagenologia.' - '.$examen;    
                }else
                    $examenes_imagenologia = $examen; 
                

            }   
            // print_r($examenes_imagenologia);
            // return;
            $pdf::MultiCell(150, $height, $examenes_imagenologia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10); 
        }
         
        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("Evolucion.pdf", "I");

        mb_internal_encoding('utf-8');
    }

    public function imprime_ReferenciaMedica($id)
    {
        $arrayDatos = Home::parametrosSistema();


        $modelAlmacen = Almacen::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $arrayDatos['idalmacen'] ]
                            ])->first();
        
        $imprimirDirecto = 1;
        $diagnostico = '';
        $band = false;
        $model = DB::table('hcl_cabecera_registro as cr')                    
                    ->select(
                        'cr.id','cr.respuesta', 'cr.tipo_consulta', 'cr.numero', 'cr.hcl_poblacion_nombrecompleto as paciente', 'cr.hcl_poblacion_matricula', 'cr.hcl_poblacion_cod', 'cr.hcl_poblacion_sexo', 
                        DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"),
                        'cr.hcl_empleador_empresa', 'cr.hcl_empleador_patronal', 'cr.idhcl_poblacion','cr.iddatosgenericos','cr.idhcl_cuaderno','cr.fecha_registro','cr.hora_registro'
                    )
                    ->where([
                        ['cr.eliminado', '=', 0],
                        ['cr.id', '=', $id]
                    ])
                    ->first();

        $modelImagenologia = DB::table('ima_registroprogramacion as ir')
                    ->join('ima_examenes_registroprogramacion as ier', 'ier.idima_registroprogramacion', '=', 'ir.id')                
                    ->join('ima_examenes as ie', 'ier.idima_examenes', '=', 'ie.id')
                    ->select('ir.id as id', 'ie.codigo as codigo', 'ie.nombre as nombre')
                    ->where([
                        ['ir.eliminado', '=', 0],                        
                        ['ir.idhcl_cabecera_registro', '=', $model->id]
                    ])->get();


        // print_r($model->id);
        // return;

        $modelReferencias = Hcl_registro_referencias_medicas::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $id]
                            ])->first();

        $modelMedico = Datosgenericos::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->iddatosgenericos]
                            ])->first();

        $modelCuaderno = Hcl_cuaderno::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->idhcl_cuaderno]
                            ])->first();

        // $modelDiagnosticos = Hcl_diagnosticosCIE::where([
        //                         ['eliminado', '=', 0],
        //                         ['idhcl_cabecera_registro', '=', $model->id]
        //                     ])->get(); 

        $modelSignos_vitales = Hcl_datos_signos_vitales::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get(); 

        $numeroImprimir = $model->numero;
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);


        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$model,$modelAlmacen) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
             
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');
            $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $x = $pdf->getPageWidth();
            $y = $pdf->getPageHeight();
            $pdf->SetFont('helvetica', '', 9); 
            $pdf->MultiCell(160, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->MultiCell(50, 5, 'Form. DM-131' , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(160, 5, $modelAlmacen->nombre , 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 5, 'N°: '.$numeroImprimir , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 10, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->SetFont('courier', 'B', 20); 
            $pdf->MultiCell(75, 10, 'REFERENCIA', 0, 'C', 0, 1, '', '', true, 1, false, true, 10, 10);

            // $pdf->SetFont('helvetica', '', 9); 
            // $pdf->MultiCell(15, 10, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);            
            // $pdf->MultiCell(15, 10, $modelAlmacen->nombre , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);

        //     $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     );

        // $pdf->write2DBarcode($model->numero.' - '.$model->hcl_poblacion_matricula.' - '.$model->paciente, 'QRCODE,L', 15, 0, 25, 25, $style, 'N');
        // $pdf->Text(14, 25, 'CNS');

            $pdf->Line(10, 25, $pdf->getPageWidth()-12,25);
        });
// $pdf::MultiCell(50, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 5, 10);
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha,$model,$modelMedico,$modelCuaderno) {
            $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            ); 
            
            $pdf->write2DBarcode($model->id.' - '.$model->hcl_poblacion_matricula.'-'.$model->hcl_poblacion_cod.' - '.$model->paciente.' - '.$model->hcl_empleador_patronal.' - '.$model->hcl_empleador_empresa, 'QRCODE,L', 15, 250, 35, 35, $style, 'N');
           

            $pdf->SetY(-20);
             $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(70, 10,'', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 10, 'Firma Asegurado', 'T', 'C', 0, 0);
            // $pdf->MultiCell(95, 10, $modelCuaderno->nombre.'-'. $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
           
            // $pdf->MultiCell(10, 10,  $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 'T', 'C', 0, 1);
            $pdf->MultiCell(10, '', '', 0, 'C', 0, 0);            
            $pdf->MultiCell(50, 10, 'Firma y sello Médico', 'T', 'C', 0, 1);

            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-10);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-10);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        // esto hace que baje la hoja
        $y_Inicio = 27;
        // hasta aqui

        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);

        // ------------------------------------------ [PACIENTE] ------------------------------------------

        // ===================================                ====================================   
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                        DB::raw("date_part('year',age(fecha_nacimiento))  as edad"))
                                ->first();
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $puntos = 3;

        $pdf::MultiCell($widthLabel, $height, 'MEDICO SOLICITANTE:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelCuaderno->nombre.', '. $modelMedico->matricula.'-'.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'FECHA/HORA:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->fecha_registro.' '.$model->hora_registro, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelhcl_poblacion->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'SEXO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->hcl_poblacion_sexo, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'PACIENTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(140, $height, $model->paciente, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        

        $pdf::MultiCell($widthLabel, $height, 'EDAD', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $model->edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        // $tipo_atencion = $model->tipo_consulta == 1? 'NUEVA' : 'REPETIDA';
        // $pdf::MultiCell($widthLabel, $height, 'TIPO ATENCIÓN', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(50, $height, $tipo_atencion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        // $pdf::MultiCell($widthLabel, $height, 'CENTRO MEDICO QUE REFIERE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(150, $height, $modelReferencias->almacen_nombreref, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(60, $height, 'CENTRO MEDICO RECEPTOR:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelReferencias->almacen_nombrerec, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, 'ESPECIALIDAD RECEPTOR:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelReferencias->hcl_cuaderno_nombrerec, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);

        $pdf::SetFont('helvetica', 'B', 9); 
        $pdf::MultiCell(40, $height, 'SIGNOS VITALES', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelReferencias->signos_vitales, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(60, $height, 'RESUMEN DE EXAMEN CLINICO', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelReferencias->examen_clinico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(90, $height, 'RESULTADOS, EXAMENES COMPLEMENTARIOS', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell('', '', $modelReferencias->examenes_complementarios, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 
        $pdf::SetFont('helvetica', '', 9);

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(50, $height, 'DIAGNOSTICOS PRESUNTIVOS', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelReferencias->diagnosticos_presuntivos, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(40, $height, 'TRATAMIENTO INICIAL', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelReferencias->tratamiento_inicial, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);     

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(40, $height, 'RECOMENDACIONES', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelReferencias->recomendaciones, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);   
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);

     

        // $pdf::MultiCell($widthLabel, $height, 'DIAGNÓSTICO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        // foreach ($modelDiagnosticos as $dato) 
        // {
        //     if ($band) {
        //         $pdf::MultiCell($widthLabel, $height, ' ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell($puntos, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
        //     }else
        //         $band =true;
            
        //     $diagnostico = '('.$dato->codigo.') '.$dato->diagnostico;
        //     $pdf::MultiCell(150, $height, $diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    
        // }    
        
        

        // $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        // $pdf::SetFont('helvetica', 'B', 9); 
        // // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(00, $height, 'SIGNOS VITALES: ', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::SetFont('helvetica', '', 9); 
        
        // $bandera_signos_vitales = true; 
        // foreach ($modelSignos_vitales as $dato) 
        // {
        //     if ($bandera_signos_vitales) { 
        //         $pdf::MultiCell($widthLabel, $height, $dato->variable, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);   
        //         $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
        //         $pdf::MultiCell(50, $height, $dato->contenido_variable, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $bandera_signos_vitales =false; 
        //     }else{    
        //         $pdf::MultiCell($widthLabel, $height, $dato->variable, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);   
        //         $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
        //         $pdf::MultiCell(150, $height, $dato->contenido_variable, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        //         $bandera_signos_vitales =true; 
        //     }            
        // }
        // if ($bandera_signos_vitales == false) {
        //     $pdf::MultiCell(150, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // }
        // $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        

        // HASTA AQUI TERMINA LOS DATOS DEL ASEGURADO
        // ===================================                ====================================   


        
        // $pdf::MultiCell(120, $height, 'DESCRIPCIÓN:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(10, $height, 'I.', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // foreach ($modelDiagnosticos as $dato) 
        // {
        //     if ($band) {
        //         $pdf::MultiCell(10, $height, ' ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell(10, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
        //     }else
        //         $band =true;
            
        //     $diagnostico = '('.$dato->codigo.') '.$dato->diagnostico;
        //     $pdf::MultiCell(150, $height, $diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    
        // }  
        // $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        // $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, false, true, 80, 20);
        

        // $modelImagenologia
        // $examenes_imagenologia = '';
        // $examen = '';
        // if (count($modelImagenologia)>0) {
        //     $pdf::SetY(240);
        //     $pdf::MultiCell($widthLabel, $height, 'IMAGENOLOGIA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);   
        //         $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);    
        //     foreach ($modelImagenologia as $dato) 
        //     { 
        //         $examen = '('.$dato->codigo.') '.$dato->nombre;
        //         if ($examenes_imagenologia != '') {
        //             $examenes_imagenologia = $examenes_imagenologia.' - '.$examen;    
        //         }else
        //             $examenes_imagenologia = $examen; 
                

        //     }   
        //     // print_r($examenes_imagenologia);
        //     // return;
        //     $pdf::MultiCell(150, $height, $examenes_imagenologia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10); 
        // }

        $pdf::SetY(230);
        $pdf::SetFont('helvetica', 'B', 9); 
        // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(190, $height, 'CONCENTIMIENTO INFORMADO ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        // $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        $pdf::MultiCell('', '', 'Yo '.$model->paciente.' mayor de edad, habiéndoseme informado sobre el cuadro clínico, autorizo la referencia, teniendo en cuenta que he sido informado claramente sobre los riesgos y beneficios que se pueden presentar.', 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 10);     
        
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
         
        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("Evolucion.pdf", "I");

        mb_internal_encoding('utf-8');
    }

    public function imprime_Receta($id)
    {
        $Far_cabecera_receta = Far_cabecera_receta::where([
                ['eliminado', '=', 0],
                ['idhcl_cabecera_registro', '=', $id]
            ])->first(); 
        if($Far_cabecera_receta != null)
        {
            Far_cabecera_receta::imprimeProgramacion($Far_cabecera_receta);
        }
    }
    public function imprime_Laboratorio($id)
    {
        $Lab_registroprogramacion = Lab_registroprogramacion::where([
                ['eliminado', '=', 0],
                ['idhcl_cabecera_registro', '=', $id]
            ])->first();
        if($Lab_registroprogramacion != null)
        {
            Lab_registroprogramacion::imprimeProgramacion($Lab_registroprogramacion->id);
        }
    }
    public function imprime_Imagenologia($id)
    {
        $Ima_registroprogramacion = Ima_registroprogramacion::where([
                ['eliminado', '=', 0],
                ['idhcl_cabecera_registro', '=', $id]
            ])->first(); 

        if($Ima_registroprogramacion != null)
        {
            Ima_registroprogramacion::imprimeProgramacion($Ima_registroprogramacion->id);
        }
    }
    public function imprime_CertificadoMedico($id)
    {
        $imprimirDirecto = 1;
        
        $model = DB::table('hcl_cabecera_registro as cr')
                    ->select(
                        'cr.numero', 'cr.idhcl_poblacion',
                        'cr.descripcion_certificado_med', 'cr.iddatosgenericos'
                        , 'cr.hcl_poblacion_matricula'
                    )
                    ->where([
                        ['cr.eliminado', '=', 0],
                        ['cr.id', '=', $id]
                    ])
                    ->get(); 
        $model = $model[0];
        $modelDatosgenericos = Datosgenericos::where('id', '=', $model->iddatosgenericos)->first();
        
        $numeroImprimir = $model->numero;
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$model,$modelDatosgenericos) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 70, '', '', 50);
            $pdf->SetFont('courier', 'B', 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY() + 50);

                $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            );

            // $pdf->write2DBarcode($model->hcl_poblacion_matricula.'-'.$modelDatosgenericos->matricula.'-'.$modelDatosgenericos->apellidos.' '.$modelDatosgenericos->nombres, 'QRCODE,L', 10, 30, 20, 20, $style, 'N');
            $pdf->write2DBarcode($model->hcl_poblacion_matricula.'-'.$modelDatosgenericos->matricula.'-'.$modelDatosgenericos->apellidos.' '.$modelDatosgenericos->nombres, 'QRCODE,L', 20, 0, 20, 25, $style, 'N');
            $pdf->Text(14, 25, 'CNS');

            // $pdf->MultiCell(130, 10, 'CERTIFICADO MÉDICO', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
        });

        // $texto = '* El presente certificado médico se constituye como único documento válido a nivel nacional, para acreditar el estado de salud de la persona, el cual debe estar impreso y contener la firma y sello del médico que lo suscribe.';
        // $pdf::setFooterCallback(function($pdf) use($usuario, $fecha, $texto) {
        //     $pdf->SetY(-30);
        //     $pdf->SetFont('helvetica', '', 9);
        //     $pdf->MultiCell(145, '', '', 0, 'C', 0, 0);
        //     $pdf->MultiCell(50, 10, 'Firma y sello del médico', 'T', 'C', 0, 1);
        //     $pdf->MultiCell('', '', $texto, 'T', 'L', 0);
        // });

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 60;
        $height = 7;
        $width = 190;
        $widthLabel = 21;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);

        // $background_text = str_repeat('TCPDF test PNG Alpha Channel ', 70);
        // $pdf::MultiCell(0, 5, $background_text, 0, 'J', 0, 2, '', '', true, 0, false);
        // $pdf::Image('img/borrador.jpg', 50, 50, 100, '', '', 'http://www.tcpdf.org', '', false, 300);
        // $mask = $pdf::Image('img/photo1.png', 20, '', '', 50);
        //$pdf->Image('img/photo1.png', 50, 140, 100, '', '', '', '', false, 300, '', true);
        // embed image, masked with previously embedded mask
        $pdf::Image('img/certificado5.jpg', 5, 5, 220, 300, '', '', '', false, 300, '', false);
        //$pdf::Image('img/pacs1.jpg', 10, '', '', 90);





        // ------------------------------------------ [PACIENTE] ------------------------------------------
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                        DB::raw("date_part('year',age(fecha_nacimiento)) as edad"))
                                ->first();
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $label_1 = 'Lugar y Fecha';
        $label_2 = 'Nombres y Apellidos (del Médico)';
        $label_3 = 'Matricula Profesional Ministerio de Salud';

        $y_label_1 = ceil($pdf::GetStringWidth($label_1, $tipoLetra, '', $tamanio, false));
        $y_label_2 = ceil($pdf::GetStringWidth($label_2, $tipoLetra, '', $tamanio, false));
        $y_label_3 = ceil($pdf::GetStringWidth($label_3, $tipoLetra, '', $tamanio, false)) + 2;
        $y_label_maximo = max($y_label_1, $y_label_2, $y_label_3);
        $puntos = 3;
        
        
        $pdf::MultiCell(120, 1, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 1, 10);
        $pdf::MultiCell(95, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(120, $height, 'Chuquisaca, '.$fecha, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $pdf::MultiCell(120, 8, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 8, 10);
        $medico_atencion = $modelDatosgenericos->apellidos.' '.$modelDatosgenericos->nombres;
        $pdf::MultiCell(95, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(120, $height, $medico_atencion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $matricula = $modelDatosgenericos->matricula != ''? $modelDatosgenericos->matricula : '';
        $pdf::MultiCell(120, 9, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 9, 10);
        $pdf::MultiCell(95, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $matricula, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        // ------------------------------------------ [RESULTADO] ------------------------------------------
        $pdf::MultiCell(120, 18, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 18, 10);
        $pdf::MultiCell(2, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(180, '', $model->descripcion_certificado_med, 1, 'L', 0, 1, '', '', true, 0, false, true, 80, 20);
        
        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');
        
        $pdf::Output("certificadoMedico.pdf", "I");
        
        mb_internal_encoding('utf-8');
    }
    public function imprime_BajaMedica($id)
    {
        $imprimirDirecto = 1;
        $model = DB::table('hcl_registro_bajas_medicas as rbm')
                    ->join('hcl_variables_bajas as vb', 'rbm.idhcl_variables_bajas', '=', 'vb.id')
                    ->join('hcl_cabecera_registro as cr', 'rbm.idhcl_cabecera_registro', '=', 'cr.id')
                    ->select('cr.id','rbm.fecha_inicio as fecha_inicio','rbm.fecha_fin as fecha_fin',
                        // DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"),
                        // DB::raw("DATE_FORMAT(rbm.fecha_inicio, '%d-%m-%Y') as fecha_inicio"),
                        // DB::raw("DATE_FORMAT(rbm.fecha_fin, '%d-%m-%Y') as fecha_fin"),
                        'rbm.cantidad_dias as cantidad_dias', 'rbm.descripcion','cr.iddatosgenericos',
                        DB::raw("f_convnl(rbm.cantidad_dias) as cantidad"),
                        'vb.nombre as variablebaja_medica','cr.idalmacen',
                        'cr.numero', 'cr.hcl_poblacion_nombrecompleto as paciente', 
                        'cr.hcl_poblacion_matricula', 'cr.hcl_poblacion_cod', 'cr.hcl_poblacion_sexo', 
                        DB::raw("date_part('year',age(cr.hcl_poblacion_fechanacimiento)) as edad"),
                        // DB::raw("COALESCE(TIMESTAMPDIFF(YEAR, cr.hcl_poblacion_fechanacimiento, CURDATE()), 0) as edad"),
                        'cr.hcl_empleador_empresa', 'cr.hcl_empleador_patronal', 'cr.idhcl_poblacion'
                    )
                    ->where([
                        ['cr.eliminado', '=', 0],
                        ['cr.id', '=', $id]
                    ])
                    ->first();
        if($model == null)
        {
            echo "NO existe Baja Médica";
            return;
        }

        // ------------------------------------------ [PACIENTE] ------------------------------------------
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                    DB::raw("date_part('year',age(fecha_nacimiento)) as edad"))
                                        // DB::raw("COALESCE(TIMESTAMPDIFF(YEAR, fecha_nacimiento, CURDATE()), 0) as edad"))
                                ->first();

         // ------------------------------------------ [MEDICO] ------------------------------------------
        $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);

         // ------------------------------------------ [UNIDAD MEDICA] ------------------------------------------
        $modelUnidad = Almacen::find($model->idalmacen);
        
        $numeroImprimir = $model->numero;
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$model,$modelDatosgenericos) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            $pdf->SetFont('courier', 'B', 15);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $x = $pdf->getPageWidth();
            $y = $pdf->getPageHeight();
            $pdf->MultiCell(130, 5, 'CAJA NACIONAL DE SALUD', 0, 'R', '', 0, '', '', 1, '', '', '', 7, 'M');
            $pdf->SetFont('courier', '', 12);
            $pdf->MultiCell(55, 6, 'Form. AVC-09', 0, 'R', '', 1, '', '', 1, '', '', '', 7, 'M');
            $pdf->SetFont('courier', 'B', 10);
            $pdf->MultiCell(190, 4, 'DEPARTAMENTO DE AFILIACION', 0, 'C', '', 1, '', '', 1, '', '', '', 5, 'M');
            $pdf->SetFont('courier', '', 12);
            $pdf->MultiCell(190, 4, 'CERTIFICADO DE INCAPACIDAD TEMPORAL', 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');
            $pdf->Line(10, $pdf->getY(), $pdf->getPageWidth()-10, $pdf->getY()); 
        });

        $pdf::setFooterCallback(function($pdf) use($usuario, $model, $fecha) {
             $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            ); 
            
            $pdf->write2DBarcode($model->id.' - '.$model->hcl_poblacion_matricula.'-'.$model->paciente.' - '.$model->hcl_empleador_patronal.' - '.$model->hcl_empleador_empresa, 'QRCODE,L', 10, 87, 35, 35, $style, 'N');

            // $pdf->write2DBarcode($model->id.' - '.$model->hcl_poblacion_matricula.'-'.$model->hcl_poblacion_cod.' - '.$model->paciente.' - '.$model->hcl_empleador_patronal.' - '.$model->hcl_empleador_empresa, 'QRCODE,L', 15, 250, 35, 35, $style, 'N');
           
            $pdf->SetY(110);
            $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(145, '', '', 0, 'C', 0, 0);
            $pdf->MultiCell(50, 10, '', 0, 'C', 0, 1);

            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(120);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(120);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 24;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio); 

        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $puntos = 3;


        $pdf::MultiCell(45, $height, '(1) AP. PATERNO', 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, '(2) AP. MATERNO', 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, '(3) NOMBRES', 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(40, $height, '(4) N° ASEGURADO', 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(45, $height, $modelhcl_poblacion->primer_apellido, 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, $modelhcl_poblacion->segundo_apellido, 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, $modelhcl_poblacion->nombre, 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(40, $height, $modelhcl_poblacion->numero_historia, 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(150, $height, '(5) NOMBRE O RAZON SOCIAL DEL EMPLEADOR', 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(40, $height, '(6) N° EMPLEADOR', 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(150, $height, $model->hcl_empleador_empresa, 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(40, $height, $model->hcl_empleador_patronal, 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(45, $height, '(7) RIESGO PROFESIONAL', 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, 'LUGAR Y FECHA', 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(85, $height, '(8) ', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(45, $height, $model->variablebaja_medica, 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, 'SUCRE, '.$fecha, 1, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(85, $height, 'Salario Bs........................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 

        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(45, $height, 'INCAPACIDAD', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(60, $height, 'DESDE: '.$model->fecha_inicio, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::Line(115, $pdf::getY(), 115, 89);
        $pdf::MultiCell(85, $height, 'Importe Subsidios Bs.....................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(45, $height, '', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(60, $height, 'HASTA: '.$model->fecha_fin, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(85, $height, 'Son...................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        // print_r($model->cantidad);
        // return;
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(105, $height, 'DIAS DE INCAPACIDAD: '.strtoupper($model->cantidad).' DIAS.', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(85, $height, '.................................................................. BOLIVIANOS', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-10, $pdf::getY());
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(45, $height, '', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10); 
        $pdf::MultiCell(60, $height, '', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(85, $height, 'CERTIFICO', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, '', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, '', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(85, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-95, $pdf::getY());
        $pdf::MultiCell(45, $height, 'FIRMA MEDICO', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(60, $height, 'SELLO MEDICO', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(85, $height, '', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont($tipoLetra, '', 7);
        $pdf::MultiCell(70, $height, 'UNIDAD MED.: '.$modelUnidad->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(35, $height, 'MATRICULA: '.$modelDatosgenericos->matricula, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-10, $pdf::getY());
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(85, $height, 'Nombre y Firma CNS', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-10, $pdf::getY());
        $pdf::MultiCell(190, 4, '', 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');
        $pdf::Line(50, $pdf::getY(), $pdf::getPageWidth()-50, $pdf::getY());
        $pdf::MultiCell(190, 4, '(9) Lugar y Fecha', 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');
        $pdf::MultiCell(190, 4, '', 0, 'C', '', 0, '', '', 0, '', '', '', 7, 'M');
        $pdf::MultiCell(190, 4, '', 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');
        $pdf::MultiCell(190, 4, '', 0, 'C', '', 0, '', '', 0, '', '', '', 7, 'M');
        $pdf::MultiCell(190, 4, '', 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');
        $pdf::MultiCell(40, 4, '', 0, 'C', '', 0, '', '', 0, '', '', '', 7, 'M');
        $pdf::MultiCell(70, 4, '(10) Firma del asegurado', 'T', 'C', 0, 0, '', '', 0, '', '', '', 7, 'M');
        $pdf::MultiCell(70, 4, '(11) Sello y Firma de la Empresa', 'T', 'C', 0, 0, '', '', 1, '', '', '', 7, 'M');
 
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("BajaMedica.pdf", "I");

        mb_internal_encoding('utf-8');
    }

    public function imprime_InternacionMedica($id)
    {
        $arrayDatos = Home::parametrosSistema();


        $modelAlmacen = Almacen::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $arrayDatos['idalmacen'] ]
                            ])->first();
        
        $imprimirDirecto = 1;
        $diagnostico = '';
        $band = false;
        $model = DB::table('hcl_cabecera_registro as cr')                    
                    ->select(
                        'cr.id','cr.respuesta', 'cr.tipo_consulta', 'cr.numero', 'cr.hcl_poblacion_nombrecompleto as paciente', 'cr.hcl_poblacion_matricula', 'cr.hcl_poblacion_cod', 'cr.hcl_poblacion_sexo', 
                        DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"),
                        'cr.hcl_empleador_empresa', 'cr.hcl_empleador_patronal', 'cr.idhcl_poblacion','cr.iddatosgenericos','cr.idhcl_cuaderno','cr.fecha_registro','cr.hora_registro'
                    )
                    ->where([
                        ['cr.eliminado', '=', 0],
                        ['cr.id', '=', $id]
                    ])
                    ->first();

        $modelImagenologia = DB::table('ima_registroprogramacion as ir')
                    ->join('ima_examenes_registroprogramacion as ier', 'ier.idima_registroprogramacion', '=', 'ir.id')                
                    ->join('ima_examenes as ie', 'ier.idima_examenes', '=', 'ie.id')
                    ->select('ir.id as id', 'ie.codigo as codigo', 'ie.nombre as nombre')
                    ->where([
                        ['ir.eliminado', '=', 0],                        
                        ['ir.idhcl_cabecera_registro', '=', $model->id]
                    ])->get();


        // print_r($model->id);
        // return;

        $modelInternacion = Hcl_internacion::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $id]
                            ])->first();

        $modelUnidad = Almacen::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $modelInternacion->idalmacen]
                            ])->first();

        $modelServicio = Hcl_cuaderno::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $modelInternacion->idhcl_cuaderno]
                            ])->first();

        $modelMedico = Datosgenericos::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->iddatosgenericos]
                            ])->first();

        $modelCuaderno = Hcl_cuaderno::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->idhcl_cuaderno]
                            ])->first();

        // $modelDiagnosticos = Hcl_diagnosticosCIE::where([
        //                         ['eliminado', '=', 0],
        //                         ['idhcl_cabecera_registro', '=', $model->id]
        //                     ])->get(); 

        $modelSignos_vitales = Hcl_datos_signos_vitales::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get(); 

        $numeroImprimir = $model->numero;
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);


        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$model,$modelAlmacen) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
             
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');
            $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $x = $pdf->getPageWidth();
            $y = $pdf->getPageHeight();
            $pdf->SetFont('helvetica', '', 9); 
            $pdf->MultiCell(160, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->MultiCell(50, 5, 'Form. ' , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(160, 5, $modelAlmacen->nombre , 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 5, 'N°: '.$numeroImprimir , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            // $pdf->MultiCell(1, 10, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->SetFont('courier', 'B', 20); 
            $pdf->MultiCell(190, 10, 'HOJA DE ADMISIÓN HOSPITALARIA', 0, 'C', 0, 1, '', '', true, 1, false, true, 10, 10);

            // $pdf->SetFont('helvetica', '', 9); 
            // $pdf->MultiCell(15, 10, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);            
            // $pdf->MultiCell(15, 10, $modelAlmacen->nombre , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);

        //     $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     );

        // $pdf->write2DBarcode($model->numero.' - '.$model->hcl_poblacion_matricula.' - '.$model->paciente, 'QRCODE,L', 15, 0, 25, 25, $style, 'N');
        // $pdf->Text(14, 25, 'CNS');

            $pdf->Line(10, 25, $pdf->getPageWidth()-12,25);
        });
// $pdf::MultiCell(50, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 5, 10);
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha,$model,$modelMedico,$modelCuaderno) {
            $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            ); 
            
            $pdf->write2DBarcode($model->id.' - '.$model->hcl_poblacion_matricula.'-'.$model->hcl_poblacion_cod.' - '.$model->paciente.' - '.$model->hcl_empleador_patronal.' - '.$model->hcl_empleador_empresa, 'QRCODE,L', 15, 250, 35, 35, $style, 'N');
           

            $pdf->SetY(-20);
             $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(70, 10,'', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 10, 'Firma Asegurado', 'T', 'C', 0, 0);
            // $pdf->MultiCell(95, 10, $modelCuaderno->nombre.'-'. $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
           
            // $pdf->MultiCell(10, 10,  $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 'T', 'C', 0, 1);
            $pdf->MultiCell(10, '', '', 0, 'C', 0, 0);            
            $pdf->MultiCell(50, 10, 'Firma y sello Médico', 'T', 'C', 0, 1);

            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-10);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-10);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        // esto hace que baje la hoja
        $y_Inicio = 27;
        // hasta aqui

        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);

        // ------------------------------------------ [PACIENTE] ------------------------------------------

        // ===================================                ====================================   
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                        DB::raw("date_part('year',age(fecha_nacimiento))  as edad"))
                                ->first();
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $puntos = 3;

        $pdf::MultiCell(50, $height, 'UNIDAD SOLICITANTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(68, $height, $modelUnidad->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'SERVICIO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $modelServicio->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'MEDICO SOLICITANTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelCuaderno->nombre.', '. $modelMedico->matricula.'-'.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'FECHA/HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->fecha_registro.' '.$model->hora_registro, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelhcl_poblacion->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'SEXO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->hcl_poblacion_sexo, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'PACIENTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(140, $height, $model->paciente, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        

        $pdf::MultiCell($widthLabel, $height, 'EDAD', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $model->edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont('helvetica', 'B', 9); 
        // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(190, $height, 'DATOS DE REFERENCIA', 'T', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(30, $height, 'Persona Referente', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $modelInternacion->familiar_datos, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(30, $height, 'Movíl referente:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelInternacion->familiar_movil, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(30, $height, 'Dirección referente', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelInternacion->familiar_domicilio, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        if ($modelInternacion->consulta != '') {
            $pdf::MultiCell(30, $height, 'Breve descripción', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(90, $height, $modelInternacion->familiar_descripcion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        } 

        // $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        // $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);

        $pdf::SetFont('helvetica', 'B', 9); 
        // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(190, $height, 'DATOS DE CONSULTORIO DE ADMISIÓN', 'T', 'C', 0, 1, '', '', true, 0, false, true, $height, 10); 

        $pdf::SetFont('helvetica', 'B', 9); 
        $pdf::MultiCell(40, $height, 'SIGNOS VITALES', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelInternacion->signos_vitales, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);    

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(50, $height, 'DIAGNOSTICOS PRESUNTIVOS', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelInternacion->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(190, $height, 'RESUMEN DE EXAMEN CLINICO', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);

        // $pdf::SetFont('helvetica', 'B', 9);
        // $pdf::MultiCell(90, $height, 'RESULTADOS, EXAMENES COMPLEMENTARIOS', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell('', '', '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 
        // $pdf::SetFont('helvetica', '', 9);

        

        // $pdf::SetFont('helvetica', 'B', 9);
        // $pdf::MultiCell(40, $height, 'TRATAMIENTO INICIAL', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);        
        // $pdf::SetFont('helvetica', '', 9);
        // $pdf::MultiCell('', '', '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);     
        if ($modelInternacion->recomendaciones != '') {
            $pdf::SetFont('helvetica', 'B', 9);
            $pdf::MultiCell(40, $height, 'RECOMENDACIONES', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);        
            $pdf::SetFont('helvetica', '', 9);
            $pdf::MultiCell('', '', $modelInternacion->recomendaciones, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);   
            $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        }
        
 
        $pdf::SetY(230);
        $pdf::SetFont('helvetica', 'B', 9); 
        // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(190, $height, 'CONCENTIMIENTO INFORMADO ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        // $pdf::MultiCell('', '', $model->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        $pdf::MultiCell('', '', 'Yo '.$model->paciente.' mayor de edad, habiéndoseme informado sobre el cuadro clínico, autorizo la internación, teniendo en cuenta que he sido informado claramente sobre los riesgos y beneficios que se pueden presentar.', 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 10);     
        
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
         
        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("internacion.pdf", "I");

        mb_internal_encoding('utf-8');
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------


}