<?php
namespace App\Http\Controllers\Salud;
use App\Http\Controllers\Controller;
use Auth;
use App\Http\Requests\PdfRequest;
use Illuminate\Http\Request; 
use Illuminate\Support\Facades\DB;
use \App\Models\Almacen\Home;
use \App\Permission;
use Charts;
use App\Providers\AppServiceProvider;
use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_diagnosticosCIE;
use \App\Models\Afiliacion\Hcl_datos_signos_vitales;
use \App\Models\Afiliacion\Hcl_clasificaciontalla;
use \App\Models\Afiliacion\Hcl_clasificacionsalud;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion; 
use \App\Models\Administracion\Datosgenericos; 
use \App\Models\Imagenologia\Ima_especialidad;
use \App\Models\Medicina_trabajo\Med_tipo_cancelacion;
use \App\Models\Medicina_trabajo\Med_tipo_pago;
use \App\Models\Almacen\Almacen;
use Elibyy\TCPDF\Facades\TCPDF;
use App\Charts\SampleChart;
use Session;

class PdfController extends Controller
{
    private $rutaVista = 'Salud.reporte.';
    private $controlador = 'hcl_reportePDF';

    private $sinContenido = 'reporte.sinContenidoPrint';
    private $sinDatos = 'SIN DATOS';
    private $landscape = 'landscape';
    private $portrait = 'portrait';
    const listaAtencion = 1;
    const form202 = 2;
    const listaRegistro = 3;
    const listaFormulario = 4;

    // protected $url;
    // protected $key;
    // protected $session;

    // private $controlador = 'pdf';
    // ----------------------------------------------------------------------------
    // ----------------------------------------------------------------------------
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->url = config('http://elisa.web/hcl_reporte');
        // $this->key = config('MYKeYPHP');
        // $this->session = "mySession";
    }

    public function index(Request $request)
    {
        $arrayDatos = Home::parametrosSistema();

       // if(Permission::verificarAcceso($this->controlador.'.index') == 0)
       //      return view('errors.403')
       //                  ->with('arrayDatos', $arrayDatos)
       //                  ->with('url', '');

       
    //     session(['session' => $arrayDatos['iddatosgenericos']]);
    //     //Session::flush();
    //     if(!Session::get('_token') and !Session::get('session')):
    //         Wppconnect::make($this->url);
    //         $response = Wppconnect::to('/api/'.$this->session.'/'.$this->key.'/generate-_token')->asJson()->post();
    //         $response = json_decode($response->getBody()->getContents(),true);
    //         if($response['status'] == 'success'):
    //             Session::put('_token', $response['_token']);
    //             Session::put('session', $response['session']);
    //         endif;
    //     endif;

       
        

    // //     #Function: Start Session 
    // // # /api/:session/start-session
    //     if(Session::get('_token') and Session::get('session') and Session::get('init')):
    //        Wppconnect::make($this->url);
    //        $response = Wppconnect::to('/api/'. Session::get('session').'/check-connection-session')->withHeaders([
    //     'Authorization' => 'Bearer '.Session::get('_token')
    //        ])->asJson()->get();
    //        $response = json_decode($response->getBody()->getContents(),true);
    //        dd($response);
    //     endif;


    //     if(Session::get('_token') and Session::get('session') and Session::get('init')):
    //        Wppconnect::make($this->url);
    //        $response = Wppconnect::to('/api/'. Session::get('session').'/send-message')->withBody([
    //     'phone' => '+59176114426',
    //     'message' => 'orale, funciona mesmo!'
    //        ])->withHeaders([
    //     'Authorization' => 'Bearer '.Session::get('_token')
    //        ])->asJson()->post();
    //         print_r($response);
    //         $response = json_decode($response->getBody()->getContents(),true);
    //         dd($response);
    //     endif;

    //     print_r(Session::get('_token'));
    //     return;

        // $dato =  "";
        $model = new Home;
        $model->idalmacen = 1;
        $model->rutaview = $this->rutaVista;
        $model->accion = $this->controlador;
 
        $pFechaDesde = $request->dateDesde != ''? $request->dateDesde : date('d-m-Y');
        $fecha = date_create($pFechaDesde);
        $dateDesde = date_format($fecha, 'Y-m-d');

        $pFechaHasta = $request->dateHasta != ''? $request->dateHasta : date('d-m-Y');
        $fecha = date_create($pFechaHasta);
        $dateHasta = date_format($fecha, 'Y-m-d');
        

        // $modelEspecialidad = Ima_especialidad::where([
        //                     ['ima_especialidad.eliminado', '=', 0],
        //                     ['ima_especialidad.idalmacen', '=', $arrayDatos['idalmacen']]
        //                 ])->get();

        $modelEspecialidad = DB::table('hcl_cuaderno_personal as cp')
                        ->join('hcl_cuaderno as c', 'cp.idhcl_cuaderno', '=', 'c.id')
                        ->where([
                            ['cp.eliminado', '=', 0],
                            ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']]
                        ])
                        ->orderBy('c.nombre', 'ASC')
                        ->get();
         
        $data1 =  DB::table('hcl_cabecera_registro')
                                ->select('id',DB::raw("CASE tipo_consulta WHEN 1 THEN 'NUEVO' WHEN 2 THEN 'REPETIDO' END AS consulta"))
                                ->where([
                                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']],                                    
                                ])
                                ->whereBetween('fecha_registro', [$dateDesde, $dateHasta])
                                ->whereIn('tipo_consulta',array('1','2'))
                                ->orderBy('id', 'desc')->get();

        $data_prueba =  DB::table('hcl_cabecera_registro')
                                ->select(DB::raw("CASE tipo_consulta WHEN 1 THEN 'NUEVO' WHEN 2 THEN 'REPETIDO' END AS name"), DB::raw("count(tipo_consulta) as data"))
                                ->where([
                                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']],   
                                    ['tipo_consulta', '!=', 0],                                                                     
                                ])
                                ->whereBetween('fecha_registro', [$dateDesde, $dateHasta])
                                ->groupBy('tipo_consulta')->get(); 

        $puntos_data1 =  [];
        $promedioCantidad = 0;
        foreach ($data_prueba as $value) {
            $arr = array ("name" => $value->name, "data" => array_map('intval', explode(',', $value->data)));
            $puntos_data1[] = $arr;
            $promedioCantidad += $value->data; 
        }    
        $model->promedioCantidad = $promedioCantidad/2;
         
        $data2 =  DB::table('hcl_cabecera_registro')
                                ->select(DB::raw("count(hcl_empleador_empresa) as cantidad"),'hcl_empleador_empresa as empresas')
                                ->where([
                                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']],                                    
                                ])
                                ->whereBetween('fecha_registro', [$dateDesde, $dateHasta])
                                ->groupBy('hcl_empleador_empresa','empresas')
                                ->limit(100)
                                ->get();

        $puntos_data2 =  [];
        foreach ($data2 as $value) {
            $arr2 = array ("name" => $value->empresas, "data" => array_map('intval', explode(',', $value->cantidad)));
            $puntos_data2[] = $arr2;
        }   

        $data3 =  DB::table('hcl_cabecera_registro')
                                ->select(DB::raw("count(hcl_poblacion_fechanacimiento) as cantidad"),DB::raw("date_part('year',age(fecha_registro,hcl_poblacion_fechanacimiento)) as edad"))
                                ->where([
                                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']],                                    
                                ])
                                ->whereBetween('fecha_registro', [$dateDesde, $dateHasta])
                                ->groupBy('hcl_poblacion_fechanacimiento','edad')
                                ->orderBy('hcl_poblacion_fechanacimiento', 'asc')
                                ->get();


        $puntos_data3 =  [];
        foreach ($data3 as $value) {
            $arr3 = array ("name" => $value->edad, "data" => array_map('intval', explode(',', $value->cantidad)));
            $puntos_data3[] = $arr3;
        }

        $data4 =  DB::table('hcl_cabecera_registro')
                                ->select('hcl_poblacion_sexo AS genero',DB::raw("count (hcl_poblacion_sexo = 'MAS')"))
                                ->where([
                                    ['iddatosgenericos', '=', $arrayDatos['iddatosgenericos']],                                    
                                ])
                                ->whereIn('hcl_poblacion_sexo',array('MAS','FEM'))
                                ->whereBetween('fecha_registro', [$dateDesde, $dateHasta])
                                ->groupBy('genero')                                
                                ->get();
        // print_r($data4);
        // return;

        $puntos_data4 =  [];
        $promedio = 0;
        foreach ($data4 as $value) {
            $arr4 = array ("name" => $value->genero, "data" => array_map('intval', explode(',', $value->count)));
            $puntos_data4[] = $arr4;
            $promedio += $value->count;   
        }

        $model->promedioGenero = $promedio/2;



        $model->data = json_encode($puntos_data1); 
        $model->data2 = json_encode($puntos_data2);
        $model->data3 = json_encode($puntos_data3);
        $model->data4 = json_encode($puntos_data4);

        return view($model->rutaview.'index')
                ->with('model', $model) 
                ->with('modelEspecialidad', $modelEspecialidad)
                ->with('arrayDatos', $arrayDatos);
    }

    public function imprimeDocumento($valores)//Request $request)
    {
        $parametro = json_decode($valores); 
        // print_r($parametro);
        // return;  
        if ($parametro->opcion == PdfController::listaAtencion) 
            $this->imprimirConsultaExterna($parametro);
        if ($parametro->opcion == PdfController::form202) 
            $this->imprimirFormulario202($parametro);
        if ($parametro->opcion == PdfController::listaRegistro) 
            $this->imprimirRegistros_lista($parametro);
        if ($parametro->opcion == PdfController::listaFormulario) 
            $this->imprimirFormularios_lista($parametro); 
    }
    // 

    
   public function imprimirFormulario202($parametro){     

        $arrayDatos = Home::parametrosSistema(); 

        if ($parametro->fechaLista == '') {
            print_r('<h3>Se requiere llenar Especialidad y fecha de solicitud de reporte.</h3>');
            return;
        } 
        //---varables del reporte----------------    
        $titulo = 'INFORME DIARIO DE CONSULTA EXTERNA';
        $path_logo='imagenes/logo.jpg';
        $titulo_cns="CAJA NACIONAL DE SALUD";
        
        $titular=true;
        $suplente=false;

        // datos de control
        //PESO
        $modelHcl_clasificacionsalud = Hcl_clasificacionsalud::get(); 

        // UNIDAD MEDICA
        $modelAlmacen = Almacen::find($arrayDatos['idalmacen']);
        $regional= $modelAlmacen->descripcion;
        // DATOS MEDICO
        $modelDatosgenericos = Datosgenericos::find($arrayDatos['iddatosgenericos']);
 


        $pidhcl_cuaderno = $parametro->especialidadForm;      
        $modelHcl_cuaderno = Hcl_cuaderno::find($pidhcl_cuaderno);                                    // dato de especialidad tabla hcl_cuaderno
        $pfecha_registro = $parametro->fechaForm;                                  // fecha de atencion
        $piddatosgenericos = $arrayDatos['iddatosgenericos'];             // id medico            tabla datosgenericos

        $modeloHcl_cabecera_registro = DB::table(DB::raw("hcl_informe_formulario_202('".$pidhcl_cuaderno."','".$pfecha_registro."','".$piddatosgenericos."')"))->get();  
        
        ob_end_clean();
        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pageLayout = array(216,330); //  tamaño Oficio Bolivia

        $pdf::SetTitle("FORMULARIO 202");
        $pdf::SetSubject("TCPDF");
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');

        $pdf::setHeaderCallback(function($pdf) use($titulo,$path_logo,$titulo_cns,$regional,$titular,$suplente,$modelAlmacen,$modelHcl_cuaderno,$pfecha_registro,$modelDatosgenericos){

            //--cabecera PDF First Page -TITULO
        $pdf->SetFont('helvetica', '', 11);
        $pdf->SetXY(5,5);
        $pdf->Cell(320, 0, $titulo, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(310,5);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->Cell(15, 0, 'FORM-202', 0, 0, 'C', 0, '', 0);

        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(75);
        $pdf->SetXY(5,5);
        $pdf->Image($path_logo, '', '', 25, 25, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $pdf->SetXY(30,5);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->Cell(50, 0, $titulo_cns, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(30,13);
        $pdf->Cell(40, 0, $regional, 0, 0, 'C', 0, '', 0);
        $pdf->Cell(50, 0, $modelAlmacen->nombre, 0, 0, 'C', 0, '', 0);
        $pdf->Cell(15, 0, '', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(30, 0, $modelHcl_cuaderno->nombre, 0, 0, 'C', 0, '', 0);
        $pdf->Cell(45, 0, '', 0, 0, 'C', 0, '', 0);
        $pdf->Cell(25, 0, $pfecha_registro, 0, 0, 'C', 0, 20, 0);
        //---lineas
        $style = array('width' => 0.2, 'cap' => 'round', 'join' => 'rmiter', 'dash' => 0, 'color' => array(0, 0, 0));
        //lineas de Regional hasta firma
        $pdf->Line(30,17, 70, 17, $style);
        $pdf->Line(75,17, 125, 17, $style);
        $pdf->Line(130,17, 160, 17, $style);
        $pdf->Line(165,17, 205, 17, $style);
        $pdf->Line(210,17, 235, 17, $style);
        $pdf->Line(280,20, 320, 20, $style);

        //CABECERA DEL FORMULARIO
        //
        $pdf->SetXY(30,17);
        $pdf->Cell(40, 0, 'REGIONAL', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(75,17);
        $pdf->Cell(50, 0, 'ESTABLECIMIENTO DE SALUD', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(130,17);
        $pdf->Cell(30, 0, 'SERVICIO', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(165,17);
        $pdf->Cell(40, 0, 'N°CONSULTORIO', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(210,17);
        $pdf->Cell(25, 0, 'FECHA', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(240,17);
        $pdf->Cell(40, 0, 'FIRMA Y SELLO MEDICO', 0, 0, 'L', 0, '', 0);
        //
        $pdf->SetXY(30,23);
        $pdf->Cell(40, 0, 'MEDICO : '.$modelDatosgenericos->titulo.' '.$modelDatosgenericos->apellidos.' '.$modelDatosgenericos->nombres.'-'.$modelDatosgenericos->matricula, 0, 0, 'L', 0, '', 0);
        $pdf->Line(46,26, 115, 26, $style);
        $pdf->SetXY(120,23);
        $pdf->Cell(40, 0, 'TITULAR', 0, 0, 'L', 0, '', 0);
        if($titular){
            $pdf->SetXY(135,23);
            $pdf->Cell(5, 0, ' X', 1, 0, 'L', 0, '', 0);
            $pdf->SetXY(165,23);
            $pdf->Cell(5, 0, ' ', 1, 0, 'L', 0, '', 0);
        }else if($suplente){
            $pdf->SetXY(135,23);
            $pdf->Cell(5, 0, ' ', 1, 0, 'L', 0, '', 0);
            $pdf->SetXY(165,23);
            $pdf->Cell(5, 0, ' X', 1, 0, 'L', 0, '', 0);
        }
        $pdf->SetXY(145,23);
        $pdf->Cell(40, 0, 'SUPLENTE ', 0, 0, 'L', 0, '', 0);
        $pdf->SetXY(173,23);
        $pdf->Cell(40, 0, 'HORAS DE CONSULTA : ', 0, 0, 'L', 0, '', 0);
        $pdf->Line(211,26, 225, 26, $style);
        $pdf->SetXY(230,23);
        $pdf->Cell(40, 0, 'ENFERMERA : ', 0, 0, 'L', 0, '', 0);
        $pdf->Line(253,26, 320, 26, $style);
        //----CABECERA DE TABLA
        //
       //=============DATOS GENERALES
        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(5,30);
        $pdf->Cell(66, 0, 'DATOS GENERALES : ', 1, 0, 'C', 0, '', 0);
        //$pdf->Ln();

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(5,33.5);
        $pdf->Cell(5, 48, 'N°', 1, 0, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(10,33.5);
        $pdf->Cell(22, 48, 'MATRICULA', 1, 1, 'C', 0, '', 0);
        $pdf->SetXY(10,46);
        $pdf->Cell(22, 30, 'ASEGURADO', 0, 0, 'C', 0, '', 0);
        //$pdf->Ln();

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(32,33.5);
        $pdf->Cell(14, 29, '', 1, 1, 'C', 0, '', 0);
        $pdf->SetXY(32,40);
        $pdf->Cell(14, 10, 'EDAD', 0, 1, 'C', 0, '', 0);
        $pdf->SetXY(32,43);
        $pdf->Cell(14, 10, 'Y', 0, 1, 'C', 0, '', 0);
        $pdf->SetXY(32,46);
        $pdf->Cell(14, 10, 'SEXO', 0, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(32,62.5);
        $pdf->Cell(7, 19, 'M', 1, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(39,62.5);
        $pdf->Cell(7, 19, 'F', 1, 1, 'C', 0, '', 0);


        //=================================================
        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(46,33.5);
        $pdf->Cell(25, 16, '', 1, 1, 'C', 0, '', 0);
        $pdf->SetXY(46,31.5);
        $pdf->Cell(25, 16, 'TIPOS DE', 0, 1, 'C', 0, '', 0);
        $pdf->SetXY(46,37);
        $pdf->Cell(25, 10, 'ASEGURADO', 0, 1, 'C', 0, '', 0);

        //---------------------------------------------
        $pdf->SetXY(46,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(13,10,'ACTIVO', 1, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(46,62.5);
        $pdf->Cell(5, 19, 'A', 1, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(51,62.5);
        $pdf->Cell(5, 19, 'B', 1, 1, 'C', 0, '', 0);

        //-------------------------------------------------
        $pdf->SetXY(56,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,10,'RENTISTA', 1, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(56,62.5);
        $pdf->Cell(5, 19, 'A', 1, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(61,62.5);
        $pdf->Cell(5, 19, 'B', 1, 1, 'C', 0, '', 0);
        //
        //
        $pdf->SetXY(66,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(32,5,'OTROS', 1, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        //=====================CONSULTAS
        $pdf->SetXY(71,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(32.5,10,'CONSULTAS', 1, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(71,62.5);
        $pdf->Cell(5, 19, 'N', 1, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(76,62.5);
        $pdf->Cell(5, 19, 'R', 1, 1, 'C', 0, '', 0);

        //=============ATENCIÓN AL NIÑO(A) MENOR DE 5 AÑOS 
        $pdf->SetXY(81,30);
        $pdf->Cell(78.3, 0, 'ATENCIÓN AL NIÑO(A) MENOR DE 5 AÑOS : ', 1, 0, 'C', 0, '', 0);
        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->SetXY(81,33.5);
        $pdf->Cell(41.8, 10, 'VIGILANCIA NUTRICIONAL', 1, 1, 'C', 0, '', 0);

        $pdf->SetXY(81,43.5);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(31.3, 6, 'PESO - TALLA', 1, 1, 'C', 0, '', 0);

        $pdf->SetXY(96.9,49.5);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(15.4, 12.9, '', 1, 0, 'C', 0, '', 0);
        $pdf->SetXY(96.6,47);
        $pdf->Cell(15.5, 12.8, 'DESNU-', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(96.6,49.5);
        $pdf->Cell(15.5, 12.8, 'TRICION', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(96.6,51.5);
        $pdf->Cell(15.5, 12.8, 'AGUDA', 0, 0, 'C', 0, '', 0);


        $pdf->SetXY(81,81.5);
        //$pdf->SetXY(81,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, 'OBESIDAD', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(86.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, 'SOBREPESO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(91.6,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, 'NORMAL', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(96.9,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(19, 5.3, 'LEVE', 1, 0, 'L', 0, '', 0);
        $pdf->StopTransform();

        //$pdf->SetXY(76,62.5);
        $pdf->SetXY(102.2,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(19, 5, 'MODERADA', 1, 0, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(107.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(19, 5, 'GRAVE', 1, 0, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(112.3,43.5);
        $pdf->Cell(10.5, 6, '', 1, 1, 'C', 0, '', 0);
        $pdf->SetXY(112.3,42.5);
        $pdf->Cell(10.5, 6, 'TALLA', 0, 1, 'C', 0, '', 0);
        $pdf->SetXY(112.3,44.5);
        $pdf->Cell(10.5, 6, 'EDAD', 0, 1, 'C', 0, '', 0);

        $pdf->SetXY(112.3,81.5);
        //$pdf->SetXY(81,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, 'TALLA NORMAL', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(117.5,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, 'TALLA BAJA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        //------------------------------
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(122.8,33.5);
        $pdf->Cell(36.5, 10, '', 1, 1, 'C', 0, '', 0);
        $pdf->SetXY(122.8,31.5);
        $pdf->Cell(36.5, 10, 'SUPLEMENTACIÓN CON', 0, 1, 'C', 0, '', 0);
        $pdf->SetXY(122.8,34.5);
        $pdf->Cell(36.5, 10, 'MICRONUTRIENTES', 0, 1, 'C', 0, '', 0);

        $pdf->SetXY(122.8,43.5);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(21.2, 6, 'HIERRO', 1, 1, 'C', 0, '', 0);

        $pdf->SetXY(122.8,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,10.6,'', 1, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(120.8,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,10.8,'6 meses', 0, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(123.8,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,10.8,'< 2 años', 0, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(122.8,62.5);
        $pdf->Cell(5.3, 19, '1', 1, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(128.1,62.5);
        $pdf->Cell(5.3, 19, '2', 1, 1, 'C', 0, '', 0);

        $pdf->SetXY(133.4,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, '2 AÑOS A < 3 AÑOS', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(138.7,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5.3, '3 AÑOS A < 5 AÑOS', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(144,43.5);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(15.3, 6, 'VIT "A"', 1, 1, 'C', 0, '', 0);

        $pdf->SetXY(144,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,15.3,'', 1, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(146,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,10.8,'6 meses', 0, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(148,62.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6.5);
        $pdf->Cell(13,10.8,'< 5 años', 0, 0, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(144,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(19, 5.3, 'DOSIS UNICA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(149.3,62.5);
        $pdf->Cell(5, 19, '1', 1, 1, 'C', 0, '', 0);

        $pdf->SetFillColor(255,255,255);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(154.3,62.5);
        $pdf->Cell(5, 19, '2', 1, 1, 'C', 0, '', 0);
        /*$pdf->SetXY(81,43.5);
        $pdf->Cell(36.5, 38, '', 1, 1, 'C', 0, '', 0);*/

        //==============ATENCIÓN A LA MUJER
        $pdf->SetXY(159.3,30);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->MultiCell(15, 9,'ATENCIÓN A LA MUJER', 1, 'C', 1, 0, '', '', true);
        $pdf->SetXY(159.3,39);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->MultiCell(15, 10.5,'CONTROL PRENATAL', 1, 'C', 1, 0, '', '', true);

        $pdf->SetXY(159.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'SEMANA DE GESTACION', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(164.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'N° DE CONTROL', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(169.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->Cell(32, 5, 'DOSIS COMPLETA HIERRO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        //======================DETECCION DE ENFERMEDADES Y
        $pdf->SetXY(174.3,30);
        $pdf->MultiCell(48, 9,'DETECCION DE ENFERMEDADES Y FACTORES DE RIESGO', 1, 'C', 1, 0, '', '', true);

        $pdf->SetXY(174.3,39);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->MultiCell(14.4, 10.5,'ESTADO DE NUTRICION (IMC)', 1, 'C', 1, 0, '', '', true);

        $pdf->SetXY(174.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 4.8, 'BAJO PESO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(179.1,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 4.8, 'SOBRE PESO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(183.9,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 4.8, 'OBESIDAD', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(188.7,49.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->Cell(10.5, 9.6, 'CANCER', 1, 1, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(188.7,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->Cell(32, 4.8, 'CUELLO UTERINO (PAP)', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(193.5,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 4.8, 'MAMA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(198.3,49.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->Cell(10.5, 9.6, '', 1, 1, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(197,49.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 5);
        $pdf->Cell(10.5, 9.6, 'HIPER-', 0, 1, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(198.4,49.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 5);
        $pdf->Cell(10.5, 9.6, 'GLUCEMIA', 0, 1, 'C', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(198.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 6);
        $pdf->Cell(32, 4.8, 'PRE-DIABETES', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(203.1,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 4.8, 'DIABETES', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(207.9,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(42.5, 4.8, 'HIPERTENSION ARTERIAL', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(212.7,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(42.5, 4.8, 'ITS - VIH - SIDA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(217.5,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(42.5, 4.8, 'SINT. RESPIRATORIO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();


        //======================DETECCION DE ENFERMEDADES Y
        $pdf->SetXY(222.3,49.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 5.5);
        $pdf->MultiCell(19.5, 10,'ENFERMEDADES PRESALENTES', 1, 'C', 1, 0, '', '', true);
        $pdf->StopTransform();

        $pdf->SetXY(222.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'IRA / NEUMONIA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(227.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'EDA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();


        $pdf->SetXY(232.3,49.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 5.5);
        $pdf->MultiCell(19.5, 10,'EXÁMENES COM- PLEMENTARIOS', 1, 'C', 1, 0, '', '', true);
        $pdf->StopTransform();

        $pdf->SetXY(232.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'LABORATORIO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(237.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'IMAGENOLOGIA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        //===============================================

        $pdf->SetXY(242.3,30);
        $pdf->MultiCell(25, 19.5,' ', 1, 'C', 1, 0, '', '', true);
        $pdf->SetXY(242,37);
        $pdf->MultiCell(35, 0,' OTRAS ', 0, 'C', 0, 0, '', '', true);
        $pdf->SetXY(242,40);
        $pdf->MultiCell(35, 0,' ACTIVIDADES ', 0, 'C', 0, 0, '', '', true);

        $pdf->SetXY(242.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'REFERENCIA', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(247.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'RETORNO', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(252.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'RIESGO PROFESIONAL', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(257.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'INCAPACIDAD TEMPORAL', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();

        $pdf->SetXY(262.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->Cell(32, 5, 'N° DE PRESCRIPCION', 1, 1, 'L', 0, '', 0);
        $pdf->StopTransform();


        //=============================





        //---------------------------------------------
        $pdf->SetXY(267.3,30);
        $pdf->MultiCell(48, 51.5,' ', 1, 'C', 1, 0, '', '', true);
        $pdf->SetXY(269,55);
        $pdf->SetFont('helvetica', '', 8);
        $pdf->MultiCell(40, 0,' DIAGNOSTICO', 0, 'C', 0, 0, '', '', true);


        $pdf->SetXY(315.3,81.5);
        $pdf->StartTransform();
        $pdf->Rotate(90);
        $pdf->Cell(51.5,12,'CODIGO CIE - 10',1,1,'C',0,'');
        $pdf->StopTransform();

        });
        $pdf::setFooterCallback(function($pdf){
            $pdf->setY(-15);   
            $pdf->SetFont('helvetica', '', 7);
            $pdf->Cell(0, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, 0, 'R',0, '', 0);   
        });

        $pdf::AddPage('L',$pageLayout);
        $pdf::SetMargins(10, 81.5, 10, true);

        $pdf::SetXY(5,81.5);

        $i=0;
        //-------datos del formulario 
        foreach ($modeloHcl_cabecera_registro as $dato) {
            $i++;
            $fecha_registro=$dato->res_fecha_registro;
            $fecha_nacimiento=$dato->res_fecha_nacimiento;
            //var_dump($dato);
            $fecha_edad=date_diff(date_create($fecha_nacimiento),date_create($fecha_registro));
            $edad=$fecha_edad->format('%y');



           // print_r($dato->res_hcl_poblacion_nombrecompleto.' ');

            $modelHcl_poblacion = Hcl_poblacion::find($dato->res_idhcl_poblacion);
            //var_dump($modelHcl_poblacion);            

            //SIGNOS VITALES
            $modelHcl_datos_signos_vitales = Hcl_datos_signos_vitales::where([
                            ['idhcl_cabecera_registro', '=',$dato->res_id],                            
                            ['idhcl_variables_signos_vitales', '=', Hcl_datos_signos_vitales::TALLA],
                        ])->first();
                        
 
            //var_dump($modelHcl_datos_signos_vitales);   NULL
            
            // PRIMER DIAGNOSTICO REGISTRADO
            $modelHcl_diagnosticoscie = Hcl_diagnosticoscie::where([
                            ['idhcl_cabecera_registro', '=', $dato->res_id],
                            ['numero', '=', Hcl_diagnosticosCIE::DIAGNOSTICO_BASICO]
                        ])->first();  
  
            //TALLA
            $talla_normal = '';
            $talla_baja = '';
            $talla_altura=0;

             
            if ($edad>=8) {
                $modelHcl_clasificaciontalla = Hcl_clasificaciontalla::where([
                            ['edad', '>=', $edad > 18? 18:$edad],
                            ['sexo', '=', $modelHcl_poblacion->sexo]
                        ])->first();   

                if ($modelHcl_datos_signos_vitales != null) {
                    $talla_altura=$modelHcl_datos_signos_vitales->contenido_variable;// altura cm
                    $talla_normal = PdfController::in_range($modelHcl_datos_signos_vitales->contenido_variable != null? $modelHcl_datos_signos_vitales->contenido_variable:0, $modelHcl_clasificaciontalla->valorinferior, $modelHcl_clasificaciontalla->valorsuperior ) == true? 'X' : '';
                    $talla_baja = ($modelHcl_datos_signos_vitales->contenido_variable != null? $modelHcl_datos_signos_vitales->contenido_variable:0)<$modelHcl_clasificaciontalla->valorinferior ? 'X' : '';
                }

            }else
                $modelHcl_clasificaciontalla = null;

            //PESO-TALLA KG-CM
            $peso_obesidad='';
            $peso_sobrepeso='';
            $peso_normal='';
            $peso_leve='';
            $peso_moderada='';
            $peso_grave='';
            $peso_medida=0;

            //SIGNOS VITALES PESO
            $modelHcl_datos_signos_vitales = Hcl_datos_signos_vitales::where([
                            ['idhcl_cabecera_registro', '=',$dato->res_id],                            
                            ['idhcl_variables_signos_vitales', '=', Hcl_datos_signos_vitales::PESO],
                        ])->first();
            if ($modelHcl_datos_signos_vitales != null) {
                $peso_medida=$modelHcl_datos_signos_vitales->contenido_variable;
                $altura=($talla_altura==0)?1:$talla_altura/100;
                $peso=($peso_medida=='')?0:$peso_medida;
                $imc = $peso / ($altura*$altura);
                $imc=number_format($imc,2);
                if($imc>0){
                    $modelHcl_salud = DB::table(DB::raw("hcl_clasificacionsalud where '".$imc."' between valorinferior and valorsuperior"))->first();  
                    if($modelHcl_salud!=null){
                    switch ($modelHcl_salud->id) {
                        case Hcl_clasificacionsalud::DELGADEZ_SEVERA:
                            $peso_grave='X';
                            break;
                        case Hcl_clasificacionsalud::DELGADEZ_MODERADA:
                            $peso_moderada='X';
                            break;
                        case Hcl_clasificacionsalud::DELGADEZ_ACEPTABLE:
                            $peso_leve='X';
                            break;
                        case Hcl_clasificacionsalud::NORMAL:
                            $peso_normal='X';
                            break;
                        case Hcl_clasificacionsalud::SOBREPESO:
                            $peso_sobrepeso='X';
                            break;
                        case Hcl_clasificacionsalud::OBESO_1:
                            $peso_obesidad='X';
                            break;
                        case Hcl_clasificacionsalud::OBESO_2:
                            $peso_obesidad='X';
                            break;                
                        case Hcl_clasificacionsalud::OBESO_3:
                            $peso_obesidad='X';
                            break;    
                        }
                    }


                }                           

            }
           // echo "<p> Altura:".$talla_altura." PESO: ".$peso_medida." IMC: ".$imc." </p>";                         

            $pdf::SetFont('helvetica', '', 7);
            $pdf::SetX(5);
            $pdf::Cell(5, 0, $i, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(22, 0, $dato->res_codigo, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(7, 0, ($dato->res_sexo == 1)? $edad: '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(7, 0, ($dato->res_sexo == 2)? $edad: '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 0, ($dato->res_idtipopaciente== 1)? 'X': '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 0, ($dato->res_idtipopaciente== 2)? 'X': '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 0, ($dato->res_idtipopaciente== 3)? 'X': '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 0, ($dato->res_idtipopaciente== 4)? 'X': '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 0, ($dato->res_idtipopaciente== 5)? 'X': '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 0, ($dato->res_tipo_consulta== 0)? 'X': '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 3.3, ($dato->res_tipo_consulta!== 0)? $dato->res_tipo_consulta: '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.3, 3.3, $peso_obesidad, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.3, 3.3, $peso_sobrepeso, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.3, 3.3, $peso_normal, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.3, 3.3, $peso_leve, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5, 3.3, $peso_moderada, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.1, 3.3, $peso_grave, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.2, 3.3, $talla_normal, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(5.3, 3.3, $talla_baja, 1, 0, 'C', 0, '', 0);
            // 19
            $pdf::Cell(5.3, 3.3,'' , 1, 0, 'C', 0, '', 0);
            // 20
            $pdf::Cell(5.3,3.3,'', 1, 0, 'C', 0, '', 0);
            // 21
            $pdf::Cell(5.3,3.3, '', 1, 0, 'C', 0, '', 0);
            // 22
            $pdf::Cell(5.3,3.3,'', 1, 0, 'C', 0, '', 0);
            // 23
            $pdf::Cell(5.3, 3.3, '', 1, 0, 'C', 0, '', 0);
            // 24
            $pdf::Cell(5, 3.3,'' , 1, 0, 'C', 0, '', 0);
            // 25
            $pdf::Cell(5, 3.3, '', 1, 0, 'C', 0, '', 0);
            // 26
            $pdf::Cell(5, 3.3, '', 1, 0, 'C', 0, '', 0);
            // 27
            $pdf::Cell(5, 3.3,'' , 1, 0, 'C', 0, '', 0);
            // 28
            $pdf::Cell(5, 3,'' , 1, 0, 'C', 0, '', 0);
            // 29
            $pdf::Cell(4.8, 3,'' , 1, 0, 'C', 0, '', 0);
            // 30
            $pdf::Cell(4.8, 3,'' , 1, 0, 'C', 0, '', 0);
            // 31
            $pdf::Cell(4.8, 3,'' , 1, 0, 'C', 0, '', 0);
            // 32
            $pdf::Cell(4.8, 0,'' , 1, 0, 'C', 0, '', 0);
            // 33
            $pdf::Cell(4.8, 0,'' , 1, 0, 'C', 0, '', 0);
            // 34
            $pdf::Cell(4.8, 0,'' , 1, 0, 'C', 0, '', 0);
            // 35
            $pdf::Cell(4.8, 0,'' , 1, 0, 'C', 0, '', 0);
            // 36
            $pdf::Cell(4.8, 0,'' , 1, 0, 'C', 0, '', 0);
            // 37
            $pdf::Cell(4.8, 0, '', 1, 0, 'C', 0, '', 0);
            // 38
            $pdf::Cell(4.8, 0, '', 1, 0, 'C', 0, '', 0);
            // 39
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 40
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 41
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 42
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 43
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 44
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 45
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 46
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            // 47
            $pdf::Cell(5, 0, '', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(48, 0, $modelHcl_diagnosticoscie != null? substr($modelHcl_diagnosticoscie->diagnostico,0,40) : 'NO REGISTRADO', 1, 0, 'L', 0, '', 0);
            $pdf::Cell(12, 0, $modelHcl_diagnosticoscie != null? $modelHcl_diagnosticoscie->codigo : '', 1, 0, 'C', 0, '', 0);
            $pdf::Ln(); 

        }
        // return;

        
        //---exportar PDF
        $pdf::Output('formulario_202.pdf', 'I');
        mb_internal_encoding('utf-8');
        


    }

    function in_range($number, $value1, $value2){
        if( !is_numeric($number) OR !is_numeric($value1) OR !is_numeric($value2) ) return false;
        if($value1>$value2){
            $min = $value2;
            $max = $value1;
        }else{
            $min = $value1;
            $max = $value2;
        }
        if( $number <= $max AND $number >= $min ) return true;
        return false;
    } 

    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
    public function imprimirConsultaExterna($parametro){     


        $arrayDatos = Home::parametrosSistema(); 

        if ($parametro->fechaLista == '') {
            print_r('<h3>Se requiere llenar Especialidad y fecha de solicitud de reporte.</h3>');
            return;
        } 
        //---varables del reporte----------------    
        $titulo = 'INFORME CONSULTA EXTERNA';
        $path_logo='imagenes/logo.jpg';
        $titulo_cns="CAJA NACIONAL DE SALUD";
        
        $fecha='08/04/2022';
        // datos de control
        $pfecha_registro = $parametro->fechaLista;                                  // fecha de atencion
        $piddatosgenericos = $arrayDatos['iddatosgenericos'];             // id medico            tabla datosgenericos
        $pidhcl_cuaderno = $parametro->especialidadLista;                                         // dato de especialidad 


        $modelHcl_cuaderno = Hcl_cuaderno::find($pidhcl_cuaderno); 

        // UNIDAD MEDICA
        $modelAlmacen = Almacen::find($arrayDatos['idalmacen']);
        $regional= $modelAlmacen->descripcion;

        // DATOS MEDICO
        $modelDatosgenericos = Datosgenericos::find($piddatosgenericos);
 
        $modeloHcl_cabecera_registro = DB::table(DB::raw("hcl_informe_formulario_202('".$pidhcl_cuaderno."','".$pfecha_registro."','".$piddatosgenericos."')"))->get();  

        ob_end_clean();
        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pageLayout = array(216,330); //  tamaño Oficio Bolivia

        $pdf::SetTitle("INFORME CONSULTA EXTERNA D");
        $pdf::SetSubject("TCPDF");
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $centro='CIMFA SUCRE 25 DE MAYO';

        $pdf::setHeaderCallback(function($pdf) use($titulo,$path_logo,$titulo_cns,$regional,$centro,$modelHcl_cuaderno){
        //--cabecera PDF First Page -TITULO
        $pdf->SetFont('helvetica', '', 11);
        $pdf->SetXY(5,5);
        $pdf->Cell(320, 0, $centro, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(310,5);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(75);
        $pdf->SetXY(5,5);
        $pdf->Image($path_logo, '', '', 15, 15, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $pdf->SetXY(30,5);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->Cell(50, 0, $titulo_cns, 0, 0, 'C', 0, '', 0);
        //---lineas
        $style = array('width' => 0.2, 'cap' => 'round', 'join' => 'rmiter', 'dash' => 0, 'color' => array(0, 0, 0));       
        //CABECERA PRINCIPAL
        $pdf->Ln();
        $pdf->Cell(0, 0, 'CONSULTA EXTERNA', 0, 0, 'C', 0, '', 0);
        $pdf->Ln();
        $pdf->Cell(0, 0, $modelHcl_cuaderno->nombre, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(210,17);               
        //----CABECERA DE TABLA
        $pdf->SetFillColor(211,211,211);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 7);
        $pdf->SetXY(10,20);
        $pdf->Cell(40, 0, 'Hora', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(25, 0, 'N°HC', 1, 0, 'C', 1, '', 0);
        // $pdf->Cell(30, 0, 'Ap.Paterno', 1, 0, 'C', 1, '', 0);
        // $pdf->Cell(30, 0, 'Ap.Materno', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(95, 0, 'Paciente', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(30, 0, 'Edad', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(20, 0, 'Consulta', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(100, 0, 'Diagnostico', 1, 0, 'C', 1, '', 0);
        // $pdf->Cell(30, 0, 'Consultorio Asignado', 1, 0, 'C', 1, '', 0);        
        });

        $pdf::setFooterCallback(function($pdf){
        $pdf->setY(-15);   
        $pdf->SetFont('helvetica', '', 7);
        //$pdf->SetFillColor(255,255,255);
        $pdf->Cell(0, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, 0, 'R',0, '', 0);   
        });

        $pdf::AddPage('L',$pageLayout);
        $pdf::SetMargins(10, 23.2, 10, true);
        $pdf::SetXY(10,23.2);        
        $pdf::SetFont('helvetica', '', 7);
        $pdf::SetFillColor(211,211,211);
        $pdf::Cell(0,0,$fecha, 1, 1, 'L', 1, '', 0);
        $i=0;        
            //-------datos del formulario 
           // for ($i=0; $i < 120; $i++) { 
           //  $pdf::SetX(10);
           //  $pdf::SetFont('helvetica', '', 7);
           //  $pdf::Cell(40, 0, 'Hora', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(25, 0, 'N°HC', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Ap.Paterno', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Ap.Materno', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(35, 0, 'Nombre(s)', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Edad', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(20, 0, 'Consulta', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(70, 0, 'Diagnostico', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Consultorio Asignado', 1, 0, 'C', 0, '', 0);
           //  $pdf::Ln();
           // }
        foreach ($modeloHcl_cabecera_registro as $dato) {

            $fecha_registro=$dato->res_fecha_registro;
            $fecha_nacimiento=$dato->res_fecha_nacimiento;
            //var_dump($dato);
            $fecha_edad=date_diff(date_create($fecha_nacimiento),date_create($fecha_registro));
            $edad=$fecha_edad->format('%y');

            // PRIMER DIAGNOSTICO REGISTRADO
            $modelHcl_diagnosticoscie = Hcl_diagnosticoscie::where([
                            ['idhcl_cabecera_registro', '=', $dato->res_id],
                            ['numero', '=', Hcl_diagnosticosCIE::DIAGNOSTICO_BASICO]
                        ])->first(); 

            $pdf::SetX(10);
            $pdf::SetFont('helvetica', '', 7);
            $pdf::Cell(40, 0, $dato->res_hora_registro, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(25, 0, $dato->res_codigo, 1, 0, 'C', 0, '', 0);
            // $pdf::Cell(30, 0, 'Ap.Paterno', 1, 0, 'C', 0, '', 0);
            // $pdf::Cell(30, 0, 'Ap.Materno', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(95, 0, $dato->res_hcl_poblacion_nombrecompleto, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(30, 0, $edad, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(20, 0, $dato->res_tipo_consulta == 1? 'NUEVA':'REPETIDA', 1, 0, 'C', 0, '', 0);
            $pdf::Cell(100, 0, $modelHcl_diagnosticoscie != null? $modelHcl_diagnosticoscie->diagnostico : 'NO REGISTRADO', 1, 0, 'L', 0, '', 0);
            // $pdf::Cell(30, 0, 'Consultorio Asignado', 1, 0, 'C', 0, '', 0);
            $pdf::Ln();
       }
        $pdf::SetFillColor(211,211,211);
        $pdf::Cell(0,0,'', 1, 0, 'L', 1, '', 0);
        $pdf::SetX(10);
        $pdf::Cell(40, 0, 'Total: '.$i, 1, 0, 'R', 0, '', 0);

        //---exportar PDF
        $pdf::Output('formulario_202.pdf', 'I');
        mb_internal_encoding('utf-8');

    }

    public static function imprimirRegistros_lista($parametro)
    {     
        $arrayDatos = Home::parametrosSistema(); 
        if ($parametro->fechaDesde == '' && $parametro->fechaHasta == '') {
            print_r('<h3>Se requiere llenar Especialidad y fecha de solicitud de reporte.</h3>');
            return;
        } 
        //---varables del reporte----------------    
        $titulo = 'LISTA DE ATENCION MEDICA';
        $path_logo='imagenes/logo.jpg';
        $titulo_cns="CAJA NACIONAL DE SALUD";       
        // datos de control
        $pfecha_desde = $parametro->fechaDesde;                                  // fecha de atencion
        $pfecha_hasta = date("Y-m-d",strtotime($parametro->fechaHasta."+ 1 days"));                                // fecha de atencion 
        // UNIDAD MEDICA
        $modelAlmacen = Almacen::find($arrayDatos['idalmacen']);
        $regional= $modelAlmacen->descripcion;
        // DATOS MEDICO 
        // $modeloHcl_cabecera_registro = DB::table(DB::raw("hcl_informe_formulario_202('".$pidhcl_cuaderno."','".$pfecha_registro."','".$piddatosgenericos."')"))->get();  
        $modelo = DB::table(DB::raw("hcl_lista_registro_diario(".$arrayDatos['iddatosgenericos'].",'".$pfecha_desde."','".$pfecha_hasta."')"))->get();        
        // print_r($modelo);
        // return;
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pageLayout = array(216,330); //  tamaño Oficio Bolivia
        $pageLayout = array(279,216); //  tamaño Carta Bolivia
        $pdf::SetTitle("LISTA DE CONSULTA");
        $pdf::SetSubject("TCPDF");
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $centro='CIMFA SUCRE 25 DE MAYO';
        $pdf::setHeaderCallback(function($pdf) use($titulo,$titulo_cns,$regional,$centro,$modelo){
        //--cabecera PDF First Page -TITULO
        $pdf->SetFont('helvetica', '', 11);
        $pdf->SetXY(5,5);
        $pdf->Cell(320, 0, $centro, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(310,5);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(75);
        $pdf->SetXY(5,5);
        // $pdf->Image($path_logo, '', '', 15, 15, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $pdf->SetXY(30,5);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->Cell(10, 0, $titulo_cns, 0, 0, 'C', 0, '', 0);
        //---lineas
        $style = array('width' => 0.2, 'cap' => 'round', 'join' => 'rmiter', 'dash' => 0, 'color' => array(0, 0, 0));       
        //CABECERA PRINCIPAL
        $pdf->Ln();
        $pdf->Cell(0, 0, 'LISTA DE ATENCION', 0, 0, 'C', 0, '', 0);
        $pdf->Ln();
        $pdf->Cell(0, 0, $modelo[0]->res_especialidad, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(210,17);               
        //----CABECERA DE TABLA
        $pdf->SetFillColor(211,211,211); 
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetXY(10,20);
        $pdf->Cell(10, 0, 'No.', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(22, 0, 'Hora', 1, 0, 'C', 1, '', 0); 
        $pdf->Cell(22, 0, 'Fecha', 1, 0, 'C', 1, '', 0); 
        $pdf->Cell(41, 0, 'Medico', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(60, 0, 'Paciente', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(30, 0, 'Atención', 1, 0, 'C', 1, '', 0);        
        });         
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetMargins(10, 23.2, 10, true);
        $pdf::SetXY(10,23.2);        
        $pdf::SetFont('helvetica', '', 8);
        $pdf::SetFillColor(211,211,211);
        $pdf::Cell(185,0,'', 1, 1, 'L', 1, '', 0);
        $i=0;  
        foreach ($modelo as $dato) 
        {
            $pdf::SetX(10);
            $pdf::SetFont('helvetica', '', 8);
            $pdf::Cell(10, 0, $dato->res_numero, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(22, 0, $dato->res_hora_registro, 1, 0, 'C', 0, '', 0);            
            $pdf::Cell(22, 0, $dato->res_fecha_registro, 1, 0, 'C', 0, '', 0);            
            $pdf::Cell(41, 0, $dato->res_ap_medico.' '.$dato->res_nombres_medico, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(60, 0, $dato->res_primer_apellido.' '.$dato->res_segundo_apellido.' '.$dato->res_nombre, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(30, 0, $dato->res_ficha_nombre, 1, 0, 'C', 0, '', 0);
            $pdf::Ln();
        }
        $pdf::SetFillColor(211,211,211);        
        $pdf::SetX(10);
        $pdf::Cell(40, 0, 'Total: '.sizeof($modelo), 1, 0, 'R', 0, '', 0);
        //---exportar PDF
        $pdf::Output('formulario_202.pdf', 'I');
        mb_internal_encoding('utf-8');
    }

    public static function imprimirFormularios_lista($parametro)
    {     
        $arrayDatos = Home::parametrosSistema(); 
        if ($parametro->fechaDesde_mdt == '' && $parametro->fechaHasta_mdt == '') {
            print_r('<h3>Se requiere fecha de solicitud de reporte.</h3>');
            return;
        } 
        //---varables del reporte----------------    
        $titulo = 'LISTA DE ATENCION MEDICA';
        $path_logo='imagenes/logo.jpg';
        $titulo_cns="CAJA NACIONAL DE SALUD";       
        // datos de control
        $pfecha_desde = $parametro->fechaDesde_mdt;                                  // fecha de atencion
        $pfecha_hasta = date("Y-m-d",strtotime($parametro->fechaHasta_mdt."+ 1 days"));                                // fecha de atencion 
        // UNIDAD MEDICA
        $modelAlmacen = Almacen::find($arrayDatos['idalmacen']);
        $regional= $modelAlmacen->descripcion;
        // DATOS MEDICO 
        // $modeloHcl_cabecera_registro = DB::table(DB::raw("hcl_informe_formulario_202('".$pidhcl_cuaderno."','".$pfecha_registro."','".$piddatosgenericos."')"))->get();  
        $modelo = DB::table(DB::raw("med_lista_formularios(".$arrayDatos['idalmacen'].",'".$pfecha_desde."','".$pfecha_hasta."')"))->orderBy('res_nro_folio', 'ASC')->get();        
        // print_r($modelo);
        // return;
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pageLayout = array(216,330); //  tamaño Oficio Bolivia
        $pageLayout = array(279,216); //  tamaño Carta Bolivia
        $pdf::SetTitle("LISTA DE REGISTRO DEPOSITOS");
        $pdf::SetSubject("TCPDF");
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $centro='CIMFA SUCRE 25 DE MAYO';
        $pdf::setHeaderCallback(function($pdf) use($titulo,$titulo_cns,$regional,$centro){
        //--cabecera PDF First Page -TITULO
        $pdf->SetFont('helvetica', '', 11);
        $pdf->SetXY(5,5);
        $pdf->Cell(320, 0, $centro, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(310,5);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(75);
        $pdf->SetXY(5,5);
        // $pdf->Image($path_logo, '', '', 15, 15, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $pdf->SetXY(30,5);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->Cell(10, 0, $titulo_cns, 0, 0, 'C', 0, '', 0);
        //---lineas
        $style = array('width' => 0.2, 'cap' => 'round', 'join' => 'rmiter', 'dash' => 0, 'color' => array(0, 0, 0));       
        //CABECERA PRINCIPAL
        $pdf->Ln();
        $pdf->Cell(0, 0, 'DEPÓSITOS DE RECIBOS POR CONCEPTO DE EXAMENES ', 0, 0, 'C', 0, '', 0);
        $pdf->Ln();
        $pdf->Cell(0, 0, 'EXAMENES PRE-OCUPACIONALES', 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(210,17);               
        //----CABECERA DE TABLA
        $pdf->SetFillColor(211,211,211); 
        $pdf->SetFont('helvetica', '', 8);
        $pdf->SetXY(10,20);
        $pdf->Cell(10, 0, 'No.', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(27, 0, 'N° RECIBO', 1, 0, 'C', 1, '', 0); 
        $pdf->Cell(28, 0, 'N° DEPOSITO', 1, 0, 'C', 1, '', 0); 
        $pdf->Cell(22, 0, 'FECHA', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(78, 0, 'APELLIDOS Y NOMBRES', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(20, 0, 'MONTO', 1, 0, 'C', 1, '', 0);        
        });         
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetMargins(10, 23.2, 10, true);
        $pdf::SetXY(10,23.2);        
        $pdf::SetFont('helvetica', '', 8);
        $pdf::SetFillColor(211,211,211);
        $pdf::Cell(185,0,'', 1, 1, 'L', 1, '', 0);
        $i=0; 
        $j=0; 
        foreach ($modelo as $dato) 
        {
            $i += 1;
            $j += $dato->res_eliminado == 0? $dato->res_costo_formulario:0;
            $pdf::SetX(10);
            $pdf::SetFont('helvetica', '', 8);
            $pdf::Cell(10, 0, $i, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(27, 0, $dato->res_nro_folio, 1, 0, 'C', 0, '', 0);    
            if ($dato->res_med_tipo_cancelacion == Med_tipo_cancelacion::EFECTIVO) 
            {   
                if ($dato->res_eliminado == 1) {
                    $pdf::Cell(28, 0, 'ANULADO', 1, 0, 'C', 0, '', 0);               
                }else         
                    $pdf::Cell(28, 0, $dato->res_tipo_pago, 1, 0, 'C', 0, '', 0);            
            }else        
            {      
                $deposito = Med_tipo_pago::where([
                            ['idmed_registro', '=', $dato->id],
                            ['eliminado', '=', 0]
                        ])->first(); 
                $pdf::Cell(28, 0, $deposito->nro_deposito, 1, 0, 'C', 0, '', 0);            
            }            
            $pdf::Cell(22, 0, $dato->res_fecha_registro, 1, 0, 'C', 0, '', 0);                        
            $pdf::Cell(78, 0, $dato->res_primer_apellido.' '.$dato->res_segundo_apellido.' '.$dato->res_nombre_paciente, 1, 0, 'C', 0, '', 0);
            $pdf::Cell(20, 0, $dato->res_eliminado == 0? $dato->res_costo_formulario:0, 1, 0, 'C', 0, '', 0);
            $pdf::Ln();
        }
        $pdf::SetFillColor(211,211,211);        
        $pdf::SetX(10);
        $pdf::Cell(37, 0, 'Total Registros: '.sizeof($modelo), 1, 0, 'R', 0, '', 0);
        $pdf::Cell(108, 0, '', 0, 0, 'R', 0, '', 0);
        $pdf::Cell(40, 0, 'TOTAL: '.$j, 1, 0, 'R', 0, '', 0);
        $pdf::Ln();
        $pdf::Ln();
        $pdf::SetX(15);
        $pdf::Cell(60, 0, 'PAPELETA DE DEPÓSITO N°:'.$parametro->deposito_mdt, 0, 1, 'C', 'B', '', 0);
        $pdf::SetX(15);
        $pdf::Cell(28, 0, $parametro->banco_mdt, 'B', 1, 'C', 'B', '', 0);
        //---exportar PDF
        $pdf::Output('lista_formularios.pdf', 'I');
        mb_internal_encoding('utf-8');
    }
    // ---------------------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------------------
}
