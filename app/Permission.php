<?php namespace App;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Zizaco\Entrust\EntrustPermission;
use Illuminate\Support\Facades\DB;
use Auth;

class Permission extends EntrustPermission implements ValidatingModelInterface
{
  use ValidatingModelTrait;

  protected $throwValidationExceptions = true;

  protected $fillable = [
    'name',
    'display_name',
    'description',
  ];

  protected $rules = [
    'name'      => 'required|unique:permissions',
  ];

  public static function verificarAcceso($permiso)
  {
    $model = DB::table('permission_role')
                ->join('roles', 'permission_role.role_id', '=', 'roles.id')
                ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                ->leftjoin('role_user', 'roles.id', '=', 'role_user.role_id')
                ->leftjoin('users', 'role_user.user_id', '=', 'users.id')
                ->select('*')
                ->where([
                        ['permissions.name', '=', $permiso],
                        ['users.name', '=', Auth::user()['name']]
                    ])
                ->get();
    return count($model);
  }
}
