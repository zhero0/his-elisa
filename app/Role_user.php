<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Role_user extends Model
{
    protected $table = 'role_user';
    public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}