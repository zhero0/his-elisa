<?php
namespace App\Models\Fichaje;

use Illuminate\Database\Eloquent\Model;

class Fic_fichas extends Model
{
	protected $table = 'fic_fichas';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
		
	const LIBRE = 1;
	const RESERVADO = 2;
	const OCUPADO = 3;
	const ATENTIDO = 4;
	const NO_ATENTIDO = 5;
}