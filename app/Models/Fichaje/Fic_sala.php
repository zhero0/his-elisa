<?php

namespace App\Models\Fichaje;

use Illuminate\Database\Eloquent\Model; 
class Fic_sala extends Model
{ 

	protected $table = 'fic_sala'; 
	//protected $fillable = array('id', 'codigo', 'nombre','descripcion','usuario' );
	public $timestamps = false;

	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('numero_historia',"like","%$dato%")
									->orwhere('codigo','like','%'.$dato.'%') 
									->orwhere('nombre','like','%'.$dato.'%') 
									->orwhere('primer_apellido','like','%'.$dato.'%')
									->orwhere('segundo_apellido','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Fic_sala::paginate(config('app.pagination'));	
		}		
	}

}

