<?php
namespace App\Models\Administracion;

use Illuminate\Database\Eloquent\Model;

class Datosgenericos extends Model
{
	protected $table = 'datosgenericos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}