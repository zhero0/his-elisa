<?php
namespace App\Models\Administracion;

use Illuminate\Database\Eloquent\Model;

class Impresion extends Model
{
	// protected $table = 'ima_estado';
	// public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
		
	const MEDIA_HOJA = 1;
	const CARTA = 2;
	const OFICIO = 3;
	const H4 = 4;
}