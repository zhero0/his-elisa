<?php
namespace App\Models\Administracion;

use Illuminate\Database\Eloquent\Model;

class Adm_estados extends Model
{
	// protected $table = 'ima_estado';
	// public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
		
	const VIGENTE = 0;
	const NO_VIGENTE = 1;
	
}