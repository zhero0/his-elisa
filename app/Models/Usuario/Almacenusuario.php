<?php 
namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Model;

class Almacenusuario extends Model
{
    protected $table = 'almacen_usuario';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}