<?php
namespace App\Models\Usuario;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Zizaco\Entrust\EntrustPermission;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Permissionrole extends Model
{
    protected $table = 'permission_role'; 
    public $timestamps = false;

    public static function registrarPermisoRol($arrayPermisos, $idrol)
    {
        foreach ($arrayPermisos as $valor)
        {
            $model = new Permissionrole();
            $model->permission_id = $valor;
            $model->role_id = $idrol;
            $model->save();
        }
    }
}
