<?php
namespace App\Models\Internacion;
use \App\Models\Almacen\Home;
use \App\Models\Almacen\Almacen;
use Illuminate\Support\Facades\DB;
use \App\Models\Afiliacion\Hcl_internacion;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Administracion\Datosgenericos;
use \App\Models\Afiliacion\Hcl_datos_signos_vitales;
use Auth;
use Elibyy\TCPDF\Facades\TCPDF;
use \App\Models\Afiliacion\Hcl_poblacion;

use Illuminate\Database\Eloquent\Model;

class Int_recepcion extends Model
{ 
	protected $table = 'int_recepcion';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	// public static function imprimeRecepcion($id)
 //    {
 //    	// phpinfo();
 //    	// return;

 //        $imprimirDirecto = 1;
 //        $model = Ima_registroprogramacion::find($id);
 //        $numeroImprimir = $model->numero;

 //        $modelResultado = DB::table('ima_resultados_programacion')
 //                ->select('*', DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"))
 //                ->where([
 //                    ['eliminado', '=', 0],
 //                    ['idima_registroprogramacion', '=', $model->id]
 //                ])
 //                ->first();

 //        ob_end_clean();
 //        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
 //        $pdf::SetTitle("Internación");
 //        $pdf::SetSubject("TCPDF");

 //        $pdf::SetCreator('PDF_CREATOR');
 //        $pdf::SetAuthor('usuario');
 //        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
 //        //cambiar margenes
 //        $pdf::SetMargins(10, 15, 10, 15);
 //        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
 //        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

 //        //set auto page breaks
 //        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
 //        //$pdf::setAutoPageBreak(true);

 //        $pdf::startPageGroup();
        
 //        //set image scale factor
 //        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
 //        $pdf::setJPEGQuality(100);

 //        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir) {
 //            // $y = $pdf->GetY();
 //            $pdf->SetFillColor(230, 230, 230);
 //            // // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

 //            // $pdf->SetY($y + 1);
 //            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
 //            // $pdf->SetFont('courier', 'B', 20);
 //            // $pdf->SetTextColor(0, 0, 0);
 //            // $pdf->SetY($pdf->GetY());
 //            // $x = $pdf->getPageWidth();
 //            // $y = $pdf->getPageHeight();
 //            // $pdf->MultiCell(130, 10, 'IMAGENOLOGIA C.N.S.', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
 //            // $pdf->MultiCell(18, 10, '', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
 //            // $pdf->MultiCell(40, 10, 'N°:'.$numeroImprimir , 0, 'R', '', 1, '', '', 1, '', '', '', 10, 'M');
 //            // $pdf->Line(31, $pdf->getY()+6, $pdf->getPageWidth()-12, $pdf->getY()+6);


 //            $pdf->SetFont('courier', 'B', 10);
 //            $pdf->MultiCell(130, 5, 'IMAGENOLOGIA C.N.S.', 0, 'L', '', 1, '', '', 1, '', '', '', 5, 'M');

 //            $pdf->SetFont('helvetica', 'B', 15);
 //            $pdf->MultiCell(170, 8, 'SOLICITUD DE EXÁMEN COMPLEMENTARIO Nº '.$numeroImprimir, 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M');

 //            $style = array(
 //                'position' => 'R',
 //                'align' => 'R',
 //                'stretch' => false,
 //                'fitwidth' => true,
 //                'cellfitalign' => '',
 //                'border' => false,
 //                'hpadding' => 'auto',
 //                'vpadding' => 'auto',
 //                'fgcolor' => array(0,0,0),
 //                'bgcolor' => false, //array(255,255,255),
 //                'text' => true,
 //                'font' => 'helvetica',
 //                'fontsize' => 8,
 //                'stretchtext' => 4
 //            ); 
 //            $pdf->write1DBarcode($numeroImprimir, 'C128', '', '', '', 18, 0.4, $style, 'N');
            
 //            // $pdf->Line(10, $pdf->getY()+7, $pdf->getPageWidth()-12, $pdf->getY()+7);
 //        });
 //        $usuario = Auth::user()['name'];
 //        $fecha = date('d-m-Y');

 //        // setPage
 //        $pdf::AddPage();
 //        $pdf::setPage(1, true);
 //        $pdf::SetFillColor(255, 255, 255);

 //        $y_Inicio = 18;
 //        $height = 5;
 //        $width = 190;
 //        $widthLabel = 21;
 //        $widthDosPuntos = 4;
 //        $widthInformacion = 40;
 //        $espacio = 3;
 //        $tipoLetra = 'helvetica';
 //        $tamanio = 9;
 //        $widthTitulo = 50;

 //        $especialidad_Examenes = DB::table('ima_examenes_registroprogramacion as rp')
 //                            ->join('ima_examenes as e', 'rp.idima_examenes', '=', 'e.id')
 //                            ->join('ima_especialidad as esp', 'e.idima_especialidad', '=', 'esp.id')
 //                            ->select(
 //                                'e.idima_especialidad', 'esp.nombre'
 //                            )
 //                            ->where([
 //                                ['rp.eliminado', '=', 0],
 //                                ['rp.idima_registroprogramacion', '=', $model->id]
 //                            ])
 //                            ->groupBy('e.idima_especialidad', 'esp.nombre')
 //                            ->orderBy('esp.nombre', 'ASC')
 //                            ->get();
        
 //        for($e = 0; $e < count($especialidad_Examenes); $e++)
 //        {
 //            if($e > 0)
 //                $pdf::AddPage();

 //            $pdf::SetY($y_Inicio);
 //            // ------------------------------------------ [PACIENTE] ------------------------------------------
 //            $pdf::SetFont($tipoLetra, 'B', $tamanio);
 //            $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            $pdf::SetFont($tipoLetra, '', $tamanio);
            
 //            $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(100, $height, $modelResultado->hcl_poblacion_matricula, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

 //            $pdf::MultiCell($widthLabel, $height, 'PACIENTE: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(100, $height, $modelResultado->hcl_poblacion_nombrecompleto, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(15, $height, 'EDAD: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(20, $height, $modelResultado->edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

 //            $pdf::MultiCell($widthLabel, $height, 'EMPRESA: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(100, $height, $modelResultado->hcl_empleador_empresa, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);


 //            // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
 //            $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);
 //            $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
 //            $modelHcl_especialidad = Hcl_especialidad::find($model->idhcl_especialidad);
 //            $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
 //            $modelHcl_cuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
 //            $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';

 //            // $pdf::MultiCell($widthTitulo, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            // $pdf::MultiCell($widthTitulo, 2, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
 //            // $pdf::SetFont($tipoLetra, 'B', $tamanio);
 //            // $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            $pdf::SetFont($tipoLetra, '', $tamanio);
            
 //            $label_Hospitalizado = 'HOSPITALIZADO';
 //            $label_Tipo = 'TIPO';
 //            $label_Medico = 'MÉDICO SOL.';
 //            $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
 //            $y_Tipo = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
 //            $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
 //            $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico);

 //            $label_Fecha = 'FECHA';
 //            $label_Especialidad = 'ESPECIALIDAD';
 //            $label_Diagnostico = 'DIAGNÓSTICO';
 //            $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
 //            $y_Especialidad = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
 //            $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
 //            $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);

 //            $hospitalizado = $model->hospitalizado == 1? "SI" : "NO";
 //            $fechaprogramacion = date('d-m-Y', strtotime($model->fechaprogramacion));
 //            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(48, $height, $hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(45, $height, $fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(13, $height, 'HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(20, $height, $model->horaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

 //            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Tipo, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(48, $height, $model->especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(35, $height, $model->cuaderno, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            
 //            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(48, $height, $model->personal, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            
 //            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Diagnostico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell(45, $height, $model->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

 //            // $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
 //            // ------------------------------------------ [OBSERVACIÓN] ------------------------------------------
 //            $pdf::MultiCell(120, $height, 'OBSERVACIÓN:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

 //            /*if ($model->impresion == Impresion::MEDIA_HOJA) {
 //                $pdf::MultiCell('', '', $model->observacion, 0, 'L', 0, 1, '', '', true, 0, false, true, 67, 20);

 //                // MultiCell(w, h, txt, border = 0, align = 'J', fill = 0, ln = 1, x = '', y = '', reseth = true, stretch = 0, ishtml = false, autopadding = true, maxh = 0)
 //                // $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 67, 20);
 //            }else{
 //                // $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 260, 20);
 //            }
 //            */
 //            $pdf::MultiCell('', '', $model->observacion, 0, 'L', 0, 1, '', '', true, 0, false, true, 67, 20);
 //            $pdf::setY($pdf::getY());

            

 //            // --------------------------------------- [EXÁMENES] ---------------------------------------
 //            // $pdf::MultiCell($widthTitulo, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            $pdf::MultiCell($widthTitulo, 2, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
 //            $pdf::SetFont($tipoLetra, 'B', $tamanio);
 //            $pdf::MultiCell($widthInformacion, $height, 'EXÁMENES', 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            $pdf::SetFont($tipoLetra, '', $tamanio);
 //            $modelo = DB::table('ima_examenes_registroprogramacion as rp')
 //                                ->join('ima_examenes as e', 'rp.idima_examenes', '=', 'e.id')
 //                                ->select(
 //                                    'e.codigo', 'e.nombre'
 //                                )
 //                                ->where([
 //                                    ['rp.eliminado', '=', 0],
 //                                    ['rp.idima_registroprogramacion', '=', $model->id],
 //                                    ['e.idima_especialidad', '=', $especialidad_Examenes[$e]->idima_especialidad]
 //                                ])
 //                                ->orderBy('e.nombre', 'ASC')
 //                                ->get();
 //            // $pdf::MultiCell(30, $height, 'CÓDIGO', 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //            // $pdf::MultiCell(100, $height, 'NOMBRE', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            for($j = 0; $j < count($modelo); $j++)
 //            {
 //                // $pdf::MultiCell(30, $height, $modelo[$j]->codigo, 'LBR', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
 //                // $pdf::MultiCell(100, $height, $modelo[$j]->nombre, 'BR', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

 //                $pdf::MultiCell(120, $height, '> '.$modelo[$j]->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
 //            }


            
 //            // ************************ [footer] *******************************************
 //            // if ($model->impresion == Impresion::MEDIA_HOJA) {
 //                $pdf::SetY(143);
 //                $pdf::SetFont('courier', 'B', 7);
 //                $pdf::MultiCell('', '','Usuario: '.$usuario, 'T', 'L', 0);
 //                // $pdf::SetY(-10);
 //                $pdf::SetY(143);
 //                $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
 //                // $pdf::MultiCell(60, '', 'Fecha: '. date("d").'-'.date("m").'-'.date("Y")  , 0, 'L', 0, 1, 60);
 //                // $pdf::SetY(-10);
 //                $pdf::SetY(143);
 //                $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
 //            // }else{
 //            //     $pdf::SetY(268);
 //            //     $pdf::SetFont('courier', 'B', 7);
 //            //     $pdf::MultiCell('', '','Usuario: '.$usuario, 'T', 'L', 0);
 //            //     // $pdf::SetY(-10);
 //            //     $pdf::SetY(268);
 //            //     $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
 //            //     // $pdf::MultiCell(60, '', 'Fecha: '. date("d").'-'.date("m").'-'.date("Y")  , 0, 'L', 0, 1, 60);
 //            //     // $pdf::SetY(-10);
 //            //     $pdf::SetY(268);
 //            //     $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
 //            // }
 //            // ****************************************************************************
 //        }

 //        // Muestra el panel para imprimir de forma directa...
 //        // if($imprimirDirecto == 1)
 //        //     $pdf::IncludeJS('print(true);');

 //        $pdf::Output("reporteInternacion.pdf", "I");

 //        mb_internal_encoding('utf-8');
 //    }
 
    public static function imprimeRecepcion($id)
    { 
        $arrayDatos = Home::parametrosSistema();


        $modelAlmacen = Almacen::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $arrayDatos['idalmacen'] ]
                            ])->first();

        $modelInternado = Int_recepcion::find($id);

        $modelInternacion = Hcl_internacion::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $modelInternado->idhcl_cabecera_registro]
                            ])->first();


         
        $diagnostico = '';
        $band = false;
        $model = DB::table('hcl_cabecera_registro as cr')                    
                    ->select(
                        'cr.id','cr.respuesta', 'cr.tipo_consulta', 'cr.numero', 'cr.hcl_poblacion_nombrecompleto as paciente', 'cr.hcl_poblacion_matricula', 'cr.hcl_poblacion_cod', 'cr.hcl_poblacion_sexo', 
                        DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"),
                        'cr.hcl_empleador_empresa', 'cr.hcl_empleador_patronal', 'cr.idhcl_poblacion','cr.iddatosgenericos','cr.idhcl_cuaderno','cr.fecha_registro','cr.hora_registro'
                    )
                    ->where([
                        ['cr.eliminado', '=', 0],
                        ['cr.id', '=', $modelInternado->idhcl_cabecera_registro]
                    ])
                    ->first();
  
        // print_r($model->id);
        // return; 

        $modelUnidad = Almacen::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $modelInternacion->idalmacen]
                            ])->first();

        $modelServicio = Hcl_cuaderno::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $modelInternacion->idhcl_cuaderno]
                            ])->first();

        $modelMedico = Datosgenericos::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->iddatosgenericos]
                            ])->first();

        $modelCuaderno = Hcl_cuaderno::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->idhcl_cuaderno]
                            ])->first();

        // $modelDiagnosticos = Hcl_diagnosticosCIE::where([
        //                         ['eliminado', '=', 0],
        //                         ['idhcl_cabecera_registro', '=', $model->id]
        //                     ])->get(); 

        $modelSignos_vitales = Hcl_datos_signos_vitales::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $model->id]
                            ])->get(); 

        $numeroImprimir = $model->numero;
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);


        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$model,$modelAlmacen) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
             
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');
            $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $x = $pdf->getPageWidth();
            $y = $pdf->getPageHeight();
            $pdf->SetFont('helvetica', '', 9); 
            $pdf->MultiCell(160, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->MultiCell(50, 5, 'Form. ' , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(160, 5, $modelAlmacen->nombre , 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
            $pdf->MultiCell(50, 5, 'N°: '.$numeroImprimir , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);
            // $pdf->MultiCell(1, 10, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);            
            $pdf->SetFont('courier', 'B', 20); 
            $pdf->MultiCell(190, 10, 'HOJA DE INTERNACION', 0, 'C', 0, 1, '', '', true, 1, false, true, 10, 10);

            // $pdf->SetFont('helvetica', '', 9); 
            // $pdf->MultiCell(15, 10, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);            
            // $pdf->MultiCell(15, 10, $modelAlmacen->nombre , 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 10);

        //     $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     );

        // $pdf->write2DBarcode($model->numero.' - '.$model->hcl_poblacion_matricula.' - '.$model->paciente, 'QRCODE,L', 15, 0, 25, 25, $style, 'N');
        // $pdf->Text(14, 25, 'CNS');

            $pdf->Line(10, 25, $pdf->getPageWidth()-12,25);
        });
// $pdf::MultiCell(50, 5, 'CAJA NACIONAL DE SALUD', 0, 'L', 0, 0, '', '', true, 0, false, true, 5, 10);
        // $pdf::setFooterCallback(function($pdf) use($usuario, $fecha,$model,$modelMedico,$modelCuaderno) {
        //     $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     ); 
            
        //     $pdf->write2DBarcode($model->id.' - '.$model->hcl_poblacion_matricula.'-'.$model->hcl_poblacion_cod.' - '.$model->paciente.' - '.$model->hcl_empleador_patronal.' - '.$model->hcl_empleador_empresa, 'QRCODE,L', 15, 250, 35, 35, $style, 'N');
           

        //     $pdf->SetY(-20);
        //      $pdf->SetFont('helvetica', '', 9);
        //     $pdf->MultiCell(70, 10,'', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
        //     $pdf->MultiCell(50, 10, 'Firma Asegurado', 'T', 'C', 0, 0);
        //     // $pdf->MultiCell(95, 10, $modelCuaderno->nombre.'-'. $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
           
        //     // $pdf->MultiCell(10, 10,  $modelMedico->matricula.' - '.$modelMedico->apellidos.' '.$modelMedico->nombres, 'T', 'C', 0, 1);
        //     $pdf->MultiCell(10, '', '', 0, 'C', 0, 0);            
        //     $pdf->MultiCell(50, 10, 'Firma y sello Médico', 'T', 'C', 0, 1);

        //     $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
        //     $pdf->SetY(-10);
        //     $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
        //     $pdf->SetY(-10);
        //     $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        // });

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        // esto hace que baje la hoja
        $y_Inicio = 27;
        // hasta aqui

        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);

        // ------------------------------------------ [PACIENTE] ------------------------------------------

        // ===================================                ====================================   
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                        DB::raw("date_part('year',age(fecha_nacimiento))  as edad"))
                                ->first();
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $puntos = 3;

        // $pdf::MultiCell(50, $height, 'UNIDAD SOLICITANTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(68, $height, $modelUnidad->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        // $pdf::MultiCell($widthLabel, $height, 'SERVICIO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(50, $height, $modelServicio->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'MEDICO SOLICITANTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelCuaderno->nombre.', '. $modelMedico->matricula.'-'.$modelMedico->apellidos.' '.$modelMedico->nombres, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'FECHA/HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->fecha_registro.' '.$model->hora_registro, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $modelhcl_poblacion->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'SEXO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(50, $height, $model->hcl_poblacion_sexo, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($widthLabel, $height, 'PACIENTE', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(140, $height, $model->paciente, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        

        $pdf::MultiCell($widthLabel, $height, 'EDAD', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, $model->edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        // $pdf::SetFont('helvetica', 'B', 9); 
        // // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(190, $height, 'DATOS DE REFERENCIA', 'T', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::SetFont($tipoLetra, '', $tamanio);
        // $pdf::MultiCell(30, $height, 'Persona Referente', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(50, $height, $modelInternacion->familiar_datos, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, 'Movíl referente:', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(90, $height, $modelInternacion->familiar_movil, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, 'Dirección referente', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(90, $height, $modelInternacion->familiar_domicilio, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        // if ($modelInternacion->consulta != '') {
        //     $pdf::MultiCell(30, $height, 'Breve descripción', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //     $pdf::MultiCell($puntos, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //     $pdf::MultiCell(90, $height, $modelInternacion->familiar_descripcion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // } 

        // $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        // $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);

        $pdf::SetFont('helvetica', 'B', 9); 
        // $pdf::MultiCell(80, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(188, $height, 'DATOS DE ADMISIÓN - ENFERMERIA', 'T', 'C', 0, 1, '', '', true, 0, false, true, $height, 10); 

        $pdf::SetFont('helvetica', 'B', 9);
        $pdf::MultiCell(50, $height, 'DIAGNOSTICOS PRESUNTIVOS', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont('helvetica', '', 9);
        $pdf::MultiCell('', '', $modelInternacion->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont('helvetica', 'B', 9); 
        $pdf::MultiCell(40, $height, 'SIGNOS VITALES', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(40, $height, 'Pr. Arterial:...........', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        
        $pdf::MultiCell(35, $height, 'Pulso:...............', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        
        $pdf::MultiCell(45, $height, 'Temperatura:................', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(40, $height, 'Estatura:..............', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(40, $height, 'Peso:................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(188, $height, 'Estado general del Paciente:...........................................................................................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(188, $height, '.........................................................................................................................................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(188, $height, '.........................................................................................................................................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(188, $height, 'Prescripción médica de urgencia:....................................................................................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(188, $height, '.........................................................................................................................................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(188, $height, '.........................................................................................................................................................................................................', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(32, 20, '', 'B', 'B', 0, 1, '', '', true, 0, false, true, 20, 20);
        $pdf::MultiCell(188, 5, 'Firma Recepcionador', 0, 'L', 'T', 1, '', '', true, 0, false, true, 20, 20);

        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-12, $pdf::getY());
        $pdf::MultiCell('', '', '' , 0, 'L', 0, 1, '', '', true, 0, true, true, 80, 20);
 

        $pdf::Output("internacion.pdf", "I");

        mb_internal_encoding('utf-8');
    }

}