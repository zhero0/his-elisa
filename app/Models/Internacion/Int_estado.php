<?php
namespace App\Models\Internacion;

use Illuminate\Database\Eloquent\Model;

class Int_estado extends Model
{ 
	protected $table = 'int_estado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}