<?php
namespace App\Models\Internacion;

use Illuminate\Database\Eloquent\Model;

class Int_sala extends Model
{ 
	protected $table = 'int_sala';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}