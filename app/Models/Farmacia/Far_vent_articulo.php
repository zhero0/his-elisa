<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model; 
use Auth;

class Far_vent_articulo extends Model
{ 

	protected $table = 'far_vent_articulo'; 
	public $timestamps = false;  

	public static function agregarArticulo($ventanilla, $idArticulo)
    { 
        $model = new Far_vent_articulo;    
        $model->idfar_ventanillas = $ventanilla; 
        $model->idfar_articulo_sucursal = $idArticulo;    
        $model->cantidad_articulo = 0; 
        $model->usuario = Auth::user()['name'];
        $model->save();
    }
}