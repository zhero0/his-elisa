<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Far_adm_articulo extends Model
{ 

	protected $table = 'far_adm_articulo'; 
	public $timestamps = false; 
	
	public static function actualizaSaldoProductos($gridDocumentodetalle)
    {
        if(count($gridDocumentodetalle) > 0)
        {
            for($i = 0; $i < count($gridDocumentodetalle); $i++)
            {                
            	$model = Inv_articulo::find($gridDocumentodetalle[$i]->Idproducto);
            	$saldo = $model->saldo -$gridDocumentodetalle[$i]->cantidad;
                Inv_articulo::where([['eliminado', '=', 0],
                                        ['inv_articulo.id', '=', $gridDocumentodetalle[$i]->Idproducto]
                                    ])->update(['saldo' => $saldo]);
            }
        }
    } 

    public static function alm_lista_articulos_farmacia()
    {        
        $query = DB::connection('serv_almacen')
            ->table(DB::raw("alm_lista_articulos_farmacia(1,0)") )
                    ->paginate(config('app.pagination'));
        return $query;
    }

    public static function alm_lista_articulos_farmacia_excluida()
    {        
        $query = DB::connection('serv_almacen')
            ->table(DB::raw("alm_lista_articulos_farmacia(1,0)") )->get();
        return $query;
    }

}