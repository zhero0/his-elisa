<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \App\Models\Farmacia\Far_adm_articulo; 
use \App\Models\Farmacia\Far_estado;
use Auth;

class Far_articulo_sucursal extends Model
{ 

    protected $table = 'far_articulo_sucursal'; 
    public $timestamps = false;  

    public static function agregarArticuloSucursal($id, $sucursal)
    {
        $modelArticulo = Far_adm_articulo::find($id); 
        $model = new Far_articulo_sucursal;
        $model->articulo_id = $modelArticulo->articulo_id;      
        $model->idfar_adm_clasificacion = $modelArticulo->clasificacion;      
        $model->codificacion_nacional = $modelArticulo->codificacion_nacional;
        $model->codificacion = $modelArticulo->codificacion;
        $model->descripcion = $modelArticulo->descripcion;
        $model->codigo_barras = $modelArticulo->codigo_barras; 
        $model->unidad = $modelArticulo->unidad;
        $model->concentracion = $modelArticulo->concentracion;   
        $model->tipo_envase = $modelArticulo->tipo_envase;          
        $model->psicotropico = $modelArticulo->psicotropico;      
        $model->estupefaciente = $modelArticulo->estupefaciente;      
        $model->idfar_adm_clasificacion = $modelArticulo->idfar_adm_clasificacion;      
        $model->idfar_estado = Far_estado::AUTORIZADO;     
        $model->cantidad_max_salidad = 0;
        $model->stock_minimo = 0;
        $model->stock_maximo = 0;
        $model->via = '';
        $model->indicacion = '';
        $model->saldo = 0;
        $model->dispensar_cuaderno = 1;
        $model->idfar_adm_articulo = $modelArticulo->id;
        $model->idfar_sucursales = $sucursal; 
        $model->usuario = Auth::user()['name'];
        $model->save();
    }

    // public static function actualizaSaldoProductos($gridDocumentodetalle)
    // {
    //     if(count($gridDocumentodetalle) > 0)
    //     {
    //         for($i = 0; $i < count($gridDocumentodetalle); $i++)
    //         {                
    //             $model = Far_articulo_sucursal::find($gridDocumentodetalle[$i]->Idproducto);
    //             $saldo = $model->saldo -$gridDocumentodetalle[$i]->cantidad;
    //             Far_articulo_sucursal::where([['idfar_estado', '=', 0],
    //                                     ['id', '=', $gridDocumentodetalle[$i]->Idproducto]
    //                                 ])->update(['saldo' => $saldo]);
    //         }
    //     }
    // } 

    public static function actualizaSaldoProductos($gridDocumentodetalle, $idfar_cabecera_receta) {
    //echo " ------------------DETALLE DE DISPENSACION------------------";
    $global_dispensado = 0;
    if (count($gridDocumentodetalle) > 0) {
        for ($i = 0; $i < count($gridDocumentodetalle); $i++) {
            $Idproducto = $gridDocumentodetalle[$i]->Idproducto;
            $cantidad = $gridDocumentodetalle[$i]->cantidad;

            //---buscando los productos disponibles
            $modeloSaldos = DB::table('far_cab_registro_detalle')->where([
                        ['idfar_articulo_sucursal', '=', $Idproducto],
                        ['saldo_total', '>', 0]
                        //['fecha_vencimiento', '>', date("Y-m-d")]
                    ])->orderBy('fecha_vencimiento', 'ASC')->get();

            $cantidad_productos = $cantidad;
            $cantidad_dispensado = 0;

            foreach ($modeloSaldos as $key => $value) {
                $saldo_total = $value->saldo_total;
                $detalle_movimiento_id = $value->detalle_movimiento_id;
                $detalle_lote_id = $value->detalle_lote_id;
                $idfar_articulo_sucursal = $value->idfar_articulo_sucursal;
                $salida = $value->salida;
                $idfar_cab_registro_detalle = $value->id;
                $precio = $value->precio_uniario;
                $dispensado = 0;

                if ($cantidad_productos <= $saldo_total) {
                    $dispensado = $cantidad_productos;
                    $cantidad_dispensado += $dispensado;
                    //echo "<p>CANTIDAD DEL LOTE: (" . $detalle_movimiento_id . "-" . $detalle_lote_id . " )" . $saldo_total . " DISPENSADO  -> " . $dispensado . "</p>";
                    $saldo_total = $saldo_total - $cantidad_productos;
                    $salida = $salida + $dispensado;
                    $cantidad_productos = 0;

                    //--actualizar saldos
                    $afectados = DB::table('far_cab_registro_detalle')->where([['id', '=', $idfar_cab_registro_detalle]])->update(['saldo_total' => $saldo_total]);
                    $afectados = DB::table('far_cab_registro_detalle')->where([['id', '=', $idfar_cab_registro_detalle]])->update(['salida' => $salida]);
                    //--guardar entrega movimiento
                    DB::table('far_cab_receta_detalle_movimiento')->insert([
                        'idfar_articulo_sucursal' =>  $idfar_articulo_sucursal,
                        'detalle_movimiento_id' => $detalle_movimiento_id,
                        'detalle_lote_id' =>$detalle_lote_id,
                        'cantidad' => $dispensado,
                        'precio' => $precio,
                        'idfar_cab_registro_detalle' => $idfar_cab_registro_detalle
                    ]);

                    break;
                } else {
                    $dispensado = $saldo_total;
                    $cantidad_dispensado += $dispensado;
                    //echo "<p>CANTIDAD DEL LOTE: (" . $detalle_movimiento_id . "-" . $detalle_lote_id . " )" . $saldo_total . "DISPENSADO -> " . $dispensado . "</p>";
                    $cantidad_productos = $cantidad_productos - $dispensado; // queda pendiente
                    $saldo_total = $saldo_total - $dispensado; // 0
                    $salida = $salida + $dispensado;
                    //--actualizar saldos
                    $afectados = DB::table('far_cab_registro_detalle')->where([['id', '=', $idfar_cab_registro_detalle]])->update(['saldo_total' => $saldo_total]);
                    $afectados = DB::table('far_cab_registro_detalle')->where([['id', '=', $idfar_cab_registro_detalle]])->update(['salida' => $salida]);
                    //--guardar entrega movimiento--
                    DB::table('far_cab_receta_detalle_movimiento')->insert([
                       'idfar_articulo_sucursal' =>  $idfar_articulo_sucursal,
                        'detalle_movimiento_id' => $detalle_movimiento_id,
                        'detalle_lote_id' =>$detalle_lote_id,
                        'cantidad' => $dispensado,
                        'precio' => $precio,
                        'idfar_cab_registro_detalle' => $idfar_cab_registro_detalle
                    ]);
                }
                //$cantidad=$value->res_id;
                //$precio='0.5';
                //---actualizar tablas;
            }

            if ($cantidad_dispensado > 0) {
                $global_dispensado += $cantidad_dispensado;
                $model = Far_articulo_sucursal::find($Idproducto);
                $saldo = $model->saldo - $cantidad_dispensado;
                Far_articulo_sucursal::where([
                    //['idfar_estado', '=', 0],
                    ['id', '=', $gridDocumentodetalle[$i]->Idproducto]
                ])->update(['saldo' => $saldo]);
            }
            
            return $global_dispensado;

        }
    }
    }
}