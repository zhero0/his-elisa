<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;

class Far_adm_estado extends Model
{ 

	protected $table = 'far_adm_estado'; 
	public $timestamps = false;  

	const AUTORIZADO = 1;
	const ANULADO = 2; 
	const DISPENSADO = 4;	
	const SISTEMA = 5;
	const MANUAL = 6;

 
}