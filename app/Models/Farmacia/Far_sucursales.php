<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Far_sucursales extends Model
{ 

	protected $table = 'far_sucursales'; 
	public $timestamps = false; 

	public static function listaSucursales($codigo,$nombre)
  {
    	$codigo = '';
    	$nombre = ''; 
          
    	$query = DB::connection('serv_almacen')
                  ->table(DB::raw("almacen"))
                  ->paginate(config('app.pagination'));
    	return $query;
  }

  public static function datoMigrado($id)
  { 
    	$query = DB::connection('serv_almacen')
                  ->table(DB::raw("almacen"))
                  ->where([ 
                          ['almacen_id', '=', $id],                                                        
                      ])
                  ->first();
    	return $query;
  }

}