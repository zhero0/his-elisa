<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;

class Far_estado extends Model
{ 

	protected $table = 'far_estado'; 
	public $timestamps = false; 

	const AUTORIZADO = 1;
	const ANULADO = 2;

}