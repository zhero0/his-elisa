<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model; 
use \App\Models\Farmacia\Far_adm_tipotransaccion;
use \App\Models\Farmacia\Far_articulo_sucursal;
use \App\Models\Farmacia\Far_adm_articulo;
use \App\Models\Farmacia\Far_estado;
use Illuminate\Support\Facades\DB;
use Auth;

class Far_cab_registro_detalle extends Model
{
	protected $table = 'far_cab_registro_detalle';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	 
	public static function registraProductoNota($modelSucursales, $query_Pedido, $query_Articulos, $modelFar_cabecera_registro)
    {
        
        // print_r($query_Articulos[0]);
        foreach ($query_Articulos as $dato) {

            $modelProducto = Far_articulo_sucursal::where([
                                    ['idfar_sucursales', '=', $modelSucursales->id],
                                    ['articulo_id', '=', $dato->res_articulo_id],
                                ])
                            ->first();
            if ($modelProducto == null) {
                
                $modelProducto_adm = Far_adm_articulo::where([                                    
                                    ['articulo_id', '=', $dato->res_articulo_id]
                                ])
                            ->first();
                if ($modelProducto_adm == null) {
                    $query = DB::connection('serv_almacen')->table(DB::raw("alm_lista_articulos_farmacia(2,'".$dato->res_articulo_id."')") )->first();
                    $modelFar_adm_articulo = new Far_adm_articulo;
                    $modelFar_adm_articulo->articulo_id = $query->res_articulo_id;      
                    $modelFar_adm_articulo->codificacion_nacional = $query->res_codificacion_nacional;
                    $modelFar_adm_articulo->codificacion = $query->res_codificacion;
                    $modelFar_adm_articulo->descripcion = $query->res_descripcion;
                    $modelFar_adm_articulo->unidad = $query->res_unidad;
                    $modelFar_adm_articulo->concentracion = $query->res_concentracion;   
                    $modelFar_adm_articulo->tipo_envase = $query->res_tipo_envase;          
                    $modelFar_adm_articulo->psicotropico = 0;      
                    $modelFar_adm_articulo->estupefaciente = 0;      
                    $modelFar_adm_articulo->idfar_adm_estado = $query->res_estado;        
                    $modelFar_adm_articulo->idfar_adm_clasificacion = 1;         
                    $modelFar_adm_articulo->usuario = 'almacen';
                    
                    if ($modelFar_adm_articulo->save()) {                        
                        Far_cab_registro_detalle::crearArticulo($modelFar_adm_articulo, $modelSucursales);
                    }
                }else{
                    Far_cab_registro_detalle::crearArticulo($modelProducto_adm, $modelSucursales);
                }

                $modelProducto = Far_articulo_sucursal::where([
                                        ['idfar_sucursales', '=', $modelSucursales->id],
                                        ['articulo_id', '=', $dato->res_articulo_id]
                                    ])
                                ->first();          
            } 

            Far_cab_registro_detalle::registrarDetalle($dato, $modelProducto,$query_Pedido,$modelFar_cabecera_registro);
            
        } 
    }

    public static function registrarDetalle($dato, $modelProducto,$query_Pedido,$modelFar_cabecera_registro)
    {

        $model = new Far_cab_registro_detalle;
        $model->detalle_movimiento_id = $dato->res_detalle_movimiento_id;
        $model->detalle_lote_id = $dato->res_detalle_lote_id;
        $model->idfar_cabecera_registro = $modelFar_cabecera_registro->id;
        $model->glosa = 'INGRESO POR NOTA NRO '.$query_Pedido->res_operacion;
        $model->idfar_articulo_sucursal = $modelProducto->id;
        $model->precio_uniario = $dato->res_precio_unitario;
        $model->importe_total = $dato->res_importe_total;
        $model->ingreso = $dato->res_cantidad;
        $model->salida = 0;
        $model->saldo = $modelProducto->saldo + $dato->res_cantidad;
        $model->saldo_total = $dato->res_saldo_lote;
        $model->hospitalizacion = 0;
        $model->lote = $dato->res_numero_lote;
        $model->fecha_vencimiento = $dato->res_fecha_lote; 
        $model->usuario = Auth::user()['name'];
        if($model->save())
        {
            if ($modelProducto != null) {
                $modelProducto->saldo = $modelProducto->saldo + $dato->res_cantidad;
                $modelProducto->save();
            } 
        }
    }

    public static function crearArticulo($modelFar_adm_articulo, $modelSucursales)
    {    
        $model = new Far_articulo_sucursal;
        $model->articulo_id = $modelFar_adm_articulo->articulo_id;      
        $model->idfar_adm_clasificacion = $modelFar_adm_articulo->clasificacion;      
        $model->codificacion_nacional = $modelFar_adm_articulo->codificacion_nacional;
        $model->codificacion = $modelFar_adm_articulo->codificacion;
        $model->descripcion = $modelFar_adm_articulo->descripcion;
        $model->codigo_barras = $modelFar_adm_articulo->codigo_barras; 
        $model->unidad = $modelFar_adm_articulo->unidad;
        $model->concentracion = $modelFar_adm_articulo->concentracion;   
        $model->tipo_envase = $modelFar_adm_articulo->tipo_envase;          
        $model->psicotropico = $modelFar_adm_articulo->psicotropico;      
        $model->estupefaciente = $modelFar_adm_articulo->estupefaciente;      
        $model->idfar_adm_clasificacion = $modelFar_adm_articulo->idfar_adm_clasificacion;      
        $model->idfar_estado = Far_estado::AUTORIZADO;     
        $model->cantidad_max_salidad = 0;
        $model->stock_minimo = 0;
        $model->stock_maximo = 0;
        $model->via = '';
        $model->indicacion = '';
        $model->saldo = 0;
        $model->dispensar_cuaderno = 1;
        $model->idfar_adm_articulo = $modelFar_adm_articulo->id;
        $model->idfar_sucursales = $modelSucursales->id; 
        $model->usuario = Auth::user()['name'];
        $model->save();
    }

    public static function eliminarProductoNota($idfar_cab_registro_detalle)
    {
        
        $model = Far_cab_registro_detalle:: find($idfar_cab_registro_detalle);
        if ($model->eliminado == false) {
            $modelProducto = Far_articulo_sucursal::find($model->idfar_articulo_sucursal);

            $modelCabecera = Far_cabecera_registro::find($model->idfar_cabecera_registro);

            $modelTipo_transaccion = Far_adm_tipotransaccion::find($modelCabecera->idfar_adm_tipotransaccion);
     
            $model->eliminado = true;
            
            if($model->save())
            {
                if($modelTipo_transaccion->tipo == Far_adm_tipotransaccion::INGRESO)
                {
                    // $modelProducto->preciocompra = str_replace(',', '', $gridItem[$i]->precio);
                    $modelProducto->saldo = $modelProducto->saldo - $model->ingreso;
                    $modelProducto->save();
                }
                else
                {
                    $modelProducto->saldo = $modelProducto->saldo + $model->salida;
                    $modelProducto->save();
                }
            } 
        } 
    }
}