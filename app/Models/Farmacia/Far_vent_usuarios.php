<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model; 
use Auth;

class Far_vent_usuarios extends Model
{ 

	protected $table = 'far_vent_usuarios'; 
	public $timestamps = false;  

	public static function agregarUsuario($ventanilla, $idUsuario)
    { 
        $model = new Far_vent_usuarios;    
        $model->idfar_ventanillas = $ventanilla; 
        $model->iddatosgenericos = $idUsuario;   
        $model->usuario = Auth::user()['name'];
        $model->save();
    }
}