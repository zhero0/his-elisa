<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \App\Models\Farmacia\Far_cab_receta_detalle;
use \App\Models\Farmacia\Far_articulo_sucursal;
use \App\Models\Farmacia\Far_cabecera_receta;
use \App\Models\Afiliacion\Hcl_diagnosticosCIE;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Administracion\Datosgenericos;
use \App\Models\Farmacia\Far_adm_estado;
use \App\Models\Almacen\Home;
use Elibyy\TCPDF\Facades\TCPDF;
use Auth;

class Far_cabecera_receta extends Model
{ 

	protected $table = 'far_cabecera_receta'; 
	public $timestamps = false;  

	public static function generarCodigo($idfar_sucursales_recepcionado)
	{
		$dato = DB::table('far_movimiento_kardex as a')
                ->join('far_adm_tipotransaccion as b', 'b.id', '=', 'a.idfar_adm_tipotransaccion')
                ->join('far_cabecera_receta as c', 'c.id', '=', 'a.idfar_cabecera_receta')
                ->select(DB::raw('case when max(c.folio) > 0 then max(c.folio)+1 else 1 end as total'))                
                ->where([
                        ['b.id', '=', $idfar_adm_tipotransaccion], 
                        // ['n.idfar_sucursales_recepcionado', '=', $idfar_sucursales_recepcionado],
                        ['a.fecha', '=', date("Y-m-d")]
                    ])
                ->first();
		return $dato->total;
	}

    public static function generarNumero($idfar_sucursales, $idfar_adm_tipotransaccion)
    {
        $numero = DB::table('far_movimiento_kardex as a')
                ->join('far_adm_tipotransaccion as b', 'b.id', '=', 'a.idfar_adm_tipotransaccion')
                ->join('far_cabecera_receta as c', 'c.id', '=', 'a.idfar_cabecera_receta')
                ->select(DB::raw('case when max(c.numero) > 0 then max(c.numero)+1 else 1 end as numero')) 
                ->where([
                                ['b.id', '=', $idfar_adm_tipotransaccion],
                                // ['idfar_sucursales_expedido', '=', $idfar_sucursales]
                            ])->max('numero');
        return $numero+1;
    }

	// public static function registraDocumentoproducto($modeloRegistro, $gridDocumentodetalle)
 //    {    	
 //        if(count($gridDocumentodetalle) > 0)
 //        {
 //        	$arrayDatos = Home::parametrosSistema();
 //            for($i = 0; $i < count($gridDocumentodetalle); $i++)
 //            {
 //             //    $modelCuaderno = Hcl_cuaderno::find($modeloRegistro->idhcl_cuaderno);
 //            	// $modelTransaccion = Inv_tipo_transaccion::find($modeloRegistro->idinv_tipo_transaccion);
 //            	// $modelAlmacen = Inv_almacenes::find($modeloRegistro->idinv_almacenes);
 //            	// $modelEstado = Inv_estado::find($modeloRegistro->idinv_estado);
 //            	// $modelMedico = Datosgenericos::find($modeloRegistro->iddatosgenericos);

 //                $model = new Far_cab_receta_detalle;
 //                $model->numero = $i+1;
 //                $model->idfar_articulo_sucursal = $gridDocumentodetalle[$i]->Idproducto;
 //                $model->cantidad = str_replace(',', '', $gridDocumentodetalle[$i]->cantidad);
 //                // $model->precio = 0; 
 //               // $model->fechadispensacion_detalle = date('d-m-Y');
 //                $model->idfar_cabecera_receta = $modeloRegistro->id;
 //                $model->idalmacen = $arrayDatos['idalmacen'];
 
                
 //        	    $model->usuario = Auth::user()['name'];
 //                $model->save();
 //            }
 //            Far_articulo_sucursal::actualizaSaldoProductos($gridDocumentodetalle);
 //        }
 //    }

    public static function registraDocumentoproducto($modeloRegistro, $gridDocumentodetalle) {
        $global_dispensado = 0;
        if (count($gridDocumentodetalle) > 0) {
            $arrayDatos = Home::parametrosSistema();
            //var_dump($arrayDatos);
            for ($i = 0; $i < count($gridDocumentodetalle); $i++) {
                //    $modelCuaderno = Hcl_cuaderno::find($modeloRegistro->idhcl_cuaderno);
                // $modelTransaccion = Inv_tipo_transaccion::find($modeloRegistro->idinv_tipo_transaccion);
                // $modelAlmacen = Inv_almacenes::find($modeloRegistro->idinv_almacenes);
                // $modelEstado = Inv_estado::find($modeloRegistro->idinv_estado);
                // $modelMedico = Datosgenericos::find($modeloRegistro->iddatosgenericos);

                $modelCuaderno = Hcl_cuaderno::find($modeloRegistro->idhcl_cuaderno);
                $modelTransaccion = Far_adm_tipotransaccion::find($modeloRegistro->idfar_adm_tipotransaccion);
                $modelAlmacen = Far_sucursales::find($modeloRegistro->idfar_sucursales_expedido);
                $modelEstado = Far_adm_estado::find($modeloRegistro->idfar_adm_estado);
                $modelMedico = Datosgenericos::find($modeloRegistro->iddatosgenericos);

                $model = new Far_cab_receta_detalle;
                $model->numero = $i+1;
                $model->cantidad = str_replace(',', '', $gridDocumentodetalle[$i]->cantidad);
                // $model->precio = 0; 
                // $model->fechadispensacion_detalle = date('d-m-Y');
                $model->idfar_articulo_sucursal = $gridDocumentodetalle[$i]->Idproducto;
                $model->codificacion_articulo = $gridDocumentodetalle[$i]->codigo;
                $model->descripcion_articulo = $gridDocumentodetalle[$i]->nombre;
                $model->indicacion_articulo = '';
                $model->idhcl_poblacion = $modeloRegistro->idhcl_poblacion;
                $model->idfar_cabecera_receta = $modeloRegistro->id;
                $model->iddatosgenericos = $modelMedico->id;
                $model->matricula_medico = $modelMedico->matricula;
                $model->hospitalizado = 0;
                $model->nombre_medico = $modelMedico->nombres.' '.$modelMedico->apellidos;
                $model->idfar_adm_tipotransaccion = $modeloRegistro->idfar_adm_tipotransaccion;
                $model->tipo_transaccion = $modelTransaccion->tipo_documento;
                $model->codigo_transaccion = $modelTransaccion->codigo;
                $model->descripcion_transaccion = $modelTransaccion->descripcion;
                $model->idfar_sucursales = $modeloRegistro->idfar_sucursales;
                $model->descripcion_almacen = $modelAlmacen->nombre;
                $model->idhcl_cuaderno = $modelCuaderno->id;
                $model->nombre_cuaderno = $modelCuaderno->nombre;
                $model->idfar_adm_estado = $modelEstado->id;
                $model->nombre_estado = $modelEstado->nombre;
                
                $model->usuario = Auth::user()['name'];


                // $model = new Far_cab_receta_detalle;
                // $model->numero = $i + 1;
                // $model->idfar_articulo_sucursal = $gridDocumentodetalle[$i]->Idproducto;
                // $model->cantidad = str_replace(',', '', $gridDocumentodetalle[$i]->cantidad);
                // // $model->precio = 0;
                // // $model->fechadispensacion_detalle = date('d-m-Y');
                // $model->idfar_cabecera_receta = $modeloRegistro->id;
                // $model->idfar_sucursales =  $modeloRegistro->idfar_sucursales_recepcionado;

                // $model->usuario = Auth::user()['name'];
                $model->save();
            }
            $global_dispensado = Far_articulo_sucursal::actualizaSaldoProductos($gridDocumentodetalle, $model->idfar_cabecera_receta);
            return $global_dispensado;
        }
    }

    public static function registraProgramacion_Prescripcion($arrayValor)
    {       
        $arrayPrescripciones = $arrayValor['arrayPrescripciones'];
        $modeloSalud = $arrayValor['model']; 

        $modelPoblacion = DB::table('hcl_poblacion as hp')
                          ->join('hcl_poblacion_afiliacion as ha', 'ha.idpoblacion', '=', 'hp.id') 
                          ->select(
                                'hp.id','hp.telefono','hp.documento','hp.domicilio','ha.idcobertura as cobertura', DB::raw("(SELECT date_part('year', age(hp.fecha_nacimiento)))::integer AS edad")
                            )->where([                                
                                ['hp.id', '=', $modeloSalud->idhcl_poblacion]
                            ])->first();

        $modelDiagnosticos = Hcl_diagnosticosCIE::where([
                                ['eliminado', '=', 0],
                                ['idhcl_cabecera_registro', '=', $modeloSalud->id]
                            ])->get();        

        $resultadoDiagnostico = '';
        foreach ($modelDiagnosticos as $dato) 
        {            
            $diagnostico = '('.$dato->codigo.') '.$dato->diagnostico; 
            $resultadoDiagnostico = $diagnostico.' '.$resultadoDiagnostico;        
        } 

        $modelCuaderno = DB::table('hcl_cuaderno as hc')
                            ->join('hcl_especialidad as he', 'hc.id_especialidad', '=', 'he.id')
                            ->join('far_adm_tipotransaccion as itt', 'he.idinv_tipo_transaccion', '=', 'itt.id')
                            ->join('far_sucursales as ia', 'hc.idfar_sucursales', '=', 'ia.id')
                            ->select(
                                'hc.id as idcuaderno','hc.nombre as nombre_cuaderno','hc.idalmacen as idalmacen','ia.id as idfar_sucursales','ia.nombre as nombre_almacen','ia.descripcion','itt.id as idfar_adm_tipotransaccion',
                                'itt.cantidad_medicamentos'
                            )
                            ->where([
                                ['hc.eliminado', '=', 0],
                                ['hc.id', '=', $modeloSalud->idhcl_cuaderno]
                            ])                            
                            ->first();

        $c = 0;
        $cantidad_medicamentos = $modelCuaderno->cantidad_medicamentos;
        $medicamentos_dispensados = count($arrayPrescripciones);
        $contador = round($medicamentos_dispensados/$cantidad_medicamentos);                    
        $hospitalizado = $modeloSalud->hospitalizado;
        if($contador > 0)
        {
            for($i = 0; $i < $contador; $i++)
            { 
                // $numero = Far_cabecera_receta::generarNumero($modelCuaderno->idfar_sucursales,$modelCuaderno->idfar_adm_tipotransaccion);
                $codigoBarra = date('y').''.date('m').''.date('d').''.$numero; 
                $modelo = new Far_cabecera_receta;         
                // $modelo->numero = $numero;
                // $modelo->folio = 0;               
                $modelo->iddatosgenericos = $modeloSalud->iddatosgenericos;
                $modelo->idhcl_cuaderno = $modeloSalud->idhcl_cuaderno;                
                $modelo->diagnostico = $resultadoDiagnostico;
                $modelo->hospitalizado = $hospitalizado;
                $modelo->emergencia = 0;
                $modelo->idhcl_poblacion = $modeloSalud->idhcl_poblacion;
                $modelo->idhcl_cobertura = $modelPoblacion->cobertura; 
                $modelo->hcl_poblacion_edad = $modelPoblacion->edad;
                $modelo->hcl_poblacion_telefono = $modelPoblacion->telefono;
                $modelo->hcl_poblacion_domicilio = $modelPoblacion->domicilio;
                $modelo->hcl_poblacion_documento = $modelPoblacion->documento;
                $modelo->fechaprogramacion_sistema = date('d-m-Y');
                $modelo->horaprogramacion_sistema = date('h:i A');
                $modelo->idhcl_cabecera_registro = $modeloSalud->id;  
                $modelo->usuario = Auth::user()['name']; 
                $modelo->save();
 
                Far_cabecera_receta::registraMedicamentos($c, $modelo, $arrayPrescripciones,$hospitalizado);
                $c += 1;
            }            
        }
    }

    public static function registraMedicamentos($i, $modeloRegistro, $gridDocumentodetalle,$hospitalizado)
    {
        if($i >= 0)
        {
            $modelCuaderno = Hcl_cuaderno::find($modeloRegistro->idhcl_cuaderno);
            $modelTransaccion = Far_adm_tipotransaccion::find($modeloRegistro->idfar_adm_tipotransaccion);
            $modelAlmacen = Far_sucursales::find($modeloRegistro->idfar_sucursales_expedido);
            $modelEstado = Far_adm_estado::find($modeloRegistro->idfar_adm_estado);
            $modelMedico = Datosgenericos::find($modeloRegistro->iddatosgenericos);

            $model = new Far_cab_receta_detalle;
            $model->numero = $i+1;
            $model->cantidad = str_replace(',', '', $gridDocumentodetalle[$i]->cantidad);
            // $model->precio = 0; 
            // $model->fechadispensacion_detalle = date('d-m-Y');
            $model->idfar_articulo_sucursal = $gridDocumentodetalle[$i]->Idproducto;
            $model->codificacion_articulo = $gridDocumentodetalle[$i]->codigo;
            $model->descripcion_articulo = $gridDocumentodetalle[$i]->nombre;
            $model->indicacion_articulo = $gridDocumentodetalle[$i]->indicacion;
            $model->idhcl_poblacion = $modeloRegistro->idhcl_poblacion;
            $model->idfar_cabecera_receta = $modeloRegistro->id;
            $model->iddatosgenericos = $modelMedico->id;
            $model->matricula_medico = $modelMedico->matricula;
            $model->hospitalizado = $hospitalizado;
            $model->nombre_medico = $modelMedico->nombres.' '.$modelMedico->apellidos;
            $model->idfar_adm_tipotransaccion = $modeloRegistro->idfar_adm_tipotransaccion;
            $model->tipo_transaccion = $modelTransaccion->tipo_documento;
            $model->codigo_transaccion = $modelTransaccion->codigo;
            $model->descripcion_transaccion = $modelTransaccion->descripcion;
            $model->idfar_sucursales = $modeloRegistro->idfar_sucursales;
            $model->descripcion_almacen = $modelAlmacen->nombre;
            $model->idhcl_cuaderno = $modelCuaderno->id;
            $model->nombre_cuaderno = $modelCuaderno->nombre;
            $model->idfar_adm_estado = $modelEstado->id;
            $model->nombre_estado = $modelEstado->nombre;
            
            $model->usuario = Auth::user()['name'];
            $model->save();  
        }
    }

    public static function imprimeProgramacion($modelInventario)
    { 
        $imprimirDirecto = 1; 
            // $medidas = array(200, 600); // Ajustar aqui segun los milimetros necesarios;
            // $pdf = new TCPDF('P', 'mm', $medidas, true, 'UTF-8', false); 

            ob_end_clean();            

            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // $pdf = new TCPDF('L', PDF_UNIT, 'A1', true, 'UTF-8', false);
            
            $pdf::SetTitle("Farmacia");
            $pdf::SetSubject("TCPDF");

            $pdf::SetCreator('PDF_CREATOR');
            $pdf::SetAuthor('usuario');
            $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
            
            //cambiar margenes
            $pdf::SetMargins(5, 0, 0, 0);
            // $pdf::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf::SetFooterMargin(PDF_MARGIN_FOOTER); 

            //set auto page breaks
            $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            //$pdf::setAutoPageBreak(true);

            $pdf::startPageGroup();
            
            //set image scale factor
            $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf::setJPEGQuality(100);   

            $pdf::SetAutoPageBreak(TRUE, 0);
            

            $usuario = Auth::user()['name'];
            $fecha = date('d-m-Y');

            
            $pdf::SetFillColor(255, 255, 255);

            $y_Inicio = 10;
            $height = 4;
            $width = 190;
            $widthLabel = 21;
            $widthDosPuntos = 4;
            $widthInformacion = 40;
            $espacio = 3;
            $tipoLetra = 'helvetica';
            $tamanio = 9;
            $widthTitulo = 50;  

        foreach ($modelInventario as $dato) 
        { 
            $pdf::AddPage();
            $pdf::SetY($y_Inicio);  

            $cantidad_medicamentos = Far_adm_tipotransaccion::find($dato->idfar_adm_tipotransaccion);  
            $numeroImprimir = $dato->numero;
            $codbarra = $dato->cod_barra;
            
            $medicamentos = Far_cab_receta_detalle::where([
                                    ['eliminado', '=', 0],
                                    ['idfar_cabecera_receta', '=', $dato->id],                                
                                ])->get();   

            $pdf::SetFillColor(230, 230, 230);  
             $pdf::SetFont('helvetica', 'B', 11);
            $pdf::MultiCell(37, 8, 'FARMACIA C.N.S.', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('helvetica', '', 11);
            $pdf::MultiCell(90, 8, $cantidad_medicamentos->descripcion, 0, 'R', '', 1, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('helvetica', 'B', 15);
            $pdf::MultiCell(65, 8, 'RECETA MÉDICA', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('helvetica', 'B', 14);
            $pdf::MultiCell(30, 8, 'FOLIO N°', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::MultiCell(30, 8, '', 1, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(38, 8, '', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('courier', '', 13);
            $pdf::MultiCell(65, 8, 'Clave Farmacéutico', 0, 'L', '', 1, '', '', 1, '', '', '', 8, 'M');
            $pdf::Line(100, 40, 130, 40); 
  
            // ************************************************************************************************
            $pdf::SetFont('courier', '', 9); 
            $pdf::SetFont($tipoLetra, 'B', $tamanio);
            $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());
            $pdf::SetFont($tipoLetra, '', $tamanio);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);
            
            $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO ','B', 'L', 1, 0, '', '', true, 0, false, '', $height, 50);

            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(50, $height, $dato->hcl_poblacion_matricula, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(25, $height, 'DOCUMENTO ',0, 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $dato->documento, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            $pdf::MultiCell($widthLabel, $height, 'PACIENTE ', 0, 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(70, $height, $dato->hcl_poblacion_nombrecompleto, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(13, $height, 'EDAD ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $dato->hcl_poblacion_edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            $pdf::MultiCell($widthLabel, $height, 'EMPRESA ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(100, $height,$dato->hcl_empleador_patronal.' - '.$dato->hcl_empleador_empresa, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);
 
                // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
                $modelDatosgenericos = Datosgenericos::find($dato->iddatosgenericos);
                $dato->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
 
                $modelHcl_cuaderno = Hcl_cuaderno::find($dato->idhcl_cuaderno);
                $dato->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';
                $pdf::SetFont($tipoLetra, '', $tamanio);
                
                $label_Hospitalizado = 'HOSPITALIZADO';
                $label_Establecimiento = 'Establecimiento';
                $label_Medico = 'MÉDICO SOL.';
                $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
                $y_Tipo = ceil($pdf::GetStringWidth($label_Establecimiento, $tipoLetra, '', $tamanio, false));
                $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
                $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico);

                $label_Fecha = 'FECHA';
                $label_Especialidad = 'ESPECIALIDAD';
                $label_Diagnostico = 'DIAGNÓSTICO';
                $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
                $y_Especialidad = ceil($pdf::GetStringWidth($label_Establecimiento, $tipoLetra, '', $tamanio, false));
                $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));                
                $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);

                $hospitalizado = $dato->hospitalizado == 1? "SI" : "NO";
                $fechaprogramacion = date('d-m-Y', strtotime($dato->fechaprogramacion_sistema));
                $pdf::MultiCell($y_label_maximo_col1, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(15, $height, $hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(15, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(30, $height, $fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(13, $height, 'HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(20, $height, $dato->horaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

                $pdf::MultiCell($y_label_maximo_col1, $height, $label_Establecimiento, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(80, $height, $medicamentos[0]->descripcion_almacen, 0, 'L', 1, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

                $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(35, $height, $dato->cuaderno, 0, 'L', 1, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

                $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(48, $height, $modelDatosgenericos->matricula.' - '.$dato->personal, 0, 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

                
                $pdf::MultiCell($y_label_maximo_columna2, $height,  '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($espacio, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(45, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 

                $pdf::SetFont($tipoLetra, 'B', $tamanio); 

                $pdf::MultiCell(26, $height, 'CODIGO ', 0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10);
                $pdf::MultiCell(26, $height, 'VADEMECUM', 0, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
                $pdf::MultiCell(75, $height, 'MEDICAMENTO ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(30, $height, 'CANTIDAD', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());
                $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

                $pdf::SetFont('courier', '', 9);
              
// ------------------------------------------ [MEDICAMENTOS] ------------------------------------------ 
                $heightCol = 5;
                for($j = 0; $j < count($medicamentos); $j++)
                { 
                    $pdf::SetFont('courier', '', 11);
                    $linecount =  $pdf::getNumLines($medicamentos[$j]->descripcion_articulo, 75);  
                    if ($linecount==1) { 
                       $heightCol = 5; 
                    }
                    if ($linecount == 2) { 
                        $heightCol = 10; 
                    }
                    if ($linecount == 3) { 
                       $heightCol = 13; 
                    }
                    if ($linecount == 4) { 
                       $heightCol = 16; 
                    }
                    $medicamento = $medicamentos[$j]->descripcion_articulo != null? $medicamentos[$j]->descripcion_articulo : '';
                    $unidad = $medicamentos[$j]->unidad_articulo != null? $medicamentos[$j]->unidad_articulo : '';
                    $pdf::MultiCell(30, $heightCol, $medicamentos[$j]->codificacion_articulo, 'B', 'C', 0, 0, '', '', true, 0, false, true, 20, 10);
                    $pdf::SetFont('courier', '', 10);
                    $pdf::MultiCell(75, $heightCol, $medicamento.' '.$unidad ,'B', 'C', 0, 0, '', '', true, 0, false, true, 20, 10);
                    $pdf::SetFont('courier', '', 11);
                    $pdf::MultiCell(20, $heightCol, $medicamentos[$j]->cantidad, 'B', 'C', 0, 1, '', '', true, 0, false, true, 20, 10);
                } 
// ------------------------------ [FIN MEDICAMENTOS] ---------------------------------------               
  
            $style = array(
                        'position' => 'L',
                        'align' => 'L',
                        'stretch' => false,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'hpadding' => 'auto',
                        'vpadding' => 'auto',
                        'fgcolor' => array(0,0,0),
                        'bgcolor' => false, //array(255,255,255),
                        'text' => true,
                        'font' => 'helvetica',
                        'fontsize' => 8,
                        'stretchtext' => 4
                    ); 
            
            if (Far_adm_tipotransaccion::TIPO_DOSIS_UNITARIA == $cantidad_medicamentos->id ) {
                $pdf::write2DBarcode($codbarra.'-'.$dato->hcl_poblacion_matricula.'-'.$dato->hcl_poblacion_nombrecompleto.'-'.$dato->hcl_empleador_patronal.'-'.$dato->hcl_empleador_empresa, 'QRCODE,L', 10, 170, 25, 25, $style, 'N');
                $pdf::MultiCell('', $height,'', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());

                $pdf::SetFont('courier', '', 9);
                $pdf::SetY(180);
                $pdf::MultiCell(50, '', '', 0, 'C', 0, 0); 
                $pdf::MultiCell(35, 20, 'FIRMA Y SELLO ENFERMERA', 'T', 'C', 0, 0, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(35, 20, 'FIRMA y SELLO MEDICO', 'T', 'C', 0, 1, '', '', true, 0, false, true, 20, 10);      
            }
            else
            {
                $pdf::write2DBarcode($codbarra.'-'.$dato->hcl_poblacion_matricula.'-'.$dato->hcl_poblacion_nombrecompleto.'-'.$dato->hcl_empleador_patronal.'-'.$dato->hcl_empleador_empresa, 'QRCODE,L', 10, 145, 25, 25, $style, 'N');
                $pdf::MultiCell('', $height,'', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());

                $pdf::SetFont('courier', '', 9);
                $pdf::SetY(160);
                $pdf::MultiCell(50, '', '', 0, 'C', 0, 0); 
                $pdf::MultiCell(35, $height, 'FIRMA MEDICO', 'T', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(35, $height, 'SELLO MEDICO', 'T', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);          

                $distancia_medicamentos = 175;
                $pdf::SetY($distancia_medicamentos);
                $pdf::MultiCell(45, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell($widthLabel, $height, 'PACIENTE: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(70, $height, $dato->hcl_poblacion_nombrecompleto, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(14, $height, $label_Fecha.': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                // $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(19, $height, $fechaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                for($j = 0; $j < count($medicamentos); $j++)
                {  
                    $pdf::MultiCell('', $height,'Modo de Uso: '.$medicamentos[$j]->codificacion_articulo.' - '.$medicamentos[$j]->descripcion_articulo.' - '.$medicamentos[$j]->indicacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);  
                }
                $pdf::MultiCell('', $height,'', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 
            }
            

            // $pdf::SetY(180);
            // $pdf::SetFont('courier', '', 9);
            // $pdf::MultiCell(125, $height, 'ANCHO EJEMPLO FIN', 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);    
            // // ************************************************************************************************        

            // Muestra el panel para imprimir de forma directa...
            
        } 
        // return;
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("resultadoProgramacion.pdf", "I");
        
        mb_internal_encoding('utf-8');
    }
}