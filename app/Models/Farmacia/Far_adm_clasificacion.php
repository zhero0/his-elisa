<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;

class Far_adm_clasificacion extends Model
{ 

	protected $table = 'far_adm_clasificacion'; 
	public $timestamps = false; 

	const MEDICAMENTOS = 1;
	const INSUMOS = 2;

}