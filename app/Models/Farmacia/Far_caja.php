<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;

class Far_caja extends Model
{
	protected $table = 'far_caja'; 
	public $timestamps = false;   
}