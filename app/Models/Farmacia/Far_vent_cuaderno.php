<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model; 
use Auth;

class Far_vent_cuaderno extends Model
{ 

	protected $table = 'far_vent_cuaderno'; 
	public $timestamps = false;  

	public static function agregarCuaderno($ventanilla, $idCuaderno)
    { 
        $model = new Far_vent_cuaderno;    
        $model->idfar_ventanillas = $ventanilla; 
        $model->idhcl_cuaderno = $idCuaderno;   
        $model->usuario = Auth::user()['name'];
        $model->save();
    }
}