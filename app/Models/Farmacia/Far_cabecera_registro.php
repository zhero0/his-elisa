<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \App\Models\Farmacia\Far_adm_tipotransaccion;
use \App\Models\Farmacia\Far_cab_registro_detalle;
use \App\Models\Almacen\Home;
use Auth;

class Far_cabecera_registro extends Model
{ 
	protected $table = 'far_cabecera_registro'; 
	public $timestamps = false;

	public static function generarCodigo($idfar_adm_tipotransaccion)
	{
		$dato = DB::table('far_cabecera_registro as n')
                ->select(DB::raw('case when max(n.numero_nota) > 0 then max(n.numero_nota)+1 else 1 end as total'))
                ->join('far_adm_tipotransaccion as td', 'td.id', '=', 'n.idfar_adm_tipotransaccion')
                ->where([
                        ['td.id', '=', $idfar_adm_tipotransaccion], 
                        // ['producto.idalmacen', '=', session('sessionIdalmacen')]
                    ])
                ->get();
		return $dato[0]->total;
	}

    public static function listaPedidos($popcion, $palmacen_id, $pmovimiento_id)
    {              
        $query = DB::connection('serv_almacen')                
                    ->table(DB::raw("alm_lista_pedidos_farmacia('".$popcion."','".$palmacen_id."','".$pmovimiento_id."')"))
                    ->paginate(config('app.pagination'));
            return $query;
    }

    public static function listaPedido_farmacia($popcion, $palmacen_id, $pmovimiento_id)
    {              
        $query = DB::connection('serv_almacen')                
                    ->table(DB::raw("alm_lista_pedidos_farmacia('".$popcion."','".$palmacen_id."','".$pmovimiento_id."')"))
                    ->first();
            return $query;
    }

    public static function listaPedido_medicamentos($pmovimiento_id)
    {              
        $query = DB::connection('serv_almacen')                
                    ->table(DB::raw("alm_lista_pedidos_detalle('".$pmovimiento_id."')"))
                    ->get();
            return $query;
    }

	public static function registrarNota_ingreso($idfar_sucursales, $res_movimiento)
	{
        $arrayDatos = Home::parametrosSistema();         
         
        $modelSucursales = Far_sucursales::where('id', '=', $idfar_sucursales)->first();

        $query_Articulos = Far_cabecera_registro::listaPedido_medicamentos($res_movimiento);

        $query_Pedido = Far_cabecera_registro::listaPedido_farmacia(1,0,$res_movimiento);

		$model = new Far_cabecera_registro;
        $model->numero_nota = $query_Pedido->res_numero_nota;
        $model->movimiento_id = $query_Pedido->res_movimiento_id;        
        $model->descripcion = $query_Pedido->res_descripcion; 
        $model->operacion = $query_Pedido->res_operacion; 
        $model->total = '0';
        $model->idfar_adm_tipotransaccion = 1;
        $model->idfar_adm_clasificacion = 1;
        $model->tipo = 1;        
        $model->tipo_documento = 11;
        $model->idfar_estado = $query_Pedido->res_estado;
        $model->idfar_proveedores = 1;
        $model->idfar_sucursales = $modelSucursales->id;
        
        $model->usuario = Auth::user()['name'];
        
        if($model->save())
    	{
            Far_cab_registro_detalle::registraProductoNota($modelSucursales, $query_Pedido, $query_Articulos, $model);
            $mensaje = config('app.mensajeGuardado');
        }
        else
        	$mensaje = config('app.mensajeErrorGuardado');
        
        return $mensaje;
	}
    
 
}