<?php
namespace App\Models\Farmacia;

use Illuminate\Database\Eloquent\Model;

class Far_adm_tipotransaccion extends Model
{ 

	protected $table = 'far_adm_tipotransaccion';   

	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	const INGRESO = 1;
	const EGRESO = 2;
	const DISPENSACION = 3;

	const TIPO_EXTERNA = 5;				//"EXTERNA";"CONSULTA EXTERNA FORM. DM-301"
	const TIPO_INTERNACION = 6;			//"INTERNACION";"INTERNACION FORM. DM-303"
	const TIPO_DOSIS_UNITARIA = 7;		//"DOSIS UNITARIA";"DOSIS UNITARIA FORM. DM-303-A" 
}
 