<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_tamano_placas extends Model
{ 
	protected $table = 'ima_tamano_placas'; 
	public $timestamps = false;
}