<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_examenes extends Model
{
	protected $table = 'ima_examenes'; 
	public $timestamps = false;

	public static function generarNumero($idalmacen)
	{
		$numero = Lab_examenes::where('id_almacen', '=', $idalmacen)->max('numero');
		return $numero+1;
	}	
}