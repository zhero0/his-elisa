<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Ima_resultados_documento extends Model
{
    protected $table = 'ima_resultados_documento';
    public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
    
    public static function registraArchivos($arrayParametro)
    {
        $files = $arrayParametro['files'];
        $id = $arrayParametro['id'];
        
        foreach ($files as $file)
        {
            $name = $file->getClientOriginalName();
            $nombre = $id.'_'.$name;
            $file->move(config('app.rutaArchivosImagenologia'), $nombre);
            
            $modelo = new Ima_resultados_documento();
            $modelo->archivo = $nombre;
            $modelo->idima_resultados_programacion = $id;
            $modelo->usuario = Auth::user()['name'];
            $modelo->save();
        }
    }
}