<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_plantillas extends Model
{
	protected $table = 'ima_plantillas';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('nombre',"like","%$dato%");
									// ->orwhere('usuario','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Ima_plantillas::paginate(config('app.pagination'));	
		}		
	}
}