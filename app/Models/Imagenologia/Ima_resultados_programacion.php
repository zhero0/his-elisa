<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Imagenologia\Ima_estado;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;

use Illuminate\Support\Facades\DB;
use Auth;

use \App\Models\Administracion\Datosgenericos;
use \App\Models\Administracion\Impresion;
use Elibyy\TCPDF\Facades\TCPDF;

class Ima_resultados_programacion extends Model
{ 
    protected $table = 'ima_resultados_programacion'; 
    public $timestamps = false;
    
    public static function generarNumero($idalmacen)
    {
        $numero = Ima_resultados_programacion::where('idalmacen', '=', $idalmacen)->max('numero');
        return $numero+1;
    }

    public static function registra($arrayDatos)
    {
        $request = $arrayDatos['request'];
        $model = $arrayDatos['model'];
        $arrayExamenes = $arrayDatos['arrayExamenes'];
        
        $array_especialidad = Ima_examenes_registroprogramacion::registraExamenRegistroprogramacion($arrayDatos, $arrayExamenes);
        if($array_especialidad != null)
        {
            $array_especialidad = array_unique($array_especialidad);
            foreach ($array_especialidad as $valor)
            {
                $modelo = new Ima_resultados_programacion();
                $modelo->idalmacen = null;
                $modelo->iddatosgenericos = null;
                $modelo->idima_registroprogramacion = $model->id;
                $modelo->idima_especialidad = $valor;
                $modelo->usuario = Auth::user()['name'];
                $modelo->idima_estado = Ima_estado::PROGRAMACION;
                
                // [ DATOS DEL PACIENTE ]
                $Hcl_poblacion = Hcl_poblacion::muestraDatosPoblacion($request);
                $modelo->hcl_poblacion_nombrecompleto = $Hcl_poblacion['hcl_poblacion_nombrecompleto'];
                $modelo->hcl_poblacion_matricula = $Hcl_poblacion['hcl_poblacion_matricula'];
                $modelo->hcl_poblacion_cod = $Hcl_poblacion['hcl_poblacion_cod'];
                $modelo->hcl_poblacion_sexo = $Hcl_poblacion['hcl_poblacion_sexo'];
                $modelo->hcl_poblacion_fechanacimiento = $Hcl_poblacion['hcl_poblacion_fechanacimiento'];
                $modelo->hcl_empleador_empresa = $Hcl_poblacion['hcl_empleador_empresa'];
                $modelo->hcl_empleador_patronal = $Hcl_poblacion['hcl_empleador_patronal'];
                
                $modelo->patologico = 0;
                // $model->producto = Venta::concatenaProductos($gridDocumentodetalle);
                $modelo->save();
            }
            Ima_resultados_programacion::seteaNombreExamen($model->id);
        }
    }

    public static function seteaNombreExamen($idima_registroprogramacion)
    {
        $modelo = Ima_resultados_programacion::where([
                        ['eliminado', '=', 0],
                        ['idima_registroprogramacion', '=', $idima_registroprogramacion]
                    ])->get();
        for($i = 0; $i < count($modelo); $i++)
        {
            $detalle_examenes = DB::table('ima_examenes_registroprogramacion as erp')
                        ->join('ima_examenes as le', 'le.id', '=', 'erp.idima_examenes')
                        ->join('ima_especialidad as e', 'le.idima_especialidad', '=', 'e.id')
                        ->select(
                            'le.id as idlab_examenes', 'le.codigo', 'le.nombre',
                            'e.id as idEspecialidad', 'e.codigo as codigoEspecialidad', 'e.nombre as nombreEspecialidad'
                        )
                        ->where([
                            ['le.eliminado', '=', 0],
                            ['erp.eliminado', '=', 0],
                            ['erp.idima_registroprogramacion', '=', $idima_registroprogramacion],
                            ['e.id', '=', $modelo[$i]->idima_especialidad]
                        ])
                        ->orderBy('le.nombre', 'ASC')
                        ->get();
            $texto = '';
            for($j = 0; $j < count($detalle_examenes); $j++)
            {
                if($j == count($detalle_examenes)-1)
                    $texto .= $detalle_examenes[$j]->nombre;
                else
                    $texto .= $detalle_examenes[$j]->nombre.', ';
            }
            
            // Actualiza el campo de "examenes_programados"
            $model = Ima_resultados_programacion::find($modelo[$i]->id);
            $model->examenes_programados = $texto;
            $model->save();
        }
    }

    public static function imprimeProgramacionResultado($id)
    {
        $valor = explode('*|*', $id);
        $id = $valor[0];
        $imprimirDirecto = $valor[1];
        
        $modelIma_resultados_programacion = Ima_resultados_programacion::find($id);

        $modelIma_especialidad = Ima_especialidad::find($modelIma_resultados_programacion->idima_especialidad);
        $model = Ima_registroprogramacion::find($modelIma_resultados_programacion->idima_registroprogramacion);
        $numeroImprimir = $model->numero;

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            $pdf->SetFont('courier', 'B', 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            $x = $pdf->getPageWidth();
            $y = $pdf->getPageHeight();
            $pdf->MultiCell(130, 10, 'IMAGENOLOGIA C.N.S.', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
            $pdf->MultiCell(18, 10, '', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
            $pdf->MultiCell(40, 10, 'N°:'.$numeroImprimir , 0, 'R', '', 1, '', '', 1, '', '', '', 10, 'M');

            // $pdf->MultiCell(140, 12, 'C.N.S.', 0, 'R', '', 1, '', '', 1, '', '', '', 12, 'M');

            $pdf->Line(31, $pdf->getY()+6, $pdf->getPageWidth()-12, $pdf->getY()+6);
        });
        
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 30;
        $height = 5;
        $width = 190;
        $widthLabel = 21;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 100;

        $pdf::SetY($y_Inicio);
        // ------------------------------------------ [PACIENTE] ------------------------------------------
        $modelhcl_poblacion = Hcl_poblacion::where('id', '=', $model->idhcl_poblacion)
                                ->select('hcl_poblacion.*', 
                                        DB::raw("(SELECT date_part('year', age(fecha_nacimiento)))::integer AS edad"))
                                ->first();
        
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell($widthInformacion, $height, 'DATOS PROGRAMACIÓN', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);

        $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(25, $height, $modelhcl_poblacion->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(19, $height, 'NOMBRES: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($widthInformacion, $height, $modelhcl_poblacion->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(21, $height, 'APELLIDOS: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($widthInformacion, $height, $modelhcl_poblacion->primer_apellido.' '.$modelhcl_poblacion->segundo_apellido, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(12, $height, 'EDAD: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(10, $height, $modelhcl_poblacion->edad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        
        // [ PACS ]
        $modelIma_pacs = Ima_pacs::buscarIdPacs($modelIma_resultados_programacion->idpacs);

        // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
        // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
        $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);
        $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
        $modelHcl_especialidad = Hcl_especialidad::find($model->idhcl_especialidad);
        $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
        $modelHcl_cuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
        $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';

        $pdf::MultiCell($widthTitulo, 7, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 7, 10);
        // $pdf::SetFont($tipoLetra, 'B', $tamanio);
        // $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        
        $label_Hospitalizado = 'HOSPITALIZADO';
        $label_Tipo = 'TIPO';
        $label_Medico = 'MÉDICO SOL.';
        $label_numeroPlaca = 'NRO PLACA';
        $label_Observacion = 'OBSERVACIÓN';
        $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
        $y_Tipo = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
        $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
        $y_numeroPlaca = ceil($pdf::GetStringWidth($label_numeroPlaca, $tipoLetra, '', $tamanio, false));
        $y_Observacion = ceil($pdf::GetStringWidth($label_Observacion, $tipoLetra, '', $tamanio, false));
        $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico,  $y_Observacion);

        $label_Fecha = 'FECHA';
        $label_Especialidad = 'ESPECIALIDAD';
        $label_Diagnostico = 'DIAGNÓSTICO';
        $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
        $y_Especialidad = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
        $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
        $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);

        $hospitalizado = $model->hospitalizado == 1? "SI" : "NO";
        $fechaprogramacion = date('d-m-Y', strtotime($model->fechaprogramacion));
        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(48, $height, $hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, $fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(13, $height, 'HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, $model->horaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Tipo, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(48, $height, $model->especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, $model->cuaderno, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(48, $height, $model->personal, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($y_label_maximo_col1, $height, $label_numeroPlaca, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, $modelIma_resultados_programacion->numero_placa, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $numeroDicom = $modelIma_pacs == null? '' :$modelIma_pacs->codigo; 
        $pdf::MultiCell(13, $height, 'DICOM', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, $numeroDicom, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Diagnostico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, $model->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Observacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(120, $height, $model->observacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::Line(10, $pdf::getY(), $pdf::getPageWidth()-15, $pdf::getY());
        // ------------------------------------------ [RESULTADO] ------------------------------------------
        $modelDatosgenericos_Respuesta = Datosgenericos::find($modelIma_resultados_programacion->iddatosgenericos);
        $personal_Medico = $modelDatosgenericos_Respuesta != ''? $modelDatosgenericos_Respuesta->nombres.' '.$modelDatosgenericos_Respuesta->apellidos : '';
        $pdf::MultiCell(120, $height, 'RESULTADO:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // print_r($modelIma_especialidad->impresion);
        // return;
        $inferior = 0;

        if ($modelIma_especialidad->impresion == Impresion::MEDIA_HOJA) {
            $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 67, 20);
             $inferior = 143;
        }
        else if($modelIma_especialidad->impresion == Impresion::CARTA) {
            $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 260, 20);

             //$inferior = 267;
        //     print_r($modelIma_especialidad->impresion);
        // return;
        }

        $pdf::setY($pdf::getY()+90);


        // // ************************ [footer] *******************************************
        // $pdf::SetY($inferior);
        // $pdf::SetFont('courier', 'B', 7);
        // $pdf::MultiCell('', '','Usuario: '.$usuario, 'T', 'L', 0);
        // // $pdf::SetY(-10);
        // $pdf::SetY($inferior);
        // $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
        // // $pdf::MultiCell(60, '', 'Fecha: '. date("d").'-'.date("m").'-'.date("Y")  , 0, 'L', 0, 1, 60);
        // // $pdf::SetY(-10);
        // $pdf::SetY($inferior);
        // $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
        // // ****************************************************************************
        

        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("resultadoProgramacion.pdf", "I");

        mb_internal_encoding('utf-8');
    
    }

}