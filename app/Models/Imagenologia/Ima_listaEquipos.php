<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_listaEquipos extends Model
{ 
	
	const MR = 'Resonancia Magnetica';
	const DR = 'Radiografia Digital';
	const CR = 'Radiologia Computarizada';
	const TC = 'Tomografia Computarizada';
	const PET = 'Tomografía por emisión de positrones';
	const CT = 'Tomografia Computarizada';
	const RMN = 'Resonancia magnética nuclear ';
	const US = 'Ecografia Computarizada';
	const TC = 'Tomografia Computarizada';
	const TC = 'Tomografia Computarizada';
}