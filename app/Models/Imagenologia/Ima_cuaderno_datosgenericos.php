<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_cuaderno_datosgenericos extends Model
{
	protected $table = 'ima_cuaderno_datosgenericos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
}