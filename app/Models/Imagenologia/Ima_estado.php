<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_estado extends Model
{
	protected $table = 'ima_estado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
		
	const SOLICITUD = 1;
	const PROGRAMACION = 2;
	const ANULADO = 3;
	const ENTREGADO = 4;
}