<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_tipo_placas extends Model
{ 
	protected $table = 'ima_tipo_placas'; 
	public $timestamps = false;
}