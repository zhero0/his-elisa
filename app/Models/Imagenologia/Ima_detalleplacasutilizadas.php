<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;

class Ima_detalleplacasutilizadas extends Model
{
	protected $table = 'ima_detalleplacasutilizadas';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
}