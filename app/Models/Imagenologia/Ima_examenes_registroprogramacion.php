<?php
namespace App\Models\Imagenologia;

use Illuminate\Database\Eloquent\Model;
use Auth;

use \App\Models\Imagenologia\Ima_examenes;

class Ima_examenes_registroprogramacion extends Model
{
	protected $table = 'ima_examenes_registroprogramacion'; 
	public $timestamps = false;
	
	public static function registraExamenRegistroprogramacion($arrayDatos, $arrayExamenes)
	{
		$request = $arrayDatos['request'];
		$modelo = $arrayDatos['model'];
		$array_especialidad = [];
		
		for($i = 0; $i < count($arrayExamenes); $i++)
        {
        	$model = new Ima_examenes_registroprogramacion;
        	$model->idima_registroprogramacion = $modelo->id;
        	$model->idima_examenes = $arrayExamenes[$i];
        	$model->usuario = Auth::user()['name'];
        	if($model->save())
			{
	        	$modelIma_examenes = Ima_examenes::where('id', '=', $arrayExamenes[$i])->first();
	            array_push($array_especialidad, $modelIma_examenes->idima_especialidad);
            }
        }
        return $array_especialidad;
	}
	
}