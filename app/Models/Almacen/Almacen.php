<?php

namespace App\Models\Almacen;

use Illuminate\Database\Eloquent\Model; 
class Almacen extends Model
{ 
	protected $table = 'almacen'; 
	public $timestamps = false; 
	// protected $fillable = array('id', 'codigo', 'nombre','direccion','descripcion','usuario','fecha','eliminado');
	// protected $hidden = ['created_at','updated_at']; 	 
}

