<?php
namespace App\Models\Almacen;

use Illuminate\Database\Eloquent\Model;

class Generico extends Model
{
	protected $table = 'generico';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	const PERSONAL = 1;
	const CONF_SISTEMA = 2;

	const MEDICO = 1;
	const LICENCIADO = 2;
	const ADMINISTRADOR = 3;
}