<?php
namespace App\Models\Almacen;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \App\Models\Almacen\Almacen;
use Auth;

use \App\Models\Usuario\Almacenusuario;
use \App\Models\Usuario\Menu;
use \App\Models\Usuario\Menuopcion;

class Home extends Model
{
	public static function parametrosSistema()
	{
        $modelAlmacenusuario = Almacenusuario::where('idusuario', '=', Auth::user()['id'])->first();
        if($modelAlmacenusuario != null)
        {
            $idalmacen = $modelAlmacenusuario->idalmacen;
            $idusuario = $modelAlmacenusuario->idusuario;
            $iddatosgenericos = $modelAlmacenusuario->iddatosgenericos;
        }
        else
        {
            $idalmacen = 0;
            $idusuario = 0;
            $iddatosgenericos = 0;
        }

        if($idalmacen > 0)
            $nombreAlmacen = Almacen::where('id', '=', $idalmacen)->first()->nombre;
        else
            $nombreAlmacen = '';
        
        $total_tareas_realizadas =  intval(0);
        $total_notificaciones = intval(0);

        // -------------------------------------- [OPCIONES MENU] --------------------------------------
        if(session()->exists('sessionMenu'))
        {
            $modelMenu = session()->get('sessionMenu');
            $modelMenuopcion = session()->get('sessionMenuopcion');
        }
        else
        {
            $modelMenu = Menu::whereRaw('
                        id in(
                            select mo.idmenu
                            from permission_role pr inner join permissions p on pr.permission_id = p.id
                                                    inner join menuopcion mo on p.idmenuopcion = mo.id
                            where role_id in(select role_id
                                                    from role_user
                                                    where user_id = '.$idusuario.')
                            group by mo.idmenu
                        )
                    ')->orderBy('orden', 'ASC')->get()->toArray();
            $modelMenuopcion = Menuopcion::whereRaw('
                        id in(
                            select p.idmenuopcion
                            from permission_role pr inner join permissions p on pr.permission_id = p.id
                                                    inner join menuopcion mo on p.idmenuopcion = mo.id
                            where role_id in(select role_id
                                                    from role_user
                                                    where user_id = '.$idusuario.')
                            group by idmenuopcion
                            )
                        ')->orderBy('orden', 'ASC')->get()->toArray();
            session([
                'sessionMenu' => $modelMenu,
                'sessionMenuopcion' => $modelMenuopcion
            ]);
        } 
        // ---------------------------------------------------------------------------------------------

        $arrayDatos = array(
            'idalmacen' => $idalmacen,
            'iddatosgenericos' => $iddatosgenericos,
            'modelMenu' => $modelMenu,
            'modelMenuopcion' => $modelMenuopcion,
            
            'nombreAlmacen' => $nombreAlmacen,
            'total_tareas_realizadas' => $total_tareas_realizadas,
            'total_notificaciones' => $total_notificaciones,
        );
        return $arrayDatos;
	}

}

