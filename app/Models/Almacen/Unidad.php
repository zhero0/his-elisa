<?php

namespace App\Models\Almacen;

use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
	protected $table = 'unidad'; 
	protected $fillable = array('id','nombre','simbolo', 'usuario','fecha','eliminado');
	protected $hidden = ['created_at','updated_at']; 
	
}
