<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Med_lista_cargos extends Model
{ 
	protected $table = 'med_lista_cargos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}