<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Med_tipo_cancelacion extends Model
{ 
	protected $table = 'med_tipo_cancelacion';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	//---------------------------------------------
  	// CONSTANTES
  	const EFECTIVO = 1;
  	const DEPOSITO = 2;
  	const TRANSFERENCIA = 3;
}