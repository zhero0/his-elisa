<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Hsi_actividad_economica_agrupacion extends Model
{ 
	protected $table = 'hsi_actividad_economica_agrupacion';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}