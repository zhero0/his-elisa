<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Med_clasificacion extends Model
{ 
	protected $table = 'med_clasificacion';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}