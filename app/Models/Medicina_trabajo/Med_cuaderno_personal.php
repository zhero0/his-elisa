<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Med_cuaderno_personal extends Model
{ 
	protected $table = 'med_cuaderno_personal';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}