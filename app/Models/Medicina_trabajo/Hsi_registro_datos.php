<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Hsi_registro_datos extends Model
{ 
	protected $table = 'hsi_registro_datos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}