<?php
namespace App\Models\Medicina_trabajo;

use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;
use Auth;

use \App\Models\Almacen\Home;
use \App\Models\Almacen\Almacen;
use \App\Models\Medicina_trabajo\Med_cifrasEnLetras; 
use \App\Models\Medicina_trabajo\Med_programacion;
use \App\Models\Medicina_trabajo\Med_datos; 
use \App\Models\Medicina_trabajo\Med_lista_cargos;
use \App\Models\Medicina_trabajo\Med_consolidado;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Imagenologia\Ima_especialidad;

class Med_reporte
{
    const opc_reporteSolicitud = 1;
    const opc_reporteProgramacion = 2;
    const opc_tipoPlaca = 5;
    const opc_ticket = 3;

    private static function mostrarHeaderFooter($arrayParametroPDF) {
        $pdf = $arrayParametroPDF['pdf'];
        $modelAlmacen = $arrayParametroPDF['modelAlmacen'];
        $mostrarEncabezado = $arrayParametroPDF['mostrarEncabezado'];
        $mostrarPie = $arrayParametroPDF['mostrarPie'];
        $tituloReporte = $arrayParametroPDF['tituloReporte'];
        $logo = $arrayParametroPDF['logo'];
        $sello = $arrayParametroPDF['sello'];

        $pdf::SetTitle($tituloReporte);
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        // $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetMargins(10, 10, 10, 10);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        if($mostrarEncabezado == 1)
        {
            $pdf::setHeaderCallback(function($pdf) use($modelAlmacen, $tituloReporte, $logo) {
                $y = $pdf->GetY();
                $pdf->SetFillColor(230, 230, 230);

                $pdf->SetY($y + 1);
                
                if($logo != '')
                    $pdf->Image('imagenes/reporte/'.$logo, 10, '', '', 15);

                $pdf->SetFont('courier', 'B', 14);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetY($pdf->GetY());
                $titulo = $tituloReporte.' '.$modelAlmacen->empresa_nombre;
                $pdf->MultiCell('', 7, $titulo, 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');

                $sumarAlto = 0;
                if($modelAlmacen->nombre != '') {
                    $sumarAlto = 0;
                    $pdf->MultiCell('', 7, '[ '.$modelAlmacen->nombre.' ]', 0, 'C', '', 1, '', '', 1, '', '', '', 7, 'M');    
                }
                else
                    $sumarAlto = 3;
                
                if($modelAlmacen->direccion != '') {
                    $sumarAlto += 0;
                    $pdf->SetFont('courier', '', 10);
                    $pdf->MultiCell('', 5, 'Dir: '.$modelAlmacen->direccion, 0, 'C', '', 1, '', '', 1, '', '', '', 5, 'M');
                }
                else
                    $sumarAlto += 1;

                if($modelAlmacen->sucursal == '' && $modelAlmacen->direccion == '')
                    $sumarAlto = 8;

                $pdf->Line(10, $pdf->getY() + $sumarAlto, $pdf->getPageWidth()-10, $pdf->getY() + $sumarAlto);
            });
        }

        if($mostrarPie == 1)
        {
            $usuario = Auth::user()->name;
            $fecha = date('d-m-Y');
            $pdf::setFooterCallback(function($pdf) use($usuario, $fecha, $sello) {
                if($sello == 1)
                {
                    $pdf->SetY(-38);
                    $pdf->SetFont('helvetica', '', 9);
                    $pdf->MultiCell(50, 5,'', 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
                    $pdf->MultiCell(50, 5, 'Firma y sello Enfermera', 'T', 'C', 0, 0);

                    $pdf->MultiCell(10, 5, '', 0, 'C', 0, 0);            
                    $pdf->MultiCell(50, 5, 'Firma y sello Médico', 'T', 'C', 0, 1);
                }

                $valor = -15; 
                $pdf->SetFont('courier', 'B', 7);
                $pdf->SetY($valor);
                $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
                $pdf->SetY($valor);
                $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'C', 0, 1, 60);
                $pdf->SetY($valor);
                $pdf->MultiCell(40, '', 'Página '.$pdf->getGroupPageNo().' de '.$pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
            });
        }
        $pdf::SetFillColor(255, 255, 255);
    }
    private static function nombrePDF_Descarga($pdf, $nombreArchivo) {
        // set javascript
        // $pdf->IncludeJS('print(true);');

        $pdf::Output($nombreArchivo.".pdf", "I");
        mb_internal_encoding('utf-8');
    }

    
    // Detalle de Placas Utilizadas
    private static function encabezadoPlacasUtilizadas($arrayValores) {
        $pdf = $arrayValores['pdf'];
        $height = $arrayValores['height'];
        
        $width_numero = $arrayValores['width_numero'];
        $width_nombreApellido = $arrayValores['width_nombreApellido'];
        $width_matricula = $arrayValores['width_matricula'];
        $width_cod = $arrayValores['width_cod'];
        $width_sexo = $arrayValores['width_sexo'];
        $width_edad = $arrayValores['width_edad'];
        $width_empresa = $arrayValores['width_empresa'];
        $width_patronal = $arrayValores['width_patronal'];
        $width_placaSolicitada = $arrayValores['width_placaSolicitada'];
        $width_tipoPlaca = $arrayValores['width_tipoPlaca'];
        $width_tamanio_placa = $arrayValores['width_tamanio_placa'];
        $width_cantidad_placas = $arrayValores['width_cantidad_placas']; // aumentado...
        $width_medicoSolicitante = $arrayValores['width_medicoSolicitante'];

        $tipoLetra = $arrayValores['tipoLetra'];
        $tamanio_subTitulo = $arrayValores['tamanio_subTitulo'];
        $tamanio = $arrayValores['tamanio'];
        $medico_responsable = $arrayValores['medico_responsable'];


        $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
        $pdf::MultiCell(40, $height, 'NOMBRE COMPLETO: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio_subTitulo);
        $pdf::MultiCell(100, $height, $medico_responsable, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        
        $pdf::MultiCell($width_numero, $height, 'Nº', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_nombreApellido, $height, 'Nombre y Apellido', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_matricula, $height, 'Matrícula', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_cod, $height, 'Cod.', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_sexo, $height, 'Sexo', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_edad, $height, 'Edad', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_empresa, $height, 'Empresa', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_patronal, $height, 'Patronal', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        
        $pdf::MultiCell($width_placaSolicitada, $height, 'Placa Solicitada', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_tipoPlaca, $height, 'Tipo Placa', 'TB', 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_medicoSolicitante, $height, 'Médico Solic.', 'TB', 'L', 1, 1, '', '', true, 0, false, true, $height);
    }
    private static function y_hoja($pdf, $y_dato, $height) {
        $valor1 = $pdf::getY() + $y_dato + $height + 31;   // para que no desborde la pagina
        $valor2 = $pdf::getPageHeight() - 6;
        return $valor1.'*'.$valor2;
    }
    // [ REPORTE: "Detalle de Placas Utilizadas" ]
    public static function imprime_placasUtilizadas_vertical($parametro) {
        $parametroEntrada = $parametro->datosParametro;
        for($i = 0; $i < count($parametroEntrada); $i++)
        {
            eval("$".$parametroEntrada[$i]->name." = '".$parametroEntrada[$i]->value."';");
        }
        $fechaDesde = $fechaDesde == ''? date('d-m-Y') : $fechaDesde;
        $fechaHasta = $fechaHasta == ''? date('d-m-Y') : $fechaHasta;
        $rangoFecha = 'Fecha Desde: '.$fechaDesde.' Hasta '.$fechaHasta;

        $modelAlmacen = Almacen::where('id', '=', $parametro->idalmacen)->first();
        
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $arrayParametroPDF = [
            'pdf' => $pdf,
            'modelAlmacen' => $modelAlmacen,
            'mostrarEncabezado' => 1,
            'mostrarPie' => 1,
            'tituloReporte' => 'PLACAS UTILIZADAS',
            'logo' => '',
            'sello' => 0,
        ];
        Ima_reporte::mostrarHeaderFooter($arrayParametroPDF);

        $y_Inicio = 30;//28;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 3;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $tamanio_subTitulo = 10;
        $widthTitulo = 100;
        
        $pdf::AddPage('P', 'Legal');
        $pdf::SetY($y_Inicio);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $anchoHoja = $pdf::getPageWidth();


        $width_numero = 10;
        $width_nombreApellido = 63;
        $width_matricula = 20;
        $width_cod = 9;
        $width_sexo = 10;
        $width_edad = 10;
        $width_patronal = 23;
        $width_empresa = 45;
        $width_placaSolicitada = 40;//35;
        $width_tipoPlaca = 35;
        $width_tamanio_placa = 25;
        $width_cantidad_placas = 10;
        $width_medicoSolicitante = 50;
        $width_examenes_programados = 60;
        $medicoResponsable = '';

        $modelo = DB::table(DB::raw("ima_listaplacas_reporte_func(".
                                $parametro->idalmacen.",".
                                $idhcl_especialidad.",".
                                $idima_especialidad.",".
                                $iddatosgenericos.",".
                                "'".$fechaDesde."', ".
                                "'".$fechaHasta."')"
                            ))->get();
        
        $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
        $pdf::MultiCell(100, $height, '', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(95, $height, 'ESPECIALIDAD: ECOGRAFIA', 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(100, $height, '', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);


        // ENCABEZADO TITULO
        $pdf::SetFont($tipoLetra, '', $tamanio_subTitulo);
        $pdf::MultiCell(80, $height, 'MÉDICO: FELIX SANCHEZ ARGUELLAS', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        

        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        // 2do Encabezado
        $pdf::MultiCell($width_numero, $height, 'Nº', 'LT', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_nombreApellido, $height, 'Nombre y Apellido', 'T', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_matricula, $height, 'Matrícula', 'T', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_cod, $height, 'Cod.', 'T', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_sexo, $height, 'Sexo', 'T', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_edad, $height, 'Edad', 'T', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_patronal, $height, 'Patronal', 'T', 'L', 1, 0, '', '', true, 0, false, true, $height);
        // $pdf::MultiCell($width_tipoPlaca, $height, 'Tipo Placa', 'TB', 'C', 1, 0, '', '', true, 0, false, true, $height);
        // $pdf::MultiCell($width_medicoSolicitante, $height, 'Médico Solic.', 'TB', 'L', 1, 1, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_medicoSolicitante, $height, 'Médico Solic.', 'TR', 'L', 1, 1, '', '', true, 0, false, true, $height);


        // 1er Encabezado
        // $pdf::MultiCell(75, $height, '', 1, 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell(/*$width_empresa*/59, $height, 'Empresa', 'LB', 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell(/*$width_placaSolicitada*/60, $height, 'Placa Solicitada', 'B', 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell(/*$width_tipoPlaca*/40, $height, 'Tipo Placa', 'B', 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell(26, $height, 'Tamaño Placa', 'B', 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell(10, $height, 'Cant.', 'BR', 'R', 1, 1, '', '', true, 0, false, true, $height);
        $pdf::SetFont($tipoLetra, '', $tamanio);


        for($i = 0; $i < count($modelo); $i++)
        {
            

            $numero = $modelo[$i]->numero_placa;
            $hcl_poblacion_nombrecompleto = $modelo[$i]->hcl_poblacion_nombrecompleto;
            $matricula = $modelo[$i]->matricula;
            $cod = $modelo[$i]->cod;
            $sexo = $modelo[$i]->sexo;
            $edad = $modelo[$i]->edad;
            $empresa = $modelo[$i]->empresa;
            $patronal = $modelo[$i]->patronal;
            $medico_solicitante = $modelo[$i]->medico_solicitante;
            $medico_responsable = $modelo[$i]->medico_responsable;
            $examenes_programados = $modelo[$i]->examenes_programados;
            

            $arrayValores = array(
                'pdf' => $pdf,
                'height' => $height,

                'width_numero' => $width_numero,
                'width_nombreApellido' => $width_nombreApellido,
                'width_matricula' => $width_matricula,
                'width_cod' => $width_cod,
                'width_sexo' => $width_sexo,
                'width_edad' => $width_edad,
                'width_empresa' => $width_empresa,
                'width_patronal' => $width_patronal,
                'width_placaSolicitada' => $width_placaSolicitada,
                'width_tipoPlaca' => $width_tipoPlaca,
                'width_tamanio_placa' => $width_tamanio_placa,
                'width_cantidad_placas' => $width_cantidad_placas,
                'width_medicoSolicitante' => $width_medicoSolicitante,

                'tipoLetra' => $tipoLetra,
                'tamanio_subTitulo' => $tamanio_subTitulo,
                'tamanio' => $tamanio,
                'medico_responsable' => $medico_responsable,
            );
            
            // if($medico_responsable != $medicoResponsable)
            // {
            //     if($i > 0)
            //     {
            //         $pdf::AddPage('L', 'A4');
            //         $pdf::SetY($y_Inicio);

            //         $medicoResponsable = $medico_responsable;
            //         Ima_reporte::encabezadoPlacasUtilizadas($arrayValores);
            //     }
            //     else
            //     {
            //         $medicoResponsable = $medico_responsable;
            //         Ima_reporte::encabezadoPlacasUtilizadas($arrayValores);
            //     }
            // }
            
            $y_numero = ceil($pdf::getStringHeight($width_numero, trim($numero), $reseth = true, $autopadding = true, $border = 0));
            $y_hcl_poblacion_nombrecompleto = ceil($pdf::getStringHeight($width_nombreApellido, trim($hcl_poblacion_nombrecompleto), $reseth = true, $autopadding = true, $border = 0));
            $y_matricula = ceil($pdf::getStringHeight($width_matricula, trim($matricula), $reseth = true, $autopadding = true, $border = 0));
            $y_cod = ceil($pdf::getStringHeight($width_cod, trim($cod), $reseth = true, $autopadding = true, $border = 0));
            $y_sexo = ceil($pdf::getStringHeight($width_sexo, trim($sexo), $reseth = true, $autopadding = true, $border = 0));
            $y_edad = ceil($pdf::getStringHeight($width_edad, trim($edad), $reseth = true, $autopadding = true, $border = 0));
            $y_patronal = ceil($pdf::getStringHeight($width_patronal, trim($patronal), $reseth = true, $autopadding = true, $border = 0));
            $y_medico_solicitante = ceil($pdf::getStringHeight($width_medicoSolicitante, trim($medico_solicitante), $reseth = true, $autopadding = true, $border = 0));

            $y_dato = max($y_numero, $y_hcl_poblacion_nombrecompleto, $y_matricula, $y_cod, $y_sexo, $y_edad, 
                          $y_patronal, $y_medico_solicitante);

            // 1ra FILA
            $pdf::MultiCell($width_numero, $y_dato, $numero, 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_nombreApellido, $y_dato, $hcl_poblacion_nombrecompleto, 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_matricula, $y_dato, $matricula, 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_cod, $y_dato, $cod, 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_sexo, $y_dato, $sexo, 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_edad, $y_dato, $edad, 'B', 'C', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_patronal, $y_dato, $patronal, 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_medicoSolicitante, $y_dato, $medico_solicitante, 'B', 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);

            // // PLACAS UTILIZADAS
            // $anchos = $width_numero + $width_nombreApellido + $width_matricula + $width_cod + $width_sexo + 
            //             $width_edad + $width_empresa + $width_patronal;
            $datos = json_decode(json_decode($modelo[$i]->datos));
            for($d = 0; $d < count($datos); $d++)
            {
                
                $width_empresa = 59;
                $width_examenes_programados = 60;

                $y_empresa = ceil($pdf::getStringHeight($width_empresa, trim($empresa), $reseth = true, $autopadding = true, $border = 0));
                $y_examenes_programados = ceil($pdf::getStringHeight($width_examenes_programados, trim($examenes_programados), $reseth = true, $autopadding = true, $border = 0));

                $y_dato = max($y_empresa, $y_examenes_programados);

                if($d == 0)
                {
                    // 2da FILA
                    $pdf::MultiCell($width_empresa, $y_dato, $empresa, 'LB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    $pdf::MultiCell($width_examenes_programados, $y_dato, $examenes_programados, 'LB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    // $pdf::MultiCell(65, $y_dato, $examenes_programados, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    // $raya = 'TB';
                }
                else
                {
                    $anchos = $width_empresa + $width_examenes_programados;
                    $pdf::MultiCell($anchos, $y_dato, '', 'B', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                }

                $pdf::SetFont($tipoLetra, 'B', $tamanio);
                $pdf::MultiCell(40, $y_dato, $datos[$d]->ima_tipo_placas, 1, 'C', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::MultiCell(26, $y_dato, $datos[$d]->ima_tamano_placas, 1, 'C', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::MultiCell(10, $y_dato, $datos[$d]->cantidad, 1, 'R', 0, 1, '', '', true, 0, false, true, $y_dato, 10);
                $pdf::SetFont($tipoLetra, '', $tamanio);
            }
        }
        
        $nombreArchivo = "placasUtilizadas ".$fechaDesde." al ".$fechaHasta;
        Ima_reporte::nombrePDF_Descarga($pdf, $nombreArchivo);
    }
    private static function headerData($arrayValores) {
        $pdf = $arrayValores['pdf'];
        $height = $arrayValores['height'];

        $tipoLetra = $arrayValores['tipoLetra'];
        $tamanio = $arrayValores['tamanio'];
        $width_numero = $arrayValores['width_numero'];
        $width_nombreApellido = $arrayValores['width_nombreApellido'];
        $width_matricula = $arrayValores['width_matricula'];
        $width_cod = $arrayValores['width_cod'];
        $width_sexo = $arrayValores['width_sexo'];
        $width_edad = $arrayValores['width_edad'];
        $width_empresa = $arrayValores['width_empresa'];
        $width_patronal = $arrayValores['width_patronal'];
        $width_placaSolicitada = $arrayValores['width_placaSolicitada'];
        $width_tipoPlaca = $arrayValores['width_tipoPlaca'];

        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell($width_numero, $height, 'Nº', 'LTB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_nombreApellido, $height, 'Nombre y Apellido', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_matricula, $height, 'Matrícula', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_cod, $height, 'Cod.', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_sexo, $height, 'Sexo', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_edad, $height, 'Edad', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_empresa, $height, 'Empresa', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_patronal, $height, 'Patronal', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        
        $pdf::MultiCell($width_placaSolicitada, $height, 'Placa Solicitada', 'TB', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($width_tipoPlaca, $height, 'TIPO PLACA', 'TB', 'C', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell(45, $height, 'Médico Solic.', 'TBR', 'L', 1, 1, '', '', true, 0, false, true, $height);
    }
    public static function imprime_placasUtilizadas_horizontal($parametro) {
        $parametroEntrada = $parametro->datosParametro;
        for($i = 0; $i < count($parametroEntrada); $i++)
        {
            eval("$".$parametroEntrada[$i]->name." = '".$parametroEntrada[$i]->value."';");
        }
        $fechaDesde = $fechaDesde == ''? date('d-m-Y') : $fechaDesde;
        $fechaHasta = $fechaHasta == ''? date('d-m-Y') : $fechaHasta;
        $rangoFecha = 'Fecha Desde: '.$fechaDesde.' Hasta '.$fechaHasta;

        $modelAlmacen = Almacen::where('id', '=', $parametro->idalmacen)->first();
        
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $arrayParametroPDF = [
            'pdf' => $pdf,
            'modelAlmacen' => $modelAlmacen,
            'mostrarEncabezado' => 0,
            'mostrarPie' => 0,
            'tituloReporte' => 'PLACAS UTILIZADAS',
            'logo' => '',
            'sello' => 0,
        ];
        Ima_reporte::mostrarHeaderFooter($arrayParametroPDF);

        $pdf::setHeaderCallback(function($pdf) use($rangoFecha, $modelAlmacen) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);

            $pdf->SetY($y + 1);
            $pdf->SetTextColor(0, 0, 0);
            
            $height = 6;
            $width = 90;

            // [ fila 1 ]
            $datoCentro = $modelAlmacen->empresa_nombre.' '.$modelAlmacen->nombre;
            $alto = ($height+5);
            $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(20, $height, '', 0, 'R', '', 0, '', '', 1, '', '', '', $height, 'M');
            $pdf->MultiCell($width, $alto, $datoCentro, 0, 'L', '', 0, '', '', 1, '', '', '', $alto, 'M');

            $pdf->SetFont('courier', 'B', 16);
            $pdf->MultiCell(160, $alto, 'DETALLE DE PLACAS UTILIZADAS', 0, 'L', '', 1, '', '', 1, '', '', '', $alto, 'M');
            

            // [ fila 2]
            $pdf->SetFont('helvetica', 'B', 9);
            $pdf->MultiCell(20, $height, '', 0, 'R', '', 0, '', '', 1, '', '', '', $height, 'M');
            $pdf->MultiCell($width, $height, 'SERVICIO DE IMAGENOLOGIA', 0, 'L', '', 0, '', '', 1, '', '', '', $height, 'M');

            $pdf->SetFont('helvetica', '', 9);
            $pdf->MultiCell(160, $height, $rangoFecha, 0, 'L', '', 1, '', '', 1, '', '', '', $height, 'M');

            $pdf->Line(10, $pdf->getY(), $pdf->getPageWidth()-5, $pdf->getY());
        });

        $usuario = Auth::user()->name;
        $fecha = date('d-m-Y');
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha) {
            $pdf->SetY(-4);
            $pdf->SetFont('courier', 'B', 7);
            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-4);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-4);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });


        $y_Inicio = 28;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $widthDosPuntos = 3;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $tamanio_subTitulo = 10;
        $widthTitulo = 100;
        
        // $pdf::AddPage('P', 'Legal');
        $pdf::AddPage('L', 'A4');
        $pdf::SetY($y_Inicio);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $anchoHoja = $pdf::getPageWidth();


        $width_numero = 10;
        $width_nombreApellido = 50;
        $width_matricula = 20;
        $width_cod = 8;
        $width_sexo = 9;
        $width_edad = 9;
        $width_empresa = 45;
        $width_patronal = 20;
        $width_placaSolicitada = 30;//35;
        $width_tipoPlaca = 35;
        $width_tamanio_placa = 25;
        $width_cantidad_placas = 10;
        $width_medicoSolicitante = 50;
        $medicoResponsable = '';
        
        $modelo = DB::table(DB::raw("ima_listaplacas_reporte_func(".
                                $parametro->idalmacen.",".
                                $idhcl_especialidad.",".
                                $idima_especialidad.",".
                                $iddatosgenericos.",".
                                "'".date('Y-m-d', strtotime($fechaDesde))."', ".
                                "'".date('Y-m-d', strtotime($fechaHasta))."')"
                            ))->get();
        $modelIma_especialidad = Ima_especialidad::where('id', '=', $idima_especialidad)->first();
        $modelMedicos = DB::table('hcl_medicos_view')->where('id', '=', $iddatosgenericos)->first();
        
        $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
        $pdf::MultiCell(185, $height, '', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(95, $height, 'ESPECIALIDAD: '.$modelIma_especialidad->nombre, 1, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(100, $height, '', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);


        // ENCABEZADO TITULO
        $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
        $pdf::MultiCell(100, $height, 'MÉDICO RESP.: '.$modelMedicos->nombres, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $arrayValores = [
            'pdf' => $pdf,
            'height' => $height,

            'width_numero' => $width_numero,
            'width_nombreApellido' => $width_nombreApellido,
            'width_matricula' => $width_matricula,
            'width_cod' => $width_cod,
            'width_sexo' => $width_sexo,
            'width_edad' => $width_edad,
            'width_empresa' => $width_empresa,
            'width_patronal' => $width_patronal,
            'width_placaSolicitada' => $width_placaSolicitada,
            'width_tipoPlaca' => $width_tipoPlaca,
            'width_tamanio_placa' => $width_tamanio_placa,
            'width_cantidad_placas' => $width_cantidad_placas,
            'width_medicoSolicitante' => $width_medicoSolicitante,

            'tipoLetra' => $tipoLetra,
            'tamanio_subTitulo' => $tamanio_subTitulo,
            'tamanio' => $tamanio,
        ];
        Ima_reporte::headerData($arrayValores);

        for($i = 0; $i < count($modelo); $i++)
        {
            $numero = $modelo[$i]->numero_placa;
            $hcl_poblacion_nombrecompleto = $modelo[$i]->hcl_poblacion_nombrecompleto;
            $matricula = $modelo[$i]->matricula;
            $cod = $modelo[$i]->cod;
            $sexo = $modelo[$i]->sexo;
            $edad = $modelo[$i]->edad;
            $empresa = $modelo[$i]->empresa;
            $patronal = $modelo[$i]->patronal;
            $medico_solicitante = $modelo[$i]->medico_solicitante;
            $medico_responsable = $modelo[$i]->medico_responsable;
            $examenes_programados = $modelo[$i]->examenes_programados;
            
            $pdf::SetFont($tipoLetra, '', $tamanio_subTitulo);
            $y_numero = ceil($pdf::getStringHeight($width_numero, trim($numero), $reseth = true, $autopadding = true, $border = 0));
            $y_hcl_poblacion_nombrecompleto = ceil($pdf::getStringHeight($width_nombreApellido, trim($hcl_poblacion_nombrecompleto), $reseth = true, $autopadding = true, $border = 0));
            $y_matricula = ceil($pdf::getStringHeight($width_matricula, trim($matricula), $reseth = true, $autopadding = true, $border = 0));
            $y_cod = ceil($pdf::getStringHeight($width_cod, trim($cod), $reseth = true, $autopadding = true, $border = 0));
            $y_sexo = ceil($pdf::getStringHeight($width_sexo, trim($sexo), $reseth = true, $autopadding = true, $border = 0));
            $y_edad = ceil($pdf::getStringHeight($width_edad, trim($edad), $reseth = true, $autopadding = true, $border = 0));
            $y_empresa = ceil($pdf::getStringHeight($width_empresa, trim($empresa), $reseth = true, $autopadding = true, $border = 0));
            $y_patronal = ceil($pdf::getStringHeight($width_patronal, trim($patronal), $reseth = true, $autopadding = true, $border = 0));
            $y_medico_solicitante = ceil($pdf::getStringHeight($width_medicoSolicitante, trim($medico_solicitante), $reseth = true, $autopadding = true, $border = 0));
             
            $y_dato = max($y_numero, $y_hcl_poblacion_nombrecompleto, $y_matricula, $y_cod, $y_sexo, $y_edad, $y_empresa, 
                          $y_patronal, $y_medico_solicitante);

            $valor1 = $pdf::getY() + $y_dato + $height + $height + $height;
            $valor2 = $pdf::getPageHeight() - $y_dato;
            if($valor1 > $valor2)
            {
                $pdf::AddPage('L', 'A4');
                $pdf::SetY($y_Inicio);
                Ima_reporte::headerData($arrayValores);
            }

            $pdf::MultiCell($width_numero, $y_dato, $numero, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_nombreApellido, $y_dato, $hcl_poblacion_nombrecompleto, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_matricula, $y_dato, $matricula, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_cod, $y_dato, $cod, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_sexo, $y_dato, $sexo, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_edad, $y_dato, $edad, 'TB', 'C', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_empresa, $y_dato, $empresa, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell($width_patronal, $y_dato, $patronal, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell(60, $y_dato, $examenes_programados, 'TB', 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
            $pdf::MultiCell(50, $y_dato, $medico_solicitante, 1, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);

            // PLACAS UTILIZADAS
            $anchos = $width_numero + $width_nombreApellido + $width_matricula + $width_cod + $width_sexo + 
                        $width_edad + $width_empresa + $width_patronal+25;
            if($modelo[$i]->datos != '')
            {
                $datos = json_decode(json_decode($modelo[$i]->datos));
                for($d = 0; $d < count($datos); $d++)
                {
                    $pdf::SetFont($tipoLetra, 'B', $tamanio_subTitulo);
                    $width_tamanoPlacas = ceil($pdf::GetStringWidth($datos[$d]->ima_tamano_placas)) + 4;
                    $pdf::MultiCell($anchos, $height, '', 0, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    $pdf::MultiCell(45, $height, $datos[$d]->ima_tipo_placas, 1, 'R', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    $pdf::MultiCell(30, $height, $datos[$d]->ima_tamano_placas, 1, 'L', 0, 0, '', '', true, 0, false, true, $y_dato, 10);
                    $pdf::MultiCell(10, $height, $datos[$d]->cantidad, 1, 'L', 0, 1, '', '', true, 0, false, true, $y_dato, 10);
                }
            }
        }

        $ancho_1 = 35;
        $ancho_2 = 22;
        $y_dato = ceil($pdf::getStringHeight($ancho_1, 0, $reseth = true, $autopadding = true, $border = 0));
        $valor1 = $pdf::getY() + $y_dato + $height + $height + $height;
        $valor2 = $pdf::getPageHeight() - $y_dato - $height; 

        if($valor1 > $valor2)
        {
            $pdf::AddPage('L', 'A4');
            $pdf::SetY($y_Inicio);
        }
        $masculino = $modelo->where('sexo','=','MAS')->count();
        $femenino = $modelo->where('sexo','=','FEM')->count(); 
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(40, $height, 'RESUMEN DE TOTALES', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio_subTitulo);
        $pdf::MultiCell($ancho_1, $height, 'HOMBRES', 'LTBR', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($ancho_2, $height,  $masculino, 'TBR', 'C', 1, 1, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($ancho_1, $height,'MUJERES', 'LTBR', 'L', 1, 0, '', '', true, 0, false, true, $height);
        $pdf::MultiCell($ancho_2, $height,  $femenino, 'TBR', 'C', 1, 1, '', '', true, 0, false, true, $height); 

        
        $nombreArchivo = "placasUtilizadas ".$fechaDesde." al ".$fechaHasta;
        Ima_reporte::nombrePDF_Descarga($pdf, $nombreArchivo);
    }


    // [ REPORTE: "Tipo Placa" ]
    // public static function imprime_tipoPlaca($parametro) {
    //     $parametroEntrada = $parametro->datosParametro; 
    //     $pdf::SetCreator('PDF_CREATOR');

    //     for($i = 0; $i < count($parametroEntrada); $i++)
    //     {
    //         eval("$".$parametroEntrada[$i]->name." = '".$parametroEntrada[$i]->value."';");
    //     } 
    //     $fechaDesde = strtotime($fechaDesde); 
    //     $fechaHasta = strtotime($fechaHasta); 
    //     $fechaDesde = $fechaDesde == ''? date('Y-m-d') : date('Y-m-d',$fechaDesde);
    //     $fechaHasta = $fechaHasta == ''? date('Y-m-d') : date('Y-m-d',$fechaHasta);
        

    //     $arrayDatos = Home::parametrosSistema();
    //     $modelAlmacen = Almacen::where('id', '=', $arrayDatos['idalmacen'])->first();
    //     $modelEspecialidad = Ima_especialidad::where('id', '=', $idima_especialidad)->first();
         
    //     ob_end_clean();
    //     $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
    //     $pdf::SetTitle("Tipo de Placas");
        
    //     $pdf::setHeaderCallback(function($pdf)use($modelAlmacen,$modelEspecialidad) {
    //         $pdf->SetY(5);
    //         $y = $pdf->GetY();
    //         $pdf->SetFillColor(230, 230, 230);
            
    //         $pdf->SetY($y + 1);
    //         $pdf->SetTextColor(0, 0, 0);
    //         $pdf->SetY($pdf->GetY());

    //         $pdf->SetFont('helvetica', '', 9);
    //         // Fila 1
    //         $pdf->MultiCell(55, 5, $modelAlmacen->empresa_nombre.' * '.$modelAlmacen->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, 10, 10);
    //         $pdf->SetFont('courier', 'B', 15);
    //         $pdf->MultiCell(82, 5, 'TIPO DE PLACAS', 0, 'C', 0, 1, '', '', true, 1, false, true, 10, 10);

    //         $pdf->SetFont('helvetica', '', 9);
    //         $pdf->MultiCell('', 5, 'SERVICIO DE '.$modelEspecialidad->nombre, 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 10);
    //         $pdf->Line(10, 18, $pdf->getPageWidth()-12, 18);
    //     });
 
    //     $y_Inicio = 30;
    //     $height = 5;
    //     $width = 190;
    //     $widthLabel = 28;
    //     $espacio = 3;
    //     $tipoLetra = 'helvetica';
    //     $tamanio = 9;
        
    //     $pdf::AddPage('P', 'Legal');
    //     $pdf::SetY($y_Inicio); 

    //     $model = DB::table(DB::raw("ima_placasutilizadas_reporte_func(".$arrayDatos['idalmacen'].",".$tipoPlaca.",".$idima_especialidad.",
    //         '".$fechaDesde."','".$fechaHasta."')"))
    //                 ->select('idima_tipo_placas', 'ima_tipo_placas')
    //                 ->groupBy('idima_tipo_placas', 'ima_tipo_placas')
    //                 ->orderBy('ima_tipo_placas', 'ASC')
    //                 ->get();
        
    //     $pdf::SetFont($tipoLetra, 'B', $tamanio);
    //     $pdf::MultiCell(40, $height, 'TIPO PLACA', 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
    //     $pdf::MultiCell(60, $height, 'TAMAÑO PLACA', 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
    //     $pdf::MultiCell(20, $height, 'TOTAL', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
    //     $pdf::SetFont($tipoLetra, '', $tamanio);

    //     for($i = 0; $i < count($model); $i++)
    //     {
    //         $pdf::MultiCell(100, $height, $model[$i]->ima_tipo_placas, 'LB', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
    //         $pdf::MultiCell(20, $height, '', 'BR', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

    //         $modelo = DB::table(DB::raw("ima_placasutilizadas_reporte_func(".$arrayDatos['idalmacen'].",".$tipoPlaca.",".$idima_especialidad.",'".$fechaDesde."','".$fechaHasta."')"))
    //                 ->select('ima_tamano_placas', DB::raw('sum(cantidad) as total'))
    //                 ->where('idima_tipo_placas', '=', $model[$i]->idima_tipo_placas)
    //                 ->groupBy('ima_tamano_placas')
    //                 ->orderBy('ima_tamano_placas', 'ASC')
    //                 ->get();
    //         for($j = 0; $j < count($modelo); $j++)
    //         {
    //             $pdf::MultiCell(40, $height, '', 'LB', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
    //             $pdf::MultiCell(60, $height, $modelo[$j]->ima_tamano_placas, 'B', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
    //             $pdf::MultiCell(20, $height, $modelo[$j]->total, 'BR', 'R', 0, 1, '', '', true, 0, false, true, $height, 10);
    //         }
    //     }
        
    //     $nombreArchivo = "TipoPlacas";
    //     Ima_reporte::nombrePDF_Descarga($pdf, $nombreArchivo);
    // }
  
    // [ REPORTE: "imprime solicitudes" ]
    public static function imprime_Solicitud($parametro) { 
        
        $model = Med_registro::find($parametro->id);

        $modelEmpleador = Hcl_empleador::find($model->idhcl_empleador);
        $modelPoblacion = Hcl_poblacion::find($model->idhcl_poblacion);
        // print_r($model);
        // return;
        $modelAlmacen = Almacen::find($model->idalmacen);
        $modelClasificacion = Med_clasificacion::find($model->idmed_clasificacion);  

        $modelDatos = DB::table(DB::raw("med_lista_cargos_impresion(".$model->id.")"))->get();   
          
        setlocale(LC_TIME, "spanish");           
        $Nueva_Fecha = date("d-m-Y", strtotime($model->fecha_registro));             
        //$Mes_Anyo = strftime("%A, %d de %B de %Y", strtotime($Nueva_Fecha));
        $fecha_registro = strftime("%d de %B de %Y", strtotime($Nueva_Fecha));
        //devuelve: lunes, 16 de abril de 2018 
   
        //Convertimos el total en letras
        $montoCancelado=strtoupper(Med_cifrasEnLetras::convertirEurosEnLetras($model->total_cancelado));
        // print_r($model->total_cancelado.'-'.$letra);
        // return;
        $medidas = array(220, 340);
        ob_end_clean();
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetMargins(10, 10, 10, 10);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf::SetTitle('MEDICINA DEL TRABAJO');
        $pdf::SetSubject("TCPDF");

        $y_Inicio = 21;
        $height = 5;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $pdf::AddPage();
        $pdf::SetY($y_Inicio);
        $pdf::SetX(2);

        //$pdf::SetFont('dejavusans', 'B', 10);
        // $pdf::MultiCell(60, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(60, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(25, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        if ($model->eliminado == 1) {
            $pdf::SetY(230); 
            $pdf::SetFont($tipoLetra, 'B', 25);
            $pdf::MultiCell(190,25, 'DOCUMENTO NO VALIDO', 'T', 'C', 0, 1, '', '', true, 0, false, true, 20, 20);
        }
        else{
            $pdf::SetFont($tipoLetra, 'B', 13);
            $pdf::MultiCell(40, 1, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(80, 6, $modelAlmacen->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);
           
            $pdf::SetX(2);
            $pdf::SetFont('dejavusans', '', 13);        
            $pdf::MultiCell(108, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(75, $height, $fecha_registro, 0, 'L', 0, 1, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);        
            $pdf::MultiCell(70, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::SetFont('courier', '', 11);  
            $pdf::MultiCell(125, $height, $modelEmpleador->nombre, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
            $pdf::SetY(40);  
            $pdf::MultiCell(90, 30, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, 50, 50);
            $pdf::SetFont('helvetica', '', 11);
            $pdf::MultiCell(115, 40, 'Reposición de gastos efectuados en la atención médica y de laboratorio a '.$modelPoblacion->nombre.' '.$modelPoblacion->primer_apellido.' '.$modelPoblacion->segundo_apellido.', atención médica en fecha '.$fecha_registro.'.', 0, 'L', 0, 1, '', '', true, 0, 5, true, $height, 10);
            $pdf::MultiCell(70, $height, '* '.$modelClasificacion->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, $modelClasificacion->costo_tramite, 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(70, $height, '* Reposición de formulario', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, $modelClasificacion->costo_formulario, 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $model->total_cancelado, 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, $height, '* Total  ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $model->total_cancelado, 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, $height, ' ', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(90, $height, 'SON: '.$montoCancelado, 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, 25, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10); 
            $pdf::SetX(15); 
            $pdf::SetY(160);  
            $i = 0; 
            $y_firmas = 180;
            $x_firmas = 15;
            foreach ($modelDatos as $datos) {            
                $i += 1; 
                $dato = $i % 2 == 0? 1 : 0;
                if ($dato == 0) {
                    $pdf::SetX($x_firmas);
                }
                $pdf::MultiCell(80, $height, $datos->res_apellidos.' '.$datos->res_nombres, 'T', 'C', 0, 0, '', '', true, 0, false, true, 12, 10);            

                if ($dato == 0) {
                    $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);                 
                }else
                {
                    $pdf::SetY($y_firmas);                
                    $pdf::MultiCell(30, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 12, 10);    
                }             
            }
            $pdf::MultiCell(1, 5, ' ',0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $j = 0; 
            $y_firmas = 165; 
            foreach ($modelDatos as $datos) {
                $j += 1; 
                $dato = $j % 2 == 0? 1 : 0;
                if ($dato == 0) { 
                    $pdf::SetY($y_firmas);
                    $pdf::SetX($x_firmas);
                    $x_firmas += 15;
                }            
                $pdf::MultiCell(80, $height, $datos->res_cargo, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10); 
                if ($dato == 0) {
                    $pdf::MultiCell(30, $height, '', 0, 'L', 0, $dato, '', '', true, 0, false, true, 12, 10);  
                    $x_firmas = 15;           
                }else
                {
                    $y_firmas += 25;
                    $pdf::SetY($y_firmas);                
                    $pdf::MultiCell(30, $height, '', 0, 'L', 0, $dato, '', '', true, 0, false, true, 12, 10); 
                }             
            }
        }       
        $nombreArchivo = "reporteSolicitud";
        Med_reporte::nombrePDF_Descarga($pdf, $nombreArchivo);
    }

    // [ REPORTE: "imprime programacion" ]
    public static function imprime_Programacion($parametro) {  
        $model = Med_programacion::find($parametro->id); 
        $modelRegistro = Med_registro::find($model->idmed_registro);        
        $modelAlmacen = Almacen::find($model->idalmacen); 
        $modelClasificacion = Med_clasificacion::find($modelRegistro->idmed_clasificacion); 

         
        setlocale(LC_TIME, "spanish");           
        $Nueva_Fecha = date("d-m-Y", strtotime($model->fecha_registro));             
        //$Mes_Anyo = strftime("%A, %d de %B de %Y", strtotime($Nueva_Fecha));
        $fecha_registro = strftime("%a, %d de %B de %Y", strtotime($Nueva_Fecha));
        //devuelve: lunes, 16 de abril de 2018 
   
        //Convertimos el total en letras
        $montoCancelado=strtoupper(Med_cifrasEnLetras::convertirEurosEnLetras($model->total_cancelado));
        // print_r($model->total_cancelado.'-'.$letra);
        // return;
        $medidas = array(220, 340);
        ob_end_clean();
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetMargins(10, 10, 10, 10);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf::SetTitle('MEDICINA DEL TRABAJO');
        $pdf::SetSubject("TCPDF");

        $y_Inicio = 21;
        $height = 5;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $pdf::AddPage();
        $pdf::SetY($y_Inicio);
        $pdf::SetX(2);

        //$pdf::SetFont('dejavusans', 'B', 10);
        // $pdf::MultiCell(60, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(60, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(25, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, 'B', 13);
        $pdf::MultiCell(40, 1, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(80, 6, $modelAlmacen->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);
        $pdf::SetX(2);
        $pdf::SetFont('dejavusans', '', 12);        
        $pdf::MultiCell(108, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(75, $height, $fecha_registro, 0, 'L', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);        
        $pdf::MultiCell(80, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(105, $height, $model->hcl_empleador_nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(90, 30, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, 50, 50);
        $pdf::SetFont('helvetica', '', 11);
        $pdf::MultiCell(115, 40, 'Reposición de gastos efectuados en la atención médica y de laboratorio a '.$model->hcl_poblacion_primer_apellido.' '.$model->hcl_poblacion_segundo_apellido.' '.$model->hcl_poblacion_nombres.', atención médica en fecha '.$fecha_registro.'.', 0, 'L', 0, 1, '', '', true, 0, 5, true, $height, 10);
        $pdf::MultiCell(70, $height, '* '.$modelClasificacion->nombre, 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(10, $height, 'Bs. ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, $modelClasificacion->costo_tramite, 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(70, $height, '* Reposición de formulario', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, $modelClasificacion->costo_formulario, 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, $model->total_cancelado, 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(165, $height, '* Total  ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, $model->total_cancelado, 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(165, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(165, $height, ' ', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(90, $height, 'SON: '.$montoCancelado, 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(165, 25, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->cargo_1, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->cargo_2, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->nombre_1, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->nombre_2, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(165, 25, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->cargo_3, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->cargo_4, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->nombre_3, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->nombre_4, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(165, 25, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->cargo_5, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->cargo_6, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->nombre_5, 0, 'C', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(30, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
        $pdf::MultiCell(60, $height, $modelDatos->nombre_6, 0, 'C', 0, 1, '', '', true, 0, false, true, 12, 10);
        $nombreArchivo = "reporteSolicitud";
        Med_reporte::nombrePDF_Descarga($pdf, $nombreArchivo);
    }

}