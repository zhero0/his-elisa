<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Hsi_tipo extends Model
{ 
	protected $table = 'hsi_tipo';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at 
}