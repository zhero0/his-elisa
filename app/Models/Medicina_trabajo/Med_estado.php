<?php
namespace App\Models\Medicina_trabajo;

use Illuminate\Database\Eloquent\Model;

class Med_estado extends Model
{ 
	protected $table = 'med_estado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at 
}