<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Seguimiento\Seg_establecimiento; 

class Seg_envio_laboratorio extends Model
{
	protected $table = 'seg_envio_laboratorio';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function registra_envioLaboratorio($model,$request)
    { 
        $modelEstablecimiento = Seg_establecimiento::find($request->idseg_establecimiento_envio); 
        $modelSeg = new Seg_envio_laboratorio;
        $modelSeg->codigo = $modelEstablecimiento->codigo;      
        $modelSeg->nombre = $modelEstablecimiento->nombre;
        $modelSeg->fecha_envio = date('Y-m-d', strtotime($request->fecha_envio));
        $modelSeg->idseg_establecimiento_envio = $modelEstablecimiento->id; 
        $modelSeg->idseg_registro_resultado = $model->id;         
        $modelSeg->save();
    }
}