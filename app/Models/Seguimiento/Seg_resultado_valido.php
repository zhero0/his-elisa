<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;

class Seg_resultado_valido extends Model
{
	protected $table = 'seg_resultado_valido';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}