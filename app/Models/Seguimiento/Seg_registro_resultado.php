<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;

use \App\Models\Almacen\Home;
use \App\Models\Almacen\Almacen; 

use Illuminate\Support\Facades\DB;
use Auth;
use Eloquent; 

use \App\Models\Seguimiento\Seg_estado_cov;
use \App\Models\Seguimiento\Seg_establecimiento;
use \App\Models\Seguimiento\Seg_ficha_generada;
use \App\Models\Seguimiento\Seg_tipo_muestra;
use \App\Models\Seguimiento\Seg_tipo_paciente;
use \App\Models\Seguimiento\Seg_metodo_diagnostico;
use \App\Models\Seguimiento\Seg_registro_resultado;
use \App\Models\Seguimiento\Seg_metodo_valores; 

use \App\Models\Afiliacion\Hcl_cuaderno;

use \App\Models\Administracion\Datosgenericos;
use \App\Models\Administracion\Impresion;

use Elibyy\TCPDF\Facades\TCPDF;

class Seg_registro_resultado extends Model
{
  protected $table = 'seg_registro_resultado';
  public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

  const ANTIGENO = 1;
  const ANTICUERPOS = 2;
  const ELISA = 3;
  const PCR = 4;

    public static function resultados_covLaboratorio($datosBuscador, $opcion)
    {
        $sistema = 0;
        $laboratorio = 0;
        $documento = '';
        $paterno = '';
        $materno = '';
        $nombres = '';
        $idseg_estado_cov = 0;
        $idSeg_metodo_diagnostico = 0;
        $datosBuscador = json_decode($datosBuscador);
        if($datosBuscador != '')
        { 
            $sistema = $datosBuscador->sistema > 0? $datosBuscador->sistema :  0;
            $laboratorio = $datosBuscador->laboratorio > 0? $datosBuscador->laboratorio : 0;
            $documento = $datosBuscador->documento;
            $paterno = $datosBuscador->primer_apellido;
            $materno = $datosBuscador->segundo_apellido;
            $nombres = $datosBuscador->nombre;
            $idseg_estado_cov = $datosBuscador->idseg_estado_cov > 0? $datosBuscador->idseg_estado_cov : 0;
            $idSeg_metodo_diagnostico = $datosBuscador->idSeg_metodo_diagnostico > 0? $datosBuscador->idSeg_metodo_diagnostico : 0;
        }
        else // Resultado Imagenologia
        {
            // $codigo = $datosBuscador;
            $nombre = $datosBuscador;
        }
        $query = DB::table(DB::raw(
      "resultados_sarscov(
                          " . $sistema . ",
                          " . $laboratorio . ",
                          '" . $documento . "',
                          '" . $paterno . "',
                          '" . $materno . "',
                          '" . $nombres . "',
                          " . $idseg_estado_cov . ",
                          " . $idSeg_metodo_diagnostico . "
                        )"
    ));
      return $query;
    }

    public static function imprimeResultado($id)
    {
      $valor = explode('*|*', $id);
      $id = $valor[0];
      $imprimirDirecto = $valor[1];
      
      $modelSeg_ficha_generada = Seg_ficha_generada::find($id); 
      $modelSeg_registro_resultado = Seg_registro_resultado::where('idseg_ficha_generada', '=', $modelSeg_ficha_generada->id)->first();

      $modelTipo_muestra = Seg_tipo_muestra::where('id', '=', $modelSeg_registro_resultado->idseg_tipo_muestra)->first();
 
      $model = Seg_resultado::where('idseg_registro_resultado', '=', $modelSeg_registro_resultado->id)->first();

      $modelMetodo_valores = Seg_metodo_valores::where('id', '=', $model->idseg_metodo_valores)->first();
       
      $numeroImprimir = $modelSeg_registro_resultado->registro_sistema;
      $codbarra = $numeroImprimir;
      ob_end_clean();
      $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
      
      $pdf::SetTitle("LABORATORIO");
      $pdf::SetSubject("TCPDF");

      $pdf::SetCreator('PDF_CREATOR');
      $pdf::SetAuthor('usuario');
      $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
      
      //cambiar margenes
      $pdf::SetMargins(5, 5, 5, 5); 

      $pdf::startPageGroup();
      
      //set image scale factor   

      $pdf::setHeaderCallback(function($pdf)use($numeroImprimir,$codbarra) {
          // $y = $pdf->GetY();
          $pdf->SetFillColor(230, 230, 230); 

          $pdf->SetFont('courier', 'B', 20);
          $pdf->MultiCell(130, 5, 'CAJA NACIONAL DE SALUD', 'B', 'C', '', 1, '', '', 1, '', '', '',20, 'M');

          $pdf->SetFont('helvetica', '', 15);
          $pdf->MultiCell(130, 8, 'CENTRO CENTINELA COVID-19', 0, 'C', '', 1, '', '', 1, '', '', '', 8, 'M');          

          $pdf->SetFont('helvetica', 'B', 15);
          $pdf->MultiCell(90, 8, 'FOLIO N°', 0, 'R', '', 0, '', '', 1, '', '', '', 8, 'M');
          $pdf->MultiCell(5, 8, '', 0, 'R', '', 0, '', '', 1, '', '', '', 8, 'M');
          $pdf->SetFont('courier', 'B', 50); 
          $pdf->MultiCell(35, 20, '350', 1, 'C', '', 0, '', '', 1, '', '', '', 30, 'M');
 
      });
      
      $usuario = Auth::user()['name'];
      $fecha = date('d-m-Y');

      // setPage
      $pdf::AddPage('L');
      $pdf::setPage(1, true);
      $pdf::SetFillColor(255, 255, 255);

      $y_Inicio = 29;
      $height = 5;
      $width = 190;
      $widthLabel = 21;
      $widthDosPuntos = 4;
      $widthInformacion = 90;
      $espacio = 3;
      $tipoLetra = 'helvetica';
      $tamanio = 9;
      $widthTitulo = 50;

      $pdf::SetY($y_Inicio);
      // ------------------------------------------ [PACIENTE] ------------------------------------------ 
      
      // $pdf::SetFont($tipoLetra, 'B', 12);
      // $pdf::MultiCell($widthInformacion, $height, 'DATOS SEGURO', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);

      $pdf::MultiCell(22, $height, 'MATRICULA: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, 'B', 10);
      $pdf::MultiCell(25, $height, $modelSeg_ficha_generada->numero_historia, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);
      $pdf::MultiCell(29, $height, 'Nº DOCUMENTO: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, 'B', 10);
      $nro_documento = $modelSeg_ficha_generada->complemento = ' '? $modelSeg_ficha_generada->documento : $modelSeg_ficha_generada->documento.'-'.$modelSeg_ficha_generada->complemento; 
      $pdf::MultiCell(25, $height, $nro_documento, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);      
      $pdf::SetFont($tipoLetra, '', $tamanio);


      $pdf::MultiCell(20, $height, 'PACIENTE: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::MultiCell($widthInformacion, $height, $modelSeg_ficha_generada->primer_apellido.' '.$modelSeg_ficha_generada->segundo_apellido.' '.$modelSeg_ficha_generada->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
      
      $pdf::MultiCell(12, $height, 'SEXO: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::MultiCell(15, $height, $modelSeg_ficha_generada->sexo = 1? 'MASC':'FEM', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10); 

      $pdf::MultiCell(12, $height, 'EDAD: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::MultiCell(10, $height, $modelSeg_ficha_generada->edad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      
      $pdf::MultiCell(12, $height, 'TELF.: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::MultiCell(20, $height, $modelSeg_ficha_generada->telefono, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

      $pdf::SetFont($tipoLetra, 'B', 8);
      $pdf::MultiCell(32, $height, 'TIPO DE MUESTRA: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);
      $pdf::MultiCell(63, 4, $modelTipo_muestra->descripcion, 'B', 'C', 0, 1, '', '', true, 0, false, true, 4, 10);
      $pdf::SetFont($tipoLetra, 'B', 8);
      $pdf::MultiCell(60, $height, 'FECHA TOMA DE MUESTRA: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);
      $pdf::MultiCell(35, 4, $modelSeg_registro_resultado->fecha_toma.' '.$modelSeg_registro_resultado->hora_toma, 0, 'C', 0, 1, '', '', true, 0, false, true, 4, 10);
      $pdf::MultiCell(59, $height, 'RESPONSABLE TOMA DE MUESTRA: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);
      $pdf::MultiCell(71, 4, strtoupper($modelSeg_registro_resultado->bioquimico_responsable_toma), 0, 'L', 0, 1, '', '', true, 0, false, true, 4, 10);
      $pdf::MultiCell(60, $height, 'RESPONSABLE DE ANALISIS: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);
      $pdf::MultiCell(70, 4, strtoupper($model->medico_responsable_analisis), 0, 'L', 0, 1, '', '', true, 1, false, true, 4, 10);
      $pdf::SetFont($tipoLetra, 'B', 8);
      $pdf::MultiCell(60, $height, 'FECHA RESULTADO: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
      $pdf::SetFont($tipoLetra, '', $tamanio);
      $pdf::MultiCell(35, 4, $model->fecha_resultado.' '.$model->hora_resultado, 0, 'C', 0, 1, '', '', true, 0, false, true, 4,10); 
      // [ PACS ]
      // $modelIma_pacs = Ima_pacs::buscarIdPacs($modelIma_resultados_programacion->idpacs);
 
      $pdf::Line(5, $pdf::getY(), 135, 64);

      $pdf::SetY(64);       
      $pdf::SetFont($tipoLetra, 'B', 17);
      $pdf::MultiCell(130, 5, $model->descripcion_metodo, 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 10);
      $pdf::SetFont($tipoLetra, '', 15);
      $pdf::MultiCell(130, 5, 'RESULTADO DE LABORATORIO', 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 10);
      if (Seg_registro_resultado::ANTIGENO == $model->idseg_metodo_diagnostico) {        
        $pdf::SetFont($tipoLetra, 'B', 20);
        $pdf::MultiCell(130, 10, $model->nombre_estado, 1, 'C', 0, 1, '', '', true, 0, false, true,20,10); 
        $pdf::MultiCell(20, $height, ' : ', 0, 'R', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(25, $height, 'Observaciones: ', 0, 'R', 0, 0, '', '', true, 0, false, true, $height, 10); 
        $pdf::MultiCell(35, 4, $model->observaciones, 0, 'C', 0, 1, '', '', true, 0, false, true, 4,10); 
      }
      if (Seg_registro_resultado::ANTICUERPOS  == $model->idseg_metodo_diagnostico) {        
        $pdf::SetFont('times', 'B', 20);
        $pdf::MultiCell(18, 9, 'IgM', 1, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
        $pdf::SetFont($tipoLetra, '', 15);
        $pdf::MultiCell(47, 9, $model->res_igm, 1, 'C', 0, 0, '', '', true, 0, false, true,40,10); 
        
        $pdf::SetFont('times', 'B', 20);
        $pdf::MultiCell(18, 9, 'IgG', 1, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
        $pdf::SetFont($tipoLetra, '', 15);
        $pdf::MultiCell(47, 9, $model->res_igg, 1, 'C', 0, 1, '', '', true, 0, false, true,40,10);  

        $pdf::SetFont($tipoLetra, '', 13);
        $pdf::MultiCell(130, 5, 'INTERPRETACION', 'B', 'C', 0, 1, '', '', true, 0, false, true, 40, 10);        
        $pdf::MultiCell(130, 5, $modelMetodo_valores->interpretacion, 'B', 'C', 0, 1, '', '', true, 0, false, true,50,10);
        $pdf::MultiCell(130, 5, 'CONDUCTA', 'B', 'C', 0, 1, '', '', true, 0, false, true, 40, 10);        
        $pdf::MultiCell(130, 5, $modelMetodo_valores->conducta, 'B', 'C', 0, 1, '', '', true, 0, false, true,50,10); 
      }
      if (Seg_registro_resultado::ELISA  == $model->idseg_metodo_diagnostico) {        
        $pdf::SetFont('times', 'B', 20);
        $pdf::MultiCell(18, 9, 'IgM', 1, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
        $pdf::SetFont($tipoLetra, '', 15);
        $pdf::MultiCell(47, 9, $model->res_igm, 1, 'C', 0, 0, '', '', true, 0, false, true,40,10);
        $pdf::SetFont('times', 'B', 20);
        $pdf::MultiCell(18, 9, 'IgG', 1, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
        $pdf::SetFont($tipoLetra, '', 15);
        $pdf::MultiCell(47, 9, $model->res_igg, 1, 'C', 0, 1, '', '', true, 0, false, true,40,10); 
        $pdf::SetFont('times', 'B', 20);
        $pdf::MultiCell(130, 5, 'VALORES DE REFERENCIA', 'B', 'C', 0, 1, '', '', true, 0, false, true, 40, 10);                
        $pdf::SetFont($tipoLetra, '', 14);
        $pdf::MultiCell(60, 5, 'MENOR A 0,90', 1, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);        
        $pdf::MultiCell(70, 5, 'NEGATIVO', 1, 'C', 0, 1, '', '', true, 0, false, true, 40, 10);   
        $pdf::MultiCell(60, 5, '0,91-1,09', 0, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);        
        $pdf::MultiCell(70, 5, 'INDETERMINADO', 0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10); 
        $pdf::MultiCell(60, 5, 'MAYOR A 1,10', 1, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);        
        $pdf::MultiCell(70, 5, 'POSITIVO', 1, 'C', 0, 1, '', '', true, 0, false, true, 40, 10);        
      }

      // ------------------------------------------ [RESULTADO] ------------------------------------------
      // $modelDatosgenericos_Respuesta = Datosgenericos::find($modelIma_resultados_programacion->iddatosgenericos);
      // $personal_Medico = $modelDatosgenericos_Respuesta != ''? $modelDatosgenericos_Respuesta->nombres.' '.$modelDatosgenericos_Respuesta->apellidos : '';
      // $pdf::MultiCell(120, $height, 'RESULTADO:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
      // // print_r($modelIma_especialidad->impresion);
      // // return;
      // $inferior = 0;

      // if ($modelIma_especialidad->impresion == Impresion::MEDIA_HOJA) {
      //     $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 67, 20);
      //      $inferior = 143;
      // }
      // else if($modelIma_especialidad->impresion == Impresion::CARTA) {
      //     $pdf::MultiCell('', '', $modelIma_resultados_programacion->respuesta, 0, 'L', 0, 1, '', '', true, 0, true, true, 260, 20);

      //      //$inferior = 267;
      // //     print_r($modelIma_especialidad->impresion);
      // // return;
      // }

      $pdf::setY(145); 

      $pdf::MultiCell(13, '', '', 0, 'C', 0, 0);  
      $pdf::SetFont($tipoLetra, 'B', 12);         
      $pdf::MultiCell(29, 12, $modelSeg_registro_resultado->registro_sistema, 0, 'C', 0, 1, '', '', true, 0, false, true, 12,10); 
      $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            );
      $pdf::setY(170);
      $pdf::MultiCell(60, '', '', 0, 'C', 0, 0);            
      $pdf::MultiCell(50, 10, 'Firma y sello Médico', 'T', 'C', 0, 1);
      $pdf::write2DBarcode($modelSeg_registro_resultado->registro_sistema.' - '.$modelSeg_ficha_generada->numero_historia.'-'.$modelSeg_ficha_generada->primer_apellido.' '.$modelSeg_ficha_generada->segundo_apellido.' '.$modelSeg_ficha_generada->nombre.' - '.$modelSeg_ficha_generada->nro_empleador.' - '.$modelSeg_ficha_generada->razon_empleador, 'QRCODE,L', 15, 148, 35, 35, $style, 'N');

      

      //$pdf::setY($pdf::getY()+90);

      $inferior = 181;
      // ************************ [footer] *******************************************
      $pdf::SetY($inferior);
      $pdf::SetFont('courier', 'B', 7);
      $pdf::MultiCell(130, '','Usuario: '.$usuario, 'T', 'L', 0);
      $pdf::SetY(-10);
      $pdf::SetY($inferior);
      $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
      // $pdf::MultiCell(60, '', 'Fecha: '. date("d").'-'.date("m").'-'.date("Y")  , 0, 'L', 0, 1, 60);
      // $pdf::SetY(-10);
      // $pdf::SetY($inferior);
      // $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
      // ****************************************************************************
      

      // Muestra el panel para imprimir de forma directa...
      if($imprimirDirecto == 1)
          $pdf::IncludeJS('print(true);');

      $pdf::Output("resultadoLaboratorio.pdf", "I");

      mb_internal_encoding('utf-8');
    
    }

}