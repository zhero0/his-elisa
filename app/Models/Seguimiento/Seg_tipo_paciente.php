<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;

class Seg_tipo_paciente extends Model
{
	protected $table = 'seg_tipo_paciente';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}