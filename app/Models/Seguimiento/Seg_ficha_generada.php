<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;

class Seg_ficha_generada extends Model
{
	protected $table = 'seg_ficha_generada';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at 
}