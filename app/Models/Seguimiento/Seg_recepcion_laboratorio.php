<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Seguimiento\Seg_establecimiento; 

class Seg_recepcion_laboratorio extends Model
{
	protected $table = 'seg_recepcion_laboratorio';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function registra_recepcionLaboratorio($model, $request)
    {
        $modelEstablecimiento = Seg_establecimiento::find($request->idseg_establecimiento_recepcion); 
        $modelSeg = new Seg_recepcion_laboratorio;
        $modelSeg->codigo = $modelEstablecimiento->codigo;      
        $modelSeg->nombre = $modelEstablecimiento->nombre;
        $modelSeg->fecha_recepcion = date('Y-m-d', strtotime($request->fecha_recepcion));
        $modelSeg->hora_recepcion = $request->hora_recepcion; 
        $modelSeg->idseg_establecimiento_recepcion = $modelEstablecimiento->id; 
        $modelSeg->idseg_registro_resultado = $model->id;         
        $modelSeg->save();
    }
}