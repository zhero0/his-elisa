<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;

class Seg_datos_epidemiologicos extends Model
{
	protected $table = 'seg_datos_epidemiologicos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function registra_datosEpidemiologicos($model, $request)
    {        
        $modelDatos = new Seg_datos_epidemiologicos;
        $modelDatos->dosis = $request->dosis;      
        $modelDatos->primera_dosis = $request->primera_dosis;
        $modelDatos->segunda_dosis = $request->segunda_dosis;
        $modelDatos->diagnostico = $request->diagnostico;
        $modelDatos->diagnostico_fecha = date('Y-m-d', strtotime($request->diagnostico_fecha));        
        $modelDatos->absorbancia = $request->absorbancia; 
        $modelDatos->absorbancia_fecha = date('Y-m-d', strtotime($request->absorbancia_fecha));
        $modelDatos->idSeg_ficha_generada = $model->id;         
        $modelDatos->save();
    }
}