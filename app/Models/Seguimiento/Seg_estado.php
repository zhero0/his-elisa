<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;

class Seg_estado extends Model
{
	protected $table = 'seg_estado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}