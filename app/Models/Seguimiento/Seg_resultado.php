<?php
namespace App\Models\Seguimiento;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Seguimiento\Seg_metodo_diagnostico;
use \App\Models\Administracion\Datosgenericos;
use \App\Models\Seguimiento\Seg_ficha_generada;

class Seg_resultado extends Model
{
	protected $table = 'seg_resultado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function registra_datosResultado($model, $request)
    {   
        $modelFind = Seg_resultado::where('idseg_registro_resultado', '=', $model->id)->first();

        if ($modelFind == null) { 
            $modelResultado = new Seg_resultado; 
        }else{ 
            $modelResultado = Seg_resultado::find($modelFind->id);
        }
 
        $modelMetodo = Seg_metodo_diagnostico::find($request->idseg_metodo_diagnostico);
        $modelResultado->idseg_metodo_diagnostico = $modelMetodo->id;
        $modelResultado->descripcion_metodo = $modelMetodo->descripcion;
        $modelResultado->diagnostico = $request->diagnostico;
        $modelResultado->diagnostico_fecha = date('Y-m-d', strtotime($request->diagnostico_fecha));
        $modelResultado->seguimiento = $request->seguimiento;
        $modelResultado->seguimiento_fecha = date('Y-m-d', strtotime($request->seguimiento_fecha));
        $modelResultado->fecha_resultado = date('Y-m-d', strtotime($request->fecha_resultado));
        $modelResultado->hora_resultado = $request->hora_resultado;

        $modelResultado->iddatosgenericos_responsable_analisis = $request->iddatosgenericos_responsable_analisis;
        // $model->bioquimico_responsable_toma = strtoupper($request->bioquimico_responsable_toma);

        $modelDatosgenericos = Datosgenericos::find($request->iddatosgenericos_responsable_analisis);
        $modelResultado->medico_responsable_analisis = $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos;
        $modelResultado->res_igm = $request->res_igm;
        $modelResultado->res_igg = $request->res_igg;
        $modelResultado->idseg_metodo_valores = $request->idseg_metodo_valores;
        $modelEstado_covid = Seg_estado_cov::find( $request->idseg_estado_cov);
        if ($modelEstado_covid!=null) {
            $modelResultado->idseg_estado_cov = $modelEstado_covid->id;
            $modelResultado->nombre_estado = $modelEstado_covid->nombre;
        }
        
        $modelSeg_ficha_generada = Seg_ficha_generada::find($request->idseg_ficha_generada);
        $modelResultado->documento = $modelSeg_ficha_generada->documento;
        $modelResultado->fecha_nacimiento = $modelSeg_ficha_generada->fecha_nacimiento;
        $modelResultado->complemento = $modelSeg_ficha_generada->complemento;
        $modelResultado->nombre_completo = $modelSeg_ficha_generada->nombre.' '.$modelSeg_ficha_generada->primer_apellido.' '.$modelSeg_ficha_generada->segundo_apellido;
        $modelResultado->idseg_registro_resultado = $model->id;
        $modelResultado->idalmacen = $request->idalmacen;
        $modelResultado->save();
    }
}