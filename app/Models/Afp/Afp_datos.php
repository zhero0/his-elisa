<?php

namespace App\Models\Afp;

use Illuminate\Database\Eloquent\Model; 
use Elibyy\TCPDF\Facades\TCPDF;
use Illuminate\Support\Facades\DB;

class Afp_datos extends Model
{ 
	protected $table = 'afp_datos'; 
	public $timestamps = false; 
	// protected $fillable = array('id', 'codigo', 'nombre','direccion','descripcion','usuario','fecha','eliminado');
	// protected $hidden = ['created_at','updated_at']; 	 

	const AFP = 1;
	const OTHER = 2;

	// [ REPORTE: "imprime solicitudes" ]
    public static function imprime_Solicitud($parametro) { 
        
        $model = Afp_datos::find($parametro->id);   
  
          
        setlocale(LC_TIME, "spanish");                       
        //$Mes_Anyo = strftime("%A, %d de %B de %Y", strtotime($Nueva_Fecha)); 
        //devuelve: lunes, 16 de abril de 2018 
   
        //Convertimos el total en letras 
        // print_r($model->total_cancelado.'-'.$letra);
        // return;
        $medidas = array(220, 340);
        ob_end_clean();
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf::SetMargins(10, 10, 10, 10);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        $pdf::SetTitle('AFP-PREVISION');
        $pdf::SetSubject("TCPDF");

        $y_Inicio = 21;
        $height = 5;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $pdf::AddPage();
        $pdf::SetY($y_Inicio);
        $pdf::SetX(2);

        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100); 


        $pdf::SetY(5); 
        $pdf::SetX(70); 
        $pdf::MultiCell(70, 100, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::Image('../public/imagenes/afp_prevision.png', 80, '', '', 16);        
        $pdf::MultiCell(20, 18, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::Image('../public/imagenes/afp_prevision.png',1 , '', 40, 40, '', '', 'T', false, 300, '', false, false, 1, false, false, false);

 

        //$pdf::SetFont('dejavusans', 'B', 10);
        // $pdf::MultiCell(60, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(60, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(25, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
        if ($model->eliminado == 1) {
            $pdf::SetY(230); 
            $pdf::SetFont($tipoLetra, 'B', 25);
            $pdf::MultiCell(190,25, 'DOCUMENTO NO VALIDO', 'T', 'C', 0, 1, '', '', true, 0, false, true, 20, 20);
        }
        else{
            $pdf::SetFont($tipoLetra, '', 13);
            $pdf::MultiCell(40, 1, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::SetTextColor(0, 0, 128); 
            $pdf::MultiCell(120, 6, 'EXAMEN PRE - OCUPACIONAL', 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 12);
            $pdf::SetFont('helvetica', '', 7);   
            // $pdf::SetTextColor(0, 0, 0); 
            $pdf::SetX(6);
            $pdf::MultiCell(197, 4, 'Este formulario debe ser llenado por los Centros Médicos habilitados por la Superintendencia, con cargo al empleador, y entregado en el plazo de 10 días en área rural o 5', 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 12);
            $pdf::SetX(6);
            $pdf::MultiCell(197, 4, 'en área urbana de registrado el dependiente  ( Resolución SP - 037/97 )   el original para el Ente Gestor de Salud,  copias   1.- AFP,   2.- Superintendencia de Pensiones,   3.-', 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 12);
            $pdf::SetX(6);
            $pdf::MultiCell(197, 4, 'Direcciones Departamentales y Regionales del Ministerio de Trabajo y Microempresa, 4.- Instituto Nacional de Salud Ocupacional, 5.- Afiliado, 6.- Empleador.', 0, 'C', 0, 1, '', '', true, 0, false, true, 10, 12);
            $pdf::SetX(2);
            $pdf::SetFont('dejavusans', '', 12);             

            //lateral 1
            $pdf::SetXY(8,94);
	        $pdf::StartTransform();
	        $pdf::Rotate(90);
	        $pdf::SetFont('helvetica', 'B', 8);
	        $pdf::SetTextColor(255, 255, 255);
	        $pdf::SetFillColor(69, 10, 250);
	        $pdf::MultiCell(48, 0, 'Trabajador Afiliado', 0, 'C', 1, 1, '', '', true, 0, false, true, 0); 
	        $pdf::StopTransform();
	        $pdf::SetFont('helvetica', '', 7);   
            $pdf::SetTextColor(0, 0, 0); 
            // DATOS NOMBRES APELLIDOS
            $pdf::SetXY(17,46);
            $pdf::MultiCell(35, 10, 'Apellido Paterno', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'Apellido Materno', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'Apellido de Casada', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(45, 10, 'Nombres', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'CUA', 1, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);
            $pdf::SetXY(17,56); 
            $pdf::MultiCell(100, 4, 'Matricula del asegurado al ente gestor de salud', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(85, 4, 'EGS.', 1, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);
            // DATOS DOCUMENTO
            $pdf::SetXY(17,60);
            $pdf::MultiCell(35, 10, 'CI', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'RUN', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'PAS/C.E.', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(18, 10, 'Sexo', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(27, 10, 'Fecha de Nacimiento', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(11.6, 10, 'DIA', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(11.6, 10, 'MES', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(11.6, 10, 'AÑO', 1, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);

            // DATOS ESTADO DEL PACIENTE
            $pdf::SetXY(17,70); 
            $salto_columna = 25;
            $pdf::MultiCell($salto_columna, 4, 'Soltero', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell($salto_columna, 4, 'Casado', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell($salto_columna, 4, 'Viudo', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell($salto_columna, 4, 'Divorciado', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell($salto_columna, 4, 'Conviviente', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell($salto_columna, 4, 'Nacionalidad:', 1, 'R', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 4, 'Boliviana', 1, 'C', 0, 0, '', '', true, 0, false, true, 10, 12);

            // DATOS UBICACION
            $pdf::SetXY(17,74);
            $pdf::MultiCell(35, 10, 'Departamento', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'Provincia', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'Ciudad/ Localidad', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(45, 10, 'Zona/ Barrio/ Urbanización', 1, 'L', 0, 0, '', '', true, 0, false, true, 10, 12);
            $pdf::MultiCell(35, 10, 'Teléfono/ otros', 1, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);
            $pdf::SetXY(17,84);
            $pdf::MultiCell(185, 10, 'Domicilio actual (Calle y Nro)', 1, 'L', 0, 1, '', '', true, 0, false, true, 10, 12);

            // lateral 2
	        $pdf::SetXY(12,94);
	        $pdf::StartTransform();
	        $pdf::Rotate(90);
	        $pdf::SetFont('helvetica', 'B', 8);
	        $pdf::SetTextColor(255, 255, 255);
	        $pdf::SetFillColor(69, 10, 250); 
	         
	        $pdf::MultiCell(48, 0, 'Dirección  Identificación', 0, 'C', 1, 0, '', '', true, 0, false, true, 0);
	        $pdf::StopTransform();

	        // lateral 3
	        $pdf::SetXY(8,147);
	        $pdf::StartTransform();
	        $pdf::Rotate(90);
	        $pdf::SetFont('helvetica', 'B', 10);
	        $pdf::SetTextColor(255, 255, 255);
	        $pdf::SetFillColor(69, 10, 250); 
	        $pdf::MultiCell(48, 7, 'Información laboral', 'C', 'C', 2, 1, '', '', true, 0, true, true, 10, 12); 
	        $pdf::StopTransform();




	        $pdf::SetTextColor(0, 0, 0);
            $pdf::MultiCell(108, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(75, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);        
            $pdf::MultiCell(80, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(105, $height,'', 0, 'L', 0, 1, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(90, 30, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, 50, 50);
            $pdf::SetFont('helvetica', '', 11);

            $pdf::MultiCell(115, 40, 'Reposición de gastos efectuados en la atención médica y de laboratorio a '.$model->hcl_poblacion_primer_apellido.' '.$model->hcl_poblacion_segundo_apellido.' '.$model->hcl_poblacion_nombres.', atención médica en fecha ', 0, 'L', 0, 1, '', '', true, 0, 5, true, $height, 10);
            $pdf::MultiCell(70, $height, '* '.'', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, '', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(70, $height, '* Reposición de formulario', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10);
            $pdf::MultiCell(50, $height, ' ', 0, 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, '', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, $height, '* Total  ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(10, $height, 'Bs. ', 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, $height, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, $height, ' ', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(90, $height, 'SON: ', 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(165, 25, ' ', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, 12, 10); 
            $pdf::SetX(20);
 
             
        }       
        $nombreArchivo = "reporteSolicitud";
        $pdf::Output($nombreArchivo.".pdf", "I");
        mb_internal_encoding('utf-8');
    }
}

