<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_estado extends Model
{ 
	protected $table = 'hcl_estado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	const VIGENTE = 1;
	const ANULADO = 2;
}