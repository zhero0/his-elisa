<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \App\Models\Almacen\Almacen;
use \App\Models\Afiliacion\Hcl_estado;
use Auth;

class Hcl_registro_referencias_medicas extends Model
{ 
	protected $table = 'hcl_registro_referencias_medicas';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function registraReferencia_Medica($arrayValor)
	{		
		$arrayReferencia = $arrayValor['arrayReferenciaMedica'];
		$modeloSalud = $arrayValor['model']; 

		$almacen = Almacen::find($modeloSalud->idalmacen);
		$cuaderno = Hcl_cuaderno::find($modeloSalud->idhcl_cuaderno);
		$signosVitales = Hcl_datos_signos_vitales::where([
                        ['eliminado', '=', 0],
                        ['idhcl_cabecera_registro', '=', $modeloSalud->id]
                    ])->get();

		$diagnosticos = Hcl_diagnosticoscie::where([
                        ['eliminado', '=', 0],
                        ['idhcl_cabecera_registro', '=', $modeloSalud->id]
                    ])->get();

		$signos_vitales = '';
		if (count($signosVitales)>0) {
			foreach ($signosVitales as $dato) {
				if ($dato->contenido_variable != '') {
					$signos_vitales = $signos_vitales.' '.$dato->variable.' '.$dato->contenido_variable;
				}
			}
		}

		$diagnosticos_presuntivos = '';
		if (count($diagnosticos)>0) {
			foreach ($diagnosticos as $dato) {				
				$diagnosticos_presuntivos = $diagnosticos_presuntivos.' '.$dato->codigo.'-'.$dato->diagnostico;				
			}
		}

		// $modelImagenologia = DB::table('ima_registroprogramacion as ir')
		//  			->join('ima_resultados_programacion as irp', 'irp.idima_registroprogramacion', '=', 'ir.id')                  
  //                   ->select('ir.id','irp.examenes_programados')
  //                   ->where([
  //                       ['ir.eliminado', '=', 0],
  //                       ['irp.eliminado', '=', 0],
  //                       ['ir.idhcl_cabecera_registro', '=', $modeloSalud->id]
  //                   ])
  //                   ->get();

        $examenes_complementarios = '';
		// if (count($modelImagenologia)>0) {
		// 	foreach ($modelImagenologia as $dato) {
		// 		$examenes_complementarios = $examenes_complementarios+' '+$dato->examenes_programados;				
		// 	}
		// }
  
		if(count($arrayReferencia) > 0)
		{
			$almacenRec = Almacen::find($arrayReferencia['0']->idAlmacen);
			$cuadernoRec = Hcl_cuaderno::find($arrayReferencia['0']->idEspecialidad);
            $modelo = new Hcl_registro_referencias_medicas;
            $modelo->idhcl_cabecera_registro = $modeloSalud->id;
            $modelo->signos_vitales = $signos_vitales;
            $modelo->diagnosticos_presuntivos = $diagnosticos_presuntivos;
            $modelo->examen_clinico = $arrayReferencia['0']->examen_clinico;
            $modelo->examenes_complementarios = $examenes_complementarios;
            $modelo->tratamiento_inicial = $arrayReferencia['0']->tratamiento_inicial;
            $modelo->recomendaciones = $arrayReferencia['0']->recomendaciones;

            $modelo->idalmacenref = $modeloSalud->idalmacen;
            $modelo->almacen_nombreref = $almacen->nombre;
            $modelo->idhcl_cuadernoref = $modeloSalud->idhcl_cuaderno;
            $modelo->hcl_cuaderno_nombreref = $cuaderno->nombre; 
            $modelo->fecha_ref = date('Y-m-d');

            $modelo->idalmacenrec = $almacenRec->id;
            $modelo->almacen_nombrerec = $almacenRec->nombre;
            $modelo->idhcl_cuadernorec = $cuadernoRec->id;
            $modelo->hcl_cuaderno_nombrerec = $cuadernoRec->nombre;

            $modelo->idhcl_estado = Hcl_estado::VIGENTE;
            $modelo->usuario = Auth::user()['name'];
            $modelo->save();
        }

 
        // if(count($arrayDiagnosticos) > 0)
        // {
        //     for($i = 0; $i < count($arrayDiagnosticos); $i++)
        //     { 
        //         $modelo = new Hcl_registro_referencias_medicas;
        //         $cie = Hcl_cie::find($arrayDiagnosticos[$i]);
        //         $modelo->numero = $i+1;
        //         $modelo->idhcl_cabecera_registro = $modeloSalud->id;
        //         $modelo->idhcl_cie = $cie->id;
        //         $modelo->codigo = $cie->codigo;
        //         $modelo->diagnostico = $cie->descripcion;
        // 	    $modelo->usuario = Auth::user()['name'];
        //         $modelo->save();
        //     }            
        // } 
	}
}