<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
class Hcl_poblacion_afiliacion extends Model
{ 

	protected $table = 'hcl_poblacion_afiliacion'; 
	public $timestamps = false;

	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('numero_historia',"like","%$dato%")
									->orwhere('codigo','like','%'.$dato.'%') 
									->orwhere('nombre','like','%'.$dato.'%') 
									->orwhere('primer_apellido','like','%'.$dato.'%')
									->orwhere('segundo_apellido','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Hcl_empleador::paginate(config('app.pagination'));	
		}		
	}

}

