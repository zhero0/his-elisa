<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
class Hcl_fichas_registro extends Model
{ 
	protected $table = 'hcl_fichas_registro';  
	public $timestamps = false;
}