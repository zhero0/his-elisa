<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Support\Facades\DB;
class Hcl_fichas_registradas extends Model
{ 
	protected $table = 'hcl_fichas_registradas';  
	public $timestamps = false;

	public static function buscarDato($dato, $idcuaderno, $datosgenericos)
	{			 
		$model =  DB::select(DB::raw('select a.id, a.idhcl_fichas_registro as res_idhcl_fichas_registro, a.numero as res_numero, a.fecha_registro as res_fecha_registro, a.hora_registro as res_hora_registro, a.idhcl_ficha_estado as res_idhcl_ficha_estado,
				b.idhcl_poblacion as res_idhcl_poblacion, 
				c.documento as res_documento, c.primer_apellido as res_primer_apellido, c.segundo_apellido as res_segundo_apellido, c.nombre as res_nombre, 
				d.sigla as res_sigla, d.nombre as res_especialidad,
				e.matricula as res_matricula, e.apellidos as res_ap_medico, e.nombres as res_nombres_medico, f.id as res_ficha_id, f.nombre as res_ficha_nombre
				from hcl_fichas_programadas a
				left join hcl_fichas_registradas b on a.id = b.idhcl_fichas_programadas
				left join hcl_poblacion c on c.id = b.idhcl_poblacion
				left join hcl_cuaderno d on d.id = b.idhcl_cuaderno
				left join datosgenericos e on e.id = b.iddatosgenericos
				left join hcl_ficha_estado f on f.id = a.idhcl_ficha_estado where '.$dato.' and b.idhcl_cuaderno= '.$idcuaderno.' and b.iddatosgenericos = '.$datosgenericos)); 
		return $model;
	}
}