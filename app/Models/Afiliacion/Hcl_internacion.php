<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Afiliacion\Hcl_cabecera_registro;
use Auth;

class Hcl_internacion extends Model
{ 
	protected $table = 'hcl_internacion';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function registraInternacion_Medica($arrayValor)
	{		
		$arrayInternacion = $arrayValor['arrayInternacionMedica'];
		$modeloSalud = $arrayValor['model']; 

		// $almacen = Almacen::find($modeloSalud->idalmacen);
		// $cuaderno = Hcl_cuaderno::find($modeloSalud->idhcl_cuaderno);
		$signosVitales = Hcl_datos_signos_vitales::where([
                        ['eliminado', '=', 0],
                        ['idhcl_cabecera_registro', '=', $modeloSalud->id]
                    ])->get();

		$diagnosticos = Hcl_diagnosticoscie::where([
                        ['eliminado', '=', 0],
                        ['idhcl_cabecera_registro', '=', $modeloSalud->id]
                    ])->get();  

		$signos_vitales = '';
		if (count($signosVitales)>0) {
			foreach ($signosVitales as $dato) {
				if ($dato->contenido_variable != '') {
					$signos_vitales = $signos_vitales.' '.$dato->variable.' '.$dato->contenido_variable;
				}
			}
		}

		$diagnosticos_presuntivos = '';
		if (count($diagnosticos)>0) {
			foreach ($diagnosticos as $dato) {				
				$diagnosticos_presuntivos = $diagnosticos_presuntivos.' '.$dato->codigo.'-'.$dato->diagnostico;				
			}
		}

		// $modelImagenologia = DB::table('ima_registroprogramacion as ir')
		//  			->join('ima_resultados_programacion as irp', 'irp.idima_registroprogramacion', '=', 'ir.id')                  
  //                   ->select('ir.id','irp.examenes_programados')
  //                   ->where([
  //                       ['ir.eliminado', '=', 0],
  //                       ['irp.eliminado', '=', 0],
  //                       ['ir.idhcl_cabecera_registro', '=', $modeloSalud->id]
  //                   ])
  //                   ->get();

        $examenes_complementarios = '';
		// if (count($modelImagenologia)>0) {
		// 	foreach ($modelImagenologia as $dato) {
		// 		$examenes_complementarios = $examenes_complementarios+' '+$dato->examenes_programados;				
		// 	}
		// }
  
		if(count($arrayInternacion) > 0)
		{ 
            $modelo = new Hcl_internacion;
            $modelo->idhcl_cabecera_registro = $modeloSalud->id;

            //$modelo->fecha_registro = $arrayInternacion['0']->fechaprogramacion;
            $modelo->hora_registro = $arrayInternacion['0']->hora_registro;

            $modelo->diagnostico = $diagnosticos_presuntivos;
            $modelo->signos_vitales = $signos_vitales;            
            $modelo->consulta = $modeloSalud->respuesta;
            $modelo->emergencia = $modeloSalud->emergencia;
            $modelo->recomendaciones = $arrayInternacion['0']->recomendaciones;

            $modelo->familiar_datos = $arrayInternacion['0']->familiar_datos;
            $modelo->familiar_movil = $arrayInternacion['0']->familiar_movil;
            $modelo->familiar_domicilio = $arrayInternacion['0']->familiar_domicilio;
            $modelo->familiar_descripcion = $arrayInternacion['0']->familiar_descripcion;

            $modelo->idalmacen = $modeloSalud->idalmacen;
            $modelo->idhcl_poblacion = $modeloSalud->idhcl_poblacion;
            $modelo->idhcl_cuaderno = $arrayInternacion['0']->idEspecialidad;
            $modelo->idalmacen = $arrayInternacion['0']->idAlmacen;
            $modelo->iddatosgenericos = $modeloSalud->iddatosgenericos;
            $modelo->usuario = Auth::user()['name'];
            $modelo->save();
        }
   
	}
}