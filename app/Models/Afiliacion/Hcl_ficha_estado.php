<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
class Hcl_ficha_estado extends Model
{ 
	protected $table = 'hcl_ficha_estado';  
	public $timestamps = false;

	const LIBRE = 1;
	const RESERVADO = 2;
	const OCUPADO = 3;
	const ATENDIDO = 4;
	const NO_ATENDIDO = 1;
}