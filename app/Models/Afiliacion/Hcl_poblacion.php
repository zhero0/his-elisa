<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class Hcl_poblacion extends Model
{ 
    protected $table = 'hcl_poblacion'; 
    public $timestamps = false;
    
    const ID_SEXO_MASCULINO = 1;
    const ID_SEXO_FEMENINO = 2;
    const ID_SEXO_TODOS = 3;
    const SEXO_MASCULINO = 'MAS';
    const SEXO_FEMENINO = 'FEM';
    
    
	public static function muestraDatosPoblacion($request)
	{
		$hcl_poblacion_nombrecompleto = '';
        $hcl_poblacion_matricula = '';
        $hcl_poblacion_cod = '';
        $hcl_poblacion_sexo = '';
        $hcl_poblacion_fechanacimiento = null;
        $hcl_empleador_empresa = '';
        $hcl_empleador_patronal = '';

		$Hcl_poblacion = Hcl_poblacion::where('id', '=', $request->txtidhcl_poblacion)->first();
        if($Hcl_poblacion != null)
        {
            $hcl_poblacion_nombrecompleto = $Hcl_poblacion->nombre.' '.$Hcl_poblacion->primer_apellido.' '.$Hcl_poblacion->segundo_apellido;
            $hcl_poblacion_matricula = $Hcl_poblacion->numero_historia;
            $hcl_poblacion_cod = substr($Hcl_poblacion->codigo, strlen($Hcl_poblacion->codigo)-2);            
            $hcl_poblacion_sexo = $Hcl_poblacion->sexo == 1? 'MAS' : 'FEM';
          
            $hcl_poblacion_fechanacimiento = $Hcl_poblacion->fecha_nacimiento;

            $dato_hcl_empleador = DB::table('hcl_poblacion_afiliacion')->where('idpoblacion', '=', $Hcl_poblacion->id)->first();
            if($dato_hcl_empleador != null)
            {
                $dato_hcl_empleador = DB::table('hcl_empleador')->where('id', '=', $dato_hcl_empleador->idempleador)->first();
                if($dato_hcl_empleador != null)
                {
                    $hcl_empleador_empresa = $dato_hcl_empleador->nombre;
                    $hcl_empleador_patronal = $dato_hcl_empleador->nro_empleador;
                }
            }
        }
        return array(
            'idhcl_poblacion' => $request->txtidhcl_poblacion,
        	'hcl_poblacion_nombrecompleto' => $hcl_poblacion_nombrecompleto,
        	'hcl_poblacion_matricula' => $hcl_poblacion_matricula,
        	'hcl_poblacion_cod' => $hcl_poblacion_cod,
        	'hcl_poblacion_sexo' => $hcl_poblacion_sexo,
        	'hcl_poblacion_fechanacimiento' => $hcl_poblacion_fechanacimiento,
        	'hcl_empleador_empresa' => $hcl_empleador_empresa,
        	'hcl_empleador_patronal' => $hcl_empleador_patronal,
        );
	}

    public static function buscarPacienteServicio($request) {
        error_reporting(0);
        $param = json_decode($request->param, true);
        try {
            $client = new Client();
            $response = $client->post(
                'https://auth-desarrollo.cns.gob.bo/connect/token',
                [
                    'form_params' => [
                        "grant_type" => "password",
                        "username" => "regional-lpz-salud",
                        "password" => "Regional-lpz-2021",
                        "scope" => "afiliaciones",
                        "client_id" => "regional-lpz",
                        "client_secret" => "regional-lpz testing"
                    ]
                ]
            );
            // print_r($param['documento']);
            if($response->getStatusCode() == "200") {
                $data = json_decode($response->getBody(), true);
                $token = $data['access_token'];

                $url = 'https://api-desarrollo.cns.gob.bo/erp/v1/Afiliaciones/Asegurados?DocumentoIdentidad='.$param['documento'].'&FechaNacimiento='.$param['fecha_nacimiento'];
                $response = $client->request('GET', $url, [
                    'headers' => [
                        'Authorization' => 'Bearer '.$token,        
                        'Accept'        => 'application/json',
                    ]
                ]);
                return json_encode([
                    'status' => 'success',
                    'response' => $response->getBody()->getContents()
                ]);
            }else{
                return json_encode([
                    'status' => 'error',
                    'response' => 'No se pudo encontar la página! '
                ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                'status' => 'error',
                'response' => $e->getMessage()
            ]);
        }
    }
    

}