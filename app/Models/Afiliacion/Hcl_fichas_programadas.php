<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
use \App\Models\Administracion\Datosgenericos;
use \App\Models\Afiliacion\Hcl_cuaderno; 
use \App\Models\Afiliacion\Hcl_poblacion;
use \App\Models\Afiliacion\Hcl_empleador;
use \App\Models\Afiliacion\Hcl_poblacion_afiliacion;
use \App\Models\Almacen\Almacen;
use Elibyy\TCPDF\Facades\TCPDF;
use \App\Models\Almacen\Home;
use Illuminate\Support\Facades\DB;
use Auth; 
class Hcl_fichas_programadas extends Model
{ 
	protected $table = 'hcl_fichas_programadas';  
	public $timestamps = false;

	// [ REPORTE: "Ticket placa" ]
    public static function imprime_Ticket_mdt($parametro) 
    {        
        $model = Hcl_fichas_registradas::where([
                                        ['eliminado', '=', 0],
                                        ['idhcl_fichas_programadas', '=', $parametro->id]
                                    ])->first(); 

        $modelCuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
        $modelPoblacion = Hcl_poblacion::find($model->idhcl_poblacion);
        $modelDatos = Datosgenericos::find($model->iddatosgenericos);


        $arrayDatos = Home::parametrosSistema();      
        $modelAlmacen = Almacen::where('id', '=', $arrayDatos['idalmacen'])->first();  
        
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');
        $medidas = array(25, 350);
        ob_end_clean();
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = new TCPDF('P', 'mm', $medidas);
        
        $arrayParametroPDF = [
            'pdf' => $pdf, 
            'mostrarEncabezado' => 1,
            'mostrarPie' => 1,
            'tituloReporte' => 'Ticket',            
            'sello' => 0,
        ];        

        $y_Inicio = 5;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $pdf::AddPage('P','A7 LANDSCAPE');
        $pdf::SetY($y_Inicio);
        $pdf::SetX(2);
        $pdf::SetTitle('SOFTWARE MEDICO');
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(60, $height, 'Ticket de Atención', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        // $pdf::SetFont('dejavusans', '', 10);
        // $pdf::MultiCell(60, $height, 'Radiologia Digital', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont('dejavusans', '', 10);
        $pdf::MultiCell(60, $height, 'ELISA', 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, '', 10);
        $pdf::MultiCell(30, $height, 'Nro de Atención: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10); 
        $pdf::SetFont($tipoLetra, 'B', 13);                 
        $pdf::MultiCell(20, 8, $model->numero,0, 'L', 0, 0, '', '', true, 0, false, true,12, 12);  
        $pdf::SetFont($tipoLetra, '', $tamanio);  
        $pdf::SetX(65);
        $pdf::MultiCell(125, 8, 'Paso 1: Inmediatamente dirigirse al Servicio de Laboratorio para la programación de los examenes complementarios.',0, 'L', 0, 1, '', '', true, 0, false, true,12, 12);   
        $pdf::SetX(65);
        $pdf::MultiCell(125, 8, 'Paso 2: Con la solicitud de radiologia pasar por el Servicio de rayos x, para realizar el examen complementario.',0, 'L', 0, 1, '', '', true, 0, false, true,12, 12);   
        $pdf::SetX(65);
        $pdf::MultiCell(125, 8, 'Paso 3: Usted puede proceder con el trámite de afiliación a partir de '.$fecha.' en la Caja Nacional de Salud seccion de afiliaciones. Calle ravelo No. 20.' ,0, 'L', 0, 1, '', '', true, 0, false, true,12, 12);   
        $pdf::SetX(65);
        $pdf::MultiCell(125, 8, 'Recomendaciones: Las ordenes de examenes complementarios tienen vigencia de 1 mes a partir de la fecha de pago realizado, tomelo en cuenta.',0, 'L', 0, 1, '', '', true, 0, false, true,12, 12);   
        $pdf::SetX(65);
        $pdf::MultiCell(125, 8, 'Se recomienda puntualidad en la cita médica, debiendo estar presente 15 minutos antes con sus formularios.',0, 'L', 0, 1, '', '', true, 0, false, true,12, 12);   
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::SetXY(2,23);
        $pdf::MultiCell(25, $height, 'Especialidad: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(43, $height, $modelCuaderno->nombre, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Medico: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);          
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(35, $height, $modelDatos->apellidos.' '.$modelDatos->nombres, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);     
        $pdf::SetX(2);
        $pdf::MultiCell(60, 5, '                 ', 'T', 'T', 0, 1, '', '', true, 0, false, true, $height, 10);      
        $pdf::SetXY(2,33);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(17, $height, 'Paciente: ', '', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(45, $height, $modelPoblacion->primer_apellido.' '.$modelPoblacion->nombre, '', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);        
        
        
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(15, $height, 'Fecha: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('dejavusans', 'B', 10);
        setlocale(LC_TIME, "spanish");
        $newFecha = date("d-m-Y", strtotime($model->fecha_ficha));
        $diaDesc = strftime("%a", strtotime($newFecha)); 
        // print_r($diaDesc);
        // return;
        $pdf::MultiCell(45, $height, $diaDesc.', '. $model->fecha_ficha, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Hora: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(35, $height, $model->hora, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Lugar: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(35, $height, $modelAlmacen->nombre, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::MultiCell(60, $height, '                 ', 'T', 'T', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $model = DB::table(DB::raw("ima_placasutilizadas_reporte_func(".$arrayDatos['idalmacen'].", ".$tipoPlaca.", '".$fechaDesde."', '".$fechaHasta."')"))
        //             ->select('idima_tipo_placas', 'ima_tipo_placas')
        //             ->groupBy('idima_tipo_placas', 'ima_tipo_placas')
        //             ->orderBy('ima_tipo_placas', 'ASC')
        //             ->get();        

        $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            ); 
            
        // $pdf::write2DBarcode($parametro->id.' - '.$modelPoblacion->documento.'-'.$model->fecha_ficha.' || '.$model->hora, 'QRCODE,L', 2, 53, 25, 25, $style, 'N');        
        $pdf::write2DBarcode('https://www.youtube.com/watch?v=9Y1w6R8sKew&ab_channel=DavidKoskas', 'QRCODE,L', 2, 53, 25, 25, $style, 'N');        
        $pdf::SetY(59);
        $pdf::MultiCell(15, $height, '', '', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, 'Gracias por su', '', 'C', 0, 1, '', '', true, 2, true, true, $height, 10);
        $pdf::MultiCell(15, $height, '', '', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, 'preferencia!', '', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetXY(2,76);
        $pdf::MultiCell(60, $height, '                 ', 'T', 'T', 0, 1, '', '', true, 0, false, true, $height, 10);


        //  $pdf::setFooterCallback(function($pdf) use($usuario, $parametro, $fecha) {
        //      $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     ); 
        //     $pdf->SetY(20);
        //     $pdf->write2DBarcode($parametro->idTicket.' - '.$parametro->tik_nombre.'-'.$parametro->tik_fecha.' - '. $parametro->tik_descripcion, 'QRCODE,L', 10, 87, 35, 35, $style, 'N');
            

        //     $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            
        //     $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);            
            
        // });
        

        // for($i = 0; $i < count($model); $i++)
        // {
        //     $pdf::MultiCell(100, $height, $model[$i]->ima_tipo_placas, 'LB', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //     $pdf::MultiCell(20, $height, '', 'BR', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        //     $modelo = DB::table(DB::raw("ima_placasutilizadas_reporte_func(".$arrayDatos['idalmacen'].", ".$tipoPlaca.", '".$fechaDesde."', '".$fechaHasta."')"))
        //             ->select('ima_tamano_placas', DB::raw('sum(cantidad) as total'))
        //             ->where('idima_tipo_placas', '=', $model[$i]->idima_tipo_placas)
        //             ->groupBy('ima_tamano_placas')
        //             ->orderBy('ima_tamano_placas', 'ASC')
        //             ->get();
        //     for($j = 0; $j < count($modelo); $j++)
        //     {
        //         $pdf::MultiCell(40, $height, '', 'LB', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell(60, $height, $modelo[$j]->ima_tamano_placas, 'B', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell(20, $height, $modelo[$j]->total, 'BR', 'R', 0, 1, '', '', true, 0, false, true, $height, 10);
        //     }
        // }
        
        $nombreArchivo = "Ticket";
        $pdf::Output($nombreArchivo.".pdf", "I");
        mb_internal_encoding('utf-8');
    }
    public static function imprime_Ticket($parametro) 
    {        
        $model = Hcl_fichas_registradas::where([
                                        ['eliminado', '=', 0],
                                        ['idhcl_fichas_programadas', '=', $parametro->id]
                                    ])->first(); 

        $modelCuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
        $modelPoblacion = Hcl_poblacion::find($model->idhcl_poblacion);
        $modelDatos = Datosgenericos::find($model->iddatosgenericos);


        $arrayDatos = Home::parametrosSistema();      
        $modelAlmacen = Almacen::where('id', '=', $arrayDatos['idalmacen'])->first();  
        
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');
        $medidas = array(25, 350);
        ob_end_clean();
        //$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf = new TCPDF('P', 'mm', $medidas);
        
        $arrayParametroPDF = [
            'pdf' => $pdf, 
            'mostrarEncabezado' => 1,
            'mostrarPie' => 1,
            'tituloReporte' => 'Ticket',            
            'sello' => 0,
        ];        

        $y_Inicio = 5;
        $height = 5;
        $width = 190;
        $widthLabel = 28;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        
        $pdf::AddPage('P','A7 LANDSCAPE');
        $pdf::SetY($y_Inicio);
        $pdf::SetX(2);
        $pdf::SetTitle('SOFTWARE MEDICO');
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(60, $height, 'Ticket de Atención', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        // $pdf::SetFont('dejavusans', '', 10);
        // $pdf::MultiCell(60, $height, 'Radiologia Digital', 0, 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont('dejavusans', '', 10);
        $pdf::MultiCell(60, $height, 'ELISA', 'B', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, '', 10);
        $pdf::MultiCell(30, $height, 'Nro de Atención: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10); 
        $pdf::SetFont($tipoLetra, 'B', 13);                 
        $pdf::MultiCell(20, 8, $model->numero,0, 'L', 0, 1, '', '', true, 0, false, true,12, 12);   
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::SetX(2);
        $pdf::MultiCell(25, $height, 'Especialidad: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(43, $height, $modelCuaderno->nombre, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Medico: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);          
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(35, $height, $modelDatos->apellidos.' '.$modelDatos->nombres, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);     
        $pdf::SetX(2);
        $pdf::MultiCell(60, 5, '                 ', 'T', 'T', 0, 1, '', '', true, 0, false, true, $height, 10);      
        $pdf::SetXY(2,33);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(17, $height, 'Paciente: ', '', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(45, $height, $modelPoblacion->primer_apellido.' '.$modelPoblacion->nombre, '', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);        
        
       
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Fecha: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(35, $height, $model->fecha_ficha, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Hora: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont('dejavusans', 'B', 10);
        $pdf::MultiCell(35, $height, $model->hora, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell(25, $height, 'Lugar: ', '', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);        
        $pdf::SetFont($tipoLetra, '', $tamanio);
        $pdf::MultiCell(35, $height, $modelAlmacen->nombre, '', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetX(2);
        $pdf::MultiCell(60, $height, '                 ', 'T', 'T', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $model = DB::table(DB::raw("ima_placasutilizadas_reporte_func(".$arrayDatos['idalmacen'].", ".$tipoPlaca.", '".$fechaDesde."', '".$fechaHasta."')"))
        //             ->select('idima_tipo_placas', 'ima_tipo_placas')
        //             ->groupBy('idima_tipo_placas', 'ima_tipo_placas')
        //             ->orderBy('ima_tipo_placas', 'ASC')
        //             ->get();        

        $style = array(
                'border' => 0,
                'vpadding' => 'auto',
                'hpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255)
                'module_width' => 1, // width of a single module in points
                'module_height' => 1 // height of a single module in points
            ); 
            
        // $pdf::write2DBarcode($parametro->id.' - '.$modelPoblacion->documento.'-'.$model->fecha_ficha.' || '.$model->hora, 'QRCODE,L', 2, 53, 25, 25, $style, 'N');        
        $pdf::write2DBarcode('https://www.youtube.com/watch?v=9Y1w6R8sKew&ab_channel=DavidKoskas', 'QRCODE,L', 2, 53, 25, 25, $style, 'N');        
        $pdf::SetY(59);
        $pdf::MultiCell(15, $height, '', '', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, 'Gracias por su', '', 'C', 0, 1, '', '', true, 2, true, true, $height, 10);
        $pdf::MultiCell(15, $height, '', '', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, 'preferencia!', '', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetXY(2,76);
        $pdf::MultiCell(60, $height, '                 ', 'T', 'T', 0, 1, '', '', true, 0, false, true, $height, 10);


        //  $pdf::setFooterCallback(function($pdf) use($usuario, $parametro, $fecha) {
        //      $style = array(
        //         'border' => 0,
        //         'vpadding' => 'auto',
        //         'hpadding' => 'auto',
        //         'fgcolor' => array(0,0,0),
        //         'bgcolor' => false, //array(255,255,255)
        //         'module_width' => 1, // width of a single module in points
        //         'module_height' => 1 // height of a single module in points
        //     ); 
        //     $pdf->SetY(20);
        //     $pdf->write2DBarcode($parametro->idTicket.' - '.$parametro->tik_nombre.'-'.$parametro->tik_fecha.' - '. $parametro->tik_descripcion, 'QRCODE,L', 10, 87, 35, 35, $style, 'N');
            

        //     $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            
        //     $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);            
            
        // });
        

        // for($i = 0; $i < count($model); $i++)
        // {
        //     $pdf::MultiCell(100, $height, $model[$i]->ima_tipo_placas, 'LB', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //     $pdf::MultiCell(20, $height, '', 'BR', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        //     $modelo = DB::table(DB::raw("ima_placasutilizadas_reporte_func(".$arrayDatos['idalmacen'].", ".$tipoPlaca.", '".$fechaDesde."', '".$fechaHasta."')"))
        //             ->select('ima_tamano_placas', DB::raw('sum(cantidad) as total'))
        //             ->where('idima_tipo_placas', '=', $model[$i]->idima_tipo_placas)
        //             ->groupBy('ima_tamano_placas')
        //             ->orderBy('ima_tamano_placas', 'ASC')
        //             ->get();
        //     for($j = 0; $j < count($modelo); $j++)
        //     {
        //         $pdf::MultiCell(40, $height, '', 'LB', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell(60, $height, $modelo[$j]->ima_tamano_placas, 'B', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        //         $pdf::MultiCell(20, $height, $modelo[$j]->total, 'BR', 'R', 0, 1, '', '', true, 0, false, true, $height, 10);
        //     }
        // }
        
        $nombreArchivo = "Ticket";
        $pdf::Output($nombreArchivo.".pdf", "I");
        mb_internal_encoding('utf-8');
    }

    public static function imprimirLista_atencion($parametro)
    {     
        $arrayDatos = Home::parametrosSistema(); 

        //---varables del reporte----------------    
        $titulo = 'LISTA DE ATENCION MEDICA';
        $path_logo='imagenes/logo.jpg';
        $titulo_cns="CAJA NACIONAL DE SALUD";
        
        $fecha=$parametro->fechaFind;


        // datos de control
        $pfecha_registro = $parametro->fechaFind;                                  // fecha de atencion
        $piddatosgenericos = $parametro->iddatosgenericos;             // id medico            tabla datosgenericos
        $pidhcl_cuaderno = $parametro->idhcl_cuaderno;                                         // dato de especialidad 
 

        $modelHcl_cuaderno = Hcl_cuaderno::find($pidhcl_cuaderno); 

        // UNIDAD MEDICA
        $modelAlmacen = Almacen::find($arrayDatos['idalmacen']);
        $regional= $modelAlmacen->descripcion;

        // DATOS MEDICO
        $modelDatosgenericos = Datosgenericos::find($piddatosgenericos);
 
        // $modeloHcl_cabecera_registro = DB::table(DB::raw("hcl_informe_formulario_202('".$pidhcl_cuaderno."','".$pfecha_registro."','".$piddatosgenericos."')"))->get();  
        $modelo = DB::table(DB::raw("hcl_lista_impresion(".$piddatosgenericos.",".$pidhcl_cuaderno.",'".$pfecha_registro."')"))->get();
        
        // print_r($modelo);
        // return;
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // $pageLayout = array(216,330); //  tamaño Oficio Bolivia
        $pageLayout = array(279,216); //  tamaño Carta Bolivia

        $pdf::SetTitle("LISTA DE CONSULTA");
        $pdf::SetSubject("TCPDF");
        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $centro='CIMFA SUCRE 25 DE MAYO';

        $pdf::setHeaderCallback(function($pdf) use($titulo,$titulo_cns,$regional,$centro,$modelHcl_cuaderno){
        //--cabecera PDF First Page -TITULO
        $pdf->SetFont('helvetica', '', 11);
        $pdf->SetXY(5,5);
        $pdf->Cell(320, 0, $centro, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(310,5);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setJPEGQuality(75);
        $pdf->SetXY(5,5);
        // $pdf->Image($path_logo, '', '', 15, 15, '', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $pdf->SetXY(30,5);
        $pdf->SetFont('helvetica', '', 9);
        $pdf->Cell(10, 0, $titulo_cns, 0, 0, 'C', 0, '', 0);
        //---lineas
        $style = array('width' => 0.2, 'cap' => 'round', 'join' => 'rmiter', 'dash' => 0, 'color' => array(0, 0, 0));       
        //CABECERA PRINCIPAL
        $pdf->Ln();
        $pdf->Cell(0, 0, 'LISTA DE ATENCION', 0, 0, 'C', 0, '', 0);
        $pdf->Ln();
        $pdf->Cell(0, 0, $modelHcl_cuaderno->nombre, 0, 0, 'C', 0, '', 0);
        $pdf->SetXY(210,17);               
        //----CABECERA DE TABLA
        $pdf->SetFillColor(211,211,211);
        //$pdf->Line(10,35, 320, 35, $style);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->SetXY(10,20);
        $pdf->Cell(10, 7, 'No.', 1, 0, 'C', 1, 7, 7);
        $pdf->Cell(20, 7, 'Hora', 1, 0, 'C', 1, '', 7);
        // $pdf->Cell(30, 0, 'Ap.Paterno', 1, 0, 'C', 1, '', 0);
        // $pdf->Cell(30, 0, 'Ap.Materno', 1, 0, 'C', 1, '', 0);
        $pdf->Cell(78, 7, 'Paciente', 1, 0, 'C', 1, '', 7);
        $pdf->Cell(55, 7, 'Empresa', 1, 0, 'C', 1, '', 7);        
        $pdf->Cell(23, 7, 'Movil', 1, 0, 'C', 1, '', 7);
        // $pdf->Cell(20, 0, 'Consulta', 1, 0, 'C', 1, '', 0);
        // $pdf->Cell(100, 0, 'Diagnostico', 1, 0, 'C', 1, '', 0);
        // $pdf->Cell(30, 0, 'Consultorio Asignado', 1, 0, 'C', 1, '', 0);        
        });
        
        // $pdf::setFooterCallback(function($pdf){
        // // $pdf->setY(-15);   
        // $pdf->SetFont('helvetica', '', 8);
        // //$pdf->SetFillColor(255,255,255);
        // $pdf->Cell(0, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, 0, 'R',0, '', 0);   
        // });

        // $pdf::AddPage('L',$pageLayout);
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetMargins(10, 23.2, 10, true);
        $pdf::SetXY(10,28);        
        $pdf::SetFont('helvetica', '', 10);
        $pdf::SetFillColor(211,211,211);
        $pdf::Cell(186,0,$fecha.' '. $modelo[0]->res_ap_medico.' '.$modelo[0]->res_nombres_medico, 1, 1, 'L', 1, '', 0);
        $i=0;        
            //-------datos del formulario 
           // for ($i=0; $i < 120; $i++) { 
           //  $pdf::SetX(10);
           //  $pdf::SetFont('helvetica', '', 7);
           //  $pdf::Cell(40, 0, 'Hora', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(25, 0, 'N°HC', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Ap.Paterno', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Ap.Materno', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(35, 0, 'Nombre(s)', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Edad', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(20, 0, 'Consulta', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(70, 0, 'Diagnostico', 1, 0, 'C', 0, '', 0);
           //  $pdf::Cell(30, 0, 'Consultorio Asignado', 1, 0, 'C', 0, '', 0);
           //  $pdf::Ln();
           // }
        $alto = 6; 
        foreach ($modelo as $dato) {
 
            //var_dump($dato);
             

            // PRIMER DIAGNOSTICO REGISTRADO
             
            $pdf::SetX(10);
            $pdf::SetFont('courier', '',10);
            $pdf::Cell(10, $alto, $dato->res_numero, 1, 0, 'C', 0, '', 0); 
            $pdf::Cell(20, $alto, $dato->res_hora_registro, 1, 0, 'C', 0, '', 0); 
            $pdf::SetFont('courier', '',10);
            $pdf::Cell(78, $alto, $dato->res_primer_apellido.' '.$dato->res_segundo_apellido.' '.$dato->res_nombre, 1, 0, 'L', 0, '', 0);
            $pdf::SetFont('courier', '',8);
            $pdf::Cell(55, $alto, $dato->empresa, 1, 0, 'C', 0, 3, 1);             
            $pdf::Cell(23, $alto, $dato->res_telefono, 1, 0, 'C', 0, '', 0); 
            $pdf::Ln();
        }
        $pdf::SetFillColor(211,211,211);
        // $pdf::Cell(0,0,'', 1, 0, 'L', 1, '', 0);
        $pdf::SetX(10);
        $pdf::Cell(40, 0, 'Total: '.sizeof($modelo), 1, 0, 'R', 0, '', 0);

        //---exportar PDF
        $pdf::Output('formulario_202.pdf', 'I');
        mb_internal_encoding('utf-8');

    }

     public static function imprime_Estudios($parametro)
    {   
        $imprimirDirecto = 1; 
        // $medidas = array(200, 600); // Ajustar aqui segun los milimetros necesarios;
        // $pdf = new TCPDF('P', 'mm', $medidas, true, 'UTF-8', false); 

        ob_end_clean();            

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // $pdf = new TCPDF('L', PDF_UNIT, 'A1', true, 'UTF-8', false);
        
        $pdf::SetTitle("Estudios");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(5, 0, 0, 0);
        // $pdf::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER); 

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);   

        $pdf::SetAutoPageBreak(TRUE, 0);
        

        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 10;
        $height = 4;
        $width = 190;
        $widthLabel = 21;
        $widthDosPuntos = 4;
        $widthInformacion = 40;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 50;  

        $model = Hcl_fichas_registradas::where([
                                        ['eliminado', '=', 0],
                                        ['idhcl_fichas_programadas', '=', $parametro->id]
                                    ])->first(); 
        $model->fecha_actual = date('d-m-Y');  

        $modelCuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
        $modelPoblacion = Hcl_poblacion::find($model->idhcl_poblacion);

        $modelEdad = Hcl_poblacion::select(DB::raw("date_part('year',age(fecha_nacimiento)) as edad"))
                    ->find($model->idhcl_poblacion); 

        $modelAfiliacion = Hcl_poblacion_afiliacion::where([
                                        ['eliminado', '=', 0],
                                        ['idpoblacion', '=', $model->idhcl_poblacion]
                                    ])->first();

        $modelEmpleador = Hcl_empleador::find($modelAfiliacion->idempleador);
        $modelDatos = Datosgenericos::find($model->iddatosgenericos);


        $arrayDatos = Home::parametrosSistema();      
        $modelAlmacen = Almacen::where('id', '=', $arrayDatos['idalmacen'])->first(); 

 
        for ($i=1; $i <= 3; $i++) 
        { 
            $pdf::AddPage();
            $pdf::SetY($y_Inicio); 
            
            $pdf::SetFillColor(230, 230, 230);  
            $pdf::SetFont('helvetica', 'B', 11);
            if ($i == 1) {
                $pdf::MultiCell(45, 8, 'RADIODIAGNOSTICO', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M');
            }
            else{
                $pdf::MultiCell(37, 8, 'LABORATORIO', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M');
            }  
            $pdf::SetFont('helvetica', '', 11);
            // $pdf::MultiCell(90, 8, $cantidad_medicamentos->descripcion, 0, 'R', '', 1, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::MultiCell(90, 8, '', 0, 'R', '', 1, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('helvetica', 'B', 15);

            $pdf::MultiCell(100, 8, 'MEDICINA DEL TRABAJO', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('helvetica', 'B', 14);
            // $pdf::MultiCell(30, 8, 'FOLIO N°', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            // $pdf::MultiCell(30, 8, '', 1, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            // $pdf::MultiCell(30, 8, '', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            
            // $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(38, 8, '', 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M'); 
            $pdf::SetFont('courier', '', 13);
            // $pdf::MultiCell(65, 8, 'Clave Farmacéutico', 0, 'L', '', 1, '', '', 1, '', '', '', 8, 'M');
            $pdf::MultiCell(65, 8, '', 0, 'L', '', 1, '', '', 1, '', '', '', 8, 'M');
            // $pdf::Line(100, 40, 130, 40); 
  
            // ************************************************************************************************
            $pdf::SetFont('courier', '', 9); 
            $pdf::SetFont($tipoLetra, 'B', $tamanio);
            $pdf::MultiCell($widthTitulo, $height, 'DATOS ', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());
            $pdf::SetFont($tipoLetra, '', $tamanio);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);
            
            $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO ','B', 'L', 1, 0, '', '', true, 0, false, '', $height, 50);

            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(50, $height, 'PARTICULAR', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(25, $height, 'DOCUMENTO ',0, 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $modelPoblacion->documento, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            $pdf::MultiCell($widthLabel, $height, 'PACIENTE ', 0, 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(70, $height, $modelPoblacion->primer_apellido.' '.$modelPoblacion->segundo_apellido.' '.$modelPoblacion->nombre,0, 'L', 0,1, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell(13, $height, 'EDAD ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell(20, $height, ' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            $pdf::MultiCell($widthLabel, $height, 'EMPRESA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(100, $height,$modelEmpleador->nro_empleador.' - '.$modelEmpleador->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell(100, $height,$dato->hcl_empleador_patronal.' - '.$dato->hcl_empleador_empresa, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
            $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);
            $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';

            $modelHcl_cuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
            $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';
            $pdf::SetFont($tipoLetra, '', $tamanio);
            
            $label_Hospitalizado = 'FECHA';
            $label_Establecimiento = 'Establecimiento';
            $label_Medico = 'MÉDICO SOL.';
            $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
            $y_Tipo = ceil($pdf::GetStringWidth($label_Establecimiento, $tipoLetra, '', $tamanio, false));
            $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
            $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico);

            $label_Fecha = 'F. NAC.';
            $label_Especialidad = 'ESPECIALIDAD';
            $label_Diagnostico = 'DIAGNÓSTICO';
            $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
            $y_Especialidad = ceil($pdf::GetStringWidth($label_Establecimiento, $tipoLetra, '', $tamanio, false));
            $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));                
            $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);
 
            $fechaprogramacion = date('d-m-Y', strtotime($model->fecha_actual));
            $pdf::MultiCell(21, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(25, $height,$fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(15, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(30, $height, $modelPoblacion->fecha_nacimiento, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(13, $height, 'EDAD', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(20, $height, $modelEdad->edad, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            // $pdf::MultiCell($y_label_maximo_col1, $height, $label_Establecimiento, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell(80, $height, $medicamentos[0]->descripcion_almacen, 0, 'L', 1, 1, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, $model->cuaderno, 0, 'L', 1, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(48, $height, $modelDatosgenericos->matricula.' - '.$model->personal, 0, 'L', 1, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, 2, '  ',0, 'L', 0, 1, '', '', true, 0, false, '', 2, 10);

            
            $pdf::MultiCell($y_label_maximo_columna2, $height,  '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($espacio, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(45, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 

            $pdf::SetFont($tipoLetra, 'B', $tamanio); 

            if ($i == 1) {
                $pdf::MultiCell(30, $height, 'ESTUDIO ', 0, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
                $pdf::MultiCell(26, $height, 'PROYECCION', 0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10); 
                $pdf::SetFont('courier', '', 12);
                $heightCol = 5;
                $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());
                $pdf::MultiCell(30, $heightCol, 'TORAX', 0, 'C', 0, 0, '', '', true, 0, false, true, 20, 10);
                $pdf::SetFont('courier', '', 10);
                $pdf::MultiCell(26, 20, 'P-A' ,0, 'C', 0, 1, '', '', true, 0, false, true, 20, 10); 
                if ($modelPoblacion->sexo == 2) {
                    $pdf::SetFont('courier', 'B', 12);
                    $pdf::MultiCell(125, $height, 'DATOS CLINICOS ','B', 'C', 0, 1, '', '', true, 0, false, true, 40, 10);                    
                    $pdf::SetFont('courier', '', 12);
                    $pdf::MultiCell(125, 35, 'FUM =                     ???? ',0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10);
                }
            }
            if ($i == 2){
                $pdf::MultiCell(50, $height, 'EXAMEN DE ORINA ', 0, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
                $pdf::MultiCell(26, $height, '', 0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10); 
                $pdf::SetFont('courier', '', 12);
                $heightCol = 5;
                $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());
                if ($modelPoblacion->sexo == 2) {
                    $pdf::MultiCell(65, $heightCol, 'Examen Físico Químico', 0, 'L', 0, 0, '', '', true, 0, false, true, 20, 10);
                }else
                    $pdf::MultiCell(65, 35, 'Examen Físico Químico', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);

                $pdf::SetFont('courier', '', 10);
                $pdf::MultiCell(26, $heightCol, '' ,0, 'C', 0, 1, '', '', true, 0, false, true, 20, 10); 
                if ($modelPoblacion->sexo == 2) {
                    $pdf::MultiCell(65, 35, 'Citobacterlógico', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);                     
                } 
            }
            if ($i == 3) {
                $pdf::MultiCell(50, $height, 'EXAMEN HEMATOLOGICO', 0, 'C', 0, 0, '', '', true, 0, false, true, 40, 10);
                $pdf::MultiCell(26, $height, '', 0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10); 
                $pdf::SetFont('courier', '', 12);
                $heightCol = 5;
                $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());
                $pdf::MultiCell(65, $heightCol, 'Cuadro Hemático', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(65, $heightCol, 'Grupo sanguíneo y R.H.', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(65, $heightCol, 'R. de plaquetas', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(65, $heightCol, 'Creatinina', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(65, $heightCol, 'Glucosa', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(65, $heightCol, 'R. de V.D.R.L.', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10);
                $pdf::MultiCell(65, 30, 'Serologia chagas', 0, 'L', 0, 1, '', '', true, 0, false, true, 20, 10); 
            }
           
            $pdf::SetFont('courier', 'B', 17);
            $pdf::MultiCell(125, $height, 'PRE-OCUPACIONAL ',0, 'C', 0, 1, '', '', true, 0, false, true, 40, 10);
    // ------------------------------ [FIN MEDICAMENTOS] ---------------------------------------               
      
            $style = array(
                        'position' => 'L',
                        'align' => 'L',
                        'stretch' => false,
                        'fitwidth' => true,
                        'cellfitalign' => '',
                        'border' => false,
                        'hpadding' => 'auto',
                        'vpadding' => 'auto',
                        'fgcolor' => array(0,0,0),
                        'bgcolor' => false, //array(255,255,255),
                        'text' => true,
                        'font' => 'helvetica',
                        'fontsize' => 8,
                        'stretchtext' => 4
                    ); 
                
                
            $pdf::write2DBarcode($model->id.'-'.$modelPoblacion->primer_apellido.' '.$modelPoblacion->segundo_apellido.' '.$modelPoblacion->nombre, 'QRCODE,L', 10, 145, 25, 25, $style, 'N');
            $pdf::MultiCell('', $height,'', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::Line(5, $pdf::getY(), 130, $pdf::getY());

            $pdf::SetFont('courier', '', 9);
            $pdf::SetY(160);
            $pdf::MultiCell(50, '', '', 0, 'C', 0, 0); 
            $pdf::MultiCell(35, $height, 'FIRMA MEDICO', 'T', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(35, $height, 'SELLO MEDICO', 'T', 'C', 0, 1, '', '', true, 0, false, true, $height, 10);          

            $distancia_medicamentos = 175;
            $pdf::SetY($distancia_medicamentos);
            $pdf::MultiCell(45, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell($widthLabel, $height, ' ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(70, $height, '', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(14, $height, $label_Fecha.': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            // $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
            $pdf::MultiCell(19, $height, $fechaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
           
            $pdf::MultiCell('', $height,'', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10); 
               
                 
            
        } 
        // return;
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("impresion_estudios.pdf", "I");
        
        mb_internal_encoding('utf-8');
    }
}