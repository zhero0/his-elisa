<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_especialidad extends Model
{
	protected $table = 'hcl_especialidad';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	// public static function generarNumero($idalmacen)
	// {
	// 	$numero = Lab_examenes::where('id_almacen', '=', $idalmacen)->max('numero');
	// 	return $numero+1;
	// }
	const Consulta_Externa = 1;
	const Emergencias = 2;
	const Embarazo = 3;
	const PARTICULAR = 4;
	const cuaderno = 1;
}