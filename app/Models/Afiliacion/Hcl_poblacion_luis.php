<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
class Hcl_poblacion extends Model
{ 

	protected $table = 'hcl_poblacion'; 
	//protected $fillable = array('id', 'codigo', 'nombre','descripcion','usuario' );
	public $timestamps = false;

	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('numero_historia',"like","%$dato%")
									->orwhere('codigo','like','%'.$dato.'%') 
									->orwhere('nombre','like','%'.$dato.'%') 
									->orwhere('primer_apellido','like','%'.$dato.'%')
									->orwhere('segundo_apellido','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Hcl_poblacion::paginate(config('app.pagination'));	
		}		
	} 

}

