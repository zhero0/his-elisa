<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_clasificacionsalud extends Model
{ 
	protected $table = 'hcl_clasificacionsalud';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	const DELGADEZ_SEVERA = 1;
	const DELGADEZ_MODERADA = 2;
	const DELGADEZ_ACEPTABLE = 3;
	const NORMAL = 4;
	const SOBREPESO = 5;
	const OBESO_1 = 6;
	const OBESO_2 = 7;
	const OBESO_3 = 8;
}