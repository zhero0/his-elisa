<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_datos_signos_vitales extends Model
{ 
	protected $table = 'hcl_datos_signos_vitales';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	const TALLA = 1;
	const PESO = 2;
}