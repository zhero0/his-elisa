<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_registro_bajas_medicas extends Model
{ 
	protected $table = 'hcl_registro_bajas_medicas';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}