<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_cabecera_registro extends Model
{ 
	protected $table = 'hcl_cabecera_registro';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at

	public static function generarNumero($idalmacen)
	{
		$numero = Hcl_cabecera_registro::where('idalmacen', '=', $idalmacen)->max('numero');
		return $numero+1;
	}
}