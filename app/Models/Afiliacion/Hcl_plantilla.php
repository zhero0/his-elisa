<?php

namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model; 
class Hcl_plantilla extends Model
{ 

	protected $table = 'hcl_plantilla'; 
	//protected $fillable = array('id', 'codigo', 'nombre','descripcion','usuario' );
	public $timestamps = false;

	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('nombre','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Hcl_empleador::paginate(config('app.pagination'));	
		}		
	}

}

