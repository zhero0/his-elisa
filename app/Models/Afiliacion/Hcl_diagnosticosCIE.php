<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Afiliacion\Hcl_cie;
use Auth;

class Hcl_diagnosticosCIE extends Model
{ 
    protected $table = 'hcl_diagnosticoscie';
    public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at


    const DIAGNOSTICO_BASICO = 1;

    public static function registraDiagnosticos($arrayValor)
    {
        $arrayDiagnosticos = $arrayValor['arrayDiagnosticos'];
        $modeloSalud = $arrayValor['model']; 
    
        //print_r('expression');
        //return;
        if(count($arrayDiagnosticos) > 0)
        {
            for($i = 0; $i < count($arrayDiagnosticos); $i++)
            { 
                $modelo = new Hcl_diagnosticosCIE;
                $cie = Hcl_cie::find($arrayDiagnosticos[$i]);
                $modelo->numero = $i+1;
                $modelo->idhcl_cabecera_registro = $modeloSalud->id;
                $modelo->idhcl_cie = $cie->id;
                $modelo->codigo = $cie->codigo;
                $modelo->diagnostico = $cie->descripcion;
                $modelo->usuario = Auth::user()['name'];
                $modelo->save();
            }            
        } 
    }
}