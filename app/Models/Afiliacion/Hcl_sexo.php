<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;

class Hcl_sexo extends Model
{ 
	protected $table = 'hcl_sexo';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
}