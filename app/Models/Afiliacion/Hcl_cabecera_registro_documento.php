<?php
namespace App\Models\Afiliacion;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Hcl_cabecera_registro_documento extends Model
{
	protected $table = 'hcl_cabecera_registro_documento';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public static function registraArchivos($arrayParametro)
	{
        $files = $arrayParametro['files'];
        $id = $arrayParametro['id'];
        
        foreach ($files as $file)
        {
            $name = $file->getClientOriginalName();
            $nombre = $id.'_'.$name;
            // $file->move(public_path('uploads/Salud/2018'), $nombre);
            $file->move(config('app.rutaArchivosSalud'), $nombre);
            
            $modelo = new Hcl_cabecera_registro_documento();
            $modelo->archivo = $nombre;
            $modelo->idhcl_cabecera_registro = $id;
            $modelo->usuario = Auth::user()['name'];
            $modelo->save();
        }
    }

}