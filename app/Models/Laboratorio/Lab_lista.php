<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_lista extends Model
{ 
	protected $table = 'lab_lista'; 
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('nombre',"like","%$dato%");
								// ->orwhere('usuario','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Lab_especialidad::paginate(config('app.pagination'));	
		}		
	}
	
	public static function registraDetalle($arrayDatos)
	{
		$modelo = $arrayDatos['model'];
		$request = $arrayDatos['request'];
		$listaTipoVariable = $arrayDatos['listaTipoVariable'];
		
		if($listaTipoVariable != '')
		{
			for($i = 0; $i < count($listaTipoVariable); $i++)
	        {
	        	$model = new lab_lista;
	        	$model->lis_variable = $listaTipoVariable[$i]->nombreTipoVariable;
	        	$model->idlab_variables = $modelo->id;
	        	$model->lis_orden = 0;
	        	$model->usuario = Auth::user()['name'];
	        	$model->save();
	        }
		}
		else
		{
			$model = new lab_lista;
        	$model->lis_variable = '';
        	$model->idlab_variables = $modelo->id;
        	$model->lis_orden = 0;
        	$model->save();
		}
	}
	
}