<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_examenes_registroprogramacion extends Model
{
	protected $table = 'lab_examenes_registroprogramacion'; 
	public $timestamps = false;
	
	public static function registraExamenRegistroprogramacion($arrayDatos, $arrayExamenes)
	{
		$request = $arrayDatos['request'];
		$modelo = $arrayDatos['model'];
		$array_especialidad = [];

		for($i = 0; $i < count($arrayExamenes); $i++)
        {
        	$model = new Lab_examenes_registroprogramacion;
        	$model->idlab_registroprogramacion = $modelo->id;
        	$model->idlab_examenes = $arrayExamenes[$i];
        	$model->usuario = Auth::user()['name'];
        	if($model->save())
			{
	        	$modelLab_examenes = Lab_examenes::where('id', '=', $arrayExamenes[$i])->first();
	            array_push($array_especialidad, $modelLab_examenes->idlab_especialidad);
            }
        }
        return $array_especialidad;
	}
	
}