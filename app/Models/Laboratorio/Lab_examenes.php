<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_examenes extends Model
{
	protected $table = 'lab_examenes';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public static function generarNumero($idalmacen)
	{
		$numero = Lab_examenes::where('idalmacen', '=', $idalmacen)->max('numero');
		return $numero+1;
	}
	public static function registraExamenes($request, $idlab_grupos) 
	{
		$examenes = json_decode($request->txtJsonExamenes);
        $orden = 1;
        for($i = 0; $i < count($examenes); $i++)
        {
        	$Lab_examenes = Lab_examenes::where('id', '=', $examenes[$i])->first();
        	
            $model = new lab_perfil_examenes;
            $model->orden = $orden;
            $model->idlab_examenes = $examenes[$i];
            $model->idlab_grupos = $idlab_grupos;
            $model->idalmacen = $request->idalmacen;
            $model->codigo = $Lab_examenes->codigo;
            $model->nombre = $Lab_examenes->nombre;
            $model->usuario = Auth::user()['name'];
            $model->save();
            
            $orden++;
        }
	}
	// public static function registraExamenes($arrayDatos)
	public static function registraExamenesX($request)
	{
		// $request = $arrayDatos['request'];
		// $examenes = json_decode($request->txtListaVariables);
		$examenes = json_decode($request->txtJsonExamenes);
		
        for($j = 0; $j < count($examenes); $j++)
        {
        	$model = new lab_perfil_examenes;
            $model->orden = $examenes[$j]->orden;
            $model->idlab_examenes = $examenes[$j]->id;
            $model->idlab_grupos = $examenes[$j]->idlab_grupos;
            $model->idalmacen = $request->idalmacen;
            $model->nombre = '';
            $model->usuario = Auth::user()['name'];
            $model->save();
        }
	}
}