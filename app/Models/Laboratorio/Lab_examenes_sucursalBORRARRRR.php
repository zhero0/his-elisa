<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_examenes_sucursal extends Model
{
	protected $table = 'lab_examenes_sucursal'; 
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public static function registraExamenUnidadMedica($arrayDatos)
	{
		$request = $arrayDatos['request'];
		$modelo = $arrayDatos['model'];
		$arrayUnidadesMedicas = json_decode($request->txtListaUnidadesMedicas);
		
		for($i = 0; $i < count($arrayUnidadesMedicas); $i++)
        {
        	$model = new Lab_examenes_sucursal;
        	$model->idlab_examenes = $modelo->id;
        	$model->id_sucursal = $arrayUnidadesMedicas[$i];
        	$model->usuario = Auth::user()['name'];
        	$model->save();
        }		
	}

}