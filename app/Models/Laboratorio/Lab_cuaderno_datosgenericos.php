<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

class Lab_cuaderno_datosgenericos extends Model
{
	protected $table = 'lab_cuaderno_datosgenericos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
}