<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use \App\Models\Imagenologia\Ima_estado;
use \App\Models\Imagenologia\Ima_examenes_registroprogramacion;

use \App\Models\Laboratorio\Lab_resultados_programacion;

use Illuminate\Support\Facades\DB;
use Auth;

use \App\Models\Administracion\Datosgenericos;
use Elibyy\TCPDF\Facades\TCPDF;

class Lab_resultados_programacion extends Model
{ 
    protected $table = 'lab_resultados_programacion'; 
    public $timestamps = false;
    
    // public static function generarNumero($idalmacen)
    // {
    //  $numero = Ima_resultados_programacion::where('idalmacen', '=', $idalmacen)->max('numero');
    //  return $numero+1;
    // }

    // public static function obtieneDatosNOGuardados($parametro) {
    //     $modelIma_registroprogramacion = $parametro['modelIma_registroprogramacion'];
    //     $model = $parametro['model'];
        
    //     $examenesProgramados = DB::table('lab_examenes_registroprogramacion as erp')
    //                             ->join('lab_examenes as e', 'erp.idlab_examenes', '=', 'e.id')
    //                             ->select('erp.idlab_examenes', 'e.codigo', 'e.nombre')
    //                             ->where([
    //                                 ['erp.eliminado', '=', 0],
    //                                 ['erp.idlab_registroprogramacion', '=', $modelIma_registroprogramacion->id]
    //                             ])->orderBy('e.nombre', 'ASC')->get();
    // }

    public static function obtieneDatosGuardados_Resultado($parametro) {
        $modelIma_registroprogramacion = $parametro['modelIma_registroprogramacion'];
        $model = $parametro['model'];
        
        // $examenesProgramados = DB::table('lab_resultados_examen as re')
        //                         ->select('re.idlab_examenes', 're.codigo_examen as codigo', 're.nombre_examen as nombre', 're.respuesta')
        //                         ->where([
        //                             ['re.is_resultado', '=', 0],
        //                             ['re.idlab_resultados_programacion', '=', $model->id]
        //                         ])->orderBy('re.nombre_examen', 'ASC')->get();
        
        $retorno = array(
            'examenesProgramados' => $examenesProgramados,
            'bb' => 322,
        );
        return $retorno;
    }

    public static function registraResultado($arrayDatos) {
        $request = $arrayDatos['request'];
        $model = $arrayDatos['model'];
        $arrayExamenes = $arrayDatos['arrayExamenes'];
        
        $array_especialidad = Lab_examenes_registroprogramacion::registraExamenRegistroprogramacion($arrayDatos, $arrayExamenes);
        if($array_especialidad != null)
        {
            $array_especialidad = array_unique($array_especialidad);
            foreach ($array_especialidad as $valor)
            {
                $modelo = new Lab_resultados_programacion();
                $modelo->idalmacen = null;
                $modelo->iddatosgenericos = null;
                $modelo->idlab_registroprogramacion = $model->id;
                $modelo->idlab_especialidad = $valor;
                $modelo->usuario = Auth::user()['name'];
                $modelo->idlab_estado = Lab_estado::PROGRAMACION;
                
                // [ DATOS DEL PACIENTE ]
                $Hcl_poblacion = Hcl_poblacion::muestraDatosPoblacion($request);
                $modelo->idhcl_poblacion = $Hcl_poblacion['idhcl_poblacion'];
                $modelo->hcl_poblacion_nombrecompleto = $Hcl_poblacion['hcl_poblacion_nombrecompleto'];
                $modelo->hcl_poblacion_matricula = $Hcl_poblacion['hcl_poblacion_matricula'];
                $modelo->hcl_poblacion_cod = $Hcl_poblacion['hcl_poblacion_cod'];
                $modelo->hcl_poblacion_sexo = $Hcl_poblacion['hcl_poblacion_sexo'];
                $modelo->hcl_poblacion_fechanacimiento = $Hcl_poblacion['hcl_poblacion_fechanacimiento'];
                $modelo->hcl_empleador_empresa = $Hcl_poblacion['hcl_empleador_empresa'];
                $modelo->hcl_empleador_patronal = $Hcl_poblacion['hcl_empleador_patronal'];
                
                $modelo->save();
            }
        }
    }

    public static function imprimeProgramacionResultado($id) {
        $imprimirDirecto = 0;

        $model = DB::table('lab_resultados_programacion')
                ->select('*', DB::raw("COALESCE(TIMESTAMPDIFF(YEAR, hcl_poblacion_fechanacimiento, CURDATE()), 0) as edad"))
                ->where('id', '=', $id)
                ->first();
        // echo "==> ".$id;
        // return;

        $modelLab_registroprogramacion = Lab_registroprogramacion::where([
                                ['eliminado', '=', 0],
                                ['id', '=', $model->idlab_registroprogramacion]
                            ])->first();

        $numeroImprimir = $modelLab_registroprogramacion->numero;
        
        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Laboratorio");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir) {
            $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);

            // ---------------------------------------------------------------------------------------------------
            $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            $pdf->SetY($y + 1);
            $pdf->Image('imagenes2/page-2_img08.jpg', 11, '', '', 16);
            $pdf->SetFont('courier', 'B', 20);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetY($pdf->GetY());
            // $x = $pdf->getPageWidth();
            // $y = $pdf->getPageHeight();
            $pdf->MultiCell(130, 10, 'LABORATORIO C.N.S.', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
            $pdf->MultiCell(10, 10, '', 0, 'C', '', 0, '', '', 1, '', '', '', 10, 'M');
            $pdf->SetFont('helvetica', 'B', 16);
            $pdf->MultiCell(50, 10, 'Nº FOLIO: 1582', 1, 'C', '', 1, '', '', 1, '', '', '', 10, 'M');

            $pdf->SetFont('courier', 'B', 16);
            $pdf->MultiCell(18, 8, '', 0, 'R', '', 0, '', '', 1, '', '', '', 8, 'M');
            $pdf->MultiCell(150, 8, 'RESULTADO DEL EXÁMEN Nº '.$numeroImprimir, 0, 'C', '', 1, '', '', 1, '', '', '', 8, 'M');
            // $pdf->Line(31, $pdf->getY()+6, $pdf->getPageWidth()-12, $pdf->getY()+6);
            // ---------------------------------------------------------------------------------------------------


            /*
            $pdf->SetFont('courier', 'B', 10);
            $pdf->MultiCell(130, 5, 'LABORATORIO C.N.S.', 0, 'L', '', 1, '', '', 1, '', '', '', 5, 'M');
            
            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->MultiCell(130, 8, 'RESULTADO DEL EXÁMEN Nº '.$numeroImprimir, 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M');
            */
            // $pdf->Line(10, $pdf->getY()+7, $pdf->getPageWidth()-12, $pdf->getY()+7);
        });

        // setPage
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');
        $pdf::setFooterCallback(function($pdf) use($usuario, $fecha) {
            // $pdf->SetY(-5);
            $pdf->SetY(-10);
            $pdf->SetFont('courier', 'B', 7);
            $pdf->MultiCell('', '', 'Usuario: '.$usuario, 'T', 'L', 0);
            $pdf->SetY(-10);
            $pdf->MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            $pdf->SetY(-10);
            $pdf->MultiCell(40, '', 'Página ' . $pdf->getGroupPageNo() . ' de ' . $pdf->getPageGroupAlias(), 0, 'R', 0, 1,$pdf->getPageWidth() - 45);
        });


        // $pdf::AddPage();
        // $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        $y_Inicio = 28;
        // $y_Inicio = 18;
        $height = 5;
        $width = 190;
        $widthLabel = 25;
        $widthDosPuntos = 4;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 42;
        
        $pdf::AddPage('P', 'Legal');
        $pdf::SetY($y_Inicio);

        // ------------------------------------------ [PACIENTE] ------------------------------------------
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell($widthTitulo, $height, 'DATOS PACIENTE', 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);

        $pdf::MultiCell(50, $height, 'Nº SEGURO: '.$model->hcl_poblacion_matricula, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(20, $height, $model->hcl_poblacion_matricula, 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(35, $height, 'EDAD: '.$model->edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(20, $height, $model->edad.' años', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $paciente = 'PACIENTE: '.$model->hcl_poblacion_nombrecompleto;
        $pdf::MultiCell(110, $height, $paciente, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(100, $height, $model->hcl_poblacion_nombrecompleto, 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                
        $pdf::MultiCell(80, $height, 'EMPRESA: '.$model->hcl_empleador_empresa, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(100, $height, $model->hcl_empleador_empresa, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);


        // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
        $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);
        $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
        $modelHcl_especialidad = Hcl_especialidad::find($modelLab_registroprogramacion->idhcl_especialidad);
        $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
        $modelHcl_cuaderno = Hcl_cuaderno::find($modelLab_registroprogramacion->idhcl_cuaderno);
        $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';

        $pdf::MultiCell($widthTitulo, 2, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);
        
        $label_Hospitalizado = 'HOSPITALIZADO';
        $label_Tipo = 'TIPO';
        $label_Medico = 'MÉDICO SOL.';
        $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
        $y_Tipo = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
        $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
        $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico);

        $label_Fecha = 'FECHA';
        $label_Especialidad = 'ESPECIALIDAD';
        $label_Diagnostico = 'DIAGNÓSTICO';
        $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
        $y_Especialidad = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
        $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
        $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);

        $hospitalizado = $modelLab_registroprogramacion->hospitalizado == 1? "SI" : "NO";
        $fechaprogramacion = date('d-m-Y', strtotime($modelLab_registroprogramacion->fechaprogramacion));
        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(48, $height, $hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, $fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(13, $height, 'HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(20, $height, $modelLab_registroprogramacion->horaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Tipo, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(48, $height, $model->especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(35, $height, $model->cuaderno, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(48, $height, $model->personal, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        
        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Diagnostico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::MultiCell(45, $height, $modelLab_registroprogramacion->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        
        // $pdf::MultiCell(120, $height, 'OBSERVACIÓN:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell('', '', $modelLab_registroprogramacion->observacion, 0, 'L', 0, 1, '', '', true, 0, false, true, 67, 20);

        
        // ------------------------------------------ [MUESTREO LABORATORIO] ------------------------------------------
        $pdf::MultiCell($widthTitulo, 2, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
        $pdf::SetFont($tipoLetra, 'B', $tamanio);
        $pdf::MultiCell($widthTitulo, $height, 'MUESTREO', 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);

        $widthLabel = $widthLabel - 9;
        // $pdf::MultiCell($widthLabel, $height, 'Nº FOLIO', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, ': '.$model->numero_muestra, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $fecha_muestra = date('d-m-Y', strtotime($model->fecha_muestra));
        $pdf::MultiCell(35, $height, 'FECHA: '.$fecha_muestra, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, ': '.$model->fecha_muestra, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(25, $height, 'HORA: '.$model->hora_muestra, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, ': '.$model->hora_muestra, 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $estado = '';
        if($model->estado_muestra == Lab_estado::idEstado_muestra_Buena)
            $estado = Lab_estado::Estado_muestra_Buena;
        elseif($model->estado_muestra == Lab_estado::idEstado_muestra_Mala)
            $estado = Lab_estado::Estado_muestra_Mala;
        elseif($model->estado_muestra == Lab_estado::idEstado_muestra_Regular)
            $estado = Lab_estado::Estado_muestra_Regular;

        $pdf::MultiCell(45, $height, 'ESTADO: '.$estado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, ': '.$estado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(20, $height, 'Nº SALA: '.$model->numero_sala, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, ': '.$model->numero_sala, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);

        $pdf::MultiCell(25, $height, 'Nº CAMA: '.$model->numero_cama, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(30, $height, ': '.$model->numero_cama, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

        // $pdf::MultiCell(25, $height, 'DESCRIPCIÓN: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
        // $pdf::MultiCell(100, '', $model->descripcion_muestra, 0, 'L', 0, 1, '', '', true, 0, false, true, 60, 10);


        // --------------------------------------- [EXÁMENES] ---------------------------------------
        // $pdf::MultiCell($widthTitulo, 2, 'aaaa', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
        $pdf::SetFont($tipoLetra, '', $tamanio);

        $modeloExamenGrupo = DB::table('lab_resultados_examen as re')
                ->leftjoin('lab_perfil_examenes as pe', 'pe.idlab_examenes', '=', 're.idlab_examenes')
                ->leftjoin('lab_grupos as g', 'g.id', '=', 'pe.idlab_grupos')
                // ->join('lab_grupos as g', 'g.id', '=', 'pe.idlab_grupos')
                ->select(
                    // DB::raw("case when re.is_resultado = 0 then pe.idlab_grupos else 0 end as idlab_grupos"),
                    // DB::raw("case when re.is_resultado = 0 then g.nombre else 'RESULTADO' end as grupo")

                    'pe.idlab_grupos', 'g.nombre as grupo'
                )
                ->where([
                    ['re.eliminado', '=', 0],
                    ['re.idlab_resultados_programacion', '=', $model->id]
                ])
                // ->groupBy('re.is_resultado', 'idlab_grupos', 'grupo')
                ->groupBy('pe.idlab_grupos', 'g.nombre')
                ->get();
        // print_r( $modeloExamenGrupo );
        // return;

        $modeloExamen = DB::table('lab_resultados_examen as re')
                            ->leftjoin('lab_perfil_examenes as pe', 'pe.idlab_examenes', '=', 're.idlab_examenes')
                            ->leftjoin('lab_grupos as g', 'g.id', '=', 'pe.idlab_grupos')
                            ->select('re.id', 're.codigo_examen', 're.nombre_examen', 're.respuesta',
                                DB::raw("case when re.is_resultado = 0 then pe.idlab_grupos else 0 end as idlab_grupos"),
                                     'g.nombre as grupo')
                            ->where([
                                ['re.eliminado', '=', 0],
                                ['re.idlab_resultados_programacion', '=', $model->id]
                            ])
                            ->orderBy('re.nombre_examen', 'ASC')
                            ->get();
        // print_r($modeloExamen);
        // return;
        for($i = 0; $i < count($modeloExamenGrupo); $i++)
        {
            $pdf::MultiCell(100, 5, '', 0, 'R', 0, 1, '', '', true, 0, false, true, 5, 10);
            
            // $grupo = $modeloExamenGrupo[$i]->grupo == ''? 'NO DIFINIDO' : $modeloExamenGrupo[$i]->grupo;
            $grupo = $modeloExamenGrupo[$i]->grupo;
            if($grupo != '')
            {
                $pdf::MultiCell(95, $height, $grupo, 'B', 'R', 0, 0, '', '', true, 0, false, true, $height, 10);
                $pdf::MultiCell(95, $height, '', 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
            }


            $hCabecera = 4;
            $pdf::MultiCell(80, $hCabecera, '', 'LT', 'L', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(32, $hCabecera, 'RESULTADO', 1, 'C', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(28, $hCabecera, 'VALOR', 'TR', 'L', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(50, $hCabecera, '', 'TR', 'L', 0, 1, '', '', true, 0, false, true, $hCabecera, 10);

            $pdf::MultiCell(80, $hCabecera, 'EXÁMEN', 'LB', 'L', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(16, $hCabecera, 'DENTRO', 1, 'L', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(16, $hCabecera, 'FUERA', 1, 'L', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(28, $hCabecera, 'REFERENCIA', 'BR', 'L', 0, 0, '', '', true, 0, false, true, $hCabecera, 10);
            $pdf::MultiCell(50, $hCabecera, 'MÉTODO', 'BR', 'L', 0, 1, '', '', true, 0, false, true, $hCabecera, 10);
            // echo ">>> ".count($modeloExamen);
            // return;

            for($j = 0; $j < count($modeloExamen); $j++)
            {
                // echo "<br> --->>> ".$modeloExamenGrupo[$i]->idlab_grupos .'==='. $modeloExamen[$j]->idlab_grupos;
                // return;
                if($modeloExamenGrupo[$i]->idlab_grupos == $modeloExamen[$j]->idlab_grupos)
                {
                    $modelo = DB::table('lab_resultados_variables')
                                ->select('id', 'nombre', 'valor_sup', 'valor_inf', 'unidad', 'metodo')
                                ->where('idlab_resultados_examen', '=', $modeloExamen[$j]->id)
                                ->orderBy('nombre', 'ASC')
                                ->get();
                    // print_r($modelo);
                    // return;
                    $examen = $modeloExamen[$j]->nombre_examen;//.' ('.$modeloExamen[$j]->codigo_examen.')';
                    $respuesta = $modeloExamen[$j]->respuesta;
                    
                    $pdf::MultiCell(80, $height, $examen, 'B', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                    for($k = 0; $k < count($modelo); $k++)
                    {
                        if($respuesta >= $modelo[$k]->valor_inf && $respuesta <= $modelo[$k]->valor_sup)
                        {
                            $dentro = $respuesta;
                            $fuera = '-';
                        }
                        else
                        {
                            $dentro = '-';
                            $fuera = $respuesta;
                        }
                        // echo "<br> ===>> ".$dentro;

                        // $pdf::MultiCell(80, $height, $modelo[$k]->nombre, $borde1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                        $pdf::MultiCell(16, $height, $dentro, 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
                        $pdf::MultiCell(16, $height, $fuera, 'B', 'C', 0, 0, '', '', true, 0, false, true, $height, 10);
                        $valorReferencia = $modelo[$k]->valor_inf.' - '.$modelo[$k]->valor_sup.' '.$modelo[$k]->unidad;
                        $pdf::MultiCell(28, $height, $valorReferencia, 'B', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
                        $pdf::MultiCell(50, $height, $modelo[$k]->metodo, 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
                    }
                }
            }
        }
        // return;
        // ************************ [footer] *******************************************
        /*
        $pdf::SetY(143);
        $pdf::SetFont('courier', 'B', 7);
        $pdf::MultiCell('', '','Usuario: '.$usuario, 'T', 'L', 0);
        // $pdf::SetY(-10);
        $pdf::SetY(143);
        $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
        
        $pdf::SetFont('courier', '', 9);
        $pdf::SetY(135);
        $pdf::MultiCell(50, '', 'Firma Médico', 'T', 'C', 0, 1, 80);

        $pdf::SetFont('courier', 'B', 7);
        $pdf::SetY(143);
        $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
        */
        // ****************************************************************************
        

        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("resultadoProgramacion.pdf", "I");

        mb_internal_encoding('utf-8');
    }
}