<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

class Lab_estado extends Model
{
	protected $table = 'lab_estado';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	const SOLICITUD = 1;
	const PROGRAMACION = 2;
	const ANULADO = 3;
	const ENTREGADO = 4;

	//  ------------------------------------ [ MUESTREO LABORATORIO ] ------------------------------------
	const idEstado_muestra_Buena = 1;
	const idEstado_muestra_Mala = 2;
	const idEstado_muestra_Regular = 3;
	
	const Estado_muestra_Buena = 'BUENA';
	const Estado_muestra_Mala = 'MALA';
	const Estado_muestra_Regular = 'REGULAR';
	// ---------------------------------------------------------------------------------------------------
}