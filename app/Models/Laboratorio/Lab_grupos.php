<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Lab_grupos extends Model
{
	protected $table = 'lab_grupos';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	

	public static function muestraGrupos($idalmacen)
	{
		$modelo = Lab_grupos::
                select('id', 'nombre', 
                    DB::raw("(select count(*) from lab_perfil_examenes where idlab_grupos = lab_grupos.id) as total_examenes")
                )
                ->where([
                    ['eliminado', '=', 0],
                    ['idalmacen', '=', $idalmacen]
                ])
                ->orderBy('nombre', 'ASC')->get()->toJson();
        return $modelo;
	}
}