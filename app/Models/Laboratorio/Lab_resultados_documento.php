<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_resultados_documento extends Model
{
	protected $table = 'lab_resultados_documento';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public static function registraArchivos($arrayParametro)
	{
        $files = $arrayParametro['files'];
        $id = $arrayParametro['id'];
        
        foreach ($files as $file)
        {
            $name = $file->getClientOriginalName();
            $nombre = $id.'_'.$name;
            // $file->move(public_path('uploads/Imagenologia/2018'), $nombre);
            // $file->move('E:\CNS\Laboratorio\2018', $nombre);
            $file->move(config('app.rutaArchivosLaboratorio'), $nombre);
            
            $modelo = new Lab_resultados_documento();
            $modelo->archivo = $nombre;
            $modelo->idlab_resultados_programacion = $id;
            $modelo->usuario = Auth::user()['name'];
            $modelo->save();
        }
    }
}