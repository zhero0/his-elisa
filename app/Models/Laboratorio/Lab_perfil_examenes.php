<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_perfil_examenes extends Model
{
	protected $table = 'lab_perfil_examenes'; 
	public $timestamps = false;
	
	public static function registraExamenVariables($arrayDatos)
	{
		$request = $arrayDatos['request'];
		$modelo = $arrayDatos['model'];
		$arrayVariables = json_decode($request->txtListaVariables);
		
		for($i = 0; $i < count($arrayVariables); $i++)
        {
        	$model = new Lab_examenes_variables;
        	$model->orden = $arrayVariables[$i]->orden;
        	$model->id_lab_examenes = $modelo->id;
        	$model->id_lab_variables = $arrayVariables[$i]->id;
        	$model->idlab_grupos = $arrayVariables[$i]->idlab_grupos;
        	$model->nombre = $arrayVariables[$i]->nombre;
        	$model->usuario = Auth::user()['name'];
        	$model->save();
        }
	}
	
}