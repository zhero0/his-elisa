<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

class Lab_programacion_view extends Model
{
	protected $table = 'lab_programacion_view';
	public $timestamps = false;
}