<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Auth;

class Lab_resultados_lista extends Model
{ 
	protected $table = 'lab_resultados_lista';
	public $timestamps = false;
}