<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

class Lab_tipo_variables extends Model
{
	protected $table = 'lab_tipo_variables';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at


	// ---------------------------------------------------------------------------------------------------
	// ------------------------------------------ [ TIPO DATO ] ------------------------------------------
	const numero = 1;
	const opcion_simple = 2;
	const opcion_multiple = 3;
	const texto = 4;
	const formula = 5;
	// ---------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------------

}