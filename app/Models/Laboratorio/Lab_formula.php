<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_formula extends Model
{
	protected $table = 'lab_formula';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	
	public static function escogeFuncionFormula($arrayFormula_Examenes) {
		// for($i = 0; $i < count($arrayFormula_Examenes); $i++)
		// {
		// 	echo "<br> ============================= <br>";
		// 	print_r($arrayFormula_Examenes[$i]);
		// }
		// return;

		for($i = 0; $i < count($arrayFormula_Examenes); $i++)
		{
			$class_formula = $arrayFormula_Examenes[$i]['class_formula'];
			$idlab_examenes_resultado = $arrayFormula_Examenes[$i]['idlab_examenes_resultado'];
			$arrayParametro = $arrayFormula_Examenes[$i]['arrayFormula_Examenes'];
			switch($class_formula) {
                case 'calculaFormula_hemograma_1':
                    $resultado = Lab_formula::calculaFormula_hemograma_1($idlab_examenes_resultado, $arrayParametro);
                    break;
                case 'calculaFormula_hemograma_2':
                    $resultado = 77; //Lab_formula::calculaFormula_hemograma_2($arrayParametro);
                    break;
                case 'calculaFormula_hemograma_3':
                    $resultado = Lab_formula::calculaFormula_hemograma_3($idlab_examenes_resultado, $arrayParametro);
                    break;
                // case 4:
                //     $resultado = Lab_formula::calculaFormula_hemograma_4($arrayParametro);
                //     break;
                // case 5:
                //     $resultado = Lab_formula::calculaFormula_hemograma_5($arrayParametro);
                //     break;
                default:
                    $resultado = -1;
                    break;
            }
            $arrayFormula_Examenes[$i]['arrayFormula_Examenes'] = json_encode($resultado);
        }

        // ----------------------------------------------------------------
  //       for($i = 0; $i < count($arrayFormula_Examenes); $i++)
		// {
		// 	echo "<br> ============================= <br>";
		// 	print_r($arrayFormula_Examenes[$i]);
		// }
		// return;
		// ----------------------------------------------------------------

		return json_encode($arrayFormula_Examenes);
	}
	
	// --------------------------------------- hemograma --------------------------------------
	public static function calculaFormula_hemograma_1($idlab_examenes_resultado, $arrayParametro) { // (VAR1 / VAR2) * 10
		$arrayParametro = json_decode($arrayParametro);
		
		// (H02 / H01) x 10
		// (VAR1 / VAR2) * 10
		$VAR1 = 0;
		$VAR2 = 0;
		for($i = 0; $i < count($arrayParametro); $i++)
        {
        	$variable = $arrayParametro[$i]->lab_parametro_variable;
        	switch ($variable) {
                case 'VAR1':
                    $VAR1 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR2':
                    $VAR2 = $arrayParametro[$i]->valor;
                    break;
            }
        }
		$resultado = $VAR2 > 0? ($VAR1/$VAR2)*10000000 : 0;
		$resultado = number_format($resultado, 2, '.', ',');
		
		for($i = 0; $i < count($arrayParametro); $i++)
		{
			if($idlab_examenes_resultado == $arrayParametro[$i]->idlab_examenes)
				$arrayParametro[$i]->valor = $resultado;
		}
		return $arrayParametro;
	}
	public static function calculaFormula_hemograma_2($idlab_examenes_resultado, $arrayParametro) { // (VAR1 / VAR2) * 100
		$arrayParametro = json_decode($arrayParametro);

		// (H03 / H02) x 100 
		// (VAR1 / VAR2) * 100
		// $VAR1 = $arrayParametro[0];
		// $VAR2 = $arrayParametro[1];
		
		$VAR1 = 0;
		$VAR2 = 0;
		for($i = 0; $i < count($arrayParametro); $i++)
        {
        	$variable = $arrayParametro[$i]->lab_parametro_variable;
        	switch ($variable) {
                case 'VAR1':
                    $VAR1 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR2':
                    $VAR2 = $arrayParametro[$i]->valor;
                    break;
            }
        }
        
		// $resultado = 0;
		// if($VAR2 > 0)
			$resultado = $VAR2 > 0? ($VAR1/$VAR2)*100 : 0;
		$resultado = number_format($resultado, 2, '.', ',');
		
		// return round($resultado, 2);
		
		for($i = 0; $i < count($arrayParametro); $i++)
		{
			if($idlab_examenes_resultado == $arrayParametro[$i]->idlab_examenes)
				$arrayParametro[$i]->valor = $resultado;
		}
		return $arrayParametro;
	}
	public static function calculaFormula_hemograma_3($idlab_examenes_resultado, $arrayParametro) { // (VAR1 + VAR2 + VAR3 + VAR4 + VAR5 + VAR6) = 100
		$arrayParametro = json_decode($arrayParametro);

		// (H09.01.01 + H09.01.02 + H09.01.03 + H09.01.04 + H09.01.05 + H09.01.06) = 100
		// (VAR1 + VAR2 + VAR3 + VAR4 + VAR5 + VAR6) = 100
		
		$VAR1 = 0;
		$VAR2 = 0;
		$VAR3 = 0;
		$VAR4 = 0;
		$VAR5 = 0;
		$VAR6 = 0;
		for($i = 0; $i < count($arrayParametro); $i++)
        {
        	$variable = $arrayParametro[$i]->lab_parametro_variable;
        	switch ($variable) {
                case 'VAR1':
                    $VAR1 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR2':
                    $VAR2 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR3':
                    $VAR3 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR4':
                    $VAR4 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR5':
                    $VAR5 = $arrayParametro[$i]->valor;
                    break;
                case 'VAR6':
                    $VAR6 = $arrayParametro[$i]->valor;
                    break;
            }
        }
        $resultado = $VAR1 + $VAR2 + $VAR3 + $VAR4 + $VAR5 + $VAR6;
        $resultado = number_format($resultado, 2, '.', ',');
		
		for($i = 0; $i < count($arrayParametro); $i++)
		{
			if($idlab_examenes_resultado == $arrayParametro[$i]->idlab_examenes)
				$arrayParametro[$i]->valor = $resultado;
		}
		
		return $arrayParametro;
	}
	public static function calculaFormula_hemograma_4($arrayParametro) { // (VAR1 + VAR2) / (100 + VAR2)
		// (H08 x H09) / (100 + H09)
		// (VAR1 + VAR2) / (100 + VAR2)
		$VAR1 = $arrayParametro[0];
		$VAR2 = $arrayParametro[1];
		
		$resultado = ($VAR1 + $VAR2) / (100 + $VAR2);
		
		return round($resultado, 2);
	}
	public static function calculaFormula_hemograma_5($arrayParametro) { // IMCOMPLETO....
		// (H20 X (H02 / 45) / H23.01
		// (VAR1 * (VAR2 / 45) / VAR3)
		$VAR1 = $arrayParametro[0];
		$VAR2 = $arrayParametro[1];
		$VAR3 = $arrayParametro[2];
		
		$resultado = ($VAR1 * ($VAR2 / 45) / $VAR3);
		
		return round($resultado, 2);
	}
	
	// --------------------------------------- QUIMICA  ---------------------------------------
	public static function calculaFormula_quimica_1($arrayParametro) { // VAR1 / VAR2
		// Q20 / Q07
		// VAR1 / VAR2
		$VAR1 = $arrayParametro[0];
		$VAR2 = $arrayParametro[1];
		
		$resultado = 0;
		if($VAR2 > 0)
			$resultado = $VAR1 / $VAR2;
		
		return round($resultado, 2);
	}
	public static function calculaFormula_quimica_2($arrayParametro) { // VAR1 - VAR2
		// Q13 - Q14
		// VAR1 - VAR2
		$VAR1 = $arrayParametro[0];
		$VAR2 = $arrayParametro[1];
		
		$resultado = $VAR1 - $VAR2;
		
		return round($resultado, 2);
	}
	public static function calculaFormula_quimica_3($arrayParametro) { // VAR1 / 5
		// Q28 / 5
		// VAR1 / 5
		$VAR1 = $arrayParametro[0];
		
		$resultado = $VAR1 / 5;
		
		return round($resultado, 2);
	}
	
}