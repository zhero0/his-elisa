<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

class Lab_variables extends Model
{
	protected $table = 'lab_variables';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public static function generarNumero($idalmacen)
	{
		$numero = Lab_variables::where('idalmacen', '=', $idalmacen)->max('numero');
		return $numero+1;
	}

}