<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

class Lab_especialidad extends Model
{
	protected $table = 'lab_especialidad';
	public $timestamps = false;
}