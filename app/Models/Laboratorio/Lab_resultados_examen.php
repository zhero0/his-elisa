<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Auth;

use \App\Models\Laboratorio\Lab_resultados_variables;

class Lab_resultados_examen extends Model
{ 
    protected $table = 'lab_resultados_examen';
    public $timestamps = false;
    

    public static function registrarResultadoExamen($request, $id)
    {
        $jsonDatos = json_decode($request->txtJsonDatos);
        $jsonDatosForm = json_decode($request->txtJsonDatosGuardar);

        foreach($jsonDatosForm as $dato) {
            $lab_resultados_examen = json_decode($dato);

            for($i = 0; $i < count($lab_resultados_examen); $i++)
            {
                $jsonResultado = json_decode($lab_resultados_examen[$i]->jsonResultado);
                
                $model = new Lab_resultados_examen;
                $model->idlab_resultados_programacion = $id;
                $model->idlab_formulaparametro = isset($lab_resultados_examen[$i]->idlab_formulaparametro)? 
                                                    $lab_resultados_examen[$i]->idlab_formulaparametro : null;
                $model->lab_parametro_variable = isset($lab_resultados_examen[$i]->lab_parametro_variable)? 
                                                    $lab_resultados_examen[$i]->lab_parametro_variable : null;
                $model->idlab_examenes = $jsonResultado->idlab_examenes;
                $model->codigo_examen = $jsonResultado->codigo;
                $model->nombre_examen = $jsonResultado->nombre;
                $model->respuesta = $lab_resultados_examen[$i]->respuesta;
                $model->is_resultado = $lab_resultados_examen[$i]->is_resultado;
                $model->usuario = Auth::user()['name'];
                
                if($model->save())
                {
                    if($lab_resultados_examen[$i]->is_resultado == 0) // VARIABLES DE UN EXAMEN "NORMAL"
                        $lab_examenesprogramados_variables = json_decode($jsonDatos->lab_examenesprogramados_variables);
                    else // VARIABLES DE UN EXAMEN "RESULTADO"
                        $lab_examenesprogramados_variables = json_decode($jsonDatos->modelVariableResultado);
                    
    		        for($j = 0; $j < count($lab_examenesprogramados_variables); $j++)
    		        {
    		            if($lab_examenesprogramados_variables[$j]->idlab_examenes == $jsonResultado->idlab_examenes)
    		            {
    		                $modelo = new Lab_resultados_variables;
    		                $modelo->idlab_resultados_examen = $model->id;
    		                $modelo->idlab_variables = $lab_examenesprogramados_variables[$j]->idlab_variables;
    		                $modelo->nombre = $lab_examenesprogramados_variables[$j]->variable;
    		                $modelo->valor_sup = $lab_examenesprogramados_variables[$j]->valor_sup;
    		                $modelo->valor_inf = $lab_examenesprogramados_variables[$j]->valor_inf;
    		                $modelo->unidad = $lab_examenesprogramados_variables[$j]->unidad;
    		                $modelo->metodo = $lab_examenesprogramados_variables[$j]->metodo;
    		                $modelo->sexo = $lab_examenesprogramados_variables[$j]->sexo;
    		                $modelo->edad_inicial = $lab_examenesprogramados_variables[$j]->edad_inicial;
    		                $modelo->edad_final = $lab_examenesprogramados_variables[$j]->edad_final;
    		                $modelo->save();
    		            }
    		        }
                }
            }
        }
    }

}