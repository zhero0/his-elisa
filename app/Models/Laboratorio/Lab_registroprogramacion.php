<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use \App\Models\Administracion\Datosgenericos;

use \App\Models\Laboratorio\Lab_estado;
use \App\Models\Laboratorio\Lab_resultados_programacion;

use \App\Models\Afiliacion\Hcl_especialidad;
use \App\Models\Afiliacion\Hcl_cuaderno;
use \App\Models\Afiliacion\Hcl_poblacion;

use Illuminate\Support\Facades\DB;
use Elibyy\TCPDF\Facades\TCPDF;
use Auth;

class Lab_registroprogramacion extends Model
{
	protected $table = 'lab_registroprogramacion';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	
	public static function generarNumero($idalmacen)
	{
		$numero = Lab_registroprogramacion::where('idalmacen', '=', $idalmacen)->max('numero');
		return $numero+1;
	}

	/*
		Este método se invoca desde "SALUD"
	*/
	public static function registraProgramacion_Laboratorio($arrayValor)
	{
		$request = $arrayValor['request'];
		$idalmacen = $arrayValor['idalmacen'];
		$arrayExamenes = $arrayValor['arrayExamenes'];
		$modeloSalud = $arrayValor['model'];

		$txtHiddenJSONdatos = json_decode($request->txtHiddenJSONdatos);
        $jsonDatoExtra = json_decode($txtHiddenJSONdatos->jsonDatoExtra);
        
		if($jsonDatoExtra->embarazada == 1 && $jsonDatoExtra->emergencia == 1)
			$idhcl_especialidad = Hcl_especialidad::Emergencias;
		else if($jsonDatoExtra->embarazada == 1 && $jsonDatoExtra->emergencia == 0)
			$idhcl_especialidad = Hcl_especialidad::Embarazo;
		else
			$idhcl_especialidad = Hcl_especialidad::Consulta_Externa;

		$model = new Lab_registroprogramacion;
		$numero = Lab_registroprogramacion::generarNumero($idalmacen);
        $date = date_create($request->fechaprogramacion);

		$model->numero = $numero;
        $model->idhcl_poblacion = $request->txtidhcl_poblacion;
        $model->idhcl_especialidad = $idhcl_especialidad;
        $model->idalmacen = $idalmacen;
        $model->idhcl_cuaderno = $request->idhcl_cuaderno;
        $model->iddatosgenericos = $request->iddatosgenericos;
        $model->hospitalizado = $request->checkHospitalizado == true? 1 : 0;
        $model->fechaprogramacion = null;
        $model->horaprogramacion = null;
        $model->diagnostico = $request->txtHcl_cie;
        $model->observacion = $jsonDatoExtra->observaciones_laboratorio;
        $model->idlab_estado = Lab_estado::SOLICITUD;
        $model->usuario = Auth::user()['name'];
		$model->fechaprogramacion_sistema = date('Y-m-d');
		$model->horaprogramacion_sistema = date('g:i A');
		$model->idhcl_cabecera_registro = $modeloSalud->id;
        
        if($model->save())
        {
            $arrayDatos = array(
                'request' => $request,
                'model' => $model,
                'arrayExamenes' => $arrayExamenes
            );
            Lab_resultados_programacion::registraResultado($arrayDatos);
        }
	}

	public static function imprimeProgramacion($id)
    {
        $imprimirDirecto = 1;
        $model = Lab_registroprogramacion::find($id);
        $numeroImprimir = $model->numero;

        $modelResultado = DB::table('lab_resultados_programacion')
	            ->select('*', DB::raw("date_part('year',age(hcl_poblacion_fechanacimiento)) as edad"))
				->where([
					['eliminado', '=', 0],
					['idlab_registroprogramacion', '=', $model->id]
				])
				->first();

        ob_end_clean();
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        $pdf::SetTitle("Imagenologia");
        $pdf::SetSubject("TCPDF");

        $pdf::SetCreator('PDF_CREATOR');
        $pdf::SetAuthor('usuario');
        $pdf::SetKeywords('TCPDF, PDF, example, test, guide');
        
        //cambiar margenes
        $pdf::SetMargins(10, 15, 10, 15);
        $pdf::SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf::SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf::SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //$pdf::setAutoPageBreak(true);

        $pdf::startPageGroup();
        
        //set image scale factor
        $pdf::setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf::setJPEGQuality(100);

        $pdf::setHeaderCallback(function($pdf)use($numeroImprimir) {
            // $y = $pdf->GetY();
            $pdf->SetFillColor(230, 230, 230);
            // $pdf->RoundedRect(10, $y, $pdf->getPageWidth()-20, 20, 2.50, '0110');

            // $pdf->SetY($y + 1);
            // $pdf->Image('img/pacs1.jpg', 12, '', '', 16);
            // $pdf->SetFont('courier', 'B', 20);
            // $pdf->SetTextColor(0, 0, 0);
            // $pdf->SetY($pdf->GetY());
            // $x = $pdf->getPageWidth();
            // $y = $pdf->getPageHeight();
            // $pdf->MultiCell(130, 10, 'LABORATORIO C.N.S.', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
            // $pdf->MultiCell(18, 10, '', 0, 'R', '', 0, '', '', 1, '', '', '', 10, 'M');
            // $pdf->MultiCell(40, 10, 'N°:'.$numeroImprimir , 0, 'R', '', 1, '', '', 1, '', '', '', 10, 'M');
            // $pdf->Line(31, $pdf->getY()+6, $pdf->getPageWidth()-12, $pdf->getY()+6);

            $pdf->SetFont('courier', 'B', 10);
            $pdf->MultiCell(130, 5, 'LABORATORIO C.N.S.', 0, 'L', '', 1, '', '', 1, '', '', '', 5, 'M');

            $pdf->SetFont('helvetica', 'B', 15);
            $pdf->MultiCell(170, 8, 'SOLICITUD DE EXÁMEN COMPLEMENTARIO Nº '.$numeroImprimir, 0, 'L', '', 0, '', '', 1, '', '', '', 8, 'M');

            $style = array(
                'position' => 'R',
                'align' => 'R',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'hpadding' => 'auto',
                'vpadding' => 'auto',
                'fgcolor' => array(0,0,0),
                'bgcolor' => false, //array(255,255,255),
                'text' => true,
                'font' => 'helvetica',
                'fontsize' => 8,
                'stretchtext' => 4
            );
                // $pdf->Cell(0, 0, 'CODE 128 AUTO', 0, 1);
            $pdf->write1DBarcode($numeroImprimir, 'C128', '', '', '', 18, 0.4, $style, 'N');
            
            // $pdf->Line(10, $pdf->getY()+7, $pdf->getPageWidth()-12, $pdf->getY()+7);
        });
        $usuario = Auth::user()['name'];
        $fecha = date('d-m-Y');

        // setPage
        $pdf::AddPage();
        $pdf::setPage(1, true);
        $pdf::SetFillColor(255, 255, 255);

        // $y_Inicio = 30;
        $y_Inicio = 18;
        $height = 5;
        $width = 190;
        $widthLabel = 25;
        $widthDosPuntos = 4;
        $espacio = 3;
        $tipoLetra = 'helvetica';
        $tamanio = 9;
        $widthTitulo = 50;
        
        $especialidad_Examenes = DB::table('lab_examenes_registroprogramacion as rp')
                            ->join('lab_examenes as e', 'rp.idlab_examenes', '=', 'e.id')
                            ->join('lab_especialidad as esp', 'e.idlab_especialidad', '=', 'esp.id')
                            ->select(
                                'e.idlab_especialidad', 'esp.nombre'
                            )
                            ->where([
                                ['rp.eliminado', '=', 0],
                                ['rp.idlab_registroprogramacion', '=', $model->id]
                            ])
                            ->groupBy('e.idlab_especialidad', 'esp.nombre')
                            ->orderBy('esp.nombre', 'ASC')
                            ->get();
        
        for($e = 0; $e < count($especialidad_Examenes); $e++)
        {
        	if($e > 0)
        		$pdf::AddPage();

	        $pdf::SetY($y_Inicio);
	        // ------------------------------------------ [PACIENTE] ------------------------------------------
	        $pdf::SetFont($tipoLetra, 'B', $tamanio);
	        $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        $pdf::SetFont($tipoLetra, '', $tamanio);

	        $pdf::MultiCell($widthLabel, $height, 'Nº SEGURO: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(100, $height, $modelResultado->hcl_poblacion_matricula, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

	        $pdf::MultiCell($widthLabel, $height, 'PACIENTE: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(100, $height, $modelResultado->hcl_poblacion_nombrecompleto, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(15, $height, 'EDAD: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(20, $height, $modelResultado->edad.' años', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

	        $pdf::MultiCell($widthLabel, $height, 'EMPRESA: ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(100, $height, $modelResultado->hcl_empleador_empresa, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);


	        // --------------------------------------- [DATOS PROGRAMACIÓN] ---------------------------------------
	        $modelDatosgenericos = Datosgenericos::find($model->iddatosgenericos);
	        $model->personal = $modelDatosgenericos != ''? $modelDatosgenericos->nombres.' '.$modelDatosgenericos->apellidos : '';
	        $modelHcl_especialidad = Hcl_especialidad::find($model->idhcl_especialidad);
	        $model->especialidad = $modelHcl_especialidad != ''? $modelHcl_especialidad->descripcion : 'NO ASIGNADO';
	        $modelHcl_cuaderno = Hcl_cuaderno::find($model->idhcl_cuaderno);
	        $model->cuaderno = $modelHcl_cuaderno != ''? $modelHcl_cuaderno->nombre : '';

	        // $pdf::MultiCell($widthTitulo, 2, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
	        // $pdf::SetFont($tipoLetra, 'B', $tamanio);
	        // $pdf::MultiCell($widthTitulo, $height, 'DATOS PROGRAMACIÓN', 'B', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        $pdf::SetFont($tipoLetra, '', $tamanio);
	        
	        $label_Hospitalizado = 'HOSPITALIZADO';
	        $label_Tipo = 'TIPO';
	        $label_Medico = 'MÉDICO SOL.';
	        $y_Hospitalizado = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
	        $y_Tipo = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
	        $y_Medico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
	        $y_label_maximo_col1 = max($y_Hospitalizado, $y_Tipo, $y_Medico);

	        $label_Fecha = 'FECHA';
	        $label_Especialidad = 'ESPECIALIDAD';
	        $label_Diagnostico = 'DIAGNÓSTICO';
	        $y_Fecha = ceil($pdf::GetStringWidth($label_Hospitalizado, $tipoLetra, '', $tamanio, false)) + 2;
	        $y_Especialidad = ceil($pdf::GetStringWidth($label_Tipo, $tipoLetra, '', $tamanio, false));
	        $y_Diagnostico = ceil($pdf::GetStringWidth($label_Medico, $tipoLetra, '', $tamanio, false));
	        $y_label_maximo_columna2 = max($y_Fecha, $y_Especialidad, $y_Diagnostico);

	        $hospitalizado = $model->hospitalizado == 1? "SI" : "NO";
	        $fechaprogramacion = date('d-m-Y', strtotime($model->fechaprogramacion));
	        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(48, $height, $hospitalizado, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Fecha, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(45, $height, $fechaprogramacion, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(13, $height, 'HORA', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(20, $height, $model->horaprogramacion, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

	        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Tipo, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(48, $height, $model->especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Especialidad, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(35, $height, $model->cuaderno, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        
	        $pdf::MultiCell($y_label_maximo_col1, $height, $label_Medico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(48, $height, $model->personal, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        
	        $pdf::MultiCell($y_label_maximo_columna2, $height, $label_Diagnostico, 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($espacio, $height, ': ', 0, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(45, $height, $model->diagnostico, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        
	        // ------------------------------------------ [OBSERVACIÓN] ------------------------------------------
	        $pdf::MultiCell(120, $height, 'OBSERVACIÓN:', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell('', '', $model->observacion, 0, 'L', 0, 1, '', '', true, 0, false, true, 67, 20);
	        $pdf::setY($pdf::getY());


	        // --------------------------------------- [EXÁMENES] ---------------------------------------
	        // $pdf::MultiCell($widthTitulo, $height, '', 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell($widthTitulo, 2, '', 0, 'L', 0, 1, '', '', true, 0, false, true, 2, 10);
	        $pdf::SetFont($tipoLetra, 'B', $tamanio);
	        $pdf::MultiCell(25, $height, 'EXÁMENES: ', 'B', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        $pdf::MultiCell(100, $height, $especialidad_Examenes[$e]->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        $pdf::SetFont($tipoLetra, '', $tamanio);
	        $modelo = DB::table('lab_examenes_registroprogramacion as rp')
	                            ->join('lab_examenes as e', 'rp.idlab_examenes', '=', 'e.id')
	                            ->select(
	                                'e.codigo', 'e.nombre'
	                            )
	                            ->where([
	                                ['rp.eliminado', '=', 0],
	                                ['rp.idlab_registroprogramacion', '=', $model->id],
	                                ['e.idlab_especialidad', '=', $especialidad_Examenes[$e]->idlab_especialidad]
	                            ])
	                            ->orderBy('e.nombre', 'ASC')
	                            ->get();
	        // $pdf::MultiCell(30, $height, 'CÓDIGO', 1, 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	        // $pdf::MultiCell(120, $height, 'NOMBRE', 1, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        for($j = 0; $j < count($modelo); $j++)
	        {
	            // $pdf::MultiCell(30, $height, $modelo[$j]->codigo, 'LBR', 'L', 0, 0, '', '', true, 0, false, true, $height, 10);
	            // $pdf::MultiCell(120, $height, $modelo[$j]->nombre, 'BR', 'L', 0, 1, '', '', true, 0, false, true, $height, 10);

	            $pdf::MultiCell(120, $height, '> '.$modelo[$j]->nombre, 0, 'L', 0, 1, '', '', true, 0, false, true, $height, 10);
	        }


	        // ************************ [footer] *******************************************
            $pdf::SetY(143);
            $pdf::SetFont('courier', 'B', 7);
            $pdf::MultiCell('', '','Usuario: '.$usuario, 'T', 'L', 0);
            // $pdf::SetY(-10);
            $pdf::SetY(143);
            $pdf::MultiCell(60, '', 'Fecha: '.$fecha, 0, 'L', 0, 1, 60);
            
            $pdf::SetFont('courier', '', 9);
            $pdf::SetY(135);
            $pdf::MultiCell(50, '', 'Firma Médico', 'T', 'C', 0, 1, 80);

            $pdf::SetFont('courier', 'B', 7);
            $pdf::SetY(143);
            $pdf::MultiCell(40, '', 'Página ' . $pdf::getGroupPageNo() . ' de ' . $pdf::getPageGroupAlias(), 0, 'R', 0, 1,$pdf::getPageWidth() - 45);
	        // ****************************************************************************
        }
        

        // Muestra el panel para imprimir de forma directa...
        if($imprimirDirecto == 1)
            $pdf::IncludeJS('print(true);');

        $pdf::Output("resultadoProgramacion.pdf", "I");

        mb_internal_encoding('utf-8');
    }

}