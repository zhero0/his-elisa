<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

use \App\Models\Laboratorio\Lab_formularespuesta;

class Lab_examenes_variables extends Model
{
	protected $table = 'lab_examenes_variables'; 
	public $timestamps = false;
	
	public static function registraExamenVariables($arrayDatos)
	{
		$request = $arrayDatos['request'];
		$modelo = $arrayDatos['model'];
		$arrayVariables = json_decode($request->txtListaVariables);
		
		for($i = 0; $i < count($arrayVariables); $i++)
        {
        	$model = new Lab_examenes_variables;
        	$model->orden = $arrayVariables[$i]->orden;
        	$model->idlab_examenes = $modelo->id;
        	$model->idlab_variables = $arrayVariables[$i]->id;
        	$model->idlab_grupos = $arrayVariables[$i]->idlab_grupos;
        	$model->nombre = $arrayVariables[$i]->nombre;
        	$model->usuario = Auth::user()['name'];
        	if($model->save())
        	{
        		$modeloFormula = Lab_formularespuesta::where('idlab_variables', '=', $model->idlab_variables)->get();
	        	if(count($modeloFormula) > 0)
	        	{
	        		$model = new Lab_formularespuesta;
	        		$model->idlab_variables = $arrayVariables[$i]->id;
	        		$model->idlab_formulaparametro = null;
	        		$model->lab_parametro_variable = null;
	        		$model->idlab_examenes = $modelo->id;
	        		$model->lab_examenes = $arrayVariables[$i]->nombre;
	        		$model->usuario = Auth::user()['name'];
	        		$model->save();
	        	}
        	}
        }
	}
	

}