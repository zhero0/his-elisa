<?php

namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model; 
class Lab_tipo_muestra extends Model
{ 

	protected $table = 'lab_tipo_muestra'; 
	//protected $fillable = array('id', 'codigo', 'nombre','descripcion','usuario' );
	public $timestamps = false;

	public function scopeBusqueda($query, $dato="")
	{
		if (trim($dato) != "") {
			$resultado = $query -> where('nombre',"like","%$dato%");
									// ->orwhere('usuario','like','%'.$dato.'%'); 
		}else
		{
			$resultado = Lab_tipo_muestra::paginate(config('app.pagination'));	
		}		
	}   
}

