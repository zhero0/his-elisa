<?php
namespace App\Models\Laboratorio;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Lab_formularespuesta extends Model
{
	protected $table = 'lab_formularespuesta';
	public $timestamps = false; //YA NO GUARDARÁ LOS CAMPOS created_at Y updated_at
	

	public static function registraFormulaRespuesta($arrayDatos)
	{
		$modelo = $arrayDatos['model'];
		$request = $arrayDatos['request'];
		$jsonLab_formularespuesta = $arrayDatos['jsonLab_formularespuesta'];
		
		for($i = 0; $i < count($jsonLab_formularespuesta); $i++)
        {
        	$idlab_examenes = $jsonLab_formularespuesta[$i]->idlab_examenes;

        	$model = new Lab_formularespuesta;
        	$model->idlab_variables = $modelo->id;
        	$model->idlab_formulaparametro = $jsonLab_formularespuesta[$i]->idlab_formulaparametro;
        	$model->lab_parametro_variable = $jsonLab_formularespuesta[$i]->variable;
        	$model->idlab_examenes = $idlab_examenes > 0? $idlab_examenes : null;
        	$model->lab_examenes = $jsonLab_formularespuesta[$i]->lab_examenes;
        	$model->usuario = Auth::user()['name'];
        	$model->save();
        }
	}

}