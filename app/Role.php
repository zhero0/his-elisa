<?php namespace App;

use Esensi\Model\Contracts\ValidatingModelInterface;
use Esensi\Model\Traits\ValidatingModelTrait;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole implements ValidatingModelInterface
{
  use ValidatingModelTrait;

  protected $throwValidationExceptions = true;

  protected $fillable = [
    'name',
    'display_name',
    'description',
  ];

  protected $rules = [
    'name'      => 'required|unique:roles',
    'display_name'      => 'required|unique:roles',
  ];

  public function scopeBusquedaRol($query, $dato="")
  {
      if (trim($dato) != "")
        $resultado = $query -> where('name', 'like', '%'.$dato.'%')
                    ->orwhere('description', 'like','%'.$dato.'%');
  }

}
