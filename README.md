# HIS ELISA
## Table of Contents
1. [Información General](#general-info)
2. [Tecnologias](#technologies)
3. [Instalación](#installation)
4. [Colaboración](#collaboration)
5. [FAQs](#faqs)

## Información general

HIS ELISA es un Sistema de Gestión Hospitalaria que tiene entre sus principales virtudes y características las siguientes:
i.	Administra de manera integrada la información de un Historial Clínico digital.
ii.	Está desarrollado sobre herramientas de software libre, lo que significa que la institución no tiene que realizar gastos anuales adicionales en licencias de bases de datos o de desarrollo de software.
iii.	Por su naturaleza de código abierto, puede integrarse fácilmente a otros sistemas que tenga en la institución CNS, ya sean administrativo-financieros, de farmacia, costos, etc.
iv.	El sistema es modular y escalable, lo que significa que se puede implementar por módulos y abarcando diferentes aspectos hasta lograr la integración total de intercambio de información entre todos los niveles de la entidad.
v.	Al mismo tiempo, el sistema es horizontal y transversal, lo que significa que se integran funcionalmente todos los establecimientos de salud en sus distintos niveles a nivel nacional. Esto brinda la enorme facilidad que si un asegurado requiere atención médica en un establecimiento ubicado en una ciudad diferente a su residencia, su expediente clínico completo puede ser consultado por el médico asignado a su atención. 
vi.	La información de todos los establecimientos de salud a nivel nacional estará disponible para los niveles decisionales del SSU mediante reportes gerenciales y de ayuda a la toma de decisiones, incluyendo la obtención automática de la mayoría de los datos e indicadores de salud solicitados por el SNIS.
vii.	El sistema es multiplataforma y multilocación, lo que significa que un doctor puede atender a un paciente desde cualquier lugar (porque puede acceder al expediente clínico desde su celular, tablet, laptop, computadora estática de oficina, etc.). Este aspecto es extremadamente útil sobre todo en circunstancias de pandemia o en casos donde el paciente realiza viajes por trabajo o de vacación. 

## Tecnologias
***
Una lista de tecnologias usadas en este proyecto:
- PHP                   (https://www.php.net/ChangeLog-8.php#8.0.2) 		version:  8.0.2 
- Laravel/Framework     (https://laravel.com/)   				version:  9.11
- Guzzlehttp            (https://docs.guzzlephp.org/en/stable/)                 version:  7.2 
- Laravel-adminlte      (https://github.com/jeroennoten/Laravel-AdminLTE)       version:  3.8
- Tcpdf-laravel         (https://tcpdf.org/)					version:  9.0

## Instalación
Pasos para implementar en servidor de producción:
1.- Realizar una copia del proyecto HIS ELISA
2.- Crear un paquete .json del proyecto -> node_modules
3.- Tener instalado NODE js
4.- ejecutar la siguiente linea de codigo en el proyecto de produccion 
	npm run production 
	npm run watch                     / permite vigilar los cambios en los archivos directorio
5.- encriptar en zip, y subir el proyecto o un repositorio para ejecutar 
6.- ejecutar el script de la bd en el servidor postgres 12
7.- revisar la correcta ejecucion del script, validando los id_eq de cada tabla, procedimientos, funciones y vistas.

## Colaboración
Autor intelectual, Ing. Luis Alberto Caballero Pérez.

## FAQs

Versionado del software 2, software libre y software de codigo abierto, como una plataforma para distribución de un sistema HIS hospitalario.







<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 2000 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
